gdjs.tirage_951Code = {};
gdjs.tirage_951Code.GDlama_95inspecteurObjects1= [];
gdjs.tirage_951Code.GDlama_95inspecteurObjects2= [];
gdjs.tirage_951Code.GDlama_95inspecteurObjects3= [];
gdjs.tirage_951Code.GDbouton_95retourObjects1= [];
gdjs.tirage_951Code.GDbouton_95retourObjects2= [];
gdjs.tirage_951Code.GDbouton_95retourObjects3= [];
gdjs.tirage_951Code.GDscore_951Objects1= [];
gdjs.tirage_951Code.GDscore_951Objects2= [];
gdjs.tirage_951Code.GDscore_951Objects3= [];

gdjs.tirage_951Code.conditionTrue_0 = {val:false};
gdjs.tirage_951Code.condition0IsTrue_0 = {val:false};
gdjs.tirage_951Code.condition1IsTrue_0 = {val:false};
gdjs.tirage_951Code.condition2IsTrue_0 = {val:false};
gdjs.tirage_951Code.condition3IsTrue_0 = {val:false};
gdjs.tirage_951Code.condition4IsTrue_0 = {val:false};
gdjs.tirage_951Code.condition5IsTrue_0 = {val:false};
gdjs.tirage_951Code.conditionTrue_1 = {val:false};
gdjs.tirage_951Code.condition0IsTrue_1 = {val:false};
gdjs.tirage_951Code.condition1IsTrue_1 = {val:false};
gdjs.tirage_951Code.condition2IsTrue_1 = {val:false};
gdjs.tirage_951Code.condition3IsTrue_1 = {val:false};
gdjs.tirage_951Code.condition4IsTrue_1 = {val:false};
gdjs.tirage_951Code.condition5IsTrue_1 = {val:false};


gdjs.tirage_951Code.eventsList0 = function(runtimeScene) {

{


gdjs.tirage_951Code.condition0IsTrue_0.val = false;
{
{gdjs.tirage_951Code.conditionTrue_1 = gdjs.tirage_951Code.condition0IsTrue_0;
gdjs.tirage_951Code.condition0IsTrue_1.val = false;
gdjs.tirage_951Code.condition1IsTrue_1.val = false;
gdjs.tirage_951Code.condition2IsTrue_1.val = false;
{
gdjs.tirage_951Code.condition0IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a0")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b1"));
if( gdjs.tirage_951Code.condition0IsTrue_1.val ) {
    gdjs.tirage_951Code.conditionTrue_1.val = true;
}
}
{
gdjs.tirage_951Code.condition1IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a0")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b2"));
if( gdjs.tirage_951Code.condition1IsTrue_1.val ) {
    gdjs.tirage_951Code.conditionTrue_1.val = true;
}
}
{
gdjs.tirage_951Code.condition2IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a0")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b3"));
if( gdjs.tirage_951Code.condition2IsTrue_1.val ) {
    gdjs.tirage_951Code.conditionTrue_1.val = true;
}
}
{
}
}
}if (gdjs.tirage_951Code.condition0IsTrue_0.val) {
{runtimeScene.getScene().getVariables().getFromIndex(0).sub(1);
}}

}


{


gdjs.tirage_951Code.condition0IsTrue_0.val = false;
gdjs.tirage_951Code.condition1IsTrue_0.val = false;
gdjs.tirage_951Code.condition2IsTrue_0.val = false;
{
gdjs.tirage_951Code.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a0")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b1"));
}if ( gdjs.tirage_951Code.condition0IsTrue_0.val ) {
{
gdjs.tirage_951Code.condition1IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a0")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b2"));
}if ( gdjs.tirage_951Code.condition1IsTrue_0.val ) {
{
gdjs.tirage_951Code.condition2IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a0")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b3"));
}}
}
if (gdjs.tirage_951Code.condition2IsTrue_0.val) {
{runtimeScene.getScene().getVariables().getFromIndex(0).add(1);
}{runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a1").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a0")));
}}

}


};gdjs.tirage_951Code.eventsList1 = function(runtimeScene) {

{


gdjs.tirage_951Code.condition0IsTrue_0.val = false;
{
{gdjs.tirage_951Code.conditionTrue_1 = gdjs.tirage_951Code.condition0IsTrue_0;
gdjs.tirage_951Code.condition0IsTrue_1.val = false;
gdjs.tirage_951Code.condition1IsTrue_1.val = false;
gdjs.tirage_951Code.condition2IsTrue_1.val = false;
gdjs.tirage_951Code.condition3IsTrue_1.val = false;
{
gdjs.tirage_951Code.condition0IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a1")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a0"));
if( gdjs.tirage_951Code.condition0IsTrue_1.val ) {
    gdjs.tirage_951Code.conditionTrue_1.val = true;
}
}
{
gdjs.tirage_951Code.condition1IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a0")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b1"));
if( gdjs.tirage_951Code.condition1IsTrue_1.val ) {
    gdjs.tirage_951Code.conditionTrue_1.val = true;
}
}
{
gdjs.tirage_951Code.condition2IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a0")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b2"));
if( gdjs.tirage_951Code.condition2IsTrue_1.val ) {
    gdjs.tirage_951Code.conditionTrue_1.val = true;
}
}
{
gdjs.tirage_951Code.condition3IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a0")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b3"));
if( gdjs.tirage_951Code.condition3IsTrue_1.val ) {
    gdjs.tirage_951Code.conditionTrue_1.val = true;
}
}
{
}
}
}if (gdjs.tirage_951Code.condition0IsTrue_0.val) {
{runtimeScene.getScene().getVariables().getFromIndex(0).sub(1);
}}

}


{


gdjs.tirage_951Code.condition0IsTrue_0.val = false;
gdjs.tirage_951Code.condition1IsTrue_0.val = false;
gdjs.tirage_951Code.condition2IsTrue_0.val = false;
gdjs.tirage_951Code.condition3IsTrue_0.val = false;
{
gdjs.tirage_951Code.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a1")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a0"));
}if ( gdjs.tirage_951Code.condition0IsTrue_0.val ) {
{
gdjs.tirage_951Code.condition1IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a0")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b1"));
}if ( gdjs.tirage_951Code.condition1IsTrue_0.val ) {
{
gdjs.tirage_951Code.condition2IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a0")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b2"));
}if ( gdjs.tirage_951Code.condition2IsTrue_0.val ) {
{
gdjs.tirage_951Code.condition3IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a0")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b3"));
}}
}
}
if (gdjs.tirage_951Code.condition3IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a2").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a0")));
}{runtimeScene.getScene().getVariables().getFromIndex(0).add(1);
}}

}


};gdjs.tirage_951Code.eventsList2 = function(runtimeScene) {

{


gdjs.tirage_951Code.condition0IsTrue_0.val = false;
{
{gdjs.tirage_951Code.conditionTrue_1 = gdjs.tirage_951Code.condition0IsTrue_0;
gdjs.tirage_951Code.condition0IsTrue_1.val = false;
gdjs.tirage_951Code.condition1IsTrue_1.val = false;
gdjs.tirage_951Code.condition2IsTrue_1.val = false;
gdjs.tirage_951Code.condition3IsTrue_1.val = false;
gdjs.tirage_951Code.condition4IsTrue_1.val = false;
{
gdjs.tirage_951Code.condition0IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a1")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a0"));
if( gdjs.tirage_951Code.condition0IsTrue_1.val ) {
    gdjs.tirage_951Code.conditionTrue_1.val = true;
}
}
{
gdjs.tirage_951Code.condition1IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a2")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a0"));
if( gdjs.tirage_951Code.condition1IsTrue_1.val ) {
    gdjs.tirage_951Code.conditionTrue_1.val = true;
}
}
{
gdjs.tirage_951Code.condition2IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a0")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b1"));
if( gdjs.tirage_951Code.condition2IsTrue_1.val ) {
    gdjs.tirage_951Code.conditionTrue_1.val = true;
}
}
{
gdjs.tirage_951Code.condition3IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a0")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b2"));
if( gdjs.tirage_951Code.condition3IsTrue_1.val ) {
    gdjs.tirage_951Code.conditionTrue_1.val = true;
}
}
{
gdjs.tirage_951Code.condition4IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a0")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b3"));
if( gdjs.tirage_951Code.condition4IsTrue_1.val ) {
    gdjs.tirage_951Code.conditionTrue_1.val = true;
}
}
{
}
}
}if (gdjs.tirage_951Code.condition0IsTrue_0.val) {
{runtimeScene.getScene().getVariables().getFromIndex(0).sub(1);
}}

}


{


gdjs.tirage_951Code.condition0IsTrue_0.val = false;
gdjs.tirage_951Code.condition1IsTrue_0.val = false;
gdjs.tirage_951Code.condition2IsTrue_0.val = false;
gdjs.tirage_951Code.condition3IsTrue_0.val = false;
gdjs.tirage_951Code.condition4IsTrue_0.val = false;
{
gdjs.tirage_951Code.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a1")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a0"));
}if ( gdjs.tirage_951Code.condition0IsTrue_0.val ) {
{
gdjs.tirage_951Code.condition1IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a2")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a0"));
}if ( gdjs.tirage_951Code.condition1IsTrue_0.val ) {
{
gdjs.tirage_951Code.condition2IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a0")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b1"));
}if ( gdjs.tirage_951Code.condition2IsTrue_0.val ) {
{
gdjs.tirage_951Code.condition3IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a0")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b2"));
}if ( gdjs.tirage_951Code.condition3IsTrue_0.val ) {
{
gdjs.tirage_951Code.condition4IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a0")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b3"));
}}
}
}
}
if (gdjs.tirage_951Code.condition4IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a3").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a0")));
}{runtimeScene.getScene().getVariables().getFromIndex(0).add(1);
}}

}


};gdjs.tirage_951Code.eventsList3 = function(runtimeScene) {

{


gdjs.tirage_951Code.condition0IsTrue_0.val = false;
{
gdjs.tirage_951Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.tirage_951Code.condition0IsTrue_0.val) {
{runtimeScene.getScene().getVariables().getFromIndex(0).setNumber(1);
}}

}


{


gdjs.tirage_951Code.condition0IsTrue_0.val = false;
{
{gdjs.tirage_951Code.conditionTrue_1 = gdjs.tirage_951Code.condition0IsTrue_0;
gdjs.tirage_951Code.condition0IsTrue_1.val = false;
gdjs.tirage_951Code.condition1IsTrue_1.val = false;
gdjs.tirage_951Code.condition2IsTrue_1.val = false;
{
gdjs.tirage_951Code.condition0IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 1;
if( gdjs.tirage_951Code.condition0IsTrue_1.val ) {
    gdjs.tirage_951Code.conditionTrue_1.val = true;
}
}
{
gdjs.tirage_951Code.condition1IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 3;
if( gdjs.tirage_951Code.condition1IsTrue_1.val ) {
    gdjs.tirage_951Code.conditionTrue_1.val = true;
}
}
{
gdjs.tirage_951Code.condition2IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 5;
if( gdjs.tirage_951Code.condition2IsTrue_1.val ) {
    gdjs.tirage_951Code.conditionTrue_1.val = true;
}
}
{
}
}
}if (gdjs.tirage_951Code.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a0").setNumber(gdjs.randomInRange(1, gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4))));
}{runtimeScene.getScene().getVariables().getFromIndex(0).add(1);
}}

}


{


gdjs.tirage_951Code.condition0IsTrue_0.val = false;
{
gdjs.tirage_951Code.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 2;
}if (gdjs.tirage_951Code.condition0IsTrue_0.val) {

{ //Subevents
gdjs.tirage_951Code.eventsList0(runtimeScene);} //End of subevents
}

}


{


gdjs.tirage_951Code.condition0IsTrue_0.val = false;
{
gdjs.tirage_951Code.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 4;
}if (gdjs.tirage_951Code.condition0IsTrue_0.val) {

{ //Subevents
gdjs.tirage_951Code.eventsList1(runtimeScene);} //End of subevents
}

}


{


gdjs.tirage_951Code.condition0IsTrue_0.val = false;
{
gdjs.tirage_951Code.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 6;
}if (gdjs.tirage_951Code.condition0IsTrue_0.val) {

{ //Subevents
gdjs.tirage_951Code.eventsList2(runtimeScene);} //End of subevents
}

}


{


gdjs.tirage_951Code.condition0IsTrue_0.val = false;
{
gdjs.tirage_951Code.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 7;
}if (gdjs.tirage_951Code.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "jeu", false);
}}

}


};

gdjs.tirage_951Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.tirage_951Code.GDlama_95inspecteurObjects1.length = 0;
gdjs.tirage_951Code.GDlama_95inspecteurObjects2.length = 0;
gdjs.tirage_951Code.GDlama_95inspecteurObjects3.length = 0;
gdjs.tirage_951Code.GDbouton_95retourObjects1.length = 0;
gdjs.tirage_951Code.GDbouton_95retourObjects2.length = 0;
gdjs.tirage_951Code.GDbouton_95retourObjects3.length = 0;
gdjs.tirage_951Code.GDscore_951Objects1.length = 0;
gdjs.tirage_951Code.GDscore_951Objects2.length = 0;
gdjs.tirage_951Code.GDscore_951Objects3.length = 0;

gdjs.tirage_951Code.eventsList3(runtimeScene);

return;

}

gdjs['tirage_951Code'] = gdjs.tirage_951Code;
