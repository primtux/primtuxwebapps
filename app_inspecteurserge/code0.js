gdjs.menuCode = {};
gdjs.menuCode.GDlama_95inspecteurObjects1= [];
gdjs.menuCode.GDlama_95inspecteurObjects2= [];
gdjs.menuCode.GDlama_95inspecteurObjects3= [];
gdjs.menuCode.GDbouton_95retourObjects1= [];
gdjs.menuCode.GDbouton_95retourObjects2= [];
gdjs.menuCode.GDbouton_95retourObjects3= [];
gdjs.menuCode.GDscore_951Objects1= [];
gdjs.menuCode.GDscore_951Objects2= [];
gdjs.menuCode.GDscore_951Objects3= [];
gdjs.menuCode.GDtableauObjects1= [];
gdjs.menuCode.GDtableauObjects2= [];
gdjs.menuCode.GDtableauObjects3= [];
gdjs.menuCode.GDphotoObjects1= [];
gdjs.menuCode.GDphotoObjects2= [];
gdjs.menuCode.GDphotoObjects3= [];
gdjs.menuCode.GDNewSpriteObjects1= [];
gdjs.menuCode.GDNewSpriteObjects2= [];
gdjs.menuCode.GDNewSpriteObjects3= [];
gdjs.menuCode.GDphoto2Objects1= [];
gdjs.menuCode.GDphoto2Objects2= [];
gdjs.menuCode.GDphoto2Objects3= [];
gdjs.menuCode.GDphoto3Objects1= [];
gdjs.menuCode.GDphoto3Objects2= [];
gdjs.menuCode.GDphoto3Objects3= [];
gdjs.menuCode.GDbouton_951Objects1= [];
gdjs.menuCode.GDbouton_951Objects2= [];
gdjs.menuCode.GDbouton_951Objects3= [];
gdjs.menuCode.GDbouton_951cObjects1= [];
gdjs.menuCode.GDbouton_951cObjects2= [];
gdjs.menuCode.GDbouton_951cObjects3= [];
gdjs.menuCode.GDbouton_951bObjects1= [];
gdjs.menuCode.GDbouton_951bObjects2= [];
gdjs.menuCode.GDbouton_951bObjects3= [];
gdjs.menuCode.GDbouton_952Objects1= [];
gdjs.menuCode.GDbouton_952Objects2= [];
gdjs.menuCode.GDbouton_952Objects3= [];
gdjs.menuCode.GDbouton_952cObjects1= [];
gdjs.menuCode.GDbouton_952cObjects2= [];
gdjs.menuCode.GDbouton_952cObjects3= [];
gdjs.menuCode.GDbouton_952bObjects1= [];
gdjs.menuCode.GDbouton_952bObjects2= [];
gdjs.menuCode.GDbouton_952bObjects3= [];
gdjs.menuCode.GDbouton_953Objects1= [];
gdjs.menuCode.GDbouton_953Objects2= [];
gdjs.menuCode.GDbouton_953Objects3= [];
gdjs.menuCode.GDbouton_953cObjects1= [];
gdjs.menuCode.GDbouton_953cObjects2= [];
gdjs.menuCode.GDbouton_953cObjects3= [];
gdjs.menuCode.GDbouton_953bObjects1= [];
gdjs.menuCode.GDbouton_953bObjects2= [];
gdjs.menuCode.GDbouton_953bObjects3= [];
gdjs.menuCode.GDscore_951bObjects1= [];
gdjs.menuCode.GDscore_951bObjects2= [];
gdjs.menuCode.GDscore_951bObjects3= [];
gdjs.menuCode.GDscore_951cObjects1= [];
gdjs.menuCode.GDscore_951cObjects2= [];
gdjs.menuCode.GDscore_951cObjects3= [];
gdjs.menuCode.GDscore_952Objects1= [];
gdjs.menuCode.GDscore_952Objects2= [];
gdjs.menuCode.GDscore_952Objects3= [];
gdjs.menuCode.GDscore_952cObjects1= [];
gdjs.menuCode.GDscore_952cObjects2= [];
gdjs.menuCode.GDscore_952cObjects3= [];
gdjs.menuCode.GDscore_952bObjects1= [];
gdjs.menuCode.GDscore_952bObjects2= [];
gdjs.menuCode.GDscore_952bObjects3= [];
gdjs.menuCode.GDscore_953Objects1= [];
gdjs.menuCode.GDscore_953Objects2= [];
gdjs.menuCode.GDscore_953Objects3= [];
gdjs.menuCode.GDscore_953cObjects1= [];
gdjs.menuCode.GDscore_953cObjects2= [];
gdjs.menuCode.GDscore_953cObjects3= [];
gdjs.menuCode.GDscore_953bObjects1= [];
gdjs.menuCode.GDscore_953bObjects2= [];
gdjs.menuCode.GDscore_953bObjects3= [];
gdjs.menuCode.GDlamaObjects1= [];
gdjs.menuCode.GDlamaObjects2= [];
gdjs.menuCode.GDlamaObjects3= [];
gdjs.menuCode.GDauteurObjects1= [];
gdjs.menuCode.GDauteurObjects2= [];
gdjs.menuCode.GDauteurObjects3= [];
gdjs.menuCode.GDversionObjects1= [];
gdjs.menuCode.GDversionObjects2= [];
gdjs.menuCode.GDversionObjects3= [];
gdjs.menuCode.GDfondObjects1= [];
gdjs.menuCode.GDfondObjects2= [];
gdjs.menuCode.GDfondObjects3= [];
gdjs.menuCode.GDtestObjects1= [];
gdjs.menuCode.GDtestObjects2= [];
gdjs.menuCode.GDtestObjects3= [];
gdjs.menuCode.GDbouton_95infosObjects1= [];
gdjs.menuCode.GDbouton_95infosObjects2= [];
gdjs.menuCode.GDbouton_95infosObjects3= [];

gdjs.menuCode.conditionTrue_0 = {val:false};
gdjs.menuCode.condition0IsTrue_0 = {val:false};
gdjs.menuCode.condition1IsTrue_0 = {val:false};
gdjs.menuCode.condition2IsTrue_0 = {val:false};
gdjs.menuCode.condition3IsTrue_0 = {val:false};
gdjs.menuCode.conditionTrue_1 = {val:false};
gdjs.menuCode.condition0IsTrue_1 = {val:false};
gdjs.menuCode.condition1IsTrue_1 = {val:false};
gdjs.menuCode.condition2IsTrue_1 = {val:false};
gdjs.menuCode.condition3IsTrue_1 = {val:false};


gdjs.menuCode.eventsList0 = function(runtimeScene) {

{


gdjs.menuCode.condition0IsTrue_0.val = false;
{
gdjs.menuCode.condition0IsTrue_0.val = gdjs.evtTools.storage.elementExistsInJSONFile("sauvegarde_inspecteurlama", "reglages");
}if (gdjs.menuCode.condition0IsTrue_0.val) {
{gdjs.evtTools.storage.readStringFromJSONFile("sauvegarde_inspecteurlama", "reglages", runtimeScene, runtimeScene.getScene().getVariables().get("sauvegarde"));
}{gdjs.evtTools.network.jsonToVariableStructure(gdjs.evtTools.variable.getVariableString(runtimeScene.getScene().getVariables().get("sauvegarde")), runtimeScene.getGame().getVariables().getFromIndex(2));
}}

}


};gdjs.menuCode.eventsList1 = function(runtimeScene) {

{


gdjs.menuCode.eventsList0(runtimeScene);
}


{


{
{gdjs.evtTools.variable.setVariableBoolean(runtimeScene.getScene().getVariables().getFromIndex(0), true);
}}

}


};gdjs.menuCode.eventsList2 = function(runtimeScene) {

{


{
{gdjs.evtTools.variable.variableClearChildren(runtimeScene.getGame().getVariables().getFromIndex(2).getChild("score"));
}}

}


{


{
{gdjs.evtTools.storage.clearJSONFile("sauvegarde_inspecteurlama");
}{gdjs.evtTools.storage.writeStringInJSONFile("sauvegarde_inspecteurlama", "reglages", gdjs.evtTools.network.variableStructureToJSON(runtimeScene.getGame().getVariables().getFromIndex(2)));
}}

}


{


{
{gdjs.evtTools.variable.setVariableBoolean(runtimeScene.getScene().getVariables().getFromIndex(0), true);
}}

}


};gdjs.menuCode.eventsList3 = function(runtimeScene) {

{


gdjs.menuCode.condition0IsTrue_0.val = false;
{
gdjs.menuCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableBoolean(runtimeScene.getScene().getVariables().getFromIndex(0), true);
}if (gdjs.menuCode.condition0IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("score_1"), gdjs.menuCode.GDscore_951Objects2);
gdjs.copyArray(runtimeScene.getObjects("score_1b"), gdjs.menuCode.GDscore_951bObjects2);
gdjs.copyArray(runtimeScene.getObjects("score_1c"), gdjs.menuCode.GDscore_951cObjects2);
gdjs.copyArray(runtimeScene.getObjects("score_2"), gdjs.menuCode.GDscore_952Objects2);
gdjs.copyArray(runtimeScene.getObjects("score_2b"), gdjs.menuCode.GDscore_952bObjects2);
gdjs.copyArray(runtimeScene.getObjects("score_2c"), gdjs.menuCode.GDscore_952cObjects2);
gdjs.copyArray(runtimeScene.getObjects("score_3"), gdjs.menuCode.GDscore_953Objects2);
gdjs.copyArray(runtimeScene.getObjects("score_3b"), gdjs.menuCode.GDscore_953bObjects2);
gdjs.copyArray(runtimeScene.getObjects("score_3c"), gdjs.menuCode.GDscore_953cObjects2);
{for(var i = 0, len = gdjs.menuCode.GDscore_951Objects2.length ;i < len;++i) {
    gdjs.menuCode.GDscore_951Objects2[i].setAnimationFrame(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2).getChild("score").getChild("1a")));
}
}{for(var i = 0, len = gdjs.menuCode.GDscore_952Objects2.length ;i < len;++i) {
    gdjs.menuCode.GDscore_952Objects2[i].setAnimationFrame(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2).getChild("score").getChild("1b")));
}
}{for(var i = 0, len = gdjs.menuCode.GDscore_953Objects2.length ;i < len;++i) {
    gdjs.menuCode.GDscore_953Objects2[i].setAnimationFrame(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2).getChild("score").getChild("1c")));
}
}{for(var i = 0, len = gdjs.menuCode.GDscore_951bObjects2.length ;i < len;++i) {
    gdjs.menuCode.GDscore_951bObjects2[i].setAnimationFrame(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2).getChild("score").getChild("2a")));
}
}{for(var i = 0, len = gdjs.menuCode.GDscore_952bObjects2.length ;i < len;++i) {
    gdjs.menuCode.GDscore_952bObjects2[i].setAnimationFrame(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2).getChild("score").getChild("2b")));
}
}{for(var i = 0, len = gdjs.menuCode.GDscore_953bObjects2.length ;i < len;++i) {
    gdjs.menuCode.GDscore_953bObjects2[i].setAnimationFrame(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2).getChild("score").getChild("2c")));
}
}{for(var i = 0, len = gdjs.menuCode.GDscore_951cObjects2.length ;i < len;++i) {
    gdjs.menuCode.GDscore_951cObjects2[i].setAnimationFrame(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2).getChild("score").getChild("3a")));
}
}{for(var i = 0, len = gdjs.menuCode.GDscore_952cObjects2.length ;i < len;++i) {
    gdjs.menuCode.GDscore_952cObjects2[i].setAnimationFrame(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2).getChild("score").getChild("3b")));
}
}{for(var i = 0, len = gdjs.menuCode.GDscore_953cObjects2.length ;i < len;++i) {
    gdjs.menuCode.GDscore_953cObjects2[i].setAnimationFrame(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2).getChild("score").getChild("3c")));
}
}}

}


{


gdjs.menuCode.condition0IsTrue_0.val = false;
gdjs.menuCode.condition1IsTrue_0.val = false;
{
gdjs.menuCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 5;
}if ( gdjs.menuCode.condition0IsTrue_0.val ) {
{
{gdjs.menuCode.conditionTrue_1 = gdjs.menuCode.condition1IsTrue_0;
gdjs.menuCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(10849100);
}
}}
if (gdjs.menuCode.condition1IsTrue_0.val) {

{ //Subevents
gdjs.menuCode.eventsList2(runtimeScene);} //End of subevents
}

}


};gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDbouton_95951Objects2Objects = Hashtable.newFrom({"bouton_1": gdjs.menuCode.GDbouton_951Objects2});
gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDbouton_95952Objects2Objects = Hashtable.newFrom({"bouton_2": gdjs.menuCode.GDbouton_952Objects2});
gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDbouton_95953Objects1Objects = Hashtable.newFrom({"bouton_3": gdjs.menuCode.GDbouton_953Objects1});
gdjs.menuCode.eventsList4 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("bouton_1"), gdjs.menuCode.GDbouton_951Objects2);

gdjs.menuCode.condition0IsTrue_0.val = false;
gdjs.menuCode.condition1IsTrue_0.val = false;
{
gdjs.menuCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDbouton_95951Objects2Objects, runtimeScene, true, false);
}if ( gdjs.menuCode.condition0IsTrue_0.val ) {
{
gdjs.menuCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}}
if (gdjs.menuCode.condition1IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(3).setNumber(0);
}{runtimeScene.getGame().getVariables().getFromIndex(7).setNumber(1);
}{runtimeScene.getGame().getVariables().getFromIndex(6).setNumber(1);
}{runtimeScene.getGame().getVariables().getFromIndex(8).setNumber(0);
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "tirage_1", false);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_2"), gdjs.menuCode.GDbouton_952Objects2);

gdjs.menuCode.condition0IsTrue_0.val = false;
gdjs.menuCode.condition1IsTrue_0.val = false;
{
gdjs.menuCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDbouton_95952Objects2Objects, runtimeScene, true, false);
}if ( gdjs.menuCode.condition0IsTrue_0.val ) {
{
gdjs.menuCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}}
if (gdjs.menuCode.condition1IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(3).setNumber(0);
}{runtimeScene.getGame().getVariables().getFromIndex(7).setNumber(1);
}{runtimeScene.getGame().getVariables().getFromIndex(6).setNumber(2);
}{runtimeScene.getGame().getVariables().getFromIndex(8).setNumber(0);
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "tirage_1", false);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_3"), gdjs.menuCode.GDbouton_953Objects1);

gdjs.menuCode.condition0IsTrue_0.val = false;
gdjs.menuCode.condition1IsTrue_0.val = false;
{
gdjs.menuCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDbouton_95953Objects1Objects, runtimeScene, true, false);
}if ( gdjs.menuCode.condition0IsTrue_0.val ) {
{
gdjs.menuCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}}
if (gdjs.menuCode.condition1IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(3).setNumber(0);
}{runtimeScene.getGame().getVariables().getFromIndex(7).setNumber(1);
}{runtimeScene.getGame().getVariables().getFromIndex(6).setNumber(3);
}{runtimeScene.getGame().getVariables().getFromIndex(8).setNumber(0);
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "tirage_1", false);
}}

}


};gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDbouton_95951bObjects2Objects = Hashtable.newFrom({"bouton_1b": gdjs.menuCode.GDbouton_951bObjects2});
gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDbouton_95952bObjects2Objects = Hashtable.newFrom({"bouton_2b": gdjs.menuCode.GDbouton_952bObjects2});
gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDbouton_95953bObjects1Objects = Hashtable.newFrom({"bouton_3b": gdjs.menuCode.GDbouton_953bObjects1});
gdjs.menuCode.eventsList5 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("bouton_1b"), gdjs.menuCode.GDbouton_951bObjects2);

gdjs.menuCode.condition0IsTrue_0.val = false;
gdjs.menuCode.condition1IsTrue_0.val = false;
{
gdjs.menuCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDbouton_95951bObjects2Objects, runtimeScene, true, false);
}if ( gdjs.menuCode.condition0IsTrue_0.val ) {
{
gdjs.menuCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}}
if (gdjs.menuCode.condition1IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(3).setNumber(0);
}{runtimeScene.getGame().getVariables().getFromIndex(7).setNumber(2);
}{runtimeScene.getGame().getVariables().getFromIndex(6).setNumber(1);
}{runtimeScene.getGame().getVariables().getFromIndex(8).setNumber(0);
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "tirage_1", false);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_2b"), gdjs.menuCode.GDbouton_952bObjects2);

gdjs.menuCode.condition0IsTrue_0.val = false;
gdjs.menuCode.condition1IsTrue_0.val = false;
{
gdjs.menuCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDbouton_95952bObjects2Objects, runtimeScene, true, false);
}if ( gdjs.menuCode.condition0IsTrue_0.val ) {
{
gdjs.menuCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}}
if (gdjs.menuCode.condition1IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(3).setNumber(0);
}{runtimeScene.getGame().getVariables().getFromIndex(7).setNumber(2);
}{runtimeScene.getGame().getVariables().getFromIndex(6).setNumber(2);
}{runtimeScene.getGame().getVariables().getFromIndex(8).setNumber(0);
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "tirage_1", false);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_3b"), gdjs.menuCode.GDbouton_953bObjects1);

gdjs.menuCode.condition0IsTrue_0.val = false;
gdjs.menuCode.condition1IsTrue_0.val = false;
{
gdjs.menuCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDbouton_95953bObjects1Objects, runtimeScene, true, false);
}if ( gdjs.menuCode.condition0IsTrue_0.val ) {
{
gdjs.menuCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}}
if (gdjs.menuCode.condition1IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(3).setNumber(0);
}{runtimeScene.getGame().getVariables().getFromIndex(7).setNumber(2);
}{runtimeScene.getGame().getVariables().getFromIndex(6).setNumber(3);
}{runtimeScene.getGame().getVariables().getFromIndex(8).setNumber(0);
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "tirage_1", false);
}}

}


};gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDbouton_95951cObjects2Objects = Hashtable.newFrom({"bouton_1c": gdjs.menuCode.GDbouton_951cObjects2});
gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDbouton_95952cObjects2Objects = Hashtable.newFrom({"bouton_2c": gdjs.menuCode.GDbouton_952cObjects2});
gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDbouton_95953cObjects1Objects = Hashtable.newFrom({"bouton_3c": gdjs.menuCode.GDbouton_953cObjects1});
gdjs.menuCode.eventsList6 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("bouton_1c"), gdjs.menuCode.GDbouton_951cObjects2);

gdjs.menuCode.condition0IsTrue_0.val = false;
gdjs.menuCode.condition1IsTrue_0.val = false;
{
gdjs.menuCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDbouton_95951cObjects2Objects, runtimeScene, true, false);
}if ( gdjs.menuCode.condition0IsTrue_0.val ) {
{
gdjs.menuCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}}
if (gdjs.menuCode.condition1IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(3).setNumber(0);
}{runtimeScene.getGame().getVariables().getFromIndex(7).setNumber(3);
}{runtimeScene.getGame().getVariables().getFromIndex(6).setNumber(1);
}{runtimeScene.getGame().getVariables().getFromIndex(8).setNumber(0);
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "tirage_1", false);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_2c"), gdjs.menuCode.GDbouton_952cObjects2);

gdjs.menuCode.condition0IsTrue_0.val = false;
gdjs.menuCode.condition1IsTrue_0.val = false;
{
gdjs.menuCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDbouton_95952cObjects2Objects, runtimeScene, true, false);
}if ( gdjs.menuCode.condition0IsTrue_0.val ) {
{
gdjs.menuCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}}
if (gdjs.menuCode.condition1IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(3).setNumber(0);
}{runtimeScene.getGame().getVariables().getFromIndex(7).setNumber(3);
}{runtimeScene.getGame().getVariables().getFromIndex(6).setNumber(2);
}{runtimeScene.getGame().getVariables().getFromIndex(8).setNumber(0);
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "tirage_1", false);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_3c"), gdjs.menuCode.GDbouton_953cObjects1);

gdjs.menuCode.condition0IsTrue_0.val = false;
gdjs.menuCode.condition1IsTrue_0.val = false;
{
gdjs.menuCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDbouton_95953cObjects1Objects, runtimeScene, true, false);
}if ( gdjs.menuCode.condition0IsTrue_0.val ) {
{
gdjs.menuCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}}
if (gdjs.menuCode.condition1IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(3).setNumber(0);
}{runtimeScene.getGame().getVariables().getFromIndex(7).setNumber(3);
}{runtimeScene.getGame().getVariables().getFromIndex(6).setNumber(3);
}{runtimeScene.getGame().getVariables().getFromIndex(8).setNumber(0);
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "tirage_1", false);
}}

}


};gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDlamaObjects1Objects = Hashtable.newFrom({"lama": gdjs.menuCode.GDlamaObjects1});
gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDbouton_9595infosObjects1Objects = Hashtable.newFrom({"bouton_infos": gdjs.menuCode.GDbouton_95infosObjects1});
gdjs.menuCode.eventsList7 = function(runtimeScene) {

{


gdjs.menuCode.condition0IsTrue_0.val = false;
{
gdjs.menuCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.menuCode.condition0IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("score_1"), gdjs.menuCode.GDscore_951Objects1);
gdjs.copyArray(runtimeScene.getObjects("score_1b"), gdjs.menuCode.GDscore_951bObjects1);
gdjs.copyArray(runtimeScene.getObjects("score_1c"), gdjs.menuCode.GDscore_951cObjects1);
gdjs.copyArray(runtimeScene.getObjects("score_2"), gdjs.menuCode.GDscore_952Objects1);
gdjs.copyArray(runtimeScene.getObjects("score_2b"), gdjs.menuCode.GDscore_952bObjects1);
gdjs.copyArray(runtimeScene.getObjects("score_2c"), gdjs.menuCode.GDscore_952cObjects1);
gdjs.copyArray(runtimeScene.getObjects("score_3"), gdjs.menuCode.GDscore_953Objects1);
gdjs.copyArray(runtimeScene.getObjects("score_3b"), gdjs.menuCode.GDscore_953bObjects1);
gdjs.copyArray(runtimeScene.getObjects("score_3c"), gdjs.menuCode.GDscore_953cObjects1);
{for(var i = 0, len = gdjs.menuCode.GDscore_951bObjects1.length ;i < len;++i) {
    gdjs.menuCode.GDscore_951bObjects1[i].pauseAnimation();
}
for(var i = 0, len = gdjs.menuCode.GDscore_951cObjects1.length ;i < len;++i) {
    gdjs.menuCode.GDscore_951cObjects1[i].pauseAnimation();
}
for(var i = 0, len = gdjs.menuCode.GDscore_952Objects1.length ;i < len;++i) {
    gdjs.menuCode.GDscore_952Objects1[i].pauseAnimation();
}
for(var i = 0, len = gdjs.menuCode.GDscore_952cObjects1.length ;i < len;++i) {
    gdjs.menuCode.GDscore_952cObjects1[i].pauseAnimation();
}
for(var i = 0, len = gdjs.menuCode.GDscore_952bObjects1.length ;i < len;++i) {
    gdjs.menuCode.GDscore_952bObjects1[i].pauseAnimation();
}
for(var i = 0, len = gdjs.menuCode.GDscore_953Objects1.length ;i < len;++i) {
    gdjs.menuCode.GDscore_953Objects1[i].pauseAnimation();
}
for(var i = 0, len = gdjs.menuCode.GDscore_953cObjects1.length ;i < len;++i) {
    gdjs.menuCode.GDscore_953cObjects1[i].pauseAnimation();
}
for(var i = 0, len = gdjs.menuCode.GDscore_953bObjects1.length ;i < len;++i) {
    gdjs.menuCode.GDscore_953bObjects1[i].pauseAnimation();
}
for(var i = 0, len = gdjs.menuCode.GDscore_951Objects1.length ;i < len;++i) {
    gdjs.menuCode.GDscore_951Objects1[i].pauseAnimation();
}
}{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "chrono");
}
{ //Subevents
gdjs.menuCode.eventsList1(runtimeScene);} //End of subevents
}

}


{


gdjs.menuCode.eventsList3(runtimeScene);
}


{


gdjs.menuCode.eventsList4(runtimeScene);
}


{


gdjs.menuCode.eventsList5(runtimeScene);
}


{


gdjs.menuCode.eventsList6(runtimeScene);
}


{

gdjs.copyArray(runtimeScene.getObjects("lama"), gdjs.menuCode.GDlamaObjects1);

gdjs.menuCode.condition0IsTrue_0.val = false;
gdjs.menuCode.condition1IsTrue_0.val = false;
gdjs.menuCode.condition2IsTrue_0.val = false;
{
gdjs.menuCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDlamaObjects1Objects, runtimeScene, true, false);
}if ( gdjs.menuCode.condition0IsTrue_0.val ) {
{
gdjs.menuCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.menuCode.condition1IsTrue_0.val ) {
{
{gdjs.menuCode.conditionTrue_1 = gdjs.menuCode.condition2IsTrue_0;
gdjs.menuCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(10866204);
}
}}
}
if (gdjs.menuCode.condition2IsTrue_0.val) {
{runtimeScene.getScene().getVariables().getFromIndex(1).add(1);
}{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/bulle_1.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_infos"), gdjs.menuCode.GDbouton_95infosObjects1);

gdjs.menuCode.condition0IsTrue_0.val = false;
gdjs.menuCode.condition1IsTrue_0.val = false;
gdjs.menuCode.condition2IsTrue_0.val = false;
{
gdjs.menuCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDbouton_9595infosObjects1Objects, runtimeScene, true, false);
}if ( gdjs.menuCode.condition0IsTrue_0.val ) {
{
gdjs.menuCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.menuCode.condition1IsTrue_0.val ) {
{
gdjs.menuCode.condition2IsTrue_0.val = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "chrono") > 0.5;
}}
}
if (gdjs.menuCode.condition2IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "infos", false);
}}

}


};

gdjs.menuCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.menuCode.GDlama_95inspecteurObjects1.length = 0;
gdjs.menuCode.GDlama_95inspecteurObjects2.length = 0;
gdjs.menuCode.GDlama_95inspecteurObjects3.length = 0;
gdjs.menuCode.GDbouton_95retourObjects1.length = 0;
gdjs.menuCode.GDbouton_95retourObjects2.length = 0;
gdjs.menuCode.GDbouton_95retourObjects3.length = 0;
gdjs.menuCode.GDscore_951Objects1.length = 0;
gdjs.menuCode.GDscore_951Objects2.length = 0;
gdjs.menuCode.GDscore_951Objects3.length = 0;
gdjs.menuCode.GDtableauObjects1.length = 0;
gdjs.menuCode.GDtableauObjects2.length = 0;
gdjs.menuCode.GDtableauObjects3.length = 0;
gdjs.menuCode.GDphotoObjects1.length = 0;
gdjs.menuCode.GDphotoObjects2.length = 0;
gdjs.menuCode.GDphotoObjects3.length = 0;
gdjs.menuCode.GDNewSpriteObjects1.length = 0;
gdjs.menuCode.GDNewSpriteObjects2.length = 0;
gdjs.menuCode.GDNewSpriteObjects3.length = 0;
gdjs.menuCode.GDphoto2Objects1.length = 0;
gdjs.menuCode.GDphoto2Objects2.length = 0;
gdjs.menuCode.GDphoto2Objects3.length = 0;
gdjs.menuCode.GDphoto3Objects1.length = 0;
gdjs.menuCode.GDphoto3Objects2.length = 0;
gdjs.menuCode.GDphoto3Objects3.length = 0;
gdjs.menuCode.GDbouton_951Objects1.length = 0;
gdjs.menuCode.GDbouton_951Objects2.length = 0;
gdjs.menuCode.GDbouton_951Objects3.length = 0;
gdjs.menuCode.GDbouton_951cObjects1.length = 0;
gdjs.menuCode.GDbouton_951cObjects2.length = 0;
gdjs.menuCode.GDbouton_951cObjects3.length = 0;
gdjs.menuCode.GDbouton_951bObjects1.length = 0;
gdjs.menuCode.GDbouton_951bObjects2.length = 0;
gdjs.menuCode.GDbouton_951bObjects3.length = 0;
gdjs.menuCode.GDbouton_952Objects1.length = 0;
gdjs.menuCode.GDbouton_952Objects2.length = 0;
gdjs.menuCode.GDbouton_952Objects3.length = 0;
gdjs.menuCode.GDbouton_952cObjects1.length = 0;
gdjs.menuCode.GDbouton_952cObjects2.length = 0;
gdjs.menuCode.GDbouton_952cObjects3.length = 0;
gdjs.menuCode.GDbouton_952bObjects1.length = 0;
gdjs.menuCode.GDbouton_952bObjects2.length = 0;
gdjs.menuCode.GDbouton_952bObjects3.length = 0;
gdjs.menuCode.GDbouton_953Objects1.length = 0;
gdjs.menuCode.GDbouton_953Objects2.length = 0;
gdjs.menuCode.GDbouton_953Objects3.length = 0;
gdjs.menuCode.GDbouton_953cObjects1.length = 0;
gdjs.menuCode.GDbouton_953cObjects2.length = 0;
gdjs.menuCode.GDbouton_953cObjects3.length = 0;
gdjs.menuCode.GDbouton_953bObjects1.length = 0;
gdjs.menuCode.GDbouton_953bObjects2.length = 0;
gdjs.menuCode.GDbouton_953bObjects3.length = 0;
gdjs.menuCode.GDscore_951bObjects1.length = 0;
gdjs.menuCode.GDscore_951bObjects2.length = 0;
gdjs.menuCode.GDscore_951bObjects3.length = 0;
gdjs.menuCode.GDscore_951cObjects1.length = 0;
gdjs.menuCode.GDscore_951cObjects2.length = 0;
gdjs.menuCode.GDscore_951cObjects3.length = 0;
gdjs.menuCode.GDscore_952Objects1.length = 0;
gdjs.menuCode.GDscore_952Objects2.length = 0;
gdjs.menuCode.GDscore_952Objects3.length = 0;
gdjs.menuCode.GDscore_952cObjects1.length = 0;
gdjs.menuCode.GDscore_952cObjects2.length = 0;
gdjs.menuCode.GDscore_952cObjects3.length = 0;
gdjs.menuCode.GDscore_952bObjects1.length = 0;
gdjs.menuCode.GDscore_952bObjects2.length = 0;
gdjs.menuCode.GDscore_952bObjects3.length = 0;
gdjs.menuCode.GDscore_953Objects1.length = 0;
gdjs.menuCode.GDscore_953Objects2.length = 0;
gdjs.menuCode.GDscore_953Objects3.length = 0;
gdjs.menuCode.GDscore_953cObjects1.length = 0;
gdjs.menuCode.GDscore_953cObjects2.length = 0;
gdjs.menuCode.GDscore_953cObjects3.length = 0;
gdjs.menuCode.GDscore_953bObjects1.length = 0;
gdjs.menuCode.GDscore_953bObjects2.length = 0;
gdjs.menuCode.GDscore_953bObjects3.length = 0;
gdjs.menuCode.GDlamaObjects1.length = 0;
gdjs.menuCode.GDlamaObjects2.length = 0;
gdjs.menuCode.GDlamaObjects3.length = 0;
gdjs.menuCode.GDauteurObjects1.length = 0;
gdjs.menuCode.GDauteurObjects2.length = 0;
gdjs.menuCode.GDauteurObjects3.length = 0;
gdjs.menuCode.GDversionObjects1.length = 0;
gdjs.menuCode.GDversionObjects2.length = 0;
gdjs.menuCode.GDversionObjects3.length = 0;
gdjs.menuCode.GDfondObjects1.length = 0;
gdjs.menuCode.GDfondObjects2.length = 0;
gdjs.menuCode.GDfondObjects3.length = 0;
gdjs.menuCode.GDtestObjects1.length = 0;
gdjs.menuCode.GDtestObjects2.length = 0;
gdjs.menuCode.GDtestObjects3.length = 0;
gdjs.menuCode.GDbouton_95infosObjects1.length = 0;
gdjs.menuCode.GDbouton_95infosObjects2.length = 0;
gdjs.menuCode.GDbouton_95infosObjects3.length = 0;

gdjs.menuCode.eventsList7(runtimeScene);

return;

}

gdjs['menuCode'] = gdjs.menuCode;
