gdjs.infosCode = {};
gdjs.infosCode.GDscore2Objects1= [];
gdjs.infosCode.GDscore2Objects2= [];
gdjs.infosCode.GDscore1Objects1= [];
gdjs.infosCode.GDscore1Objects2= [];
gdjs.infosCode.GDdoigtObjects1= [];
gdjs.infosCode.GDdoigtObjects2= [];
gdjs.infosCode.GDdoigt2Objects1= [];
gdjs.infosCode.GDdoigt2Objects2= [];
gdjs.infosCode.GDquestion_95nbObjects1= [];
gdjs.infosCode.GDquestion_95nbObjects2= [];
gdjs.infosCode.GDtrainObjects1= [];
gdjs.infosCode.GDtrainObjects2= [];
gdjs.infosCode.GDtrain2Objects1= [];
gdjs.infosCode.GDtrain2Objects2= [];
gdjs.infosCode.GDajout_95centaineObjects1= [];
gdjs.infosCode.GDajout_95centaineObjects2= [];
gdjs.infosCode.GDajout_95dizaineObjects1= [];
gdjs.infosCode.GDajout_95dizaineObjects2= [];
gdjs.infosCode.GDajout_95uniteObjects1= [];
gdjs.infosCode.GDajout_95uniteObjects2= [];
gdjs.infosCode.GDretrait_95centaineObjects1= [];
gdjs.infosCode.GDretrait_95centaineObjects2= [];
gdjs.infosCode.GDretrait_95dizaineObjects1= [];
gdjs.infosCode.GDretrait_95dizaineObjects2= [];
gdjs.infosCode.GDretrait_95uniteObjects1= [];
gdjs.infosCode.GDretrait_95uniteObjects2= [];
gdjs.infosCode.GDnb_95unitesObjects1= [];
gdjs.infosCode.GDnb_95unitesObjects2= [];
gdjs.infosCode.GDnb_95centainesObjects1= [];
gdjs.infosCode.GDnb_95centainesObjects2= [];
gdjs.infosCode.GDnb_95dizainesObjects1= [];
gdjs.infosCode.GDnb_95dizainesObjects2= [];
gdjs.infosCode.GDaxe_95fleche_95reponseObjects1= [];
gdjs.infosCode.GDaxe_95fleche_95reponseObjects2= [];
gdjs.infosCode.GDbouton_95retourObjects1= [];
gdjs.infosCode.GDbouton_95retourObjects2= [];
gdjs.infosCode.GDmaison2Objects1= [];
gdjs.infosCode.GDmaison2Objects2= [];
gdjs.infosCode.GDmaison3Objects1= [];
gdjs.infosCode.GDmaison3Objects2= [];
gdjs.infosCode.GDmaison4Objects1= [];
gdjs.infosCode.GDmaison4Objects2= [];
gdjs.infosCode.GDmaison5Objects1= [];
gdjs.infosCode.GDmaison5Objects2= [];
gdjs.infosCode.GDmaison1Objects1= [];
gdjs.infosCode.GDmaison1Objects2= [];
gdjs.infosCode.GDrouteObjects1= [];
gdjs.infosCode.GDrouteObjects2= [];
gdjs.infosCode.GDville1Objects1= [];
gdjs.infosCode.GDville1Objects2= [];
gdjs.infosCode.GDfond_95blancObjects1= [];
gdjs.infosCode.GDfond_95blancObjects2= [];
gdjs.infosCode.GDinfosObjects1= [];
gdjs.infosCode.GDinfosObjects2= [];

gdjs.infosCode.conditionTrue_0 = {val:false};
gdjs.infosCode.condition0IsTrue_0 = {val:false};
gdjs.infosCode.condition1IsTrue_0 = {val:false};
gdjs.infosCode.condition2IsTrue_0 = {val:false};


gdjs.infosCode.mapOfGDgdjs_46infosCode_46GDbouton_9595retourObjects1Objects = Hashtable.newFrom({"bouton_retour": gdjs.infosCode.GDbouton_95retourObjects1});
gdjs.infosCode.eventsList0 = function(runtimeScene) {

{


gdjs.infosCode.condition0IsTrue_0.val = false;
{
gdjs.infosCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.infosCode.condition0IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("fond_blanc"), gdjs.infosCode.GDfond_95blancObjects1);
{for(var i = 0, len = gdjs.infosCode.GDfond_95blancObjects1.length ;i < len;++i) {
    gdjs.infosCode.GDfond_95blancObjects1[i].setOpacity(200);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_retour"), gdjs.infosCode.GDbouton_95retourObjects1);

gdjs.infosCode.condition0IsTrue_0.val = false;
gdjs.infosCode.condition1IsTrue_0.val = false;
{
gdjs.infosCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.infosCode.mapOfGDgdjs_46infosCode_46GDbouton_9595retourObjects1Objects, runtimeScene, true, false);
}if ( gdjs.infosCode.condition0IsTrue_0.val ) {
{
gdjs.infosCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}}
if (gdjs.infosCode.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "menu", false);
}}

}


{


{
}

}


};

gdjs.infosCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.infosCode.GDscore2Objects1.length = 0;
gdjs.infosCode.GDscore2Objects2.length = 0;
gdjs.infosCode.GDscore1Objects1.length = 0;
gdjs.infosCode.GDscore1Objects2.length = 0;
gdjs.infosCode.GDdoigtObjects1.length = 0;
gdjs.infosCode.GDdoigtObjects2.length = 0;
gdjs.infosCode.GDdoigt2Objects1.length = 0;
gdjs.infosCode.GDdoigt2Objects2.length = 0;
gdjs.infosCode.GDquestion_95nbObjects1.length = 0;
gdjs.infosCode.GDquestion_95nbObjects2.length = 0;
gdjs.infosCode.GDtrainObjects1.length = 0;
gdjs.infosCode.GDtrainObjects2.length = 0;
gdjs.infosCode.GDtrain2Objects1.length = 0;
gdjs.infosCode.GDtrain2Objects2.length = 0;
gdjs.infosCode.GDajout_95centaineObjects1.length = 0;
gdjs.infosCode.GDajout_95centaineObjects2.length = 0;
gdjs.infosCode.GDajout_95dizaineObjects1.length = 0;
gdjs.infosCode.GDajout_95dizaineObjects2.length = 0;
gdjs.infosCode.GDajout_95uniteObjects1.length = 0;
gdjs.infosCode.GDajout_95uniteObjects2.length = 0;
gdjs.infosCode.GDretrait_95centaineObjects1.length = 0;
gdjs.infosCode.GDretrait_95centaineObjects2.length = 0;
gdjs.infosCode.GDretrait_95dizaineObjects1.length = 0;
gdjs.infosCode.GDretrait_95dizaineObjects2.length = 0;
gdjs.infosCode.GDretrait_95uniteObjects1.length = 0;
gdjs.infosCode.GDretrait_95uniteObjects2.length = 0;
gdjs.infosCode.GDnb_95unitesObjects1.length = 0;
gdjs.infosCode.GDnb_95unitesObjects2.length = 0;
gdjs.infosCode.GDnb_95centainesObjects1.length = 0;
gdjs.infosCode.GDnb_95centainesObjects2.length = 0;
gdjs.infosCode.GDnb_95dizainesObjects1.length = 0;
gdjs.infosCode.GDnb_95dizainesObjects2.length = 0;
gdjs.infosCode.GDaxe_95fleche_95reponseObjects1.length = 0;
gdjs.infosCode.GDaxe_95fleche_95reponseObjects2.length = 0;
gdjs.infosCode.GDbouton_95retourObjects1.length = 0;
gdjs.infosCode.GDbouton_95retourObjects2.length = 0;
gdjs.infosCode.GDmaison2Objects1.length = 0;
gdjs.infosCode.GDmaison2Objects2.length = 0;
gdjs.infosCode.GDmaison3Objects1.length = 0;
gdjs.infosCode.GDmaison3Objects2.length = 0;
gdjs.infosCode.GDmaison4Objects1.length = 0;
gdjs.infosCode.GDmaison4Objects2.length = 0;
gdjs.infosCode.GDmaison5Objects1.length = 0;
gdjs.infosCode.GDmaison5Objects2.length = 0;
gdjs.infosCode.GDmaison1Objects1.length = 0;
gdjs.infosCode.GDmaison1Objects2.length = 0;
gdjs.infosCode.GDrouteObjects1.length = 0;
gdjs.infosCode.GDrouteObjects2.length = 0;
gdjs.infosCode.GDville1Objects1.length = 0;
gdjs.infosCode.GDville1Objects2.length = 0;
gdjs.infosCode.GDfond_95blancObjects1.length = 0;
gdjs.infosCode.GDfond_95blancObjects2.length = 0;
gdjs.infosCode.GDinfosObjects1.length = 0;
gdjs.infosCode.GDinfosObjects2.length = 0;

gdjs.infosCode.eventsList0(runtimeScene);
return;

}

gdjs['infosCode'] = gdjs.infosCode;
