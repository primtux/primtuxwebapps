gdjs.optionsCode = {};
gdjs.optionsCode.GDscore2Objects1= [];
gdjs.optionsCode.GDscore2Objects2= [];
gdjs.optionsCode.GDscore1Objects1= [];
gdjs.optionsCode.GDscore1Objects2= [];
gdjs.optionsCode.GDdoigtObjects1= [];
gdjs.optionsCode.GDdoigtObjects2= [];
gdjs.optionsCode.GDdoigt2Objects1= [];
gdjs.optionsCode.GDdoigt2Objects2= [];
gdjs.optionsCode.GDquestion_95nbObjects1= [];
gdjs.optionsCode.GDquestion_95nbObjects2= [];
gdjs.optionsCode.GDtrainObjects1= [];
gdjs.optionsCode.GDtrainObjects2= [];
gdjs.optionsCode.GDtrain2Objects1= [];
gdjs.optionsCode.GDtrain2Objects2= [];
gdjs.optionsCode.GDajout_95centaineObjects1= [];
gdjs.optionsCode.GDajout_95centaineObjects2= [];
gdjs.optionsCode.GDajout_95dizaineObjects1= [];
gdjs.optionsCode.GDajout_95dizaineObjects2= [];
gdjs.optionsCode.GDajout_95uniteObjects1= [];
gdjs.optionsCode.GDajout_95uniteObjects2= [];
gdjs.optionsCode.GDretrait_95centaineObjects1= [];
gdjs.optionsCode.GDretrait_95centaineObjects2= [];
gdjs.optionsCode.GDretrait_95dizaineObjects1= [];
gdjs.optionsCode.GDretrait_95dizaineObjects2= [];
gdjs.optionsCode.GDretrait_95uniteObjects1= [];
gdjs.optionsCode.GDretrait_95uniteObjects2= [];
gdjs.optionsCode.GDnb_95unitesObjects1= [];
gdjs.optionsCode.GDnb_95unitesObjects2= [];
gdjs.optionsCode.GDnb_95centainesObjects1= [];
gdjs.optionsCode.GDnb_95centainesObjects2= [];
gdjs.optionsCode.GDnb_95dizainesObjects1= [];
gdjs.optionsCode.GDnb_95dizainesObjects2= [];
gdjs.optionsCode.GDaxe_95fleche_95reponseObjects1= [];
gdjs.optionsCode.GDaxe_95fleche_95reponseObjects2= [];
gdjs.optionsCode.GDbouton_95retourObjects1= [];
gdjs.optionsCode.GDbouton_95retourObjects2= [];
gdjs.optionsCode.GDmaison2Objects1= [];
gdjs.optionsCode.GDmaison2Objects2= [];
gdjs.optionsCode.GDmaison3Objects1= [];
gdjs.optionsCode.GDmaison3Objects2= [];
gdjs.optionsCode.GDmaison4Objects1= [];
gdjs.optionsCode.GDmaison4Objects2= [];
gdjs.optionsCode.GDmaison5Objects1= [];
gdjs.optionsCode.GDmaison5Objects2= [];
gdjs.optionsCode.GDmaison1Objects1= [];
gdjs.optionsCode.GDmaison1Objects2= [];
gdjs.optionsCode.GDrouteObjects1= [];
gdjs.optionsCode.GDrouteObjects2= [];
gdjs.optionsCode.GDville1Objects1= [];
gdjs.optionsCode.GDville1Objects2= [];
gdjs.optionsCode.GDfond_95blancObjects1= [];
gdjs.optionsCode.GDfond_95blancObjects2= [];
gdjs.optionsCode.GDnb_95maxiObjects1= [];
gdjs.optionsCode.GDnb_95maxiObjects2= [];
gdjs.optionsCode.GDnb_95miniObjects1= [];
gdjs.optionsCode.GDnb_95miniObjects2= [];
gdjs.optionsCode.GDetObjects1= [];
gdjs.optionsCode.GDetObjects2= [];
gdjs.optionsCode.GDtravaillerObjects1= [];
gdjs.optionsCode.GDtravaillerObjects2= [];

gdjs.optionsCode.conditionTrue_0 = {val:false};
gdjs.optionsCode.condition0IsTrue_0 = {val:false};
gdjs.optionsCode.condition1IsTrue_0 = {val:false};
gdjs.optionsCode.condition2IsTrue_0 = {val:false};
gdjs.optionsCode.condition3IsTrue_0 = {val:false};
gdjs.optionsCode.condition4IsTrue_0 = {val:false};


gdjs.optionsCode.mapOfGDgdjs_46optionsCode_46GDajout_9595uniteObjects1Objects = Hashtable.newFrom({"ajout_unite": gdjs.optionsCode.GDajout_95uniteObjects1});
gdjs.optionsCode.mapOfGDgdjs_46optionsCode_46GDretrait_9595uniteObjects1Objects = Hashtable.newFrom({"retrait_unite": gdjs.optionsCode.GDretrait_95uniteObjects1});
gdjs.optionsCode.mapOfGDgdjs_46optionsCode_46GDajout_9595dizaineObjects1Objects = Hashtable.newFrom({"ajout_dizaine": gdjs.optionsCode.GDajout_95dizaineObjects1});
gdjs.optionsCode.mapOfGDgdjs_46optionsCode_46GDretrait_9595dizaineObjects1Objects = Hashtable.newFrom({"retrait_dizaine": gdjs.optionsCode.GDretrait_95dizaineObjects1});
gdjs.optionsCode.mapOfGDgdjs_46optionsCode_46GDbouton_9595retourObjects1Objects = Hashtable.newFrom({"bouton_retour": gdjs.optionsCode.GDbouton_95retourObjects1});
gdjs.optionsCode.eventsList0 = function(runtimeScene) {

{


gdjs.optionsCode.condition0IsTrue_0.val = false;
{
gdjs.optionsCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.optionsCode.condition0IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("fond_blanc"), gdjs.optionsCode.GDfond_95blancObjects1);
gdjs.copyArray(runtimeScene.getObjects("nb_maxi"), gdjs.optionsCode.GDnb_95maxiObjects1);
gdjs.copyArray(runtimeScene.getObjects("nb_mini"), gdjs.optionsCode.GDnb_95miniObjects1);
{for(var i = 0, len = gdjs.optionsCode.GDnb_95miniObjects1.length ;i < len;++i) {
    gdjs.optionsCode.GDnb_95miniObjects1[i].setString(gdjs.evtTools.common.toString(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4).getChild("mini"))));
}
}{for(var i = 0, len = gdjs.optionsCode.GDnb_95maxiObjects1.length ;i < len;++i) {
    gdjs.optionsCode.GDnb_95maxiObjects1[i].setString(gdjs.evtTools.common.toString(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4).getChild("maxi"))));
}
}{for(var i = 0, len = gdjs.optionsCode.GDfond_95blancObjects1.length ;i < len;++i) {
    gdjs.optionsCode.GDfond_95blancObjects1[i].setOpacity(200);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("ajout_unite"), gdjs.optionsCode.GDajout_95uniteObjects1);

gdjs.optionsCode.condition0IsTrue_0.val = false;
gdjs.optionsCode.condition1IsTrue_0.val = false;
gdjs.optionsCode.condition2IsTrue_0.val = false;
gdjs.optionsCode.condition3IsTrue_0.val = false;
{
gdjs.optionsCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.optionsCode.mapOfGDgdjs_46optionsCode_46GDajout_9595uniteObjects1Objects, runtimeScene, true, false);
}if ( gdjs.optionsCode.condition0IsTrue_0.val ) {
{
gdjs.optionsCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.optionsCode.condition1IsTrue_0.val ) {
{
gdjs.optionsCode.condition2IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4).getChild("mini")) < 100;
}if ( gdjs.optionsCode.condition2IsTrue_0.val ) {
{
gdjs.optionsCode.condition3IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4).getChild("mini")) < gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4).getChild("maxi")) - 10;
}}
}
}
if (gdjs.optionsCode.condition3IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("nb_mini"), gdjs.optionsCode.GDnb_95miniObjects1);
{runtimeScene.getGame().getVariables().getFromIndex(4).getChild("mini").add(10);
}{for(var i = 0, len = gdjs.optionsCode.GDnb_95miniObjects1.length ;i < len;++i) {
    gdjs.optionsCode.GDnb_95miniObjects1[i].setString(gdjs.evtTools.common.toString(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4).getChild("mini"))));
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("retrait_unite"), gdjs.optionsCode.GDretrait_95uniteObjects1);

gdjs.optionsCode.condition0IsTrue_0.val = false;
gdjs.optionsCode.condition1IsTrue_0.val = false;
gdjs.optionsCode.condition2IsTrue_0.val = false;
{
gdjs.optionsCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.optionsCode.mapOfGDgdjs_46optionsCode_46GDretrait_9595uniteObjects1Objects, runtimeScene, true, false);
}if ( gdjs.optionsCode.condition0IsTrue_0.val ) {
{
gdjs.optionsCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.optionsCode.condition1IsTrue_0.val ) {
{
gdjs.optionsCode.condition2IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4).getChild("mini")) > 0;
}}
}
if (gdjs.optionsCode.condition2IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("nb_mini"), gdjs.optionsCode.GDnb_95miniObjects1);
{runtimeScene.getGame().getVariables().getFromIndex(4).getChild("mini").sub(10);
}{for(var i = 0, len = gdjs.optionsCode.GDnb_95miniObjects1.length ;i < len;++i) {
    gdjs.optionsCode.GDnb_95miniObjects1[i].setString(gdjs.evtTools.common.toString(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4).getChild("mini"))));
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("ajout_dizaine"), gdjs.optionsCode.GDajout_95dizaineObjects1);

gdjs.optionsCode.condition0IsTrue_0.val = false;
gdjs.optionsCode.condition1IsTrue_0.val = false;
gdjs.optionsCode.condition2IsTrue_0.val = false;
{
gdjs.optionsCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.optionsCode.mapOfGDgdjs_46optionsCode_46GDajout_9595dizaineObjects1Objects, runtimeScene, true, false);
}if ( gdjs.optionsCode.condition0IsTrue_0.val ) {
{
gdjs.optionsCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.optionsCode.condition1IsTrue_0.val ) {
{
gdjs.optionsCode.condition2IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4).getChild("maxi")) < 100;
}}
}
if (gdjs.optionsCode.condition2IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("nb_maxi"), gdjs.optionsCode.GDnb_95maxiObjects1);
{runtimeScene.getGame().getVariables().getFromIndex(4).getChild("maxi").add(10);
}{for(var i = 0, len = gdjs.optionsCode.GDnb_95maxiObjects1.length ;i < len;++i) {
    gdjs.optionsCode.GDnb_95maxiObjects1[i].setString(gdjs.evtTools.common.toString(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4).getChild("maxi"))));
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("retrait_dizaine"), gdjs.optionsCode.GDretrait_95dizaineObjects1);

gdjs.optionsCode.condition0IsTrue_0.val = false;
gdjs.optionsCode.condition1IsTrue_0.val = false;
gdjs.optionsCode.condition2IsTrue_0.val = false;
gdjs.optionsCode.condition3IsTrue_0.val = false;
{
gdjs.optionsCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.optionsCode.mapOfGDgdjs_46optionsCode_46GDretrait_9595dizaineObjects1Objects, runtimeScene, true, false);
}if ( gdjs.optionsCode.condition0IsTrue_0.val ) {
{
gdjs.optionsCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.optionsCode.condition1IsTrue_0.val ) {
{
gdjs.optionsCode.condition2IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4).getChild("maxi")) > 0;
}if ( gdjs.optionsCode.condition2IsTrue_0.val ) {
{
gdjs.optionsCode.condition3IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4).getChild("maxi")) > gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4).getChild("mini")) + 10;
}}
}
}
if (gdjs.optionsCode.condition3IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("nb_maxi"), gdjs.optionsCode.GDnb_95maxiObjects1);
{runtimeScene.getGame().getVariables().getFromIndex(4).getChild("maxi").sub(10);
}{for(var i = 0, len = gdjs.optionsCode.GDnb_95maxiObjects1.length ;i < len;++i) {
    gdjs.optionsCode.GDnb_95maxiObjects1[i].setString(gdjs.evtTools.common.toString(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4).getChild("maxi"))));
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_retour"), gdjs.optionsCode.GDbouton_95retourObjects1);

gdjs.optionsCode.condition0IsTrue_0.val = false;
gdjs.optionsCode.condition1IsTrue_0.val = false;
{
gdjs.optionsCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.optionsCode.mapOfGDgdjs_46optionsCode_46GDbouton_9595retourObjects1Objects, runtimeScene, true, false);
}if ( gdjs.optionsCode.condition0IsTrue_0.val ) {
{
gdjs.optionsCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}}
if (gdjs.optionsCode.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "menu", false);
}}

}


};

gdjs.optionsCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.optionsCode.GDscore2Objects1.length = 0;
gdjs.optionsCode.GDscore2Objects2.length = 0;
gdjs.optionsCode.GDscore1Objects1.length = 0;
gdjs.optionsCode.GDscore1Objects2.length = 0;
gdjs.optionsCode.GDdoigtObjects1.length = 0;
gdjs.optionsCode.GDdoigtObjects2.length = 0;
gdjs.optionsCode.GDdoigt2Objects1.length = 0;
gdjs.optionsCode.GDdoigt2Objects2.length = 0;
gdjs.optionsCode.GDquestion_95nbObjects1.length = 0;
gdjs.optionsCode.GDquestion_95nbObjects2.length = 0;
gdjs.optionsCode.GDtrainObjects1.length = 0;
gdjs.optionsCode.GDtrainObjects2.length = 0;
gdjs.optionsCode.GDtrain2Objects1.length = 0;
gdjs.optionsCode.GDtrain2Objects2.length = 0;
gdjs.optionsCode.GDajout_95centaineObjects1.length = 0;
gdjs.optionsCode.GDajout_95centaineObjects2.length = 0;
gdjs.optionsCode.GDajout_95dizaineObjects1.length = 0;
gdjs.optionsCode.GDajout_95dizaineObjects2.length = 0;
gdjs.optionsCode.GDajout_95uniteObjects1.length = 0;
gdjs.optionsCode.GDajout_95uniteObjects2.length = 0;
gdjs.optionsCode.GDretrait_95centaineObjects1.length = 0;
gdjs.optionsCode.GDretrait_95centaineObjects2.length = 0;
gdjs.optionsCode.GDretrait_95dizaineObjects1.length = 0;
gdjs.optionsCode.GDretrait_95dizaineObjects2.length = 0;
gdjs.optionsCode.GDretrait_95uniteObjects1.length = 0;
gdjs.optionsCode.GDretrait_95uniteObjects2.length = 0;
gdjs.optionsCode.GDnb_95unitesObjects1.length = 0;
gdjs.optionsCode.GDnb_95unitesObjects2.length = 0;
gdjs.optionsCode.GDnb_95centainesObjects1.length = 0;
gdjs.optionsCode.GDnb_95centainesObjects2.length = 0;
gdjs.optionsCode.GDnb_95dizainesObjects1.length = 0;
gdjs.optionsCode.GDnb_95dizainesObjects2.length = 0;
gdjs.optionsCode.GDaxe_95fleche_95reponseObjects1.length = 0;
gdjs.optionsCode.GDaxe_95fleche_95reponseObjects2.length = 0;
gdjs.optionsCode.GDbouton_95retourObjects1.length = 0;
gdjs.optionsCode.GDbouton_95retourObjects2.length = 0;
gdjs.optionsCode.GDmaison2Objects1.length = 0;
gdjs.optionsCode.GDmaison2Objects2.length = 0;
gdjs.optionsCode.GDmaison3Objects1.length = 0;
gdjs.optionsCode.GDmaison3Objects2.length = 0;
gdjs.optionsCode.GDmaison4Objects1.length = 0;
gdjs.optionsCode.GDmaison4Objects2.length = 0;
gdjs.optionsCode.GDmaison5Objects1.length = 0;
gdjs.optionsCode.GDmaison5Objects2.length = 0;
gdjs.optionsCode.GDmaison1Objects1.length = 0;
gdjs.optionsCode.GDmaison1Objects2.length = 0;
gdjs.optionsCode.GDrouteObjects1.length = 0;
gdjs.optionsCode.GDrouteObjects2.length = 0;
gdjs.optionsCode.GDville1Objects1.length = 0;
gdjs.optionsCode.GDville1Objects2.length = 0;
gdjs.optionsCode.GDfond_95blancObjects1.length = 0;
gdjs.optionsCode.GDfond_95blancObjects2.length = 0;
gdjs.optionsCode.GDnb_95maxiObjects1.length = 0;
gdjs.optionsCode.GDnb_95maxiObjects2.length = 0;
gdjs.optionsCode.GDnb_95miniObjects1.length = 0;
gdjs.optionsCode.GDnb_95miniObjects2.length = 0;
gdjs.optionsCode.GDetObjects1.length = 0;
gdjs.optionsCode.GDetObjects2.length = 0;
gdjs.optionsCode.GDtravaillerObjects1.length = 0;
gdjs.optionsCode.GDtravaillerObjects2.length = 0;

gdjs.optionsCode.eventsList0(runtimeScene);
return;

}

gdjs['optionsCode'] = gdjs.optionsCode;
