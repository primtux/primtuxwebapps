gdjs.infosCode = {};
gdjs.infosCode.localVariables = [];
gdjs.infosCode.GDfondObjects1= [];
gdjs.infosCode.GDfondObjects2= [];
gdjs.infosCode.GDbbtxt_9595info_95951Objects1= [];
gdjs.infosCode.GDbbtxt_9595info_95951Objects2= [];
gdjs.infosCode.GDfond_9595jauneObjects1= [];
gdjs.infosCode.GDfond_9595jauneObjects2= [];
gdjs.infosCode.GDinfos_95951Objects1= [];
gdjs.infosCode.GDinfos_95951Objects2= [];
gdjs.infosCode.GDbouton_9595reculerObjects1= [];
gdjs.infosCode.GDbouton_9595reculerObjects2= [];
gdjs.infosCode.GDtxt_9595pageObjects1= [];
gdjs.infosCode.GDtxt_9595pageObjects2= [];
gdjs.infosCode.GDbbtxt_9595info_95952Objects1= [];
gdjs.infosCode.GDbbtxt_9595info_95952Objects2= [];
gdjs.infosCode.GDbbtxt_9595info_95953Objects1= [];
gdjs.infosCode.GDbbtxt_9595info_95953Objects2= [];
gdjs.infosCode.GDbbtxt_9595info_95954Objects1= [];
gdjs.infosCode.GDbbtxt_9595info_95954Objects2= [];
gdjs.infosCode.GDjeu_9595etape_95951Objects1= [];
gdjs.infosCode.GDjeu_9595etape_95951Objects2= [];
gdjs.infosCode.GDjeu_9595etape_95952Objects1= [];
gdjs.infosCode.GDjeu_9595etape_95952Objects2= [];
gdjs.infosCode.GDjeu_9595etape_95953Objects1= [];
gdjs.infosCode.GDjeu_9595etape_95953Objects2= [];
gdjs.infosCode.GDoeuf_9595bObjects1= [];
gdjs.infosCode.GDoeuf_9595bObjects2= [];
gdjs.infosCode.GDoeuf_9595aObjects1= [];
gdjs.infosCode.GDoeuf_9595aObjects2= [];
gdjs.infosCode.GDcarteObjects1= [];
gdjs.infosCode.GDcarteObjects2= [];
gdjs.infosCode.GDtxt_9595oeuf_95951Objects1= [];
gdjs.infosCode.GDtxt_9595oeuf_95951Objects2= [];
gdjs.infosCode.GDtxt_9595oeuf_95952Objects1= [];
gdjs.infosCode.GDtxt_9595oeuf_95952Objects2= [];
gdjs.infosCode.GDbouton_9595suivantObjects1= [];
gdjs.infosCode.GDbouton_9595suivantObjects2= [];
gdjs.infosCode.GDswitch_9595couleurObjects1= [];
gdjs.infosCode.GDswitch_9595couleurObjects2= [];
gdjs.infosCode.GDtxt_9595etObjects1= [];
gdjs.infosCode.GDtxt_9595etObjects2= [];
gdjs.infosCode.GDscore_9595Objects1= [];
gdjs.infosCode.GDscore_9595Objects2= [];
gdjs.infosCode.GDtxt_9595couleurObjects1= [];
gdjs.infosCode.GDtxt_9595couleurObjects2= [];
gdjs.infosCode.GDpouletObjects1= [];
gdjs.infosCode.GDpouletObjects2= [];
gdjs.infosCode.GDbouton_9595retourObjects1= [];
gdjs.infosCode.GDbouton_9595retourObjects2= [];


gdjs.infosCode.mapOfGDgdjs_9546infosCode_9546GDbouton_95959595retourObjects1Objects = Hashtable.newFrom({"bouton_retour": gdjs.infosCode.GDbouton_9595retourObjects1});
gdjs.infosCode.mapOfGDgdjs_9546infosCode_9546GDbouton_95959595reculerObjects1Objects = Hashtable.newFrom({"bouton_reculer": gdjs.infosCode.GDbouton_9595reculerObjects1});
gdjs.infosCode.eventsList0 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("fond"), gdjs.infosCode.GDfondObjects1);
gdjs.copyArray(runtimeScene.getObjects("fond_jaune"), gdjs.infosCode.GDfond_9595jauneObjects1);
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "chrono");
}{for(var i = 0, len = gdjs.infosCode.GDfondObjects1.length ;i < len;++i) {
    gdjs.infosCode.GDfondObjects1[i].getBehavior("Opacity").setOpacity(100);
}
}{for(var i = 0, len = gdjs.infosCode.GDfond_9595jauneObjects1.length ;i < len;++i) {
    gdjs.infosCode.GDfond_9595jauneObjects1[i].getBehavior("Opacity").setOpacity(150);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_retour"), gdjs.infosCode.GDbouton_9595retourObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.infosCode.mapOfGDgdjs_9546infosCode_9546GDbouton_95959595retourObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "chrono") >= 0.5;
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "menu", false);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_reculer"), gdjs.infosCode.GDbouton_9595reculerObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.infosCode.mapOfGDgdjs_9546infosCode_9546GDbouton_95959595reculerObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "infos2", false);
}}

}


};

gdjs.infosCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.infosCode.GDfondObjects1.length = 0;
gdjs.infosCode.GDfondObjects2.length = 0;
gdjs.infosCode.GDbbtxt_9595info_95951Objects1.length = 0;
gdjs.infosCode.GDbbtxt_9595info_95951Objects2.length = 0;
gdjs.infosCode.GDfond_9595jauneObjects1.length = 0;
gdjs.infosCode.GDfond_9595jauneObjects2.length = 0;
gdjs.infosCode.GDinfos_95951Objects1.length = 0;
gdjs.infosCode.GDinfos_95951Objects2.length = 0;
gdjs.infosCode.GDbouton_9595reculerObjects1.length = 0;
gdjs.infosCode.GDbouton_9595reculerObjects2.length = 0;
gdjs.infosCode.GDtxt_9595pageObjects1.length = 0;
gdjs.infosCode.GDtxt_9595pageObjects2.length = 0;
gdjs.infosCode.GDbbtxt_9595info_95952Objects1.length = 0;
gdjs.infosCode.GDbbtxt_9595info_95952Objects2.length = 0;
gdjs.infosCode.GDbbtxt_9595info_95953Objects1.length = 0;
gdjs.infosCode.GDbbtxt_9595info_95953Objects2.length = 0;
gdjs.infosCode.GDbbtxt_9595info_95954Objects1.length = 0;
gdjs.infosCode.GDbbtxt_9595info_95954Objects2.length = 0;
gdjs.infosCode.GDjeu_9595etape_95951Objects1.length = 0;
gdjs.infosCode.GDjeu_9595etape_95951Objects2.length = 0;
gdjs.infosCode.GDjeu_9595etape_95952Objects1.length = 0;
gdjs.infosCode.GDjeu_9595etape_95952Objects2.length = 0;
gdjs.infosCode.GDjeu_9595etape_95953Objects1.length = 0;
gdjs.infosCode.GDjeu_9595etape_95953Objects2.length = 0;
gdjs.infosCode.GDoeuf_9595bObjects1.length = 0;
gdjs.infosCode.GDoeuf_9595bObjects2.length = 0;
gdjs.infosCode.GDoeuf_9595aObjects1.length = 0;
gdjs.infosCode.GDoeuf_9595aObjects2.length = 0;
gdjs.infosCode.GDcarteObjects1.length = 0;
gdjs.infosCode.GDcarteObjects2.length = 0;
gdjs.infosCode.GDtxt_9595oeuf_95951Objects1.length = 0;
gdjs.infosCode.GDtxt_9595oeuf_95951Objects2.length = 0;
gdjs.infosCode.GDtxt_9595oeuf_95952Objects1.length = 0;
gdjs.infosCode.GDtxt_9595oeuf_95952Objects2.length = 0;
gdjs.infosCode.GDbouton_9595suivantObjects1.length = 0;
gdjs.infosCode.GDbouton_9595suivantObjects2.length = 0;
gdjs.infosCode.GDswitch_9595couleurObjects1.length = 0;
gdjs.infosCode.GDswitch_9595couleurObjects2.length = 0;
gdjs.infosCode.GDtxt_9595etObjects1.length = 0;
gdjs.infosCode.GDtxt_9595etObjects2.length = 0;
gdjs.infosCode.GDscore_9595Objects1.length = 0;
gdjs.infosCode.GDscore_9595Objects2.length = 0;
gdjs.infosCode.GDtxt_9595couleurObjects1.length = 0;
gdjs.infosCode.GDtxt_9595couleurObjects2.length = 0;
gdjs.infosCode.GDpouletObjects1.length = 0;
gdjs.infosCode.GDpouletObjects2.length = 0;
gdjs.infosCode.GDbouton_9595retourObjects1.length = 0;
gdjs.infosCode.GDbouton_9595retourObjects2.length = 0;

gdjs.infosCode.eventsList0(runtimeScene);

return;

}

gdjs['infosCode'] = gdjs.infosCode;
