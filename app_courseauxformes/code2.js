gdjs.tirageCode = {};
gdjs.tirageCode.GDlama_95courseObjects1= [];
gdjs.tirageCode.GDlama_95courseObjects2= [];
gdjs.tirageCode.GDlama_95courseObjects3= [];
gdjs.tirageCode.GDlama_95courseObjects4= [];
gdjs.tirageCode.GDfond_95courseObjects1= [];
gdjs.tirageCode.GDfond_95courseObjects2= [];
gdjs.tirageCode.GDfond_95courseObjects3= [];
gdjs.tirageCode.GDfond_95courseObjects4= [];
gdjs.tirageCode.GDscore1Objects1= [];
gdjs.tirageCode.GDscore1Objects2= [];
gdjs.tirageCode.GDscore1Objects3= [];
gdjs.tirageCode.GDscore1Objects4= [];
gdjs.tirageCode.GDscore3Objects1= [];
gdjs.tirageCode.GDscore3Objects2= [];
gdjs.tirageCode.GDscore3Objects3= [];
gdjs.tirageCode.GDscore3Objects4= [];
gdjs.tirageCode.GDscore2Objects1= [];
gdjs.tirageCode.GDscore2Objects2= [];
gdjs.tirageCode.GDscore2Objects3= [];
gdjs.tirageCode.GDscore2Objects4= [];
gdjs.tirageCode.GDfond_95titreObjects1= [];
gdjs.tirageCode.GDfond_95titreObjects2= [];
gdjs.tirageCode.GDfond_95titreObjects3= [];
gdjs.tirageCode.GDfond_95titreObjects4= [];
gdjs.tirageCode.GDbouton_95retourObjects1= [];
gdjs.tirageCode.GDbouton_95retourObjects2= [];
gdjs.tirageCode.GDbouton_95retourObjects3= [];
gdjs.tirageCode.GDbouton_95retourObjects4= [];
gdjs.tirageCode.GDchargementObjects1= [];
gdjs.tirageCode.GDchargementObjects2= [];
gdjs.tirageCode.GDchargementObjects3= [];
gdjs.tirageCode.GDchargementObjects4= [];

gdjs.tirageCode.conditionTrue_0 = {val:false};
gdjs.tirageCode.condition0IsTrue_0 = {val:false};
gdjs.tirageCode.condition1IsTrue_0 = {val:false};
gdjs.tirageCode.condition2IsTrue_0 = {val:false};
gdjs.tirageCode.condition3IsTrue_0 = {val:false};
gdjs.tirageCode.condition4IsTrue_0 = {val:false};
gdjs.tirageCode.condition5IsTrue_0 = {val:false};
gdjs.tirageCode.condition6IsTrue_0 = {val:false};
gdjs.tirageCode.condition7IsTrue_0 = {val:false};
gdjs.tirageCode.condition8IsTrue_0 = {val:false};
gdjs.tirageCode.condition9IsTrue_0 = {val:false};
gdjs.tirageCode.condition10IsTrue_0 = {val:false};
gdjs.tirageCode.conditionTrue_1 = {val:false};
gdjs.tirageCode.condition0IsTrue_1 = {val:false};
gdjs.tirageCode.condition1IsTrue_1 = {val:false};
gdjs.tirageCode.condition2IsTrue_1 = {val:false};
gdjs.tirageCode.condition3IsTrue_1 = {val:false};
gdjs.tirageCode.condition4IsTrue_1 = {val:false};
gdjs.tirageCode.condition5IsTrue_1 = {val:false};
gdjs.tirageCode.condition6IsTrue_1 = {val:false};
gdjs.tirageCode.condition7IsTrue_1 = {val:false};
gdjs.tirageCode.condition8IsTrue_1 = {val:false};
gdjs.tirageCode.condition9IsTrue_1 = {val:false};
gdjs.tirageCode.condition10IsTrue_1 = {val:false};


gdjs.tirageCode.eventsList0 = function(runtimeScene) {

{


gdjs.tirageCode.condition0IsTrue_0.val = false;
{
gdjs.tirageCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) == 1;
}if (gdjs.tirageCode.condition0IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("lama_course"), gdjs.tirageCode.GDlama_95courseObjects2);
{for(var i = 0, len = gdjs.tirageCode.GDlama_95courseObjects2.length ;i < len;++i) {
    gdjs.tirageCode.GDlama_95courseObjects2[i].setAnimation(2);
}
}}

}


{


gdjs.tirageCode.condition0IsTrue_0.val = false;
{
gdjs.tirageCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) == 2;
}if (gdjs.tirageCode.condition0IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("lama_course"), gdjs.tirageCode.GDlama_95courseObjects2);
{for(var i = 0, len = gdjs.tirageCode.GDlama_95courseObjects2.length ;i < len;++i) {
    gdjs.tirageCode.GDlama_95courseObjects2[i].setAnimation(4);
}
}}

}


{


gdjs.tirageCode.condition0IsTrue_0.val = false;
{
gdjs.tirageCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) == 3;
}if (gdjs.tirageCode.condition0IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("lama_course"), gdjs.tirageCode.GDlama_95courseObjects1);
{for(var i = 0, len = gdjs.tirageCode.GDlama_95courseObjects1.length ;i < len;++i) {
    gdjs.tirageCode.GDlama_95courseObjects1[i].setAnimation(0);
}
}}

}


};gdjs.tirageCode.eventsList1 = function(runtimeScene) {

};gdjs.tirageCode.eventsList2 = function(runtimeScene) {

{


gdjs.tirageCode.condition0IsTrue_0.val = false;
{
gdjs.tirageCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1).getChild("nombre2")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1).getChild("nombre1"));
}if (gdjs.tirageCode.condition0IsTrue_0.val) {
{runtimeScene.getScene().getVariables().getFromIndex(1).getChild("etape").add(1);
}}

}


};gdjs.tirageCode.eventsList3 = function(runtimeScene) {

{


gdjs.tirageCode.condition0IsTrue_0.val = false;
gdjs.tirageCode.condition1IsTrue_0.val = false;
{
gdjs.tirageCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1).getChild("nombre3")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1).getChild("nombre1"));
}if ( gdjs.tirageCode.condition0IsTrue_0.val ) {
{
gdjs.tirageCode.condition1IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1).getChild("nombre3")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1).getChild("nombre2"));
}}
if (gdjs.tirageCode.condition1IsTrue_0.val) {
{runtimeScene.getScene().getVariables().getFromIndex(1).getChild("etape").add(1);
}}

}


};gdjs.tirageCode.eventsList4 = function(runtimeScene) {

{


gdjs.tirageCode.condition0IsTrue_0.val = false;
{
gdjs.tirageCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1).getChild("nombre4")) == 1;
}if (gdjs.tirageCode.condition0IsTrue_0.val) {
{runtimeScene.getScene().getVariables().getFromIndex(1).getChild("reponse").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1).getChild("nombre1")));
}{runtimeScene.getScene().getVariables().getFromIndex(1).getChild("etape").add(1);
}{runtimeScene.getScene().getVariables().getFromIndex(0).add(1);
}}

}


{


gdjs.tirageCode.condition0IsTrue_0.val = false;
{
gdjs.tirageCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1).getChild("nombre4")) == 2;
}if (gdjs.tirageCode.condition0IsTrue_0.val) {
{runtimeScene.getScene().getVariables().getFromIndex(1).getChild("reponse").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1).getChild("nombre2")));
}{runtimeScene.getScene().getVariables().getFromIndex(1).getChild("etape").add(1);
}{runtimeScene.getScene().getVariables().getFromIndex(0).add(1);
}}

}


{


gdjs.tirageCode.condition0IsTrue_0.val = false;
{
gdjs.tirageCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1).getChild("nombre4")) == 3;
}if (gdjs.tirageCode.condition0IsTrue_0.val) {
{runtimeScene.getScene().getVariables().getFromIndex(1).getChild("reponse").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1).getChild("nombre3")));
}{runtimeScene.getScene().getVariables().getFromIndex(1).getChild("etape").add(1);
}{runtimeScene.getScene().getVariables().getFromIndex(0).add(1);
}}

}


};gdjs.tirageCode.eventsList5 = function(runtimeScene) {

{


gdjs.tirageCode.condition0IsTrue_0.val = false;
{
gdjs.tirageCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1).getChild("etape")) == 1;
}if (gdjs.tirageCode.condition0IsTrue_0.val) {
{runtimeScene.getScene().getVariables().getFromIndex(1).getChild("nombre1").setNumber(gdjs.randomInRange(0, 3));
}{runtimeScene.getScene().getVariables().getFromIndex(1).getChild("etape").add(1);
}}

}


{


gdjs.tirageCode.condition0IsTrue_0.val = false;
{
gdjs.tirageCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1).getChild("etape")) == 2;
}if (gdjs.tirageCode.condition0IsTrue_0.val) {
{runtimeScene.getScene().getVariables().getFromIndex(1).getChild("nombre2").setNumber(gdjs.randomInRange(0, 3));
}
{ //Subevents
gdjs.tirageCode.eventsList2(runtimeScene);} //End of subevents
}

}


{


gdjs.tirageCode.condition0IsTrue_0.val = false;
{
gdjs.tirageCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1).getChild("etape")) == 3;
}if (gdjs.tirageCode.condition0IsTrue_0.val) {
{runtimeScene.getScene().getVariables().getFromIndex(1).getChild("nombre3").setNumber(gdjs.randomInRange(0, 3));
}
{ //Subevents
gdjs.tirageCode.eventsList3(runtimeScene);} //End of subevents
}

}


{


gdjs.tirageCode.condition0IsTrue_0.val = false;
{
gdjs.tirageCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1).getChild("etape")) == 4;
}if (gdjs.tirageCode.condition0IsTrue_0.val) {
{runtimeScene.getScene().getVariables().getFromIndex(1).getChild("nombre4").setNumber(gdjs.randomInRange(1, 3));
}{runtimeScene.getScene().getVariables().getFromIndex(1).getChild("etape").add(1);
}
{ //Subevents
gdjs.tirageCode.eventsList4(runtimeScene);} //End of subevents
}

}


{



}


};gdjs.tirageCode.eventsList6 = function(runtimeScene) {

{


gdjs.tirageCode.condition0IsTrue_0.val = false;
{
gdjs.tirageCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("b").getChild("question")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("a").getChild("question"));
}if (gdjs.tirageCode.condition0IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("lama_course"), gdjs.tirageCode.GDlama_95courseObjects2);
{runtimeScene.getScene().getVariables().getFromIndex(0).add(1);
}{for(var i = 0, len = gdjs.tirageCode.GDlama_95courseObjects2.length ;i < len;++i) {
    gdjs.tirageCode.GDlama_95courseObjects2[i].setX(gdjs.tirageCode.GDlama_95courseObjects2[i].getX() + (115));
}
}}

}


{


gdjs.tirageCode.condition0IsTrue_0.val = false;
{
{gdjs.tirageCode.conditionTrue_1 = gdjs.tirageCode.condition0IsTrue_0;
gdjs.tirageCode.condition0IsTrue_1.val = false;
{
gdjs.tirageCode.condition0IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("b").getChild("question")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("a").getChild("question"));
if( gdjs.tirageCode.condition0IsTrue_1.val ) {
    gdjs.tirageCode.conditionTrue_1.val = true;
}
}
{
}
}
}if (gdjs.tirageCode.condition0IsTrue_0.val) {
{runtimeScene.getScene().getVariables().getFromIndex(0).sub(1);
}}

}


};gdjs.tirageCode.eventsList7 = function(runtimeScene) {

{


gdjs.tirageCode.condition0IsTrue_0.val = false;
gdjs.tirageCode.condition1IsTrue_0.val = false;
{
gdjs.tirageCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("c").getChild("question")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("b").getChild("question"));
}if ( gdjs.tirageCode.condition0IsTrue_0.val ) {
{
gdjs.tirageCode.condition1IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("c").getChild("question")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("a").getChild("question"));
}}
if (gdjs.tirageCode.condition1IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("lama_course"), gdjs.tirageCode.GDlama_95courseObjects2);
{runtimeScene.getScene().getVariables().getFromIndex(0).add(1);
}{for(var i = 0, len = gdjs.tirageCode.GDlama_95courseObjects2.length ;i < len;++i) {
    gdjs.tirageCode.GDlama_95courseObjects2[i].setX(gdjs.tirageCode.GDlama_95courseObjects2[i].getX() + (115));
}
}}

}


{


gdjs.tirageCode.condition0IsTrue_0.val = false;
{
{gdjs.tirageCode.conditionTrue_1 = gdjs.tirageCode.condition0IsTrue_0;
gdjs.tirageCode.condition0IsTrue_1.val = false;
gdjs.tirageCode.condition1IsTrue_1.val = false;
{
gdjs.tirageCode.condition0IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("c").getChild("question")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("a").getChild("question"));
if( gdjs.tirageCode.condition0IsTrue_1.val ) {
    gdjs.tirageCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirageCode.condition1IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("c").getChild("question")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("b").getChild("question"));
if( gdjs.tirageCode.condition1IsTrue_1.val ) {
    gdjs.tirageCode.conditionTrue_1.val = true;
}
}
{
}
}
}if (gdjs.tirageCode.condition0IsTrue_0.val) {
{runtimeScene.getScene().getVariables().getFromIndex(0).sub(1);
}}

}


};gdjs.tirageCode.eventsList8 = function(runtimeScene) {

{


gdjs.tirageCode.condition0IsTrue_0.val = false;
gdjs.tirageCode.condition1IsTrue_0.val = false;
gdjs.tirageCode.condition2IsTrue_0.val = false;
{
gdjs.tirageCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("d").getChild("question")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("a").getChild("question"));
}if ( gdjs.tirageCode.condition0IsTrue_0.val ) {
{
gdjs.tirageCode.condition1IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("d").getChild("question")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("b").getChild("question"));
}if ( gdjs.tirageCode.condition1IsTrue_0.val ) {
{
gdjs.tirageCode.condition2IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("d").getChild("question")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("c").getChild("question"));
}}
}
if (gdjs.tirageCode.condition2IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("lama_course"), gdjs.tirageCode.GDlama_95courseObjects2);
{runtimeScene.getScene().getVariables().getFromIndex(0).add(1);
}{for(var i = 0, len = gdjs.tirageCode.GDlama_95courseObjects2.length ;i < len;++i) {
    gdjs.tirageCode.GDlama_95courseObjects2[i].setX(gdjs.tirageCode.GDlama_95courseObjects2[i].getX() + (115));
}
}}

}


{


gdjs.tirageCode.condition0IsTrue_0.val = false;
{
{gdjs.tirageCode.conditionTrue_1 = gdjs.tirageCode.condition0IsTrue_0;
gdjs.tirageCode.condition0IsTrue_1.val = false;
gdjs.tirageCode.condition1IsTrue_1.val = false;
gdjs.tirageCode.condition2IsTrue_1.val = false;
{
gdjs.tirageCode.condition0IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("d").getChild("question")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("a").getChild("question"));
if( gdjs.tirageCode.condition0IsTrue_1.val ) {
    gdjs.tirageCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirageCode.condition1IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("d").getChild("question")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("b").getChild("question"));
if( gdjs.tirageCode.condition1IsTrue_1.val ) {
    gdjs.tirageCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirageCode.condition2IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("d").getChild("question")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("c").getChild("question"));
if( gdjs.tirageCode.condition2IsTrue_1.val ) {
    gdjs.tirageCode.conditionTrue_1.val = true;
}
}
{
}
}
}if (gdjs.tirageCode.condition0IsTrue_0.val) {
{runtimeScene.getScene().getVariables().getFromIndex(0).sub(1);
}}

}


};gdjs.tirageCode.eventsList9 = function(runtimeScene) {

{


gdjs.tirageCode.condition0IsTrue_0.val = false;
gdjs.tirageCode.condition1IsTrue_0.val = false;
gdjs.tirageCode.condition2IsTrue_0.val = false;
{
gdjs.tirageCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("e").getChild("question")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("d").getChild("question"));
}if ( gdjs.tirageCode.condition0IsTrue_0.val ) {
{
gdjs.tirageCode.condition1IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("e").getChild("question")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("c").getChild("question"));
}if ( gdjs.tirageCode.condition1IsTrue_0.val ) {
{
gdjs.tirageCode.condition2IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("e").getChild("question")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("b").getChild("question"));
}}
}
if (gdjs.tirageCode.condition2IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("lama_course"), gdjs.tirageCode.GDlama_95courseObjects2);
{runtimeScene.getScene().getVariables().getFromIndex(0).add(1);
}{for(var i = 0, len = gdjs.tirageCode.GDlama_95courseObjects2.length ;i < len;++i) {
    gdjs.tirageCode.GDlama_95courseObjects2[i].setX(gdjs.tirageCode.GDlama_95courseObjects2[i].getX() + (115));
}
}}

}


{


gdjs.tirageCode.condition0IsTrue_0.val = false;
{
{gdjs.tirageCode.conditionTrue_1 = gdjs.tirageCode.condition0IsTrue_0;
gdjs.tirageCode.condition0IsTrue_1.val = false;
gdjs.tirageCode.condition1IsTrue_1.val = false;
gdjs.tirageCode.condition2IsTrue_1.val = false;
{
gdjs.tirageCode.condition0IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("e").getChild("question")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("d").getChild("question"));
if( gdjs.tirageCode.condition0IsTrue_1.val ) {
    gdjs.tirageCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirageCode.condition1IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("e").getChild("question")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("c").getChild("question"));
if( gdjs.tirageCode.condition1IsTrue_1.val ) {
    gdjs.tirageCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirageCode.condition2IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("e").getChild("question")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("b").getChild("question"));
if( gdjs.tirageCode.condition2IsTrue_1.val ) {
    gdjs.tirageCode.conditionTrue_1.val = true;
}
}
{
}
}
}if (gdjs.tirageCode.condition0IsTrue_0.val) {
{runtimeScene.getScene().getVariables().getFromIndex(0).sub(1);
}}

}


};gdjs.tirageCode.eventsList10 = function(runtimeScene) {

{


gdjs.tirageCode.condition0IsTrue_0.val = false;
gdjs.tirageCode.condition1IsTrue_0.val = false;
gdjs.tirageCode.condition2IsTrue_0.val = false;
{
gdjs.tirageCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("f").getChild("question")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("c").getChild("question"));
}if ( gdjs.tirageCode.condition0IsTrue_0.val ) {
{
gdjs.tirageCode.condition1IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("f").getChild("question")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("d").getChild("question"));
}if ( gdjs.tirageCode.condition1IsTrue_0.val ) {
{
gdjs.tirageCode.condition2IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("f").getChild("question")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("e").getChild("question"));
}}
}
if (gdjs.tirageCode.condition2IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("lama_course"), gdjs.tirageCode.GDlama_95courseObjects2);
{runtimeScene.getScene().getVariables().getFromIndex(0).add(1);
}{for(var i = 0, len = gdjs.tirageCode.GDlama_95courseObjects2.length ;i < len;++i) {
    gdjs.tirageCode.GDlama_95courseObjects2[i].setX(gdjs.tirageCode.GDlama_95courseObjects2[i].getX() + (115));
}
}}

}


{


gdjs.tirageCode.condition0IsTrue_0.val = false;
{
{gdjs.tirageCode.conditionTrue_1 = gdjs.tirageCode.condition0IsTrue_0;
gdjs.tirageCode.condition0IsTrue_1.val = false;
gdjs.tirageCode.condition1IsTrue_1.val = false;
gdjs.tirageCode.condition2IsTrue_1.val = false;
{
gdjs.tirageCode.condition0IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("f").getChild("question")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("c").getChild("question"));
if( gdjs.tirageCode.condition0IsTrue_1.val ) {
    gdjs.tirageCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirageCode.condition1IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("f").getChild("question")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("d").getChild("question"));
if( gdjs.tirageCode.condition1IsTrue_1.val ) {
    gdjs.tirageCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirageCode.condition2IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("f").getChild("question")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("e").getChild("question"));
if( gdjs.tirageCode.condition2IsTrue_1.val ) {
    gdjs.tirageCode.conditionTrue_1.val = true;
}
}
{
}
}
}if (gdjs.tirageCode.condition0IsTrue_0.val) {
{runtimeScene.getScene().getVariables().getFromIndex(0).sub(1);
}}

}


};gdjs.tirageCode.eventsList11 = function(runtimeScene) {

{


gdjs.tirageCode.condition0IsTrue_0.val = false;
gdjs.tirageCode.condition1IsTrue_0.val = false;
gdjs.tirageCode.condition2IsTrue_0.val = false;
{
gdjs.tirageCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("g").getChild("question")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("d").getChild("question"));
}if ( gdjs.tirageCode.condition0IsTrue_0.val ) {
{
gdjs.tirageCode.condition1IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("g").getChild("question")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("e").getChild("question"));
}if ( gdjs.tirageCode.condition1IsTrue_0.val ) {
{
gdjs.tirageCode.condition2IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("g").getChild("question")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("f").getChild("question"));
}}
}
if (gdjs.tirageCode.condition2IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("lama_course"), gdjs.tirageCode.GDlama_95courseObjects2);
{runtimeScene.getScene().getVariables().getFromIndex(0).add(1);
}{for(var i = 0, len = gdjs.tirageCode.GDlama_95courseObjects2.length ;i < len;++i) {
    gdjs.tirageCode.GDlama_95courseObjects2[i].setX(gdjs.tirageCode.GDlama_95courseObjects2[i].getX() + (115));
}
}}

}


{


gdjs.tirageCode.condition0IsTrue_0.val = false;
{
{gdjs.tirageCode.conditionTrue_1 = gdjs.tirageCode.condition0IsTrue_0;
gdjs.tirageCode.condition0IsTrue_1.val = false;
gdjs.tirageCode.condition1IsTrue_1.val = false;
gdjs.tirageCode.condition2IsTrue_1.val = false;
{
gdjs.tirageCode.condition0IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("g").getChild("question")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("d").getChild("question"));
if( gdjs.tirageCode.condition0IsTrue_1.val ) {
    gdjs.tirageCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirageCode.condition1IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("g").getChild("question")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("e").getChild("question"));
if( gdjs.tirageCode.condition1IsTrue_1.val ) {
    gdjs.tirageCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirageCode.condition2IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("g").getChild("question")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("f").getChild("question"));
if( gdjs.tirageCode.condition2IsTrue_1.val ) {
    gdjs.tirageCode.conditionTrue_1.val = true;
}
}
{
}
}
}if (gdjs.tirageCode.condition0IsTrue_0.val) {
{runtimeScene.getScene().getVariables().getFromIndex(0).sub(1);
}}

}


};gdjs.tirageCode.eventsList12 = function(runtimeScene) {

{


gdjs.tirageCode.condition0IsTrue_0.val = false;
gdjs.tirageCode.condition1IsTrue_0.val = false;
gdjs.tirageCode.condition2IsTrue_0.val = false;
{
gdjs.tirageCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("h").getChild("question")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("g").getChild("question"));
}if ( gdjs.tirageCode.condition0IsTrue_0.val ) {
{
gdjs.tirageCode.condition1IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("h").getChild("question")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("e").getChild("question"));
}if ( gdjs.tirageCode.condition1IsTrue_0.val ) {
{
gdjs.tirageCode.condition2IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("h").getChild("question")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("f").getChild("question"));
}}
}
if (gdjs.tirageCode.condition2IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("lama_course"), gdjs.tirageCode.GDlama_95courseObjects2);
{runtimeScene.getScene().getVariables().getFromIndex(0).add(1);
}{for(var i = 0, len = gdjs.tirageCode.GDlama_95courseObjects2.length ;i < len;++i) {
    gdjs.tirageCode.GDlama_95courseObjects2[i].setX(gdjs.tirageCode.GDlama_95courseObjects2[i].getX() + (115));
}
}}

}


{


gdjs.tirageCode.condition0IsTrue_0.val = false;
{
{gdjs.tirageCode.conditionTrue_1 = gdjs.tirageCode.condition0IsTrue_0;
gdjs.tirageCode.condition0IsTrue_1.val = false;
gdjs.tirageCode.condition1IsTrue_1.val = false;
gdjs.tirageCode.condition2IsTrue_1.val = false;
{
gdjs.tirageCode.condition0IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("h").getChild("question")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("g").getChild("question"));
if( gdjs.tirageCode.condition0IsTrue_1.val ) {
    gdjs.tirageCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirageCode.condition1IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("h").getChild("question")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("e").getChild("question"));
if( gdjs.tirageCode.condition1IsTrue_1.val ) {
    gdjs.tirageCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirageCode.condition2IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("h").getChild("question")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("f").getChild("question"));
if( gdjs.tirageCode.condition2IsTrue_1.val ) {
    gdjs.tirageCode.conditionTrue_1.val = true;
}
}
{
}
}
}if (gdjs.tirageCode.condition0IsTrue_0.val) {
{runtimeScene.getScene().getVariables().getFromIndex(0).sub(1);
}}

}


};gdjs.tirageCode.eventsList13 = function(runtimeScene) {

{


gdjs.tirageCode.condition0IsTrue_0.val = false;
gdjs.tirageCode.condition1IsTrue_0.val = false;
gdjs.tirageCode.condition2IsTrue_0.val = false;
{
gdjs.tirageCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("i").getChild("question")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("g").getChild("question"));
}if ( gdjs.tirageCode.condition0IsTrue_0.val ) {
{
gdjs.tirageCode.condition1IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("i").getChild("question")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("h").getChild("question"));
}if ( gdjs.tirageCode.condition1IsTrue_0.val ) {
{
gdjs.tirageCode.condition2IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("i").getChild("question")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("f").getChild("question"));
}}
}
if (gdjs.tirageCode.condition2IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("lama_course"), gdjs.tirageCode.GDlama_95courseObjects2);
{runtimeScene.getScene().getVariables().getFromIndex(0).add(1);
}{for(var i = 0, len = gdjs.tirageCode.GDlama_95courseObjects2.length ;i < len;++i) {
    gdjs.tirageCode.GDlama_95courseObjects2[i].setX(gdjs.tirageCode.GDlama_95courseObjects2[i].getX() + (115));
}
}}

}


{


gdjs.tirageCode.condition0IsTrue_0.val = false;
{
{gdjs.tirageCode.conditionTrue_1 = gdjs.tirageCode.condition0IsTrue_0;
gdjs.tirageCode.condition0IsTrue_1.val = false;
gdjs.tirageCode.condition1IsTrue_1.val = false;
gdjs.tirageCode.condition2IsTrue_1.val = false;
{
gdjs.tirageCode.condition0IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("i").getChild("question")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("g").getChild("question"));
if( gdjs.tirageCode.condition0IsTrue_1.val ) {
    gdjs.tirageCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirageCode.condition1IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("i").getChild("question")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("h").getChild("question"));
if( gdjs.tirageCode.condition1IsTrue_1.val ) {
    gdjs.tirageCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirageCode.condition2IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("i").getChild("question")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("f").getChild("question"));
if( gdjs.tirageCode.condition2IsTrue_1.val ) {
    gdjs.tirageCode.conditionTrue_1.val = true;
}
}
{
}
}
}if (gdjs.tirageCode.condition0IsTrue_0.val) {
{runtimeScene.getScene().getVariables().getFromIndex(0).sub(1);
}}

}


};gdjs.tirageCode.eventsList14 = function(runtimeScene) {

{


gdjs.tirageCode.condition0IsTrue_0.val = false;
gdjs.tirageCode.condition1IsTrue_0.val = false;
gdjs.tirageCode.condition2IsTrue_0.val = false;
{
gdjs.tirageCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("j").getChild("question")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("g").getChild("question"));
}if ( gdjs.tirageCode.condition0IsTrue_0.val ) {
{
gdjs.tirageCode.condition1IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("j").getChild("question")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("h").getChild("question"));
}if ( gdjs.tirageCode.condition1IsTrue_0.val ) {
{
gdjs.tirageCode.condition2IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("j").getChild("question")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("i").getChild("question"));
}}
}
if (gdjs.tirageCode.condition2IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("lama_course"), gdjs.tirageCode.GDlama_95courseObjects2);
{runtimeScene.getScene().getVariables().getFromIndex(0).add(1);
}{for(var i = 0, len = gdjs.tirageCode.GDlama_95courseObjects2.length ;i < len;++i) {
    gdjs.tirageCode.GDlama_95courseObjects2[i].setX(gdjs.tirageCode.GDlama_95courseObjects2[i].getX() + (115));
}
}}

}


{


gdjs.tirageCode.condition0IsTrue_0.val = false;
{
{gdjs.tirageCode.conditionTrue_1 = gdjs.tirageCode.condition0IsTrue_0;
gdjs.tirageCode.condition0IsTrue_1.val = false;
gdjs.tirageCode.condition1IsTrue_1.val = false;
gdjs.tirageCode.condition2IsTrue_1.val = false;
{
gdjs.tirageCode.condition0IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("j").getChild("question")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("g").getChild("question"));
if( gdjs.tirageCode.condition0IsTrue_1.val ) {
    gdjs.tirageCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirageCode.condition1IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("j").getChild("question")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("h").getChild("question"));
if( gdjs.tirageCode.condition1IsTrue_1.val ) {
    gdjs.tirageCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirageCode.condition2IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("j").getChild("question")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("i").getChild("question"));
if( gdjs.tirageCode.condition2IsTrue_1.val ) {
    gdjs.tirageCode.conditionTrue_1.val = true;
}
}
{
}
}
}if (gdjs.tirageCode.condition0IsTrue_0.val) {
{runtimeScene.getScene().getVariables().getFromIndex(0).sub(1);
}}

}


};gdjs.tirageCode.eventsList15 = function(runtimeScene) {

{


gdjs.tirageCode.condition0IsTrue_0.val = false;
{
gdjs.tirageCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.tirageCode.condition0IsTrue_0.val) {
{runtimeScene.getScene().getVariables().getFromIndex(0).setNumber(1);
}
{ //Subevents
gdjs.tirageCode.eventsList0(runtimeScene);} //End of subevents
}

}


{


gdjs.tirageCode.eventsList1(runtimeScene);
}


{


gdjs.tirageCode.condition0IsTrue_0.val = false;
{
{gdjs.tirageCode.conditionTrue_1 = gdjs.tirageCode.condition0IsTrue_0;
gdjs.tirageCode.condition0IsTrue_1.val = false;
gdjs.tirageCode.condition1IsTrue_1.val = false;
gdjs.tirageCode.condition2IsTrue_1.val = false;
gdjs.tirageCode.condition3IsTrue_1.val = false;
gdjs.tirageCode.condition4IsTrue_1.val = false;
gdjs.tirageCode.condition5IsTrue_1.val = false;
gdjs.tirageCode.condition6IsTrue_1.val = false;
gdjs.tirageCode.condition7IsTrue_1.val = false;
gdjs.tirageCode.condition8IsTrue_1.val = false;
gdjs.tirageCode.condition9IsTrue_1.val = false;
{
gdjs.tirageCode.condition0IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 1;
if( gdjs.tirageCode.condition0IsTrue_1.val ) {
    gdjs.tirageCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirageCode.condition1IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 3;
if( gdjs.tirageCode.condition1IsTrue_1.val ) {
    gdjs.tirageCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirageCode.condition2IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 5;
if( gdjs.tirageCode.condition2IsTrue_1.val ) {
    gdjs.tirageCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirageCode.condition3IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 7;
if( gdjs.tirageCode.condition3IsTrue_1.val ) {
    gdjs.tirageCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirageCode.condition4IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 9;
if( gdjs.tirageCode.condition4IsTrue_1.val ) {
    gdjs.tirageCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirageCode.condition5IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 11;
if( gdjs.tirageCode.condition5IsTrue_1.val ) {
    gdjs.tirageCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirageCode.condition6IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 13;
if( gdjs.tirageCode.condition6IsTrue_1.val ) {
    gdjs.tirageCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirageCode.condition7IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 15;
if( gdjs.tirageCode.condition7IsTrue_1.val ) {
    gdjs.tirageCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirageCode.condition8IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 17;
if( gdjs.tirageCode.condition8IsTrue_1.val ) {
    gdjs.tirageCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirageCode.condition9IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 19;
if( gdjs.tirageCode.condition9IsTrue_1.val ) {
    gdjs.tirageCode.conditionTrue_1.val = true;
}
}
{
}
}
}if (gdjs.tirageCode.condition0IsTrue_0.val) {
{runtimeScene.getScene().getVariables().getFromIndex(1).getChild("etape").setNumber(1);
}}

}


{


gdjs.tirageCode.eventsList5(runtimeScene);
}


{


gdjs.tirageCode.condition0IsTrue_0.val = false;
gdjs.tirageCode.condition1IsTrue_0.val = false;
{
gdjs.tirageCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 2;
}if ( gdjs.tirageCode.condition0IsTrue_0.val ) {
{
{gdjs.tirageCode.conditionTrue_1 = gdjs.tirageCode.condition1IsTrue_0;
gdjs.tirageCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(9757332);
}
}}
if (gdjs.tirageCode.condition1IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("lama_course"), gdjs.tirageCode.GDlama_95courseObjects1);
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("a").getChild("1").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1).getChild("nombre1")));
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("a").getChild("2").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1).getChild("nombre2")));
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("a").getChild("3").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1).getChild("nombre3")));
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("a").getChild("question").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1).getChild("reponse")));
}{runtimeScene.getScene().getVariables().getFromIndex(0).add(1);
}{for(var i = 0, len = gdjs.tirageCode.GDlama_95courseObjects1.length ;i < len;++i) {
    gdjs.tirageCode.GDlama_95courseObjects1[i].setX(gdjs.tirageCode.GDlama_95courseObjects1[i].getX() + (115));
}
}}

}


{


gdjs.tirageCode.condition0IsTrue_0.val = false;
{
gdjs.tirageCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 4;
}if (gdjs.tirageCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("b").getChild("1").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1).getChild("nombre1")));
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("b").getChild("2").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1).getChild("nombre2")));
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("b").getChild("3").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1).getChild("nombre3")));
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("b").getChild("question").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1).getChild("reponse")));
}
{ //Subevents
gdjs.tirageCode.eventsList6(runtimeScene);} //End of subevents
}

}


{


gdjs.tirageCode.condition0IsTrue_0.val = false;
{
gdjs.tirageCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 6;
}if (gdjs.tirageCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("c").getChild("1").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1).getChild("nombre1")));
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("c").getChild("2").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1).getChild("nombre2")));
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("c").getChild("3").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1).getChild("nombre3")));
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("c").getChild("question").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1).getChild("reponse")));
}
{ //Subevents
gdjs.tirageCode.eventsList7(runtimeScene);} //End of subevents
}

}


{


gdjs.tirageCode.condition0IsTrue_0.val = false;
{
gdjs.tirageCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 8;
}if (gdjs.tirageCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("d").getChild("1").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1).getChild("nombre1")));
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("d").getChild("2").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1).getChild("nombre2")));
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("d").getChild("3").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1).getChild("nombre3")));
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("d").getChild("question").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1).getChild("reponse")));
}
{ //Subevents
gdjs.tirageCode.eventsList8(runtimeScene);} //End of subevents
}

}


{


gdjs.tirageCode.condition0IsTrue_0.val = false;
{
gdjs.tirageCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 10;
}if (gdjs.tirageCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("e").getChild("1").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1).getChild("nombre1")));
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("e").getChild("2").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1).getChild("nombre2")));
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("e").getChild("3").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1).getChild("nombre3")));
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("e").getChild("question").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1).getChild("reponse")));
}
{ //Subevents
gdjs.tirageCode.eventsList9(runtimeScene);} //End of subevents
}

}


{


gdjs.tirageCode.condition0IsTrue_0.val = false;
{
gdjs.tirageCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 12;
}if (gdjs.tirageCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("f").getChild("1").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1).getChild("nombre1")));
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("f").getChild("2").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1).getChild("nombre2")));
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("f").getChild("3").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1).getChild("nombre3")));
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("f").getChild("question").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1).getChild("reponse")));
}
{ //Subevents
gdjs.tirageCode.eventsList10(runtimeScene);} //End of subevents
}

}


{


gdjs.tirageCode.condition0IsTrue_0.val = false;
{
gdjs.tirageCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 14;
}if (gdjs.tirageCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("g").getChild("1").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1).getChild("nombre1")));
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("g").getChild("2").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1).getChild("nombre2")));
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("g").getChild("3").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1).getChild("nombre3")));
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("g").getChild("question").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1).getChild("reponse")));
}
{ //Subevents
gdjs.tirageCode.eventsList11(runtimeScene);} //End of subevents
}

}


{


gdjs.tirageCode.condition0IsTrue_0.val = false;
{
gdjs.tirageCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 16;
}if (gdjs.tirageCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("h").getChild("1").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1).getChild("nombre1")));
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("h").getChild("2").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1).getChild("nombre2")));
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("h").getChild("3").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1).getChild("nombre3")));
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("h").getChild("question").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1).getChild("reponse")));
}
{ //Subevents
gdjs.tirageCode.eventsList12(runtimeScene);} //End of subevents
}

}


{


gdjs.tirageCode.condition0IsTrue_0.val = false;
{
gdjs.tirageCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 18;
}if (gdjs.tirageCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("i").getChild("1").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1).getChild("nombre1")));
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("i").getChild("2").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1).getChild("nombre2")));
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("i").getChild("3").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1).getChild("nombre3")));
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("i").getChild("question").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1).getChild("reponse")));
}
{ //Subevents
gdjs.tirageCode.eventsList13(runtimeScene);} //End of subevents
}

}


{


gdjs.tirageCode.condition0IsTrue_0.val = false;
{
gdjs.tirageCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 20;
}if (gdjs.tirageCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("j").getChild("1").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1).getChild("nombre1")));
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("j").getChild("2").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1).getChild("nombre2")));
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("j").getChild("3").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1).getChild("nombre3")));
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("j").getChild("question").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1).getChild("reponse")));
}
{ //Subevents
gdjs.tirageCode.eventsList14(runtimeScene);} //End of subevents
}

}


{


gdjs.tirageCode.condition0IsTrue_0.val = false;
gdjs.tirageCode.condition1IsTrue_0.val = false;
{
gdjs.tirageCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 21;
}if ( gdjs.tirageCode.condition0IsTrue_0.val ) {
{
{gdjs.tirageCode.conditionTrue_1 = gdjs.tirageCode.condition1IsTrue_0;
gdjs.tirageCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(9796580);
}
}}
if (gdjs.tirageCode.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "jeu1", false);
}}

}


};

gdjs.tirageCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.tirageCode.GDlama_95courseObjects1.length = 0;
gdjs.tirageCode.GDlama_95courseObjects2.length = 0;
gdjs.tirageCode.GDlama_95courseObjects3.length = 0;
gdjs.tirageCode.GDlama_95courseObjects4.length = 0;
gdjs.tirageCode.GDfond_95courseObjects1.length = 0;
gdjs.tirageCode.GDfond_95courseObjects2.length = 0;
gdjs.tirageCode.GDfond_95courseObjects3.length = 0;
gdjs.tirageCode.GDfond_95courseObjects4.length = 0;
gdjs.tirageCode.GDscore1Objects1.length = 0;
gdjs.tirageCode.GDscore1Objects2.length = 0;
gdjs.tirageCode.GDscore1Objects3.length = 0;
gdjs.tirageCode.GDscore1Objects4.length = 0;
gdjs.tirageCode.GDscore3Objects1.length = 0;
gdjs.tirageCode.GDscore3Objects2.length = 0;
gdjs.tirageCode.GDscore3Objects3.length = 0;
gdjs.tirageCode.GDscore3Objects4.length = 0;
gdjs.tirageCode.GDscore2Objects1.length = 0;
gdjs.tirageCode.GDscore2Objects2.length = 0;
gdjs.tirageCode.GDscore2Objects3.length = 0;
gdjs.tirageCode.GDscore2Objects4.length = 0;
gdjs.tirageCode.GDfond_95titreObjects1.length = 0;
gdjs.tirageCode.GDfond_95titreObjects2.length = 0;
gdjs.tirageCode.GDfond_95titreObjects3.length = 0;
gdjs.tirageCode.GDfond_95titreObjects4.length = 0;
gdjs.tirageCode.GDbouton_95retourObjects1.length = 0;
gdjs.tirageCode.GDbouton_95retourObjects2.length = 0;
gdjs.tirageCode.GDbouton_95retourObjects3.length = 0;
gdjs.tirageCode.GDbouton_95retourObjects4.length = 0;
gdjs.tirageCode.GDchargementObjects1.length = 0;
gdjs.tirageCode.GDchargementObjects2.length = 0;
gdjs.tirageCode.GDchargementObjects3.length = 0;
gdjs.tirageCode.GDchargementObjects4.length = 0;

gdjs.tirageCode.eventsList15(runtimeScene);

return;

}

gdjs['tirageCode'] = gdjs.tirageCode;
