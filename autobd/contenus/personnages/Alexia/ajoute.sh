#!/bin/bash

# Parcourir tous les fichiers SVG récursivement
find . -type f -name "*.svg" | while read -r file; do
    # Vérifier si le fichier SVG contient déjà la propriété preserveAspectRatio
    if ! grep -q 'preserveAspectRatio="xMidYMin slice"' "$file"; then
        # Ajouter la propriété à l'intérieur de la première balise <svg>
        sed -i '0,/<svg /s/<svg /<svg preserveAspectRatio="xMidYMin slice" /' "$file"
        echo "La propriété preserveAspectRatio a été ajoutée à $file"
    else
        echo "La propriété preserveAspectRatio existe déjà dans $file"
    fi
done


