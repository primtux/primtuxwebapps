// Afficher la galerie d'aperçus
function affiche(type_licence,theme,variante,premiereFois) {
    let images = [];
    let noms_fichiers;
  
    if (type_galerie===fonds){noms_fichiers=liste_fonds}
    else if (type_galerie===personnages)  {noms_fichiers=liste_personnages}
    else if (type_galerie===objets)  {noms_fichiers=liste_objets}
  
    if (premiereFois){ // Création des balises img   
      noms_fichiers.forEach(element => {
        let image = document.createElement('img');
        image.classList.add('apercu');
        image.draggable="true";
        image.style.display='none';
        galerie_images.appendChild(image);
        images.push(image);
      });
  
    } else { // Masquage de toutes les images
      images=document.querySelectorAll('.apercu');
      images.forEach(function(image) {
        image.style.display='none';
      });
    }  
    
    // Mise à jour des chemins des fichiers image et des classes 'fond' / 'personnage'    
    let debut='';
  
    //if (forge){
    //  debut = 'https://forge.aeif.fr/achampollion/autobd/-/raw/main/';
    //}    
    
    if (theme==='Personnalisés'){
      console.log("personnalisés")
      let fondsPersos=galerie_images.querySelectorAll('.perso');
      fondsPersos.forEach(fond => {
        fond.style.display='inline-block';
      });
    } else {

      let i=0;
      images.forEach(image => {
        if (i>=nb_fonds_perso){
          if (type_galerie===fonds){
            image.src=debut+'contenus/fonds/'+theme+'/'+variante+'/reduites/'+noms_fichiers[i-nb_fonds_perso]+'.jpeg';
            image.classList.remove('personnage');
            image.classList.add('fond');
          }
          else if (type_galerie===personnages){
            image.src=debut+'contenus/personnages/'+theme+'/'+variante+'/'+noms_fichiers[i-nb_fonds_perso];
            image.classList.remove('fond');
            image.classList.add('personnage');
          }
          else if (type_galerie===objets){
            image.src=debut+'contenus/objets/'+theme+'/'+variante+'/'+noms_fichiers[i-nb_fonds_perso];
            image.classList.remove('fond');
            image.classList.add('personnage');
          }
        }    
          i+=1;      
          let tentativesRestantes = 3; // Nombre de tentatives
    
          function chargerImage() {
            image.onload = function() {
              image.style.display = 'inline-block';
              image.themecredit = type_licence;
            };
            image.onerror = function() {        
              if (tentativesRestantes > 0) {
                tentativesRestantes--;
                setTimeout(chargerImage, 2000); // Recharge après 2 secondes
              }
            };  
          }

          chargerImage();
      });
    }
  
    // Mise à jour de la hauteur de la div galerie
    galerie_images.style.height=(galerie.offsetHeight-choix.offsetHeight-zone_gros_boutons.offsetHeight-30)+'px';

}

// Mettre à jour les contrôles lors du clic sur un objet
function maj_controles(objet){

    let enfant = objet.querySelector('p');
    let styles_conteneur=window.getComputedStyle(objet);
    let styles_enfant=window.getComputedStyle(enfant);
    let couleur_fond;
  
    if (objet.classList.contains('onomatopee')){
      couleur_fond=rgbToHex(styles_enfant.WebkitTextStrokeColor);
    } else if (objet.classList.contains('colere')){
      let image=objet.querySelector('svg');
      let chemin=image.getElementById('chemin_colere');
      couleur_fond=rgbToHex(chemin.style.fill);    
    } else {
      couleur_fond=rgbToHex(styles_conteneur.backgroundColor);
    }
  
    let couleur_texte = rgbToHex(styles_enfant.color);
    let taille_police = parseFloat(styles_enfant.fontSize);
    let type_police = styles_enfant.fontFamily;
  
    // Mise à jour des contrôles
    fond_couleur.value=couleur_fond;
    police_couleur.value = couleur_texte;
    police_taille.value = taille_police;
    police_choix.value = type_police;
}


// Déplacer verticalement les objets pour tenir compte de la hauteur du titre
function ajuste(hauteur) {
    // Sélectionnez tous les éléments ayant la classe 'draggable' dans la div avec l'ID 'bd'
    let elements = document.querySelectorAll('#bd > .draggable');
        // Parcourez chaque élément et déplacez-le vers le bas
    elements.forEach(function(element) {
        let currentPosition = window.getComputedStyle(element).getPropertyValue('top');
        let currentTop = parseInt(currentPosition) || 0;
        element.style.top = (currentTop + hauteur) + 'px';
    });
}

function change_variantes(option,premiereFois){  
    if (type_galerie===personnages){
      parmetresUrl('personnage', input_themes.value);
    }
    else if (type_galerie===fonds){
      parmetresUrl('fond', input_themes.value);
    }
    if (type_galerie===objets){
      parmetresUrl('objet', input_themes.value);
    }
    input_variantes.innerHTML='';
    let tour=0;
    let variante;
    let index=null;
    type_galerie.forEach(function(ligne){ 
  
      if (ligne[0]===option){
        index=tour;  
      }
      tour+=1;
    });
  
  
  
    if (index!=null && type_galerie[index].length>3){
      let nbPositions = type_galerie[index].length-3;
      for (let i = 0; i < nbPositions; i++) {
        let newOption = document.createElement('option');
        newOption.value=i+1;
        newOption.innerHTML=type_galerie[index][i+3];
        input_variantes.appendChild(newOption);
      }    
      input_variantes.value = 1;
      input_variantes.style.display=null;
  
    } else {
      input_variantes.style.display="none";    
    }
    variante = 1;
    licence = type_galerie[index][2];
    affiche(licence,option,variante,premiereFois);
  }
  
  
  function nouveau(){
      bd.innerHTML=contenu_nouveau;
      // Reconstitution des listes de bandes et vignettes
      creeElementsDom();
      restaure_listes();    
      raz_globales();
      liste_credits=[];
      liste_credits_pour_paragraphe=[];
      liste_credits_polices_pour_paragraphe=[];
      update_boutons();
      // Sauvegarde
      sauvegarde();
  }

  function maj_credits(objet,mode){
    console.log('maj_credits '+objet+' '+mode);
    if (objet){    
      let draggedClasses = objet.classList;
      console.log('classes '+draggedClasses);
      for (let i = 0; i < credits.length; i++) {        
          let classeToCheck = credits[i][0];        
          
          if (draggedClasses.contains(classeToCheck)) { // Test si l'objet est soumis à licence
            
            if (mode==='suppr'){
              let indexASupprimer = liste_credits.indexOf(credits[i][0]);
              if (indexASupprimer !== -1) {
                liste_credits.splice(indexASupprimer, 1); // Suppression d'une occurence dans la liste des crédits
              }
            
            } else {
              console.log("ajout de "+credits[i][0])
              liste_credits.push(credits[i][0]); // Ajout d'une occurence dans la liste des crédits
            }
  
            if (compterOccurrences(credits[i][0],liste_credits)===0) {
              let index=liste_credits_pour_paragraphe.indexOf(credits[i][0]);
              liste_credits_pour_paragraphe.splice(index,1); // Suppression du texte de licence            
            } else if (compterOccurrences(credits[i][0],liste_credits)===1 && mode!='suppr'){
              liste_credits_pour_paragraphe.push(credits[i][1]); // Ajout du texte de licence 
            }         
            console.log("paragraphe "+liste_credits_pour_paragraphe)
            // Mise à jour du paragraphe
            images_credits.innerText=liste_credits_pour_paragraphe.join('\n');
            
            break;  
          }
      }
    }
  }
  
  function maj_credits_police(){
    const paragraphes = bd.querySelectorAll('p:not(.hide):not(.hide p)');
    console.log(paragraphes.length+' paragraphes');
    console.log(paragraphes);
    let liste_polices_utilisees=[];
  
    // Récupération de toutes les polices utilisées
    paragraphes.forEach(paragraphe => {
      let police = window.getComputedStyle(paragraphe).fontFamily;
      credits.forEach(ligne => {
        if (ligne[0]===police){
          if (!liste_polices_utilisees.includes(police)){
            liste_polices_utilisees.push(police);
          }
        }
      });
    });
    
    liste_credits_polices_pour_paragraphe=[];
    credits.forEach(element =>{
      if (liste_polices_utilisees.includes(element[0])){
        liste_credits_polices_pour_paragraphe.push(element[1]);
      }
    });
  
  
    // Mise à jour des crédits
    polices_credits.innerText=liste_credits_polices_pour_paragraphe.join('\n');
  
  
  }


  // On affiche les thèmes selon le type de galerie (fonds, personnages, objets)
function change_galerie(liste,bouton,premiereFois){
  console.log('change_galeries '+liste.choix)
  if (liste===personnages){
    parmetresUrl('type_galerie', 'personnages');
  }
  else if (liste===fonds){
    parmetresUrl('type_galerie', 'fonds');
  }
  else if (liste===objets){
    parmetresUrl('type_galerie', 'objets');
  }
  let boutons=document.querySelectorAll('.gros_bouton');
  boutons.forEach(function(element){
    element.classList.remove('actif');
  });
  bouton.classList.add('actif');
  type_galerie=liste;

  galeries.innerHTML='';
  liste.forEach(function(element){
    let option=document.createElement('option');
    option.value=element[0];
    option.innerHTML=element[1];
    input_themes.appendChild(option);
  });
  input_themes.value=liste.choix;
  change_variantes(liste.choix,premiereFois); 
}