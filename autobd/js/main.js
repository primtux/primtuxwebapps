// On vérifie si on est sur la forge
let url_complete = window.location.href;
let forge=false;
if (url_complete.startsWith('https://achampollion.forge.aeif.fr')) {
  forge=true;
}

// On vérifie si des paramètres sont dans l'URL
let url = window.location.search;
let urlParams = new URLSearchParams(url);
if (urlParams.get('personnage')) {personnages.choix=urlParams.get('personnage');}
else {personnages.choix=tirerAuSort(personnages)[0];}
if (urlParams.get('fond') && urlParams.get('fond')!='Personnalisés' ) {fonds.choix=urlParams.get('fond');}
else {fonds.choix=tirerAuSort(fonds)[0];}
if (urlParams.get('objet')) {objets.choix=urlParams.get('objet');}
else {objets.choix=tirerAuSort(objets)[0];}
if (urlParams.get('type_galerie')) {
  type_galerie_url=urlParams.get('type_galerie')
  if (type_galerie_url==='personnages'){type_galerie=personnages;bouton_actif = bouton_personnages;}
  else if (type_galerie_url==='fonds'){type_galerie=fonds;bouton_actif = bouton_fonds;}
  else if (type_galerie_url==='objets'){type_galerie=objets;bouton_actif = bouton_objets;}
  else {type_galerie=personnages;}
} else {type_galerie=personnages;bouton_actif = bouton_personnages;}
{
  barre_outils.style.paddingLeft='8rem';
}

// On bascule sur l'onglet  fonds, personnages ou objets
change_galerie (type_galerie,bouton_actif,true);

// On démarre l'application avec une bande de 3 vignettes et on sauvegarde cet état.
cree_bande(3,true);
contenu_nouveau=bd.innerHTML;

// On vérifie l'existence d'une sauvegarde locale
if (localStorage.getItem('sauvegardesJson')){
  console.log("coucou")
  // Récupérer la chaîne JSON du stockage local
  let sauvegardesJson = localStorage.getItem('sauvegardesJson');

  // Convertir la chaîne JSON en tableau
  liste_sauvegarde_cache = JSON.parse(sauvegardesJson);

  // Utiliser le tableau récupéré
  bd.innerHTML=liste_sauvegarde_cache[0];
  liste_credits=liste_sauvegarde_cache[1].slice();
  liste_credits_pour_paragraphe=liste_sauvegarde_cache[2].slice();
  liste_credits_polices_pour_paragraphe=liste_sauvegarde_cache[3].slice();
  creeElementsDom();
  restaure_listes();    
  raz_globales();  
  update_boutons();
  maj_credits();
  sauvegarde();
}
