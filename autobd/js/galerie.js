// Structure de la galerie
  // Nom du dossier, Nom affiché, Crédits auteur, Thèmes...
  fonds = [
    ['Abel_Bellina','Abel & Bellina','Abel_Bellina'],
    ['Ada','Ada & Zangemann','Ada_Zangemann'],
    ['Jous','Le Monde des Jous','Jous'],
    ['École','École','Bing'],
    ['Fantastique','Fantastique','Bing'],
    ['Hôpital','Hôpital','Bing'],
    ['Maison','Maison','Bing'],
    ['Montagne','Montagne','Bing'],
    ['Plage','Plage','Bing'],
    ["Primtux","Primtux",'Primtux'],
    ['Restaurant','Restaurant','Bing'],
    ['Science_fiction','Science fiction','Bing'],
    ['Travail','Travail','Bing'],
    ['Village','Village','Bing'],
    ['Ville','Ville','Bing'],
  ]
  // Nom du dossier, Nom affiché, Crédits auteur, Thèmes...
  personnages = [
    ['Abel','Abel','Abel_Bellina','Série 1','Série 2','Série 3','Série 4'],
    ['Abel_Bellina','Abel & Bellina','Abel_Bellina'],
    ['Abby','Abby The Pup','Abby'],
    ['Ada_Zangemann','Ada & Zangemann','Ada_Zangemann'],
    ["Alexia","Alexia",'Ramya_Mylavarapu',"Doigt levé","Porte un livre"],
    ["Benjamin","Benjamin",'svgrepo',"Face","Trois-quarts"],
    ['Bellina','Bellina','Abel_Bellina','Série 1','Série 2','Série 3','Série 4'],
    ['Brigit_Komit','Brigit & Komit','Brigit_Komit'],
    ["Caroline","Caroline","svgrepo","Face","Trois-quarts"],
    ["Clémence","Clémence","Ramya_Mylavarapu","Doigt levé","Porte un livre"],
    ["Divers","Divers","svgrepo"],
    ["Jous","Jous","Jous"],
    ["Léopold","Léopold",'Devarani_B',"Doigt levé","Tient une baguette"],
    ["Lily","Lily",'Bing'],
    ["Manon","Manon",'Bing'],
    ["Mathilde","Mathilde",'Bing'],
    ["Max","Max",'Bing'],
    ["Ours","Ours",'Bing'],
    ["Primtux","Primtux",'Primtux'],
    ["Tasse","Tasse",'Ramya_Mylavarapu',"Doigt pointé","Bras écartés","Journal"],
    ["Tux","Tux",'Tux']
  ]
  // Nom du dossier, Nom affiché, Crédits auteur, Thèmes...
    objets = [
      ['Abel_Bellina','Abel & Bellina','Abel_Bellina']
    ]
  // Noms polices
    polices = [
      "IndieFlower","FuntypeRegular","ThatNogoFontCasual","Kaph","Teleindicadore","Segment"
    ]
  // Référence, texte à afficher
    credits = [
      ['Abel_Bellina','Dessins Abel & Bellina par Odysseus, licence Art Libre'],
      ['Abby','Dessins Feelings par Kulsoom Ayyaz, licence Public Domain'],
      ['Jous','Dessins des Jous par Arnaud Champollion, licence CC-BY-SA-4.0'],
      ['Ada_Zangemann','Dessins Ada & Zangemann par Sandra Brandstätter, licence CC-BY-SA-3.0-DE'],
      ['Brigit_Komit','Dessins de Brigit & Komit par Juliette Taka, licence CC BY.'],  
      ["svgrepo","Dessins SVGRepo"],
      ["Bing","Images générées par IA sur Bing"],
      ["Ramya_Mylavarapu","Dessins par Ramya Mylavarapu sur gramener.com/comicgen, licence CC0"],
      ['Devarani_B','Dessins par Devarani B sur gramener.com/comicgen, licence CC0'],
      ['Tux',"Dessin de Tux original par Larry Ewing, version crystal par Everaldo Coelho"],
      ["IndieFlower","Police de caractère Indie Flower par Kimberly Geswein, licence OFL"],
      ["FuntypeRegular","Police de caractère Fun Type Regular par Frank Baranowski, licence OFL"],
      ["ThatNogoFontCasual","Police de caractère That Nogo Font Casual par Kimberly Geswein, licence OFL"],
      ["Kaph","Police de caractère Kaph par GGBotNet, licence OFL"],
      ["Teleindicadore","Police de caractère Tele Indicadore sous Public Licence"],
      ["Segment","Police de caractère Segment par Paul Flo Williams, licence OFL"],
      ["Primtux","Dessins des personnages de Primtux par Romain Ronflette, licence CC BY 4.0"]
    ]

