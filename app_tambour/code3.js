gdjs.codeCode = {};
gdjs.codeCode.GDtexte_9595codeObjects1= [];
gdjs.codeCode.GDtexte_9595codeObjects2= [];
gdjs.codeCode.GDtexte_9595codeObjects3= [];
gdjs.codeCode.GDbouton_95951Objects1= [];
gdjs.codeCode.GDbouton_95951Objects2= [];
gdjs.codeCode.GDbouton_95951Objects3= [];
gdjs.codeCode.GDbouton_95952Objects1= [];
gdjs.codeCode.GDbouton_95952Objects2= [];
gdjs.codeCode.GDbouton_95952Objects3= [];
gdjs.codeCode.GDbouton_95953Objects1= [];
gdjs.codeCode.GDbouton_95953Objects2= [];
gdjs.codeCode.GDbouton_95953Objects3= [];
gdjs.codeCode.GDbouton_95954Objects1= [];
gdjs.codeCode.GDbouton_95954Objects2= [];
gdjs.codeCode.GDbouton_95954Objects3= [];
gdjs.codeCode.GDbouton_95955Objects1= [];
gdjs.codeCode.GDbouton_95955Objects2= [];
gdjs.codeCode.GDbouton_95955Objects3= [];
gdjs.codeCode.GDtambour_9595mini2Objects1= [];
gdjs.codeCode.GDtambour_9595mini2Objects2= [];
gdjs.codeCode.GDtambour_9595mini2Objects3= [];
gdjs.codeCode.GDtambour_9595miniObjects1= [];
gdjs.codeCode.GDtambour_9595miniObjects2= [];
gdjs.codeCode.GDtambour_9595miniObjects3= [];
gdjs.codeCode.GDtambourObjects1= [];
gdjs.codeCode.GDtambourObjects2= [];
gdjs.codeCode.GDtambourObjects3= [];
gdjs.codeCode.GDecouterObjects1= [];
gdjs.codeCode.GDecouterObjects2= [];
gdjs.codeCode.GDecouterObjects3= [];
gdjs.codeCode.GDbouton_9595retourObjects1= [];
gdjs.codeCode.GDbouton_9595retourObjects2= [];
gdjs.codeCode.GDbouton_9595retourObjects3= [];
gdjs.codeCode.GDtxt_9595versionObjects1= [];
gdjs.codeCode.GDtxt_9595versionObjects2= [];
gdjs.codeCode.GDtxt_9595versionObjects3= [];
gdjs.codeCode.GDfond2Objects1= [];
gdjs.codeCode.GDfond2Objects2= [];
gdjs.codeCode.GDfond2Objects3= [];
gdjs.codeCode.GDfondObjects1= [];
gdjs.codeCode.GDfondObjects2= [];
gdjs.codeCode.GDfondObjects3= [];
gdjs.codeCode.GDfleche_9595clignotanteObjects1= [];
gdjs.codeCode.GDfleche_9595clignotanteObjects2= [];
gdjs.codeCode.GDfleche_9595clignotanteObjects3= [];
gdjs.codeCode.GDbulle_9595penseeObjects1= [];
gdjs.codeCode.GDbulle_9595penseeObjects2= [];
gdjs.codeCode.GDbulle_9595penseeObjects3= [];
gdjs.codeCode.GDbulle_9595chiffreObjects1= [];
gdjs.codeCode.GDbulle_9595chiffreObjects2= [];
gdjs.codeCode.GDbulle_9595chiffreObjects3= [];
gdjs.codeCode.GDfond_9595musiqueObjects1= [];
gdjs.codeCode.GDfond_9595musiqueObjects2= [];
gdjs.codeCode.GDfond_9595musiqueObjects3= [];
gdjs.codeCode.GDaideObjects1= [];
gdjs.codeCode.GDaideObjects2= [];
gdjs.codeCode.GDaideObjects3= [];
gdjs.codeCode.GDscore1Objects1= [];
gdjs.codeCode.GDscore1Objects2= [];
gdjs.codeCode.GDscore1Objects3= [];
gdjs.codeCode.GDscore2Objects1= [];
gdjs.codeCode.GDscore2Objects2= [];
gdjs.codeCode.GDscore2Objects3= [];
gdjs.codeCode.GDbouton_9595optionsObjects1= [];
gdjs.codeCode.GDbouton_9595optionsObjects2= [];
gdjs.codeCode.GDbouton_9595optionsObjects3= [];
gdjs.codeCode.GDlamaObjects1= [];
gdjs.codeCode.GDlamaObjects2= [];
gdjs.codeCode.GDlamaObjects3= [];
gdjs.codeCode.GDpointsObjects1= [];
gdjs.codeCode.GDpointsObjects2= [];
gdjs.codeCode.GDpointsObjects3= [];
gdjs.codeCode.GDdrapeau1Objects1= [];
gdjs.codeCode.GDdrapeau1Objects2= [];
gdjs.codeCode.GDdrapeau1Objects3= [];
gdjs.codeCode.GDdrapeau2Objects1= [];
gdjs.codeCode.GDdrapeau2Objects2= [];
gdjs.codeCode.GDdrapeau2Objects3= [];
gdjs.codeCode.GDsergeObjects1= [];
gdjs.codeCode.GDsergeObjects2= [];
gdjs.codeCode.GDsergeObjects3= [];


gdjs.codeCode.mapOfGDgdjs_9546codeCode_9546GDbouton_959595953Objects1Objects = Hashtable.newFrom({"bouton_3": gdjs.codeCode.GDbouton_95953Objects1});
gdjs.codeCode.eventsList0 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) != 0;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(12637140);
}
}
if (isConditionTrue_0) {
{runtimeScene.getScene().getVariables().getFromIndex(1).setNumber(0);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 0;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(12638292);
}
}
if (isConditionTrue_0) {
{runtimeScene.getScene().getVariables().getFromIndex(1).add(1);
}}

}


};gdjs.codeCode.mapOfGDgdjs_9546codeCode_9546GDbouton_959595955Objects1Objects = Hashtable.newFrom({"bouton_5": gdjs.codeCode.GDbouton_95955Objects1});
gdjs.codeCode.eventsList1 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) != 1;
if (isConditionTrue_0) {
{runtimeScene.getScene().getVariables().getFromIndex(1).setNumber(0);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 1;
if (isConditionTrue_0) {
{runtimeScene.getScene().getVariables().getFromIndex(1).add(1);
}}

}


};gdjs.codeCode.mapOfGDgdjs_9546codeCode_9546GDbouton_959595952Objects1Objects = Hashtable.newFrom({"bouton_2": gdjs.codeCode.GDbouton_95952Objects1});
gdjs.codeCode.eventsList2 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) != 2;
if (isConditionTrue_0) {
{runtimeScene.getScene().getVariables().getFromIndex(1).setNumber(0);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 2;
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "options", false);
}}

}


};gdjs.codeCode.mapOfGDgdjs_9546codeCode_9546GDbouton_959595951Objects1Objects = Hashtable.newFrom({"bouton_1": gdjs.codeCode.GDbouton_95951Objects1});
gdjs.codeCode.mapOfGDgdjs_9546codeCode_9546GDbouton_95959595retourObjects1Objects = Hashtable.newFrom({"bouton_retour": gdjs.codeCode.GDbouton_9595retourObjects1});
gdjs.codeCode.mapOfGDgdjs_9546codeCode_9546GDbouton_959595951Objects1ObjectsGDgdjs_9546codeCode_9546GDbouton_959595952Objects1ObjectsGDgdjs_9546codeCode_9546GDbouton_959595953Objects1ObjectsGDgdjs_9546codeCode_9546GDbouton_959595954Objects1ObjectsGDgdjs_9546codeCode_9546GDbouton_959595955Objects1Objects = Hashtable.newFrom({"bouton_1": gdjs.codeCode.GDbouton_95951Objects1, "bouton_2": gdjs.codeCode.GDbouton_95952Objects1, "bouton_3": gdjs.codeCode.GDbouton_95953Objects1, "bouton_4": gdjs.codeCode.GDbouton_95954Objects1, "bouton_5": gdjs.codeCode.GDbouton_95955Objects1});
gdjs.codeCode.eventsList3 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
{runtimeScene.getScene().getVariables().getFromIndex(1).setNumber(0);
}{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/audio_vide.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_3"), gdjs.codeCode.GDbouton_95953Objects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.codeCode.mapOfGDgdjs_9546codeCode_9546GDbouton_959595953Objects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(12636540);
}
}
}
if (isConditionTrue_0) {

{ //Subevents
gdjs.codeCode.eventsList0(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_5"), gdjs.codeCode.GDbouton_95955Objects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.codeCode.mapOfGDgdjs_9546codeCode_9546GDbouton_959595955Objects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(12639588);
}
}
}
if (isConditionTrue_0) {

{ //Subevents
gdjs.codeCode.eventsList1(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_2"), gdjs.codeCode.GDbouton_95952Objects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.codeCode.mapOfGDgdjs_9546codeCode_9546GDbouton_959595952Objects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(12641900);
}
}
}
if (isConditionTrue_0) {

{ //Subevents
gdjs.codeCode.eventsList2(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_1"), gdjs.codeCode.GDbouton_95951Objects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.codeCode.mapOfGDgdjs_9546codeCode_9546GDbouton_959595951Objects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(12644356);
}
}
}
if (isConditionTrue_0) {
{runtimeScene.getScene().getVariables().getFromIndex(1).setNumber(0);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_retour"), gdjs.codeCode.GDbouton_9595retourObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.codeCode.mapOfGDgdjs_9546codeCode_9546GDbouton_95959595retourObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "menu", false);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_1"), gdjs.codeCode.GDbouton_95951Objects1);
gdjs.copyArray(runtimeScene.getObjects("bouton_2"), gdjs.codeCode.GDbouton_95952Objects1);
gdjs.copyArray(runtimeScene.getObjects("bouton_3"), gdjs.codeCode.GDbouton_95953Objects1);
gdjs.copyArray(runtimeScene.getObjects("bouton_4"), gdjs.codeCode.GDbouton_95954Objects1);
gdjs.copyArray(runtimeScene.getObjects("bouton_5"), gdjs.codeCode.GDbouton_95955Objects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.codeCode.mapOfGDgdjs_9546codeCode_9546GDbouton_959595951Objects1ObjectsGDgdjs_9546codeCode_9546GDbouton_959595952Objects1ObjectsGDgdjs_9546codeCode_9546GDbouton_959595953Objects1ObjectsGDgdjs_9546codeCode_9546GDbouton_959595954Objects1ObjectsGDgdjs_9546codeCode_9546GDbouton_959595955Objects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(12646612);
}
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/bulle_1.mp3", 1, false, 100, 1);
}}

}


};

gdjs.codeCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.codeCode.GDtexte_9595codeObjects1.length = 0;
gdjs.codeCode.GDtexte_9595codeObjects2.length = 0;
gdjs.codeCode.GDtexte_9595codeObjects3.length = 0;
gdjs.codeCode.GDbouton_95951Objects1.length = 0;
gdjs.codeCode.GDbouton_95951Objects2.length = 0;
gdjs.codeCode.GDbouton_95951Objects3.length = 0;
gdjs.codeCode.GDbouton_95952Objects1.length = 0;
gdjs.codeCode.GDbouton_95952Objects2.length = 0;
gdjs.codeCode.GDbouton_95952Objects3.length = 0;
gdjs.codeCode.GDbouton_95953Objects1.length = 0;
gdjs.codeCode.GDbouton_95953Objects2.length = 0;
gdjs.codeCode.GDbouton_95953Objects3.length = 0;
gdjs.codeCode.GDbouton_95954Objects1.length = 0;
gdjs.codeCode.GDbouton_95954Objects2.length = 0;
gdjs.codeCode.GDbouton_95954Objects3.length = 0;
gdjs.codeCode.GDbouton_95955Objects1.length = 0;
gdjs.codeCode.GDbouton_95955Objects2.length = 0;
gdjs.codeCode.GDbouton_95955Objects3.length = 0;
gdjs.codeCode.GDtambour_9595mini2Objects1.length = 0;
gdjs.codeCode.GDtambour_9595mini2Objects2.length = 0;
gdjs.codeCode.GDtambour_9595mini2Objects3.length = 0;
gdjs.codeCode.GDtambour_9595miniObjects1.length = 0;
gdjs.codeCode.GDtambour_9595miniObjects2.length = 0;
gdjs.codeCode.GDtambour_9595miniObjects3.length = 0;
gdjs.codeCode.GDtambourObjects1.length = 0;
gdjs.codeCode.GDtambourObjects2.length = 0;
gdjs.codeCode.GDtambourObjects3.length = 0;
gdjs.codeCode.GDecouterObjects1.length = 0;
gdjs.codeCode.GDecouterObjects2.length = 0;
gdjs.codeCode.GDecouterObjects3.length = 0;
gdjs.codeCode.GDbouton_9595retourObjects1.length = 0;
gdjs.codeCode.GDbouton_9595retourObjects2.length = 0;
gdjs.codeCode.GDbouton_9595retourObjects3.length = 0;
gdjs.codeCode.GDtxt_9595versionObjects1.length = 0;
gdjs.codeCode.GDtxt_9595versionObjects2.length = 0;
gdjs.codeCode.GDtxt_9595versionObjects3.length = 0;
gdjs.codeCode.GDfond2Objects1.length = 0;
gdjs.codeCode.GDfond2Objects2.length = 0;
gdjs.codeCode.GDfond2Objects3.length = 0;
gdjs.codeCode.GDfondObjects1.length = 0;
gdjs.codeCode.GDfondObjects2.length = 0;
gdjs.codeCode.GDfondObjects3.length = 0;
gdjs.codeCode.GDfleche_9595clignotanteObjects1.length = 0;
gdjs.codeCode.GDfleche_9595clignotanteObjects2.length = 0;
gdjs.codeCode.GDfleche_9595clignotanteObjects3.length = 0;
gdjs.codeCode.GDbulle_9595penseeObjects1.length = 0;
gdjs.codeCode.GDbulle_9595penseeObjects2.length = 0;
gdjs.codeCode.GDbulle_9595penseeObjects3.length = 0;
gdjs.codeCode.GDbulle_9595chiffreObjects1.length = 0;
gdjs.codeCode.GDbulle_9595chiffreObjects2.length = 0;
gdjs.codeCode.GDbulle_9595chiffreObjects3.length = 0;
gdjs.codeCode.GDfond_9595musiqueObjects1.length = 0;
gdjs.codeCode.GDfond_9595musiqueObjects2.length = 0;
gdjs.codeCode.GDfond_9595musiqueObjects3.length = 0;
gdjs.codeCode.GDaideObjects1.length = 0;
gdjs.codeCode.GDaideObjects2.length = 0;
gdjs.codeCode.GDaideObjects3.length = 0;
gdjs.codeCode.GDscore1Objects1.length = 0;
gdjs.codeCode.GDscore1Objects2.length = 0;
gdjs.codeCode.GDscore1Objects3.length = 0;
gdjs.codeCode.GDscore2Objects1.length = 0;
gdjs.codeCode.GDscore2Objects2.length = 0;
gdjs.codeCode.GDscore2Objects3.length = 0;
gdjs.codeCode.GDbouton_9595optionsObjects1.length = 0;
gdjs.codeCode.GDbouton_9595optionsObjects2.length = 0;
gdjs.codeCode.GDbouton_9595optionsObjects3.length = 0;
gdjs.codeCode.GDlamaObjects1.length = 0;
gdjs.codeCode.GDlamaObjects2.length = 0;
gdjs.codeCode.GDlamaObjects3.length = 0;
gdjs.codeCode.GDpointsObjects1.length = 0;
gdjs.codeCode.GDpointsObjects2.length = 0;
gdjs.codeCode.GDpointsObjects3.length = 0;
gdjs.codeCode.GDdrapeau1Objects1.length = 0;
gdjs.codeCode.GDdrapeau1Objects2.length = 0;
gdjs.codeCode.GDdrapeau1Objects3.length = 0;
gdjs.codeCode.GDdrapeau2Objects1.length = 0;
gdjs.codeCode.GDdrapeau2Objects2.length = 0;
gdjs.codeCode.GDdrapeau2Objects3.length = 0;
gdjs.codeCode.GDsergeObjects1.length = 0;
gdjs.codeCode.GDsergeObjects2.length = 0;
gdjs.codeCode.GDsergeObjects3.length = 0;

gdjs.codeCode.eventsList3(runtimeScene);

return;

}

gdjs['codeCode'] = gdjs.codeCode;
