gdjs.optionsCode = {};
gdjs.optionsCode.GDtxt_9595intervalle_95951Objects1= [];
gdjs.optionsCode.GDtxt_9595intervalle_95951Objects2= [];
gdjs.optionsCode.GDtxt_9595intervalle_95951Objects3= [];
gdjs.optionsCode.GDtxt_9595intervalle_95952Objects1= [];
gdjs.optionsCode.GDtxt_9595intervalle_95952Objects2= [];
gdjs.optionsCode.GDtxt_9595intervalle_95952Objects3= [];
gdjs.optionsCode.GDNewBBTextObjects1= [];
gdjs.optionsCode.GDNewBBTextObjects2= [];
gdjs.optionsCode.GDNewBBTextObjects3= [];
gdjs.optionsCode.GDnb_9595miniObjects1= [];
gdjs.optionsCode.GDnb_9595miniObjects2= [];
gdjs.optionsCode.GDnb_9595miniObjects3= [];
gdjs.optionsCode.GDnb_9595maxiObjects1= [];
gdjs.optionsCode.GDnb_9595maxiObjects2= [];
gdjs.optionsCode.GDnb_9595maxiObjects3= [];
gdjs.optionsCode.GDbouton_9595plusObjects1= [];
gdjs.optionsCode.GDbouton_9595plusObjects2= [];
gdjs.optionsCode.GDbouton_9595plusObjects3= [];
gdjs.optionsCode.GDbouton_9595plus2Objects1= [];
gdjs.optionsCode.GDbouton_9595plus2Objects2= [];
gdjs.optionsCode.GDbouton_9595plus2Objects3= [];
gdjs.optionsCode.GDbouton_9595moinsObjects1= [];
gdjs.optionsCode.GDbouton_9595moinsObjects2= [];
gdjs.optionsCode.GDbouton_9595moinsObjects3= [];
gdjs.optionsCode.GDbouton_9595moins2Objects1= [];
gdjs.optionsCode.GDbouton_9595moins2Objects2= [];
gdjs.optionsCode.GDbouton_9595moins2Objects3= [];
gdjs.optionsCode.GDtambour_9595mini2Objects1= [];
gdjs.optionsCode.GDtambour_9595mini2Objects2= [];
gdjs.optionsCode.GDtambour_9595mini2Objects3= [];
gdjs.optionsCode.GDtambour_9595miniObjects1= [];
gdjs.optionsCode.GDtambour_9595miniObjects2= [];
gdjs.optionsCode.GDtambour_9595miniObjects3= [];
gdjs.optionsCode.GDtambourObjects1= [];
gdjs.optionsCode.GDtambourObjects2= [];
gdjs.optionsCode.GDtambourObjects3= [];
gdjs.optionsCode.GDecouterObjects1= [];
gdjs.optionsCode.GDecouterObjects2= [];
gdjs.optionsCode.GDecouterObjects3= [];
gdjs.optionsCode.GDbouton_9595retourObjects1= [];
gdjs.optionsCode.GDbouton_9595retourObjects2= [];
gdjs.optionsCode.GDbouton_9595retourObjects3= [];
gdjs.optionsCode.GDtxt_9595versionObjects1= [];
gdjs.optionsCode.GDtxt_9595versionObjects2= [];
gdjs.optionsCode.GDtxt_9595versionObjects3= [];
gdjs.optionsCode.GDfond2Objects1= [];
gdjs.optionsCode.GDfond2Objects2= [];
gdjs.optionsCode.GDfond2Objects3= [];
gdjs.optionsCode.GDfondObjects1= [];
gdjs.optionsCode.GDfondObjects2= [];
gdjs.optionsCode.GDfondObjects3= [];
gdjs.optionsCode.GDfleche_9595clignotanteObjects1= [];
gdjs.optionsCode.GDfleche_9595clignotanteObjects2= [];
gdjs.optionsCode.GDfleche_9595clignotanteObjects3= [];
gdjs.optionsCode.GDbulle_9595penseeObjects1= [];
gdjs.optionsCode.GDbulle_9595penseeObjects2= [];
gdjs.optionsCode.GDbulle_9595penseeObjects3= [];
gdjs.optionsCode.GDbulle_9595chiffreObjects1= [];
gdjs.optionsCode.GDbulle_9595chiffreObjects2= [];
gdjs.optionsCode.GDbulle_9595chiffreObjects3= [];
gdjs.optionsCode.GDfond_9595musiqueObjects1= [];
gdjs.optionsCode.GDfond_9595musiqueObjects2= [];
gdjs.optionsCode.GDfond_9595musiqueObjects3= [];
gdjs.optionsCode.GDaideObjects1= [];
gdjs.optionsCode.GDaideObjects2= [];
gdjs.optionsCode.GDaideObjects3= [];
gdjs.optionsCode.GDscore1Objects1= [];
gdjs.optionsCode.GDscore1Objects2= [];
gdjs.optionsCode.GDscore1Objects3= [];
gdjs.optionsCode.GDscore2Objects1= [];
gdjs.optionsCode.GDscore2Objects2= [];
gdjs.optionsCode.GDscore2Objects3= [];
gdjs.optionsCode.GDbouton_9595optionsObjects1= [];
gdjs.optionsCode.GDbouton_9595optionsObjects2= [];
gdjs.optionsCode.GDbouton_9595optionsObjects3= [];
gdjs.optionsCode.GDlamaObjects1= [];
gdjs.optionsCode.GDlamaObjects2= [];
gdjs.optionsCode.GDlamaObjects3= [];
gdjs.optionsCode.GDpointsObjects1= [];
gdjs.optionsCode.GDpointsObjects2= [];
gdjs.optionsCode.GDpointsObjects3= [];
gdjs.optionsCode.GDdrapeau1Objects1= [];
gdjs.optionsCode.GDdrapeau1Objects2= [];
gdjs.optionsCode.GDdrapeau1Objects3= [];
gdjs.optionsCode.GDdrapeau2Objects1= [];
gdjs.optionsCode.GDdrapeau2Objects2= [];
gdjs.optionsCode.GDdrapeau2Objects3= [];
gdjs.optionsCode.GDsergeObjects1= [];
gdjs.optionsCode.GDsergeObjects2= [];
gdjs.optionsCode.GDsergeObjects3= [];


gdjs.optionsCode.mapOfGDgdjs_9546optionsCode_9546GDbouton_95959595retourObjects1Objects = Hashtable.newFrom({"bouton_retour": gdjs.optionsCode.GDbouton_9595retourObjects1});
gdjs.optionsCode.eventsList0 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
{gdjs.evtTools.storage.clearJSONFile("sauvegarde_tambourbattant");
}{gdjs.evtTools.storage.writeStringInJSONFile("sauvegarde_tambourbattant", "reglages", gdjs.evtTools.network.variableStructureToJSON(runtimeScene.getGame().getVariables().getFromIndex(2)));
}}

}


};gdjs.optionsCode.eventsList1 = function(runtimeScene) {

{


gdjs.optionsCode.eventsList0(runtimeScene);
}


{


let isConditionTrue_0 = false;
{
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "menu", false);
}}

}


};gdjs.optionsCode.mapOfGDgdjs_9546optionsCode_9546GDbouton_95959595plusObjects1Objects = Hashtable.newFrom({"bouton_plus": gdjs.optionsCode.GDbouton_9595plusObjects1});
gdjs.optionsCode.eventsList2 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2).getChild("nb_coups_mini")) <= gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2).getChild("nb_coups_maxi")) - 5;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(2).getChild("nb_coups_mini").add(1);
}{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/bulle_1.mp3", 1, false, 100, 1);
}}

}


};gdjs.optionsCode.mapOfGDgdjs_9546optionsCode_9546GDbouton_95959595moinsObjects1Objects = Hashtable.newFrom({"bouton_moins": gdjs.optionsCode.GDbouton_9595moinsObjects1});
gdjs.optionsCode.eventsList3 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2).getChild("nb_coups_mini")) > 1;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(2).getChild("nb_coups_mini").sub(1);
}{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/bulle_1.mp3", 1, false, 100, 1);
}}

}


};gdjs.optionsCode.mapOfGDgdjs_9546optionsCode_9546GDbouton_95959595plus2Objects1Objects = Hashtable.newFrom({"bouton_plus2": gdjs.optionsCode.GDbouton_9595plus2Objects1});
gdjs.optionsCode.eventsList4 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2).getChild("nb_coups_maxi")) < 10;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(2).getChild("nb_coups_maxi").add(1);
}{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/bulle_1.mp3", 1, false, 100, 1);
}}

}


};gdjs.optionsCode.mapOfGDgdjs_9546optionsCode_9546GDbouton_95959595moins2Objects1Objects = Hashtable.newFrom({"bouton_moins2": gdjs.optionsCode.GDbouton_9595moins2Objects1});
gdjs.optionsCode.eventsList5 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2).getChild("nb_coups_maxi")) >= gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2).getChild("nb_coups_mini")) + 5;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(2).getChild("nb_coups_maxi").sub(1);
}{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/bulle_1.mp3", 1, false, 100, 1);
}}

}


};gdjs.optionsCode.eventsList6 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("fond2"), gdjs.optionsCode.GDfond2Objects1);
{for(var i = 0, len = gdjs.optionsCode.GDfond2Objects1.length ;i < len;++i) {
    gdjs.optionsCode.GDfond2Objects1[i].setOpacity(150);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_retour"), gdjs.optionsCode.GDbouton_9595retourObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.optionsCode.mapOfGDgdjs_9546optionsCode_9546GDbouton_95959595retourObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {

{ //Subevents
gdjs.optionsCode.eventsList1(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("nb_maxi"), gdjs.optionsCode.GDnb_9595maxiObjects1);
gdjs.copyArray(runtimeScene.getObjects("nb_mini"), gdjs.optionsCode.GDnb_9595miniObjects1);
{for(var i = 0, len = gdjs.optionsCode.GDnb_9595miniObjects1.length ;i < len;++i) {
    gdjs.optionsCode.GDnb_9595miniObjects1[i].setString(gdjs.evtTools.common.toString(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2).getChild("nb_coups_mini"))));
}
}{for(var i = 0, len = gdjs.optionsCode.GDnb_9595maxiObjects1.length ;i < len;++i) {
    gdjs.optionsCode.GDnb_9595maxiObjects1[i].setString(gdjs.evtTools.common.toString(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2).getChild("nb_coups_maxi"))));
}
}}

}


{



}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_plus"), gdjs.optionsCode.GDbouton_9595plusObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.optionsCode.mapOfGDgdjs_9546optionsCode_9546GDbouton_95959595plusObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(12679652);
}
}
}
if (isConditionTrue_0) {

{ //Subevents
gdjs.optionsCode.eventsList2(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_moins"), gdjs.optionsCode.GDbouton_9595moinsObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.optionsCode.mapOfGDgdjs_9546optionsCode_9546GDbouton_95959595moinsObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(12682700);
}
}
}
if (isConditionTrue_0) {

{ //Subevents
gdjs.optionsCode.eventsList3(runtimeScene);} //End of subevents
}

}


{



}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_plus2"), gdjs.optionsCode.GDbouton_9595plus2Objects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.optionsCode.mapOfGDgdjs_9546optionsCode_9546GDbouton_95959595plus2Objects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(12685276);
}
}
}
if (isConditionTrue_0) {

{ //Subevents
gdjs.optionsCode.eventsList4(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_moins2"), gdjs.optionsCode.GDbouton_9595moins2Objects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.optionsCode.mapOfGDgdjs_9546optionsCode_9546GDbouton_95959595moins2Objects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(12687652);
}
}
}
if (isConditionTrue_0) {

{ //Subevents
gdjs.optionsCode.eventsList5(runtimeScene);} //End of subevents
}

}


};

gdjs.optionsCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.optionsCode.GDtxt_9595intervalle_95951Objects1.length = 0;
gdjs.optionsCode.GDtxt_9595intervalle_95951Objects2.length = 0;
gdjs.optionsCode.GDtxt_9595intervalle_95951Objects3.length = 0;
gdjs.optionsCode.GDtxt_9595intervalle_95952Objects1.length = 0;
gdjs.optionsCode.GDtxt_9595intervalle_95952Objects2.length = 0;
gdjs.optionsCode.GDtxt_9595intervalle_95952Objects3.length = 0;
gdjs.optionsCode.GDNewBBTextObjects1.length = 0;
gdjs.optionsCode.GDNewBBTextObjects2.length = 0;
gdjs.optionsCode.GDNewBBTextObjects3.length = 0;
gdjs.optionsCode.GDnb_9595miniObjects1.length = 0;
gdjs.optionsCode.GDnb_9595miniObjects2.length = 0;
gdjs.optionsCode.GDnb_9595miniObjects3.length = 0;
gdjs.optionsCode.GDnb_9595maxiObjects1.length = 0;
gdjs.optionsCode.GDnb_9595maxiObjects2.length = 0;
gdjs.optionsCode.GDnb_9595maxiObjects3.length = 0;
gdjs.optionsCode.GDbouton_9595plusObjects1.length = 0;
gdjs.optionsCode.GDbouton_9595plusObjects2.length = 0;
gdjs.optionsCode.GDbouton_9595plusObjects3.length = 0;
gdjs.optionsCode.GDbouton_9595plus2Objects1.length = 0;
gdjs.optionsCode.GDbouton_9595plus2Objects2.length = 0;
gdjs.optionsCode.GDbouton_9595plus2Objects3.length = 0;
gdjs.optionsCode.GDbouton_9595moinsObjects1.length = 0;
gdjs.optionsCode.GDbouton_9595moinsObjects2.length = 0;
gdjs.optionsCode.GDbouton_9595moinsObjects3.length = 0;
gdjs.optionsCode.GDbouton_9595moins2Objects1.length = 0;
gdjs.optionsCode.GDbouton_9595moins2Objects2.length = 0;
gdjs.optionsCode.GDbouton_9595moins2Objects3.length = 0;
gdjs.optionsCode.GDtambour_9595mini2Objects1.length = 0;
gdjs.optionsCode.GDtambour_9595mini2Objects2.length = 0;
gdjs.optionsCode.GDtambour_9595mini2Objects3.length = 0;
gdjs.optionsCode.GDtambour_9595miniObjects1.length = 0;
gdjs.optionsCode.GDtambour_9595miniObjects2.length = 0;
gdjs.optionsCode.GDtambour_9595miniObjects3.length = 0;
gdjs.optionsCode.GDtambourObjects1.length = 0;
gdjs.optionsCode.GDtambourObjects2.length = 0;
gdjs.optionsCode.GDtambourObjects3.length = 0;
gdjs.optionsCode.GDecouterObjects1.length = 0;
gdjs.optionsCode.GDecouterObjects2.length = 0;
gdjs.optionsCode.GDecouterObjects3.length = 0;
gdjs.optionsCode.GDbouton_9595retourObjects1.length = 0;
gdjs.optionsCode.GDbouton_9595retourObjects2.length = 0;
gdjs.optionsCode.GDbouton_9595retourObjects3.length = 0;
gdjs.optionsCode.GDtxt_9595versionObjects1.length = 0;
gdjs.optionsCode.GDtxt_9595versionObjects2.length = 0;
gdjs.optionsCode.GDtxt_9595versionObjects3.length = 0;
gdjs.optionsCode.GDfond2Objects1.length = 0;
gdjs.optionsCode.GDfond2Objects2.length = 0;
gdjs.optionsCode.GDfond2Objects3.length = 0;
gdjs.optionsCode.GDfondObjects1.length = 0;
gdjs.optionsCode.GDfondObjects2.length = 0;
gdjs.optionsCode.GDfondObjects3.length = 0;
gdjs.optionsCode.GDfleche_9595clignotanteObjects1.length = 0;
gdjs.optionsCode.GDfleche_9595clignotanteObjects2.length = 0;
gdjs.optionsCode.GDfleche_9595clignotanteObjects3.length = 0;
gdjs.optionsCode.GDbulle_9595penseeObjects1.length = 0;
gdjs.optionsCode.GDbulle_9595penseeObjects2.length = 0;
gdjs.optionsCode.GDbulle_9595penseeObjects3.length = 0;
gdjs.optionsCode.GDbulle_9595chiffreObjects1.length = 0;
gdjs.optionsCode.GDbulle_9595chiffreObjects2.length = 0;
gdjs.optionsCode.GDbulle_9595chiffreObjects3.length = 0;
gdjs.optionsCode.GDfond_9595musiqueObjects1.length = 0;
gdjs.optionsCode.GDfond_9595musiqueObjects2.length = 0;
gdjs.optionsCode.GDfond_9595musiqueObjects3.length = 0;
gdjs.optionsCode.GDaideObjects1.length = 0;
gdjs.optionsCode.GDaideObjects2.length = 0;
gdjs.optionsCode.GDaideObjects3.length = 0;
gdjs.optionsCode.GDscore1Objects1.length = 0;
gdjs.optionsCode.GDscore1Objects2.length = 0;
gdjs.optionsCode.GDscore1Objects3.length = 0;
gdjs.optionsCode.GDscore2Objects1.length = 0;
gdjs.optionsCode.GDscore2Objects2.length = 0;
gdjs.optionsCode.GDscore2Objects3.length = 0;
gdjs.optionsCode.GDbouton_9595optionsObjects1.length = 0;
gdjs.optionsCode.GDbouton_9595optionsObjects2.length = 0;
gdjs.optionsCode.GDbouton_9595optionsObjects3.length = 0;
gdjs.optionsCode.GDlamaObjects1.length = 0;
gdjs.optionsCode.GDlamaObjects2.length = 0;
gdjs.optionsCode.GDlamaObjects3.length = 0;
gdjs.optionsCode.GDpointsObjects1.length = 0;
gdjs.optionsCode.GDpointsObjects2.length = 0;
gdjs.optionsCode.GDpointsObjects3.length = 0;
gdjs.optionsCode.GDdrapeau1Objects1.length = 0;
gdjs.optionsCode.GDdrapeau1Objects2.length = 0;
gdjs.optionsCode.GDdrapeau1Objects3.length = 0;
gdjs.optionsCode.GDdrapeau2Objects1.length = 0;
gdjs.optionsCode.GDdrapeau2Objects2.length = 0;
gdjs.optionsCode.GDdrapeau2Objects3.length = 0;
gdjs.optionsCode.GDsergeObjects1.length = 0;
gdjs.optionsCode.GDsergeObjects2.length = 0;
gdjs.optionsCode.GDsergeObjects3.length = 0;

gdjs.optionsCode.eventsList6(runtimeScene);

return;

}

gdjs['optionsCode'] = gdjs.optionsCode;
