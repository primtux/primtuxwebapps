"use strict"

const messages = {
  accueil: "<div class='consigne'>Pour commencer, tu dois choisir un exercice parmi les thèmes proposés.</div>",
  erreur: "<div class='messages'>Erreur !</div>",
  FinExo: "<div class='messages'>Tu as terminé l'exercice.</div><div class='consigne'>Tu peux choisir un autre exercice parmi les thèmes proposés.</div>",
  bravo: "<div class='messages'>Bravo !</div><div class='consigne'>Clique sur [suivant] ou appuie sur Entrée pour poursuivre.</div>",
  felicitations: "<div class='messages'>Félicitations, tu as réussi tout l'exercice !</div><div class='consigne'>Tu peux choisir un autre exercice parmi les thèmes proposés.</div>",
  phraseDouble: "<div class='consigne'>Recopie la partie de phrase en noir en respectant bien la ponctuation et les espaces.</div>",
  passer: "<div class='consigne'>Clique sur [suivant] ou appuie sur Entrée pour poursuivre.</div>",
  passerExo: "<div class='consigne'>Tu peux choisir un autre exercice parmi les thèmes proposés.</div>",
  erreur1: "<div class='messages'>Ce que tu as écrit n'est pas exact. As-tu bien recopié la partie en noir ?</div><div class='consigne'>Corrige puis clique à nouveau sur [Vérifier].",
  erreur2: "<div class='messages'>Ce n'est toujours pas exact.</div><div class='consigne'> Clique sur le bouton [Revoir] et la phrase va à nouveau s'afficher quelques instants.",
  erreur3: "<div class='messages'>Malheureusement il y a encore des erreurs. Relis la partie en noir.</div><div class='consigne'>Clique sur le bouton [J'ai lu] lorsque tu es sûr d'avoir bien relu."
}

const options = {
  tempsAffichage: 2000, // Temps d'affichage du mot
  fonte:"script", // Fonte par défaut, "script" ou "cursive"
  nbPhrasesBase: 5, // Nombre de mots à proposer par exercice
};

const paramPhrases = {
  listePhrases: [],
  phraseDouble: false,
  partiePhrase: [],
  ctParties: 0,
  ctEssais: 0,
  reponseAttendue: "",
  phraseModele: "",
  ctPhrases: 0,
  nbPhrasesExo: options.nbPhrasesBase,
  ctVues: 0
}

// Variables nécessaires au calcul des résultats
const paramResultats = {
  ctTentatives: 0,
  ctTotalTentatives: 0,
  ctExercices: 0,
  ctEpreuves: 0,
  ctTotalEpreuves: 0,
  nbReussis: 0,
  nbReussisTotal: 0,
  ctAbandonsEpreuve: 0,
  ctTotalAbandons: 0,
  listeExercices: []
}

// Pour l'affichage dans la zone d'exercice
const phraseReference = document.getElementById("modele");
const annonce = document.getElementById("annonces");
const phraseSaisie = document.getElementById("reponse");

// Pour l'affichage des résultats
const nbEpreuves = document.getElementById("numero-epreuve");
const nbTentatives = document.getElementById("nbtentatives");
const nbTotalEpreuves = document.getElementById("nbtotalepreuves");
const tauxEpreuves = document.getElementById("tauxepreuves");
const nbEchecs = document.getElementById("nbechecs");
const nbExercices = document.getElementById("nbexercices");
const tauxExos = document.getElementById("tauxexos");
const nbTotalEchecs = document.getElementById("nbtotalechecs");
const nomEleve =  document.getElementById("patronyme");

function alea(min,max) {
  let nb = min + (max-min+1) * Math.random();
  return Math.floor(nb);
}

function melange(tableau) {
  let i;
  let tab = tableau.slice();
  let tirage;
  let tab_alea = [];

  for (i = 0; i < tab.length; i++) {
    do {
      tirage = alea(0, tab.length -1);
    } while (tab[tirage] === 0);
    tab_alea[i] = tab[tirage];
    tab[tirage] = 0;
  }
  return tab_alea;
}

function rogneChaine(chaine) {
  let s1, s2;
  s1 = chaine.replace(/^[ ]*/,'');
  s2 = s1.replace(/[ ]*$/,'');
  return s2;
}

function initialiseCompteurs() {
  paramPhrases.ctPhrases = 0;
  paramResultats.ctTentatives = 0;
  paramResultats.ctEpreuves = 0;
  paramResultats.nbReussis = 0;
  paramResultats.ctAbandonsEpreuve = 0;
  paramPhrases.nbPhrasesExo = options.nbPhrasesBase;
}

function tirePhrases(tableau) {
  let i;
  let tirage;
  let tabAlea = [];
  if (( sessionStorage.getItem("nb-mots")) && (sessionStorage.getItem("nb-mots") !== 'undefined')) {
    options.nbPhrasesBase = sessionStorage.getItem("nb-mots");
    paramPhrases.nbPhrasesExo = options.nbPhrasesBase;
  }
  if ( tableau.length < options.nbPhrasesBase) {
    paramPhrases.nbPhrasesExo = tableau.length;
    paramPhrases.listePhrases = melange(tableau);
    return;
  }
  for(i=0; i < options.nbPhrasesBase ; i++) {
    do {
      tirage = alea(0, tableau.length -1);
    } while (tabAlea.indexOf(tirage) >= 0);
    tabAlea[i] = tirage;
    paramPhrases.listePhrases[i] = tableau[tirage];
  }
}

function rangePhrases(tableau) {
  let i;
  paramPhrases.nbPhrasesExo = tableau.length;
  for(i=0; i < paramPhrases.nbPhrasesExo ; i++) {
    paramPhrases.listePhrases[i] = tableau[i];
  }
}

function creeListesSelection() {
  let i;
  const exemples = document.getElementById("liste-exemples");
  const bestiaire = document.getElementById("liste-bestiaire");
  for(i=0; i < listeExemples.length ; i++) {
    exemples.length++;
    exemples.options[exemples.length-1].text = listeExemples[i];
  }
  for(i=0; i < listeBestiaire.length ; i++) {
    bestiaire.length++;
    bestiaire.options[bestiaire.length-1].text = listeBestiaire[i];
  }
}

function creeExercice(typeExercice) {
  const liste = document.getElementById(typeExercice);
  let choix = liste.selectedIndex -1;
  liste.options[liste.selectedIndex].selected = false;
  initialiseCompteurs();
  annonce.innerHTML = '';
  switch(typeExercice) {
    case 'liste-exemples':
      tirePhrases(Exemples[choix]);
      paramResultats.listeExercices.push(listeExemples[choix + 1]);
      break;
    case 'liste-bestiaire':
      rangePhrases(Bestiaire[choix]);
      paramResultats.listeExercices.push(listeBestiaire[choix + 1]);
      break;
  }
  nouvellePhrase();
  document.getElementById("lbsaisie").className="lb-actif";
  effaceResultatsExercice();
}

function chargeFonte() {
  let i;
  const lettres = document.getElementsByClassName("lettre");
  if (( sessionStorage.getItem("fonte") ) && (sessionStorage.getItem("fonte") !== 'undefined')) {
    options.fonte = sessionStorage.getItem("fonte");
  }
  if ( options.fonte == "cursive" ) {
    for(i = 0; i < lettres.length; i++) {
      lettres[i].classList.add("cursive");
    }
  }
  else {
    for(i = 0; i < lettres.length; i++) {
      lettres[i].classList.remove("cursive");
    }
  }
}

function lanceOptions() {
  document.getElementById("ecran-blanc").className = "visible";
  document.getElementById("options").className = "visible";
}

function annuleOptions() {
  document.getElementById("ecran-blanc").className = "invisible";
  document.getElementById("options").className = "invisible";
}

function valideOptions() {
  let i;
  const choixFonte = document.getElementsByName('fonte');
  let valeurFonte = "script";
  let valeurTemps = 2;
  // Récupère et stocke la valeur choisie pour le temps d'affichage
  valeurTemps = document.forms["form-options"].elements["temps"].value;
  valeurTemps = valeurTemps * 1000;
  sessionStorage.setItem("temps",valeurTemps);
  //  Récupère et stocke la valeur choisie pour le type de police
  for(i = 0; i < choixFonte.length; i++) {
    if(choixFonte[i].checked){
      valeurFonte = choixFonte[i].value;
    }
  }
  sessionStorage.setItem("fonte",valeurFonte);
  // Récupère et stocke la valeur choisie pour le nombre de mots par exercice
  sessionStorage.setItem("nb-mots",document.forms["form-options"].elements["nbmots"].value);
}

function activePartie(partie) {
  let i;
  let nbParties = paramPhrases.partiePhrase.length;
  paramPhrases.phraseModele = "";
  for (i = 0; i < partie ; i++) {
    paramPhrases.phraseModele = paramPhrases.phraseModele + paramPhrases.partiePhrase[i];
  }
  paramPhrases.phraseModele =  paramPhrases.phraseModele + "<span class='actif'>";
  paramPhrases.phraseModele =  paramPhrases.phraseModele + paramPhrases.partiePhrase[partie] + "</span>";
  for (i = partie+1; i < nbParties ; i++) {
    paramPhrases.phraseModele = paramPhrases.phraseModele + paramPhrases.partiePhrase[i];
  }
  phraseReference.innerHTML = paramPhrases.phraseModele;
}

function decoupePhrase() {
  paramPhrases.partiePhrase = paramPhrases.listePhrases[paramPhrases.ctPhrases].split('/');
  activePartie(0);
  paramPhrases.reponseAttendue = paramPhrases.partiePhrase[0];
}

function nouvellePhrase() {
  let ct = paramPhrases.ctPhrases + 1;
  paramPhrases.ctVues = 0;
  paramPhrases.ctParties = 0;
  if (paramPhrases.listePhrases[paramPhrases.ctPhrases].indexOf('/') > 0) {
    paramPhrases.phraseDouble = true;
    decoupePhrase();
    annonce.innerHTML = messages.phraseDouble;
    phraseReference.classList.remove("modele-actif");
    phraseReference.classList.add("modele-inactif");
  }
  else {
    paramPhrases.phraseModele = paramPhrases.listePhrases[paramPhrases.ctPhrases];
    paramPhrases.reponseAttendue = paramPhrases.listePhrases[paramPhrases.ctPhrases];
    paramPhrases.phraseDouble = false;
    phraseReference.classList.remove("modele-inactif");
    phraseReference.classList.add("modele-actif");
  }
  phraseSaisie.value = "";
  nbEpreuves.innerHTML = ct + "/" + paramPhrases.nbPhrasesExo;
  affichePhrase();
}

function cachePhrase() {
  let cache ="";
  phraseReference.innerHTML = cache;
  document.getElementById("btlu").disabled = true;
  document.getElementById('reponse').disabled=false;
  document.getElementById("btverifier").disabled = false;
  phraseSaisie.focus();
}

function affichePhrase() {
  document.getElementById("btlu").disabled = false;
  if ( options.fonte =="cursive" ) {
    phraseReference.classList.add("cursive");
  }
  phraseSaisie.focus();
  if (( sessionStorage.getItem("temps") ) && (sessionStorage.getItem("temps") !== 'undefined')) {
    options.tempsAffichage = sessionStorage.getItem("temps");
  }
  phraseReference.innerHTML = paramPhrases.phraseModele;
  document.getElementById("btsuivant").disabled = true;
}

function reaffichePhrase() {
  document.getElementById("btlu").disabled = false;
  if ( options.fonte =="cursive" ) {
    phraseReference.classList.add("cursive");
  }
  phraseReference.innerHTML = paramPhrases.phraseModele;
  document.getElementById("btrevoir").disabled = true;
}

function revoirPhrase() {
  if ( options.fonte =="cursive" ) {
    phraseReference.classList.add("cursive");
  }
  paramPhrases.ctVues++;
  phraseSaisie.focus();
  phraseReference.innerHTML = paramPhrases.phraseModele;
  setTimeout(cachePhrase, options.tempsAffichage);
  document.getElementById("btrevoir").disabled = true;
  document.getElementById("btverifier").disabled = false;
}

function indiqueErreur(chaineSaisie) {
  let s;
  let visualise;
  let i = 0;
  while ( i < chaineSaisie.length ) {
    if ( chaineSaisie[i] != paramPhrases.reponseAttendue[i] ) {
      break;
    }
    i++;
  }
  if (paramPhrases.reponseAttendue.length >= chaineSaisie.length) {
    s = paramPhrases.reponseAttendue.replace(/ /g,' ');
  }
  else {
    s = chaineSaisie.replace(/ /g,' ');
  }
  visualise = s.substring(0,i) + "<span class='surligne'>" + s.substring(i,s.length + 1) + "</span>";
  return visualise;
}

function afficheResultats() {
  let taux1 = 0;
  let taux2 = 0;
  if ( paramResultats.ctTentatives > 0 ) {
    taux1 = Math.round((paramResultats.nbReussis * 100) / paramResultats.ctTentatives);
  }
  if ( paramResultats.ctTotalTentatives > 0 ) {
    taux2 = Math.round((paramResultats.nbReussisTotal * 100) / paramResultats.ctTotalTentatives);
  }
  nbTentatives.innerHTML = paramResultats.ctTentatives;
  if ( taux1 ) { tauxEpreuves.innerHTML = taux1 + "%"; }
  nbEchecs.innerHTML = paramResultats.ctAbandonsEpreuve;
  nbExercices.innerHTML = paramResultats.ctExercices;
  nbTotalEpreuves.innerHTML = paramResultats.ctTotalEpreuves;
  if ( taux2 ) { tauxExos.innerHTML = taux2 + "%"; }
  nbTotalEchecs.innerHTML = paramResultats.ctTotalAbandons;
}

function effaceResultatsExercice() {
  nbEpreuves.innerHTML = "";
  nbTentatives.innerHTML = "";
  tauxEpreuves.innerHTML = "";
  nbEchecs.innerHTML = "";
}

function prepareNouvelExercice() {
  document.getElementById("btverifier").disabled = true;
  // activeListesSelection();
  paramResultats.ctExercices++;
  paramResultats.ctTentatives = 0;
  afficheResultats();
  effaceResultatsExercice();
}

function verifier() {
  const Reponse = document.getElementById('reponse');
  paramResultats.ctTentatives++;
  paramPhrases.ctEssais++;
  paramResultats.ctTotalTentatives++;
  Reponse.value = rogneChaine(Reponse.value);
  if ( Reponse.value != paramPhrases.reponseAttendue ) {
    switch(paramPhrases.ctEssais) {
      case 1:
        annonce.innerHTML = messages.erreur1;
        break;
      case 2:
        annonce.innerHTML = messages.erreur2;
        document.getElementById("btrevoir").disabled = false;
        document.getElementById("btverifier").disabled = true;
        break;
      case 3:
        document.getElementById('btsolution').disabled = false;
      default:
        annonce.innerHTML = messages.erreur3;
        document.getElementById("btlu").disabled = false;
        document.getElementById('reponse').disabled=true;
        document.getElementById("btverifier").disabled = true;
        reaffichePhrase();
    }
    afficheResultats();
    return;
  }
  paramResultats.nbReussis++;
  paramResultats.nbReussisTotal++;
  paramResultats.ctEpreuves++;
  paramResultats.ctTotalEpreuves++;
  document.getElementById("btsuivant").disabled = true;
  document.getElementById('reponse').disabled=true;
  document.getElementById('btsolution').disabled=true;
  if ((paramPhrases.ctPhrases == paramPhrases.nbPhrasesExo - 1) && ((! paramPhrases.phraseDouble) || ((paramPhrases.phraseDouble) && (paramPhrases.ctParties == paramPhrases.partiePhrase.length-1)))) {
    if (paramResultats.ctAbandonsEpreuve == 0) {
      annonce.innerHTML = messages.felicitations;
    }
    else {
      annonce.innerHTML = messages.finExo;
    }
    prepareNouvelExercice();
  }
  else {
    annonce.innerHTML = messages.bravo;
    if ( options.fonte =="cursive" ) {
      phraseReference.classList.add("cursive");
    }
    phraseReference.innerHTML = paramPhrases.phraseModele;
    afficheResultats();
    document.getElementById("btverifier").disabled = true;
    document.getElementById("btsuivant").disabled = false;
    document.getElementById("btsuivant").focus();
  }
}

function phraseSuivante() {
  annonce.innerHTML = '';
  document.getElementById('btsolution').disabled = true;
  document.getElementById("btverifier").disabled = true;
  document.getElementById("btlu").disabled = false;
  paramPhrases.ctEssais = 0;
  if ((paramPhrases.phraseDouble) && (paramPhrases.ctParties < paramPhrases.partiePhrase.length - 1)) {
    paramPhrases.ctParties++;
    paramPhrases.reponseAttendue = paramPhrases.reponseAttendue + paramPhrases.partiePhrase[paramPhrases.ctParties];
    activePartie(paramPhrases.ctParties);
    affichePhrase();
    annonce.innerHTML = messages.phraseDouble;
  }
  else {
    paramPhrases.ctPhrases++;
    paramPhrases.reponseAttendue = paramPhrases.listePhrases[paramPhrases.ctPhrases];
    let Reponse = document.getElementById('reponse');
    Reponse.value = "";
    nouvellePhrase();
  }
}

function Solution() {
  const reponse = document.getElementById('reponse');
  paramPhrases.ctEssais++;
  paramResultats.ctTentatives++;
  paramResultats.ctTotalTentatives++;
  paramResultats.ctAbandonsEpreuve++;
  paramResultats.ctTotalAbandons++;
  paramResultats.ctEpreuves++;
  paramResultats.ctTotalEpreuves++;
  phraseReference.innerHTML = paramPhrases.phraseModele;
  reponse.value = rogneChaine(reponse.value);
  annonce.innerHTML = "<div class='messages'>" + indiqueErreur(Reponse.value) + "</div>";
  afficheResultats();
  if ( paramPhrases.ctPhrases + 1 == paramPhrases.nbPhrasesExo ) {
    paramPhrases.ctPhrases++;
    annonce.innerHTML = annonce.innerHTML + messages.passerExo;
    document.getElementById("btsuivant").disabled = true;
    prepareNouvelExercice();
  }
  else {
    annonce.innerHTML = annonce.innerHTML + messages.passer;
    document.getElementById("btsuivant").disabled = false;
  }
  document.getElementById('btsolution').disabled = true;
}

function demandeNom() {
  document.getElementById("ecran-blanc").className = "visible";
  document.getElementById("saisie-nom").className = "visible";
  document.getElementById("patronyme").focus();
}

function pageImpression() {
  let taux;
  if ( paramResultats.ctTotalTentatives > 0 ) {
    taux = Math.round((paramResultats.nbReussisTotal * 100) / paramResultats.ctTotalTentatives);
  }

  sessionStorage.setItem("nom",nomEleve.value);
  sessionStorage.setItem("exos",paramResultats.ctExercices);
  sessionStorage.setItem("liste-exos",paramResultats.listeExercices);
  sessionStorage.setItem("epreuves",paramResultats.ctTotalEpreuves);
  sessionStorage.setItem("taux",taux);
  sessionStorage.setItem("echecs",paramResultats.ctTotalAbandons);
  window.open('impression.html');
}

function annuleImpression() {
  document.getElementById("ecran-blanc").className = "invisible";
  document.getElementById("saisie-nom").className = "invisible";
}

function soumetReponse(ev) {
  ev.preventDefault();
  verifier();
}

function initialise() {
  creeListesSelection();
  chargeFonte();
  annonce.innerHTML = messages.accueil;
  nbEpreuves.innerHTML = "";
  nbTentatives.innerHTML = "";
  tauxEpreuves.innerHTML = "";
}

window.onload = initialise();
