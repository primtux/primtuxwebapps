gdjs.menuCode = {};
gdjs.menuCode.GDscore2Objects1_1final = [];

gdjs.menuCode.GDscoreObjects1_1final = [];

gdjs.menuCode.GDstand1Objects1_1final = [];

gdjs.menuCode.GDstand2Objects1_1final = [];

gdjs.menuCode.GDjeu2Objects1= [];
gdjs.menuCode.GDjeu2Objects2= [];
gdjs.menuCode.GDjeu2Objects3= [];
gdjs.menuCode.GDjeu1Objects1= [];
gdjs.menuCode.GDjeu1Objects2= [];
gdjs.menuCode.GDjeu1Objects3= [];
gdjs.menuCode.GDcreditsObjects1= [];
gdjs.menuCode.GDcreditsObjects2= [];
gdjs.menuCode.GDcreditsObjects3= [];
gdjs.menuCode.GDAuteurObjects1= [];
gdjs.menuCode.GDAuteurObjects2= [];
gdjs.menuCode.GDAuteurObjects3= [];
gdjs.menuCode.GDsoustitreObjects1= [];
gdjs.menuCode.GDsoustitreObjects2= [];
gdjs.menuCode.GDsoustitreObjects3= [];
gdjs.menuCode.GDTitreObjects1= [];
gdjs.menuCode.GDTitreObjects2= [];
gdjs.menuCode.GDTitreObjects3= [];
gdjs.menuCode.GDcompetencesObjects1= [];
gdjs.menuCode.GDcompetencesObjects2= [];
gdjs.menuCode.GDcompetencesObjects3= [];
gdjs.menuCode.GDsergeObjects1= [];
gdjs.menuCode.GDsergeObjects2= [];
gdjs.menuCode.GDsergeObjects3= [];
gdjs.menuCode.GDversionObjects1= [];
gdjs.menuCode.GDversionObjects2= [];
gdjs.menuCode.GDversionObjects3= [];
gdjs.menuCode.GDballonObjects1= [];
gdjs.menuCode.GDballonObjects2= [];
gdjs.menuCode.GDballonObjects3= [];
gdjs.menuCode.GDnombreObjects1= [];
gdjs.menuCode.GDnombreObjects2= [];
gdjs.menuCode.GDnombreObjects3= [];
gdjs.menuCode.GDfond2Objects1= [];
gdjs.menuCode.GDfond2Objects2= [];
gdjs.menuCode.GDfond2Objects3= [];
gdjs.menuCode.GDintervalleObjects1= [];
gdjs.menuCode.GDintervalleObjects2= [];
gdjs.menuCode.GDintervalleObjects3= [];
gdjs.menuCode.GDbouton_9595infoObjects1= [];
gdjs.menuCode.GDbouton_9595infoObjects2= [];
gdjs.menuCode.GDbouton_9595infoObjects3= [];
gdjs.menuCode.GDscore2Objects1= [];
gdjs.menuCode.GDscore2Objects2= [];
gdjs.menuCode.GDscore2Objects3= [];
gdjs.menuCode.GDscoreObjects1= [];
gdjs.menuCode.GDscoreObjects2= [];
gdjs.menuCode.GDscoreObjects3= [];
gdjs.menuCode.GDfondObjects1= [];
gdjs.menuCode.GDfondObjects2= [];
gdjs.menuCode.GDfondObjects3= [];
gdjs.menuCode.GDbouton_9595retourObjects1= [];
gdjs.menuCode.GDbouton_9595retourObjects2= [];
gdjs.menuCode.GDbouton_9595retourObjects3= [];
gdjs.menuCode.GDnombreObjects1= [];
gdjs.menuCode.GDnombreObjects2= [];
gdjs.menuCode.GDnombreObjects3= [];
gdjs.menuCode.GDbouton_9595optionObjects1= [];
gdjs.menuCode.GDbouton_9595optionObjects2= [];
gdjs.menuCode.GDbouton_9595optionObjects3= [];
gdjs.menuCode.GDstand1Objects1= [];
gdjs.menuCode.GDstand1Objects2= [];
gdjs.menuCode.GDstand1Objects3= [];
gdjs.menuCode.GDstand2Objects1= [];
gdjs.menuCode.GDstand2Objects2= [];
gdjs.menuCode.GDstand2Objects3= [];
gdjs.menuCode.GDbouton_9595sergeObjects1= [];
gdjs.menuCode.GDbouton_9595sergeObjects2= [];
gdjs.menuCode.GDbouton_9595sergeObjects3= [];


gdjs.menuCode.eventsList0 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.storage.elementExistsInJSONFile("sauvegarde_lakermesse", "reglages");
if (isConditionTrue_0) {
{gdjs.evtTools.storage.readStringFromJSONFile("sauvegarde_lakermesse", "reglages", runtimeScene, runtimeScene.getScene().getVariables().get("sauvegarde"));
}{gdjs.evtTools.network.jsonToVariableStructure(gdjs.evtTools.variable.getVariableString(runtimeScene.getScene().getVariables().get("sauvegarde")), runtimeScene.getGame().getVariables().getFromIndex(3));
}}

}


};gdjs.menuCode.eventsList1 = function(runtimeScene) {

{


gdjs.menuCode.eventsList0(runtimeScene);
}


{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("score"), gdjs.menuCode.GDscoreObjects2);
gdjs.copyArray(runtimeScene.getObjects("score2"), gdjs.menuCode.GDscore2Objects2);
{for(var i = 0, len = gdjs.menuCode.GDscoreObjects2.length ;i < len;++i) {
    gdjs.menuCode.GDscoreObjects2[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3).getChild("score1")));
}
}{for(var i = 0, len = gdjs.menuCode.GDscore2Objects2.length ;i < len;++i) {
    gdjs.menuCode.GDscore2Objects2[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3).getChild("score2")));
}
}{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/audio_vide.mp3", 1, false, 100, 1);
}{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/audio_vide.mp3", 2, false, 100, 1);
}}

}


{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("intervalle"), gdjs.menuCode.GDintervalleObjects1);
{for(var i = 0, len = gdjs.menuCode.GDintervalleObjects1.length ;i < len;++i) {
    gdjs.menuCode.GDintervalleObjects1[i].setBBText(gdjs.evtTools.common.toString(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3).getChild("nombreA").getChild("mini"))) + " - " + gdjs.evtTools.common.toString(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3).getChild("nombreA").getChild("maxi"))));
}
}}

}


};gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDstand1Objects2Objects = Hashtable.newFrom({"stand1": gdjs.menuCode.GDstand1Objects2});
gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDscoreObjects2Objects = Hashtable.newFrom({"score": gdjs.menuCode.GDscoreObjects2});
gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDstand2Objects2Objects = Hashtable.newFrom({"stand2": gdjs.menuCode.GDstand2Objects2});
gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDscore2Objects2Objects = Hashtable.newFrom({"score2": gdjs.menuCode.GDscore2Objects2});
gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDbouton_95959595optionObjects1Objects = Hashtable.newFrom({"bouton_option": gdjs.menuCode.GDbouton_9595optionObjects1});
gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDbouton_95959595infoObjects1Objects = Hashtable.newFrom({"bouton_info": gdjs.menuCode.GDbouton_9595infoObjects1});
gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDsergeObjects1Objects = Hashtable.newFrom({"serge": gdjs.menuCode.GDsergeObjects1});
gdjs.menuCode.eventsList2 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
{gdjs.evtTools.storage.clearJSONFile("sauvegarde_lakermesse");
}{gdjs.evtTools.storage.writeStringInJSONFile("sauvegarde_lakermesse", "reglages", gdjs.evtTools.network.variableStructureToJSON(runtimeScene.getGame().getVariables().getFromIndex(3)));
}}

}


};gdjs.menuCode.eventsList3 = function(runtimeScene) {

{


gdjs.menuCode.eventsList2(runtimeScene);
}


{


let isConditionTrue_0 = false;
{
}

}


};gdjs.menuCode.eventsList4 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("ballon"), gdjs.menuCode.GDballonObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.menuCode.GDballonObjects2.length;i<l;++i) {
    if ( gdjs.menuCode.GDballonObjects2[i].getY() > 60 ) {
        isConditionTrue_0 = true;
        gdjs.menuCode.GDballonObjects2[k] = gdjs.menuCode.GDballonObjects2[i];
        ++k;
    }
}
gdjs.menuCode.GDballonObjects2.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.menuCode.GDballonObjects2 */
gdjs.copyArray(runtimeScene.getObjects("nombre"), gdjs.menuCode.GDnombreObjects2);
{for(var i = 0, len = gdjs.menuCode.GDballonObjects2.length ;i < len;++i) {
    gdjs.menuCode.GDballonObjects2[i].setY(gdjs.menuCode.GDballonObjects2[i].getY() - (0.2));
}
}{for(var i = 0, len = gdjs.menuCode.GDnombreObjects2.length ;i < len;++i) {
    gdjs.menuCode.GDnombreObjects2[i].setY(gdjs.menuCode.GDnombreObjects2[i].getY() - (0.2));
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("ballon"), gdjs.menuCode.GDballonObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.menuCode.GDballonObjects1.length;i<l;++i) {
    if ( gdjs.menuCode.GDballonObjects1[i].getY() <= 60 ) {
        isConditionTrue_0 = true;
        gdjs.menuCode.GDballonObjects1[k] = gdjs.menuCode.GDballonObjects1[i];
        ++k;
    }
}
gdjs.menuCode.GDballonObjects1.length = k;
if (isConditionTrue_0) {
{runtimeScene.getScene().getVariables().get("anim_ballon").getChild("1").setNumber(1);
}}

}


};gdjs.menuCode.eventsList5 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("ballon"), gdjs.menuCode.GDballonObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.menuCode.GDballonObjects2.length;i<l;++i) {
    if ( gdjs.menuCode.GDballonObjects2[i].getY() < 90 ) {
        isConditionTrue_0 = true;
        gdjs.menuCode.GDballonObjects2[k] = gdjs.menuCode.GDballonObjects2[i];
        ++k;
    }
}
gdjs.menuCode.GDballonObjects2.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.menuCode.GDballonObjects2 */
gdjs.copyArray(runtimeScene.getObjects("nombre"), gdjs.menuCode.GDnombreObjects2);
{for(var i = 0, len = gdjs.menuCode.GDballonObjects2.length ;i < len;++i) {
    gdjs.menuCode.GDballonObjects2[i].setY(gdjs.menuCode.GDballonObjects2[i].getY() + (0.2));
}
}{for(var i = 0, len = gdjs.menuCode.GDnombreObjects2.length ;i < len;++i) {
    gdjs.menuCode.GDnombreObjects2[i].setY(gdjs.menuCode.GDnombreObjects2[i].getY() + (0.2));
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("ballon"), gdjs.menuCode.GDballonObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.menuCode.GDballonObjects1.length;i<l;++i) {
    if ( gdjs.menuCode.GDballonObjects1[i].getY() >= 90 ) {
        isConditionTrue_0 = true;
        gdjs.menuCode.GDballonObjects1[k] = gdjs.menuCode.GDballonObjects1[i];
        ++k;
    }
}
gdjs.menuCode.GDballonObjects1.length = k;
if (isConditionTrue_0) {
{runtimeScene.getScene().getVariables().get("anim_ballon").getChild("1").setNumber(0);
}}

}


};gdjs.menuCode.eventsList6 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
{gdjs.evtTools.input.showCursor(runtimeScene);
}
{ //Subevents
gdjs.menuCode.eventsList1(runtimeScene);} //End of subevents
}

}


{

gdjs.menuCode.GDscoreObjects1.length = 0;

gdjs.menuCode.GDstand1Objects1.length = 0;


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{gdjs.menuCode.GDscoreObjects1_1final.length = 0;
gdjs.menuCode.GDstand1Objects1_1final.length = 0;
let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("stand1"), gdjs.menuCode.GDstand1Objects2);
isConditionTrue_1 = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDstand1Objects2Objects, runtimeScene, true, false);
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.menuCode.GDstand1Objects2.length; j < jLen ; ++j) {
        if ( gdjs.menuCode.GDstand1Objects1_1final.indexOf(gdjs.menuCode.GDstand1Objects2[j]) === -1 )
            gdjs.menuCode.GDstand1Objects1_1final.push(gdjs.menuCode.GDstand1Objects2[j]);
    }
}
}
{
gdjs.copyArray(runtimeScene.getObjects("score"), gdjs.menuCode.GDscoreObjects2);
isConditionTrue_1 = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDscoreObjects2Objects, runtimeScene, true, false);
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.menuCode.GDscoreObjects2.length; j < jLen ; ++j) {
        if ( gdjs.menuCode.GDscoreObjects1_1final.indexOf(gdjs.menuCode.GDscoreObjects2[j]) === -1 )
            gdjs.menuCode.GDscoreObjects1_1final.push(gdjs.menuCode.GDscoreObjects2[j]);
    }
}
}
{
gdjs.copyArray(gdjs.menuCode.GDscoreObjects1_1final, gdjs.menuCode.GDscoreObjects1);
gdjs.copyArray(gdjs.menuCode.GDstand1Objects1_1final, gdjs.menuCode.GDstand1Objects1);
}
}
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(4).setNumber(0);
}{runtimeScene.getGame().getVariables().getFromIndex(3).getChild("score0").setNumber(0);
}{runtimeScene.getGame().getVariables().getFromIndex(5).setNumber(1);
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "tirage", false);
}}

}


{

gdjs.menuCode.GDscore2Objects1.length = 0;

gdjs.menuCode.GDstand2Objects1.length = 0;


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{gdjs.menuCode.GDscore2Objects1_1final.length = 0;
gdjs.menuCode.GDstand2Objects1_1final.length = 0;
let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("stand2"), gdjs.menuCode.GDstand2Objects2);
isConditionTrue_1 = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDstand2Objects2Objects, runtimeScene, true, false);
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.menuCode.GDstand2Objects2.length; j < jLen ; ++j) {
        if ( gdjs.menuCode.GDstand2Objects1_1final.indexOf(gdjs.menuCode.GDstand2Objects2[j]) === -1 )
            gdjs.menuCode.GDstand2Objects1_1final.push(gdjs.menuCode.GDstand2Objects2[j]);
    }
}
}
{
gdjs.copyArray(runtimeScene.getObjects("score2"), gdjs.menuCode.GDscore2Objects2);
isConditionTrue_1 = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDscore2Objects2Objects, runtimeScene, true, false);
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.menuCode.GDscore2Objects2.length; j < jLen ; ++j) {
        if ( gdjs.menuCode.GDscore2Objects1_1final.indexOf(gdjs.menuCode.GDscore2Objects2[j]) === -1 )
            gdjs.menuCode.GDscore2Objects1_1final.push(gdjs.menuCode.GDscore2Objects2[j]);
    }
}
}
{
gdjs.copyArray(gdjs.menuCode.GDscore2Objects1_1final, gdjs.menuCode.GDscore2Objects1);
gdjs.copyArray(gdjs.menuCode.GDstand2Objects1_1final, gdjs.menuCode.GDstand2Objects1);
}
}
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(4).setNumber(0);
}{runtimeScene.getGame().getVariables().getFromIndex(3).getChild("score0").setNumber(0);
}{runtimeScene.getGame().getVariables().getFromIndex(5).setNumber(2);
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "tirage", false);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_option"), gdjs.menuCode.GDbouton_9595optionObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDbouton_95959595optionObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "code", false);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_info"), gdjs.menuCode.GDbouton_9595infoObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDbouton_95959595infoObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "infos", false);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("serge"), gdjs.menuCode.GDsergeObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDsergeObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {
{runtimeScene.getScene().getVariables().get("score_zero").add(1);
}{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/bulle_1.mp3", 1, false, 100, 1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().get("score_zero")) == 5;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("score"), gdjs.menuCode.GDscoreObjects1);
gdjs.copyArray(runtimeScene.getObjects("score2"), gdjs.menuCode.GDscore2Objects1);
{runtimeScene.getScene().getVariables().get("score_zero").setNumber(0);
}{runtimeScene.getGame().getVariables().getFromIndex(3).getChild("score1").setNumber(0);
}{for(var i = 0, len = gdjs.menuCode.GDscoreObjects1.length ;i < len;++i) {
    gdjs.menuCode.GDscoreObjects1[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3).getChild("score1")));
}
}{runtimeScene.getGame().getVariables().getFromIndex(3).getChild("score2").setNumber(0);
}{for(var i = 0, len = gdjs.menuCode.GDscore2Objects1.length ;i < len;++i) {
    gdjs.menuCode.GDscore2Objects1[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3).getChild("score2")));
}
}
{ //Subevents
gdjs.menuCode.eventsList3(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().get("anim_ballon").getChild("1")) == 0;
if (isConditionTrue_0) {

{ //Subevents
gdjs.menuCode.eventsList4(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().get("anim_ballon").getChild("1")) == 1;
if (isConditionTrue_0) {

{ //Subevents
gdjs.menuCode.eventsList5(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
{
}

}


};

gdjs.menuCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.menuCode.GDjeu2Objects1.length = 0;
gdjs.menuCode.GDjeu2Objects2.length = 0;
gdjs.menuCode.GDjeu2Objects3.length = 0;
gdjs.menuCode.GDjeu1Objects1.length = 0;
gdjs.menuCode.GDjeu1Objects2.length = 0;
gdjs.menuCode.GDjeu1Objects3.length = 0;
gdjs.menuCode.GDcreditsObjects1.length = 0;
gdjs.menuCode.GDcreditsObjects2.length = 0;
gdjs.menuCode.GDcreditsObjects3.length = 0;
gdjs.menuCode.GDAuteurObjects1.length = 0;
gdjs.menuCode.GDAuteurObjects2.length = 0;
gdjs.menuCode.GDAuteurObjects3.length = 0;
gdjs.menuCode.GDsoustitreObjects1.length = 0;
gdjs.menuCode.GDsoustitreObjects2.length = 0;
gdjs.menuCode.GDsoustitreObjects3.length = 0;
gdjs.menuCode.GDTitreObjects1.length = 0;
gdjs.menuCode.GDTitreObjects2.length = 0;
gdjs.menuCode.GDTitreObjects3.length = 0;
gdjs.menuCode.GDcompetencesObjects1.length = 0;
gdjs.menuCode.GDcompetencesObjects2.length = 0;
gdjs.menuCode.GDcompetencesObjects3.length = 0;
gdjs.menuCode.GDsergeObjects1.length = 0;
gdjs.menuCode.GDsergeObjects2.length = 0;
gdjs.menuCode.GDsergeObjects3.length = 0;
gdjs.menuCode.GDversionObjects1.length = 0;
gdjs.menuCode.GDversionObjects2.length = 0;
gdjs.menuCode.GDversionObjects3.length = 0;
gdjs.menuCode.GDballonObjects1.length = 0;
gdjs.menuCode.GDballonObjects2.length = 0;
gdjs.menuCode.GDballonObjects3.length = 0;
gdjs.menuCode.GDnombreObjects1.length = 0;
gdjs.menuCode.GDnombreObjects2.length = 0;
gdjs.menuCode.GDnombreObjects3.length = 0;
gdjs.menuCode.GDfond2Objects1.length = 0;
gdjs.menuCode.GDfond2Objects2.length = 0;
gdjs.menuCode.GDfond2Objects3.length = 0;
gdjs.menuCode.GDintervalleObjects1.length = 0;
gdjs.menuCode.GDintervalleObjects2.length = 0;
gdjs.menuCode.GDintervalleObjects3.length = 0;
gdjs.menuCode.GDbouton_9595infoObjects1.length = 0;
gdjs.menuCode.GDbouton_9595infoObjects2.length = 0;
gdjs.menuCode.GDbouton_9595infoObjects3.length = 0;
gdjs.menuCode.GDscore2Objects1.length = 0;
gdjs.menuCode.GDscore2Objects2.length = 0;
gdjs.menuCode.GDscore2Objects3.length = 0;
gdjs.menuCode.GDscoreObjects1.length = 0;
gdjs.menuCode.GDscoreObjects2.length = 0;
gdjs.menuCode.GDscoreObjects3.length = 0;
gdjs.menuCode.GDfondObjects1.length = 0;
gdjs.menuCode.GDfondObjects2.length = 0;
gdjs.menuCode.GDfondObjects3.length = 0;
gdjs.menuCode.GDbouton_9595retourObjects1.length = 0;
gdjs.menuCode.GDbouton_9595retourObjects2.length = 0;
gdjs.menuCode.GDbouton_9595retourObjects3.length = 0;
gdjs.menuCode.GDnombreObjects1.length = 0;
gdjs.menuCode.GDnombreObjects2.length = 0;
gdjs.menuCode.GDnombreObjects3.length = 0;
gdjs.menuCode.GDbouton_9595optionObjects1.length = 0;
gdjs.menuCode.GDbouton_9595optionObjects2.length = 0;
gdjs.menuCode.GDbouton_9595optionObjects3.length = 0;
gdjs.menuCode.GDstand1Objects1.length = 0;
gdjs.menuCode.GDstand1Objects2.length = 0;
gdjs.menuCode.GDstand1Objects3.length = 0;
gdjs.menuCode.GDstand2Objects1.length = 0;
gdjs.menuCode.GDstand2Objects2.length = 0;
gdjs.menuCode.GDstand2Objects3.length = 0;
gdjs.menuCode.GDbouton_9595sergeObjects1.length = 0;
gdjs.menuCode.GDbouton_9595sergeObjects2.length = 0;
gdjs.menuCode.GDbouton_9595sergeObjects3.length = 0;

gdjs.menuCode.eventsList6(runtimeScene);

return;

}

gdjs['menuCode'] = gdjs.menuCode;
