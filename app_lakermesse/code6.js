gdjs.infosCode = {};
gdjs.infosCode.GDfond_9595blancObjects1= [];
gdjs.infosCode.GDfond_9595blancObjects2= [];
gdjs.infosCode.GDinfosObjects1= [];
gdjs.infosCode.GDinfosObjects2= [];
gdjs.infosCode.GDscore2Objects1= [];
gdjs.infosCode.GDscore2Objects2= [];
gdjs.infosCode.GDscoreObjects1= [];
gdjs.infosCode.GDscoreObjects2= [];
gdjs.infosCode.GDfondObjects1= [];
gdjs.infosCode.GDfondObjects2= [];
gdjs.infosCode.GDbouton_9595retourObjects1= [];
gdjs.infosCode.GDbouton_9595retourObjects2= [];
gdjs.infosCode.GDnombreObjects1= [];
gdjs.infosCode.GDnombreObjects2= [];
gdjs.infosCode.GDbouton_9595optionObjects1= [];
gdjs.infosCode.GDbouton_9595optionObjects2= [];
gdjs.infosCode.GDstand1Objects1= [];
gdjs.infosCode.GDstand1Objects2= [];
gdjs.infosCode.GDstand2Objects1= [];
gdjs.infosCode.GDstand2Objects2= [];
gdjs.infosCode.GDbouton_9595sergeObjects1= [];
gdjs.infosCode.GDbouton_9595sergeObjects2= [];


gdjs.infosCode.mapOfGDgdjs_9546infosCode_9546GDbouton_95959595retourObjects1Objects = Hashtable.newFrom({"bouton_retour": gdjs.infosCode.GDbouton_9595retourObjects1});
gdjs.infosCode.eventsList0 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("fond_blanc"), gdjs.infosCode.GDfond_9595blancObjects1);
{for(var i = 0, len = gdjs.infosCode.GDfond_9595blancObjects1.length ;i < len;++i) {
    gdjs.infosCode.GDfond_9595blancObjects1[i].setOpacity(150);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_retour"), gdjs.infosCode.GDbouton_9595retourObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.infosCode.mapOfGDgdjs_9546infosCode_9546GDbouton_95959595retourObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "menu", false);
}}

}


};

gdjs.infosCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.infosCode.GDfond_9595blancObjects1.length = 0;
gdjs.infosCode.GDfond_9595blancObjects2.length = 0;
gdjs.infosCode.GDinfosObjects1.length = 0;
gdjs.infosCode.GDinfosObjects2.length = 0;
gdjs.infosCode.GDscore2Objects1.length = 0;
gdjs.infosCode.GDscore2Objects2.length = 0;
gdjs.infosCode.GDscoreObjects1.length = 0;
gdjs.infosCode.GDscoreObjects2.length = 0;
gdjs.infosCode.GDfondObjects1.length = 0;
gdjs.infosCode.GDfondObjects2.length = 0;
gdjs.infosCode.GDbouton_9595retourObjects1.length = 0;
gdjs.infosCode.GDbouton_9595retourObjects2.length = 0;
gdjs.infosCode.GDnombreObjects1.length = 0;
gdjs.infosCode.GDnombreObjects2.length = 0;
gdjs.infosCode.GDbouton_9595optionObjects1.length = 0;
gdjs.infosCode.GDbouton_9595optionObjects2.length = 0;
gdjs.infosCode.GDstand1Objects1.length = 0;
gdjs.infosCode.GDstand1Objects2.length = 0;
gdjs.infosCode.GDstand2Objects1.length = 0;
gdjs.infosCode.GDstand2Objects2.length = 0;
gdjs.infosCode.GDbouton_9595sergeObjects1.length = 0;
gdjs.infosCode.GDbouton_9595sergeObjects2.length = 0;

gdjs.infosCode.eventsList0(runtimeScene);

return;

}

gdjs['infosCode'] = gdjs.infosCode;
