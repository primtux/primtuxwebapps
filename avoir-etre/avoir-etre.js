"use strict"

const messages = {
  accueil: "<div class='consigne'>Sélectionne un verbe et un temps de conjugaison.</div>",
  verifier: "<div class='consigne'>Clique sur le bouton [Vérifier] ou appuie sur la touche Entrée lorsque tu penses avoir saisi la bonne réponse.</div>",
  erreur: ["Ce que tu as écrit n'est pas la bonne réponse.","Malheureusement, ce n'est toujours pas la bonne réponse.</br><div class='consigne'>Fais également attention à la majuscule si le verbe commence la phrase.</div>","C'est toujours inexact. </br><div class='consigne'>Tu peux encore essayer, ou abandonner et voir la bonne réponse en cliquant sur le bouton [Solution].</div>"],
  bravo: "<span class='messages--vert'>Bravo !</span><div class='consigne'>Clique sur [Nouveau nombre], ou sélectionne un autre type d'exercice ou un autre niveau.</div>",
  nouvelExercice: "<div class='consigne'>Choisis un autre verbe ou un autre temps de conjugaison.</div>",
  felicitations: "<span class='messages--vert'>Félicitations ! Tu as réussi toutes les phrases de l'exercice.</span><div class='consigne'>Clique sur [Nouvel exercice] pour sélectionner un verbe et un temps de conjugaison.</div>",
};

const paramExercice = {
  verbe: "",
  tempsConjug: "",
  nbPhrases: 10,
  numeroPhrase: 0,
  listePhrases: [],
  solutions: [],
}

// Variables nécessaires au calcul des résultats
const paramResultats = {
  ctTentatives: 0,
  ctTotalTentatives: {
                      "avoir": {"indicatif-present": 0,"indicatif-futur": 0,"indicatif-imparfait": 0,"indicatif-passe-compose": 0,"conditionnel-present": 0,"subjonctif-present": 0},
                      "etre": {"indicatif-present": 0,"indicatif-futur": 0,"indicatif-imparfait": 0,"indicatif-passe-compose": 0,"conditionnel-present": 0,"subjonctif-present": 0},
                     },
  ctPhrases: {
              "avoir":   {"indicatif-present": 0,"indicatif-futur": 0,"indicatif-imparfait": 0,"indicatif-passe-compose": 0,"conditionnel-present": 0,"subjonctif-present": 0},
              "etre":  {"indicatif-present": 0,"indicatif-futur": 0,"indicatif-imparfait": 0,"indicatif-passe-compose": 0,"conditionnel-present": 0,"subjonctif-present": 0},
              },
  ctReussis: {
              "avoir":{"indicatif-present": 0,"indicatif-futur": 0,"indicatif-imparfait": 0,"indicatif-passe-compose": 0,"conditionnel-present": 0,"subjonctif-present": 0},
              "etre": {"indicatif-present": 0,"indicatif-futur": 0,"indicatif-imparfait": 0,"indicatif-passe-compose": 0,"conditionnel-present": 0,"subjonctif-present": 0},
  },
}

// Affichages divers
const affichageTypeExo = document.getElementById("zone-annexe-affichage");
const zoneSelection = document.getElementById("zone-annexe-selection");
const annonce = document.getElementById("annonces");

// Affichage des résultats
const nbTentatives = document.getElementById("nbtentatives");
const nbEpreuves = document.getElementById("nbepreuves");
const totalTentatives = document.getElementById("total-tentatives");
const tauxEfficacite = document.getElementById("efficacite");
const nbEchecs = document.getElementById("nbechecs");
const tauxReussite = document.getElementById("reussite");
const nomEleve =  document.getElementById("patronyme");
const tabResultats = document.getElementById('tab-resultats');

// Boutons
const btVerifier = document.getElementById("btverifier");
const btSolution = document.getElementById("btsolution");
const btchange = document.getElementById("btchange");

// Affichage des modales
const ecranBlanc = document.getElementById("ecran-blanc");
const modaleInfos = document.getElementById("modale-infos");
const saisieNom = document.getElementById("saisie-nom");
const modaleResultats = document.getElementById("modale-resultats");

function alea(min,max) {
  let nb = min + (max-min+1) * Math.random();
  return Math.floor(nb);
}

function melange(tableau) {
  let i;
  let tirage;
  let tab_alea = [];
  for (i = 0; i < tableau.length; i++) {
    do {
      tirage = alea(0, tableau.length -1);
    } while (tableau[tirage] === 0);
    tab_alea[i] = tableau[tirage];
    tableau[tirage] = 0;
  }
  return tab_alea;
}

function getChoixVerbe() {
  let nomVerbe;
  switch (paramExercice.verbe) {
    case "avoir":
      nomVerbe = "Avoir";
      break;
    case "etre":
      nomVerbe = "Être";
      break;
  }
  return nomVerbe;
}

function getChoixTemps() {
  let nomTemps;
  switch (paramExercice.tempsConjug) {
    case "indicatif-present":
      nomTemps = "Indicatif présent";
      break;
    case "indicatif-futur":
      nomTemps = "Indicatif futur";
      break;
    case "indicatif-imparfait":
      nomTemps = "Indicatif imparfait";
      break;
    case "indicatif-passe-compose":
      nomTemps = "Indicatif passé composé";
      break;
    case "conditionnel-present":
      nomTemps = "Conditionnel présent";
      break;
    case "subjonctif-present":
      nomTemps = "Subjonctif présent";
      break;
  }
  return nomTemps;
}

function tireIndices(nombre, tailleTableau) {
  let indice;
  let tabAlea = [];
  if (tailleTableau === 0) {
    return;
  }
  for (let i = 0; i < nombre; i++) {
    indice = alea(0,tailleTableau - 1);
    while (tabAlea.indexOf(indice) !== -1) {
      indice = alea(0,tailleTableau - 1);
    }
    tabAlea.push(indice);
  }
  return tabAlea;
}

function recupereContenu(listeIndices,tableau) {
  let listeContenu = [];
  if (tableau.length === 0) {
    return;
  }
/*   for (let i =0; i < listeIndices.length; i++) {
    listeContenu.push(tableau[listeIndices[i]]);
  } */
  listeContenu = listeIndices.map(function(indice){
    return tableau[indice];
  });
  return listeContenu;
}

function chargePhrases() {
  let listeIndices = [];
  let i = 0;
  let ctId = 0;
  let idPhrase = "";
  let phrase = "";
  if (paramExercice.verbe === "avoir") {
    listeIndices = tireIndices(paramExercice.nbPhrases,AVOIR[paramExercice.tempsConjug].length);
    paramExercice.listePhrases = recupereContenu(listeIndices,AVOIR[paramExercice.tempsConjug]);
  }
  else {
    listeIndices = tireIndices(paramExercice.nbPhrases,ETRE[paramExercice.tempsConjug].length);
    paramExercice.listePhrases = recupereContenu(listeIndices,ETRE[paramExercice.tempsConjug]);
  }
  for (i = 0; i < paramExercice.listePhrases.length; i++) {
    ctId = i + 1;
    idPhrase = "phrase-" + ctId;
    phrase = paramExercice.listePhrases[i].split('/');
    document.getElementById(idPhrase + "-debut").innerHTML = phrase[0];
    document.getElementById(idPhrase + "-fin").innerHTML = phrase[2];
    paramExercice.solutions[i] = phrase[1];
  }
}

function activePhrase(numero) {
  let idPhrase = "phrase-" + numero;
  document.getElementById(idPhrase).classList.replace("zoneExercice_form--invisible","zoneExercice_form--visible");
  idPhrase = idPhrase + "-saisie";
  document.getElementById(idPhrase).focus();
  annonce.innerHTML = messages.verifier;
  btVerifier.disabled = false;
  btSolution.disabled = true;
}

function eteintPhrase(numero,reussite) {
  let classe = "zoneExercice_solution--" + reussite;
  let phrase = paramExercice.listePhrases[numero - 1].split('/');
  let idPhrase = "phrase-" + numero;
  document.getElementById(idPhrase).classList.replace("zoneExercice_form--visible","zoneExercice_form--invisible");
  idPhrase = idPhrase + "-solution";
  document.getElementById(idPhrase).innerHTML = phrase[0] + "<span class='" + classe + "'>" + phrase[1] + "</span>" + phrase[2];
  document.getElementById(idPhrase).classList.replace("zoneExercice_solution--invisible","zoneExercice_solution");
}

function construitTabResultats() {
  let ctVerbe = 0;
  let ctTemps = 0;
  let verbe, temps;
  let contenu = "";
  let idCell = "";
  let taux1 = 0;
  let taux2 = 0;
  let nbEchecs = 0;
  for (verbe in paramResultats.ctTotalTentatives) {
    ctTemps = 0;
    for (temps in paramResultats.ctTotalTentatives[verbe]) {
      taux1 = 0;
      taux2 = 0;
      if ( paramResultats.ctTotalTentatives[verbe][temps] > 0 ) {
        taux1 = Math.round((paramResultats.ctPhrases[verbe][temps] * 100) / paramResultats.ctTotalTentatives[verbe][temps]);
      }
      contenu = paramResultats.ctPhrases[verbe][temps] + "<br />" + paramResultats.ctTotalTentatives[verbe][temps] + "<br />";
      if ( taux1 ) {
        contenu = contenu +  taux1 + "%<br>";
      }
      else {
        contenu = contenu +  "<br />";
      }
      nbEchecs = paramResultats.ctPhrases[verbe][temps] - paramResultats.ctReussis[verbe][temps];
      contenu = contenu + nbEchecs + "<br />";
      if ( paramResultats.ctPhrases[verbe][temps] > 0 ) {
        taux2 = Math.round((paramResultats.ctReussis[verbe][temps] * 100) / paramResultats.ctPhrases[verbe][temps]);
      }
      if ( taux2 ) {
        contenu = contenu + taux2 + "%";
      }
      idCell = "resu-" + ctVerbe + "-" + ctTemps;
      document.getElementById(idCell).innerHTML = contenu;
      ctTemps += 1;
    }
    ctVerbe += 1;
  }
}

function defCouleur(taux) {
  let couleur = "";
  switch (true) {
    case (taux < 40):
      couleur = "red";
      break;
    case (taux < 70):
      couleur = "tomato";
      break;
    case (taux <= 100):
      couleur = "green";
  }
  return couleur;
}

function abandonner() {
  inscritResultats("echec");
  eteintPhrase(paramExercice.numeroPhrase,"abandon");
  paramExercice.numeroPhrase += 1;
  if (paramExercice.numeroPhrase <= paramExercice.listePhrases.length) {
    activePhrase(paramExercice.numeroPhrase);
  }
  else {
    annonce.innerHTML = messages.nouvelExercice;
  }
  paramResultats.ctTentatives = 0;
}

function afficheResultats() {
  let taux1 = 0;
  let taux2 = 0;
  const verbe = paramExercice.verbe;
  const temps = paramExercice.tempsConjug;
  if ((verbe === -1) || (temps === -1)) {
    return;
  }
  if ( paramResultats.ctTotalTentatives[verbe][temps] > 0 ) {
    taux1 = Math.round((paramResultats.ctPhrases[verbe][temps] * 100) / paramResultats.ctTotalTentatives[verbe][temps]);
  }
  nbEpreuves.innerHTML = paramResultats.ctPhrases[verbe][temps];
  if ( taux1 ) { tauxEfficacite.innerHTML = taux1 + "%"; }
  tauxEfficacite.style.color = defCouleur(taux1);
  totalTentatives.innerHTML = paramResultats.ctTotalTentatives[verbe][temps];
  nbEchecs.innerHTML = paramResultats.ctPhrases[verbe][temps] - paramResultats.ctReussis[verbe][temps];
  if ( paramResultats.ctPhrases[verbe][temps] > 0 ) {
    taux2 = Math.round((paramResultats.ctReussis[verbe][temps] * 100) / paramResultats.ctPhrases[verbe][temps]);
  }
  if ( taux2 ) { tauxReussite.innerHTML = taux2 + "%"; }
  tauxReussite.style.color = defCouleur(taux2);
}

function effaceResultats() {
  nbTentatives.innerHTML = "";
  nbEpreuves.innerHTML = "";
  nbTentatives.innerHTML = "";
  tauxEfficacite.innerHTML = "";
  nbEchecs.innerHTML = "";
  tauxReussite.innerHTML = "";
}

function lanceExercice() {
  affichageTypeExo.innerHTML = getChoixVerbe() + "<br>" + getChoixTemps();
  chargePhrases();
  activePhrase(1);
  paramExercice.numeroPhrase = 1;
  btchange.disabled = true;
  zoneSelection.classList.replace("zoneAnnexe_zoneSelection--visible","zoneAnnexe_zoneSelection--invisible");
  affichageTypeExo.classList.replace("zoneAnnexe_affichage--invisible","zoneAnnexe_affichage--visible");
}

function lanceVerbe(verbe) {
  paramExercice.verbe = verbe;
  if (paramExercice.tempsConjug !== "") {
    lanceExercice();
  }
}

function lanceTemps(choix) {
  paramExercice.tempsConjug = choix;
  if (paramExercice.verbe !== "") {
    lanceExercice();
  }
}

function changeExercice() {
  let i;
  const idBase = "phrase-";
  let idPhrase;
  effaceResultats();
  document.querySelectorAll('.zoneAnnexe_zoneSelection_radio').forEach(function(item){item.checked = false});
  for (i = 1; i <= paramExercice.nbPhrases; i++) {
    idPhrase = idBase + i + "-solution";
    document.getElementById(idPhrase).classList.replace("zoneExercice_solution","zoneExercice_solution--invisible");
    idPhrase = idBase + i + "-saisie";
    document.getElementById(idPhrase).value = "";
  }
  zoneSelection.classList.replace("zoneAnnexe_zoneSelection--invisible","zoneAnnexe_zoneSelection--visible");
  affichageTypeExo.classList.replace("zoneAnnexe_affichage--visible","zoneAnnexe_affichage--invisible");
  paramExercice.verbe = "";
  paramExercice.tempsConjug = "";
  btchange.disabled = true;
}

function effaceResultats() {
  nbTentatives.innerHTML = "";
  totalTentatives.innerHTML = "";
  nbEpreuves.innerHTML = "";
  nbTentatives.innerHTML = "";
  tauxEfficacite.innerHTML = "";
  nbEchecs.innerHTML = "";
  tauxReussite.innerHTML = "";
}

function inscritResultats(type) {
  paramResultats.ctTotalTentatives[paramExercice.verbe][paramExercice.tempsConjug] = paramResultats.ctTotalTentatives[paramExercice.verbe][paramExercice.tempsConjug] + paramResultats.ctTentatives + 1;
  paramResultats.ctPhrases[paramExercice.verbe][paramExercice.tempsConjug] += 1;
  if ( type === "reussite") {
    paramResultats.ctReussis[paramExercice.verbe][paramExercice.tempsConjug] += 1;
  }
  afficheResultats();
}

function verifie(ev) {
  ev.preventDefault();
  const idSaisie =  "phrase-" + paramExercice.numeroPhrase + "-saisie";
  const reponse = document.getElementById(idSaisie);
  let i = 0;
  let ctErreurs = paramResultats.ctTentatives;
  let proposition = reponse.value;
  nbTentatives.innerHTML = paramResultats.ctTentatives + 1;
  if (proposition === paramExercice.solutions[paramExercice.numeroPhrase-1]) {
    inscritResultats("reussite");
    if (ctErreurs > 1) {
      eteintPhrase(paramExercice.numeroPhrase, "moyen");
    }
    else {
      eteintPhrase(paramExercice.numeroPhrase,"optimal");
    }
    paramExercice.numeroPhrase += 1;
    if (paramExercice.numeroPhrase <= paramExercice.listePhrases.length) {
      annonce.innerHTML = messages.bravo;
      activePhrase(paramExercice.numeroPhrase);
      paramResultats.ctTentatives = 0;
      paramResultats.ctTentatives = 0;
      return;
    }
    else {
      annonce.innerHTML = messages.felicitations;
      btchange.disabled = false;
      btVerifier.disabled = true;
    }
    return;
  }
  if (ctErreurs >= 2) {
    ctErreurs = 2;
    btSolution.disabled = false;
  };
  annonce.innerHTML = messages.erreur[ctErreurs];
  paramResultats.ctTentatives += 1;
}

function quitteModaleNom() {
  ecranBlanc.classList.replace("visible","invisible");
  saisieNom.classList.replace("visible","invisible");
}

function quittePageResultats() {
  ecranBlanc.classList.replace("visible","invisible");
  modaleResultats.classList.replace("visible","invisible");
}

function quitteModaleInfos() {
  ecranBlanc.classList.replace("visible","invisible");
  modaleInfos.classList.replace("visible","invisible");
}

function demandeNom() {
  ecranBlanc.classList.replace("invisible","visible");
  saisieNom.classList.replace("invisible","visible");
  document.getElementById("patronyme").focus();
}

function pageResultats(ev) {
  let laDate = new Date();
  let zoneDate = document.getElementById('date');
  let Nom = document.getElementById('nom');
  ev.preventDefault();
  quitteModaleNom();
  ecranBlanc.classList.replace("invisible","visible");
  modaleResultats.classList.replace("invisible","visible");
  zoneDate.innerHTML = "Date : " + laDate.getDate() +"/" + (laDate.getMonth()+1) +"/" + laDate.getFullYear();
  Nom.innerHTML = nomEleve.value;
  construitTabResultats();
}

function afficheInfos() {
  ecranBlanc.classList.replace("invisible","visible");
  modaleInfos.classList.replace("invisible","visible");
  document.getElementById("btquitter").focus();
}

function initialise() {
  annonce.innerHTML = messages.accueil;
  effaceResultats();
}

window.onload = initialise();
