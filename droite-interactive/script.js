// Droite interactive //
// Arnaud Champollion, 2024 //
// Licence GNU/GPL //

let prefixeAppli = 'droite-interactive';

// Est-on dans Openboard ?
openboard = Boolean(window.widget || window.sankore);
//----------------------------

// Appels au DOM
const educajou = document.getElementById('educajou');
const droite = document.getElementById('droite');
const inputNombreMin = document.getElementById('input-nombre-min');
const inputNombreMax = document.getElementById('input-nombre-max');
const inputNombreCourant = document.getElementById('input-nombre-courant');
const inputOpacite = document.getElementById('input-opacite');
const inputPoliceNombres = document.getElementById('input-police-nombres');
const inputOpaciteNombres = document.getElementById('input-opacite-nombres');
const zoneDroite = document.getElementById('zone-droite');
const blocModele = document.getElementById('bloc-modele');
const pontsModele = document.getElementById('ponts-modele');
const pontModele = document.getElementById('pont-modele');
const sucetteModele = document.getElementById('sucette-modele');
const modeles = document.getElementById('modeles');
const panneau = document.getElementById('panneau');
const boutonPanneau = document.getElementById('bouton-panneau');
const boutonPoubelle = document.getElementById('bouton-poubelle');
const boutonsVisibiliteValeurs = document.querySelectorAll('#visibilite-valeurs .bouton-panneau');
const boutonsVisibiliteGraduations = document.querySelectorAll('#visibilite-graduations .bouton-panneau');
const boutonGraduations1 = document.getElementById('bouton-graduations-1');
const boutonValeurs1 = document.getElementById('bouton-valeurs-1');
const modeleSvg = document.getElementById('modele-svg-fleche');
const boutonVisibiliteDroite = document.getElementById('bouton-visibilite-droite');
const inputZoom = document.getElementById('inputZoom');


// Couleurs des sucettes
const colors = [
    "#ffff00", // Jaune
    "#99ccff", // Cyan
    "#ff9966", // Orange foncé
    "#990099", // Violet
    "#00ff00", // Vert
    "#000000", // Noir
    "#0000cc", // Bleu foncé
    "#ff2f00", // Rouge
    "#ff66ff", // Rose
    "#009933", // Vert foncé
    "#800000", // Marron
    "#00ffcc"  // Turquoise    
];

// Couleurs des blocs
const couleursCuisineaire = [
    { valeur: 1, rgbaCouleur: "rgba(255, 255, 255, 1)" },    // Blanc
    { valeur: 2, rgbaCouleur: "rgba(255, 51, 0, 1)" },        // Rouge
    { valeur: 3, rgbaCouleur: "rgba(204, 255, 204, 1)" },        // Vert clair
    { valeur: 4, rgbaCouleur: "rgba(255, 153, 204, 1)" },      // Rose
    { valeur: 5, rgbaCouleur: "rgba(255, 255, 0, 1)" },      // Jaune
    { valeur: 6, rgbaCouleur: "rgba(0, 128, 0, 1)" },        // Vert foncé
    { valeur: 7, rgbaCouleur: "rgba(0,0,0,1)" },      // Noir
    { valeur: 8, rgbaCouleur: "rgba(153, 102, 51, 1)" },    // Marron
    { valeur: 9, rgbaCouleur: "rgba(27, 27, 170)" },          // Bleu foncé
    { valeur: 0, rgbaCouleur: "rgba(255, 102, 0, 1)" }        // Orange
];

// Variables globales
let primtux = false;
let menuOuvert = false;
let panneauVisible = false;
let dragged = null;
let selected = null;
let isScrollByFunction = false;
let graduationProche = null;
let blocProche = null;
let oldGraduationProche = null;
let objetARedimensionner = null;
let redim = null;
let ecart = 100;
let nombreMin = 0;
let nombreMax = 5;
let nombreCourant = 0;
let difference = 5;
let reductionPolice = 2;
let colorIndex = 0;
let opacite = 1;
let opaciteDesNombres = 1;
let hauteur = 1;
let afficherLesValeurs = true;
let visibiliteDroiteUrl = true;
let taillePolice = 50;
let visibiliteTypeValeurs = 1;
let visibiliteTypeGraduations = 1;
let ecartReel = ecart * visibiliteTypeValeurs;
let zoomAffichage = 100;




function appliqueReglages() {
    taillePolice = Math.max(Math.min(60,ecartReel/reductionPolice),10);
    document.documentElement.style.setProperty('--opacite-fond', opaciteDesNombres);    
    document.documentElement.style.setProperty('--taille-police-nombres', taillePolice);    
    inputNombreMin.value = nombreMin;
    inputNombreMax.value = nombreMax;
    inputNombreCourant.value = nombreCourant;
    inputNombreCourant.min = nombreMin;
    inputNombreCourant.max = nombreMax;
    inputOpacite.value = opacite;
    inputPoliceNombres.value = taillePolice;
    inputOpaciteNombres.value = opaciteDesNombres;
    boutonsVisibiliteGraduations.forEach(bouton => {
        bouton.classList.remove('actif');
    });
    boutonsVisibiliteValeurs.forEach(bouton => {
        bouton.classList.remove('actif');
    });
    inputZoom.value=zoomAffichage;
    inputPoliceNombres.value = taillePolice;

    console.log("Applique réglage Primtux "+primtux)
    if (primtux) {        
        educajou.style.display='none';
    }

    if (visibiliteDroiteUrl===false) {
        visibiliteDroite();
    }
}


// Début du programme
async function executeFunctions() {
    await checkReglages();
    appliqueReglages();
    changeColors();
    zoom(zoomAffichage);    
    creeGraduations();
    allerA(nombreCourant);
    visibiliteValeurs(visibiliteTypeValeurs);
    visibiliteGraduations(visibiliteTypeGraduations);
}
executeFunctions();
///////////////////////

async function checkReglages() {
    console.log('--- Lecture URL et stockage local');
    let url = window.location.search;
    let urlParams = new URLSearchParams(url);
    let valeurARecuperer;


    let primtux = urlParams.get('primtuxmenu');
    if (primtux){
        body.classList.add('primtux');
    }

    valeurARecuperer = urlParams.get('min') || await litDepuisStockage('min');
    if (valeurARecuperer) {
        nombreMin = parseInt(valeurARecuperer);
    }

    valeurARecuperer = urlParams.get('max') || await litDepuisStockage('max');
    if (valeurARecuperer) {
        nombreMax = parseInt(valeurARecuperer);
    }

    valeurARecuperer = urlParams.get('allera') || await litDepuisStockage('allera');
    if (valeurARecuperer) {
        nombreCourant = parseInt(valeurARecuperer);
    }

    valeurARecuperer = urlParams.get('zoom') || await litDepuisStockage('zoom');
    if (valeurARecuperer) {
        zoomAffichage = parseInt(valeurARecuperer);
    }

    valeurARecuperer = urlParams.get('graduations') || await litDepuisStockage('graduations');
    if (valeurARecuperer) {
        visibiliteTypeGraduations = parseInt(valeurARecuperer);
    }

    valeurARecuperer = urlParams.get('nombres') || await litDepuisStockage('nombres');
    if (valeurARecuperer) {
        visibiliteTypeValeurs = parseInt(valeurARecuperer);
    }

    valeurARecuperer = urlParams.get('opacite-fond-nombres') || await litDepuisStockage('opacite-fond-nombres');
    if (valeurARecuperer) {
        opaciteDesNombres = parseInt(valeurARecuperer);
    }

    valeurARecuperer = urlParams.get('zoom') || await litDepuisStockage('zoom');
    if (valeurARecuperer) {
        zoomAffichage = parseInt(valeurARecuperer);
    }

    valeurARecuperer = urlParams.get('taille-police-nombres') || await litDepuisStockage('taille-police-nombres');
    if (valeurARecuperer) {
        taillePolice = parseInt(valeurARecuperer);
    }

    valeurARecuperer = urlParams.get('opacite-blocs') || await litDepuisStockage('opacite-blocs');
    if (valeurARecuperer) {
        opacite = parseFloat(valeurARecuperer);
    }

    valeurARecuperer = urlParams.get('afficher-valeurs') || await litDepuisStockage('afficher-valeurs');
    if (valeurARecuperer) {
        afficherLesValeurs = valeurARecuperer!='false';
    }

    valeurARecuperer = urlParams.get('afficher-droite') || await litDepuisStockage('afficher-droite');
    if (valeurARecuperer) {
        visibiliteDroiteUrl = valeurARecuperer!='false';
    }

   
}



function changeColors() {
    colorIndex++; 
    console.log(colors[colorIndex])    
    if (colorIndex>colors.length-1){colorIndex=0;}
    const color = colors[colorIndex];
    const children = sucetteModele.children;
    for (let i = 0; i < children.length; i++) {
        children[i].style.backgroundColor = color;
    }      
}

function policeNombres(value) {
    taillePolice = value;
    document.documentElement.style.setProperty('--taille-police-nombres', taillePolice + 'px');
    adaptePolice();
}


function visibiliteDroite() {
    droite.classList.toggle('hide');
    if (droite.classList.contains('hide')) {
        boutonVisibiliteDroite.innerText = "Afficher la droite";
        visibiliteDroiteUrl = false;
    } else {
        boutonVisibiliteDroite.innerText = "Masquer la droite";
        visibiliteDroiteUrl = true;
    }
    stocke('afficher-droite',visibiliteDroiteUrl);majUrl('afficher-droite',visibiliteDroiteUrl);


}


// Création des graduations
function creeGraduations() {
    return new Promise((resolve, reject) => {

        reductionPolice = 2; // La taille de police est la moitié de l'écart entre deux graduations
        difference = nombreMax - nombreMin; // Différence min / max       

        document.documentElement.style.setProperty('--taille-police-nombres', taillePolice + 'px');

        for (let i = nombreMin; i < nombreMax+1; i++) { // On crée autant de graduations que la différence + 1

            console.log('Grad '+i)
            console.log('Max '+nombreMax)

            let nouvelleGraduation = document.createElement('div');
            nouvelleGraduation.classList.add('graduation');
            let nouvelleValeur = document.createElement('span');

            nouvelleValeur.classList.add('valeur');
            nouvelleValeur.innerHTML=i; // Ajout de la valeur            

       
            if (i%100===0){ // Multiples de 100
                nouvelleGraduation.classList.add('graduation-100','graduation-2','graduation-5','graduation-10');
                nouvelleValeur.classList.add('valeur-100','valeur-2','valeur-5','valeur-10');
            }
            if (i%10===0){ // Multiples de 10
                nouvelleGraduation.classList.add('graduation-10','graduation-2','graduation-5');
                nouvelleValeur.classList.add('valeur-10','valeur-2','valeur-5');
            } else if (i%5===0){ // Multiples de 5
                nouvelleGraduation.classList.add('graduation-5');
                nouvelleValeur.classList.add('valeur-5');
                if (visibiliteTypeValeurs === 2 || visibiliteTypeValeurs === 10){nouvelleValeur.classList.add('hide')}
            } else if (i%2===0){ // Nombres pairs
                nouvelleGraduation.classList.add('graduation-2');
                nouvelleValeur.classList.add('valeur-2');
                if (visibiliteTypeValeurs === 5 || visibiliteTypeValeurs === 10){nouvelleValeur.classList.add('hide')}
            } else { // Autres
                nouvelleGraduation.classList.add('graduation-1');
                nouvelleValeur.classList.add('valeur-1');
                if (visibiliteTypeValeurs != 1){nouvelleValeur.classList.add('transparent')}
            } 
            if (visibiliteTypeValeurs === 0){nouvelleValeur.classList.add('transparent')}

            nouvelleGraduation.style.left=((i-nombreMin+1)*ecart)+'px'; // Positionnement de la graduation
            nouvelleGraduation.appendChild(nouvelleValeur); // Affectation de la valeur à sa graduation
            droite.appendChild(nouvelleGraduation); // Ajout de la graduation à la droite
        }

        // Mise à jour de la largeur de la droite
        droite.style.width=difference*ecart+2*ecart+'px';
        
        // Résolution de la promesse une fois le traitement terminé
        resolve();
    });
}

function monte(objet){
    console.log('monte '+hauteur+' '+objet.classList)
    objet.style.zIndex = hauteur;
    hauteur+=1;
    console.log('nouvelle hauteur = '+hauteur)
    panneau.style.zIndex = hauteur + 10;
    boutonPoubelle.style.zIndex = hauteur + 9;
    boutonPanneau.style.zIndex = hauteur + 9;
    const graduations = zoneDroite.querySelectorAll('.graduation');
    graduations.forEach (graduation => {
        graduation.style.zIndex = hauteur + 5;
    });
    droite.style.zIndex = hauteur + 5;
}

function visibiliteValeurs(multiple) {
    console.log('visibiliteValeurs',multiple)
    boutonsVisibiliteValeurs.forEach(bouton => {
        bouton.classList.remove('actif');
    });
    let bouton = document.getElementById(`input-visibilite-valeurs-${multiple}`);
    bouton.classList.add('actif');

    let valeurs = document.querySelectorAll('.valeur');
    valeurs.forEach(valeur => {
        valeur.classList.add('hide');
    });

    if (multiple === 0){
        visibiliteTypeValeurs = 0;
    }
    

    if (multiple === 10){
        let valeurs = document.querySelectorAll('.valeur-10');
        valeurs.forEach(valeur => {
            valeur.classList.remove('hide');
            visibiliteTypeValeurs = 10;
        });
    }

    if (multiple === 5){
        let valeurs = document.querySelectorAll('.valeur-5');
        valeurs.forEach(valeur => {
            valeur.classList.remove('hide');
            visibiliteTypeValeurs = 5;
        });
    }

    if (multiple === 2){
        let valeurs = document.querySelectorAll('.valeur-2');
        valeurs.forEach(valeur => {
            valeur.classList.remove('hide');
            visibiliteTypeValeurs = 2;
        });
    }

    if (multiple === 1){
        let valeurs = document.querySelectorAll('.valeur');
        valeurs.forEach(valeur => {
            valeur.classList.remove('hide');
            visibiliteTypeValeurs = 1;
        });
    }
    stocke('nombres',multiple);majUrl('nombres',multiple);
    changePolice();
    adaptePolice();
}

function visibiliteGraduations(multiple) {
    boutonsVisibiliteGraduations.forEach(bouton => {
        bouton.classList.remove('actif');
    });

    let bouton = document.getElementById(`input-visibilite-graduations-${multiple}`);

    bouton.classList.add('actif');

    let valeurs = document.querySelectorAll('.graduation');
    valeurs.forEach(valeur => {
        valeur.classList.add('transparent');
    });

    if (multiple === 0){
        visibiliteTypeGraduations = 0;
    }
    

    if (multiple === 10){
        let valeurs = document.querySelectorAll('.graduation-10');
        valeurs.forEach(valeur => {
            valeur.classList.remove('transparent');
            visibiliteTypeGraduations = 10;
        });
    }

    if (multiple === 5){
        let valeurs = document.querySelectorAll('.graduation-5');
        valeurs.forEach(valeur => {
            valeur.classList.remove('transparent');
            visibiliteTypeGraduations = 5;
        });
    }

    if (multiple === 2){
        let valeurs = document.querySelectorAll('.graduation-2');
        valeurs.forEach(valeur => {
            valeur.classList.remove('transparent');
            visibiliteTypeGraduations = 2;
        });
    }

    if (multiple === 1){
        let valeurs = document.querySelectorAll('.graduation');
        valeurs.forEach(valeur => {
            valeur.classList.remove('transparent');
            visibiliteTypeGraduations = 1;
        });
    }

    stocke('graduations',multiple);majUrl('graduations',multiple);
}

function creeBloc(type) {
    console.log("cree bloc");
    let nouveauBloc = document.createElement('div');
    nouveauBloc.classList.add('block');
    monte(nouveauBloc);    
    if (type === blocModele) {
        nouveauBloc.classList.add('bloc', 'draggable');
        nouveauBloc.style.width = ecart + 'px';
        nouveauBloc.style.backgroundColor='rgba(255,255,255,'+ opacite +')';
    } else if (type === pontsModele) {
        nouveauBloc.classList.add('ponts', 'draggable');
        nouveauBloc.style.width = ecart + 'px';
        nouveauBloc.style.backgroundSize = ecart + 'px 50px';
    } else if (type === pontModele) {
        nouveauBloc.classList.add('pont', 'draggable');
        nouveauBloc.style.width = ecart + 'px';
        // Clone l'élément SVG modèle
        let nouveauSvg = modeleSvg.cloneNode(true);
        // Supprime l'attribut 'id' du nouveau SVG
        nouveauSvg.removeAttribute('id');
        // Dimensions du SVG
        nouveauSvg.setAttribute('width', ecart + 'px');
        nouveauSvg.setAttribute('height', '50px');
        // Centrer le SVG horizontalement dans son conteneur
        nouveauSvg.setAttribute('x', (ecart - 100) / 2);
        // Centrer le SVG verticalement dans son conteneur
        nouveauSvg.setAttribute('y', (50 - 50) / 2);
        // Supprime la classe 'hide' du nouveau SVG
        nouveauSvg.classList.remove('hide');
        nouveauBloc.appendChild(nouveauSvg);
    } else if (type === sucetteModele) {
        nouveauBloc.classList.add('sucette', 'draggable');
        let nouvelleTete = document.createElement('div');        
        nouvelleTete.classList.add('tete-sucette');
        nouvelleTete.style.backgroundColor=colors[colorIndex];
        let nouveauManche = document.createElement('div');
        nouveauManche.classList.add('manche-sucette');
        nouveauManche.style.backgroundColor=colors[colorIndex];
        nouveauBloc.appendChild(nouvelleTete);
        nouveauBloc.appendChild(nouveauManche);
        changeColors();        
    }

    selectionne(nouveauBloc);
    
    let positionTop = type.offsetTop + modeles.offsetTop;
    let positionLeft = type.offsetLeft + modeles.offsetLeft - modeles.offsetWidth / 2 ;

    nouveauBloc.style.top = positionTop + 'px';
    nouveauBloc.style.left = positionLeft + 'px';
    
    if (type!=sucetteModele){
        let nouvelInput = document.createElement('input');
        nouvelInput.type = "number";
        nouvelInput.classList.add('nombre-bloc');
        if (!afficherLesValeurs){nouvelInput.classList.add('hide');}
        nouvelInput.value = 1;
        nouvelInput.min = 1;

        nouvelInput.addEventListener('change', function() {
            largeurBloc(nouveauBloc, nouvelInput.value);
        });
        nouveauBloc.appendChild(nouvelInput);
    }

    // Clic droit --> menu contextuel
    nouveauBloc.addEventListener('contextmenu', function(e) {
        e.preventDefault();
        menuContextuel(e, nouveauBloc);
    });

    zoneDroite.appendChild(nouveauBloc);

    return nouveauBloc;
}


function menuContextuel(e,bloc) {
    if (!menuOuvert){
        menuOuvert=true;
        // Création du menu contextuel
        menu = document.createElement('div');
        menu.classList.add('menu-contextuel');
        menu.style.zIndex = hauteur + 6;

        let texteOptionMasquer;
        let input = bloc.querySelector('input');
        if (input.classList.contains('hide')) {
            texteOptionMasquer = 'Afficher la valeur';
        } else  {
            texteOptionMasquer = 'Masquer la valeur';
        }

        // Création des options du menu
        let optionMasquer = document.createElement('div');
        optionMasquer.classList.add('option-menu');
        optionMasquer.textContent = texteOptionMasquer;
        let optionDupliquer = document.createElement('div');
        optionDupliquer.classList.add('option-menu');
        optionDupliquer.textContent = "Dupliquer";
        let optionRetourner = document.createElement('div');
        optionRetourner.classList.add('option-menu');
        optionRetourner.textContent = "Retourner";
        let optionSupprimer = document.createElement('div');
        optionSupprimer.classList.add('option-menu');
        optionSupprimer.textContent = "Supprimer";
        
        // Ajout des écouteurs d'événements pour les options du menu



        optionMasquer.addEventListener('click', function() {
            console.log('visibilite');
            visibiliteValeur(bloc);
            menu.remove();
            menuOuvert=false;
        });


        optionDupliquer.addEventListener('click', function() {
            console.log('duplique');
            dupliquer(bloc);
            menu.remove();
            menuOuvert=false;
        });

        optionRetourner.addEventListener('click', function() {
            retourner(bloc);
            menu.remove();
            menuOuvert=false;
        });

        optionSupprimer.addEventListener('click', function() {
            console.log('suppr');
            bloc.remove();
            menu.remove();
            menuOuvert=false;
        });

        // Ajout des options au menu
        menu.appendChild(optionMasquer);
        menu.appendChild(optionDupliquer);
        if (bloc.classList.contains('pont')){
            menu.appendChild(optionRetourner);
        }
        menu.appendChild(optionSupprimer);

        // Positionnement du menu contextuel
        menu.style.left = e.clientX + 'px';
        menu.style.top = e.clientY + 'px';

        // Ajout du menu au corps du document
        document.body.appendChild(menu);

        // Fermeture du menu contextuel lors du clic en dehors de celui-ci
        document.addEventListener('click', function(e) {
            if (!menu.contains(e.target)) {
                menu.remove();
                menuOuvert=false;
            }
        });
    }
}

function visibiliteValeur(bloc) {
    let input = bloc.querySelector('input');
    input.classList.toggle('hide');
}

function visibiliteDesValeurs(afficher) {
    console.log('Visibilité des valeurs '+afficher);
    let valeurs = document.querySelectorAll('.nombre-bloc');
    if (afficher){
        afficherLesValeurs = true;
        valeurs.forEach(input => {
            input.classList.remove('hide');
        });
    } else {
        afficherLesValeurs = false;
        valeurs.forEach(input => {
            input.classList.add('hide');
        }); 
    }
    stocke('afficher-valeurs',afficherLesValeurs);majUrl('afficher-valeurs',afficherLesValeurs);

}

function retourner(bloc){
    bloc.classList.toggle('retourne');
}

function dupliquer(bloc) {
    // Création d'un clone de l'élément
    var clone = bloc.cloneNode(true);
    
    // Récupération des dimensions de l'élément original
    var rect = bloc.getBoundingClientRect();
    
    // Calcul des nouvelles positions pour le clone
    var newX = bloc.offsetLeft + 10; // Décalage de 10px à droite
    var newY = bloc.offsetTop + 10; // Décalage de 10px en bas
    
    // Attribution des nouvelles positions
    clone.style.left = newX + 'px';
    clone.style.top = newY + 'px';

    clone.firstChild.addEventListener('change', function() {
        largeurBloc(clone, clone.firstChild.value);
    });

    // Clic droit --> menu contextuel
    clone.addEventListener('contextmenu', function(e) {
        e.preventDefault();
        menuContextuel(e,clone);
    });
    
    // Ajout du clone au même parent que l'élément original
    bloc.parentNode.appendChild(clone);
}

function supprimerBlocs() {
    let blocs = document.querySelectorAll('.bloc, .ponts, .pont, .sucette');
    blocs.forEach(bloc => {
        bloc.remove();
    });
    selected = null;
}

function largeurBloc(bloc, valeur) {
    console.log('largeur bloc '+bloc.classList);
    console.log('valeur '+valeur);    
    valeur = parseInt(valeur);
    let diff;
    if (!isNaN(valeur) && valeur > 0 && Number.isInteger(valeur)) {
        let largeur = valeur * ecart;
        diff = largeur - bloc.offsetWidth;
        console.log('largeur '+largeur);
        if (bloc.classList.contains('ponts')){
            console.log('agrandissement classe ponts');
            bloc.style.backgroundSize = ecart + 'px 50px';
            bloc.style.width = largeur + 'px';        
        } else if (bloc.classList.contains('pont')){
            console.log('agrandissement classe pont');
            bloc.style.width = largeur + 'px';
            let svg = bloc.querySelector('svg');
            svg.setAttribute('width', largeur);
            svg.setAttribute('viewBox', '0 0 '+ largeur + ' 50');
            let path = svg.getElementById('arc-de-cercle');
            path.setAttribute('d', d="m 2.5,50 a 47.5," + ((50/(valeur*(ecart/100)))-2.5) + " 0 0 1 " + (largeur - 5) + ",0")
        } else {
            bloc.style.width = largeur + 'px';
        }
        if (bloc.classList.contains('bloc')){
            couleurBloc(bloc,valeur);
        }
    }
    return diff;
}


















function creer(input){

    // Remise à zéro des graduations et valeurs
    droite.innerHTML='';
    // Récupération des valeurs du formulaire
    nombreMin = parseInt(inputNombreMin.value);
    nombreMax = parseInt(inputNombreMax.value);
    console.log('créer'+parseInt(input.value))

    
    if(Number.isInteger(nombreMin) && Number.isInteger(nombreMax)) { // Vérification si les nombres sont entiers
        // Mise à jour des min/max du nombre courant
        inputNombreCourant.min=nombreMin;
        inputNombreCourant.max=nombreMax;

        stocke('min',nombreMin);majUrl('min',nombreMin);
        stocke('max',nombreMax);majUrl('max',nombreMax);


        if (nombreCourant > inputNombreCourant.max) {
            inputNombreCourant.value = nombreCourant = inputNombreCourant.max;
        }
        if (nombreCourant < inputNombreCourant.min) {
            inputNombreCourant.value = nombreCourant = inputNombreCourant.min;
        }

        // Si le nombre courant n'est pas dans la plage, réajustement
        console.log('nombreCourant ' + nombreCourant)
        if(Number.isInteger(nombreCourant)){
            if (nombreCourant<nombreMin){
                inputNombreCourant.value=nombreCourant=nombreMin;
            }
            if (nombreCourant>nombreMax){
                inputNombreCourant.value=nombreCourant=nombreMax;
            }
        }
        
        if(nombreMin <= nombreMax) { // Vérification si nombreMin <= nombreMax
            creeGraduations().then(() => { // Création des graduations (attente)
                adaptePolice(); // Vérification des chevauchements d'étiquettes et adaptation de la police si nécessaire  
            });
        } else { // Sinon égalisation des valeurs            
            nombreMax = nombreCourant = nombreMin;
            inputNombreMin.value = nombreMin;
            inputNombreMax.value = nombreMax;
            inputNombreCourant.value = inputNombreCourant;
            // Puis lancement des mêmes fonctions
            creeGraduations().then(() => {
                adaptePolice();
            });           
        }
        stocke('allera',nombreCourant);majUrl('allera',nombreCourant);
    }

}

function allerA(valeur){
    console.log('aller à '+valeur)
    let nombre = parseInt(valeur);
    nombreCourant = nombre;
    if(Number.isInteger(nombre) && nombre >= nombreMin && nombre <= nombreMax) {
        let position = ((nombre-nombreMin)+1) * ecart - zoneDroite.offsetWidth/2;
        // Définir le drapeau pour indiquer que le scroll est déclenché par la fonction
        isScrollByFunction = true;
        stocke('allera',nombreCourant);majUrl('allera',nombreCourant);
        zoneDroite.scrollLeft = position;    
    }
}


function adaptePolice(){
    let valeurs = Array.from(document.getElementsByClassName('valeur'));
    let iterations = 0;
    let maxIterations = 10; // Limite le nombre d'itérations
    let chevauche;

    valeurs.forEach(valeur => {
        valeur.classList.remove('masquage-chevauchement');
    });
    
    do {
        chevauche = chevauchement();
        if (chevauche) {            
            //reductionPolice += 0.1;
            valeurs.forEach(valeur => {
                if (iterations===0) {
                    if (!valeur.classList.contains('valeur-2')) {
                        valeur.classList.add('masquage-chevauchement');
                    } else {
                        valeur.classList.remove('masquage-chevauchement');
                    }
                }
                else if (iterations===1) {
                    if (!valeur.classList.contains('valeur-5')) {
                        valeur.classList.add('masquage-chevauchement');
                    } else {
                        valeur.classList.remove('masquage-chevauchement');
                    }
                }
                else if (iterations===2) {
                    if (!valeur.classList.contains('valeur-10')) {
                        valeur.classList.add('masquage-chevauchement');
                    } else {
                        valeur.classList.remove('masquage-chevauchement');
                    }
                } else if (iterations===3) {
                    if (!valeur.classList.contains('valeur-100')) {
                        valeur.classList.add('masquage-chevauchement');
                    } else {
                        valeur.classList.remove('masquage-chevauchement');
                    }
                }   
            });
            iterations++;
        }
    } while (chevauche && iterations < maxIterations);
}


function chevauchement() {
    let valeurs = Array.from(document.getElementsByClassName('valeur')).filter(element => !element.classList.contains('masquage-chevauchement') && !element.classList.contains('hide'));
    for (let i = 0; i < valeurs.length - 1; i++) {
        let rect1 = valeurs[i].getBoundingClientRect();
        let rect2 = valeurs[i + 1].getBoundingClientRect();
        let valeur1 = valeurs[i].textContent;
        let valeur2 = valeurs[i + 1].textContent;
        if (rect1.right >= rect2.left) {
            console.log(`Chevauchement détecté entre "${valeur1}" et "${valeur2}"`);
            console.log(rect1.right+' '+rect2.left);
            return true; // Chevauchement détecté
        }
    }
    return false; // Pas de chevauchement détecté
}

function changePolice () {
    ecartReel = ecart * visibiliteTypeValeurs;
    taillePolice = Math.max(Math.min(60,ecartReel/reductionPolice),10);
    document.documentElement.style.setProperty('--taille-police-nombres', taillePolice + 'px');
    inputPoliceNombres.value = taillePolice;
    stocke('taille-police-nombres',taillePolice);majUrl('taille-police-nombres',taillePolice);
}

function zoom(valeur){
    
    changePolice();


    return new Promise((resolve, reject) => {
        oldNombreCourant = nombreCourant;
        ancienScroll = zoneDroite.scrollLeft;
        let ecartOld = ecart;

        changePolice();

        let graduations = document.querySelectorAll('.graduation');
        let blocs = document.querySelectorAll('.bloc, .ponts, .pont');
        let sucettes = document.querySelectorAll('.sucette');

        ecart = parseInt(valeur);
        let index = 0;

        graduations.forEach(graduation => {
            let nouvellePosition = (index+1)*ecart;
            graduation.style.left = nouvellePosition+'px';
            index += 1;
        });

        blocs.forEach(bloc => {
            let position = bloc.offsetLeft / ecartOld - 1;
            let input = bloc.querySelector('input');
            bloc.style.transition = 'all ease 0.2s';
            bloc.style.left = (position + 1) * ecart + 'px';
            largeurBloc(bloc,input.value);
            bloc.style.transition = null;
        });

        sucettes.forEach(sucette => {
            let position = sucette.offsetLeft / ecartOld - 1;
            sucette.style.left = (position + 1) * ecart + 'px';;
        });

        droite.style.width=difference*ecart+2*ecart+'px';

        // Résolution de la promesse après la mise à jour
        resolve();
    }).then(() => {
        // Appel de la fonction adaptePolice() après la mise à jour
        zoomAffichage = valeur;
        stocke('zoom',zoomAffichage);majUrl('zoom',zoomAffichage);
        adaptePolice();
        allerA(oldNombreCourant);
        
    });

    
}

/*
function zoom(sens){

    return new Promise((resolve, reject) => {
        oldNombreCourant = nombreCourant;
        ancienScroll = zoneDroite.scrollLeft;

        let graduations = document.querySelectorAll('.graduation');
        let blocs = document.querySelectorAll('.bloc, .ponts, .pont');
        let sucettes = document.querySelectorAll('.sucette');

        ecart += sens;
        let index = 0;

        graduations.forEach(graduation => {
            let nouvellePosition = (index+1)*ecart;
            graduation.style.left = nouvellePosition+'px';
            //graduation.firstChild.style.fontSize = ecart/reductionPolice + 'px';
            //graduation.firstChild.style.bottom=-3*ecart/4-5+'px';
            index += 1;
        });

        blocs.forEach(bloc => {
            let position = bloc.offsetLeft / (ecart - (sens)) - 1;
            let input = bloc.querySelector('input');
            bloc.style.transition = 'all ease 0.2s';
            bloc.style.left = (position + 1) * ecart + 'px';
            largeurBloc(bloc,input.value);
            bloc.style.transition = null;
        });

        sucettes.forEach(sucette => {
            let position = sucette.offsetLeft / (ecart - (sens)) - 1;
            sucette.style.left = (position + 1) * ecart + 'px';;
        });


        //largeurBloc(blocModele,1);
        //largeurBloc(pontModele,1);
        //largeurBloc(pontsModele,1);
        droite.style.width=difference*ecart+2*ecart+'px';

        // Résolution de la promesse après la mise à jour
        resolve();
    }).then(() => {
        // Appel de la fonction adaptePolice() après la mise à jour
        adaptePolice();
        allerA(oldNombreCourant);
        
    });

    
}
*/

function objetOuEnfantDe(cible, objet) {
    // Vérifie si la cible est égale à l'objet
    if (cible === objet) {
        return true;
    }
    // Vérifie si la cible est un descendant de l'objet
    let parent = cible.parentNode;
    while (parent) {
        if (parent === objet) {
            return true;
        }
        parent = parent.parentNode;
    }
    // Si aucun cas n'est vérifié, retourne false
    return false;
}

function estDescendantDe(objet, classe) {
    let parent = objet.parentNode;
    while (parent) {
        if (parent.classList && parent.classList.contains(classe)) {
            console.log('parent trouvé '+parent.classList);
            return parent;

        }
        parent = parent.parentNode;
    }
    // Si aucun ascendant avec la classe donnée n'est trouvé, retourne null
    return null;
}

function blocAredimensionner(event) {
    // Sélectionner tous les éléments de classe .bloc
    let blocs = document.querySelectorAll('.bloc, .pont, .ponts');

    // Récupérer la position horizontale et verticale de la souris
    let mouseX = event.clientX || event.targetTouches[0].clientX; // Adjust for touch event
    let mouseY = event.clientY || event.targetTouches[0].clientY; // Adjust for touch event

    // Parcourir tous les éléments .bloc
    for (let bloc of blocs) {
        // Récupérer les coordonnées du bloc
        let rect = bloc.getBoundingClientRect();
        let blocGauche = rect.left; // Position du bord gauche
        let blocDroite = rect.right; // Position du bord droit
        let blocHaut = rect.top; // Position du bord haut
        let blocBas = rect.bottom; // Position du bord bas

        // Vérifier si la souris est à moins de 5px du bord gauche ou droit du bloc
        if (mouseX >= blocGauche - 5 && mouseX <= blocGauche + 5 && mouseY >= blocHaut && mouseY <= blocBas) {
            // La souris est à moins de 5px du bord gauche du bloc
            objetARedimensionner = bloc;
            console.log('gauche');
            return -1;
        } else if (mouseX >= blocDroite - 5 && mouseX <= blocDroite + 5 && mouseY >= blocHaut && mouseY <= blocBas) {
            // La souris est à moins de 5px du bord droit du bloc
            objetARedimensionner = bloc;
            console.log('droite');
            return 1;
        }
    }

    // Si la souris n'est pas à proximité des bords gauche ou droit d'un bloc
    return null;
}


function clic(event){

    let cible = event.target;

    if (cible.parentNode === sucetteModele){cible = sucetteModele}

    if (menuOuvert && !cible.classList.contains('option-menu')){
        menu.remove();
        menuOuvert=false;
    }

    if (panneauVisible && !objetOuEnfantDe(cible,panneau)){
        visibilitePanneau();
    }

    redim = blocAredimensionner(event);
    console.log('clic redim='+redim)


    if (redim){
        zoneDroite.style.cursor = 'ew-resize';
        posXdepart = event?.targetTouches?.[0]?.clientX || event.clientX;
        posYdepart = event?.targetTouches?.[0]?.clientY || event.clientY;
        widthObjetARedimensionner = objetARedimensionner.offsetWidth;
        leftObjetARedimensionner = objetARedimensionner.offsetLeft; 
    } else {
    
        if (cible.classList.contains('draggable')) {
            dragged = cible;
            selectionne(dragged);
            clicked = cible;
        } else if (cible === blocModele || cible === pontModele || cible === pontsModele || cible === sucetteModele) {
            dragged = creeBloc(cible);
        } else {
            dragged = estDescendantDe(cible,'draggable'); 
            selectionne(dragged);
                   
        }

        if (cible === zoneDroite){selectionne();}

        if (dragged){
            monte(dragged);
            zoneDroite.style.cursor = 'grabbing';
            posX = event?.targetTouches?.[0]?.clientX || event.clientX;
            posY = event?.targetTouches?.[0]?.clientY || event.clientY;
            posX_objet = dragged.offsetLeft;
            posY_objet = dragged.offsetTop;
            diffsourisx = (posX - posX_objet);
            diffsourisy = (posY - posY_objet);
            dragged.classList.add('dragged');
        }

    }
}

zoneDroite.childElementCount

function move(event) {

    posX = event?.targetTouches?.[0]?.clientX || event.clientX;
    posY = event?.targetTouches?.[0]?.clientY || event.clientY;



    if (redim){

        event.preventDefault();

        let decalageX = posX - posXdepart;
        let largeur = widthObjetARedimensionner + (decalageX*redim)        
        objetARedimensionner.style.width = largeur + 'px';

        if (redim===-1){
            objetARedimensionner.style.left = leftObjetARedimensionner + decalageX + 'px';
        }

        if (objetARedimensionner.classList.contains('pont')){
            let valeur = largeur / ecart;
            let svg = objetARedimensionner.querySelector('svg');
            svg.setAttribute('width', largeur);
            svg.setAttribute('viewBox', '0 0 '+ largeur + ' 50');
            let path = svg.getElementById('arc-de-cercle');
            path.setAttribute('d', d="m 2.5,50 a 47.5," + ((50/(valeur*(ecart/100)))-2.5) + " 0 0 1 " + (largeur - 5) + ",0")
        }

        majValeurBloc(objetARedimensionner);
    }

    else if (dragged) {

        event.preventDefault();

        dragged.style.left = posX - diffsourisx + "px";
        dragged.style.top = posY - diffsourisy + "px";

        graduationProche = estProche(dragged, 'graduation', 20);     

        if (graduationProche){
            console.log("graduation proche")
            oldGraduationProche = graduationProche;
            graduationProche.classList.add('proche');
        } else {
            graduationProche = null;
            if (oldGraduationProche){
                oldGraduationProche.classList.remove('proche');
            }
            blocProche = estProche(dragged, 'block', 20);
            if (blocProche) {
                console.log("bloc proche")
            }  
        }

    } else {
        if (blocAredimensionner(event)){
            zoneDroite.style.cursor= 'ew-resize';
        } else {
            zoneDroite.style.cursor= null;
        }
    }
}

function release(event) {

    if (redim) {
        objetARedimensionner.style.transition = 'all 0.2s ease';
        let valeur = objetARedimensionner.querySelector('input').value;

        // Redimensionner l'élément et attendre la fin de l'exécution
        redimensionnerElement(objetARedimensionner, valeur).then(diff => {
            // Calculer la nouvelle largeur une fois que la fonction largeurBloc est terminée
            leftObjetARedimensionner = objetARedimensionner.offsetLeft;
            if (redim === -1) {
                console.log('diff ' + diff)
                objetARedimensionner.style.left = leftObjetARedimensionner - diff + 'px';
            }
            let objetTransition = objetARedimensionner;
            setTimeout(() => {
                objetTransition.style.transition = null;
            }, 200);

            redim = null;
            objetARedimensionner = null;
            zoneDroite.style.cursor = null;
        });
    }

    if (dragged) {        
        dragged.classList.remove('dragged');
        dragged.classList.remove('proche');
        zoneDroite.style.cursor = null;
        if (graduationProche){
            let leftGraduationProche = graduationProche.offsetLeft;
            if (leftGraduationProche){
                let draggedHeight = dragged.offsetHeight;
                let droiteTop = droite.offsetTop;
                if (dragged.classList.contains('sucette')){
                    dragged.style.left = leftGraduationProche - dragged.offsetWidth/2 + 'px';
                    dragged.style.top = droiteTop - draggedHeight + 'px';
                } else {
                    console.log(droiteTop);
                    console.log(draggedHeight);
                    dragged.style.left = leftGraduationProche + 'px';
                    dragged.style.top = droiteTop - draggedHeight + 6 + 'px';
                }
            }
        }

        if (blocProche){
            let rightBlocProche = blocProche.offsetLeft + blocProche.offsetWidth;
            let topBlocProche = blocProche.offsetTop;
            dragged.style.left = rightBlocProche + 'px';
            dragged.style.top = topBlocProche + 'px';}
        }

    blocProche= null;
    graduationProche = null;
    if (oldGraduationProche){
        oldGraduationProche.classList.remove('proche');
    }

    dragged=null;
        
}

// Fonction qui redimensionne un élément et renvoie une promesse une fois terminée
function redimensionnerElement(element, valeur) {
    return new Promise(resolve => {
        let diff = largeurBloc(element, valeur); // Appel de la fonction de redimensionnement    
        resolve(diff); // Résolution de la promesse une fois le redimensionnement terminé
    });
}

function majValeurBloc(bloc) {
    let valeur = calculeValeurBloc(bloc);
    let input = bloc.querySelector('input');
    if (valeur<1){valeur=1;}
    input.value = valeur;
    if (bloc.classList.contains('bloc')){
        couleurBloc(bloc,valeur);
    }
}

function calculeValeurBloc(bloc){
    let valeur = Math.round(bloc.offsetWidth / ecart);
    return valeur;
}


function selectionne(objet){
    console.log('selectionne '+objet)
    if (selected){
        selected.classList.remove('selection');
        selected = null;
    }
    if (objet){
        selected = objet;
        selected.classList.add('selection');
    }
    graduationProche = null;
}


function estProche(objet, classe, tolerance) {
    // Sélection de toutes les divs avec la classe passée en argument
    let elements = document.querySelectorAll('.' + classe);
    // Filtrer les éléments pour exclure l'objet
    elements = Array.from(elements).filter(element => element !== objet);

    // Récupérer les propriétés de l'objet à l'extérieur de la boucle
    let objetLeft = objet.offsetLeft;
    let objetWidth = objet.offsetWidth;      
    let objetBottom = objet.offsetTop + objet.offsetHeight;

    if (objet.classList.contains('sucette')){
        objetLeft += objetWidth / 2;
    }

    // Fonction pour vérifier si la position left de l'objet est proche de la position left d'une div de classe 'graduation'
    function isNearElement(objetLeft, elementRight, tolerance) {
        return Math.abs(objetLeft - elementRight) <= tolerance;
    }

    // Fonction pour vérifier si la position top de l'objet est proche de la position top de #droite
    function isNearTop(objetBottom, elementBottom, tolerance) {
        return Math.abs(objetBottom - elementBottom) <= tolerance;
    }

    // Fonction pour vérifier la position de l'objet par rapport aux éléments de la classe spécifiée
    function checkPosition() {
        for (let i = 0; i < elements.length; i++) {
            let elementRight = elements[i].offsetLeft + elements[i].offsetWidth;
            let elementBottom = classe !== 'graduation' ? elements[i].offsetTop + elements[i].offsetHeight : droite.offsetTop;
            
            if (isNearElement(objetLeft, elementRight, tolerance) && isNearTop(objetBottom, elementBottom, tolerance)) {
                // Faire quelque chose lorsque l'objet est proche d'un élément de la classe spécifiée
                return elements[i];
            }
        }
        return false;
    }

    // Appeler la fonction pour vérifier la position initiale
    return checkPosition();
}


function opaciteBlocs(valeur) {
    opacite = valeur;
    const blocs = zoneDroite.querySelectorAll('.bloc');
    blocs.forEach(bloc => {
        opaciteBloc(bloc,valeur);
    });
    stocke('opacite-blocs',opacite);majUrl('opacite-blocs',opacite);
}

function opaciteBloc(bloc,valeur){
    const couleurActuelle = window.getComputedStyle(bloc).getPropertyValue('background-color');
    const rgbaValues = couleurActuelle.match(/\d+(\.\d+)?/g); // Récupère les valeurs R, G, B et A
    const rgbaString = `rgba(${rgbaValues.slice(0, 3).join(', ')}, ${valeur})`; // Concatène les valeurs R, G, B et ajoute l'opacité
    bloc.style.backgroundColor = rgbaString;
}

function opaciteNombres(valeur) {
    opaciteDesNombres = valeur;
    document.documentElement.style.setProperty('--opacite-fond', opaciteDesNombres);
    stocke('opacite-fond-nombres',opaciteDesNombres);majUrl('opacite-fond-nombres',opaciteDesNombres);
}


function couleurBloc(bloc, valeur) {
    // Chercher la couleur correspondante à la valeur donnée
    const couleur = couleursCuisineaire.find(couleur => couleur.valeur === valeur%10);

    // Si la couleur correspondante est trouvée, changer le backgroundColor du bloc
    if (couleur) {
        const couleurActuelle = couleur.rgbaCouleur;
        const rgbaValues = couleurActuelle.match(/\d+(\.\d+)?/g); // Récupère les valeurs R, G, B et A
        const rgbaString = `rgba(${rgbaValues.slice(0, 3).join(', ')}, ${opacite})`; // Concatène les valeurs R, G, B et ajoute l'opacité
        bloc.style.backgroundColor = rgbaString;
    } else {
        // Si aucune couleur correspondante n'est trouvée, laisser le backgroundColor vide
        bloc.style.backgroundColor = '';
    }
}

function visibilitePanneau() {
    if (panneauVisible){
        panneau.style.left=null;
        boutonPanneau.style.left=null;     
    } else {
        panneau.style.left='0px';
        boutonPanneau.style.left='calc(100% + 5px)';
    }
    panneauVisible=!panneauVisible;
}

// Touch events
document.addEventListener("touchstart", clic);
document.addEventListener("touchend", release);
document.addEventListener('touchmove', function(event) {
    if (dragged){event.preventDefault();}
    move(event); // Call move function with event parameter
}, { passive: false });

// Mouse events
document.addEventListener("mousedown", clic);
document.addEventListener("mousemove", function(event) {
    move(event); // Call move function with event parameter
});
document.addEventListener("mouseup", release);





// Gérer l'événement keydown
document.addEventListener('keydown', function(event) {
    // Vérifier si la touche enfoncée est une flèche gauche ou droite
    if (event.key === 'ArrowRight') {
      // Faire défiler la zone vers la droite
      zoneDroite.scrollLeft += 10; // Vous pouvez ajuster cette valeur selon vos besoins
    } else if (event.key === 'ArrowLeft') {
      // Faire défiler la zone vers la gauche
      zoneDroite.scrollLeft -= 10; // Vous pouvez ajuster cette valeur selon vos besoins
    }
});


zoneDroite.addEventListener('scroll', function(e) {
    // Vérifier si le scroll est déclenché manuellement par l'utilisateur
    if (!isScrollByFunction) {
        console.log('scroll manuel');
        let scrollLeft = e.target.scrollLeft;
        if (scrollLeft != 0) {
            let position = nombreMin + Math.floor((scrollLeft + zoneDroite.offsetWidth/2 - 10 )/ ecart) - 1;
            inputNombreCourant.value = nombreCourant = position;
            stocke('allera',nombreCourant);majUrl('allera',nombreCourant);

        }
    }
    // Réinitialiser le drapeau après chaque événement de scroll
    isScrollByFunction = false;

});


document.addEventListener('keydown', function(event) {
    if (event.key === 'Delete' || event.key === 'Del') {
        selected.remove();
        selected = null;
    }
});