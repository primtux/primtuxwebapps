gdjs.infosCode = {};
gdjs.infosCode.GDinformationsObjects1= [];
gdjs.infosCode.GDinformationsObjects2= [];
gdjs.infosCode.GDscore2Objects1= [];
gdjs.infosCode.GDscore2Objects2= [];
gdjs.infosCode.GDscore1Objects1= [];
gdjs.infosCode.GDscore1Objects2= [];
gdjs.infosCode.GDmains3Objects1= [];
gdjs.infosCode.GDmains3Objects2= [];
gdjs.infosCode.GDmains2Objects1= [];
gdjs.infosCode.GDmains2Objects2= [];
gdjs.infosCode.GDmains1Objects1= [];
gdjs.infosCode.GDmains1Objects2= [];
gdjs.infosCode.GDmainsObjects1= [];
gdjs.infosCode.GDmainsObjects2= [];
gdjs.infosCode.GDtheatreObjects1= [];
gdjs.infosCode.GDtheatreObjects2= [];
gdjs.infosCode.GDfilleObjects1= [];
gdjs.infosCode.GDfilleObjects2= [];
gdjs.infosCode.GDgarconObjects1= [];
gdjs.infosCode.GDgarconObjects2= [];
gdjs.infosCode.GDmot_95951syllabeObjects1= [];
gdjs.infosCode.GDmot_95951syllabeObjects2= [];
gdjs.infosCode.GDmot_95952syllabesObjects1= [];
gdjs.infosCode.GDmot_95952syllabesObjects2= [];
gdjs.infosCode.GDmot_95953syllabesObjects1= [];
gdjs.infosCode.GDmot_95953syllabesObjects2= [];
gdjs.infosCode.GDfond_9595vertObjects1= [];
gdjs.infosCode.GDfond_9595vertObjects2= [];
gdjs.infosCode.GDbouton_9595retourObjects1= [];
gdjs.infosCode.GDbouton_9595retourObjects2= [];


gdjs.infosCode.mapOfGDgdjs_9546infosCode_9546GDbouton_95959595retourObjects1Objects = Hashtable.newFrom({"bouton_retour": gdjs.infosCode.GDbouton_9595retourObjects1});
gdjs.infosCode.eventsList0 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("fond_vert"), gdjs.infosCode.GDfond_9595vertObjects1);
{for(var i = 0, len = gdjs.infosCode.GDfond_9595vertObjects1.length ;i < len;++i) {
    gdjs.infosCode.GDfond_9595vertObjects1[i].setOpacity(200);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_retour"), gdjs.infosCode.GDbouton_9595retourObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.infosCode.mapOfGDgdjs_9546infosCode_9546GDbouton_95959595retourObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "menu", false);
}}

}


};

gdjs.infosCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.infosCode.GDinformationsObjects1.length = 0;
gdjs.infosCode.GDinformationsObjects2.length = 0;
gdjs.infosCode.GDscore2Objects1.length = 0;
gdjs.infosCode.GDscore2Objects2.length = 0;
gdjs.infosCode.GDscore1Objects1.length = 0;
gdjs.infosCode.GDscore1Objects2.length = 0;
gdjs.infosCode.GDmains3Objects1.length = 0;
gdjs.infosCode.GDmains3Objects2.length = 0;
gdjs.infosCode.GDmains2Objects1.length = 0;
gdjs.infosCode.GDmains2Objects2.length = 0;
gdjs.infosCode.GDmains1Objects1.length = 0;
gdjs.infosCode.GDmains1Objects2.length = 0;
gdjs.infosCode.GDmainsObjects1.length = 0;
gdjs.infosCode.GDmainsObjects2.length = 0;
gdjs.infosCode.GDtheatreObjects1.length = 0;
gdjs.infosCode.GDtheatreObjects2.length = 0;
gdjs.infosCode.GDfilleObjects1.length = 0;
gdjs.infosCode.GDfilleObjects2.length = 0;
gdjs.infosCode.GDgarconObjects1.length = 0;
gdjs.infosCode.GDgarconObjects2.length = 0;
gdjs.infosCode.GDmot_95951syllabeObjects1.length = 0;
gdjs.infosCode.GDmot_95951syllabeObjects2.length = 0;
gdjs.infosCode.GDmot_95952syllabesObjects1.length = 0;
gdjs.infosCode.GDmot_95952syllabesObjects2.length = 0;
gdjs.infosCode.GDmot_95953syllabesObjects1.length = 0;
gdjs.infosCode.GDmot_95953syllabesObjects2.length = 0;
gdjs.infosCode.GDfond_9595vertObjects1.length = 0;
gdjs.infosCode.GDfond_9595vertObjects2.length = 0;
gdjs.infosCode.GDbouton_9595retourObjects1.length = 0;
gdjs.infosCode.GDbouton_9595retourObjects2.length = 0;

gdjs.infosCode.eventsList0(runtimeScene);

return;

}

gdjs['infosCode'] = gdjs.infosCode;
