gdjs.optionsCode = {};
gdjs.optionsCode.GDbouton_95recommencerObjects1= [];
gdjs.optionsCode.GDbouton_95recommencerObjects2= [];
gdjs.optionsCode.GDbouton_95recommencerObjects3= [];
gdjs.optionsCode.GDbouton_95validerObjects1= [];
gdjs.optionsCode.GDbouton_95validerObjects2= [];
gdjs.optionsCode.GDbouton_95validerObjects3= [];
gdjs.optionsCode.GDbouton_95retourObjects1= [];
gdjs.optionsCode.GDbouton_95retourObjects2= [];
gdjs.optionsCode.GDbouton_95retourObjects3= [];
gdjs.optionsCode.GDcyclisteObjects1= [];
gdjs.optionsCode.GDcyclisteObjects2= [];
gdjs.optionsCode.GDcyclisteObjects3= [];
gdjs.optionsCode.GDajout_95uniteObjects1= [];
gdjs.optionsCode.GDajout_95uniteObjects2= [];
gdjs.optionsCode.GDajout_95uniteObjects3= [];
gdjs.optionsCode.GDajout_95dizaineObjects1= [];
gdjs.optionsCode.GDajout_95dizaineObjects2= [];
gdjs.optionsCode.GDajout_95dizaineObjects3= [];
gdjs.optionsCode.GDretrait_95dizaineObjects1= [];
gdjs.optionsCode.GDretrait_95dizaineObjects2= [];
gdjs.optionsCode.GDretrait_95dizaineObjects3= [];
gdjs.optionsCode.GDretrait_95uniteObjects1= [];
gdjs.optionsCode.GDretrait_95uniteObjects2= [];
gdjs.optionsCode.GDretrait_95uniteObjects3= [];
gdjs.optionsCode.GDfond_95paysage1Objects1= [];
gdjs.optionsCode.GDfond_95paysage1Objects2= [];
gdjs.optionsCode.GDfond_95paysage1Objects3= [];
gdjs.optionsCode.GDscoreObjects1= [];
gdjs.optionsCode.GDscoreObjects2= [];
gdjs.optionsCode.GDscoreObjects3= [];
gdjs.optionsCode.GDchoix_95ecartObjects1= [];
gdjs.optionsCode.GDchoix_95ecartObjects2= [];
gdjs.optionsCode.GDchoix_95ecartObjects3= [];
gdjs.optionsCode.GDecart_954Objects1= [];
gdjs.optionsCode.GDecart_954Objects2= [];
gdjs.optionsCode.GDecart_954Objects3= [];
gdjs.optionsCode.GDecart_9510Objects1= [];
gdjs.optionsCode.GDecart_9510Objects2= [];
gdjs.optionsCode.GDecart_9510Objects3= [];
gdjs.optionsCode.GDchoix_95nombresObjects1= [];
gdjs.optionsCode.GDchoix_95nombresObjects2= [];
gdjs.optionsCode.GDchoix_95nombresObjects3= [];
gdjs.optionsCode.GDnb_95maxiObjects1= [];
gdjs.optionsCode.GDnb_95maxiObjects2= [];
gdjs.optionsCode.GDnb_95maxiObjects3= [];
gdjs.optionsCode.GDnb_95miniObjects1= [];
gdjs.optionsCode.GDnb_95miniObjects2= [];
gdjs.optionsCode.GDnb_95miniObjects3= [];
gdjs.optionsCode.GDchoix_95multipleObjects1= [];
gdjs.optionsCode.GDchoix_95multipleObjects2= [];
gdjs.optionsCode.GDchoix_95multipleObjects3= [];
gdjs.optionsCode.GDouiObjects1= [];
gdjs.optionsCode.GDouiObjects2= [];
gdjs.optionsCode.GDouiObjects3= [];
gdjs.optionsCode.GDnonObjects1= [];
gdjs.optionsCode.GDnonObjects2= [];
gdjs.optionsCode.GDnonObjects3= [];
gdjs.optionsCode.GDecart_956Objects1= [];
gdjs.optionsCode.GDecart_956Objects2= [];
gdjs.optionsCode.GDecart_956Objects3= [];
gdjs.optionsCode.GDecart_958Objects1= [];
gdjs.optionsCode.GDecart_958Objects2= [];
gdjs.optionsCode.GDecart_958Objects3= [];

gdjs.optionsCode.conditionTrue_0 = {val:false};
gdjs.optionsCode.condition0IsTrue_0 = {val:false};
gdjs.optionsCode.condition1IsTrue_0 = {val:false};
gdjs.optionsCode.condition2IsTrue_0 = {val:false};
gdjs.optionsCode.condition3IsTrue_0 = {val:false};
gdjs.optionsCode.condition4IsTrue_0 = {val:false};
gdjs.optionsCode.conditionTrue_1 = {val:false};
gdjs.optionsCode.condition0IsTrue_1 = {val:false};
gdjs.optionsCode.condition1IsTrue_1 = {val:false};
gdjs.optionsCode.condition2IsTrue_1 = {val:false};
gdjs.optionsCode.condition3IsTrue_1 = {val:false};
gdjs.optionsCode.condition4IsTrue_1 = {val:false};


gdjs.optionsCode.eventsList0 = function(runtimeScene) {

{


gdjs.optionsCode.condition0IsTrue_0.val = false;
{
gdjs.optionsCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("4")) == 1;
}if (gdjs.optionsCode.condition0IsTrue_0.val) {
gdjs.optionsCode.GDecart_954Objects2.createFrom(runtimeScene.getObjects("ecart_4"));
{for(var i = 0, len = gdjs.optionsCode.GDecart_954Objects2.length ;i < len;++i) {
    gdjs.optionsCode.GDecart_954Objects2[i].setAnimation(1);
}
}}

}


{


gdjs.optionsCode.condition0IsTrue_0.val = false;
{
gdjs.optionsCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("4")) == 0;
}if (gdjs.optionsCode.condition0IsTrue_0.val) {
gdjs.optionsCode.GDecart_954Objects2.createFrom(runtimeScene.getObjects("ecart_4"));
{for(var i = 0, len = gdjs.optionsCode.GDecart_954Objects2.length ;i < len;++i) {
    gdjs.optionsCode.GDecart_954Objects2[i].setAnimation(0);
}
}}

}


{


gdjs.optionsCode.condition0IsTrue_0.val = false;
{
gdjs.optionsCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("10")) == 1;
}if (gdjs.optionsCode.condition0IsTrue_0.val) {
gdjs.optionsCode.GDecart_9510Objects2.createFrom(runtimeScene.getObjects("ecart_10"));
{for(var i = 0, len = gdjs.optionsCode.GDecart_9510Objects2.length ;i < len;++i) {
    gdjs.optionsCode.GDecart_9510Objects2[i].setAnimation(1);
}
}}

}


{


gdjs.optionsCode.condition0IsTrue_0.val = false;
{
gdjs.optionsCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("10")) == 0;
}if (gdjs.optionsCode.condition0IsTrue_0.val) {
gdjs.optionsCode.GDecart_9510Objects2.createFrom(runtimeScene.getObjects("ecart_10"));
{for(var i = 0, len = gdjs.optionsCode.GDecart_9510Objects2.length ;i < len;++i) {
    gdjs.optionsCode.GDecart_9510Objects2[i].setAnimation(0);
}
}}

}


{


gdjs.optionsCode.condition0IsTrue_0.val = false;
{
gdjs.optionsCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("6")) == 1;
}if (gdjs.optionsCode.condition0IsTrue_0.val) {
gdjs.optionsCode.GDecart_956Objects2.createFrom(runtimeScene.getObjects("ecart_6"));
{for(var i = 0, len = gdjs.optionsCode.GDecart_956Objects2.length ;i < len;++i) {
    gdjs.optionsCode.GDecart_956Objects2[i].setAnimation(1);
}
}}

}


{


gdjs.optionsCode.condition0IsTrue_0.val = false;
{
gdjs.optionsCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("6")) == 0;
}if (gdjs.optionsCode.condition0IsTrue_0.val) {
gdjs.optionsCode.GDecart_956Objects2.createFrom(runtimeScene.getObjects("ecart_6"));
{for(var i = 0, len = gdjs.optionsCode.GDecart_956Objects2.length ;i < len;++i) {
    gdjs.optionsCode.GDecart_956Objects2[i].setAnimation(0);
}
}}

}


{


gdjs.optionsCode.condition0IsTrue_0.val = false;
{
gdjs.optionsCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("8")) == 1;
}if (gdjs.optionsCode.condition0IsTrue_0.val) {
gdjs.optionsCode.GDecart_958Objects2.createFrom(runtimeScene.getObjects("ecart_8"));
{for(var i = 0, len = gdjs.optionsCode.GDecart_958Objects2.length ;i < len;++i) {
    gdjs.optionsCode.GDecart_958Objects2[i].setAnimation(1);
}
}}

}


{


gdjs.optionsCode.condition0IsTrue_0.val = false;
{
gdjs.optionsCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("8")) == 0;
}if (gdjs.optionsCode.condition0IsTrue_0.val) {
gdjs.optionsCode.GDecart_958Objects2.createFrom(runtimeScene.getObjects("ecart_8"));
{for(var i = 0, len = gdjs.optionsCode.GDecart_958Objects2.length ;i < len;++i) {
    gdjs.optionsCode.GDecart_958Objects2[i].setAnimation(0);
}
}}

}


{


gdjs.optionsCode.condition0IsTrue_0.val = false;
{
gdjs.optionsCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4)) == 1;
}if (gdjs.optionsCode.condition0IsTrue_0.val) {
gdjs.optionsCode.GDnonObjects2.createFrom(runtimeScene.getObjects("non"));
gdjs.optionsCode.GDouiObjects2.createFrom(runtimeScene.getObjects("oui"));
{for(var i = 0, len = gdjs.optionsCode.GDouiObjects2.length ;i < len;++i) {
    gdjs.optionsCode.GDouiObjects2[i].setAnimation(0);
}
}{for(var i = 0, len = gdjs.optionsCode.GDnonObjects2.length ;i < len;++i) {
    gdjs.optionsCode.GDnonObjects2[i].setAnimation(1);
}
}}

}


{


gdjs.optionsCode.condition0IsTrue_0.val = false;
{
gdjs.optionsCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4)) == 0;
}if (gdjs.optionsCode.condition0IsTrue_0.val) {
gdjs.optionsCode.GDnonObjects1.createFrom(runtimeScene.getObjects("non"));
gdjs.optionsCode.GDouiObjects1.createFrom(runtimeScene.getObjects("oui"));
{for(var i = 0, len = gdjs.optionsCode.GDouiObjects1.length ;i < len;++i) {
    gdjs.optionsCode.GDouiObjects1[i].setAnimation(1);
}
}{for(var i = 0, len = gdjs.optionsCode.GDnonObjects1.length ;i < len;++i) {
    gdjs.optionsCode.GDnonObjects1[i].setAnimation(0);
}
}}

}


};gdjs.optionsCode.mapOfGDgdjs_46optionsCode_46GDbouton_9595retourObjects1Objects = Hashtable.newFrom({"bouton_retour": gdjs.optionsCode.GDbouton_95retourObjects1});gdjs.optionsCode.mapOfGDgdjs_46optionsCode_46GDecart_95954Objects1Objects = Hashtable.newFrom({"ecart_4": gdjs.optionsCode.GDecart_954Objects1});gdjs.optionsCode.eventsList1 = function(runtimeScene) {

{


gdjs.optionsCode.condition0IsTrue_0.val = false;
{
gdjs.optionsCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("4")) < 2;
}if (gdjs.optionsCode.condition0IsTrue_0.val) {
gdjs.optionsCode.GDecart_954Objects2.createFrom(gdjs.optionsCode.GDecart_954Objects1);

{runtimeScene.getGame().getVariables().getFromIndex(1).getChild("4").add(1);
}{for(var i = 0, len = gdjs.optionsCode.GDecart_954Objects2.length ;i < len;++i) {
    gdjs.optionsCode.GDecart_954Objects2[i].setAnimation(gdjs.optionsCode.GDecart_954Objects2[i].getAnimation() + (1));
}
}}

}


{


gdjs.optionsCode.condition0IsTrue_0.val = false;
{
gdjs.optionsCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("4")) == 2;
}if (gdjs.optionsCode.condition0IsTrue_0.val) {
/* Reuse gdjs.optionsCode.GDecart_954Objects1 */
{runtimeScene.getGame().getVariables().getFromIndex(1).getChild("4").setNumber(0);
}{for(var i = 0, len = gdjs.optionsCode.GDecart_954Objects1.length ;i < len;++i) {
    gdjs.optionsCode.GDecart_954Objects1[i].setAnimation(0);
}
}}

}


};gdjs.optionsCode.mapOfGDgdjs_46optionsCode_46GDecart_95956Objects1Objects = Hashtable.newFrom({"ecart_6": gdjs.optionsCode.GDecart_956Objects1});gdjs.optionsCode.eventsList2 = function(runtimeScene) {

{


gdjs.optionsCode.condition0IsTrue_0.val = false;
{
gdjs.optionsCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("6")) < 2;
}if (gdjs.optionsCode.condition0IsTrue_0.val) {
gdjs.optionsCode.GDecart_956Objects2.createFrom(gdjs.optionsCode.GDecart_956Objects1);

{runtimeScene.getGame().getVariables().getFromIndex(1).getChild("6").add(1);
}{for(var i = 0, len = gdjs.optionsCode.GDecart_956Objects2.length ;i < len;++i) {
    gdjs.optionsCode.GDecart_956Objects2[i].setAnimation(gdjs.optionsCode.GDecart_956Objects2[i].getAnimation() + (1));
}
}}

}


{


gdjs.optionsCode.condition0IsTrue_0.val = false;
{
gdjs.optionsCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("6")) == 2;
}if (gdjs.optionsCode.condition0IsTrue_0.val) {
/* Reuse gdjs.optionsCode.GDecart_956Objects1 */
{runtimeScene.getGame().getVariables().getFromIndex(1).getChild("6").setNumber(0);
}{for(var i = 0, len = gdjs.optionsCode.GDecart_956Objects1.length ;i < len;++i) {
    gdjs.optionsCode.GDecart_956Objects1[i].setAnimation(0);
}
}}

}


};gdjs.optionsCode.mapOfGDgdjs_46optionsCode_46GDecart_95958Objects1Objects = Hashtable.newFrom({"ecart_8": gdjs.optionsCode.GDecart_958Objects1});gdjs.optionsCode.eventsList3 = function(runtimeScene) {

{


gdjs.optionsCode.condition0IsTrue_0.val = false;
{
gdjs.optionsCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("8")) < 2;
}if (gdjs.optionsCode.condition0IsTrue_0.val) {
gdjs.optionsCode.GDecart_958Objects2.createFrom(gdjs.optionsCode.GDecart_958Objects1);

{runtimeScene.getGame().getVariables().getFromIndex(1).getChild("8").add(1);
}{for(var i = 0, len = gdjs.optionsCode.GDecart_958Objects2.length ;i < len;++i) {
    gdjs.optionsCode.GDecart_958Objects2[i].setAnimation(gdjs.optionsCode.GDecart_958Objects2[i].getAnimation() + (1));
}
}}

}


{


gdjs.optionsCode.condition0IsTrue_0.val = false;
{
gdjs.optionsCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("8")) == 2;
}if (gdjs.optionsCode.condition0IsTrue_0.val) {
/* Reuse gdjs.optionsCode.GDecart_958Objects1 */
{runtimeScene.getGame().getVariables().getFromIndex(1).getChild("8").setNumber(0);
}{for(var i = 0, len = gdjs.optionsCode.GDecart_958Objects1.length ;i < len;++i) {
    gdjs.optionsCode.GDecart_958Objects1[i].setAnimation(0);
}
}}

}


};gdjs.optionsCode.mapOfGDgdjs_46optionsCode_46GDecart_959510Objects1Objects = Hashtable.newFrom({"ecart_10": gdjs.optionsCode.GDecart_9510Objects1});gdjs.optionsCode.eventsList4 = function(runtimeScene) {

{


gdjs.optionsCode.condition0IsTrue_0.val = false;
{
gdjs.optionsCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("10")) < 2;
}if (gdjs.optionsCode.condition0IsTrue_0.val) {
gdjs.optionsCode.GDecart_9510Objects2.createFrom(gdjs.optionsCode.GDecart_9510Objects1);

{runtimeScene.getGame().getVariables().getFromIndex(1).getChild("10").add(1);
}{for(var i = 0, len = gdjs.optionsCode.GDecart_9510Objects2.length ;i < len;++i) {
    gdjs.optionsCode.GDecart_9510Objects2[i].setAnimation(gdjs.optionsCode.GDecart_9510Objects2[i].getAnimation() + (1));
}
}}

}


{


gdjs.optionsCode.condition0IsTrue_0.val = false;
{
gdjs.optionsCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("10")) == 2;
}if (gdjs.optionsCode.condition0IsTrue_0.val) {
/* Reuse gdjs.optionsCode.GDecart_9510Objects1 */
{runtimeScene.getGame().getVariables().getFromIndex(1).getChild("10").setNumber(0);
}{for(var i = 0, len = gdjs.optionsCode.GDecart_9510Objects1.length ;i < len;++i) {
    gdjs.optionsCode.GDecart_9510Objects1[i].setAnimation(0);
}
}}

}


};gdjs.optionsCode.mapOfGDgdjs_46optionsCode_46GDajout_9595uniteObjects1Objects = Hashtable.newFrom({"ajout_unite": gdjs.optionsCode.GDajout_95uniteObjects1});gdjs.optionsCode.mapOfGDgdjs_46optionsCode_46GDretrait_9595uniteObjects1Objects = Hashtable.newFrom({"retrait_unite": gdjs.optionsCode.GDretrait_95uniteObjects1});gdjs.optionsCode.mapOfGDgdjs_46optionsCode_46GDajout_9595dizaineObjects1Objects = Hashtable.newFrom({"ajout_dizaine": gdjs.optionsCode.GDajout_95dizaineObjects1});gdjs.optionsCode.mapOfGDgdjs_46optionsCode_46GDretrait_9595dizaineObjects1Objects = Hashtable.newFrom({"retrait_dizaine": gdjs.optionsCode.GDretrait_95dizaineObjects1});gdjs.optionsCode.mapOfGDgdjs_46optionsCode_46GDouiObjects1Objects = Hashtable.newFrom({"oui": gdjs.optionsCode.GDouiObjects1});gdjs.optionsCode.mapOfGDgdjs_46optionsCode_46GDnonObjects1Objects = Hashtable.newFrom({"non": gdjs.optionsCode.GDnonObjects1});gdjs.optionsCode.eventsList5 = function(runtimeScene) {

{


gdjs.optionsCode.condition0IsTrue_0.val = false;
{
gdjs.optionsCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.optionsCode.condition0IsTrue_0.val) {
gdjs.optionsCode.GDnb_95maxiObjects1.createFrom(runtimeScene.getObjects("nb_maxi"));
gdjs.optionsCode.GDnb_95miniObjects1.createFrom(runtimeScene.getObjects("nb_mini"));
{for(var i = 0, len = gdjs.optionsCode.GDnb_95miniObjects1.length ;i < len;++i) {
    gdjs.optionsCode.GDnb_95miniObjects1[i].setString(gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("mini"))));
}
}{for(var i = 0, len = gdjs.optionsCode.GDnb_95maxiObjects1.length ;i < len;++i) {
    gdjs.optionsCode.GDnb_95maxiObjects1[i].setString(gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("maxi"))));
}
}
{ //Subevents
gdjs.optionsCode.eventsList0(runtimeScene);} //End of subevents
}

}


{

gdjs.optionsCode.GDbouton_95retourObjects1.createFrom(runtimeScene.getObjects("bouton_retour"));

gdjs.optionsCode.condition0IsTrue_0.val = false;
gdjs.optionsCode.condition1IsTrue_0.val = false;
{
gdjs.optionsCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.optionsCode.condition0IsTrue_0.val ) {
{
gdjs.optionsCode.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.optionsCode.mapOfGDgdjs_46optionsCode_46GDbouton_9595retourObjects1Objects, runtimeScene, true, false);
}}
if (gdjs.optionsCode.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "menu", false);
}}

}


{

gdjs.optionsCode.GDecart_954Objects1.createFrom(runtimeScene.getObjects("ecart_4"));

gdjs.optionsCode.condition0IsTrue_0.val = false;
gdjs.optionsCode.condition1IsTrue_0.val = false;
gdjs.optionsCode.condition2IsTrue_0.val = false;
{
gdjs.optionsCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.optionsCode.condition0IsTrue_0.val ) {
{
gdjs.optionsCode.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.optionsCode.mapOfGDgdjs_46optionsCode_46GDecart_95954Objects1Objects, runtimeScene, true, false);
}if ( gdjs.optionsCode.condition1IsTrue_0.val ) {
{
{gdjs.optionsCode.conditionTrue_1 = gdjs.optionsCode.condition2IsTrue_0;
gdjs.optionsCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(10161596);
}
}}
}
if (gdjs.optionsCode.condition2IsTrue_0.val) {

{ //Subevents
gdjs.optionsCode.eventsList1(runtimeScene);} //End of subevents
}

}


{

gdjs.optionsCode.GDecart_956Objects1.createFrom(runtimeScene.getObjects("ecart_6"));

gdjs.optionsCode.condition0IsTrue_0.val = false;
gdjs.optionsCode.condition1IsTrue_0.val = false;
gdjs.optionsCode.condition2IsTrue_0.val = false;
{
gdjs.optionsCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.optionsCode.condition0IsTrue_0.val ) {
{
gdjs.optionsCode.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.optionsCode.mapOfGDgdjs_46optionsCode_46GDecart_95956Objects1Objects, runtimeScene, true, false);
}if ( gdjs.optionsCode.condition1IsTrue_0.val ) {
{
{gdjs.optionsCode.conditionTrue_1 = gdjs.optionsCode.condition2IsTrue_0;
gdjs.optionsCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(10164300);
}
}}
}
if (gdjs.optionsCode.condition2IsTrue_0.val) {

{ //Subevents
gdjs.optionsCode.eventsList2(runtimeScene);} //End of subevents
}

}


{

gdjs.optionsCode.GDecart_958Objects1.createFrom(runtimeScene.getObjects("ecart_8"));

gdjs.optionsCode.condition0IsTrue_0.val = false;
gdjs.optionsCode.condition1IsTrue_0.val = false;
gdjs.optionsCode.condition2IsTrue_0.val = false;
{
gdjs.optionsCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.optionsCode.condition0IsTrue_0.val ) {
{
gdjs.optionsCode.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.optionsCode.mapOfGDgdjs_46optionsCode_46GDecart_95958Objects1Objects, runtimeScene, true, false);
}if ( gdjs.optionsCode.condition1IsTrue_0.val ) {
{
{gdjs.optionsCode.conditionTrue_1 = gdjs.optionsCode.condition2IsTrue_0;
gdjs.optionsCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(10167244);
}
}}
}
if (gdjs.optionsCode.condition2IsTrue_0.val) {

{ //Subevents
gdjs.optionsCode.eventsList3(runtimeScene);} //End of subevents
}

}


{

gdjs.optionsCode.GDecart_9510Objects1.createFrom(runtimeScene.getObjects("ecart_10"));

gdjs.optionsCode.condition0IsTrue_0.val = false;
gdjs.optionsCode.condition1IsTrue_0.val = false;
gdjs.optionsCode.condition2IsTrue_0.val = false;
{
gdjs.optionsCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.optionsCode.condition0IsTrue_0.val ) {
{
gdjs.optionsCode.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.optionsCode.mapOfGDgdjs_46optionsCode_46GDecart_959510Objects1Objects, runtimeScene, true, false);
}if ( gdjs.optionsCode.condition1IsTrue_0.val ) {
{
{gdjs.optionsCode.conditionTrue_1 = gdjs.optionsCode.condition2IsTrue_0;
gdjs.optionsCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(10169924);
}
}}
}
if (gdjs.optionsCode.condition2IsTrue_0.val) {

{ //Subevents
gdjs.optionsCode.eventsList4(runtimeScene);} //End of subevents
}

}


{

gdjs.optionsCode.GDajout_95uniteObjects1.createFrom(runtimeScene.getObjects("ajout_unite"));

gdjs.optionsCode.condition0IsTrue_0.val = false;
gdjs.optionsCode.condition1IsTrue_0.val = false;
gdjs.optionsCode.condition2IsTrue_0.val = false;
gdjs.optionsCode.condition3IsTrue_0.val = false;
{
gdjs.optionsCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.optionsCode.mapOfGDgdjs_46optionsCode_46GDajout_9595uniteObjects1Objects, runtimeScene, true, false);
}if ( gdjs.optionsCode.condition0IsTrue_0.val ) {
{
gdjs.optionsCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.optionsCode.condition1IsTrue_0.val ) {
{
gdjs.optionsCode.condition2IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("mini")) < 80;
}if ( gdjs.optionsCode.condition2IsTrue_0.val ) {
{
gdjs.optionsCode.condition3IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("mini")) < gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("maxi")) - 10;
}}
}
}
if (gdjs.optionsCode.condition3IsTrue_0.val) {
gdjs.optionsCode.GDnb_95miniObjects1.createFrom(runtimeScene.getObjects("nb_mini"));
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("mini").add(10);
}{for(var i = 0, len = gdjs.optionsCode.GDnb_95miniObjects1.length ;i < len;++i) {
    gdjs.optionsCode.GDnb_95miniObjects1[i].setString(gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("mini"))));
}
}}

}


{

gdjs.optionsCode.GDretrait_95uniteObjects1.createFrom(runtimeScene.getObjects("retrait_unite"));

gdjs.optionsCode.condition0IsTrue_0.val = false;
gdjs.optionsCode.condition1IsTrue_0.val = false;
gdjs.optionsCode.condition2IsTrue_0.val = false;
{
gdjs.optionsCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.optionsCode.mapOfGDgdjs_46optionsCode_46GDretrait_9595uniteObjects1Objects, runtimeScene, true, false);
}if ( gdjs.optionsCode.condition0IsTrue_0.val ) {
{
gdjs.optionsCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.optionsCode.condition1IsTrue_0.val ) {
{
gdjs.optionsCode.condition2IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("mini")) > 0;
}}
}
if (gdjs.optionsCode.condition2IsTrue_0.val) {
gdjs.optionsCode.GDnb_95miniObjects1.createFrom(runtimeScene.getObjects("nb_mini"));
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("mini").sub(10);
}{for(var i = 0, len = gdjs.optionsCode.GDnb_95miniObjects1.length ;i < len;++i) {
    gdjs.optionsCode.GDnb_95miniObjects1[i].setString(gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("mini"))));
}
}}

}


{

gdjs.optionsCode.GDajout_95dizaineObjects1.createFrom(runtimeScene.getObjects("ajout_dizaine"));

gdjs.optionsCode.condition0IsTrue_0.val = false;
gdjs.optionsCode.condition1IsTrue_0.val = false;
gdjs.optionsCode.condition2IsTrue_0.val = false;
{
gdjs.optionsCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.optionsCode.mapOfGDgdjs_46optionsCode_46GDajout_9595dizaineObjects1Objects, runtimeScene, true, false);
}if ( gdjs.optionsCode.condition0IsTrue_0.val ) {
{
gdjs.optionsCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.optionsCode.condition1IsTrue_0.val ) {
{
gdjs.optionsCode.condition2IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("maxi")) < 90;
}}
}
if (gdjs.optionsCode.condition2IsTrue_0.val) {
gdjs.optionsCode.GDnb_95maxiObjects1.createFrom(runtimeScene.getObjects("nb_maxi"));
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("maxi").add(10);
}{for(var i = 0, len = gdjs.optionsCode.GDnb_95maxiObjects1.length ;i < len;++i) {
    gdjs.optionsCode.GDnb_95maxiObjects1[i].setString(gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("maxi"))));
}
}}

}


{

gdjs.optionsCode.GDretrait_95dizaineObjects1.createFrom(runtimeScene.getObjects("retrait_dizaine"));

gdjs.optionsCode.condition0IsTrue_0.val = false;
gdjs.optionsCode.condition1IsTrue_0.val = false;
gdjs.optionsCode.condition2IsTrue_0.val = false;
{
gdjs.optionsCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.optionsCode.mapOfGDgdjs_46optionsCode_46GDretrait_9595dizaineObjects1Objects, runtimeScene, true, false);
}if ( gdjs.optionsCode.condition0IsTrue_0.val ) {
{
gdjs.optionsCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.optionsCode.condition1IsTrue_0.val ) {
{
gdjs.optionsCode.condition2IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("maxi")) > gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("mini")) + 10;
}}
}
if (gdjs.optionsCode.condition2IsTrue_0.val) {
gdjs.optionsCode.GDnb_95maxiObjects1.createFrom(runtimeScene.getObjects("nb_maxi"));
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("maxi").sub(10);
}{for(var i = 0, len = gdjs.optionsCode.GDnb_95maxiObjects1.length ;i < len;++i) {
    gdjs.optionsCode.GDnb_95maxiObjects1[i].setString(gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("maxi"))));
}
}}

}


{


gdjs.optionsCode.condition0IsTrue_0.val = false;
gdjs.optionsCode.condition1IsTrue_0.val = false;
{
gdjs.optionsCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("mini")) > 9;
}if ( gdjs.optionsCode.condition0IsTrue_0.val ) {
{
{gdjs.optionsCode.conditionTrue_1 = gdjs.optionsCode.condition1IsTrue_0;
gdjs.optionsCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(10178212);
}
}}
if (gdjs.optionsCode.condition1IsTrue_0.val) {
gdjs.optionsCode.GDnb_95miniObjects1.createFrom(runtimeScene.getObjects("nb_mini"));
{for(var i = 0, len = gdjs.optionsCode.GDnb_95miniObjects1.length ;i < len;++i) {
    gdjs.optionsCode.GDnb_95miniObjects1[i].setX(gdjs.optionsCode.GDnb_95miniObjects1[i].getX() - (15));
}
}}

}


{


gdjs.optionsCode.condition0IsTrue_0.val = false;
gdjs.optionsCode.condition1IsTrue_0.val = false;
{
gdjs.optionsCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("mini")) < 9;
}if ( gdjs.optionsCode.condition0IsTrue_0.val ) {
{
{gdjs.optionsCode.conditionTrue_1 = gdjs.optionsCode.condition1IsTrue_0;
gdjs.optionsCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(10178780);
}
}}
if (gdjs.optionsCode.condition1IsTrue_0.val) {
gdjs.optionsCode.GDnb_95miniObjects1.createFrom(runtimeScene.getObjects("nb_mini"));
{for(var i = 0, len = gdjs.optionsCode.GDnb_95miniObjects1.length ;i < len;++i) {
    gdjs.optionsCode.GDnb_95miniObjects1[i].setX(gdjs.optionsCode.GDnb_95miniObjects1[i].getX() + (15));
}
}}

}


{

gdjs.optionsCode.GDouiObjects1.createFrom(runtimeScene.getObjects("oui"));

gdjs.optionsCode.condition0IsTrue_0.val = false;
gdjs.optionsCode.condition1IsTrue_0.val = false;
{
gdjs.optionsCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.optionsCode.condition0IsTrue_0.val ) {
{
gdjs.optionsCode.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.optionsCode.mapOfGDgdjs_46optionsCode_46GDouiObjects1Objects, runtimeScene, true, false);
}}
if (gdjs.optionsCode.condition1IsTrue_0.val) {
gdjs.optionsCode.GDnonObjects1.createFrom(runtimeScene.getObjects("non"));
/* Reuse gdjs.optionsCode.GDouiObjects1 */
{runtimeScene.getGame().getVariables().getFromIndex(4).setNumber(1);
}{for(var i = 0, len = gdjs.optionsCode.GDouiObjects1.length ;i < len;++i) {
    gdjs.optionsCode.GDouiObjects1[i].setAnimation(0);
}
}{for(var i = 0, len = gdjs.optionsCode.GDnonObjects1.length ;i < len;++i) {
    gdjs.optionsCode.GDnonObjects1[i].setAnimation(1);
}
}}

}


{

gdjs.optionsCode.GDnonObjects1.createFrom(runtimeScene.getObjects("non"));

gdjs.optionsCode.condition0IsTrue_0.val = false;
gdjs.optionsCode.condition1IsTrue_0.val = false;
{
gdjs.optionsCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.optionsCode.condition0IsTrue_0.val ) {
{
gdjs.optionsCode.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.optionsCode.mapOfGDgdjs_46optionsCode_46GDnonObjects1Objects, runtimeScene, true, false);
}}
if (gdjs.optionsCode.condition1IsTrue_0.val) {
/* Reuse gdjs.optionsCode.GDnonObjects1 */
gdjs.optionsCode.GDouiObjects1.createFrom(runtimeScene.getObjects("oui"));
{runtimeScene.getGame().getVariables().getFromIndex(4).setNumber(0);
}{for(var i = 0, len = gdjs.optionsCode.GDouiObjects1.length ;i < len;++i) {
    gdjs.optionsCode.GDouiObjects1[i].setAnimation(1);
}
}{for(var i = 0, len = gdjs.optionsCode.GDnonObjects1.length ;i < len;++i) {
    gdjs.optionsCode.GDnonObjects1[i].setAnimation(0);
}
}}

}


{


{
}

}


};

gdjs.optionsCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.optionsCode.GDbouton_95recommencerObjects1.length = 0;
gdjs.optionsCode.GDbouton_95recommencerObjects2.length = 0;
gdjs.optionsCode.GDbouton_95recommencerObjects3.length = 0;
gdjs.optionsCode.GDbouton_95validerObjects1.length = 0;
gdjs.optionsCode.GDbouton_95validerObjects2.length = 0;
gdjs.optionsCode.GDbouton_95validerObjects3.length = 0;
gdjs.optionsCode.GDbouton_95retourObjects1.length = 0;
gdjs.optionsCode.GDbouton_95retourObjects2.length = 0;
gdjs.optionsCode.GDbouton_95retourObjects3.length = 0;
gdjs.optionsCode.GDcyclisteObjects1.length = 0;
gdjs.optionsCode.GDcyclisteObjects2.length = 0;
gdjs.optionsCode.GDcyclisteObjects3.length = 0;
gdjs.optionsCode.GDajout_95uniteObjects1.length = 0;
gdjs.optionsCode.GDajout_95uniteObjects2.length = 0;
gdjs.optionsCode.GDajout_95uniteObjects3.length = 0;
gdjs.optionsCode.GDajout_95dizaineObjects1.length = 0;
gdjs.optionsCode.GDajout_95dizaineObjects2.length = 0;
gdjs.optionsCode.GDajout_95dizaineObjects3.length = 0;
gdjs.optionsCode.GDretrait_95dizaineObjects1.length = 0;
gdjs.optionsCode.GDretrait_95dizaineObjects2.length = 0;
gdjs.optionsCode.GDretrait_95dizaineObjects3.length = 0;
gdjs.optionsCode.GDretrait_95uniteObjects1.length = 0;
gdjs.optionsCode.GDretrait_95uniteObjects2.length = 0;
gdjs.optionsCode.GDretrait_95uniteObjects3.length = 0;
gdjs.optionsCode.GDfond_95paysage1Objects1.length = 0;
gdjs.optionsCode.GDfond_95paysage1Objects2.length = 0;
gdjs.optionsCode.GDfond_95paysage1Objects3.length = 0;
gdjs.optionsCode.GDscoreObjects1.length = 0;
gdjs.optionsCode.GDscoreObjects2.length = 0;
gdjs.optionsCode.GDscoreObjects3.length = 0;
gdjs.optionsCode.GDchoix_95ecartObjects1.length = 0;
gdjs.optionsCode.GDchoix_95ecartObjects2.length = 0;
gdjs.optionsCode.GDchoix_95ecartObjects3.length = 0;
gdjs.optionsCode.GDecart_954Objects1.length = 0;
gdjs.optionsCode.GDecart_954Objects2.length = 0;
gdjs.optionsCode.GDecart_954Objects3.length = 0;
gdjs.optionsCode.GDecart_9510Objects1.length = 0;
gdjs.optionsCode.GDecart_9510Objects2.length = 0;
gdjs.optionsCode.GDecart_9510Objects3.length = 0;
gdjs.optionsCode.GDchoix_95nombresObjects1.length = 0;
gdjs.optionsCode.GDchoix_95nombresObjects2.length = 0;
gdjs.optionsCode.GDchoix_95nombresObjects3.length = 0;
gdjs.optionsCode.GDnb_95maxiObjects1.length = 0;
gdjs.optionsCode.GDnb_95maxiObjects2.length = 0;
gdjs.optionsCode.GDnb_95maxiObjects3.length = 0;
gdjs.optionsCode.GDnb_95miniObjects1.length = 0;
gdjs.optionsCode.GDnb_95miniObjects2.length = 0;
gdjs.optionsCode.GDnb_95miniObjects3.length = 0;
gdjs.optionsCode.GDchoix_95multipleObjects1.length = 0;
gdjs.optionsCode.GDchoix_95multipleObjects2.length = 0;
gdjs.optionsCode.GDchoix_95multipleObjects3.length = 0;
gdjs.optionsCode.GDouiObjects1.length = 0;
gdjs.optionsCode.GDouiObjects2.length = 0;
gdjs.optionsCode.GDouiObjects3.length = 0;
gdjs.optionsCode.GDnonObjects1.length = 0;
gdjs.optionsCode.GDnonObjects2.length = 0;
gdjs.optionsCode.GDnonObjects3.length = 0;
gdjs.optionsCode.GDecart_956Objects1.length = 0;
gdjs.optionsCode.GDecart_956Objects2.length = 0;
gdjs.optionsCode.GDecart_956Objects3.length = 0;
gdjs.optionsCode.GDecart_958Objects1.length = 0;
gdjs.optionsCode.GDecart_958Objects2.length = 0;
gdjs.optionsCode.GDecart_958Objects3.length = 0;

gdjs.optionsCode.eventsList5(runtimeScene);
return;

}

gdjs['optionsCode'] = gdjs.optionsCode;
