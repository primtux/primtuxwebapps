gdjs.infosCode = {};
gdjs.infosCode.GDfond_9595blancObjects1= [];
gdjs.infosCode.GDfond_9595blancObjects2= [];
gdjs.infosCode.GDinfosObjects1= [];
gdjs.infosCode.GDinfosObjects2= [];
gdjs.infosCode.GDscore_95952Objects1= [];
gdjs.infosCode.GDscore_95952Objects2= [];
gdjs.infosCode.GDscore_9595Objects1= [];
gdjs.infosCode.GDscore_9595Objects2= [];
gdjs.infosCode.GDbouton_9595suivantObjects1= [];
gdjs.infosCode.GDbouton_9595suivantObjects2= [];
gdjs.infosCode.GDbouton_9595recommencerObjects1= [];
gdjs.infosCode.GDbouton_9595recommencerObjects2= [];
gdjs.infosCode.GDbouton_9595retourObjects1= [];
gdjs.infosCode.GDbouton_9595retourObjects2= [];
gdjs.infosCode.GDbouton_9595switchObjects1= [];
gdjs.infosCode.GDbouton_9595switchObjects2= [];
gdjs.infosCode.GDfaux_9595vraiObjects1= [];
gdjs.infosCode.GDfaux_9595vraiObjects2= [];
gdjs.infosCode.GDlamaObjects1= [];
gdjs.infosCode.GDlamaObjects2= [];
gdjs.infosCode.GDdrapeau1Objects1= [];
gdjs.infosCode.GDdrapeau1Objects2= [];
gdjs.infosCode.GDdrapeau2Objects1= [];
gdjs.infosCode.GDdrapeau2Objects2= [];
gdjs.infosCode.GDpointObjects1= [];
gdjs.infosCode.GDpointObjects2= [];
gdjs.infosCode.GDfond_9595chambreObjects1= [];
gdjs.infosCode.GDfond_9595chambreObjects2= [];


gdjs.infosCode.mapOfGDgdjs_9546infosCode_9546GDbouton_95959595retourObjects1Objects = Hashtable.newFrom({"bouton_retour": gdjs.infosCode.GDbouton_9595retourObjects1});
gdjs.infosCode.eventsList0 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("fond_blanc"), gdjs.infosCode.GDfond_9595blancObjects1);
{for(var i = 0, len = gdjs.infosCode.GDfond_9595blancObjects1.length ;i < len;++i) {
    gdjs.infosCode.GDfond_9595blancObjects1[i].setOpacity(200);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_retour"), gdjs.infosCode.GDbouton_9595retourObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.infosCode.mapOfGDgdjs_9546infosCode_9546GDbouton_95959595retourObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "menu", false);
}}

}


};

gdjs.infosCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.infosCode.GDfond_9595blancObjects1.length = 0;
gdjs.infosCode.GDfond_9595blancObjects2.length = 0;
gdjs.infosCode.GDinfosObjects1.length = 0;
gdjs.infosCode.GDinfosObjects2.length = 0;
gdjs.infosCode.GDscore_95952Objects1.length = 0;
gdjs.infosCode.GDscore_95952Objects2.length = 0;
gdjs.infosCode.GDscore_9595Objects1.length = 0;
gdjs.infosCode.GDscore_9595Objects2.length = 0;
gdjs.infosCode.GDbouton_9595suivantObjects1.length = 0;
gdjs.infosCode.GDbouton_9595suivantObjects2.length = 0;
gdjs.infosCode.GDbouton_9595recommencerObjects1.length = 0;
gdjs.infosCode.GDbouton_9595recommencerObjects2.length = 0;
gdjs.infosCode.GDbouton_9595retourObjects1.length = 0;
gdjs.infosCode.GDbouton_9595retourObjects2.length = 0;
gdjs.infosCode.GDbouton_9595switchObjects1.length = 0;
gdjs.infosCode.GDbouton_9595switchObjects2.length = 0;
gdjs.infosCode.GDfaux_9595vraiObjects1.length = 0;
gdjs.infosCode.GDfaux_9595vraiObjects2.length = 0;
gdjs.infosCode.GDlamaObjects1.length = 0;
gdjs.infosCode.GDlamaObjects2.length = 0;
gdjs.infosCode.GDdrapeau1Objects1.length = 0;
gdjs.infosCode.GDdrapeau1Objects2.length = 0;
gdjs.infosCode.GDdrapeau2Objects1.length = 0;
gdjs.infosCode.GDdrapeau2Objects2.length = 0;
gdjs.infosCode.GDpointObjects1.length = 0;
gdjs.infosCode.GDpointObjects2.length = 0;
gdjs.infosCode.GDfond_9595chambreObjects1.length = 0;
gdjs.infosCode.GDfond_9595chambreObjects2.length = 0;

gdjs.infosCode.eventsList0(runtimeScene);

return;

}

gdjs['infosCode'] = gdjs.infosCode;
