gdjs.infosCode = {};
gdjs.infosCode.GDinformationObjects1= [];
gdjs.infosCode.GDinformationObjects2= [];
gdjs.infosCode.GDpetale_95951Objects1= [];
gdjs.infosCode.GDpetale_95951Objects2= [];
gdjs.infosCode.GDpetale_95952Objects1= [];
gdjs.infosCode.GDpetale_95952Objects2= [];
gdjs.infosCode.GDpetale_95953Objects1= [];
gdjs.infosCode.GDpetale_95953Objects2= [];
gdjs.infosCode.GDpetale_95954Objects1= [];
gdjs.infosCode.GDpetale_95954Objects2= [];
gdjs.infosCode.GDpetale_95955Objects1= [];
gdjs.infosCode.GDpetale_95955Objects2= [];
gdjs.infosCode.GDspr_9595bouton_9595retourObjects1= [];
gdjs.infosCode.GDspr_9595bouton_9595retourObjects2= [];
gdjs.infosCode.GDfond_9595parcObjects1= [];
gdjs.infosCode.GDfond_9595parcObjects2= [];
gdjs.infosCode.GDdoigt_95951GObjects1= [];
gdjs.infosCode.GDdoigt_95951GObjects2= [];
gdjs.infosCode.GDdoigt_95951Objects1= [];
gdjs.infosCode.GDdoigt_95951Objects2= [];
gdjs.infosCode.GDdoigt_95952GObjects1= [];
gdjs.infosCode.GDdoigt_95952GObjects2= [];
gdjs.infosCode.GDdoigt_95952Objects1= [];
gdjs.infosCode.GDdoigt_95952Objects2= [];
gdjs.infosCode.GDdoigt_95953GObjects1= [];
gdjs.infosCode.GDdoigt_95953GObjects2= [];
gdjs.infosCode.GDdoigt_95953Objects1= [];
gdjs.infosCode.GDdoigt_95953Objects2= [];
gdjs.infosCode.GDdoigt_95954GObjects1= [];
gdjs.infosCode.GDdoigt_95954GObjects2= [];
gdjs.infosCode.GDdoigt_95954Objects1= [];
gdjs.infosCode.GDdoigt_95954Objects2= [];
gdjs.infosCode.GDdoigt_95955GObjects1= [];
gdjs.infosCode.GDdoigt_95955GObjects2= [];
gdjs.infosCode.GDdoigt_95955Objects1= [];
gdjs.infosCode.GDdoigt_95955Objects2= [];


gdjs.infosCode.mapOfGDgdjs_9546infosCode_9546GDspr_95959595bouton_95959595retourObjects1Objects = Hashtable.newFrom({"spr_bouton_retour": gdjs.infosCode.GDspr_9595bouton_9595retourObjects1});
gdjs.infosCode.eventsList0 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("spr_bouton_retour"), gdjs.infosCode.GDspr_9595bouton_9595retourObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.infosCode.mapOfGDgdjs_9546infosCode_9546GDspr_95959595bouton_95959595retourObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "menu", false);
}}

}


};

gdjs.infosCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.infosCode.GDinformationObjects1.length = 0;
gdjs.infosCode.GDinformationObjects2.length = 0;
gdjs.infosCode.GDpetale_95951Objects1.length = 0;
gdjs.infosCode.GDpetale_95951Objects2.length = 0;
gdjs.infosCode.GDpetale_95952Objects1.length = 0;
gdjs.infosCode.GDpetale_95952Objects2.length = 0;
gdjs.infosCode.GDpetale_95953Objects1.length = 0;
gdjs.infosCode.GDpetale_95953Objects2.length = 0;
gdjs.infosCode.GDpetale_95954Objects1.length = 0;
gdjs.infosCode.GDpetale_95954Objects2.length = 0;
gdjs.infosCode.GDpetale_95955Objects1.length = 0;
gdjs.infosCode.GDpetale_95955Objects2.length = 0;
gdjs.infosCode.GDspr_9595bouton_9595retourObjects1.length = 0;
gdjs.infosCode.GDspr_9595bouton_9595retourObjects2.length = 0;
gdjs.infosCode.GDfond_9595parcObjects1.length = 0;
gdjs.infosCode.GDfond_9595parcObjects2.length = 0;
gdjs.infosCode.GDdoigt_95951GObjects1.length = 0;
gdjs.infosCode.GDdoigt_95951GObjects2.length = 0;
gdjs.infosCode.GDdoigt_95951Objects1.length = 0;
gdjs.infosCode.GDdoigt_95951Objects2.length = 0;
gdjs.infosCode.GDdoigt_95952GObjects1.length = 0;
gdjs.infosCode.GDdoigt_95952GObjects2.length = 0;
gdjs.infosCode.GDdoigt_95952Objects1.length = 0;
gdjs.infosCode.GDdoigt_95952Objects2.length = 0;
gdjs.infosCode.GDdoigt_95953GObjects1.length = 0;
gdjs.infosCode.GDdoigt_95953GObjects2.length = 0;
gdjs.infosCode.GDdoigt_95953Objects1.length = 0;
gdjs.infosCode.GDdoigt_95953Objects2.length = 0;
gdjs.infosCode.GDdoigt_95954GObjects1.length = 0;
gdjs.infosCode.GDdoigt_95954GObjects2.length = 0;
gdjs.infosCode.GDdoigt_95954Objects1.length = 0;
gdjs.infosCode.GDdoigt_95954Objects2.length = 0;
gdjs.infosCode.GDdoigt_95955GObjects1.length = 0;
gdjs.infosCode.GDdoigt_95955GObjects2.length = 0;
gdjs.infosCode.GDdoigt_95955Objects1.length = 0;
gdjs.infosCode.GDdoigt_95955Objects2.length = 0;

gdjs.infosCode.eventsList0(runtimeScene);

return;

}

gdjs['infosCode'] = gdjs.infosCode;
