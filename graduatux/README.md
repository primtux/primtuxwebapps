## Graduatux

*Ceci est la présentation de l'application.
Pour l'utiliser directement, rendez-vous sur https://educajou.forge.apps.education.fr/graduatux*

## Présentation

Graduatux est une application libre qui permet de travailler le repérage sur la droite numérique.
Une droite décimale est affichée, avec seulement deux nombres apparents, et cinq à renseigner par l'élève.

![](https://forge.apps.education.fr/educajou/graduatux/-/raw/main/images/screenshot_droite.png?ref_type=heads)

## Compétences travaillées

### Attendus de fin de cycle 2
- Utiliser diverses représentations des nombres (écritures en chiffres et en lettres, noms à
l’oral, graduations sur une demi-droite, constellations sur des dés, doigts de la main...)
- Associer un nombre entier à une position sur une demi-droite graduée, ainsi qu’à la
distance de ce point à l’origine.
- Repérer et placer un nombre décimal sur une demi-droite graduée adaptée. Comparer,
ranger des nombres décimaux.

### Attendus de fin de cycle 3
- Comparer, ranger, encadrer des grands nombres entiers, les repérer et les placer sur une
demi-droite graduée adaptée.
- Repérer et placer un nombre décimal sur une demi-droite graduée adaptée. Comparer,
ranger des nombres décimaux.

## Accès à l'application

### En ligne

Une instance fonctionnelle peut être utilisée sur https://educajou.forge.apps.education.fr/graduatux

### Hors ligne

- Télécharger l'application **[📦 graduatux-main.zip](https://forge.apps.education.fr/educajou/graduatux/-/archive/main/graduatux-main.zip)**
- Extraire le ZIP
- Ouvrir le fichier **index.html**

### Sur Primtux

Graduatux est intégrée au système d'exploitation [Primtux](https://primtux.fr/).
Pour l'utiliser, après l'installation de Primtux, installer les applications complémentaires.

## Utilisation

### Réglages des options

#### Pas

Il est possible de choisir la ou les les puissances de 10 utilisées pour les pas (distance entre deux graduations):

- de 0,001 en 0,0001
- de 0,001 en 0,001, 
- de 0,1 en 0,1, 
- de 1 en 1, 
- de 10 en 10, 
- de 100 en 100,
- de 1000 en 1000

*Si plusieurs choix sont sélectionnés, les puissances seront tirées au sort à chaque fois. Ce qui rend l'exercice plus difficile car l'élève doit alors le déduire d'après les nombres affichés.*

#### Franchissement de pas supérieur

On peut aussi décider si les valeurs affichées : *jamais / parfois / toujours* obligeront à franchir une puissance de 10 plus élevée.

Par exemple, si on travaille de 10 en 10, est-ce qu'on n'affiche de des bornes comme 500 et 600, ce qui confront l'élève à des nombres comme 470, 540, 630 ... ou est-ce que l'application doit proposer aussi des situations comme 900 à 1000 ? (Ce qui confronte l'élève a des nombres comme 1020) ?

### Options dans l'URL

Les options dans l'URL sont précédées de ? et séparées par &.

#### Primtux

Ajouter primtuxmenu=true pour adapter l'interface à Primtux.
https://educajou.forge.apps.education.fr/graduatux?primtuxmenu=true

#### Pas

Ajouter log=0_1_2 ... pour précocher les pas correspondant aux puissances 1, 10, 100 ...

Exemples :

https://educajou.forge.apps.education.fr/graduatux?log=0_1_2 pour travailler avec des pas de 1, 10 et 100.

https://educajou.forge.apps.education.fr/graduatux/?log=-2_-1 pour travailler avec des pas de 0,01 et 0,1.

#### Franchissement de pas supérieur

Exemple :

https://educajou.forge.apps.education.fr/graduatux/?passage=parfois

#### Mode professeur

En mode professeur, la validation des réponses n'est pas nécessaire pour passer à l'exercice suivant, et la consigne n'est pas affichée.

Ce mode permet à l'enseignant d'afficher des situations au tableau, à fns de travail collectif de verbalisation, par exemple.

https://educajou.forge.apps.education.fr/graduatux/?mode=prof

#### Cumuler les options

Pour cumuler les options, utiliser le séparateur & :

https://educajou.forge.apps.education.fr/graduatux/?passage=parfois&mode=prof&log=-2_-1_0_1

## Signaler un bug ou proposer une amélioration

Pour participer au développement en rapportant des bugs ou en proposant une fonctionnalité, vous pouvez déposer un ticket sur cette page :
https://forge.apps.education.fr/educajou/graduatux/-/issues

### Comment créer un ticket ?

<iframe title="Créer un ticket sur La Forge des Communs Numériques Éducatifs - Vidéo Tuto" width="560" height="315" src="https://tube-sciences-technologies.apps.education.fr/videos/embed/058b9ab7-ab65-4d25-b24f-6a34feda013d" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups allow-forms"></iframe>

https://tube-sciences-technologies.apps.education.fr/w/1FHx5ntDrd9mwo5k6dAB2F

