// Fichier traitant de la partie interactivité
  
/////////////////////////////////////////////////////////////////////////////////
// Fonctions pour incrémentations continues si click maintenu sur boutons -/+  //
/////////////////////////////////////////////////////////////////////////////////

// Sélectionnez tous les boutons avec la classe "btn"
var buttons = document.querySelectorAll('.btIncrement');
// Fonction à appeler lorsque le clic est maintenu
function repeatFunction(id) {
  incrementationHoraire(id);
  console.log('La fonction est répétée pour le bouton avec l\'ID : ' + id);
}
// Variables pour le suivi de l'état du clic
var isMouseDown = false;
var repeatInterval;
// Fonction pour gérer le clic maintenu
function handleMouseDown(id) {
  isMouseDown = true;
  // Appel initial de la fonction avec l'ID du bouton
  // repeatFunction(id);
  // Répétition de la fonction toutes les 300 millisecondes (ajustez selon vos besoins)
  repeatInterval = setInterval(function() {
    repeatFunction(id);
  }, 300);
}
// Fonction pour gérer le relâchement du clic
function handleMouseUp() {
  isMouseDown = false;
  // Arrêtez la répétition de la fonction
  clearInterval(repeatInterval);
}
// Fonction pour gérer la sortie du curseur de l'élément
function handleMouseLeave() {
  if (isMouseDown) {
    isMouseDown = false;
    // Arrêtez la répétition de la fonction
    clearInterval(repeatInterval);
  }
}
// Parcourez tous les boutons et ajoutez les écouteurs d'événements
buttons.forEach(function(button) {
  var buttonId = button.id; // Récupérez l'ID du bouton
  button.addEventListener('mousedown', function() {
    handleMouseDown(buttonId);
  });
  button.addEventListener('mouseup', handleMouseUp);
  button.addEventListener('mouseleave', handleMouseLeave);
});

//////////////////////////////////////////////////////////////////////////
//                  PARTIE POUR MANIPULER LES AIGUILLES                 //
//////////////////////////////////////////////////////////////////////////
/*function initialisationAiguillesManip() {
    let heure_manip = document.getElementById("aiguille_manip_heures_id");
    let minute_manip = document.getElementById("aiguille_manip_minutes_id");
    let secondes_manip = document.getElementById("aiguille_manip_secondes_id");
    heure_manip.style.transform = `rotate(${305}deg)`;
    minute_manip.style.transform = `rotate(${63}deg)`;
    secondes_manip.style.transform = `rotate(${180}deg)`;
  }
initialisationAiguillesManip();*/
  
  //////////////////////////////////////////////////////////////////////////////////////////////
  // -------  rotation des aiguilles à manipuler  -------- //
  ///////////////////////////////////////////////////////////
  
  //Code généré au départ par OpenAI Chat API le 29/05/2023 en réponse à la question
  // How to move with the mouse a clock hand in javascript ?
  // Et à des améliorations successives dans le code suite à des échanges.
  
 /* var clock = document.getElementById("aiguilles_manip_id");
  var hourHand = document.getElementById("aiguille_manip_heures_id");
  var minuteHand = document.getElementById("aiguille_manip_minutes_id");
  var secondHand = document.getElementById("aiguille_manip_secondes_id");
  
  var activeClockHand = null; // Variable to store the currently active clock hand
  
  hourHand.addEventListener('mousedown', startClockHandInteraction);
  hourHand.addEventListener('touchstart', startClockHandInteraction);
  
  minuteHand.addEventListener('mousedown', startClockHandInteraction);
  minuteHand.addEventListener('touchstart', startClockHandInteraction);
  
  secondHand.addEventListener('mousedown', startClockHandInteraction);
  secondHand.addEventListener('touchstart', startClockHandInteraction);

  
  function startClockHandInteraction(event) {
    var isTouchEvent = event.type.startsWith('touch');
    setActiveClockHand(event.currentTarget, isTouchEvent);
    if (isTouchEvent) {
      event.preventDefault();
      event.stopPropagation();
    }
  }

  function setActiveClockHand(clockHand, isTouchEvent) {
    activeClockHand = clockHand;
    if (isTouchEvent) {
      document.addEventListener('touchmove', moveClockHand, { passive: false });
      document.addEventListener('touchend', releaseClockHand);
    } else {
      document.addEventListener('mousemove', moveClockHand);
      document.addEventListener('mouseup', releaseClockHand);
    }
  }
  


  function moveClockHand(event) {
    if (!activeClockHand) return;
    var clockRect = clock.getBoundingClientRect(); // Get the clock element's position and dimensions
    var clockParent = clock.parentElement;
    var clockParentRect = clockParent.getBoundingClientRect();
    var clockX = clockRect.left - clock.offsetLeft + 302.5;
    var clockY = clockRect.top + clock.offsetTop+ 151;
    var clientX, clientY;

    if (event.type.startsWith('touch')) {
      clientX = event.touches[0].clientX;
      clientY = event.touches[0].clientY;
    } else {
      clientX = event.clientX;
      clientY = event.clientY;
    }
  
      //Déplacement indépendant des aiguilles de manip
      console.log("Aiguilles non liées.");
      var deltaX = clientX - clockX;
      var deltaY = clientY - clockY;
      var angle = Math.atan2(deltaY, deltaX);
      angle = angle * (180 / Math.PI)+90; // Convert radians to degrees with a constant angle offset of 90 degrees
      activeClockHand.style.transformOrigin = `bottom center`;
      // Apply the translation and rotation transformations
      activeClockHand.style.transform = `translate(0px, 0px) rotate(${angle}deg)`;
  }

  function releaseClockHand() {
    if (!activeClockHand) return;
    document.removeEventListener('touchmove', moveClockHand);
    document.removeEventListener('touchend', releaseClockHand);
    document.removeEventListener('mousemove', moveClockHand);
    document.removeEventListener('mouseup', releaseClockHand);
    activeClockHand = null; // Reset the active clock hand
  }
*/

///////////////////////////////////////////////////////////////////////////////////////////
///     Partie pour mouvoir les aiguilles dépendamment des autres et sans collisions    ///
///////////////////////////////////////////////////////////////////////////////////////////
///                               PARTIE FONCTIONNELLE                                  ///
///////////////////////////////////////////////////////////////////////////////////////////

// Déclaration des variables globales
let canvas = document.getElementById('horloge_canvas_id');
let ctx = canvas.getContext('2d');
let centerX, centerY;
let hours, minutes, secondes;
let hourAngle, minuteAngle, secondAngle;
let hoursHandLength, minutesHandLength, secondsHandLength;
let isDragging = false;
let startX, startY;
let offsetX = 0;
let offsetY = 0;
let offsetXHour, offsetYHour;
let offsetXMinute, offsetYMinute;
let offsetXSecond, offsetYSecond;
let dragTarget = null;
let touchScreen

// Fonction pour normaliser un angle entre 0 et 2π
function normalizeAngle(angle) {
  return (angle + 2 * Math.PI) % (2 * Math.PI);
}

// Add event listeners for mouse or touch interactions
function determineTypeAppareil(){
  touchScreen = ('ontouchstart' in window || navigator.maxTouchPoints > 0 || navigator.msMaxTouchPoints > 0);
  if (touchScreen) {
    canvas.addEventListener('touchstart', handleMouseDown, false);
    canvas.addEventListener('touchend', handleMouseUp, false);
    canvas.addEventListener('touchmove', handleMouseMove, false);
  }
  else {
    canvas.onmousedown = handleMouseDown;
    canvas.onmouseup = handleMouseUp;
    canvas.onmousemove = handleMouseMove;
  }
}

// Fonction pour dessiner l'horloge complète
function drawClock() {
  const radius = Math.min(canvas.width, canvas.height) / 2;
  centerX = canvas.width / 2;
  centerY = canvas.height / 2;
  drawClockHands();
  // Demander une nouvelle frame d'animation
  setTimeout( function() { requestAnimationFrame(drawClock); }, 50);
}

// Fonction pour dessiner les aiguilles manipulables de l'horloge
function drawClockHands() {
  // Effacer le canvas
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  ctx.globalAlpha = 0.75;

  // Aiguille des secondes
  var element = document.getElementById('cacAiguilleManipSecondes_id');
  if (element.checked == true && cacExoAvecSecondes_id.checked == true) {
    ctx.beginPath();
    ctx.moveTo(centerX, centerY);
    ctx.lineWidth = 10;
    ctx.lineCap = 'round';
    ctx.strokeStyle = '#e21c15';
    ctx.lineTo(
      centerX + Math.cos(secondAngle - Math.PI / 2) * secondsHandLength,
      centerY + Math.sin(secondAngle - Math.PI / 2) * secondsHandLength
    );
    ctx.stroke();
  }

  // Aiguille des minutes
  var element = document.getElementById('cacAiguilleManipMinutes_id');
  if (element.checked == true) {
    ctx.beginPath();
    ctx.moveTo(centerX, centerY);
    ctx.lineWidth = 14;
    ctx.lineCap = 'round';
    ctx.strokeStyle = '#55d400ff';
    ctx.lineTo(
      centerX + Math.cos(minuteAngle - Math.PI / 2) * minutesHandLength,
      centerY + Math.sin(minuteAngle - Math.PI / 2) * minutesHandLength
    );
    ctx.stroke();
  }

  // Aiguille des heures
  var element = document.getElementById('cacAiguilleManipHeures_id');
  if (element.checked == true) {
    ctx.beginPath();
    ctx.moveTo(centerX, centerY);
    ctx.lineWidth = 14;
    ctx.lineCap = 'round';
    ctx.strokeStyle = '#2987e4';
    ctx.lineTo(
      centerX + Math.cos(hourAngle - Math.PI / 2) * hoursHandLength,
      centerY + Math.sin(hourAngle - Math.PI / 2) * hoursHandLength
    );
    ctx.stroke();
  }

}

// Variables pour le déplacement lié des aiguilles manip
isDragging = false;
let dragOffsetX = 0;
let dragOffsetY = 0;
var initialHourAngle, initialMinuteAngle, initialSecondAngle;

// Event handler for mouse down on the canvas
function handleMouseDown(event) {
  if (touchScreen) {
    event.preventDefault();
    event = event.touches[0]; // Gestion du tactile
  }
  console.log(touchScreen);
  const rect = canvas.getBoundingClientRect();
  const mouseX = event.clientX - rect.left;
  const mouseY = event.clientY - rect.top;
  initialHourAngle = hourAngle;
  initialMinuteAngle = minuteAngle;
  initialSecondAngle = secondAngle;

  // Check if the click is on a hand
  const proximityThreshold = 10; // Proximity threshold for clicking on the hand

  const hourHandStartX = centerX;
  const hourHandStartY = centerY;
  const hourHandEndX = centerX + Math.cos(hourAngle - Math.PI / 2) * hoursHandLength;
  const hourHandEndY = centerY + Math.sin(hourAngle - Math.PI / 2) * hoursHandLength;

  const minuteHandStartX = centerX;
  const minuteHandStartY = centerY;
  const minuteHandEndX = centerX + Math.cos(minuteAngle - Math.PI / 2) * minutesHandLength;
  const minuteHandEndY = centerY + Math.sin(minuteAngle - Math.PI / 2) * minutesHandLength;

  const secondHandStartX = centerX;
  const secondHandStartY = centerY;
  const secondHandEndX = centerX + Math.cos(secondAngle - Math.PI / 2) * secondsHandLength;
  const secondHandEndY = centerY + Math.sin(secondAngle - Math.PI / 2) * secondsHandLength;

  if (
    isPointOnLine(mouseX, mouseY, hourHandStartX, hourHandStartY, hourHandEndX, hourHandEndY, proximityThreshold)
  ) {
    dragTarget = 'hour';
    dragOffsetX = mouseX - centerX;
    dragOffsetY = mouseY - centerY;
  } else if (
    isPointOnLine(mouseX, mouseY, minuteHandStartX, minuteHandStartY, minuteHandEndX, minuteHandEndY, proximityThreshold)
  ) {
    dragTarget = 'minute';
    dragOffsetX = mouseX - centerX;
    dragOffsetY = mouseY - centerY;
  } else if (
    isPointOnLine(mouseX, mouseY, secondHandStartX, secondHandStartY, secondHandEndX, secondHandEndY, proximityThreshold)
  ) {
    dragTarget = 'second';
    dragOffsetX = mouseX - centerX;
    dragOffsetY = mouseY - centerY;
  } else {
    dragTarget = null;
  }

  isDragging = true;

  console.log(dragTarget);
  console.log(hours,initialHourAngle,minutes,secondes);

}

// Function to check if a point is near a line segment
function isPointOnLine(x, y, x1, y1, x2, y2, threshold) {
  const d1 = Math.sqrt((x - x1) ** 2 + (y - y1) ** 2);
  const d2 = Math.sqrt((x - x2) ** 2 + (y - y2) ** 2);
  const lineLength = Math.sqrt((x1 - x2) ** 2 + (y1 - y2) ** 2);
  const buffer = 0.1; // A small buffer to handle slight deviations
  return d1 + d2 >= lineLength - buffer && d1 + d2 <= lineLength + threshold + buffer;
}


let prevHourAngle = initialHourAngle;
let prevMinuteAngle = initialMinuteAngle;
let prevSecondAngle = initialSecondAngle;
var degrees, initialDegrees, differenceDegrees,roundedDifferenceDegrees,roundedAngle;

// Event handler for mouse move on the canvas
function handleMouseMove(event) {
  if (touchScreen) {
    event.preventDefault();
    event = event.touches[0]; // Gestion du tactile
  }
  var element = document.getElementById('cacAiguillesLiées_id');
  var déplacementsLiés;
  if (element.checked == true) {
    déplacementsLiés=true;
  }  
  if (isDragging) {
    const rect = canvas.getBoundingClientRect();
    const mouseX = event.clientX - rect.left;
    const mouseY = event.clientY - rect.top;

    const dx = mouseX - centerX;
    const dy = mouseY - centerY;

    const angle = Math.atan2(dy, dx) + Math.PI / 2;

    if (dragTarget === 'hour') {
      degrees = (angle * 180 / Math.PI) % 360;
      initialDegrees = (initialHourAngle * 180 / Math.PI) % 360;
      differenceDegrees = degrees - initialDegrees;
      if (déplacementsLiés){
        roundedDifferenceDegrees = Math.round(differenceDegrees / 30) * 30;
      }
      else {
        roundedDifferenceDegrees = Math.round(differenceDegrees);
      }
      roundedAngle = (initialDegrees + roundedDifferenceDegrees) * Math.PI / 180;
      if (roundedAngle < 0){ // Pour ne pas avoir de valeur négative
        roundedAngle = roundedAngle + Math.PI * 2;
      }
      hourAngle = roundedAngle;
      console.log("hourAngle",hourAngle);
     
    } else if (dragTarget === 'minute') {
      degrees = (angle * 180 / Math.PI) % 360;
      initialDegrees = (initialMinuteAngle * 180 / Math.PI) % 360;
      differenceDegrees = degrees - initialDegrees;
      if (déplacementsLiés){
        roundedDifferenceDegrees = Math.round(differenceDegrees / 6) * 6;
      }
      else {
        roundedDifferenceDegrees = Math.round(differenceDegrees);
      }
      roundedAngle = (initialDegrees + roundedDifferenceDegrees) * Math.PI / 180;
      if (roundedAngle < 0){ // Pour ne pas avoir de valeur négative
        roundedAngle = roundedAngle + Math.PI * 2;
      }
      minuteAngle = roundedAngle;
      // Vérifier si minuteAngle est revenu à 0
      if (prevMinuteAngle > Math.PI*3/2 && minuteAngle < Math.PI/2) { // Cas où on avance
        //console.log("AvanceMin",initialMinuteAngle);
        initialHourAngle=initialHourAngle + Math.PI/6;
      } else if (prevMinuteAngle < Math.PI/2 && minuteAngle > Math.PI/2*3 && minuteAngle<= Math.PI*2 ) { // Cas où on recule
        //console.log("ReculMin",initialMinuteAngle);
        initialHourAngle=initialHourAngle - Math.PI/6;
      }
      //console.log("initialHourAngle",initialHourAngle, "hourAngle",hourAngle,"minuteAngle", minuteAngle);
      if (déplacementsLiés){
        hourAngle =(((minuteAngle-initialMinuteAngle) / 12) + initialHourAngle) % (2 * Math.PI);
      }
     }
    else if (dragTarget === 'second') {
      degrees = (angle * 180 / Math.PI) % 360;
      roundedDegrees = Math.round(degrees / 6) * 6;
      roundedAngle = (roundedDegrees * Math.PI) / 180;
      if (roundedAngle < 0){ // Pour ne pas avoir de valeur négative
        roundedAngle = roundedAngle + Math.PI * 2;
      }
      secondAngle = roundedAngle;
      if (prevSecondAngle > Math.PI*3/2 && secondAngle < Math.PI/2) { // Cas où on avance
        //console.log("AvanceSec",initialMinuteAngle,minuteAngle);
        initialMinuteAngle=initialMinuteAngle + Math.PI/30;
        initialHourAngle = initialHourAngle + Math.PI/360;
      } else if (prevSecondAngle < Math.PI/2 && secondAngle > Math.PI/2*3 && secondAngle<= Math.PI*2 ) { // Cas où on recule
        //console.log("ReculSec",initialMinuteAngle, minuteAngle);
        initialMinuteAngle = initialMinuteAngle - Math.PI/30;
        initialHourAngle = initialHourAngle - Math.PI/360;
      }
      if (déplacementsLiés){
      minuteAngle = ((secondAngle-initialSecondAngle)/60 + initialMinuteAngle) % (2 * Math.PI);
      hourAngle = ((secondAngle-initialSecondAngle)/720 + initialHourAngle) % (2 * Math.PI);
      }
    }

    drawClock();

    hours = Math.round((hourAngle * 6 / Math.PI) % 12);
    minutes = Math.round((minuteAngle * 30 / Math.PI) % 60);
    secondes = Math.round((secondAngle * 30 / Math.PI) % 60);
    console.log(hours,minutes,secondes);
    //console.log(Math.round((hourAngle* 180 / Math.PI) % 360));

    prevHourAngle = hourAngle;
    prevMinuteAngle = minuteAngle;
    prevSecondAngle = secondAngle;

  }
}

// Event handler for mouse up on the canvas
function handleMouseUp(event) {
  isDragging = false;
}


// Redimensionner le canvas pour qu'il soit proportionnel à la fenêtre
function resizeCanvas() {
  canvas.width = canvas.offsetWidth;
  canvas.height = canvas.offsetHeight;
  centerX = canvas.width / 2;
  centerY = canvas.height / 2;
}

// Appeler la fonction de redimensionnement du canvas lors du chargement initial de la page
// et le traçage ainsi que le placement initial des aiguilles à manipuler.
window.addEventListener('load', () => {
  determineTypeAppareil();
  resizeCanvas();
  hours=9;
  minutes=30;
  secondes=0;
  réinitialisationAffichage();
  drawClock();
});

function réinitialisationAffichage() {
  // permet de replacer les aiguilles manipulables dans une position liée
  hourAngle = ((hours % 12) * 60 + minutes) * (Math.PI / 360) + (secondes / 3600) * (Math.PI / 6);
  minuteAngle = (minutes * 60 + secondes) * (Math.PI / 1800);
  secondAngle = (secondes / 60) * (2 * Math.PI);
  hoursHandLength = Math.min(canvas.width, canvas.height) / 2 * 0.5;
  minutesHandLength = Math.min(canvas.width, canvas.height) / 2 * 0.65;
  secondsHandLength = Math.min(canvas.width, canvas.height) / 2 * 0.70;
}

// Appeler la fonction de redimensionnement du canvas lors du redimensionnement de la fenêtre
window.addEventListener('resize', resizeCanvas);


function affichageDesSecondes() {
  // Permet de travailler ou pas avec les secondes
  if (cacExoAvecSecondes_id.checked == true) {
    console.log("TRAVAIL avec SECONDES");
    cacBoiteSecondes.checked = true;
    cacAiguilleSecondes.checked = true; 
    if (btAffichageGlobalHeureDigit_id.innerText == "Afficher l'heure digitale") {
      boite_secondes_id.style.visibility = "hidden";
    }
    else {
      boite_secondes_id.style.visibility = "visible";
    }
    if (btAffichageGlobalAiguille_id.innerText == "Afficher les aiguilles") {
      aiguille_secondes_id.style.visibility = "hidden";
      cacAiguilleSecondes.checked = false;
    }
    else {
      aiguille_secondes_id.style.visibility = "visible";
      cacAiguilleSecondes.checked = true;
    }
    if (btAffichageGlobalAiguilleManip_id.style.visibility == "hidden") { // cas avec exo2
      cacAiguilleManipSecondes_id.checked = false;
      reponse_boite_secondes_id.style.visibility = "visible";
    }
    else{
      cacAiguilleManipSecondes_id.checked = true;
      reponse_boite_secondes_id.style.visibility = "hidden";
    }
    btSecondesMoins_id.style.visibility = "visible";
    btSecondesPlus_id.style.visibility = "visible";
    pas_secondes_id.style.visibility = "visible";
  }
  if (cacExoAvecSecondes_id.checked == false){
    console.log("TRAVAIL sans SECONDES");
    secondes=0; //permet que l'aiguille des minutes suive les graduations
    boite_secondes_id.value = 0;
    deplacementAiguillesHeureInteractive();
    réinitialisationAffichage();
    cacBoiteSecondes.checked = false;
    boite_secondes_id.style.visibility = "hidden";
    cacAiguilleSecondes.checked = false;
    btSecondesMoins_id.style.visibility = "hidden";
    btSecondesPlus_id.style.visibility = "hidden";
    pas_secondes_id.style.visibility = "hidden";
    cacAiguilleManipSecondes_id.checked = false;
    aiguille_secondes_id.style.visibility = "hidden";
    reponse_boite_secondes_id.style.visibility = "hidden";


    }
}
