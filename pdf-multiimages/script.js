function generatePDF() {
    const { jsPDF } = window.jspdf;
    const files = document.getElementById('fileInput').files;
    const imagesPerPageInput = document.querySelector('input[name="imagesPerPage"]:checked');
    const orientation = document.querySelector('input[name="orientation"]:checked').value;
    const fileName = document.getElementById('fileName').value;
    const rectoVerso = document.getElementById('rectoVerso').checked;

    // Vérification des erreurs
    let errorMessages = "";
    if (!imagesPerPageInput) {
        errorMessages += "Veuillez choisir le nombre d'images par page.<br>";
    }
    if (!fileName) {
        errorMessages += "Veuillez entrer un nom pour le fichier PDF.<br>";
    }
    if (errorMessages) {
        document.getElementById('errorMessages').innerHTML = errorMessages;
        return;
    } else {
        document.getElementById('errorMessages').innerHTML = "";
    }

    const imagesPerPage = parseInt(imagesPerPageInput.value);
    const doc = new jsPDF(orientation, 'mm', 'a4');

    let row, col, cellWidth, cellHeight;
    const pageWidth = doc.internal.pageSize.getWidth();
    const pageHeight = doc.internal.pageSize.getHeight();
    const margin = 10;
    const availableWidth = pageWidth - 2 * margin;
    const availableHeight = pageHeight - 2 * margin;

    switch (imagesPerPage) {
        case 2:
            row = 1; col = 2; break;
        case 4:
            row = 2; col = 2; break;
        case 6:
            row = 2; col = 3; break;
        case 9:
            row = 3; col = 3; break;
        case 12:
            row = 3; col = 4; break;
        default:
            row = 1; col = 1; break;
    }

    cellWidth = (availableWidth - (col - 1) * margin) / col;
    cellHeight = (availableHeight - (row - 1) * margin) / row;

    let x = 0, y = 0;
    let imgCount = 0;

    Array.from(files).forEach((file, index) => {
        const reader = new FileReader();
        reader.onload = function(e) {
            const img = new Image();
            img.src = e.target.result;
            img.onload = function() {
                if (imgCount % imagesPerPage === 0 && imgCount !== 0) {
                    doc.addPage();
                    x = 0; y = 0;
                }

                const imgWidth = img.width;
                const imgHeight = img.height;
                const imgRatio = imgWidth / imgHeight;
                const cellRatio = cellWidth / cellHeight;

                let drawWidth, drawHeight;

                if (imgRatio > cellRatio) {
                    drawWidth = cellWidth;
                    drawHeight = cellWidth / imgRatio;
                } else {
                    drawHeight = cellHeight;
                    drawWidth = cellHeight * imgRatio;
                }

                const offsetX = (cellWidth - drawWidth) / 2;
                const offsetY = (cellHeight - drawHeight) / 2;

                if (imagesPerPage === 6 && rectoVerso && y === 1) {
                    // Créer un canevas pour la rotation de l'image
                    const canvas = document.createElement('canvas');
                    const ctx = canvas.getContext('2d');
                    canvas.width = imgWidth;
                    canvas.height = imgHeight;

                    // Rotation de l'image à 180 degrés
                    ctx.translate(imgWidth / 2, imgHeight / 2);
                    ctx.rotate(Math.PI);
                    ctx.drawImage(img, -imgWidth / 2, -imgHeight / 2, imgWidth, imgHeight);

                    const rotatedImage = canvas.toDataURL('image/jpeg');

                    // Ajouter l'image retournée au PDF avec une taille ajustée
                    doc.addImage(rotatedImage, 'JPEG', margin + x * (cellWidth + margin) + offsetX, margin + y * (cellHeight + margin) + offsetY, drawWidth, drawHeight);
                } else {
                    doc.addImage(img, 'JPEG', margin + x * (cellWidth + margin) + offsetX, margin + y * (cellHeight + margin) + offsetY, drawWidth, drawHeight);
                }

                imgCount++;
                if (x < col - 1) {
                    x++;
                } else {
                    x = 0;
                    y++;
                }
                if (imgCount === files.length) {
                    doc.save(fileName + '.pdf');
                }
            };
        };
        reader.readAsDataURL(file);
    });
}

// Afficher ou masquer l'option "recto verso" en fonction du nombre d'images par page et de l'orientation sélectionnés
function toggleRectoVersoControl() {
    const imagesPerPage = document.querySelector('input[name="imagesPerPage"]:checked').value;
    const orientation = document.querySelector('input[name="orientation"]:checked').value;
    const rectoVersoControl = document.getElementById('rectoVersoControl');
    
    if (imagesPerPage === '6' && orientation === 'landscape') {
        rectoVersoControl.style.display = 'block';
    } else {
        rectoVersoControl.style.display = 'none';
        document.getElementById('rectoVerso').checked = false;
    }
}

document.querySelectorAll('input[name="imagesPerPage"]').forEach(radio => {
    radio.addEventListener('change', toggleRectoVersoControl);
});

document.querySelectorAll('input[name="orientation"]').forEach(radio => {
    radio.addEventListener('change', toggleRectoVersoControl);
});
