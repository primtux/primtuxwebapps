gdjs.infosCode = {};
gdjs.infosCode.GDfond_9595blancObjects1= [];
gdjs.infosCode.GDfond_9595blancObjects2= [];
gdjs.infosCode.GDinfosObjects1= [];
gdjs.infosCode.GDinfosObjects2= [];
gdjs.infosCode.GDscore2Objects1= [];
gdjs.infosCode.GDscore2Objects2= [];
gdjs.infosCode.GDscore1Objects1= [];
gdjs.infosCode.GDscore1Objects2= [];
gdjs.infosCode.GDdoigtObjects1= [];
gdjs.infosCode.GDdoigtObjects2= [];
gdjs.infosCode.GDdoigt2Objects1= [];
gdjs.infosCode.GDdoigt2Objects2= [];
gdjs.infosCode.GDlamaObjects1= [];
gdjs.infosCode.GDlamaObjects2= [];
gdjs.infosCode.GDdrapeau1Objects1= [];
gdjs.infosCode.GDdrapeau1Objects2= [];
gdjs.infosCode.GDdrapeau2Objects1= [];
gdjs.infosCode.GDdrapeau2Objects2= [];
gdjs.infosCode.GDetapeObjects1= [];
gdjs.infosCode.GDetapeObjects2= [];
gdjs.infosCode.GDnb_9595carreObjects1= [];
gdjs.infosCode.GDnb_9595carreObjects2= [];
gdjs.infosCode.GDquestion_9595nbObjects1= [];
gdjs.infosCode.GDquestion_9595nbObjects2= [];
gdjs.infosCode.GDtrainObjects1= [];
gdjs.infosCode.GDtrainObjects2= [];
gdjs.infosCode.GDrailsObjects1= [];
gdjs.infosCode.GDrailsObjects2= [];
gdjs.infosCode.GDtrain2Objects1= [];
gdjs.infosCode.GDtrain2Objects2= [];
gdjs.infosCode.GDpaysage1Objects1= [];
gdjs.infosCode.GDpaysage1Objects2= [];
gdjs.infosCode.GDbouton_9595retourObjects1= [];
gdjs.infosCode.GDbouton_9595retourObjects2= [];
gdjs.infosCode.GDbouton_9595sonObjects1= [];
gdjs.infosCode.GDbouton_9595sonObjects2= [];


gdjs.infosCode.mapOfGDgdjs_9546infosCode_9546GDbouton_95959595retourObjects1Objects = Hashtable.newFrom({"bouton_retour": gdjs.infosCode.GDbouton_9595retourObjects1});
gdjs.infosCode.eventsList0 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("fond_blanc"), gdjs.infosCode.GDfond_9595blancObjects1);
{for(var i = 0, len = gdjs.infosCode.GDfond_9595blancObjects1.length ;i < len;++i) {
    gdjs.infosCode.GDfond_9595blancObjects1[i].setOpacity(150);
}
}{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/audio_vide.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_retour"), gdjs.infosCode.GDbouton_9595retourObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.infosCode.mapOfGDgdjs_9546infosCode_9546GDbouton_95959595retourObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "menu", false);
}}

}


};

gdjs.infosCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.infosCode.GDfond_9595blancObjects1.length = 0;
gdjs.infosCode.GDfond_9595blancObjects2.length = 0;
gdjs.infosCode.GDinfosObjects1.length = 0;
gdjs.infosCode.GDinfosObjects2.length = 0;
gdjs.infosCode.GDscore2Objects1.length = 0;
gdjs.infosCode.GDscore2Objects2.length = 0;
gdjs.infosCode.GDscore1Objects1.length = 0;
gdjs.infosCode.GDscore1Objects2.length = 0;
gdjs.infosCode.GDdoigtObjects1.length = 0;
gdjs.infosCode.GDdoigtObjects2.length = 0;
gdjs.infosCode.GDdoigt2Objects1.length = 0;
gdjs.infosCode.GDdoigt2Objects2.length = 0;
gdjs.infosCode.GDlamaObjects1.length = 0;
gdjs.infosCode.GDlamaObjects2.length = 0;
gdjs.infosCode.GDdrapeau1Objects1.length = 0;
gdjs.infosCode.GDdrapeau1Objects2.length = 0;
gdjs.infosCode.GDdrapeau2Objects1.length = 0;
gdjs.infosCode.GDdrapeau2Objects2.length = 0;
gdjs.infosCode.GDetapeObjects1.length = 0;
gdjs.infosCode.GDetapeObjects2.length = 0;
gdjs.infosCode.GDnb_9595carreObjects1.length = 0;
gdjs.infosCode.GDnb_9595carreObjects2.length = 0;
gdjs.infosCode.GDquestion_9595nbObjects1.length = 0;
gdjs.infosCode.GDquestion_9595nbObjects2.length = 0;
gdjs.infosCode.GDtrainObjects1.length = 0;
gdjs.infosCode.GDtrainObjects2.length = 0;
gdjs.infosCode.GDrailsObjects1.length = 0;
gdjs.infosCode.GDrailsObjects2.length = 0;
gdjs.infosCode.GDtrain2Objects1.length = 0;
gdjs.infosCode.GDtrain2Objects2.length = 0;
gdjs.infosCode.GDpaysage1Objects1.length = 0;
gdjs.infosCode.GDpaysage1Objects2.length = 0;
gdjs.infosCode.GDbouton_9595retourObjects1.length = 0;
gdjs.infosCode.GDbouton_9595retourObjects2.length = 0;
gdjs.infosCode.GDbouton_9595sonObjects1.length = 0;
gdjs.infosCode.GDbouton_9595sonObjects2.length = 0;

gdjs.infosCode.eventsList0(runtimeScene);

return;

}

gdjs['infosCode'] = gdjs.infosCode;
