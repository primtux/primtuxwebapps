gdjs.menuCode = {};
gdjs.menuCode.GDjeu_95950_959510aObjects1_1final = [];

gdjs.menuCode.GDjeu_95950_959510bObjects1_1final = [];

gdjs.menuCode.GDscore1Objects1_1final = [];

gdjs.menuCode.GDscore2Objects1_1final = [];

gdjs.menuCode.GDwagon1Objects1_1final = [];

gdjs.menuCode.GDwagon2Objects1_1final = [];

gdjs.menuCode.GDjeu_95950_959510bObjects1= [];
gdjs.menuCode.GDjeu_95950_959510bObjects2= [];
gdjs.menuCode.GDjeu_95950_959510bObjects3= [];
gdjs.menuCode.GDjeu_95950_959510aObjects1= [];
gdjs.menuCode.GDjeu_95950_959510aObjects2= [];
gdjs.menuCode.GDjeu_95950_959510aObjects3= [];
gdjs.menuCode.GDtitreObjects1= [];
gdjs.menuCode.GDtitreObjects2= [];
gdjs.menuCode.GDtitreObjects3= [];
gdjs.menuCode.GDsergeObjects1= [];
gdjs.menuCode.GDsergeObjects2= [];
gdjs.menuCode.GDsergeObjects3= [];
gdjs.menuCode.GDauteurObjects1= [];
gdjs.menuCode.GDauteurObjects2= [];
gdjs.menuCode.GDauteurObjects3= [];
gdjs.menuCode.GDcreditsObjects1= [];
gdjs.menuCode.GDcreditsObjects2= [];
gdjs.menuCode.GDcreditsObjects3= [];
gdjs.menuCode.GDversionObjects1= [];
gdjs.menuCode.GDversionObjects2= [];
gdjs.menuCode.GDversionObjects3= [];
gdjs.menuCode.GDdixObjects1= [];
gdjs.menuCode.GDdixObjects2= [];
gdjs.menuCode.GDdixObjects3= [];
gdjs.menuCode.GDtrain2Objects1= [];
gdjs.menuCode.GDtrain2Objects2= [];
gdjs.menuCode.GDtrain2Objects3= [];
gdjs.menuCode.GDplein_9595ecranObjects1= [];
gdjs.menuCode.GDplein_9595ecranObjects2= [];
gdjs.menuCode.GDplein_9595ecranObjects3= [];
gdjs.menuCode.GDbouton_9595infoObjects1= [];
gdjs.menuCode.GDbouton_9595infoObjects2= [];
gdjs.menuCode.GDbouton_9595infoObjects3= [];
gdjs.menuCode.GDwagon1Objects1= [];
gdjs.menuCode.GDwagon1Objects2= [];
gdjs.menuCode.GDwagon1Objects3= [];
gdjs.menuCode.GDwagon2Objects1= [];
gdjs.menuCode.GDwagon2Objects2= [];
gdjs.menuCode.GDwagon2Objects3= [];
gdjs.menuCode.GDscore2Objects1= [];
gdjs.menuCode.GDscore2Objects2= [];
gdjs.menuCode.GDscore2Objects3= [];
gdjs.menuCode.GDscore1Objects1= [];
gdjs.menuCode.GDscore1Objects2= [];
gdjs.menuCode.GDscore1Objects3= [];
gdjs.menuCode.GDdoigtObjects1= [];
gdjs.menuCode.GDdoigtObjects2= [];
gdjs.menuCode.GDdoigtObjects3= [];
gdjs.menuCode.GDdoigt2Objects1= [];
gdjs.menuCode.GDdoigt2Objects2= [];
gdjs.menuCode.GDdoigt2Objects3= [];
gdjs.menuCode.GDlamaObjects1= [];
gdjs.menuCode.GDlamaObjects2= [];
gdjs.menuCode.GDlamaObjects3= [];
gdjs.menuCode.GDdrapeau1Objects1= [];
gdjs.menuCode.GDdrapeau1Objects2= [];
gdjs.menuCode.GDdrapeau1Objects3= [];
gdjs.menuCode.GDdrapeau2Objects1= [];
gdjs.menuCode.GDdrapeau2Objects2= [];
gdjs.menuCode.GDdrapeau2Objects3= [];
gdjs.menuCode.GDetapeObjects1= [];
gdjs.menuCode.GDetapeObjects2= [];
gdjs.menuCode.GDetapeObjects3= [];
gdjs.menuCode.GDnb_9595carreObjects1= [];
gdjs.menuCode.GDnb_9595carreObjects2= [];
gdjs.menuCode.GDnb_9595carreObjects3= [];
gdjs.menuCode.GDquestion_9595nbObjects1= [];
gdjs.menuCode.GDquestion_9595nbObjects2= [];
gdjs.menuCode.GDquestion_9595nbObjects3= [];
gdjs.menuCode.GDtrainObjects1= [];
gdjs.menuCode.GDtrainObjects2= [];
gdjs.menuCode.GDtrainObjects3= [];
gdjs.menuCode.GDrailsObjects1= [];
gdjs.menuCode.GDrailsObjects2= [];
gdjs.menuCode.GDrailsObjects3= [];
gdjs.menuCode.GDtrain2Objects1= [];
gdjs.menuCode.GDtrain2Objects2= [];
gdjs.menuCode.GDtrain2Objects3= [];
gdjs.menuCode.GDpaysage1Objects1= [];
gdjs.menuCode.GDpaysage1Objects2= [];
gdjs.menuCode.GDpaysage1Objects3= [];
gdjs.menuCode.GDbouton_9595retourObjects1= [];
gdjs.menuCode.GDbouton_9595retourObjects2= [];
gdjs.menuCode.GDbouton_9595retourObjects3= [];
gdjs.menuCode.GDbouton_9595sonObjects1= [];
gdjs.menuCode.GDbouton_9595sonObjects2= [];
gdjs.menuCode.GDbouton_9595sonObjects3= [];


gdjs.menuCode.eventsList0 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.storage.elementExistsInJSONFile("sauvegarde_axe10", "score");
if (isConditionTrue_0) {
{gdjs.evtTools.storage.readStringFromJSONFile("sauvegarde_axe10", "score", runtimeScene, runtimeScene.getScene().getVariables().get("sauvegarde"));
}{gdjs.evtTools.network.jsonToVariableStructure(gdjs.evtTools.variable.getVariableString(runtimeScene.getScene().getVariables().get("sauvegarde")), runtimeScene.getGame().getVariables().getFromIndex(9));
}}

}


};gdjs.menuCode.eventsList1 = function(runtimeScene) {

{


gdjs.menuCode.eventsList0(runtimeScene);
}


{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("score1"), gdjs.menuCode.GDscore1Objects1);
gdjs.copyArray(runtimeScene.getObjects("score2"), gdjs.menuCode.GDscore2Objects1);
{for(var i = 0, len = gdjs.menuCode.GDscore1Objects1.length ;i < len;++i) {
    gdjs.menuCode.GDscore1Objects1[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(9).getChild("score1")));
}
}{for(var i = 0, len = gdjs.menuCode.GDscore2Objects1.length ;i < len;++i) {
    gdjs.menuCode.GDscore2Objects1[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(9).getChild("score2")));
}
}}

}


};gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDjeu_959595950_9595959510aObjects2Objects = Hashtable.newFrom({"jeu_0_10a": gdjs.menuCode.GDjeu_95950_959510aObjects2});
gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDscore1Objects2Objects = Hashtable.newFrom({"score1": gdjs.menuCode.GDscore1Objects2});
gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDwagon1Objects2Objects = Hashtable.newFrom({"wagon1": gdjs.menuCode.GDwagon1Objects2});
gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDjeu_959595950_9595959510bObjects2Objects = Hashtable.newFrom({"jeu_0_10b": gdjs.menuCode.GDjeu_95950_959510bObjects2});
gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDwagon2Objects2Objects = Hashtable.newFrom({"wagon2": gdjs.menuCode.GDwagon2Objects2});
gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDscore2Objects2Objects = Hashtable.newFrom({"score2": gdjs.menuCode.GDscore2Objects2});
gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDsergeObjects1Objects = Hashtable.newFrom({"serge": gdjs.menuCode.GDsergeObjects1});
gdjs.menuCode.eventsList2 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
{gdjs.evtTools.storage.clearJSONFile("sauvegarde_axe10");
}{gdjs.evtTools.storage.writeStringInJSONFile("sauvegarde_axe10", "score", gdjs.evtTools.network.variableStructureToJSON(runtimeScene.getGame().getVariables().getFromIndex(9)));
}}

}


};gdjs.menuCode.eventsList3 = function(runtimeScene) {

{


gdjs.menuCode.eventsList2(runtimeScene);
}


{


let isConditionTrue_0 = false;
{
}

}


};gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDplein_95959595ecranObjects1Objects = Hashtable.newFrom({"plein_ecran": gdjs.menuCode.GDplein_9595ecranObjects1});
gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDplein_95959595ecranObjects1Objects = Hashtable.newFrom({"plein_ecran": gdjs.menuCode.GDplein_9595ecranObjects1});
gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDbouton_95959595infoObjects1Objects = Hashtable.newFrom({"bouton_info": gdjs.menuCode.GDbouton_9595infoObjects1});
gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDbouton_95959595sonObjects2Objects = Hashtable.newFrom({"bouton_son": gdjs.menuCode.GDbouton_9595sonObjects2});
gdjs.menuCode.eventsList4 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("bouton_son"), gdjs.menuCode.GDbouton_9595sonObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDbouton_95959595sonObjects2Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(8).add(1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(8)) == 0;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(13510108);
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("bouton_son"), gdjs.menuCode.GDbouton_9595sonObjects2);
{for(var i = 0, len = gdjs.menuCode.GDbouton_9595sonObjects2.length ;i < len;++i) {
    gdjs.menuCode.GDbouton_9595sonObjects2[i].setAnimationFrame(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(8)));
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(8)) == 1;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(13511676);
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("bouton_son"), gdjs.menuCode.GDbouton_9595sonObjects2);
{for(var i = 0, len = gdjs.menuCode.GDbouton_9595sonObjects2.length ;i < len;++i) {
    gdjs.menuCode.GDbouton_9595sonObjects2[i].setAnimationFrame(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(8)));
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(8)) == 2;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(13521820);
}
}
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(8).setNumber(0);
}}

}


};gdjs.menuCode.eventsList5 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("bouton_son"), gdjs.menuCode.GDbouton_9595sonObjects1);
{for(var i = 0, len = gdjs.menuCode.GDbouton_9595sonObjects1.length ;i < len;++i) {
    gdjs.menuCode.GDbouton_9595sonObjects1[i].pauseAnimation();
}
}{for(var i = 0, len = gdjs.menuCode.GDbouton_9595sonObjects1.length ;i < len;++i) {
    gdjs.menuCode.GDbouton_9595sonObjects1[i].setAnimationFrame(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(8)));
}
}{runtimeScene.getScene().getVariables().get("score_zero").setNumber(0);
}
{ //Subevents
gdjs.menuCode.eventsList1(runtimeScene);} //End of subevents
}

}


{

gdjs.menuCode.GDjeu_95950_959510aObjects1.length = 0;

gdjs.menuCode.GDscore1Objects1.length = 0;

gdjs.menuCode.GDwagon1Objects1.length = 0;


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{gdjs.menuCode.GDjeu_95950_959510aObjects1_1final.length = 0;
gdjs.menuCode.GDscore1Objects1_1final.length = 0;
gdjs.menuCode.GDwagon1Objects1_1final.length = 0;
let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("jeu_0_10a"), gdjs.menuCode.GDjeu_95950_959510aObjects2);
isConditionTrue_1 = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDjeu_959595950_9595959510aObjects2Objects, runtimeScene, true, false);
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.menuCode.GDjeu_95950_959510aObjects2.length; j < jLen ; ++j) {
        if ( gdjs.menuCode.GDjeu_95950_959510aObjects1_1final.indexOf(gdjs.menuCode.GDjeu_95950_959510aObjects2[j]) === -1 )
            gdjs.menuCode.GDjeu_95950_959510aObjects1_1final.push(gdjs.menuCode.GDjeu_95950_959510aObjects2[j]);
    }
}
}
{
gdjs.copyArray(runtimeScene.getObjects("score1"), gdjs.menuCode.GDscore1Objects2);
isConditionTrue_1 = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDscore1Objects2Objects, runtimeScene, true, false);
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.menuCode.GDscore1Objects2.length; j < jLen ; ++j) {
        if ( gdjs.menuCode.GDscore1Objects1_1final.indexOf(gdjs.menuCode.GDscore1Objects2[j]) === -1 )
            gdjs.menuCode.GDscore1Objects1_1final.push(gdjs.menuCode.GDscore1Objects2[j]);
    }
}
}
{
gdjs.copyArray(runtimeScene.getObjects("wagon1"), gdjs.menuCode.GDwagon1Objects2);
isConditionTrue_1 = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDwagon1Objects2Objects, runtimeScene, true, false);
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.menuCode.GDwagon1Objects2.length; j < jLen ; ++j) {
        if ( gdjs.menuCode.GDwagon1Objects1_1final.indexOf(gdjs.menuCode.GDwagon1Objects2[j]) === -1 )
            gdjs.menuCode.GDwagon1Objects1_1final.push(gdjs.menuCode.GDwagon1Objects2[j]);
    }
}
}
{
gdjs.copyArray(gdjs.menuCode.GDjeu_95950_959510aObjects1_1final, gdjs.menuCode.GDjeu_95950_959510aObjects1);
gdjs.copyArray(gdjs.menuCode.GDscore1Objects1_1final, gdjs.menuCode.GDscore1Objects1);
gdjs.copyArray(gdjs.menuCode.GDwagon1Objects1_1final, gdjs.menuCode.GDwagon1Objects1);
}
}
}
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(1).getChild("mini").setNumber(0);
}{runtimeScene.getGame().getVariables().getFromIndex(1).getChild("maxi").setNumber(10);
}{runtimeScene.getGame().getVariables().getFromIndex(3).setNumber(1);
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "tirage", false);
}}

}


{

gdjs.menuCode.GDjeu_95950_959510bObjects1.length = 0;

gdjs.menuCode.GDscore2Objects1.length = 0;

gdjs.menuCode.GDwagon2Objects1.length = 0;


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{gdjs.menuCode.GDjeu_95950_959510bObjects1_1final.length = 0;
gdjs.menuCode.GDscore2Objects1_1final.length = 0;
gdjs.menuCode.GDwagon2Objects1_1final.length = 0;
let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("jeu_0_10b"), gdjs.menuCode.GDjeu_95950_959510bObjects2);
isConditionTrue_1 = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDjeu_959595950_9595959510bObjects2Objects, runtimeScene, true, false);
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.menuCode.GDjeu_95950_959510bObjects2.length; j < jLen ; ++j) {
        if ( gdjs.menuCode.GDjeu_95950_959510bObjects1_1final.indexOf(gdjs.menuCode.GDjeu_95950_959510bObjects2[j]) === -1 )
            gdjs.menuCode.GDjeu_95950_959510bObjects1_1final.push(gdjs.menuCode.GDjeu_95950_959510bObjects2[j]);
    }
}
}
{
gdjs.copyArray(runtimeScene.getObjects("wagon2"), gdjs.menuCode.GDwagon2Objects2);
isConditionTrue_1 = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDwagon2Objects2Objects, runtimeScene, true, false);
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.menuCode.GDwagon2Objects2.length; j < jLen ; ++j) {
        if ( gdjs.menuCode.GDwagon2Objects1_1final.indexOf(gdjs.menuCode.GDwagon2Objects2[j]) === -1 )
            gdjs.menuCode.GDwagon2Objects1_1final.push(gdjs.menuCode.GDwagon2Objects2[j]);
    }
}
}
{
gdjs.copyArray(runtimeScene.getObjects("score2"), gdjs.menuCode.GDscore2Objects2);
isConditionTrue_1 = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDscore2Objects2Objects, runtimeScene, true, false);
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.menuCode.GDscore2Objects2.length; j < jLen ; ++j) {
        if ( gdjs.menuCode.GDscore2Objects1_1final.indexOf(gdjs.menuCode.GDscore2Objects2[j]) === -1 )
            gdjs.menuCode.GDscore2Objects1_1final.push(gdjs.menuCode.GDscore2Objects2[j]);
    }
}
}
{
gdjs.copyArray(gdjs.menuCode.GDjeu_95950_959510bObjects1_1final, gdjs.menuCode.GDjeu_95950_959510bObjects1);
gdjs.copyArray(gdjs.menuCode.GDscore2Objects1_1final, gdjs.menuCode.GDscore2Objects1);
gdjs.copyArray(gdjs.menuCode.GDwagon2Objects1_1final, gdjs.menuCode.GDwagon2Objects1);
}
}
}
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(1).getChild("mini").setNumber(0);
}{runtimeScene.getGame().getVariables().getFromIndex(1).getChild("maxi").setNumber(10);
}{runtimeScene.getGame().getVariables().getFromIndex(3).setNumber(2);
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "tirage", false);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
}
}
}
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(1).getChild("mini").setNumber(0);
}{runtimeScene.getGame().getVariables().getFromIndex(1).getChild("maxi").setNumber(20);
}{runtimeScene.getGame().getVariables().getFromIndex(3).setNumber(3);
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "tirage", false);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("serge"), gdjs.menuCode.GDsergeObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDsergeObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {
{runtimeScene.getScene().getVariables().get("score_zero").add(1);
}{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/bulle_1.mp3", 1, false, 100, 1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().get("score_zero")) == 5;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("score1"), gdjs.menuCode.GDscore1Objects1);
gdjs.copyArray(runtimeScene.getObjects("score2"), gdjs.menuCode.GDscore2Objects1);
{runtimeScene.getScene().getVariables().get("score_zero").setNumber(0);
}{runtimeScene.getGame().getVariables().getFromIndex(9).getChild("score1").setNumber(0);
}{for(var i = 0, len = gdjs.menuCode.GDscore1Objects1.length ;i < len;++i) {
    gdjs.menuCode.GDscore1Objects1[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(9).getChild("score1")));
}
}{runtimeScene.getGame().getVariables().getFromIndex(9).getChild("score2").setNumber(0);
}{for(var i = 0, len = gdjs.menuCode.GDscore2Objects1.length ;i < len;++i) {
    gdjs.menuCode.GDscore2Objects1[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(9).getChild("score2")));
}
}
{ //Subevents
gdjs.menuCode.eventsList3(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("train2"), gdjs.menuCode.GDtrain2Objects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.menuCode.GDtrain2Objects1.length;i<l;++i) {
    if ( gdjs.menuCode.GDtrain2Objects1[i].getX() < 400 ) {
        isConditionTrue_0 = true;
        gdjs.menuCode.GDtrain2Objects1[k] = gdjs.menuCode.GDtrain2Objects1[i];
        ++k;
    }
}
gdjs.menuCode.GDtrain2Objects1.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.menuCode.GDtrain2Objects1 */
{for(var i = 0, len = gdjs.menuCode.GDtrain2Objects1.length ;i < len;++i) {
    gdjs.menuCode.GDtrain2Objects1[i].setX(gdjs.menuCode.GDtrain2Objects1[i].getX() + (4));
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("plein_ecran"), gdjs.menuCode.GDplein_9595ecranObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDplein_95959595ecranObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(6)) == 0;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(13513836);
}
}
}
}
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(6).setNumber(1);
}{gdjs.evtTools.window.setFullScreen(runtimeScene, true, true);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("plein_ecran"), gdjs.menuCode.GDplein_9595ecranObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDplein_95959595ecranObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(6)) == 1;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(13519748);
}
}
}
}
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(6).setNumber(0);
}{gdjs.evtTools.window.setFullScreen(runtimeScene, false, true);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_info"), gdjs.menuCode.GDbouton_9595infoObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDbouton_95959595infoObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "infos", false);
}}

}


{


gdjs.menuCode.eventsList4(runtimeScene);
}


};

gdjs.menuCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.menuCode.GDjeu_95950_959510bObjects1.length = 0;
gdjs.menuCode.GDjeu_95950_959510bObjects2.length = 0;
gdjs.menuCode.GDjeu_95950_959510bObjects3.length = 0;
gdjs.menuCode.GDjeu_95950_959510aObjects1.length = 0;
gdjs.menuCode.GDjeu_95950_959510aObjects2.length = 0;
gdjs.menuCode.GDjeu_95950_959510aObjects3.length = 0;
gdjs.menuCode.GDtitreObjects1.length = 0;
gdjs.menuCode.GDtitreObjects2.length = 0;
gdjs.menuCode.GDtitreObjects3.length = 0;
gdjs.menuCode.GDsergeObjects1.length = 0;
gdjs.menuCode.GDsergeObjects2.length = 0;
gdjs.menuCode.GDsergeObjects3.length = 0;
gdjs.menuCode.GDauteurObjects1.length = 0;
gdjs.menuCode.GDauteurObjects2.length = 0;
gdjs.menuCode.GDauteurObjects3.length = 0;
gdjs.menuCode.GDcreditsObjects1.length = 0;
gdjs.menuCode.GDcreditsObjects2.length = 0;
gdjs.menuCode.GDcreditsObjects3.length = 0;
gdjs.menuCode.GDversionObjects1.length = 0;
gdjs.menuCode.GDversionObjects2.length = 0;
gdjs.menuCode.GDversionObjects3.length = 0;
gdjs.menuCode.GDdixObjects1.length = 0;
gdjs.menuCode.GDdixObjects2.length = 0;
gdjs.menuCode.GDdixObjects3.length = 0;
gdjs.menuCode.GDtrain2Objects1.length = 0;
gdjs.menuCode.GDtrain2Objects2.length = 0;
gdjs.menuCode.GDtrain2Objects3.length = 0;
gdjs.menuCode.GDplein_9595ecranObjects1.length = 0;
gdjs.menuCode.GDplein_9595ecranObjects2.length = 0;
gdjs.menuCode.GDplein_9595ecranObjects3.length = 0;
gdjs.menuCode.GDbouton_9595infoObjects1.length = 0;
gdjs.menuCode.GDbouton_9595infoObjects2.length = 0;
gdjs.menuCode.GDbouton_9595infoObjects3.length = 0;
gdjs.menuCode.GDwagon1Objects1.length = 0;
gdjs.menuCode.GDwagon1Objects2.length = 0;
gdjs.menuCode.GDwagon1Objects3.length = 0;
gdjs.menuCode.GDwagon2Objects1.length = 0;
gdjs.menuCode.GDwagon2Objects2.length = 0;
gdjs.menuCode.GDwagon2Objects3.length = 0;
gdjs.menuCode.GDscore2Objects1.length = 0;
gdjs.menuCode.GDscore2Objects2.length = 0;
gdjs.menuCode.GDscore2Objects3.length = 0;
gdjs.menuCode.GDscore1Objects1.length = 0;
gdjs.menuCode.GDscore1Objects2.length = 0;
gdjs.menuCode.GDscore1Objects3.length = 0;
gdjs.menuCode.GDdoigtObjects1.length = 0;
gdjs.menuCode.GDdoigtObjects2.length = 0;
gdjs.menuCode.GDdoigtObjects3.length = 0;
gdjs.menuCode.GDdoigt2Objects1.length = 0;
gdjs.menuCode.GDdoigt2Objects2.length = 0;
gdjs.menuCode.GDdoigt2Objects3.length = 0;
gdjs.menuCode.GDlamaObjects1.length = 0;
gdjs.menuCode.GDlamaObjects2.length = 0;
gdjs.menuCode.GDlamaObjects3.length = 0;
gdjs.menuCode.GDdrapeau1Objects1.length = 0;
gdjs.menuCode.GDdrapeau1Objects2.length = 0;
gdjs.menuCode.GDdrapeau1Objects3.length = 0;
gdjs.menuCode.GDdrapeau2Objects1.length = 0;
gdjs.menuCode.GDdrapeau2Objects2.length = 0;
gdjs.menuCode.GDdrapeau2Objects3.length = 0;
gdjs.menuCode.GDetapeObjects1.length = 0;
gdjs.menuCode.GDetapeObjects2.length = 0;
gdjs.menuCode.GDetapeObjects3.length = 0;
gdjs.menuCode.GDnb_9595carreObjects1.length = 0;
gdjs.menuCode.GDnb_9595carreObjects2.length = 0;
gdjs.menuCode.GDnb_9595carreObjects3.length = 0;
gdjs.menuCode.GDquestion_9595nbObjects1.length = 0;
gdjs.menuCode.GDquestion_9595nbObjects2.length = 0;
gdjs.menuCode.GDquestion_9595nbObjects3.length = 0;
gdjs.menuCode.GDtrainObjects1.length = 0;
gdjs.menuCode.GDtrainObjects2.length = 0;
gdjs.menuCode.GDtrainObjects3.length = 0;
gdjs.menuCode.GDrailsObjects1.length = 0;
gdjs.menuCode.GDrailsObjects2.length = 0;
gdjs.menuCode.GDrailsObjects3.length = 0;
gdjs.menuCode.GDtrain2Objects1.length = 0;
gdjs.menuCode.GDtrain2Objects2.length = 0;
gdjs.menuCode.GDtrain2Objects3.length = 0;
gdjs.menuCode.GDpaysage1Objects1.length = 0;
gdjs.menuCode.GDpaysage1Objects2.length = 0;
gdjs.menuCode.GDpaysage1Objects3.length = 0;
gdjs.menuCode.GDbouton_9595retourObjects1.length = 0;
gdjs.menuCode.GDbouton_9595retourObjects2.length = 0;
gdjs.menuCode.GDbouton_9595retourObjects3.length = 0;
gdjs.menuCode.GDbouton_9595sonObjects1.length = 0;
gdjs.menuCode.GDbouton_9595sonObjects2.length = 0;
gdjs.menuCode.GDbouton_9595sonObjects3.length = 0;

gdjs.menuCode.eventsList5(runtimeScene);

return;

}

gdjs['menuCode'] = gdjs.menuCode;
