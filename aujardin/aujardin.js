var CtLegumes = [0,0,0];
var rapportHautLargeImgLegumes = [3,0.7,1];
var LegumeparEnclos = [];
var imgLegumeparEnclos = [];

var solution = [];
var proposition = [];
for (var i=0; i<4; i++) {
  solution[i] = [0,0,0];
  proposition[i] = [0,0,0];
}
var dragObjet = null;
var currentDroppable = null;
var PositionX;
var PositionY;
var difficulte = "";
var informationsObjet = document.getElementById("informations");
var Poubelle = document.getElementById("poubelle");
var zoomLegume1 = document.createElement("img");
zoomLegume1.src = "img/poireau.png";
var zoomLegume2 = document.createElement("img");
zoomLegume2.src = "img/pomme.png";
var zoomLegume3 = document.createElement("img");
zoomLegume3.src = "img/salade.png";

var btNiveau1 = document.getElementById("Niveau1");
var btNiveau2 = document.getElementById("Niveau2");
var btNewExo = document.getElementById("newExo");
var btVerif = document.getElementById("verifier");

var Modele1 = document.getElementById("modele1");
var Modele2 = document.getElementById("modele2");
var Modele3 = document.getElementById("modele3");

function alea(min,max) {
  var nb = min + (max-min+1) * Math.random();
  return Math.floor(nb);
}

function estPresent(nombre, tableau) {
  for (var i = 0; i < tableau.length; i++) {
    if ( tableau[i] === nombre ) {
      return true;
    }
  }
  return false;
}

function aleaDifferent(min,max,quantite) {
  var tableau = [];
  var nombre;
  for (var i = 0; i < quantite; i++) {
    do {
      nombre = alea(min, max);
      } while (estPresent(nombre, tableau));
    tableau[i] = nombre;
  }
  return tableau;
}

function melange(tableau) {
  var tirage;
  var tab_alea = [];

  for (var i = 0; i < tableau.length; i++) {
    do {
      tirage = alea(0, tableau.length -1);
    } while (tableau[tirage] === 0);
    tab_alea[i] = tableau[tirage];
    tableau[tirage] = 0;
  }
  return tab_alea;
}

function activeBtNiveau() {
  document.getElementById("Niveau1").disabled = false;
  document.getElementById("Niveau2").disabled = false;
}

function desactiveBtNiveau() {
  document.getElementById("Niveau1").disabled = true;
  document.getElementById("Niveau2").disabled = true;
}

function nouvelExercice() {
  tirageLegumes();
  afficheExercice();
  document.getElementById("verifier").disabled = false;
}

function defNiveau1() {
  difficulte = btNiveau1.value;
  desactiveBtNiveau();
  nouvelExercice();
}

function defNiveau2() {
  difficulte = btNiveau2.value;
  desactiveBtNiveau();
  nouvelExercice();
}

function tirageLegumes() {
  var EnclosAleatoire = [1,2,3];
  EnclosAleatoire = melange(EnclosAleatoire);
  var valeurs = [];

  switch(difficulte) {
    case "Niveau1":
      valeurs = aleaDifferent(1,10,3);
      break;
    case "Niveau2":
      valeurs = aleaDifferent(10,20,3);
      break;
  }
  solution[0][EnclosAleatoire[0]-1] = valeurs[0];
  solution[1][EnclosAleatoire[1]-1] = valeurs[1];
  solution[2][EnclosAleatoire[2]-1] = valeurs[2];
  for (var i=0; i<3; i++) {
    LegumeparEnclos[EnclosAleatoire[i]-1] = i;
  }
}

function verification() {
  var IdErreur;
  var NoErreur;
  var erreur = false;
  var SolutionParEnclos = [];
  var PropositionParEnclos = [];
  for (var i = 0; i < 3; i++) {
    SolutionParEnclos[i] = [0,0,0];
    PropositionParEnclos[i] = [0,0,0];
  }
  for (var i = 0; i < 3; i++) {
    for (var j = 0; j < 3; j++) {
      SolutionParEnclos[j][i] = solution[i][j];
      PropositionParEnclos[j][i] = proposition[i][j];
    }
  }
  for (var i = 0; i < 3; i++) {
    NoErreur = i + 1;
    IdErreur = "erreur" + NoErreur;
    if (PropositionParEnclos[i].join() === SolutionParEnclos[i].join()) {
      document.getElementById(IdErreur).innerHTML = "";
      }
      else {
        document.getElementById(IdErreur).innerHTML = "X";
        erreur = true;
      }
  }
  if (! erreur) {
    showDialog('Bravo !',0.5,'img/happy-tux.png', 89, 91, 'left');
    document.getElementById("verifier").disabled = true;
    reinitialise();
  }
}

function effaceLegumes() {
  var tab = document.getElementsByClassName("enclos");
  for (var i = 0; i < tab.length; i++) {
    while (tab[i].firstChild) {
      tab[i].removeChild(tab[i].firstChild);
    }
  }
}

function reinitialise() {
  var IdErreur;
  var NoErreur;
  for (var i = 0; i < 3; i++) {
    for (var j = 0; j < 3; j++) {
      solution[i][j] = 0;
      proposition[i][j] = 0;
    }
    NoErreur = i + 1;
    IdErreur = "erreur" + NoErreur;
    document.getElementById(IdErreur).innerHTML = "";
  }
  activeBtNiveau();
  Modele1.innerHTML = "";
  Modele2.innerHTML = "";
  Modele3.innerHTML = "";
  Modele1.classList.remove("modele-actif");
  Modele2.classList.remove("modele-actif");
  Modele3.classList.remove("modele-actif");
  effaceLegumes();
}

function getNomLegume(objet) {
  var Nom = "";
  var tableau = objet.split("-");
  Nom = tableau[0];
  return Nom;
}

function getIdLegume(objet) {
  var IdLegume = "";
  var tableau = objet.split("-");
  IdLegume = tableau[1];
  return IdLegume;
}

function getIdAccueil(objet) {
  var IdAccueil = "";
  var tableau = objet.split("-");
  IdAccueil = tableau[2];
  return IdAccueil;
}

function getIndiceLegume(legume) {
  return parseInt(legume.charAt(6) - 1);
}

function getIndiceEnclos(enclos) {
  return parseInt(enclos.charAt(6) - 1);
}

function getImgLegumeparEnclos() {
  for (var i=0; i<3; i++) {
    switch(LegumeparEnclos[i]) {
      case 0:
        imgLegumeparEnclos[i] = "img/poireau-mini.png";
        break;
      case 1:
        imgLegumeparEnclos[i] = "img/pomme-mini.png";
        break;
      case 2:
        imgLegumeparEnclos[i] = "img/salade-mini.png";
        break;
   }
  }
}

function allowDrop(ev) {
  ev.preventDefault();
}

function survolPig(ev) {
  ev.preventDefault();
  Poubelle.src="img/cochon-croque.png";
}

function finSurvolPig(ev) {
  Poubelle.src="img/cochon.png";
}

function drag(ev) {
  var infosObjet;

  function afficheImgZoom(legume) {
    switch(legume) {
      case 'legume1':
        var offsetX = zoomLegume1.width / 2;
        var offsetY = zoomLegume1.height /2;
        ev.dataTransfer.setDragImage(zoomLegume1, offsetX, offsetY);
        dragObjet = zoomLegume1;
        break;
      case 'legume2':
        var offsetX = zoomLegume2.width / 2;
        var offsetY = zoomLegume2.height /2;
        ev.dataTransfer.setDragImage(zoomLegume2, offsetX, offsetY);
        dragObjet = zoomLegume2;
        break;
      case 'legume3':
        var offsetX = zoomLegume3.width / 2;
        var offsetY = zoomLegume3.height /2;
        ev.dataTransfer.setDragImage(zoomLegume3, offsetX, offsetY);
        dragObjet = zoomLegume3;
        break;
    }
  }

  infosObjet = ev.target.alt + "-" + ev.target.id + "-" + ev.target.parentNode.id;
  afficheImgZoom(ev.target.alt);
  ev.dataTransfer.setData("text", infosObjet);
}

function detruit(ev) {
  ev.preventDefault();
  Poubelle.src="img/cochon.png";
  var IdLegume = getIdLegume(ev.dataTransfer.getData("text"));
  var Legume = document.getElementById(IdLegume);
  var NomLegume = getNomLegume(ev.dataTransfer.getData("Text"));
  var NoLegume = getIndiceLegume(NomLegume);
  var IdDepart = getIdAccueil(ev.dataTransfer.getData("text"));
  var NoEnclos = getIndiceEnclos(IdDepart);
  if (IdDepart != "reserve") {
      Legume.parentNode.removeChild(Legume);
      proposition[NoLegume][NoEnclos] -= 1;
  }
}

function drop(ev) {

  function getPositionSouris() {
    dragObjet.hidden = true;
    PositionX = ev.offsetX;
    PositionY = ev.offsetY;
    dragObjet.hidden = false;
  }

  function fixePosition(element) {
    var positionGauche;
    var positionDroite;
    var largeur;
    var hauteur;
    var positionInfo = ev.target.getBoundingClientRect();
    largeur = element.offsetWidth * 100 / positionInfo.width;
    switch (element.id.substring(0,7)) {
      case "legume1":
        hauteur = element.offsetWidth * rapportHautLargeImgLegumes[0] *100 / positionInfo.height;
        break;
      case "legume2":
        hauteur = element.offsetWidth * rapportHautLargeImgLegumes[1] *100 / positionInfo.height;
        break;
      case "legume3":
        hauteur = element.offsetWidth * rapportHautLargeImgLegumes[2] *100 / positionInfo.height;
        break;
    }
    positionGauche = (PositionX * 100 / positionInfo.width) - (largeur / 2);
    positionHaute = (PositionY * 100 / positionInfo.height) -(hauteur / 2);
    if (positionGauche< 0) {
      positionGauche = 0;
    }
    if ((positionHaute + hauteur) > 100) {
      positionHaute = 100 - hauteur;
    }
    element.style.left = positionGauche + "%";
    element.style.top = positionHaute + "%";
  }

  ev.preventDefault();
  var Legume = getNomLegume(ev.dataTransfer.getData('Text'));
  var NoLegume = getIndiceLegume(Legume);
  var IdLegume = getIdLegume(ev.dataTransfer.getData("text"));
  var IdDepart = getIdAccueil(ev.dataTransfer.getData("text"));
  var NoEnclos = getIndiceEnclos(ev.target.id);
  if ((IdDepart === "reserve") && (ev.target.id.substring(0,6) === "enclos")) {
      getPositionSouris();
      var objet = new Image();
      objet.src = "img/" + Legume + ".png";
      objet.id = Legume + "_" + CtLegumes[NoLegume];
      objet.className = Legume;
      objet.alt = Legume;
      objet.addEventListener("dragstart", drag);
      ev.target.appendChild(objet);
      fixePosition(objet);
      CtLegumes[NoLegume] += 1;
      proposition[NoLegume][NoEnclos] += 1;
    }
    else if ((IdDepart.substring(0,6) === "enclos")  && (ev.target.id.substring(0,6) === "enclos")) {
      getPositionSouris();
      ev.target.appendChild(document.getElementById(IdLegume));
      fixePosition(document.getElementById(IdLegume));
      proposition[NoLegume][getIndiceEnclos(IdDepart)] -= 1;
      proposition[NoLegume][NoEnclos] += 1;
    }
}

function afficheExercice() {
  getImgLegumeparEnclos();
  Modele1.innerHTML = '<img src="' + imgLegumeparEnclos[0] + '"> ' + solution[LegumeparEnclos[0]][0];
  Modele1.classList.add("modele-actif");
  Modele2.innerHTML = '<img src="' + imgLegumeparEnclos[1] + '"> ' + solution[LegumeparEnclos[1]][1];
  Modele2.classList.add("modele-actif");
  Modele3.innerHTML = '<img src="' + imgLegumeparEnclos[2] + '"> ' + solution[LegumeparEnclos[2]][2];
  Modele3.classList.add("modele-actif");
}
