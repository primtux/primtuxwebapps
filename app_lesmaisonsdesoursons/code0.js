gdjs.introCode = {};
gdjs.introCode.localVariables = [];
gdjs.introCode.GDintro_9595remerciementsObjects1= [];
gdjs.introCode.GDintro_9595remerciementsObjects2= [];
gdjs.introCode.GDsp_9595bouton_9595validerObjects1= [];
gdjs.introCode.GDsp_9595bouton_9595validerObjects2= [];
gdjs.introCode.GDsp_9595bouton_9595retourObjects1= [];
gdjs.introCode.GDsp_9595bouton_9595retourObjects2= [];
gdjs.introCode.GDsp_9595bouton_9595chiffre_9595deObjects1= [];
gdjs.introCode.GDsp_9595bouton_9595chiffre_9595deObjects2= [];
gdjs.introCode.GDtxt_9595representation_9595quantiteObjects1= [];
gdjs.introCode.GDtxt_9595representation_9595quantiteObjects2= [];
gdjs.introCode.GDsp_9595bouton_9595rejouerObjects1= [];
gdjs.introCode.GDsp_9595bouton_9595rejouerObjects2= [];
gdjs.introCode.GDsp_9595scoreObjects1= [];
gdjs.introCode.GDsp_9595scoreObjects2= [];
gdjs.introCode.GDsp_9595faux_9595vraiObjects1= [];
gdjs.introCode.GDsp_9595faux_9595vraiObjects2= [];
gdjs.introCode.GDsp_9595bouton_9595aideObjects1= [];
gdjs.introCode.GDsp_9595bouton_9595aideObjects2= [];
gdjs.introCode.GDsp_9595maisonObjects1= [];
gdjs.introCode.GDsp_9595maisonObjects2= [];
gdjs.introCode.GDsp_9595fond_9595maison_95951Objects1= [];
gdjs.introCode.GDsp_9595fond_9595maison_95951Objects2= [];
gdjs.introCode.GDsp_9595fond_9595maison_95952Objects1= [];
gdjs.introCode.GDsp_9595fond_9595maison_95952Objects2= [];
gdjs.introCode.GDsp_9595bouton_9595quantiteObjects1= [];
gdjs.introCode.GDsp_9595bouton_9595quantiteObjects2= [];
gdjs.introCode.GDsp_9595etiquette_9595etObjects1= [];
gdjs.introCode.GDsp_9595etiquette_9595etObjects2= [];
gdjs.introCode.GDsp_9595ourson_95951Objects1= [];
gdjs.introCode.GDsp_9595ourson_95951Objects2= [];
gdjs.introCode.GDsp_9595decompositionObjects1= [];
gdjs.introCode.GDsp_9595decompositionObjects2= [];
gdjs.introCode.GDGreenFlatBarObjects1= [];
gdjs.introCode.GDGreenFlatBarObjects2= [];
gdjs.introCode.GDtsp_9595Objects1= [];
gdjs.introCode.GDtsp_9595Objects2= [];
gdjs.introCode.GDfondObjects1= [];
gdjs.introCode.GDfondObjects2= [];


gdjs.introCode.mapOfGDgdjs_9546introCode_9546GDsp_95959595bouton_95959595validerObjects1Objects = Hashtable.newFrom({"sp_bouton_valider": gdjs.introCode.GDsp_9595bouton_9595validerObjects1});
gdjs.introCode.eventsList0 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("sp_bouton_valider"), gdjs.introCode.GDsp_9595bouton_9595validerObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.introCode.mapOfGDgdjs_9546introCode_9546GDsp_95959595bouton_95959595validerObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "menu", false);
}}

}


};

gdjs.introCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.introCode.GDintro_9595remerciementsObjects1.length = 0;
gdjs.introCode.GDintro_9595remerciementsObjects2.length = 0;
gdjs.introCode.GDsp_9595bouton_9595validerObjects1.length = 0;
gdjs.introCode.GDsp_9595bouton_9595validerObjects2.length = 0;
gdjs.introCode.GDsp_9595bouton_9595retourObjects1.length = 0;
gdjs.introCode.GDsp_9595bouton_9595retourObjects2.length = 0;
gdjs.introCode.GDsp_9595bouton_9595chiffre_9595deObjects1.length = 0;
gdjs.introCode.GDsp_9595bouton_9595chiffre_9595deObjects2.length = 0;
gdjs.introCode.GDtxt_9595representation_9595quantiteObjects1.length = 0;
gdjs.introCode.GDtxt_9595representation_9595quantiteObjects2.length = 0;
gdjs.introCode.GDsp_9595bouton_9595rejouerObjects1.length = 0;
gdjs.introCode.GDsp_9595bouton_9595rejouerObjects2.length = 0;
gdjs.introCode.GDsp_9595scoreObjects1.length = 0;
gdjs.introCode.GDsp_9595scoreObjects2.length = 0;
gdjs.introCode.GDsp_9595faux_9595vraiObjects1.length = 0;
gdjs.introCode.GDsp_9595faux_9595vraiObjects2.length = 0;
gdjs.introCode.GDsp_9595bouton_9595aideObjects1.length = 0;
gdjs.introCode.GDsp_9595bouton_9595aideObjects2.length = 0;
gdjs.introCode.GDsp_9595maisonObjects1.length = 0;
gdjs.introCode.GDsp_9595maisonObjects2.length = 0;
gdjs.introCode.GDsp_9595fond_9595maison_95951Objects1.length = 0;
gdjs.introCode.GDsp_9595fond_9595maison_95951Objects2.length = 0;
gdjs.introCode.GDsp_9595fond_9595maison_95952Objects1.length = 0;
gdjs.introCode.GDsp_9595fond_9595maison_95952Objects2.length = 0;
gdjs.introCode.GDsp_9595bouton_9595quantiteObjects1.length = 0;
gdjs.introCode.GDsp_9595bouton_9595quantiteObjects2.length = 0;
gdjs.introCode.GDsp_9595etiquette_9595etObjects1.length = 0;
gdjs.introCode.GDsp_9595etiquette_9595etObjects2.length = 0;
gdjs.introCode.GDsp_9595ourson_95951Objects1.length = 0;
gdjs.introCode.GDsp_9595ourson_95951Objects2.length = 0;
gdjs.introCode.GDsp_9595decompositionObjects1.length = 0;
gdjs.introCode.GDsp_9595decompositionObjects2.length = 0;
gdjs.introCode.GDGreenFlatBarObjects1.length = 0;
gdjs.introCode.GDGreenFlatBarObjects2.length = 0;
gdjs.introCode.GDtsp_9595Objects1.length = 0;
gdjs.introCode.GDtsp_9595Objects2.length = 0;
gdjs.introCode.GDfondObjects1.length = 0;
gdjs.introCode.GDfondObjects2.length = 0;

gdjs.introCode.eventsList0(runtimeScene);

return;

}

gdjs['introCode'] = gdjs.introCode;
