gdjs.tirage_95jeu_952Code = {};
gdjs.tirage_95jeu_952Code.localVariables = [];
gdjs.tirage_95jeu_952Code.GDsp_9595bouton_9595validerObjects1= [];
gdjs.tirage_95jeu_952Code.GDsp_9595bouton_9595validerObjects2= [];
gdjs.tirage_95jeu_952Code.GDsp_9595bouton_9595validerObjects3= [];
gdjs.tirage_95jeu_952Code.GDsp_9595bouton_9595retourObjects1= [];
gdjs.tirage_95jeu_952Code.GDsp_9595bouton_9595retourObjects2= [];
gdjs.tirage_95jeu_952Code.GDsp_9595bouton_9595retourObjects3= [];
gdjs.tirage_95jeu_952Code.GDsp_9595bouton_9595chiffre_9595deObjects1= [];
gdjs.tirage_95jeu_952Code.GDsp_9595bouton_9595chiffre_9595deObjects2= [];
gdjs.tirage_95jeu_952Code.GDsp_9595bouton_9595chiffre_9595deObjects3= [];
gdjs.tirage_95jeu_952Code.GDtxt_9595representation_9595quantiteObjects1= [];
gdjs.tirage_95jeu_952Code.GDtxt_9595representation_9595quantiteObjects2= [];
gdjs.tirage_95jeu_952Code.GDtxt_9595representation_9595quantiteObjects3= [];
gdjs.tirage_95jeu_952Code.GDsp_9595bouton_9595rejouerObjects1= [];
gdjs.tirage_95jeu_952Code.GDsp_9595bouton_9595rejouerObjects2= [];
gdjs.tirage_95jeu_952Code.GDsp_9595bouton_9595rejouerObjects3= [];
gdjs.tirage_95jeu_952Code.GDsp_9595scoreObjects1= [];
gdjs.tirage_95jeu_952Code.GDsp_9595scoreObjects2= [];
gdjs.tirage_95jeu_952Code.GDsp_9595scoreObjects3= [];
gdjs.tirage_95jeu_952Code.GDsp_9595faux_9595vraiObjects1= [];
gdjs.tirage_95jeu_952Code.GDsp_9595faux_9595vraiObjects2= [];
gdjs.tirage_95jeu_952Code.GDsp_9595faux_9595vraiObjects3= [];
gdjs.tirage_95jeu_952Code.GDsp_9595bouton_9595aideObjects1= [];
gdjs.tirage_95jeu_952Code.GDsp_9595bouton_9595aideObjects2= [];
gdjs.tirage_95jeu_952Code.GDsp_9595bouton_9595aideObjects3= [];
gdjs.tirage_95jeu_952Code.GDsp_9595maisonObjects1= [];
gdjs.tirage_95jeu_952Code.GDsp_9595maisonObjects2= [];
gdjs.tirage_95jeu_952Code.GDsp_9595maisonObjects3= [];
gdjs.tirage_95jeu_952Code.GDsp_9595fond_9595maison_95951Objects1= [];
gdjs.tirage_95jeu_952Code.GDsp_9595fond_9595maison_95951Objects2= [];
gdjs.tirage_95jeu_952Code.GDsp_9595fond_9595maison_95951Objects3= [];
gdjs.tirage_95jeu_952Code.GDsp_9595fond_9595maison_95952Objects1= [];
gdjs.tirage_95jeu_952Code.GDsp_9595fond_9595maison_95952Objects2= [];
gdjs.tirage_95jeu_952Code.GDsp_9595fond_9595maison_95952Objects3= [];
gdjs.tirage_95jeu_952Code.GDsp_9595bouton_9595quantiteObjects1= [];
gdjs.tirage_95jeu_952Code.GDsp_9595bouton_9595quantiteObjects2= [];
gdjs.tirage_95jeu_952Code.GDsp_9595bouton_9595quantiteObjects3= [];
gdjs.tirage_95jeu_952Code.GDsp_9595etiquette_9595etObjects1= [];
gdjs.tirage_95jeu_952Code.GDsp_9595etiquette_9595etObjects2= [];
gdjs.tirage_95jeu_952Code.GDsp_9595etiquette_9595etObjects3= [];
gdjs.tirage_95jeu_952Code.GDsp_9595ourson_95951Objects1= [];
gdjs.tirage_95jeu_952Code.GDsp_9595ourson_95951Objects2= [];
gdjs.tirage_95jeu_952Code.GDsp_9595ourson_95951Objects3= [];
gdjs.tirage_95jeu_952Code.GDsp_9595decompositionObjects1= [];
gdjs.tirage_95jeu_952Code.GDsp_9595decompositionObjects2= [];
gdjs.tirage_95jeu_952Code.GDsp_9595decompositionObjects3= [];
gdjs.tirage_95jeu_952Code.GDGreenFlatBarObjects1= [];
gdjs.tirage_95jeu_952Code.GDGreenFlatBarObjects2= [];
gdjs.tirage_95jeu_952Code.GDGreenFlatBarObjects3= [];
gdjs.tirage_95jeu_952Code.GDtsp_9595Objects1= [];
gdjs.tirage_95jeu_952Code.GDtsp_9595Objects2= [];
gdjs.tirage_95jeu_952Code.GDtsp_9595Objects3= [];
gdjs.tirage_95jeu_952Code.GDfondObjects1= [];
gdjs.tirage_95jeu_952Code.GDfondObjects2= [];
gdjs.tirage_95jeu_952Code.GDfondObjects3= [];


gdjs.tirage_95jeu_952Code.eventsList0 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
{gdjs.evtTools.variable.variableClearChildren(runtimeScene.getGame().getVariables().getFromIndex(3).getChild("a"));
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4).getChild("nb_mini")) == 4;
if (isConditionTrue_0) {
{runtimeScene.getScene().getVariables().getFromIndex(1).setNumber(4);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4).getChild("nb_mini")) == 5;
if (isConditionTrue_0) {
{runtimeScene.getScene().getVariables().getFromIndex(1).setNumber(9);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4).getChild("nb_mini")) == 6;
if (isConditionTrue_0) {
{runtimeScene.getScene().getVariables().getFromIndex(1).setNumber(15);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4).getChild("nb_mini")) == 7;
if (isConditionTrue_0) {
{runtimeScene.getScene().getVariables().getFromIndex(1).setNumber(22);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4).getChild("nb_mini")) == 8;
if (isConditionTrue_0) {
{runtimeScene.getScene().getVariables().getFromIndex(1).setNumber(30);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4).getChild("nb_mini")) == 9;
if (isConditionTrue_0) {
{runtimeScene.getScene().getVariables().getFromIndex(1).setNumber(39);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4).getChild("nb_mini")) == 10;
if (isConditionTrue_0) {
{runtimeScene.getScene().getVariables().getFromIndex(1).setNumber(49);
}}

}


{


let isConditionTrue_0 = false;
{
{runtimeScene.getScene().getVariables().getFromIndex(0).setNumber(1);
}}

}


};gdjs.tirage_95jeu_952Code.eventsList1 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4).getChild("nb_maxi")) == 3;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 4;
}
if (isConditionTrue_0) {
{runtimeScene.getScene().getVariables().getFromIndex(0).setNumber(2);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4).getChild("nb_maxi")) == 4;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 9;
}
if (isConditionTrue_0) {
{runtimeScene.getScene().getVariables().getFromIndex(0).setNumber(2);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4).getChild("nb_maxi")) == 5;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 15;
}
if (isConditionTrue_0) {
{runtimeScene.getScene().getVariables().getFromIndex(0).setNumber(2);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4).getChild("nb_maxi")) == 6;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 22;
}
if (isConditionTrue_0) {
{runtimeScene.getScene().getVariables().getFromIndex(0).setNumber(2);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4).getChild("nb_maxi")) == 7;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 30;
}
if (isConditionTrue_0) {
{runtimeScene.getScene().getVariables().getFromIndex(0).setNumber(2);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4).getChild("nb_maxi")) == 8;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 30;
}
if (isConditionTrue_0) {
{runtimeScene.getScene().getVariables().getFromIndex(0).setNumber(2);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4).getChild("nb_maxi")) == 9;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 39;
}
if (isConditionTrue_0) {
{runtimeScene.getScene().getVariables().getFromIndex(0).setNumber(2);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4).getChild("nb_maxi")) == 10;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 60;
}
if (isConditionTrue_0) {
{runtimeScene.getScene().getVariables().getFromIndex(0).setNumber(2);
}}

}


};gdjs.tirage_95jeu_952Code.eventsList2 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 1;
if (isConditionTrue_0) {
{gdjs.evtTools.variable.valuePush(runtimeScene.getGame().getVariables().getFromIndex(3).getChild("a"), runtimeScene.getScene().getVariables().getFromIndex(1).getAsNumber());
}{runtimeScene.getScene().getVariables().getFromIndex(1).add(1);
}
{ //Subevents
gdjs.tirage_95jeu_952Code.eventsList1(runtimeScene);} //End of subevents
}

}


};gdjs.tirage_95jeu_952Code.eventsList3 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 2;
if (isConditionTrue_0) {
{gdjs.evtsExt__ArrayTools__GlobalShuffle.func(runtimeScene, runtimeScene.getGame().getVariables().getFromIndex(3).getChild("a"), (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}{runtimeScene.getScene().getVariables().getFromIndex(0).setNumber(3);
}{runtimeScene.getGame().getVariables().getFromIndex(3).getChild("nb_parties_maxi").setNumber(gdjs.evtTools.variable.getVariableChildCount(runtimeScene.getGame().getVariables().getFromIndex(3).getChild("a")));
}}

}


};gdjs.tirage_95jeu_952Code.eventsList4 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(6).setNumber(0);
}
{ //Subevents
gdjs.tirage_95jeu_952Code.eventsList0(runtimeScene);} //End of subevents
}

}


{


gdjs.tirage_95jeu_952Code.eventsList2(runtimeScene);
}


{


gdjs.tirage_95jeu_952Code.eventsList3(runtimeScene);
}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 3;
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "jeu_2", false);
}}

}


};

gdjs.tirage_95jeu_952Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.tirage_95jeu_952Code.GDsp_9595bouton_9595validerObjects1.length = 0;
gdjs.tirage_95jeu_952Code.GDsp_9595bouton_9595validerObjects2.length = 0;
gdjs.tirage_95jeu_952Code.GDsp_9595bouton_9595validerObjects3.length = 0;
gdjs.tirage_95jeu_952Code.GDsp_9595bouton_9595retourObjects1.length = 0;
gdjs.tirage_95jeu_952Code.GDsp_9595bouton_9595retourObjects2.length = 0;
gdjs.tirage_95jeu_952Code.GDsp_9595bouton_9595retourObjects3.length = 0;
gdjs.tirage_95jeu_952Code.GDsp_9595bouton_9595chiffre_9595deObjects1.length = 0;
gdjs.tirage_95jeu_952Code.GDsp_9595bouton_9595chiffre_9595deObjects2.length = 0;
gdjs.tirage_95jeu_952Code.GDsp_9595bouton_9595chiffre_9595deObjects3.length = 0;
gdjs.tirage_95jeu_952Code.GDtxt_9595representation_9595quantiteObjects1.length = 0;
gdjs.tirage_95jeu_952Code.GDtxt_9595representation_9595quantiteObjects2.length = 0;
gdjs.tirage_95jeu_952Code.GDtxt_9595representation_9595quantiteObjects3.length = 0;
gdjs.tirage_95jeu_952Code.GDsp_9595bouton_9595rejouerObjects1.length = 0;
gdjs.tirage_95jeu_952Code.GDsp_9595bouton_9595rejouerObjects2.length = 0;
gdjs.tirage_95jeu_952Code.GDsp_9595bouton_9595rejouerObjects3.length = 0;
gdjs.tirage_95jeu_952Code.GDsp_9595scoreObjects1.length = 0;
gdjs.tirage_95jeu_952Code.GDsp_9595scoreObjects2.length = 0;
gdjs.tirage_95jeu_952Code.GDsp_9595scoreObjects3.length = 0;
gdjs.tirage_95jeu_952Code.GDsp_9595faux_9595vraiObjects1.length = 0;
gdjs.tirage_95jeu_952Code.GDsp_9595faux_9595vraiObjects2.length = 0;
gdjs.tirage_95jeu_952Code.GDsp_9595faux_9595vraiObjects3.length = 0;
gdjs.tirage_95jeu_952Code.GDsp_9595bouton_9595aideObjects1.length = 0;
gdjs.tirage_95jeu_952Code.GDsp_9595bouton_9595aideObjects2.length = 0;
gdjs.tirage_95jeu_952Code.GDsp_9595bouton_9595aideObjects3.length = 0;
gdjs.tirage_95jeu_952Code.GDsp_9595maisonObjects1.length = 0;
gdjs.tirage_95jeu_952Code.GDsp_9595maisonObjects2.length = 0;
gdjs.tirage_95jeu_952Code.GDsp_9595maisonObjects3.length = 0;
gdjs.tirage_95jeu_952Code.GDsp_9595fond_9595maison_95951Objects1.length = 0;
gdjs.tirage_95jeu_952Code.GDsp_9595fond_9595maison_95951Objects2.length = 0;
gdjs.tirage_95jeu_952Code.GDsp_9595fond_9595maison_95951Objects3.length = 0;
gdjs.tirage_95jeu_952Code.GDsp_9595fond_9595maison_95952Objects1.length = 0;
gdjs.tirage_95jeu_952Code.GDsp_9595fond_9595maison_95952Objects2.length = 0;
gdjs.tirage_95jeu_952Code.GDsp_9595fond_9595maison_95952Objects3.length = 0;
gdjs.tirage_95jeu_952Code.GDsp_9595bouton_9595quantiteObjects1.length = 0;
gdjs.tirage_95jeu_952Code.GDsp_9595bouton_9595quantiteObjects2.length = 0;
gdjs.tirage_95jeu_952Code.GDsp_9595bouton_9595quantiteObjects3.length = 0;
gdjs.tirage_95jeu_952Code.GDsp_9595etiquette_9595etObjects1.length = 0;
gdjs.tirage_95jeu_952Code.GDsp_9595etiquette_9595etObjects2.length = 0;
gdjs.tirage_95jeu_952Code.GDsp_9595etiquette_9595etObjects3.length = 0;
gdjs.tirage_95jeu_952Code.GDsp_9595ourson_95951Objects1.length = 0;
gdjs.tirage_95jeu_952Code.GDsp_9595ourson_95951Objects2.length = 0;
gdjs.tirage_95jeu_952Code.GDsp_9595ourson_95951Objects3.length = 0;
gdjs.tirage_95jeu_952Code.GDsp_9595decompositionObjects1.length = 0;
gdjs.tirage_95jeu_952Code.GDsp_9595decompositionObjects2.length = 0;
gdjs.tirage_95jeu_952Code.GDsp_9595decompositionObjects3.length = 0;
gdjs.tirage_95jeu_952Code.GDGreenFlatBarObjects1.length = 0;
gdjs.tirage_95jeu_952Code.GDGreenFlatBarObjects2.length = 0;
gdjs.tirage_95jeu_952Code.GDGreenFlatBarObjects3.length = 0;
gdjs.tirage_95jeu_952Code.GDtsp_9595Objects1.length = 0;
gdjs.tirage_95jeu_952Code.GDtsp_9595Objects2.length = 0;
gdjs.tirage_95jeu_952Code.GDtsp_9595Objects3.length = 0;
gdjs.tirage_95jeu_952Code.GDfondObjects1.length = 0;
gdjs.tirage_95jeu_952Code.GDfondObjects2.length = 0;
gdjs.tirage_95jeu_952Code.GDfondObjects3.length = 0;

gdjs.tirage_95jeu_952Code.eventsList4(runtimeScene);

return;

}

gdjs['tirage_95jeu_952Code'] = gdjs.tirage_95jeu_952Code;
