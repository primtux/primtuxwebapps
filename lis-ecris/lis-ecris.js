"use strict"

const messages = {
  accueil: "<div class='consigne'>Pour commencer, tu dois choisir un exercice parmi les thèmes proposés.</div>",
  erreur: "Erreur !",
  finExo: "Tu as terminé l'exercice.<div class='consigne'>Tu peux choisir un autre exercice parmi les thèmes proposés.</div>",
  bravo: "Bravo !<div class='consigne'>Clique sur [suivant] ou appuie sur Entrée pour poursuivre.</div>",
  felicitations: "Félicitations, tu as réussi tout l'exercice !<div class='consigne'>Tu peux choisir un autre exercice parmi les thèmes proposés.</div>",
  passer: "Échec !<div class='consigne'>Clique sur [suivant] ou appuie sur Entrée pour poursuivre.</div>",
  passerExo: "Échec ! L'exercice est terminé.<div class='consigne'>Tu peux choisir un autre exercice parmi les thèmes proposés.</div>",
};

const options = {
  tempsAffichage: 2000, // Temps d'affichage du mot
  fonte:"script", // Fonte par défaut, "script" ou "cursive"
  nbMotsBase: 5, // Nombre de mots à proposer par exercice
  nbClicsRevoir: 2 // Nombre de clics autorisés sur le bouton [Revoir]
};

const paramMots = {
  listeMots: [],
  ctMots: 0,
  nbMotsExo: options.nbMotsBase,
  ctClics: 0
}

// Variables nécessaires au calcul des résultats
const paramResultats = {
  ctTentatives: 0,
  ctTotalTentatives: 0,
  ctExercices: 0,
  ctEpreuves: 0,
  ctTotalEpreuves: 0,
  nbReussis: 0,
  nbReussisTotal: 0,
  ctAbandonsEpreuve: 0,
  ctTotalAbandons: 0,
  listeExercices: []
}

// Affichage de la zone d'exercice
const motReference = document.getElementById("modele");
const annonce = document.getElementById("annonces");
const motSaisi = document.getElementById("reponse");

// Affichage des résultats
const nbEpreuves = document.getElementById("numero-epreuve");
const nbEpreuvesReussies = document.getElementById("nbepreuvesreussies");
const nbTotalEpreuves = document.getElementById("nbtotalepreuves");
const tauxEpreuves = document.getElementById("tauxepreuves");
const nbEchecs = document.getElementById("nbechecs");
const nbExercices = document.getElementById("nbexercices");
const nbTotalEchecs = document.getElementById("nbtotalechecs");
const nomEleve =  document.getElementById("patronyme");

function alea(min,max) {
  let nb = min + (max-min+1) * Math.random();
  return Math.floor(nb);
}

function melange(tableau) {
  let tab = tableau.slice();
  let tirage;
  let tab_alea = [];

  for (var i = 0; i < tab.length; i++) {
    do {
      tirage = alea(0, tab.length -1);
    } while (tab[tirage] === 0);
    tab_alea[i] = tab[tirage];
    tab[tirage] = 0;
  }
  return tab_alea;
}

function rogneChaine(chaine) {
  let s1, s2;
  s1 = chaine.replace(/^[ ]*/,'');
  s2 = s1.replace(/[ ]*$/,'');
  return s2;
}

function initialiseCompteurs() {
  paramMots.ctMots = 0;
  paramResultats.ctTentatives = 0;
  paramResultats.ctEpreuves = 0;
  paramResultats.nbReussis = 0;
  paramResultats.ctAbandonsEpreuve = 0;
  paramMots.nbMotsExo = options.nbMotsBase;
}

function tireMots(tableau) {
  let tirage;
  let tabAlea = [];
  if (( sessionStorage.getItem("nb-mots")) && (sessionStorage.getItem("nb-mots") !== 'undefined')) {
    options.nbMotsBase = sessionStorage.getItem("nb-mots");
    paramMots.nbMotsExo = options.nbMotsBase;
  }
  if ( tableau.length < options.nbMotsBase) {
    paramMots.nbMotsExo = tableau.length;
    paramMots.listeMots = melange(tableau);
    return;
  }
  for(var i=0; i < options.nbMotsBase ; i++) {
    do {
      tirage = alea(0, tableau.length -1);
    }while (tabAlea.indexOf(tirage) >= 0);
    tabAlea[i] = tirage;
    paramMots.listeMots[i] = tableau[tirage];
  }
}

function creeListesSelection() {
  let i;
  const sonsC2 = document.getElementById("liste-sonsDBC2");
  const sonsC3 = document.getElementById("liste-sonsDBC3");
  const echelons = document.getElementById("liste-echelons");
  const  echelons2 = document.getElementById("liste-echelons2");
  const categories = document.getElementById("liste-categories");
  for(i=0; i < listeSons.length ; i++) {
    sonsC2.length++;
    sonsC2.options[sonsC2.length-1].text = listeSons[i];
  }
  for(i=0; i < listeSons.length ; i++) {
    sonsC3.length++;
    sonsC3.options[sonsC3.length-1].text = listeSons[i];
  }
  for(i=0; i < listeEchelons.length ; i++) {
    echelons.length++;
    echelons.options[echelons.length-1].text = listeEchelons[i];
  }
  for(i=0; i < listeEchelons2.length ; i++) {
    echelons2.length++;
    echelons2.options[echelons2.length-1].text = listeEchelons2[i];
  }
  for(i=0; i < listeCategories.length ; i++) {
    categories.length++;
    categories.options[categories.length-1].text = listeCategories[i];
  }
}

function creeExercice(typeExercice) {
  const liste = document.getElementById(typeExercice);
  let choix = liste.selectedIndex -1;
  liste.options[liste.selectedIndex].selected = false;
  initialiseCompteurs();
  switch(typeExercice) {
    case 'liste-sonsDBC2':
      tireMots(motsSonsDBC2[choix]);
      paramResultats.listeExercices.push(listeSons[choix + 1] + " cycle 2");
      break;
    case 'liste-sonsDBC3':
      tireMots(motsSonsDBC3[choix]);
      paramResultats.listeExercices.push(listeSons[choix + 1] + " cycle 3");
      break;
    case 'liste-echelons':
      tireMots(motsEchelons[choix]);
      paramResultats.listeExercices.push(listeEchelons[choix + 1]);
      break;
    case 'liste-echelons2':
      tireMots(motsEchelons2[choix]);
      paramResultats.listeExercices.push(listeEchelons2[choix + 1]);
      break;
    case 'liste-categories':
      tireMots(motsCategories[choix]);
      paramResultats.listeExercices.push(listeCategories[choix + 1]);
      break;
  }
  afficheMot();
  annonce.innerHTML = '';
  document.getElementById("lbsaisie").className="lb-actif";
  effaceResultatsExercice();
}

function chargeFonte() {
  let i;
  const lettres = document.getElementsByClassName("lettre");
  if (( sessionStorage.getItem("fonte") ) && (sessionStorage.getItem("fonte") !== 'undefined')) {
    options.fonte = sessionStorage.getItem("fonte");
  }
  if ( options.fonte == "cursive" ) {
    for( i = 0; i < lettres.length; i++) {
      lettres[i].classList.add("cursive");
    }
  }
  else {
    for(i = 0; i < lettres.length; i++) {
      lettres[i].classList.remove("cursive");
    }
  }
}

function lanceOptions() {
  document.getElementById("ecran-blanc").className = "visible";
  document.getElementById("options").className = "visible";
}

function annuleOptions() {
  document.getElementById("ecran-blanc").className = "invisible";
  document.getElementById("options").className = "invisible";
}

function valideOptions() {
  let i;
  const choixFonte = document.getElementsByName('fonte');
  let valeurFonte = "script";
  let valeurTemps = 2;
  // Récupère et stocke la valeur choisie pour le temps d'affichage
  valeurTemps = document.forms["form-options"].elements["temps"].value;
  valeurTemps = valeurTemps * 1000;
  sessionStorage.setItem("temps",valeurTemps);
  //  Récupère et stocke la valeur choisie pour le type de police
  for(i = 0; i < choixFonte.length; i++){
    if(choixFonte[i].checked){
      valeurFonte = choixFonte[i].value;
    }
  }
  sessionStorage.setItem("fonte",valeurFonte);
  // Récupère et stocke la valeur choisie pour le nombre de mots par exercice
  sessionStorage.setItem("nb-mots",document.forms["form-options"].elements["nbmots"].value);
  // Récupère et stocke le nombre de clics possibles sur le bouton [revoir]
   sessionStorage.setItem("revoir",document.getElementById('reaffichage').value);
}

function cacheMot() {
  let cache ="";
  motReference.classList.remove("cursive");
  motReference.textContent = cache;
  motSaisi.disabled = false;
  motSaisi.focus();
}

function afficheMot() {
  let ct = paramMots.ctMots + 1;
  paramMots.ctClics = 0;
  document.getElementById("btrevoir").disabled = false;
  if (( sessionStorage.getItem("revoir") ) && (sessionStorage.getItem("revoir") !== 'undefined')) {
    options.nbClicsRevoir = sessionStorage.getItem("revoir");
  }
  if ( options.fonte =="cursive" ) {
    motReference.classList.add("cursive");
  }
  motSaisi.value = "";
  motSaisi.focus();
  document.getElementById("btverifier").disabled = false;
  if (( sessionStorage.getItem("temps") ) && (sessionStorage.getItem("temps") !== 'undefined')) {
    options.tempsAffichage = sessionStorage.getItem("temps");
  }
  motReference.textContent = paramMots.listeMots[paramMots.ctMots];
  setTimeout(cacheMot, options.tempsAffichage);
  document.getElementById("btsuivant").disabled = true;
  nbEpreuves.innerHTML = ct + "/" + paramMots.nbMotsExo;
}

function revoirMot() {
  motSaisi.disabled = true;
  if ( options.fonte =="cursive" ) {
    motReference.classList.add("cursive");
  }
  paramMots.ctClics++;
  if ( paramMots.ctClics == options.nbClicsRevoir ) {
    document.getElementById("btrevoir").disabled = true;
    document.getElementById("btpasser").disabled = false;
  }
  motSaisi.focus();
  motReference.textContent = paramMots.listeMots[paramMots.ctMots];
  setTimeout(cacheMot, options.tempsAffichage);
}

function afficheResultats() {
  let taux1 = 0;
  let taux2 = 0;
  if ( paramResultats.ctTentatives > 0 ) {
    taux1 = Math.round((paramResultats.nbReussis * 100) / paramResultats.ctTentatives);
  }
  if ( paramResultats.ctTotalTentatives > 0 ) {
    taux2 = Math.round((paramResultats.nbReussisTotal * 100) / paramResultats.ctTotalTentatives);
  }
  nbEpreuvesReussies.innerHTML = paramResultats.ctTentatives;
  if ( taux1 ) { tauxEpreuves.innerHTML = taux1 + "%"; }
  nbEchecs.innerHTML = paramResultats.ctAbandonsEpreuve;
  nbExercices.innerHTML = paramResultats.ctExercices;
  nbTotalEpreuves.innerHTML = paramResultats.ctTotalEpreuves;
  if ( taux2 ) { document.getElementById("tauxexos").innerHTML = taux2 + "%"; }
  nbTotalEchecs.innerHTML = paramResultats.ctTotalAbandons;
}

function effaceResultatsExercice() {
  nbEpreuves.innerHTML = "";
  nbEpreuvesReussies.innerHTML = "";
  tauxEpreuves.innerHTML = "";
  nbEchecs.innerHTML = "";
}

function prepareNouvelExercice() {
  document.getElementById("btrevoir").disabled = true;
  document.getElementById("btverifier").disabled = true;
  paramResultats.ctExercices++;
  paramResultats.ctTentatives = 0;
  afficheResultats();
  effaceResultatsExercice();
}

function verifier() {
  const reponse = document.getElementById('reponse');
  let proposition = rogneChaine(reponse.value);
  paramResultats.ctTentatives++;
  paramResultats.ctTotalTentatives++;
  if ( proposition != paramMots.listeMots[paramMots.ctMots] ) {
    annonce.innerHTML = messages.erreur;
    afficheResultats();
    return;
  }
  paramResultats.nbReussis++;
  paramResultats.nbReussisTotal++;
  paramResultats.ctEpreuves++;
  paramResultats.ctTotalEpreuves++;
  motSaisi.disabled = true;
  document.getElementById("btsuivant").disabled = true;
  // desactiveBtPasser();
  document.getElementById("btpasser").disabled = true;
  if ( paramMots.ctMots == paramMots.nbMotsExo - 1 ) {
    if (paramResultats.ctAbandonsEpreuve == 0) {
      annonce.innerHTML = messages.felicitations;
    }
    else {
      annonce.innerHTML = messages.finExo;
    }
    prepareNouvelExercice();
  }
  else {
    annonce.innerHTML = messages.bravo;
    if ( options.fonte =="cursive" ) {
      motReference.classList.add("cursive");
    }
    motReference.textContent = paramMots.listeMots[paramMots.ctMots];
    afficheResultats();
    document.getElementById("btrevoir").disabled = true;
    document.getElementById("btverifier").disabled = true;
    document.getElementById("btsuivant").disabled = false;
    document.getElementById("btsuivant").focus();
  }
}

function motSuivant() {
  const reponse = document.getElementById('reponse');
  paramMots.ctMots++;
  annonce.innerHTML = '';
  afficheMot();
  reponse.value = "";
  // desactiveBtPasser();
  document.getElementById("btpasser").disabled = true;
}

function passeMot() {
  paramResultats.ctTentatives++;
  paramResultats.ctTotalTentatives++;
  paramResultats.ctAbandonsEpreuve++;
  paramResultats.ctTotalAbandons++;
  paramResultats.ctEpreuves++;
  paramResultats.ctTotalEpreuves++;
  motReference.textContent = paramMots.listeMots[paramMots.ctMots];
  afficheResultats();
  document.getElementById("btverifier").disabled = true;
  if ( paramMots.ctMots + 1 == paramMots.nbMotsExo ) {
    paramMots.ctMots++;
    annonce.innerHTML = messages.passerExo;
    document.getElementById("btsuivant").disabled = true;
    prepareNouvelExercice();
  }
  else {
    annonce.innerHTML = messages.passer;
    document.getElementById("btsuivant").disabled = false;
    document.getElementById("btsuivant").focus();
  }
  document.getElementById("btpasser").disabled = true;
}

function demandeNom() {
  document.getElementById("ecran-blanc").className = "visible";
  document.getElementById("saisie-nom").className = "visible";
  document.getElementById("patronyme").focus();
}

function pageImpression() {
  let taux;
  if ( paramResultats.ctTotalTentatives > 0 ) {
    taux = Math.round((paramResultats.nbReussisTotal * 100) / paramResultats.ctTotalTentatives);
  }
  sessionStorage.setItem("nom",nomEleve.value);
  sessionStorage.setItem("exos",paramResultats.ctExercices);
  sessionStorage.setItem("liste-exos",paramResultats.listeExercices);
  sessionStorage.setItem("epreuves",paramResultats.ctTotalEpreuves);
  sessionStorage.setItem("taux",taux);
  sessionStorage.setItem("echecs",paramResultats.ctTotalAbandons);
  window.open('impression.html');
}

function annuleImpression() {
  document.getElementById("ecran-blanc").className = "invisible";
  document.getElementById("saisie-nom").className = "invisible";
}

function soumetReponse(ev) {
  ev.preventDefault();
  verifier();
}

function initialise() {
  creeListesSelection();
  chargeFonte();
  annonce.innerHTML = messages.accueil;
  nbEpreuves.innerHTML = "";
  nbEpreuvesReussies.innerHTML = "";
  tauxEpreuves.innerHTML = "";
}

window.onload = initialise();
