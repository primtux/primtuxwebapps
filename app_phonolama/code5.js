gdjs.infosCode = {};
gdjs.infosCode.GDexplicationObjects1= [];
gdjs.infosCode.GDexplicationObjects2= [];
gdjs.infosCode.GDscore2Objects1= [];
gdjs.infosCode.GDscore2Objects2= [];
gdjs.infosCode.GDscore1Objects1= [];
gdjs.infosCode.GDscore1Objects2= [];
gdjs.infosCode.GDbouton_9595jeu1Objects1= [];
gdjs.infosCode.GDbouton_9595jeu1Objects2= [];
gdjs.infosCode.GDlamaObjects1= [];
gdjs.infosCode.GDlamaObjects2= [];
gdjs.infosCode.GDbouton_9595jeu2Objects1= [];
gdjs.infosCode.GDbouton_9595jeu2Objects2= [];
gdjs.infosCode.GDfond_9595menuObjects1= [];
gdjs.infosCode.GDfond_9595menuObjects2= [];
gdjs.infosCode.GDbouton_9595retourObjects1= [];
gdjs.infosCode.GDbouton_9595retourObjects2= [];


gdjs.infosCode.mapOfGDgdjs_9546infosCode_9546GDbouton_95959595retourObjects1Objects = Hashtable.newFrom({"bouton_retour": gdjs.infosCode.GDbouton_9595retourObjects1});
gdjs.infosCode.eventsList0 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("bouton_retour"), gdjs.infosCode.GDbouton_9595retourObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.infosCode.mapOfGDgdjs_9546infosCode_9546GDbouton_95959595retourObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "menu", false);
}}

}


};

gdjs.infosCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.infosCode.GDexplicationObjects1.length = 0;
gdjs.infosCode.GDexplicationObjects2.length = 0;
gdjs.infosCode.GDscore2Objects1.length = 0;
gdjs.infosCode.GDscore2Objects2.length = 0;
gdjs.infosCode.GDscore1Objects1.length = 0;
gdjs.infosCode.GDscore1Objects2.length = 0;
gdjs.infosCode.GDbouton_9595jeu1Objects1.length = 0;
gdjs.infosCode.GDbouton_9595jeu1Objects2.length = 0;
gdjs.infosCode.GDlamaObjects1.length = 0;
gdjs.infosCode.GDlamaObjects2.length = 0;
gdjs.infosCode.GDbouton_9595jeu2Objects1.length = 0;
gdjs.infosCode.GDbouton_9595jeu2Objects2.length = 0;
gdjs.infosCode.GDfond_9595menuObjects1.length = 0;
gdjs.infosCode.GDfond_9595menuObjects2.length = 0;
gdjs.infosCode.GDbouton_9595retourObjects1.length = 0;
gdjs.infosCode.GDbouton_9595retourObjects2.length = 0;

gdjs.infosCode.eventsList0(runtimeScene);

return;

}

gdjs['infosCode'] = gdjs.infosCode;
