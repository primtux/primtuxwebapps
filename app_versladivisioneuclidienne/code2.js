gdjs.tirageCode = {};
gdjs.tirageCode.GDavatar_9595ver1Objects1= [];
gdjs.tirageCode.GDavatar_9595ver1Objects2= [];
gdjs.tirageCode.GDavatar_9595ver2Objects1= [];
gdjs.tirageCode.GDavatar_9595ver2Objects2= [];
gdjs.tirageCode.GDbouton_9595retourObjects1= [];
gdjs.tirageCode.GDbouton_9595retourObjects2= [];


gdjs.tirageCode.eventsList0 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
{gdjs.evtsExt__ArrayTools__GlobalShuffle.func(runtimeScene, runtimeScene.getGame().getVariables().getFromIndex(3), (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}{runtimeScene.getGame().getVariables().getFromIndex(5).setNumber(0);
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "jeu", false);
}}

}


};

gdjs.tirageCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.tirageCode.GDavatar_9595ver1Objects1.length = 0;
gdjs.tirageCode.GDavatar_9595ver1Objects2.length = 0;
gdjs.tirageCode.GDavatar_9595ver2Objects1.length = 0;
gdjs.tirageCode.GDavatar_9595ver2Objects2.length = 0;
gdjs.tirageCode.GDbouton_9595retourObjects1.length = 0;
gdjs.tirageCode.GDbouton_9595retourObjects2.length = 0;

gdjs.tirageCode.eventsList0(runtimeScene);

return;

}

gdjs['tirageCode'] = gdjs.tirageCode;
