"use strict"

const paramListes = {
  numeroListe: 0,
  nbListes: 0,
  listesFaites: [],
  niveau: -1
}

const paramPositionnement = {
  dernierSurvol: {},
  ctEspaces: 0,
  espaceWidth: "1.07%",
  coefEspaceWidth: 0.01,
  image: "url(img/attache3.png)"
}

var boutons = [];
var solution = [];

const receptacle = document.getElementById("receptacle");
const btVerif = document.getElementById("verifier");

function alea(min,max) {
  let nb = min + (max-min+1) * Math.random();
  return Math.floor(nb);
}

function melange(tableau) {
  let i;
  let tirage;
  let tab_alea = [];
  for (i = 0; i < tableau.length; i++) {
    do {
      tirage = alea(0, tableau.length -1);
    } while (tableau[tirage] === 0);
    tab_alea[i] = tableau[tirage];
    tableau[tirage] = 0;
  }
  return tab_alea;
}

function creeBtListes() {
  const commandes = document.getElementById("commandes");
  let numero;
  let i;
  document.getElementById("niveau").disabled = true;
  paramListes.nbListes = items[paramListes.niveau].length;
  for (i = 0; i < paramListes.nbListes; i++) {
    const newBt = document.createElement("button");
    newBt.id = "liste" + i;
    newBt.type = "button";
    newBt.className = "btcommande";
    numero = i + 1;
    const t = document.createTextNode("Série " + numero);
    newBt.appendChild(t);
    newBt.onclick = defListe;
    boutons[i] = newBt;
    commandes.insertBefore(boutons[i], btVerif);
  }
}

function videBtListes() {
  let i;
  const elementParent = document.getElementById("commandes");
  for (i = 0; i < paramListes.nbListes; i++) {
    elementParent.removeChild(boutons[i]);
  }
}

function nouvelEspace(idItem) {
  const newEspace = document.createElement("div");
  newEspace.id = "espace-"+idItem;
  paramPositionnement.ctEspaces += 1;
  newEspace.className = "espace";
  newEspace.addEventListener("dragover", auSurvol);
  newEspace.addEventListener("dragleave", finSurvol);
  return newEspace;
}

// Pour savoir à quel élément est lié l'espace survolé
function getIdItemEspace(idObjet) {
  let idEspace = "";
  let tableau = idObjet.split("-");
  idEspace= tableau[1];
  return idEspace;
}

function creeListeItems() {
  let i;
  const reserve = document.getElementById("reserve");
  let listeItems = items[paramListes.niveau][paramListes.numeroListe - 1];
  solution = items[paramListes.niveau][paramListes.numeroListe - 1].join();
  listeItems = melange(listeItems);
  for (i = 0; i < listeItems.length; i++) {
    const newDiv = document.createElement("div");
    newDiv.id = "item" + i;
    newDiv.className = "draggable";
    newDiv.setAttribute('draggable', 'true');
    newDiv.addEventListener("dragstart", drag);
    const newDivTexte = document.createElement("div");
    newDivTexte.className = "div-texte";
    const newTexte = document.createTextNode(listeItems[i]);
    newDivTexte.appendChild(newTexte);
    newDiv.appendChild(newDivTexte);
    reserve.appendChild(newDiv);
  }
}

function desactiveBtListe() {
  let i;
  for (i=0; i<paramListes.nbListes; i++) {
    boutons[i].disabled = true;
  }
}

function activeBtListe() {
  let i;
  for (i=0; i<paramListes.nbListes; i++) {
    if (paramListes.listesFaites.indexOf(i+1) < 0) {
      boutons[i].disabled = false;
    }
  }
}

function defListe() {
  paramListes.numeroListe = parseInt(this.innerText.substring(5));
  desactiveBtListe();
  commencer();
}

function getPremiereDifference(listeReference,listeComparee) {
  let i = 0;
  let nbMots = 1;
  let difference = false;
  while ((i < listeReference.length) && (! difference)) {
    if (listeReference[i] === ",") {
      nbMots += 1;
    }
    if (listeReference[i] !== listeComparee[i]) {
      difference = true;
    }
    i += 1;
  }
  if (difference) {return nbMots}
  else {return 0}
}

function getIdObjet(infosObjet) {
  let tableau = infosObjet.split("-");
  return tableau[0];
}

function getIdParent(infosObjet) {
  let tableau = infosObjet.split("-");
  return tableau[1];
}

function  reinitialise() {
  let i;
  activeBtListe();
  while (receptacle.firstChild) {
    receptacle.removeChild(receptacle.firstChild);
  }
  receptacle.innerHTML = "<div id='locomotive'></div>";
  paramListes.numeroListe = 0;
}

function choixNiveau() {
  document.getElementById("choix-niveau").className = "visible";
}

function lanceNiveau(ev) {
  let i;
  videBtListes();
  const listeNiveaux = document.getElementsByName('niveau');
  for(i = 0; i < listeNiveaux.length; i++){
    if (listeNiveaux[i].checked) {
      paramListes.niveau = parseInt(listeNiveaux[i].value);
    }
  }
  creeBtListes();
  document.getElementById("choix-niveau").className = "invisible";
  ev.preventDefault();
}

function commencer() {
  creeListeItems();
  document.getElementById("verifier").disabled = false;
}

function verification() {
  let i;
  let proposition = "";
  let ordreErreur = 0;
  for (i = 0; i < receptacle.childNodes.length; i++) {
    if (i + 1 < receptacle.childNodes.length) {
      if (receptacle.childNodes[i].className === "draggable") {proposition = proposition + receptacle.childNodes[i].childNodes[0].innerHTML + ",";}
      }
      else {
        if (receptacle.childNodes[i].className === "draggable") {proposition = proposition + receptacle.childNodes[i].childNodes[0].innerHTML;}
      }
  }
  if (proposition !== solution) {
    ordreErreur = getPremiereDifference(solution,proposition);
    showDialog('<p>Malheureusement, il y a des erreurs à partir du wagon ' + ordreErreur + ' ! </p><p>Tu peux redéplacer les wagons vers la gauche ou vers la droite pour corriger.</p>');
    return;
  }
  paramListes.listesFaites.push(parseInt(paramListes.numeroListe));
  document.getElementById("verifier").disabled = true;
  document.getElementById("niveau").disabled = false;
  reinitialise();
  if (paramListes.listesFaites.length < paramListes.nbListes) {showDialog('Bravo !',0.5,'img/happy-tux.png', 89, 91, 'left')}
    else {
      showDialog("Félicitations !<p> Tu as réussi à classer correctement les " + paramListes.nbListes + " listes.</p>",0.5,'img/trophee.png', 128, 128, 'left');
      paramListes.niveau = -1;
      videBtListes();
    }
}

function allowDrop(ev) {
    ev.preventDefault();
}

function drag(ev) {
	let infosObjet;
	infosObjet = ev.target.id + "-" + ev.target.parentNode.id;
  ev.dataTransfer.setData("text", infosObjet);
}

function auSurvol(ev) {
  ev.preventDefault();
  if (ev.target === paramPositionnement.dernierSurvol) {return;} // Evite de boucler l'opération tant que l'on reste sur la même zone
  ev.target.className = "espace-large";
  paramPositionnement.dernierSurvol = ev.target;
}

function finSurvol(ev) {
  ev.target.className = "espace";
  paramPositionnement.dernierSurvol = null;
}

function drop(ev) {
  ev.preventDefault();
  const idObjet  = getIdObjet(ev.dataTransfer.getData('Text'));
  const idParent = getIdParent(ev.dataTransfer.getData('Text'));
  const Objet = document.getElementById(idObjet);
  if (ev.target.id === "receptacle") {  // L'élément est lâché sur une zone libre du réceptacle
      ev.target.appendChild(nouvelEspace(idObjet));
      if (idParent === "receptacle") {receptacle.removeChild(document.getElementById("espace-"+ Objet.id));}// On supprime l'espace lié à l'élément en cas de déplacement au sein de la zone réceptacle
      ev.target.appendChild(Objet);
    }
    else { // ou est lâché sur un espace entre éléments
      if ((ev.target.className !== "espace") && (ev.target.className !== "espace-large")) {return;}
      ev.target.className = "espace";
      if (getIdItemEspace(ev.target.id) === idObjet) {return;} // Si l'on survole l'espace lié à l'élément lui-même, on ne déplace pas
      if (idParent === "receptacle") {receptacle.removeChild(document.getElementById("espace-"+ Objet.id));} // On supprime l'espace lié à l'élément en cas de déplacement au sein de la zone réceptacle
      ev.target.parentNode.insertBefore(nouvelEspace(idObjet), ev.target);
      ev.target.parentNode.insertBefore(Objet, ev.target);
  }
  paramPositionnement.dernierSurvol = null;
}

choixNiveau();