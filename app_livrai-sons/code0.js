gdjs.menuCode = {};
gdjs.menuCode.GDbouton_95validerObjects1_1final = [];

gdjs.menuCode.GDjeuObjects1_1final = [];

gdjs.menuCode.GDscore_95Objects1_1final = [];

gdjs.menuCode.GDscore_95Objects1= [];
gdjs.menuCode.GDscore_95Objects2= [];
gdjs.menuCode.GDremorque_95gObjects1= [];
gdjs.menuCode.GDremorque_95gObjects2= [];
gdjs.menuCode.GDlama_95gObjects1= [];
gdjs.menuCode.GDlama_95gObjects2= [];
gdjs.menuCode.GDbouton_95retourObjects1= [];
gdjs.menuCode.GDbouton_95retourObjects2= [];
gdjs.menuCode.GDbouton_95validerObjects1= [];
gdjs.menuCode.GDbouton_95validerObjects2= [];
gdjs.menuCode.GDfond_95titre2Objects1= [];
gdjs.menuCode.GDfond_95titre2Objects2= [];
gdjs.menuCode.GDjeuObjects1= [];
gdjs.menuCode.GDjeuObjects2= [];
gdjs.menuCode.GDTitreObjects1= [];
gdjs.menuCode.GDTitreObjects2= [];
gdjs.menuCode.GDsoustitreObjects1= [];
gdjs.menuCode.GDsoustitreObjects2= [];
gdjs.menuCode.GDversionObjects1= [];
gdjs.menuCode.GDversionObjects2= [];
gdjs.menuCode.GDcreditsObjects1= [];
gdjs.menuCode.GDcreditsObjects2= [];
gdjs.menuCode.GDauteurObjects1= [];
gdjs.menuCode.GDauteurObjects2= [];
gdjs.menuCode.GDsergeObjects1= [];
gdjs.menuCode.GDsergeObjects2= [];
gdjs.menuCode.GDoptionsObjects1= [];
gdjs.menuCode.GDoptionsObjects2= [];
gdjs.menuCode.GDcompetencesObjects1= [];
gdjs.menuCode.GDcompetencesObjects2= [];
gdjs.menuCode.GDfond_95couleurObjects1= [];
gdjs.menuCode.GDfond_95couleurObjects2= [];
gdjs.menuCode.GDinfoObjects1= [];
gdjs.menuCode.GDinfoObjects2= [];
gdjs.menuCode.GDbouton_95infoObjects1= [];
gdjs.menuCode.GDbouton_95infoObjects2= [];

gdjs.menuCode.conditionTrue_0 = {val:false};
gdjs.menuCode.condition0IsTrue_0 = {val:false};
gdjs.menuCode.condition1IsTrue_0 = {val:false};
gdjs.menuCode.condition2IsTrue_0 = {val:false};
gdjs.menuCode.condition3IsTrue_0 = {val:false};
gdjs.menuCode.conditionTrue_1 = {val:false};
gdjs.menuCode.condition0IsTrue_1 = {val:false};
gdjs.menuCode.condition1IsTrue_1 = {val:false};
gdjs.menuCode.condition2IsTrue_1 = {val:false};
gdjs.menuCode.condition3IsTrue_1 = {val:false};


gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDjeuObjects2Objects = Hashtable.newFrom({"jeu": gdjs.menuCode.GDjeuObjects2});
gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDscore_9595Objects2Objects = Hashtable.newFrom({"score_": gdjs.menuCode.GDscore_95Objects2});
gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDbouton_9595validerObjects2Objects = Hashtable.newFrom({"bouton_valider": gdjs.menuCode.GDbouton_95validerObjects2});
gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDsergeObjects1Objects = Hashtable.newFrom({"serge": gdjs.menuCode.GDsergeObjects1});
gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDoptionsObjects1Objects = Hashtable.newFrom({"options": gdjs.menuCode.GDoptionsObjects1});
gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDbouton_9595infoObjects1Objects = Hashtable.newFrom({"bouton_info": gdjs.menuCode.GDbouton_95infoObjects1});
gdjs.menuCode.eventsList0 = function(runtimeScene) {

{


gdjs.menuCode.condition0IsTrue_0.val = false;
{
gdjs.menuCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.menuCode.condition0IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("fond_couleur"), gdjs.menuCode.GDfond_95couleurObjects1);
gdjs.copyArray(runtimeScene.getObjects("fond_titre2"), gdjs.menuCode.GDfond_95titre2Objects1);
gdjs.copyArray(runtimeScene.getObjects("lama_g"), gdjs.menuCode.GDlama_95gObjects1);
gdjs.copyArray(runtimeScene.getObjects("remorque_g"), gdjs.menuCode.GDremorque_95gObjects1);
gdjs.copyArray(runtimeScene.getObjects("score_"), gdjs.menuCode.GDscore_95Objects1);
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/audio_vide.mp3", 1, false, 100, 1);
}{for(var i = 0, len = gdjs.menuCode.GDscore_95Objects1.length ;i < len;++i) {
    gdjs.menuCode.GDscore_95Objects1[i].pauseAnimation();
}
}{for(var i = 0, len = gdjs.menuCode.GDscore_95Objects1.length ;i < len;++i) {
    gdjs.menuCode.GDscore_95Objects1[i].setAnimationFrame(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4).getChild("a1")));
}
}{for(var i = 0, len = gdjs.menuCode.GDlama_95gObjects1.length ;i < len;++i) {
    gdjs.menuCode.GDlama_95gObjects1[i].flipX(true);
}
}{for(var i = 0, len = gdjs.menuCode.GDremorque_95gObjects1.length ;i < len;++i) {
    gdjs.menuCode.GDremorque_95gObjects1[i].setX(gdjs.menuCode.GDremorque_95gObjects1[i].getX() - (300));
}
}{for(var i = 0, len = gdjs.menuCode.GDlama_95gObjects1.length ;i < len;++i) {
    gdjs.menuCode.GDlama_95gObjects1[i].setX(gdjs.menuCode.GDlama_95gObjects1[i].getX() - (300));
}
}{for(var i = 0, len = gdjs.menuCode.GDfond_95couleurObjects1.length ;i < len;++i) {
    gdjs.menuCode.GDfond_95couleurObjects1[i].setOpacity(150);
}
}{for(var i = 0, len = gdjs.menuCode.GDfond_95titre2Objects1.length ;i < len;++i) {
    gdjs.menuCode.GDfond_95titre2Objects1[i].setOpacity(150);
}
}}

}


{

gdjs.menuCode.GDbouton_95validerObjects1.length = 0;

gdjs.menuCode.GDjeuObjects1.length = 0;

gdjs.menuCode.GDscore_95Objects1.length = 0;


gdjs.menuCode.condition0IsTrue_0.val = false;
gdjs.menuCode.condition1IsTrue_0.val = false;
{
gdjs.menuCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.menuCode.condition0IsTrue_0.val ) {
{
{gdjs.menuCode.conditionTrue_1 = gdjs.menuCode.condition1IsTrue_0;
gdjs.menuCode.GDbouton_95validerObjects1_1final.length = 0;gdjs.menuCode.GDjeuObjects1_1final.length = 0;gdjs.menuCode.GDscore_95Objects1_1final.length = 0;gdjs.menuCode.condition0IsTrue_1.val = false;
gdjs.menuCode.condition1IsTrue_1.val = false;
gdjs.menuCode.condition2IsTrue_1.val = false;
{
gdjs.copyArray(runtimeScene.getObjects("jeu"), gdjs.menuCode.GDjeuObjects2);
gdjs.menuCode.condition0IsTrue_1.val = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDjeuObjects2Objects, runtimeScene, true, false);
if( gdjs.menuCode.condition0IsTrue_1.val ) {
    gdjs.menuCode.conditionTrue_1.val = true;
    for(var j = 0, jLen = gdjs.menuCode.GDjeuObjects2.length;j<jLen;++j) {
        if ( gdjs.menuCode.GDjeuObjects1_1final.indexOf(gdjs.menuCode.GDjeuObjects2[j]) === -1 )
            gdjs.menuCode.GDjeuObjects1_1final.push(gdjs.menuCode.GDjeuObjects2[j]);
    }
}
}
{
gdjs.copyArray(runtimeScene.getObjects("score_"), gdjs.menuCode.GDscore_95Objects2);
gdjs.menuCode.condition1IsTrue_1.val = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDscore_9595Objects2Objects, runtimeScene, true, false);
if( gdjs.menuCode.condition1IsTrue_1.val ) {
    gdjs.menuCode.conditionTrue_1.val = true;
    for(var j = 0, jLen = gdjs.menuCode.GDscore_95Objects2.length;j<jLen;++j) {
        if ( gdjs.menuCode.GDscore_95Objects1_1final.indexOf(gdjs.menuCode.GDscore_95Objects2[j]) === -1 )
            gdjs.menuCode.GDscore_95Objects1_1final.push(gdjs.menuCode.GDscore_95Objects2[j]);
    }
}
}
{
gdjs.copyArray(runtimeScene.getObjects("bouton_valider"), gdjs.menuCode.GDbouton_95validerObjects2);
gdjs.menuCode.condition2IsTrue_1.val = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDbouton_9595validerObjects2Objects, runtimeScene, true, false);
if( gdjs.menuCode.condition2IsTrue_1.val ) {
    gdjs.menuCode.conditionTrue_1.val = true;
    for(var j = 0, jLen = gdjs.menuCode.GDbouton_95validerObjects2.length;j<jLen;++j) {
        if ( gdjs.menuCode.GDbouton_95validerObjects1_1final.indexOf(gdjs.menuCode.GDbouton_95validerObjects2[j]) === -1 )
            gdjs.menuCode.GDbouton_95validerObjects1_1final.push(gdjs.menuCode.GDbouton_95validerObjects2[j]);
    }
}
}
{
gdjs.copyArray(gdjs.menuCode.GDbouton_95validerObjects1_1final, gdjs.menuCode.GDbouton_95validerObjects1);
gdjs.copyArray(gdjs.menuCode.GDjeuObjects1_1final, gdjs.menuCode.GDjeuObjects1);
gdjs.copyArray(gdjs.menuCode.GDscore_95Objects1_1final, gdjs.menuCode.GDscore_95Objects1);
}
}
}}
if (gdjs.menuCode.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "tirage", true);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("remorque_g"), gdjs.menuCode.GDremorque_95gObjects1);

gdjs.menuCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.menuCode.GDremorque_95gObjects1.length;i<l;++i) {
    if ( gdjs.menuCode.GDremorque_95gObjects1[i].getX() < 56 ) {
        gdjs.menuCode.condition0IsTrue_0.val = true;
        gdjs.menuCode.GDremorque_95gObjects1[k] = gdjs.menuCode.GDremorque_95gObjects1[i];
        ++k;
    }
}
gdjs.menuCode.GDremorque_95gObjects1.length = k;}if (gdjs.menuCode.condition0IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("lama_g"), gdjs.menuCode.GDlama_95gObjects1);
/* Reuse gdjs.menuCode.GDremorque_95gObjects1 */
{for(var i = 0, len = gdjs.menuCode.GDlama_95gObjects1.length ;i < len;++i) {
    gdjs.menuCode.GDlama_95gObjects1[i].setX(gdjs.menuCode.GDlama_95gObjects1[i].getX() + (2));
}
}{for(var i = 0, len = gdjs.menuCode.GDremorque_95gObjects1.length ;i < len;++i) {
    gdjs.menuCode.GDremorque_95gObjects1[i].setX(gdjs.menuCode.GDremorque_95gObjects1[i].getX() + (2));
}
}{for(var i = 0, len = gdjs.menuCode.GDlama_95gObjects1.length ;i < len;++i) {
    gdjs.menuCode.GDlama_95gObjects1[i].setAnimation(1);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("remorque_g"), gdjs.menuCode.GDremorque_95gObjects1);

gdjs.menuCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.menuCode.GDremorque_95gObjects1.length;i<l;++i) {
    if ( gdjs.menuCode.GDremorque_95gObjects1[i].getX() == 56 ) {
        gdjs.menuCode.condition0IsTrue_0.val = true;
        gdjs.menuCode.GDremorque_95gObjects1[k] = gdjs.menuCode.GDremorque_95gObjects1[i];
        ++k;
    }
}
gdjs.menuCode.GDremorque_95gObjects1.length = k;}if (gdjs.menuCode.condition0IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("lama_g"), gdjs.menuCode.GDlama_95gObjects1);
{for(var i = 0, len = gdjs.menuCode.GDlama_95gObjects1.length ;i < len;++i) {
    gdjs.menuCode.GDlama_95gObjects1[i].setAnimation(0);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("serge"), gdjs.menuCode.GDsergeObjects1);

gdjs.menuCode.condition0IsTrue_0.val = false;
gdjs.menuCode.condition1IsTrue_0.val = false;
gdjs.menuCode.condition2IsTrue_0.val = false;
{
gdjs.menuCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDsergeObjects1Objects, runtimeScene, true, false);
}if ( gdjs.menuCode.condition0IsTrue_0.val ) {
{
gdjs.menuCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.menuCode.condition1IsTrue_0.val ) {
{
{gdjs.menuCode.conditionTrue_1 = gdjs.menuCode.condition2IsTrue_0;
gdjs.menuCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(13934796);
}
}}
}
if (gdjs.menuCode.condition2IsTrue_0.val) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/bulle_1.mp3", 1, false, 100, 1);
}{runtimeScene.getVariables().getFromIndex(0).add(1);
}}

}


{


gdjs.menuCode.condition0IsTrue_0.val = false;
{
gdjs.menuCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) >= 5;
}if (gdjs.menuCode.condition0IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("score_"), gdjs.menuCode.GDscore_95Objects1);
{runtimeScene.getGame().getVariables().getFromIndex(4).getChild("a1").setNumber(0);
}{for(var i = 0, len = gdjs.menuCode.GDscore_95Objects1.length ;i < len;++i) {
    gdjs.menuCode.GDscore_95Objects1[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4).getChild("a1")));
}
}{runtimeScene.getVariables().getFromIndex(0).setNumber(0);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("options"), gdjs.menuCode.GDoptionsObjects1);

gdjs.menuCode.condition0IsTrue_0.val = false;
gdjs.menuCode.condition1IsTrue_0.val = false;
{
gdjs.menuCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDoptionsObjects1Objects, runtimeScene, true, false);
}if ( gdjs.menuCode.condition0IsTrue_0.val ) {
{
gdjs.menuCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}}
if (gdjs.menuCode.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "code", false);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_info"), gdjs.menuCode.GDbouton_95infoObjects1);

gdjs.menuCode.condition0IsTrue_0.val = false;
gdjs.menuCode.condition1IsTrue_0.val = false;
{
gdjs.menuCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDbouton_9595infoObjects1Objects, runtimeScene, true, false);
}if ( gdjs.menuCode.condition0IsTrue_0.val ) {
{
gdjs.menuCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}}
if (gdjs.menuCode.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "infos", false);
}}

}


};

gdjs.menuCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.menuCode.GDscore_95Objects1.length = 0;
gdjs.menuCode.GDscore_95Objects2.length = 0;
gdjs.menuCode.GDremorque_95gObjects1.length = 0;
gdjs.menuCode.GDremorque_95gObjects2.length = 0;
gdjs.menuCode.GDlama_95gObjects1.length = 0;
gdjs.menuCode.GDlama_95gObjects2.length = 0;
gdjs.menuCode.GDbouton_95retourObjects1.length = 0;
gdjs.menuCode.GDbouton_95retourObjects2.length = 0;
gdjs.menuCode.GDbouton_95validerObjects1.length = 0;
gdjs.menuCode.GDbouton_95validerObjects2.length = 0;
gdjs.menuCode.GDfond_95titre2Objects1.length = 0;
gdjs.menuCode.GDfond_95titre2Objects2.length = 0;
gdjs.menuCode.GDjeuObjects1.length = 0;
gdjs.menuCode.GDjeuObjects2.length = 0;
gdjs.menuCode.GDTitreObjects1.length = 0;
gdjs.menuCode.GDTitreObjects2.length = 0;
gdjs.menuCode.GDsoustitreObjects1.length = 0;
gdjs.menuCode.GDsoustitreObjects2.length = 0;
gdjs.menuCode.GDversionObjects1.length = 0;
gdjs.menuCode.GDversionObjects2.length = 0;
gdjs.menuCode.GDcreditsObjects1.length = 0;
gdjs.menuCode.GDcreditsObjects2.length = 0;
gdjs.menuCode.GDauteurObjects1.length = 0;
gdjs.menuCode.GDauteurObjects2.length = 0;
gdjs.menuCode.GDsergeObjects1.length = 0;
gdjs.menuCode.GDsergeObjects2.length = 0;
gdjs.menuCode.GDoptionsObjects1.length = 0;
gdjs.menuCode.GDoptionsObjects2.length = 0;
gdjs.menuCode.GDcompetencesObjects1.length = 0;
gdjs.menuCode.GDcompetencesObjects2.length = 0;
gdjs.menuCode.GDfond_95couleurObjects1.length = 0;
gdjs.menuCode.GDfond_95couleurObjects2.length = 0;
gdjs.menuCode.GDinfoObjects1.length = 0;
gdjs.menuCode.GDinfoObjects2.length = 0;
gdjs.menuCode.GDbouton_95infoObjects1.length = 0;
gdjs.menuCode.GDbouton_95infoObjects2.length = 0;

gdjs.menuCode.eventsList0(runtimeScene);
return;

}

gdjs['menuCode'] = gdjs.menuCode;
