gdjs.merciCode = {};
gdjs.merciCode.GDnb_952Objects1= [];
gdjs.merciCode.GDnb_952Objects2= [];
gdjs.merciCode.GDnb_953Objects1= [];
gdjs.merciCode.GDnb_953Objects2= [];
gdjs.merciCode.GDnb_95Objects1= [];
gdjs.merciCode.GDnb_95Objects2= [];
gdjs.merciCode.GDnb_954Objects1= [];
gdjs.merciCode.GDnb_954Objects2= [];
gdjs.merciCode.GDnb_956Objects1= [];
gdjs.merciCode.GDnb_956Objects2= [];
gdjs.merciCode.GDnb_955Objects1= [];
gdjs.merciCode.GDnb_955Objects2= [];
gdjs.merciCode.GDnb_951Objects1= [];
gdjs.merciCode.GDnb_951Objects2= [];
gdjs.merciCode.GDflecheObjects1= [];
gdjs.merciCode.GDflecheObjects2= [];
gdjs.merciCode.GDfusee1controlObjects1= [];
gdjs.merciCode.GDfusee1controlObjects2= [];
gdjs.merciCode.GDfusee1Objects1= [];
gdjs.merciCode.GDfusee1Objects2= [];
gdjs.merciCode.GDbouton_95recommencerObjects1= [];
gdjs.merciCode.GDbouton_95recommencerObjects2= [];
gdjs.merciCode.GDetoile1Objects1= [];
gdjs.merciCode.GDetoile1Objects2= [];
gdjs.merciCode.GDetoile2Objects1= [];
gdjs.merciCode.GDetoile2Objects2= [];
gdjs.merciCode.GDhautparleur_953Objects1= [];
gdjs.merciCode.GDhautparleur_953Objects2= [];
gdjs.merciCode.GDhautparleur_954Objects1= [];
gdjs.merciCode.GDhautparleur_954Objects2= [];
gdjs.merciCode.GDhautparleur_955Objects1= [];
gdjs.merciCode.GDhautparleur_955Objects2= [];
gdjs.merciCode.GDhautparleur_956Objects1= [];
gdjs.merciCode.GDhautparleur_956Objects2= [];
gdjs.merciCode.GDhautparleur_951Objects1= [];
gdjs.merciCode.GDhautparleur_951Objects2= [];
gdjs.merciCode.GDhautparleur_95Objects1= [];
gdjs.merciCode.GDhautparleur_95Objects2= [];
gdjs.merciCode.GDhautparleur_952Objects1= [];
gdjs.merciCode.GDhautparleur_952Objects2= [];
gdjs.merciCode.GDswitchObjects1= [];
gdjs.merciCode.GDswitchObjects2= [];
gdjs.merciCode.GDniveau2Objects1= [];
gdjs.merciCode.GDniveau2Objects2= [];
gdjs.merciCode.GDniveau1Objects1= [];
gdjs.merciCode.GDniveau1Objects2= [];
gdjs.merciCode.GDfiniObjects1= [];
gdjs.merciCode.GDfiniObjects2= [];
gdjs.merciCode.GDbouton_95retourObjects1= [];
gdjs.merciCode.GDbouton_95retourObjects2= [];
gdjs.merciCode.GDtxt_950Objects1= [];
gdjs.merciCode.GDtxt_950Objects2= [];
gdjs.merciCode.GDplaneteObjects1= [];
gdjs.merciCode.GDplaneteObjects2= [];
gdjs.merciCode.GDbouton_95aideObjects1= [];
gdjs.merciCode.GDbouton_95aideObjects2= [];
gdjs.merciCode.GDfond_95etoilesObjects1= [];
gdjs.merciCode.GDfond_95etoilesObjects2= [];
gdjs.merciCode.GDspr_95serge_95lamaObjects1= [];
gdjs.merciCode.GDspr_95serge_95lamaObjects2= [];
gdjs.merciCode.GDspr_95tabletteObjects1= [];
gdjs.merciCode.GDspr_95tabletteObjects2= [];
gdjs.merciCode.GDtxt_95merciObjects1= [];
gdjs.merciCode.GDtxt_95merciObjects2= [];
gdjs.merciCode.GDtxt_95auteurObjects1= [];
gdjs.merciCode.GDtxt_95auteurObjects2= [];
gdjs.merciCode.GDtxt_95creditsObjects1= [];
gdjs.merciCode.GDtxt_95creditsObjects2= [];
gdjs.merciCode.GDscore2Objects1= [];
gdjs.merciCode.GDscore2Objects2= [];
gdjs.merciCode.GDscore1Objects1= [];
gdjs.merciCode.GDscore1Objects2= [];
gdjs.merciCode.GDniv2Objects1= [];
gdjs.merciCode.GDniv2Objects2= [];
gdjs.merciCode.GDsoustitreObjects1= [];
gdjs.merciCode.GDsoustitreObjects2= [];
gdjs.merciCode.GDTitreObjects1= [];
gdjs.merciCode.GDTitreObjects2= [];
gdjs.merciCode.GDniv1Objects1= [];
gdjs.merciCode.GDniv1Objects2= [];
gdjs.merciCode.GDversion_95testObjects1= [];
gdjs.merciCode.GDversion_95testObjects2= [];
gdjs.merciCode.GDremerciementsObjects1= [];
gdjs.merciCode.GDremerciementsObjects2= [];

gdjs.merciCode.conditionTrue_0 = {val:false};
gdjs.merciCode.condition0IsTrue_0 = {val:false};
gdjs.merciCode.condition1IsTrue_0 = {val:false};
gdjs.merciCode.condition2IsTrue_0 = {val:false};
gdjs.merciCode.condition3IsTrue_0 = {val:false};


gdjs.merciCode.mapOfGDgdjs_46merciCode_46GDbouton_9595retourObjects1Objects = Hashtable.newFrom({"bouton_retour": gdjs.merciCode.GDbouton_95retourObjects1});gdjs.merciCode.mapOfGDgdjs_46merciCode_46GDspr_9595tabletteObjects1Objects = Hashtable.newFrom({"spr_tablette": gdjs.merciCode.GDspr_95tabletteObjects1});gdjs.merciCode.mapOfGDgdjs_46merciCode_46GDspr_9595tabletteObjects1Objects = Hashtable.newFrom({"spr_tablette": gdjs.merciCode.GDspr_95tabletteObjects1});gdjs.merciCode.eventsList0x5b70b8 = function(runtimeScene) {

{

gdjs.merciCode.GDbouton_95retourObjects1.createFrom(runtimeScene.getObjects("bouton_retour"));

gdjs.merciCode.condition0IsTrue_0.val = false;
gdjs.merciCode.condition1IsTrue_0.val = false;
{
gdjs.merciCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.merciCode.condition0IsTrue_0.val ) {
{
gdjs.merciCode.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.merciCode.mapOfGDgdjs_46merciCode_46GDbouton_9595retourObjects1Objects, runtimeScene, true, false);
}}
if (gdjs.merciCode.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "menu", false);
}}

}


{

gdjs.merciCode.GDspr_95tabletteObjects1.createFrom(runtimeScene.getObjects("spr_tablette"));

gdjs.merciCode.condition0IsTrue_0.val = false;
gdjs.merciCode.condition1IsTrue_0.val = false;
gdjs.merciCode.condition2IsTrue_0.val = false;
{
gdjs.merciCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.merciCode.condition0IsTrue_0.val ) {
{
gdjs.merciCode.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.merciCode.mapOfGDgdjs_46merciCode_46GDspr_9595tabletteObjects1Objects, runtimeScene, true, false);
}if ( gdjs.merciCode.condition1IsTrue_0.val ) {
{
gdjs.merciCode.condition2IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 0;
}}
}
if (gdjs.merciCode.condition2IsTrue_0.val) {
{gdjs.evtTools.window.setFullScreen(runtimeScene, true, true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(1);
}}

}


{

gdjs.merciCode.GDspr_95tabletteObjects1.createFrom(runtimeScene.getObjects("spr_tablette"));

gdjs.merciCode.condition0IsTrue_0.val = false;
gdjs.merciCode.condition1IsTrue_0.val = false;
gdjs.merciCode.condition2IsTrue_0.val = false;
{
gdjs.merciCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.merciCode.condition0IsTrue_0.val ) {
{
gdjs.merciCode.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.merciCode.mapOfGDgdjs_46merciCode_46GDspr_9595tabletteObjects1Objects, runtimeScene, true, false);
}if ( gdjs.merciCode.condition1IsTrue_0.val ) {
{
gdjs.merciCode.condition2IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 1;
}}
}
if (gdjs.merciCode.condition2IsTrue_0.val) {
{gdjs.evtTools.window.setFullScreen(runtimeScene, true, true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(0);
}}

}


{


gdjs.merciCode.condition0IsTrue_0.val = false;
{
gdjs.merciCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.merciCode.condition0IsTrue_0.val) {
gdjs.merciCode.GDfusee1Objects1.createFrom(runtimeScene.getObjects("fusee1"));
{for(var i = 0, len = gdjs.merciCode.GDfusee1Objects1.length ;i < len;++i) {
    gdjs.merciCode.GDfusee1Objects1[i].setAnimation(1);
}
}}

}


}; //End of gdjs.merciCode.eventsList0x5b70b8


gdjs.merciCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.merciCode.GDnb_952Objects1.length = 0;
gdjs.merciCode.GDnb_952Objects2.length = 0;
gdjs.merciCode.GDnb_953Objects1.length = 0;
gdjs.merciCode.GDnb_953Objects2.length = 0;
gdjs.merciCode.GDnb_95Objects1.length = 0;
gdjs.merciCode.GDnb_95Objects2.length = 0;
gdjs.merciCode.GDnb_954Objects1.length = 0;
gdjs.merciCode.GDnb_954Objects2.length = 0;
gdjs.merciCode.GDnb_956Objects1.length = 0;
gdjs.merciCode.GDnb_956Objects2.length = 0;
gdjs.merciCode.GDnb_955Objects1.length = 0;
gdjs.merciCode.GDnb_955Objects2.length = 0;
gdjs.merciCode.GDnb_951Objects1.length = 0;
gdjs.merciCode.GDnb_951Objects2.length = 0;
gdjs.merciCode.GDflecheObjects1.length = 0;
gdjs.merciCode.GDflecheObjects2.length = 0;
gdjs.merciCode.GDfusee1controlObjects1.length = 0;
gdjs.merciCode.GDfusee1controlObjects2.length = 0;
gdjs.merciCode.GDfusee1Objects1.length = 0;
gdjs.merciCode.GDfusee1Objects2.length = 0;
gdjs.merciCode.GDbouton_95recommencerObjects1.length = 0;
gdjs.merciCode.GDbouton_95recommencerObjects2.length = 0;
gdjs.merciCode.GDetoile1Objects1.length = 0;
gdjs.merciCode.GDetoile1Objects2.length = 0;
gdjs.merciCode.GDetoile2Objects1.length = 0;
gdjs.merciCode.GDetoile2Objects2.length = 0;
gdjs.merciCode.GDhautparleur_953Objects1.length = 0;
gdjs.merciCode.GDhautparleur_953Objects2.length = 0;
gdjs.merciCode.GDhautparleur_954Objects1.length = 0;
gdjs.merciCode.GDhautparleur_954Objects2.length = 0;
gdjs.merciCode.GDhautparleur_955Objects1.length = 0;
gdjs.merciCode.GDhautparleur_955Objects2.length = 0;
gdjs.merciCode.GDhautparleur_956Objects1.length = 0;
gdjs.merciCode.GDhautparleur_956Objects2.length = 0;
gdjs.merciCode.GDhautparleur_951Objects1.length = 0;
gdjs.merciCode.GDhautparleur_951Objects2.length = 0;
gdjs.merciCode.GDhautparleur_95Objects1.length = 0;
gdjs.merciCode.GDhautparleur_95Objects2.length = 0;
gdjs.merciCode.GDhautparleur_952Objects1.length = 0;
gdjs.merciCode.GDhautparleur_952Objects2.length = 0;
gdjs.merciCode.GDswitchObjects1.length = 0;
gdjs.merciCode.GDswitchObjects2.length = 0;
gdjs.merciCode.GDniveau2Objects1.length = 0;
gdjs.merciCode.GDniveau2Objects2.length = 0;
gdjs.merciCode.GDniveau1Objects1.length = 0;
gdjs.merciCode.GDniveau1Objects2.length = 0;
gdjs.merciCode.GDfiniObjects1.length = 0;
gdjs.merciCode.GDfiniObjects2.length = 0;
gdjs.merciCode.GDbouton_95retourObjects1.length = 0;
gdjs.merciCode.GDbouton_95retourObjects2.length = 0;
gdjs.merciCode.GDtxt_950Objects1.length = 0;
gdjs.merciCode.GDtxt_950Objects2.length = 0;
gdjs.merciCode.GDplaneteObjects1.length = 0;
gdjs.merciCode.GDplaneteObjects2.length = 0;
gdjs.merciCode.GDbouton_95aideObjects1.length = 0;
gdjs.merciCode.GDbouton_95aideObjects2.length = 0;
gdjs.merciCode.GDfond_95etoilesObjects1.length = 0;
gdjs.merciCode.GDfond_95etoilesObjects2.length = 0;
gdjs.merciCode.GDspr_95serge_95lamaObjects1.length = 0;
gdjs.merciCode.GDspr_95serge_95lamaObjects2.length = 0;
gdjs.merciCode.GDspr_95tabletteObjects1.length = 0;
gdjs.merciCode.GDspr_95tabletteObjects2.length = 0;
gdjs.merciCode.GDtxt_95merciObjects1.length = 0;
gdjs.merciCode.GDtxt_95merciObjects2.length = 0;
gdjs.merciCode.GDtxt_95auteurObjects1.length = 0;
gdjs.merciCode.GDtxt_95auteurObjects2.length = 0;
gdjs.merciCode.GDtxt_95creditsObjects1.length = 0;
gdjs.merciCode.GDtxt_95creditsObjects2.length = 0;
gdjs.merciCode.GDscore2Objects1.length = 0;
gdjs.merciCode.GDscore2Objects2.length = 0;
gdjs.merciCode.GDscore1Objects1.length = 0;
gdjs.merciCode.GDscore1Objects2.length = 0;
gdjs.merciCode.GDniv2Objects1.length = 0;
gdjs.merciCode.GDniv2Objects2.length = 0;
gdjs.merciCode.GDsoustitreObjects1.length = 0;
gdjs.merciCode.GDsoustitreObjects2.length = 0;
gdjs.merciCode.GDTitreObjects1.length = 0;
gdjs.merciCode.GDTitreObjects2.length = 0;
gdjs.merciCode.GDniv1Objects1.length = 0;
gdjs.merciCode.GDniv1Objects2.length = 0;
gdjs.merciCode.GDversion_95testObjects1.length = 0;
gdjs.merciCode.GDversion_95testObjects2.length = 0;
gdjs.merciCode.GDremerciementsObjects1.length = 0;
gdjs.merciCode.GDremerciementsObjects2.length = 0;

gdjs.merciCode.eventsList0x5b70b8(runtimeScene);
return;

}

gdjs['merciCode'] = gdjs.merciCode;
