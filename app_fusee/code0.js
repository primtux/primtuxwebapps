gdjs.menuCode = {};
gdjs.menuCode.GDnb_952Objects1= [];
gdjs.menuCode.GDnb_952Objects2= [];
gdjs.menuCode.GDnb_953Objects1= [];
gdjs.menuCode.GDnb_953Objects2= [];
gdjs.menuCode.GDnb_95Objects1= [];
gdjs.menuCode.GDnb_95Objects2= [];
gdjs.menuCode.GDnb_954Objects1= [];
gdjs.menuCode.GDnb_954Objects2= [];
gdjs.menuCode.GDnb_956Objects1= [];
gdjs.menuCode.GDnb_956Objects2= [];
gdjs.menuCode.GDnb_955Objects1= [];
gdjs.menuCode.GDnb_955Objects2= [];
gdjs.menuCode.GDnb_951Objects1= [];
gdjs.menuCode.GDnb_951Objects2= [];
gdjs.menuCode.GDflecheObjects1= [];
gdjs.menuCode.GDflecheObjects2= [];
gdjs.menuCode.GDfusee1controlObjects1= [];
gdjs.menuCode.GDfusee1controlObjects2= [];
gdjs.menuCode.GDfusee1Objects1= [];
gdjs.menuCode.GDfusee1Objects2= [];
gdjs.menuCode.GDbouton_95recommencerObjects1= [];
gdjs.menuCode.GDbouton_95recommencerObjects2= [];
gdjs.menuCode.GDetoile1Objects1= [];
gdjs.menuCode.GDetoile1Objects2= [];
gdjs.menuCode.GDetoile2Objects1= [];
gdjs.menuCode.GDetoile2Objects2= [];
gdjs.menuCode.GDhautparleur_953Objects1= [];
gdjs.menuCode.GDhautparleur_953Objects2= [];
gdjs.menuCode.GDhautparleur_954Objects1= [];
gdjs.menuCode.GDhautparleur_954Objects2= [];
gdjs.menuCode.GDhautparleur_955Objects1= [];
gdjs.menuCode.GDhautparleur_955Objects2= [];
gdjs.menuCode.GDhautparleur_956Objects1= [];
gdjs.menuCode.GDhautparleur_956Objects2= [];
gdjs.menuCode.GDhautparleur_951Objects1= [];
gdjs.menuCode.GDhautparleur_951Objects2= [];
gdjs.menuCode.GDhautparleur_95Objects1= [];
gdjs.menuCode.GDhautparleur_95Objects2= [];
gdjs.menuCode.GDhautparleur_952Objects1= [];
gdjs.menuCode.GDhautparleur_952Objects2= [];
gdjs.menuCode.GDswitchObjects1= [];
gdjs.menuCode.GDswitchObjects2= [];
gdjs.menuCode.GDniveau2Objects1= [];
gdjs.menuCode.GDniveau2Objects2= [];
gdjs.menuCode.GDniveau1Objects1= [];
gdjs.menuCode.GDniveau1Objects2= [];
gdjs.menuCode.GDfiniObjects1= [];
gdjs.menuCode.GDfiniObjects2= [];
gdjs.menuCode.GDbouton_95retourObjects1= [];
gdjs.menuCode.GDbouton_95retourObjects2= [];
gdjs.menuCode.GDtxt_950Objects1= [];
gdjs.menuCode.GDtxt_950Objects2= [];
gdjs.menuCode.GDplaneteObjects1= [];
gdjs.menuCode.GDplaneteObjects2= [];
gdjs.menuCode.GDbouton_95aideObjects1= [];
gdjs.menuCode.GDbouton_95aideObjects2= [];
gdjs.menuCode.GDfond_95etoilesObjects1= [];
gdjs.menuCode.GDfond_95etoilesObjects2= [];
gdjs.menuCode.GDspr_95serge_95lamaObjects1= [];
gdjs.menuCode.GDspr_95serge_95lamaObjects2= [];
gdjs.menuCode.GDspr_95tabletteObjects1= [];
gdjs.menuCode.GDspr_95tabletteObjects2= [];
gdjs.menuCode.GDtxt_95merciObjects1= [];
gdjs.menuCode.GDtxt_95merciObjects2= [];
gdjs.menuCode.GDtxt_95auteurObjects1= [];
gdjs.menuCode.GDtxt_95auteurObjects2= [];
gdjs.menuCode.GDtxt_95creditsObjects1= [];
gdjs.menuCode.GDtxt_95creditsObjects2= [];
gdjs.menuCode.GDscore2Objects1= [];
gdjs.menuCode.GDscore2Objects2= [];
gdjs.menuCode.GDscore1Objects1= [];
gdjs.menuCode.GDscore1Objects2= [];
gdjs.menuCode.GDniv2Objects1= [];
gdjs.menuCode.GDniv2Objects2= [];
gdjs.menuCode.GDcompetencesObjects1= [];
gdjs.menuCode.GDcompetencesObjects2= [];
gdjs.menuCode.GDsoustitreObjects1= [];
gdjs.menuCode.GDsoustitreObjects2= [];
gdjs.menuCode.GDTitreObjects1= [];
gdjs.menuCode.GDTitreObjects2= [];
gdjs.menuCode.GDniv1Objects1= [];
gdjs.menuCode.GDniv1Objects2= [];
gdjs.menuCode.GDversion_95testObjects1= [];
gdjs.menuCode.GDversion_95testObjects2= [];
gdjs.menuCode.GDpleinecranObjects1= [];
gdjs.menuCode.GDpleinecranObjects2= [];

gdjs.menuCode.conditionTrue_0 = {val:false};
gdjs.menuCode.condition0IsTrue_0 = {val:false};
gdjs.menuCode.condition1IsTrue_0 = {val:false};
gdjs.menuCode.condition2IsTrue_0 = {val:false};
gdjs.menuCode.condition3IsTrue_0 = {val:false};


gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDniv1Objects1Objects = Hashtable.newFrom({"niv1": gdjs.menuCode.GDniv1Objects1});gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDetoile1Objects1Objects = Hashtable.newFrom({"etoile1": gdjs.menuCode.GDetoile1Objects1});gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDniv2Objects1Objects = Hashtable.newFrom({"niv2": gdjs.menuCode.GDniv2Objects1});gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDetoile2Objects1Objects = Hashtable.newFrom({"etoile2": gdjs.menuCode.GDetoile2Objects1});gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDtxt_9595merciObjects1Objects = Hashtable.newFrom({"txt_merci": gdjs.menuCode.GDtxt_95merciObjects1});gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDspr_9595tabletteObjects1Objects = Hashtable.newFrom({"spr_tablette": gdjs.menuCode.GDspr_95tabletteObjects1});gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDspr_9595tabletteObjects1Objects = Hashtable.newFrom({"spr_tablette": gdjs.menuCode.GDspr_95tabletteObjects1});gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDspr_9595serge_9595lamaObjects1Objects = Hashtable.newFrom({"spr_serge_lama": gdjs.menuCode.GDspr_95serge_95lamaObjects1});gdjs.menuCode.eventsList0x5b70b8 = function(runtimeScene) {

{

gdjs.menuCode.GDniv1Objects1.createFrom(runtimeScene.getObjects("niv1"));

gdjs.menuCode.condition0IsTrue_0.val = false;
gdjs.menuCode.condition1IsTrue_0.val = false;
{
gdjs.menuCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.menuCode.condition0IsTrue_0.val ) {
{
gdjs.menuCode.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDniv1Objects1Objects, runtimeScene, true, false);
}}
if (gdjs.menuCode.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "1_niv1", false);
}}

}


{

gdjs.menuCode.GDetoile1Objects1.createFrom(runtimeScene.getObjects("etoile1"));

gdjs.menuCode.condition0IsTrue_0.val = false;
gdjs.menuCode.condition1IsTrue_0.val = false;
{
gdjs.menuCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.menuCode.condition0IsTrue_0.val ) {
{
gdjs.menuCode.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDetoile1Objects1Objects, runtimeScene, true, false);
}}
if (gdjs.menuCode.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "1_niv1", false);
}}

}


{

gdjs.menuCode.GDniv2Objects1.createFrom(runtimeScene.getObjects("niv2"));

gdjs.menuCode.condition0IsTrue_0.val = false;
gdjs.menuCode.condition1IsTrue_0.val = false;
{
gdjs.menuCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.menuCode.condition0IsTrue_0.val ) {
{
gdjs.menuCode.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDniv2Objects1Objects, runtimeScene, true, false);
}}
if (gdjs.menuCode.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "1_niv2", false);
}}

}


{

gdjs.menuCode.GDetoile2Objects1.createFrom(runtimeScene.getObjects("etoile2"));

gdjs.menuCode.condition0IsTrue_0.val = false;
gdjs.menuCode.condition1IsTrue_0.val = false;
{
gdjs.menuCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.menuCode.condition0IsTrue_0.val ) {
{
gdjs.menuCode.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDetoile2Objects1Objects, runtimeScene, true, false);
}}
if (gdjs.menuCode.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "1_niv2", false);
}}

}


{

gdjs.menuCode.GDtxt_95merciObjects1.createFrom(runtimeScene.getObjects("txt_merci"));

gdjs.menuCode.condition0IsTrue_0.val = false;
gdjs.menuCode.condition1IsTrue_0.val = false;
{
gdjs.menuCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.menuCode.condition0IsTrue_0.val ) {
{
gdjs.menuCode.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDtxt_9595merciObjects1Objects, runtimeScene, true, false);
}}
if (gdjs.menuCode.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "merci", false);
}}

}


{

gdjs.menuCode.GDspr_95tabletteObjects1.createFrom(runtimeScene.getObjects("spr_tablette"));

gdjs.menuCode.condition0IsTrue_0.val = false;
gdjs.menuCode.condition1IsTrue_0.val = false;
gdjs.menuCode.condition2IsTrue_0.val = false;
{
gdjs.menuCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.menuCode.condition0IsTrue_0.val ) {
{
gdjs.menuCode.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDspr_9595tabletteObjects1Objects, runtimeScene, true, false);
}if ( gdjs.menuCode.condition1IsTrue_0.val ) {
{
gdjs.menuCode.condition2IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 0;
}}
}
if (gdjs.menuCode.condition2IsTrue_0.val) {
{gdjs.evtTools.window.setFullScreen(runtimeScene, true, true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(1);
}}

}


{

gdjs.menuCode.GDspr_95tabletteObjects1.createFrom(runtimeScene.getObjects("spr_tablette"));

gdjs.menuCode.condition0IsTrue_0.val = false;
gdjs.menuCode.condition1IsTrue_0.val = false;
gdjs.menuCode.condition2IsTrue_0.val = false;
{
gdjs.menuCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.menuCode.condition0IsTrue_0.val ) {
{
gdjs.menuCode.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDspr_9595tabletteObjects1Objects, runtimeScene, true, false);
}if ( gdjs.menuCode.condition1IsTrue_0.val ) {
{
gdjs.menuCode.condition2IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 1;
}}
}
if (gdjs.menuCode.condition2IsTrue_0.val) {
{gdjs.evtTools.window.setFullScreen(runtimeScene, true, true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(0);
}}

}


{


gdjs.menuCode.condition0IsTrue_0.val = false;
{
gdjs.menuCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.menuCode.condition0IsTrue_0.val) {
gdjs.menuCode.GDfusee1Objects1.createFrom(runtimeScene.getObjects("fusee1"));
gdjs.menuCode.GDscore1Objects1.createFrom(runtimeScene.getObjects("score1"));
gdjs.menuCode.GDscore2Objects1.createFrom(runtimeScene.getObjects("score2"));
{for(var i = 0, len = gdjs.menuCode.GDfusee1Objects1.length ;i < len;++i) {
    gdjs.menuCode.GDfusee1Objects1[i].setAnimation(1);
}
}{for(var i = 0, len = gdjs.menuCode.GDscore1Objects1.length ;i < len;++i) {
    gdjs.menuCode.GDscore1Objects1[i].setAnimation(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(5)));
}
}{for(var i = 0, len = gdjs.menuCode.GDscore2Objects1.length ;i < len;++i) {
    gdjs.menuCode.GDscore2Objects1[i].setAnimation(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(6)));
}
}}

}


{

gdjs.menuCode.GDspr_95serge_95lamaObjects1.createFrom(runtimeScene.getObjects("spr_serge_lama"));

gdjs.menuCode.condition0IsTrue_0.val = false;
gdjs.menuCode.condition1IsTrue_0.val = false;
{
gdjs.menuCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.menuCode.condition0IsTrue_0.val ) {
{
gdjs.menuCode.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDspr_9595serge_9595lamaObjects1Objects, runtimeScene, true, false);
}}
if (gdjs.menuCode.condition1IsTrue_0.val) {
{runtimeScene.getVariables().getFromIndex(0).add(1);
}}

}


{


gdjs.menuCode.condition0IsTrue_0.val = false;
{
gdjs.menuCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) == 5;
}if (gdjs.menuCode.condition0IsTrue_0.val) {
gdjs.menuCode.GDscore1Objects1.createFrom(runtimeScene.getObjects("score1"));
gdjs.menuCode.GDscore2Objects1.createFrom(runtimeScene.getObjects("score2"));
{runtimeScene.getGame().getVariables().getFromIndex(5).setNumber(0);
}{runtimeScene.getGame().getVariables().getFromIndex(6).setNumber(0);
}{for(var i = 0, len = gdjs.menuCode.GDscore1Objects1.length ;i < len;++i) {
    gdjs.menuCode.GDscore1Objects1[i].setAnimation(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(5)));
}
}{for(var i = 0, len = gdjs.menuCode.GDscore2Objects1.length ;i < len;++i) {
    gdjs.menuCode.GDscore2Objects1[i].setAnimation(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(6)));
}
}}

}


}; //End of gdjs.menuCode.eventsList0x5b70b8


gdjs.menuCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.menuCode.GDnb_952Objects1.length = 0;
gdjs.menuCode.GDnb_952Objects2.length = 0;
gdjs.menuCode.GDnb_953Objects1.length = 0;
gdjs.menuCode.GDnb_953Objects2.length = 0;
gdjs.menuCode.GDnb_95Objects1.length = 0;
gdjs.menuCode.GDnb_95Objects2.length = 0;
gdjs.menuCode.GDnb_954Objects1.length = 0;
gdjs.menuCode.GDnb_954Objects2.length = 0;
gdjs.menuCode.GDnb_956Objects1.length = 0;
gdjs.menuCode.GDnb_956Objects2.length = 0;
gdjs.menuCode.GDnb_955Objects1.length = 0;
gdjs.menuCode.GDnb_955Objects2.length = 0;
gdjs.menuCode.GDnb_951Objects1.length = 0;
gdjs.menuCode.GDnb_951Objects2.length = 0;
gdjs.menuCode.GDflecheObjects1.length = 0;
gdjs.menuCode.GDflecheObjects2.length = 0;
gdjs.menuCode.GDfusee1controlObjects1.length = 0;
gdjs.menuCode.GDfusee1controlObjects2.length = 0;
gdjs.menuCode.GDfusee1Objects1.length = 0;
gdjs.menuCode.GDfusee1Objects2.length = 0;
gdjs.menuCode.GDbouton_95recommencerObjects1.length = 0;
gdjs.menuCode.GDbouton_95recommencerObjects2.length = 0;
gdjs.menuCode.GDetoile1Objects1.length = 0;
gdjs.menuCode.GDetoile1Objects2.length = 0;
gdjs.menuCode.GDetoile2Objects1.length = 0;
gdjs.menuCode.GDetoile2Objects2.length = 0;
gdjs.menuCode.GDhautparleur_953Objects1.length = 0;
gdjs.menuCode.GDhautparleur_953Objects2.length = 0;
gdjs.menuCode.GDhautparleur_954Objects1.length = 0;
gdjs.menuCode.GDhautparleur_954Objects2.length = 0;
gdjs.menuCode.GDhautparleur_955Objects1.length = 0;
gdjs.menuCode.GDhautparleur_955Objects2.length = 0;
gdjs.menuCode.GDhautparleur_956Objects1.length = 0;
gdjs.menuCode.GDhautparleur_956Objects2.length = 0;
gdjs.menuCode.GDhautparleur_951Objects1.length = 0;
gdjs.menuCode.GDhautparleur_951Objects2.length = 0;
gdjs.menuCode.GDhautparleur_95Objects1.length = 0;
gdjs.menuCode.GDhautparleur_95Objects2.length = 0;
gdjs.menuCode.GDhautparleur_952Objects1.length = 0;
gdjs.menuCode.GDhautparleur_952Objects2.length = 0;
gdjs.menuCode.GDswitchObjects1.length = 0;
gdjs.menuCode.GDswitchObjects2.length = 0;
gdjs.menuCode.GDniveau2Objects1.length = 0;
gdjs.menuCode.GDniveau2Objects2.length = 0;
gdjs.menuCode.GDniveau1Objects1.length = 0;
gdjs.menuCode.GDniveau1Objects2.length = 0;
gdjs.menuCode.GDfiniObjects1.length = 0;
gdjs.menuCode.GDfiniObjects2.length = 0;
gdjs.menuCode.GDbouton_95retourObjects1.length = 0;
gdjs.menuCode.GDbouton_95retourObjects2.length = 0;
gdjs.menuCode.GDtxt_950Objects1.length = 0;
gdjs.menuCode.GDtxt_950Objects2.length = 0;
gdjs.menuCode.GDplaneteObjects1.length = 0;
gdjs.menuCode.GDplaneteObjects2.length = 0;
gdjs.menuCode.GDbouton_95aideObjects1.length = 0;
gdjs.menuCode.GDbouton_95aideObjects2.length = 0;
gdjs.menuCode.GDfond_95etoilesObjects1.length = 0;
gdjs.menuCode.GDfond_95etoilesObjects2.length = 0;
gdjs.menuCode.GDspr_95serge_95lamaObjects1.length = 0;
gdjs.menuCode.GDspr_95serge_95lamaObjects2.length = 0;
gdjs.menuCode.GDspr_95tabletteObjects1.length = 0;
gdjs.menuCode.GDspr_95tabletteObjects2.length = 0;
gdjs.menuCode.GDtxt_95merciObjects1.length = 0;
gdjs.menuCode.GDtxt_95merciObjects2.length = 0;
gdjs.menuCode.GDtxt_95auteurObjects1.length = 0;
gdjs.menuCode.GDtxt_95auteurObjects2.length = 0;
gdjs.menuCode.GDtxt_95creditsObjects1.length = 0;
gdjs.menuCode.GDtxt_95creditsObjects2.length = 0;
gdjs.menuCode.GDscore2Objects1.length = 0;
gdjs.menuCode.GDscore2Objects2.length = 0;
gdjs.menuCode.GDscore1Objects1.length = 0;
gdjs.menuCode.GDscore1Objects2.length = 0;
gdjs.menuCode.GDniv2Objects1.length = 0;
gdjs.menuCode.GDniv2Objects2.length = 0;
gdjs.menuCode.GDcompetencesObjects1.length = 0;
gdjs.menuCode.GDcompetencesObjects2.length = 0;
gdjs.menuCode.GDsoustitreObjects1.length = 0;
gdjs.menuCode.GDsoustitreObjects2.length = 0;
gdjs.menuCode.GDTitreObjects1.length = 0;
gdjs.menuCode.GDTitreObjects2.length = 0;
gdjs.menuCode.GDniv1Objects1.length = 0;
gdjs.menuCode.GDniv1Objects2.length = 0;
gdjs.menuCode.GDversion_95testObjects1.length = 0;
gdjs.menuCode.GDversion_95testObjects2.length = 0;
gdjs.menuCode.GDpleinecranObjects1.length = 0;
gdjs.menuCode.GDpleinecranObjects2.length = 0;

gdjs.menuCode.eventsList0x5b70b8(runtimeScene);
return;

}

gdjs['menuCode'] = gdjs.menuCode;
