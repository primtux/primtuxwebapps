gdjs.introCode = {};
gdjs.introCode.localVariables = [];
gdjs.introCode.GDsp_9595introObjects1= [];
gdjs.introCode.GDsp_9595introObjects2= [];
gdjs.introCode.GDsp_9595carte_9595lamaObjects1= [];
gdjs.introCode.GDsp_9595carte_9595lamaObjects2= [];
gdjs.introCode.GDsp_9595bouton_9595retourObjects1= [];
gdjs.introCode.GDsp_9595bouton_9595retourObjects2= [];
gdjs.introCode.GDsp_9595bouton_9595validerObjects1= [];
gdjs.introCode.GDsp_9595bouton_9595validerObjects2= [];
gdjs.introCode.GDsp_9595jetonObjects1= [];
gdjs.introCode.GDsp_9595jetonObjects2= [];
gdjs.introCode.GDsp_9595bouton_9595recommencerObjects1= [];
gdjs.introCode.GDsp_9595bouton_9595recommencerObjects2= [];
gdjs.introCode.GDsp_9595fondObjects1= [];
gdjs.introCode.GDsp_9595fondObjects2= [];


gdjs.introCode.mapOfGDgdjs_9546introCode_9546GDsp_95959595bouton_95959595validerObjects1Objects = Hashtable.newFrom({"sp_bouton_valider": gdjs.introCode.GDsp_9595bouton_9595validerObjects1});
gdjs.introCode.eventsList0 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
}

}


{

gdjs.copyArray(runtimeScene.getObjects("sp_bouton_valider"), gdjs.introCode.GDsp_9595bouton_9595validerObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.introCode.mapOfGDgdjs_9546introCode_9546GDsp_95959595bouton_95959595validerObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "menu", false);
}}

}


};

gdjs.introCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.introCode.GDsp_9595introObjects1.length = 0;
gdjs.introCode.GDsp_9595introObjects2.length = 0;
gdjs.introCode.GDsp_9595carte_9595lamaObjects1.length = 0;
gdjs.introCode.GDsp_9595carte_9595lamaObjects2.length = 0;
gdjs.introCode.GDsp_9595bouton_9595retourObjects1.length = 0;
gdjs.introCode.GDsp_9595bouton_9595retourObjects2.length = 0;
gdjs.introCode.GDsp_9595bouton_9595validerObjects1.length = 0;
gdjs.introCode.GDsp_9595bouton_9595validerObjects2.length = 0;
gdjs.introCode.GDsp_9595jetonObjects1.length = 0;
gdjs.introCode.GDsp_9595jetonObjects2.length = 0;
gdjs.introCode.GDsp_9595bouton_9595recommencerObjects1.length = 0;
gdjs.introCode.GDsp_9595bouton_9595recommencerObjects2.length = 0;
gdjs.introCode.GDsp_9595fondObjects1.length = 0;
gdjs.introCode.GDsp_9595fondObjects2.length = 0;

gdjs.introCode.eventsList0(runtimeScene);
gdjs.introCode.GDsp_9595introObjects1.length = 0;
gdjs.introCode.GDsp_9595introObjects2.length = 0;
gdjs.introCode.GDsp_9595carte_9595lamaObjects1.length = 0;
gdjs.introCode.GDsp_9595carte_9595lamaObjects2.length = 0;
gdjs.introCode.GDsp_9595bouton_9595retourObjects1.length = 0;
gdjs.introCode.GDsp_9595bouton_9595retourObjects2.length = 0;
gdjs.introCode.GDsp_9595bouton_9595validerObjects1.length = 0;
gdjs.introCode.GDsp_9595bouton_9595validerObjects2.length = 0;
gdjs.introCode.GDsp_9595jetonObjects1.length = 0;
gdjs.introCode.GDsp_9595jetonObjects2.length = 0;
gdjs.introCode.GDsp_9595bouton_9595recommencerObjects1.length = 0;
gdjs.introCode.GDsp_9595bouton_9595recommencerObjects2.length = 0;
gdjs.introCode.GDsp_9595fondObjects1.length = 0;
gdjs.introCode.GDsp_9595fondObjects2.length = 0;


return;

}

gdjs['introCode'] = gdjs.introCode;
