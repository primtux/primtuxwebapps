gdjs.jeu_95facileCode = {};
gdjs.jeu_95facileCode.localVariables = [];
gdjs.jeu_95facileCode.GDtxt_9595consigneObjects1= [];
gdjs.jeu_95facileCode.GDtxt_9595consigneObjects2= [];
gdjs.jeu_95facileCode.GDtxt_9595consigneObjects3= [];
gdjs.jeu_95facileCode.GDtxt_9595consigneObjects4= [];
gdjs.jeu_95facileCode.GDtxt_9595consigneObjects5= [];
gdjs.jeu_95facileCode.GDsp_9595bouton_9595consigneObjects1= [];
gdjs.jeu_95facileCode.GDsp_9595bouton_9595consigneObjects2= [];
gdjs.jeu_95facileCode.GDsp_9595bouton_9595consigneObjects3= [];
gdjs.jeu_95facileCode.GDsp_9595bouton_9595consigneObjects4= [];
gdjs.jeu_95facileCode.GDsp_9595bouton_9595consigneObjects5= [];
gdjs.jeu_95facileCode.GDsp_9595faux_9595vraiObjects1= [];
gdjs.jeu_95facileCode.GDsp_9595faux_9595vraiObjects2= [];
gdjs.jeu_95facileCode.GDsp_9595faux_9595vraiObjects3= [];
gdjs.jeu_95facileCode.GDsp_9595faux_9595vraiObjects4= [];
gdjs.jeu_95facileCode.GDsp_9595faux_9595vraiObjects5= [];
gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects1= [];
gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects2= [];
gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects3= [];
gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects4= [];
gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects5= [];
gdjs.jeu_95facileCode.GDsp_9595bouton_9595retourObjects1= [];
gdjs.jeu_95facileCode.GDsp_9595bouton_9595retourObjects2= [];
gdjs.jeu_95facileCode.GDsp_9595bouton_9595retourObjects3= [];
gdjs.jeu_95facileCode.GDsp_9595bouton_9595retourObjects4= [];
gdjs.jeu_95facileCode.GDsp_9595bouton_9595retourObjects5= [];
gdjs.jeu_95facileCode.GDsp_9595bouton_9595validerObjects1= [];
gdjs.jeu_95facileCode.GDsp_9595bouton_9595validerObjects2= [];
gdjs.jeu_95facileCode.GDsp_9595bouton_9595validerObjects3= [];
gdjs.jeu_95facileCode.GDsp_9595bouton_9595validerObjects4= [];
gdjs.jeu_95facileCode.GDsp_9595bouton_9595validerObjects5= [];
gdjs.jeu_95facileCode.GDsp_9595jetonObjects1= [];
gdjs.jeu_95facileCode.GDsp_9595jetonObjects2= [];
gdjs.jeu_95facileCode.GDsp_9595jetonObjects3= [];
gdjs.jeu_95facileCode.GDsp_9595jetonObjects4= [];
gdjs.jeu_95facileCode.GDsp_9595jetonObjects5= [];
gdjs.jeu_95facileCode.GDsp_9595bouton_9595recommencerObjects1= [];
gdjs.jeu_95facileCode.GDsp_9595bouton_9595recommencerObjects2= [];
gdjs.jeu_95facileCode.GDsp_9595bouton_9595recommencerObjects3= [];
gdjs.jeu_95facileCode.GDsp_9595bouton_9595recommencerObjects4= [];
gdjs.jeu_95facileCode.GDsp_9595bouton_9595recommencerObjects5= [];
gdjs.jeu_95facileCode.GDsp_9595fondObjects1= [];
gdjs.jeu_95facileCode.GDsp_9595fondObjects2= [];
gdjs.jeu_95facileCode.GDsp_9595fondObjects3= [];
gdjs.jeu_95facileCode.GDsp_9595fondObjects4= [];
gdjs.jeu_95facileCode.GDsp_9595fondObjects5= [];


gdjs.jeu_95facileCode.eventsList0 = function(runtimeScene) {

{



}


{


let isConditionTrue_0 = false;
{
{runtimeScene.getScene().getVariables().getFromIndex(1).setNumber(runtimeScene.getGame().getVariables().getFromIndex(2).getAsNumber());
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 1;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("blanc").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("chapeau").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("lunettes").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("tapis").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("collier").setBoolean(false);
}{runtimeScene.getScene().getVariables().getFromIndex(3).getChild("a").setString("blanc");
}{runtimeScene.getScene().getVariables().getFromIndex(3).getChild("b").setString("chapeau");
}{runtimeScene.getScene().getVariables().getFromIndex(3).getChild("c").setString("tapis");
}{runtimeScene.getScene().getVariables().getFromIndex(4).setNumber(4);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 2;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("blanc").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("chapeau").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("lunettes").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("tapis").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("collier").setBoolean(false);
}{runtimeScene.getScene().getVariables().getFromIndex(3).getChild("a").setString("blanc");
}{runtimeScene.getScene().getVariables().getFromIndex(3).getChild("b").setString("lunettes");
}{runtimeScene.getScene().getVariables().getFromIndex(3).getChild("c").setString("tapis");
}{runtimeScene.getScene().getVariables().getFromIndex(4).setNumber(5);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 3;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("blanc").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("chapeau").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("lunettes").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("tapis").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("collier").setBoolean(false);
}{runtimeScene.getScene().getVariables().getFromIndex(3).getChild("a").setString("marron");
}{runtimeScene.getScene().getVariables().getFromIndex(3).getChild("b").setString("lunettes");
}{runtimeScene.getScene().getVariables().getFromIndex(3).getChild("c").setString("tapis");
}{runtimeScene.getScene().getVariables().getFromIndex(4).setNumber(8);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 4;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("blanc").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("lunettes").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("chapeau").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("tapis").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("collier").setBoolean(true);
}{runtimeScene.getScene().getVariables().getFromIndex(3).getChild("a").setString("blanc");
}{runtimeScene.getScene().getVariables().getFromIndex(3).getChild("b").setString("lunettes");
}{runtimeScene.getScene().getVariables().getFromIndex(3).getChild("c").setString("collier");
}{runtimeScene.getScene().getVariables().getFromIndex(4).setNumber(15);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 5;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("blanc").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("chapeau").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("lunettes").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("tapis").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("collier").setBoolean(true);
}{runtimeScene.getScene().getVariables().getFromIndex(3).getChild("a").setString("blanc");
}{runtimeScene.getScene().getVariables().getFromIndex(3).getChild("b").setString("tapis");
}{runtimeScene.getScene().getVariables().getFromIndex(3).getChild("c").setString("collier");
}{runtimeScene.getScene().getVariables().getFromIndex(4).setNumber(16);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 6;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("blanc").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("chapeau").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("lunettes").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("tapis").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("collier").setBoolean(true);
}{runtimeScene.getScene().getVariables().getFromIndex(3).getChild("a").setString("marron");
}{runtimeScene.getScene().getVariables().getFromIndex(3).getChild("b").setString("lunettes");
}{runtimeScene.getScene().getVariables().getFromIndex(3).getChild("c").setString("collier");
}{runtimeScene.getScene().getVariables().getFromIndex(4).setNumber(18);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 7;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("blanc").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("chapeau").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("lunettes").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("tapis").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("collier").setBoolean(true);
}{runtimeScene.getScene().getVariables().getFromIndex(3).getChild("a").setString("blanc");
}{runtimeScene.getScene().getVariables().getFromIndex(3).getChild("b").setString("chapeau");
}{runtimeScene.getScene().getVariables().getFromIndex(3).getChild("c").setString("collier");
}{runtimeScene.getScene().getVariables().getFromIndex(4).setNumber(19);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 8;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("blanc").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("chapeau").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("lunettes").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("tapis").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("collier").setBoolean(true);
}{runtimeScene.getScene().getVariables().getFromIndex(3).getChild("a").setString("marron");
}{runtimeScene.getScene().getVariables().getFromIndex(3).getChild("b").setString("tapis");
}{runtimeScene.getScene().getVariables().getFromIndex(3).getChild("c").setString("collier");
}{runtimeScene.getScene().getVariables().getFromIndex(4).setNumber(20);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 9;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("blanc").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("chapeau").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("lunettes").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("tapis").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("collier").setBoolean(false);
}{runtimeScene.getScene().getVariables().getFromIndex(3).getChild("a").setString("marron");
}{runtimeScene.getScene().getVariables().getFromIndex(3).getChild("b").setString("chapeau");
}{runtimeScene.getScene().getVariables().getFromIndex(3).getChild("c").setString("lunettes");
}{runtimeScene.getScene().getVariables().getFromIndex(4).setNumber(22);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 10;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("blanc").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("chapeau").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("lunettes").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("tapis").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("collier").setBoolean(true);
}{runtimeScene.getScene().getVariables().getFromIndex(3).getChild("a").setString("marron");
}{runtimeScene.getScene().getVariables().getFromIndex(3).getChild("b").setString("chapeau");
}{runtimeScene.getScene().getVariables().getFromIndex(3).getChild("c").setString("collier");
}{runtimeScene.getScene().getVariables().getFromIndex(4).setNumber(25);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 11;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("blanc").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("chapeau").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("lunettes").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("tapis").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("collier").setBoolean(false);
}{runtimeScene.getScene().getVariables().getFromIndex(3).getChild("a").setString("marron");
}{runtimeScene.getScene().getVariables().getFromIndex(3).getChild("b").setString("chapeau");
}{runtimeScene.getScene().getVariables().getFromIndex(3).getChild("c").setString("tapis");
}{runtimeScene.getScene().getVariables().getFromIndex(4).setNumber(26);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 12;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("blanc").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("chapeau").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("lunettes").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("tapis").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("collier").setBoolean(false);
}{runtimeScene.getScene().getVariables().getFromIndex(3).getChild("a").setString("blanc");
}{runtimeScene.getScene().getVariables().getFromIndex(3).getChild("b").setString("chapeau");
}{runtimeScene.getScene().getVariables().getFromIndex(3).getChild("c").setString("lunettes");
}{runtimeScene.getScene().getVariables().getFromIndex(4).setNumber(31);
}}

}


};gdjs.jeu_95facileCode.eventsList1 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
{gdjs.evtsExt__ArrayTools__Shuffle.func(runtimeScene, runtimeScene.getScene().getVariables().getFromIndex(2), (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("sp_bouton_consigne"), gdjs.jeu_95facileCode.GDsp_9595bouton_9595consigneObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_95facileCode.GDsp_9595bouton_9595consigneObjects2.length;i<l;++i) {
    if ( gdjs.jeu_95facileCode.GDsp_9595bouton_9595consigneObjects2[i].getVariableNumber(gdjs.jeu_95facileCode.GDsp_9595bouton_9595consigneObjects2[i].getVariables().getFromIndex(0)) == 1 ) {
        isConditionTrue_0 = true;
        gdjs.jeu_95facileCode.GDsp_9595bouton_9595consigneObjects2[k] = gdjs.jeu_95facileCode.GDsp_9595bouton_9595consigneObjects2[i];
        ++k;
    }
}
gdjs.jeu_95facileCode.GDsp_9595bouton_9595consigneObjects2.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.jeu_95facileCode.GDsp_9595bouton_9595consigneObjects2 */
{for(var i = 0, len = gdjs.jeu_95facileCode.GDsp_9595bouton_9595consigneObjects2.length ;i < len;++i) {
    gdjs.jeu_95facileCode.GDsp_9595bouton_9595consigneObjects2[i].returnVariable(gdjs.jeu_95facileCode.GDsp_9595bouton_9595consigneObjects2[i].getVariables().getFromIndex(1)).setNumber(gdjs.evtsExt__ArrayTools__ShiftNumber.func(runtimeScene, runtimeScene.getScene().getVariables().getFromIndex(2), (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined)));
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("sp_bouton_consigne"), gdjs.jeu_95facileCode.GDsp_9595bouton_9595consigneObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_95facileCode.GDsp_9595bouton_9595consigneObjects2.length;i<l;++i) {
    if ( gdjs.jeu_95facileCode.GDsp_9595bouton_9595consigneObjects2[i].getVariableNumber(gdjs.jeu_95facileCode.GDsp_9595bouton_9595consigneObjects2[i].getVariables().getFromIndex(0)) == 2 ) {
        isConditionTrue_0 = true;
        gdjs.jeu_95facileCode.GDsp_9595bouton_9595consigneObjects2[k] = gdjs.jeu_95facileCode.GDsp_9595bouton_9595consigneObjects2[i];
        ++k;
    }
}
gdjs.jeu_95facileCode.GDsp_9595bouton_9595consigneObjects2.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.jeu_95facileCode.GDsp_9595bouton_9595consigneObjects2 */
{for(var i = 0, len = gdjs.jeu_95facileCode.GDsp_9595bouton_9595consigneObjects2.length ;i < len;++i) {
    gdjs.jeu_95facileCode.GDsp_9595bouton_9595consigneObjects2[i].returnVariable(gdjs.jeu_95facileCode.GDsp_9595bouton_9595consigneObjects2[i].getVariables().getFromIndex(1)).setNumber(gdjs.evtsExt__ArrayTools__ShiftNumber.func(runtimeScene, runtimeScene.getScene().getVariables().getFromIndex(2), (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined)));
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("sp_bouton_consigne"), gdjs.jeu_95facileCode.GDsp_9595bouton_9595consigneObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_95facileCode.GDsp_9595bouton_9595consigneObjects1.length;i<l;++i) {
    if ( gdjs.jeu_95facileCode.GDsp_9595bouton_9595consigneObjects1[i].getVariableNumber(gdjs.jeu_95facileCode.GDsp_9595bouton_9595consigneObjects1[i].getVariables().getFromIndex(0)) == 3 ) {
        isConditionTrue_0 = true;
        gdjs.jeu_95facileCode.GDsp_9595bouton_9595consigneObjects1[k] = gdjs.jeu_95facileCode.GDsp_9595bouton_9595consigneObjects1[i];
        ++k;
    }
}
gdjs.jeu_95facileCode.GDsp_9595bouton_9595consigneObjects1.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.jeu_95facileCode.GDsp_9595bouton_9595consigneObjects1 */
{for(var i = 0, len = gdjs.jeu_95facileCode.GDsp_9595bouton_9595consigneObjects1.length ;i < len;++i) {
    gdjs.jeu_95facileCode.GDsp_9595bouton_9595consigneObjects1[i].returnVariable(gdjs.jeu_95facileCode.GDsp_9595bouton_9595consigneObjects1[i].getVariables().getFromIndex(1)).setNumber(gdjs.evtsExt__ArrayTools__ShiftNumber.func(runtimeScene, runtimeScene.getScene().getVariables().getFromIndex(2), (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined)));
}
}}

}


};gdjs.jeu_95facileCode.eventsList2 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
}

}


{


gdjs.jeu_95facileCode.eventsList0(runtimeScene);
}


{



}


{


gdjs.jeu_95facileCode.eventsList1(runtimeScene);
}


};gdjs.jeu_95facileCode.mapOfGDgdjs_9546jeu_959595facileCode_9546GDsp_95959595carte_95959595lamaObjects1Objects = Hashtable.newFrom({"sp_carte_lama": gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects1});
gdjs.jeu_95facileCode.mapOfGDgdjs_9546jeu_959595facileCode_9546GDsp_95959595jetonObjects1Objects = Hashtable.newFrom({"sp_jeton": gdjs.jeu_95facileCode.GDsp_9595jetonObjects1});
gdjs.jeu_95facileCode.mapOfGDgdjs_9546jeu_959595facileCode_9546GDsp_95959595carte_95959595lamaObjects1Objects = Hashtable.newFrom({"sp_carte_lama": gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects1});
gdjs.jeu_95facileCode.eventsList3 = function(runtimeScene) {

{

gdjs.copyArray(gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects1, gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects2);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects2.length;i<l;++i) {
    if ( gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects2[i].getVariableBoolean(gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects2[i].getVariables().getFromIndex(1), false, false) ) {
        isConditionTrue_0 = true;
        gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects2[k] = gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects2[i];
        ++k;
    }
}
gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects2.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects2 */
{for(var i = 0, len = gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects2.length ;i < len;++i) {
    gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects2[i].setColor("53;53;53");
}
}}

}


{

/* Reuse gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects1 */

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects1.length;i<l;++i) {
    if ( gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects1[i].getVariableBoolean(gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects1[i].getVariables().getFromIndex(1), true, false) ) {
        isConditionTrue_0 = true;
        gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects1[k] = gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects1[i];
        ++k;
    }
}
gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects1.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects1 */
{for(var i = 0, len = gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects1.length ;i < len;++i) {
    gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects1[i].setColor("255;255;255");
}
}}

}


};gdjs.jeu_95facileCode.mapOfGDgdjs_9546jeu_959595facileCode_9546GDsp_95959595bouton_95959595recommencerObjects1Objects = Hashtable.newFrom({"sp_bouton_recommencer": gdjs.jeu_95facileCode.GDsp_9595bouton_9595recommencerObjects1});
gdjs.jeu_95facileCode.eventsList4 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("sp_carte_lama"), gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects2.length;i<l;++i) {
    if ( gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects2[i].getVariableBoolean(gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects2[i].getVariables().getFromIndex(1), false, false) ) {
        isConditionTrue_0 = true;
        gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects2[k] = gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects2[i];
        ++k;
    }
}
gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects2.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects2 */
{for(var i = 0, len = gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects2.length ;i < len;++i) {
    gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects2[i].setColor("53;53;53");
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("sp_carte_lama"), gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects1.length;i<l;++i) {
    if ( gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects1[i].getVariableBoolean(gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects1[i].getVariables().getFromIndex(1), true, false) ) {
        isConditionTrue_0 = true;
        gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects1[k] = gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects1[i];
        ++k;
    }
}
gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects1.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects1 */
{for(var i = 0, len = gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects1.length ;i < len;++i) {
    gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects1[i].setColor("255;255;255");
}
}}

}


};gdjs.jeu_95facileCode.mapOfGDgdjs_9546jeu_959595facileCode_9546GDsp_95959595bouton_95959595consigneObjects1Objects = Hashtable.newFrom({"sp_bouton_consigne": gdjs.jeu_95facileCode.GDsp_9595bouton_9595consigneObjects1});
gdjs.jeu_95facileCode.eventsList5 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableString(runtimeScene.getScene().getVariables().getFromIndex(3).getChild("a")) == "blanc";
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/laine_blanche.mp3", 1, false, 100, 1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableString(runtimeScene.getScene().getVariables().getFromIndex(3).getChild("a")) == "marron";
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/laine_marron.mp3", 1, false, 100, 1);
}}

}


};gdjs.jeu_95facileCode.eventsList6 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableString(runtimeScene.getScene().getVariables().getFromIndex(3).getChild("b")) == "chapeau";
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/chapeau_oui.mp3", 1, false, 100, 1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableString(runtimeScene.getScene().getVariables().getFromIndex(3).getChild("b")) == "lunettes";
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/lunettes_oui.mp3", 1, false, 100, 1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableString(runtimeScene.getScene().getVariables().getFromIndex(3).getChild("b")) == "tapis";
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/tapis_oui.mp3", 1, false, 100, 1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableString(runtimeScene.getScene().getVariables().getFromIndex(3).getChild("b")) == "collier";
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/collier_oui.mp3", 1, false, 100, 1);
}}

}


};gdjs.jeu_95facileCode.eventsList7 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableString(runtimeScene.getScene().getVariables().getFromIndex(3).getChild("c")) == "chapeau";
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/chapeau_oui.mp3", 1, false, 100, 1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableString(runtimeScene.getScene().getVariables().getFromIndex(3).getChild("c")) == "lunettes";
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/lunettes_oui.mp3", 1, false, 100, 1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableString(runtimeScene.getScene().getVariables().getFromIndex(3).getChild("c")) == "tapis";
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/tapis_oui.mp3", 1, false, 100, 1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableString(runtimeScene.getScene().getVariables().getFromIndex(3).getChild("c")) == "collier";
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/collier_oui.mp3", 1, false, 100, 1);
}}

}


};gdjs.jeu_95facileCode.eventsList8 = function(runtimeScene) {

{

gdjs.copyArray(gdjs.jeu_95facileCode.GDsp_9595bouton_9595consigneObjects1, gdjs.jeu_95facileCode.GDsp_9595bouton_9595consigneObjects2);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_95facileCode.GDsp_9595bouton_9595consigneObjects2.length;i<l;++i) {
    if ( gdjs.jeu_95facileCode.GDsp_9595bouton_9595consigneObjects2[i].getVariableNumber(gdjs.jeu_95facileCode.GDsp_9595bouton_9595consigneObjects2[i].getVariables().getFromIndex(1)) == 1 ) {
        isConditionTrue_0 = true;
        gdjs.jeu_95facileCode.GDsp_9595bouton_9595consigneObjects2[k] = gdjs.jeu_95facileCode.GDsp_9595bouton_9595consigneObjects2[i];
        ++k;
    }
}
gdjs.jeu_95facileCode.GDsp_9595bouton_9595consigneObjects2.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.jeu_95facileCode.GDsp_9595bouton_9595consigneObjects2 */
{for(var i = 0, len = gdjs.jeu_95facileCode.GDsp_9595bouton_9595consigneObjects2.length ;i < len;++i) {
    gdjs.jeu_95facileCode.GDsp_9595bouton_9595consigneObjects2[i].getBehavior("Opacity").setOpacity(100);
}
}
{ //Subevents
gdjs.jeu_95facileCode.eventsList5(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(gdjs.jeu_95facileCode.GDsp_9595bouton_9595consigneObjects1, gdjs.jeu_95facileCode.GDsp_9595bouton_9595consigneObjects2);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_95facileCode.GDsp_9595bouton_9595consigneObjects2.length;i<l;++i) {
    if ( gdjs.jeu_95facileCode.GDsp_9595bouton_9595consigneObjects2[i].getVariableNumber(gdjs.jeu_95facileCode.GDsp_9595bouton_9595consigneObjects2[i].getVariables().getFromIndex(1)) == 2 ) {
        isConditionTrue_0 = true;
        gdjs.jeu_95facileCode.GDsp_9595bouton_9595consigneObjects2[k] = gdjs.jeu_95facileCode.GDsp_9595bouton_9595consigneObjects2[i];
        ++k;
    }
}
gdjs.jeu_95facileCode.GDsp_9595bouton_9595consigneObjects2.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.jeu_95facileCode.GDsp_9595bouton_9595consigneObjects2 */
{for(var i = 0, len = gdjs.jeu_95facileCode.GDsp_9595bouton_9595consigneObjects2.length ;i < len;++i) {
    gdjs.jeu_95facileCode.GDsp_9595bouton_9595consigneObjects2[i].getBehavior("Opacity").setOpacity(100);
}
}
{ //Subevents
gdjs.jeu_95facileCode.eventsList6(runtimeScene);} //End of subevents
}

}


{

/* Reuse gdjs.jeu_95facileCode.GDsp_9595bouton_9595consigneObjects1 */

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_95facileCode.GDsp_9595bouton_9595consigneObjects1.length;i<l;++i) {
    if ( gdjs.jeu_95facileCode.GDsp_9595bouton_9595consigneObjects1[i].getVariableNumber(gdjs.jeu_95facileCode.GDsp_9595bouton_9595consigneObjects1[i].getVariables().getFromIndex(1)) == 3 ) {
        isConditionTrue_0 = true;
        gdjs.jeu_95facileCode.GDsp_9595bouton_9595consigneObjects1[k] = gdjs.jeu_95facileCode.GDsp_9595bouton_9595consigneObjects1[i];
        ++k;
    }
}
gdjs.jeu_95facileCode.GDsp_9595bouton_9595consigneObjects1.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.jeu_95facileCode.GDsp_9595bouton_9595consigneObjects1 */
{for(var i = 0, len = gdjs.jeu_95facileCode.GDsp_9595bouton_9595consigneObjects1.length ;i < len;++i) {
    gdjs.jeu_95facileCode.GDsp_9595bouton_9595consigneObjects1[i].getBehavior("Opacity").setOpacity(100);
}
}
{ //Subevents
gdjs.jeu_95facileCode.eventsList7(runtimeScene);} //End of subevents
}

}


};gdjs.jeu_95facileCode.eventsList9 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("sp_bouton_consigne"), gdjs.jeu_95facileCode.GDsp_9595bouton_9595consigneObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.jeu_95facileCode.mapOfGDgdjs_9546jeu_959595facileCode_9546GDsp_95959595bouton_95959595consigneObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {

{ //Subevents
gdjs.jeu_95facileCode.eventsList8(runtimeScene);} //End of subevents
}

}


};gdjs.jeu_95facileCode.mapOfGDgdjs_9546jeu_959595facileCode_9546GDsp_95959595bouton_95959595validerObjects1Objects = Hashtable.newFrom({"sp_bouton_valider": gdjs.jeu_95facileCode.GDsp_9595bouton_9595validerObjects1});
gdjs.jeu_95facileCode.mapOfGDgdjs_9546jeu_959595facileCode_9546GDsp_95959595jetonObjects1Objects = Hashtable.newFrom({"sp_jeton": gdjs.jeu_95facileCode.GDsp_9595jetonObjects1});
gdjs.jeu_95facileCode.mapOfGDgdjs_9546jeu_959595facileCode_9546GDsp_95959595carte_95959595lamaObjects1Objects = Hashtable.newFrom({"sp_carte_lama": gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects1});
gdjs.jeu_95facileCode.eventsList10 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) == 1;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(3).getChild("scores2").getChild("niv1").setNumber(1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) == 2;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(3).getChild("scores2").getChild("niv2").setNumber(1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) == 3;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(3).getChild("scores2").getChild("niv3").setNumber(1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) == 4;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(3).getChild("scores2").getChild("niv4").setNumber(1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) == 5;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(3).getChild("scores2").getChild("niv5").setNumber(1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) == 6;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(3).getChild("scores2").getChild("niv6").setNumber(1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) == 7;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(3).getChild("scores2").getChild("niv7").setNumber(1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) == 8;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(3).getChild("scores2").getChild("niv8").setNumber(1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) == 9;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(3).getChild("scores2").getChild("niv9").setNumber(1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) == 10;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(3).getChild("scores2").getChild("niv10").setNumber(1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) == 11;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(3).getChild("scores2").getChild("niv11").setNumber(1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) == 12;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(3).getChild("scores2").getChild("niv12").setNumber(1);
}}

}


};gdjs.jeu_95facileCode.eventsList11 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
{gdjs.evtTools.storage.clearJSONFile("sauvegarde_quelestlelama");
}{gdjs.evtTools.storage.writeStringInJSONFile("sauvegarde_quelestlelama", "reglages", gdjs.evtTools.network.variableStructureToJSON(runtimeScene.getGame().getVariables().getFromIndex(3)));
}}

}


};gdjs.jeu_95facileCode.eventsList12 = function(runtimeScene) {

{


gdjs.jeu_95facileCode.eventsList10(runtimeScene);
}


{


gdjs.jeu_95facileCode.eventsList11(runtimeScene);
}


};gdjs.jeu_95facileCode.eventsList13 = function(runtimeScene) {

{

gdjs.copyArray(gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects1, gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects2);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects2.length;i<l;++i) {
    if ( gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects2[i].getVariableNumber(gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects2[i].getVariables().getFromIndex(2)) == runtimeScene.getScene().getVariables().getFromIndex(4).getAsNumber() ) {
        isConditionTrue_0 = true;
        gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects2[k] = gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects2[i];
        ++k;
    }
}
gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects2.length = k;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("sp_bouton_recommencer"), gdjs.jeu_95facileCode.GDsp_9595bouton_9595recommencerObjects2);
gdjs.copyArray(runtimeScene.getObjects("sp_bouton_retour"), gdjs.jeu_95facileCode.GDsp_9595bouton_9595retourObjects2);
gdjs.copyArray(gdjs.jeu_95facileCode.GDsp_9595bouton_9595validerObjects1, gdjs.jeu_95facileCode.GDsp_9595bouton_9595validerObjects2);

gdjs.copyArray(runtimeScene.getObjects("sp_faux_vrai"), gdjs.jeu_95facileCode.GDsp_9595faux_9595vraiObjects2);
gdjs.copyArray(gdjs.jeu_95facileCode.GDsp_9595jetonObjects1, gdjs.jeu_95facileCode.GDsp_9595jetonObjects2);

{for(var i = 0, len = gdjs.jeu_95facileCode.GDsp_9595faux_9595vraiObjects2.length ;i < len;++i) {
    gdjs.jeu_95facileCode.GDsp_9595faux_9595vraiObjects2[i].getBehavior("Animation").setAnimationName("vrai");
}
}{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/vrai.mp3", 1, false, 100, 1);
}{for(var i = 0, len = gdjs.jeu_95facileCode.GDsp_9595bouton_9595validerObjects2.length ;i < len;++i) {
    gdjs.jeu_95facileCode.GDsp_9595bouton_9595validerObjects2[i].deleteFromScene(runtimeScene);
}
}{for(var i = 0, len = gdjs.jeu_95facileCode.GDsp_9595jetonObjects2.length ;i < len;++i) {
    gdjs.jeu_95facileCode.GDsp_9595jetonObjects2[i].activateBehavior("Déplaçable", false);
}
}{for(var i = 0, len = gdjs.jeu_95facileCode.GDsp_9595bouton_9595recommencerObjects2.length ;i < len;++i) {
    gdjs.jeu_95facileCode.GDsp_9595bouton_9595recommencerObjects2[i].getBehavior("Animation").resumeAnimation();
}
}{for(var i = 0, len = gdjs.jeu_95facileCode.GDsp_9595bouton_9595retourObjects2.length ;i < len;++i) {
    gdjs.jeu_95facileCode.GDsp_9595bouton_9595retourObjects2[i].getBehavior("Animation").resumeAnimation();
}
}
{ //Subevents
gdjs.jeu_95facileCode.eventsList12(runtimeScene);} //End of subevents
}

}


{

/* Reuse gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects1 */

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects1.length;i<l;++i) {
    if ( gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects1[i].getVariableNumber(gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects1[i].getVariables().getFromIndex(2)) != runtimeScene.getScene().getVariables().getFromIndex(4).getAsNumber() ) {
        isConditionTrue_0 = true;
        gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects1[k] = gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects1[i];
        ++k;
    }
}
gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects1.length = k;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("sp_faux_vrai"), gdjs.jeu_95facileCode.GDsp_9595faux_9595vraiObjects1);
{for(var i = 0, len = gdjs.jeu_95facileCode.GDsp_9595faux_9595vraiObjects1.length ;i < len;++i) {
    gdjs.jeu_95facileCode.GDsp_9595faux_9595vraiObjects1[i].getBehavior("Animation").setAnimationName("faux");
}
}{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/faux.mp3", 1, false, 100, 1);
}}

}


};gdjs.jeu_95facileCode.eventsList14 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("sp_faux_vrai"), gdjs.jeu_95facileCode.GDsp_9595faux_9595vraiObjects2);
{for(var i = 0, len = gdjs.jeu_95facileCode.GDsp_9595faux_9595vraiObjects2.length ;i < len;++i) {
    gdjs.jeu_95facileCode.GDsp_9595faux_9595vraiObjects2[i].hide(false);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("sp_carte_lama"), gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects1);
gdjs.copyArray(runtimeScene.getObjects("sp_jeton"), gdjs.jeu_95facileCode.GDsp_9595jetonObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.jeu_95facileCode.mapOfGDgdjs_9546jeu_959595facileCode_9546GDsp_95959595jetonObjects1Objects, gdjs.jeu_95facileCode.mapOfGDgdjs_9546jeu_959595facileCode_9546GDsp_95959595carte_95959595lamaObjects1Objects, false, runtimeScene, false);
if (isConditionTrue_0) {

{ //Subevents
gdjs.jeu_95facileCode.eventsList13(runtimeScene);} //End of subevents
}

}


};gdjs.jeu_95facileCode.mapOfGDgdjs_9546jeu_959595facileCode_9546GDsp_95959595bouton_95959595retourObjects1Objects = Hashtable.newFrom({"sp_bouton_retour": gdjs.jeu_95facileCode.GDsp_9595bouton_9595retourObjects1});
gdjs.jeu_95facileCode.mapOfGDgdjs_9546jeu_959595facileCode_9546GDsp_95959595jetonObjects2Objects = Hashtable.newFrom({"sp_jeton": gdjs.jeu_95facileCode.GDsp_9595jetonObjects2});
gdjs.jeu_95facileCode.mapOfGDgdjs_9546jeu_959595facileCode_9546GDsp_95959595carte_95959595lamaObjects2Objects = Hashtable.newFrom({"sp_carte_lama": gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects2});
gdjs.jeu_95facileCode.mapOfGDgdjs_9546jeu_959595facileCode_9546GDsp_95959595jetonObjects1Objects = Hashtable.newFrom({"sp_jeton": gdjs.jeu_95facileCode.GDsp_9595jetonObjects1});
gdjs.jeu_95facileCode.mapOfGDgdjs_9546jeu_959595facileCode_9546GDsp_95959595carte_95959595lamaObjects1Objects = Hashtable.newFrom({"sp_carte_lama": gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects1});
gdjs.jeu_95facileCode.eventsList15 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("sp_bouton_valider"), gdjs.jeu_95facileCode.GDsp_9595bouton_9595validerObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_95facileCode.GDsp_9595bouton_9595validerObjects2.length;i<l;++i) {
    if ( gdjs.jeu_95facileCode.GDsp_9595bouton_9595validerObjects2[i].getVariableBoolean(gdjs.jeu_95facileCode.GDsp_9595bouton_9595validerObjects2[i].getVariables().getFromIndex(0), false, false) ) {
        isConditionTrue_0 = true;
        gdjs.jeu_95facileCode.GDsp_9595bouton_9595validerObjects2[k] = gdjs.jeu_95facileCode.GDsp_9595bouton_9595validerObjects2[i];
        ++k;
    }
}
gdjs.jeu_95facileCode.GDsp_9595bouton_9595validerObjects2.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.jeu_95facileCode.GDsp_9595bouton_9595validerObjects2 */
{for(var i = 0, len = gdjs.jeu_95facileCode.GDsp_9595bouton_9595validerObjects2.length ;i < len;++i) {
    gdjs.jeu_95facileCode.GDsp_9595bouton_9595validerObjects2[i].getBehavior("Opacity").setOpacity(100);
}
}{for(var i = 0, len = gdjs.jeu_95facileCode.GDsp_9595bouton_9595validerObjects2.length ;i < len;++i) {
    gdjs.jeu_95facileCode.GDsp_9595bouton_9595validerObjects2[i].getBehavior("Animation").pauseAnimation();
}
}{for(var i = 0, len = gdjs.jeu_95facileCode.GDsp_9595bouton_9595validerObjects2.length ;i < len;++i) {
    gdjs.jeu_95facileCode.GDsp_9595bouton_9595validerObjects2[i].setAnimationFrame(0);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("sp_bouton_valider"), gdjs.jeu_95facileCode.GDsp_9595bouton_9595validerObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_95facileCode.GDsp_9595bouton_9595validerObjects2.length;i<l;++i) {
    if ( gdjs.jeu_95facileCode.GDsp_9595bouton_9595validerObjects2[i].getVariableBoolean(gdjs.jeu_95facileCode.GDsp_9595bouton_9595validerObjects2[i].getVariables().getFromIndex(0), true, false) ) {
        isConditionTrue_0 = true;
        gdjs.jeu_95facileCode.GDsp_9595bouton_9595validerObjects2[k] = gdjs.jeu_95facileCode.GDsp_9595bouton_9595validerObjects2[i];
        ++k;
    }
}
gdjs.jeu_95facileCode.GDsp_9595bouton_9595validerObjects2.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.jeu_95facileCode.GDsp_9595bouton_9595validerObjects2 */
{for(var i = 0, len = gdjs.jeu_95facileCode.GDsp_9595bouton_9595validerObjects2.length ;i < len;++i) {
    gdjs.jeu_95facileCode.GDsp_9595bouton_9595validerObjects2[i].getBehavior("Opacity").setOpacity(255);
}
}{for(var i = 0, len = gdjs.jeu_95facileCode.GDsp_9595bouton_9595validerObjects2.length ;i < len;++i) {
    gdjs.jeu_95facileCode.GDsp_9595bouton_9595validerObjects2[i].getBehavior("Animation").resumeAnimation();
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("sp_carte_lama"), gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects2);
gdjs.copyArray(runtimeScene.getObjects("sp_jeton"), gdjs.jeu_95facileCode.GDsp_9595jetonObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.jeu_95facileCode.mapOfGDgdjs_9546jeu_959595facileCode_9546GDsp_95959595jetonObjects2Objects, gdjs.jeu_95facileCode.mapOfGDgdjs_9546jeu_959595facileCode_9546GDsp_95959595carte_95959595lamaObjects2Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(14645940);
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("sp_bouton_valider"), gdjs.jeu_95facileCode.GDsp_9595bouton_9595validerObjects2);
{for(var i = 0, len = gdjs.jeu_95facileCode.GDsp_9595bouton_9595validerObjects2.length ;i < len;++i) {
    gdjs.jeu_95facileCode.GDsp_9595bouton_9595validerObjects2[i].returnVariable(gdjs.jeu_95facileCode.GDsp_9595bouton_9595validerObjects2[i].getVariables().getFromIndex(0)).setBoolean(true);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("sp_carte_lama"), gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects1);
gdjs.copyArray(runtimeScene.getObjects("sp_jeton"), gdjs.jeu_95facileCode.GDsp_9595jetonObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.jeu_95facileCode.mapOfGDgdjs_9546jeu_959595facileCode_9546GDsp_95959595jetonObjects1Objects, gdjs.jeu_95facileCode.mapOfGDgdjs_9546jeu_959595facileCode_9546GDsp_95959595carte_95959595lamaObjects1Objects, true, runtimeScene, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(14647156);
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("sp_bouton_valider"), gdjs.jeu_95facileCode.GDsp_9595bouton_9595validerObjects1);
{for(var i = 0, len = gdjs.jeu_95facileCode.GDsp_9595bouton_9595validerObjects1.length ;i < len;++i) {
    gdjs.jeu_95facileCode.GDsp_9595bouton_9595validerObjects1[i].returnVariable(gdjs.jeu_95facileCode.GDsp_9595bouton_9595validerObjects1[i].getVariables().getFromIndex(0)).setBoolean(false);
}
}}

}


};gdjs.jeu_95facileCode.eventsList16 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("sp_bouton_recommencer"), gdjs.jeu_95facileCode.GDsp_9595bouton_9595recommencerObjects1);
gdjs.copyArray(runtimeScene.getObjects("sp_bouton_retour"), gdjs.jeu_95facileCode.GDsp_9595bouton_9595retourObjects1);
gdjs.copyArray(runtimeScene.getObjects("sp_bouton_valider"), gdjs.jeu_95facileCode.GDsp_9595bouton_9595validerObjects1);
gdjs.copyArray(runtimeScene.getObjects("sp_carte_lama"), gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects1);
gdjs.copyArray(runtimeScene.getObjects("sp_faux_vrai"), gdjs.jeu_95facileCode.GDsp_9595faux_9595vraiObjects1);
gdjs.copyArray(runtimeScene.getObjects("sp_fond"), gdjs.jeu_95facileCode.GDsp_9595fondObjects1);
{for(var i = 0, len = gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects1.length ;i < len;++i) {
    gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects1[i].getBehavior("Animation").pauseAnimation();
}
}{for(var i = 0, len = gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects1.length ;i < len;++i) {
    gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects1[i].setAnimationFrame(gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects1[i].getVariables().getFromIndex(0).getAsNumber());
}
}{for(var i = 0, len = gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects1.length ;i < len;++i) {
    gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects1[i].getBehavior("Effect").enableEffect("Effect", false);
}
}{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/question.mp3", 1, false, 100, 1);
}{for(var i = 0, len = gdjs.jeu_95facileCode.GDsp_9595faux_9595vraiObjects1.length ;i < len;++i) {
    gdjs.jeu_95facileCode.GDsp_9595faux_9595vraiObjects1[i].hide();
}
}{for(var i = 0, len = gdjs.jeu_95facileCode.GDsp_9595bouton_9595validerObjects1.length ;i < len;++i) {
    gdjs.jeu_95facileCode.GDsp_9595bouton_9595validerObjects1[i].returnVariable(gdjs.jeu_95facileCode.GDsp_9595bouton_9595validerObjects1[i].getVariables().getFromIndex(0)).setBoolean(false);
}
}{for(var i = 0, len = gdjs.jeu_95facileCode.GDsp_9595bouton_9595recommencerObjects1.length ;i < len;++i) {
    gdjs.jeu_95facileCode.GDsp_9595bouton_9595recommencerObjects1[i].getBehavior("Animation").pauseAnimation();
}
}{for(var i = 0, len = gdjs.jeu_95facileCode.GDsp_9595fondObjects1.length ;i < len;++i) {
    gdjs.jeu_95facileCode.GDsp_9595fondObjects1[i].getBehavior("Animation").setAnimationName("jeu");
}
}{for(var i = 0, len = gdjs.jeu_95facileCode.GDsp_9595fondObjects1.length ;i < len;++i) {
    gdjs.jeu_95facileCode.GDsp_9595fondObjects1[i].getBehavior("Opacity").setOpacity(100);
}
}{for(var i = 0, len = gdjs.jeu_95facileCode.GDsp_9595bouton_9595retourObjects1.length ;i < len;++i) {
    gdjs.jeu_95facileCode.GDsp_9595bouton_9595retourObjects1[i].getBehavior("Animation").pauseAnimation();
}
}
{ //Subevents
gdjs.jeu_95facileCode.eventsList2(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("sp_carte_lama"), gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects1);
gdjs.copyArray(runtimeScene.getObjects("sp_jeton"), gdjs.jeu_95facileCode.GDsp_9595jetonObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.jeu_95facileCode.mapOfGDgdjs_9546jeu_959595facileCode_9546GDsp_95959595carte_95959595lamaObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.jeu_95facileCode.mapOfGDgdjs_9546jeu_959595facileCode_9546GDsp_95959595jetonObjects1Objects, gdjs.jeu_95facileCode.mapOfGDgdjs_9546jeu_959595facileCode_9546GDsp_95959595carte_95959595lamaObjects1Objects, true, runtimeScene, false);
}
}
if (isConditionTrue_0) {
/* Reuse gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects1 */
{runtimeScene.getScene().getVariables().getFromIndex(0).setNumber(1);
}{for(var i = 0, len = gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects1.length ;i < len;++i) {
    gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects1[i].returnVariable(gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects1[i].getVariables().getFromIndex(1)).toggle();
}
}
{ //Subevents
gdjs.jeu_95facileCode.eventsList3(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("sp_bouton_recommencer"), gdjs.jeu_95facileCode.GDsp_9595bouton_9595recommencerObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.jeu_95facileCode.mapOfGDgdjs_9546jeu_959595facileCode_9546GDsp_95959595bouton_95959595recommencerObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "jeu_facile", false);
}
{ //Subevents
gdjs.jeu_95facileCode.eventsList4(runtimeScene);} //End of subevents
}

}


{


gdjs.jeu_95facileCode.eventsList9(runtimeScene);
}


{

gdjs.copyArray(runtimeScene.getObjects("sp_bouton_valider"), gdjs.jeu_95facileCode.GDsp_9595bouton_9595validerObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.jeu_95facileCode.mapOfGDgdjs_9546jeu_959595facileCode_9546GDsp_95959595bouton_95959595validerObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_95facileCode.GDsp_9595bouton_9595validerObjects1.length;i<l;++i) {
    if ( gdjs.jeu_95facileCode.GDsp_9595bouton_9595validerObjects1[i].getVariableBoolean(gdjs.jeu_95facileCode.GDsp_9595bouton_9595validerObjects1[i].getVariables().getFromIndex(0), true, false) ) {
        isConditionTrue_0 = true;
        gdjs.jeu_95facileCode.GDsp_9595bouton_9595validerObjects1[k] = gdjs.jeu_95facileCode.GDsp_9595bouton_9595validerObjects1[i];
        ++k;
    }
}
gdjs.jeu_95facileCode.GDsp_9595bouton_9595validerObjects1.length = k;
}
}
if (isConditionTrue_0) {

{ //Subevents
gdjs.jeu_95facileCode.eventsList14(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("sp_bouton_retour"), gdjs.jeu_95facileCode.GDsp_9595bouton_9595retourObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.jeu_95facileCode.mapOfGDgdjs_9546jeu_959595facileCode_9546GDsp_95959595bouton_95959595retourObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "menu", false);
}}

}


{


gdjs.jeu_95facileCode.eventsList15(runtimeScene);
}


};

gdjs.jeu_95facileCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.jeu_95facileCode.GDtxt_9595consigneObjects1.length = 0;
gdjs.jeu_95facileCode.GDtxt_9595consigneObjects2.length = 0;
gdjs.jeu_95facileCode.GDtxt_9595consigneObjects3.length = 0;
gdjs.jeu_95facileCode.GDtxt_9595consigneObjects4.length = 0;
gdjs.jeu_95facileCode.GDtxt_9595consigneObjects5.length = 0;
gdjs.jeu_95facileCode.GDsp_9595bouton_9595consigneObjects1.length = 0;
gdjs.jeu_95facileCode.GDsp_9595bouton_9595consigneObjects2.length = 0;
gdjs.jeu_95facileCode.GDsp_9595bouton_9595consigneObjects3.length = 0;
gdjs.jeu_95facileCode.GDsp_9595bouton_9595consigneObjects4.length = 0;
gdjs.jeu_95facileCode.GDsp_9595bouton_9595consigneObjects5.length = 0;
gdjs.jeu_95facileCode.GDsp_9595faux_9595vraiObjects1.length = 0;
gdjs.jeu_95facileCode.GDsp_9595faux_9595vraiObjects2.length = 0;
gdjs.jeu_95facileCode.GDsp_9595faux_9595vraiObjects3.length = 0;
gdjs.jeu_95facileCode.GDsp_9595faux_9595vraiObjects4.length = 0;
gdjs.jeu_95facileCode.GDsp_9595faux_9595vraiObjects5.length = 0;
gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects1.length = 0;
gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects2.length = 0;
gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects3.length = 0;
gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects4.length = 0;
gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects5.length = 0;
gdjs.jeu_95facileCode.GDsp_9595bouton_9595retourObjects1.length = 0;
gdjs.jeu_95facileCode.GDsp_9595bouton_9595retourObjects2.length = 0;
gdjs.jeu_95facileCode.GDsp_9595bouton_9595retourObjects3.length = 0;
gdjs.jeu_95facileCode.GDsp_9595bouton_9595retourObjects4.length = 0;
gdjs.jeu_95facileCode.GDsp_9595bouton_9595retourObjects5.length = 0;
gdjs.jeu_95facileCode.GDsp_9595bouton_9595validerObjects1.length = 0;
gdjs.jeu_95facileCode.GDsp_9595bouton_9595validerObjects2.length = 0;
gdjs.jeu_95facileCode.GDsp_9595bouton_9595validerObjects3.length = 0;
gdjs.jeu_95facileCode.GDsp_9595bouton_9595validerObjects4.length = 0;
gdjs.jeu_95facileCode.GDsp_9595bouton_9595validerObjects5.length = 0;
gdjs.jeu_95facileCode.GDsp_9595jetonObjects1.length = 0;
gdjs.jeu_95facileCode.GDsp_9595jetonObjects2.length = 0;
gdjs.jeu_95facileCode.GDsp_9595jetonObjects3.length = 0;
gdjs.jeu_95facileCode.GDsp_9595jetonObjects4.length = 0;
gdjs.jeu_95facileCode.GDsp_9595jetonObjects5.length = 0;
gdjs.jeu_95facileCode.GDsp_9595bouton_9595recommencerObjects1.length = 0;
gdjs.jeu_95facileCode.GDsp_9595bouton_9595recommencerObjects2.length = 0;
gdjs.jeu_95facileCode.GDsp_9595bouton_9595recommencerObjects3.length = 0;
gdjs.jeu_95facileCode.GDsp_9595bouton_9595recommencerObjects4.length = 0;
gdjs.jeu_95facileCode.GDsp_9595bouton_9595recommencerObjects5.length = 0;
gdjs.jeu_95facileCode.GDsp_9595fondObjects1.length = 0;
gdjs.jeu_95facileCode.GDsp_9595fondObjects2.length = 0;
gdjs.jeu_95facileCode.GDsp_9595fondObjects3.length = 0;
gdjs.jeu_95facileCode.GDsp_9595fondObjects4.length = 0;
gdjs.jeu_95facileCode.GDsp_9595fondObjects5.length = 0;

gdjs.jeu_95facileCode.eventsList16(runtimeScene);
gdjs.jeu_95facileCode.GDtxt_9595consigneObjects1.length = 0;
gdjs.jeu_95facileCode.GDtxt_9595consigneObjects2.length = 0;
gdjs.jeu_95facileCode.GDtxt_9595consigneObjects3.length = 0;
gdjs.jeu_95facileCode.GDtxt_9595consigneObjects4.length = 0;
gdjs.jeu_95facileCode.GDtxt_9595consigneObjects5.length = 0;
gdjs.jeu_95facileCode.GDsp_9595bouton_9595consigneObjects1.length = 0;
gdjs.jeu_95facileCode.GDsp_9595bouton_9595consigneObjects2.length = 0;
gdjs.jeu_95facileCode.GDsp_9595bouton_9595consigneObjects3.length = 0;
gdjs.jeu_95facileCode.GDsp_9595bouton_9595consigneObjects4.length = 0;
gdjs.jeu_95facileCode.GDsp_9595bouton_9595consigneObjects5.length = 0;
gdjs.jeu_95facileCode.GDsp_9595faux_9595vraiObjects1.length = 0;
gdjs.jeu_95facileCode.GDsp_9595faux_9595vraiObjects2.length = 0;
gdjs.jeu_95facileCode.GDsp_9595faux_9595vraiObjects3.length = 0;
gdjs.jeu_95facileCode.GDsp_9595faux_9595vraiObjects4.length = 0;
gdjs.jeu_95facileCode.GDsp_9595faux_9595vraiObjects5.length = 0;
gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects1.length = 0;
gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects2.length = 0;
gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects3.length = 0;
gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects4.length = 0;
gdjs.jeu_95facileCode.GDsp_9595carte_9595lamaObjects5.length = 0;
gdjs.jeu_95facileCode.GDsp_9595bouton_9595retourObjects1.length = 0;
gdjs.jeu_95facileCode.GDsp_9595bouton_9595retourObjects2.length = 0;
gdjs.jeu_95facileCode.GDsp_9595bouton_9595retourObjects3.length = 0;
gdjs.jeu_95facileCode.GDsp_9595bouton_9595retourObjects4.length = 0;
gdjs.jeu_95facileCode.GDsp_9595bouton_9595retourObjects5.length = 0;
gdjs.jeu_95facileCode.GDsp_9595bouton_9595validerObjects1.length = 0;
gdjs.jeu_95facileCode.GDsp_9595bouton_9595validerObjects2.length = 0;
gdjs.jeu_95facileCode.GDsp_9595bouton_9595validerObjects3.length = 0;
gdjs.jeu_95facileCode.GDsp_9595bouton_9595validerObjects4.length = 0;
gdjs.jeu_95facileCode.GDsp_9595bouton_9595validerObjects5.length = 0;
gdjs.jeu_95facileCode.GDsp_9595jetonObjects1.length = 0;
gdjs.jeu_95facileCode.GDsp_9595jetonObjects2.length = 0;
gdjs.jeu_95facileCode.GDsp_9595jetonObjects3.length = 0;
gdjs.jeu_95facileCode.GDsp_9595jetonObjects4.length = 0;
gdjs.jeu_95facileCode.GDsp_9595jetonObjects5.length = 0;
gdjs.jeu_95facileCode.GDsp_9595bouton_9595recommencerObjects1.length = 0;
gdjs.jeu_95facileCode.GDsp_9595bouton_9595recommencerObjects2.length = 0;
gdjs.jeu_95facileCode.GDsp_9595bouton_9595recommencerObjects3.length = 0;
gdjs.jeu_95facileCode.GDsp_9595bouton_9595recommencerObjects4.length = 0;
gdjs.jeu_95facileCode.GDsp_9595bouton_9595recommencerObjects5.length = 0;
gdjs.jeu_95facileCode.GDsp_9595fondObjects1.length = 0;
gdjs.jeu_95facileCode.GDsp_9595fondObjects2.length = 0;
gdjs.jeu_95facileCode.GDsp_9595fondObjects3.length = 0;
gdjs.jeu_95facileCode.GDsp_9595fondObjects4.length = 0;
gdjs.jeu_95facileCode.GDsp_9595fondObjects5.length = 0;


return;

}

gdjs['jeu_95facileCode'] = gdjs.jeu_95facileCode;
