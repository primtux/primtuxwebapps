gdjs.infosCode = {};
gdjs.infosCode.localVariables = [];
gdjs.infosCode.GDbbtxt_9595info1Objects1= [];
gdjs.infosCode.GDbbtxt_9595info1Objects2= [];
gdjs.infosCode.GDsp_9595demo_9595jeuObjects1= [];
gdjs.infosCode.GDsp_9595demo_9595jeuObjects2= [];
gdjs.infosCode.GDsp_9595niveauObjects1= [];
gdjs.infosCode.GDsp_9595niveauObjects2= [];
gdjs.infosCode.GDbbtxt_9595info2Objects1= [];
gdjs.infosCode.GDbbtxt_9595info2Objects2= [];
gdjs.infosCode.GDbbtxt_9595info3Objects1= [];
gdjs.infosCode.GDbbtxt_9595info3Objects2= [];
gdjs.infosCode.GDsp_9595cadreObjects1= [];
gdjs.infosCode.GDsp_9595cadreObjects2= [];
gdjs.infosCode.GDsp_9595serge_9595lamaObjects1= [];
gdjs.infosCode.GDsp_9595serge_9595lamaObjects2= [];
gdjs.infosCode.GDsp_9595carte_9595lamaObjects1= [];
gdjs.infosCode.GDsp_9595carte_9595lamaObjects2= [];
gdjs.infosCode.GDsp_9595bouton_9595retourObjects1= [];
gdjs.infosCode.GDsp_9595bouton_9595retourObjects2= [];
gdjs.infosCode.GDsp_9595bouton_9595validerObjects1= [];
gdjs.infosCode.GDsp_9595bouton_9595validerObjects2= [];
gdjs.infosCode.GDsp_9595jetonObjects1= [];
gdjs.infosCode.GDsp_9595jetonObjects2= [];
gdjs.infosCode.GDsp_9595bouton_9595recommencerObjects1= [];
gdjs.infosCode.GDsp_9595bouton_9595recommencerObjects2= [];
gdjs.infosCode.GDsp_9595fondObjects1= [];
gdjs.infosCode.GDsp_9595fondObjects2= [];


gdjs.infosCode.mapOfGDgdjs_9546infosCode_9546GDsp_95959595bouton_95959595retourObjects1Objects = Hashtable.newFrom({"sp_bouton_retour": gdjs.infosCode.GDsp_9595bouton_9595retourObjects1});
gdjs.infosCode.eventsList0 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("sp_cadre"), gdjs.infosCode.GDsp_9595cadreObjects1);
gdjs.copyArray(runtimeScene.getObjects("sp_fond"), gdjs.infosCode.GDsp_9595fondObjects1);
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "chrono");
}{for(var i = 0, len = gdjs.infosCode.GDsp_9595fondObjects1.length ;i < len;++i) {
    gdjs.infosCode.GDsp_9595fondObjects1[i].getBehavior("Animation").setAnimationIndex(1);
}
}{for(var i = 0, len = gdjs.infosCode.GDsp_9595cadreObjects1.length ;i < len;++i) {
    gdjs.infosCode.GDsp_9595cadreObjects1[i].getBehavior("Opacity").setOpacity(200);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("sp_bouton_retour"), gdjs.infosCode.GDsp_9595bouton_9595retourObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.infosCode.mapOfGDgdjs_9546infosCode_9546GDsp_95959595bouton_95959595retourObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "chrono") >= 0.3;
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "menu", false);
}}

}


};

gdjs.infosCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.infosCode.GDbbtxt_9595info1Objects1.length = 0;
gdjs.infosCode.GDbbtxt_9595info1Objects2.length = 0;
gdjs.infosCode.GDsp_9595demo_9595jeuObjects1.length = 0;
gdjs.infosCode.GDsp_9595demo_9595jeuObjects2.length = 0;
gdjs.infosCode.GDsp_9595niveauObjects1.length = 0;
gdjs.infosCode.GDsp_9595niveauObjects2.length = 0;
gdjs.infosCode.GDbbtxt_9595info2Objects1.length = 0;
gdjs.infosCode.GDbbtxt_9595info2Objects2.length = 0;
gdjs.infosCode.GDbbtxt_9595info3Objects1.length = 0;
gdjs.infosCode.GDbbtxt_9595info3Objects2.length = 0;
gdjs.infosCode.GDsp_9595cadreObjects1.length = 0;
gdjs.infosCode.GDsp_9595cadreObjects2.length = 0;
gdjs.infosCode.GDsp_9595serge_9595lamaObjects1.length = 0;
gdjs.infosCode.GDsp_9595serge_9595lamaObjects2.length = 0;
gdjs.infosCode.GDsp_9595carte_9595lamaObjects1.length = 0;
gdjs.infosCode.GDsp_9595carte_9595lamaObjects2.length = 0;
gdjs.infosCode.GDsp_9595bouton_9595retourObjects1.length = 0;
gdjs.infosCode.GDsp_9595bouton_9595retourObjects2.length = 0;
gdjs.infosCode.GDsp_9595bouton_9595validerObjects1.length = 0;
gdjs.infosCode.GDsp_9595bouton_9595validerObjects2.length = 0;
gdjs.infosCode.GDsp_9595jetonObjects1.length = 0;
gdjs.infosCode.GDsp_9595jetonObjects2.length = 0;
gdjs.infosCode.GDsp_9595bouton_9595recommencerObjects1.length = 0;
gdjs.infosCode.GDsp_9595bouton_9595recommencerObjects2.length = 0;
gdjs.infosCode.GDsp_9595fondObjects1.length = 0;
gdjs.infosCode.GDsp_9595fondObjects2.length = 0;

gdjs.infosCode.eventsList0(runtimeScene);
gdjs.infosCode.GDbbtxt_9595info1Objects1.length = 0;
gdjs.infosCode.GDbbtxt_9595info1Objects2.length = 0;
gdjs.infosCode.GDsp_9595demo_9595jeuObjects1.length = 0;
gdjs.infosCode.GDsp_9595demo_9595jeuObjects2.length = 0;
gdjs.infosCode.GDsp_9595niveauObjects1.length = 0;
gdjs.infosCode.GDsp_9595niveauObjects2.length = 0;
gdjs.infosCode.GDbbtxt_9595info2Objects1.length = 0;
gdjs.infosCode.GDbbtxt_9595info2Objects2.length = 0;
gdjs.infosCode.GDbbtxt_9595info3Objects1.length = 0;
gdjs.infosCode.GDbbtxt_9595info3Objects2.length = 0;
gdjs.infosCode.GDsp_9595cadreObjects1.length = 0;
gdjs.infosCode.GDsp_9595cadreObjects2.length = 0;
gdjs.infosCode.GDsp_9595serge_9595lamaObjects1.length = 0;
gdjs.infosCode.GDsp_9595serge_9595lamaObjects2.length = 0;
gdjs.infosCode.GDsp_9595carte_9595lamaObjects1.length = 0;
gdjs.infosCode.GDsp_9595carte_9595lamaObjects2.length = 0;
gdjs.infosCode.GDsp_9595bouton_9595retourObjects1.length = 0;
gdjs.infosCode.GDsp_9595bouton_9595retourObjects2.length = 0;
gdjs.infosCode.GDsp_9595bouton_9595validerObjects1.length = 0;
gdjs.infosCode.GDsp_9595bouton_9595validerObjects2.length = 0;
gdjs.infosCode.GDsp_9595jetonObjects1.length = 0;
gdjs.infosCode.GDsp_9595jetonObjects2.length = 0;
gdjs.infosCode.GDsp_9595bouton_9595recommencerObjects1.length = 0;
gdjs.infosCode.GDsp_9595bouton_9595recommencerObjects2.length = 0;
gdjs.infosCode.GDsp_9595fondObjects1.length = 0;
gdjs.infosCode.GDsp_9595fondObjects2.length = 0;


return;

}

gdjs['infosCode'] = gdjs.infosCode;
