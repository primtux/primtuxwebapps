gdjs.jeuCode = {};
gdjs.jeuCode.localVariables = [];
gdjs.jeuCode.GDtxt_9595consigneObjects1= [];
gdjs.jeuCode.GDtxt_9595consigneObjects2= [];
gdjs.jeuCode.GDtxt_9595consigneObjects3= [];
gdjs.jeuCode.GDtxt_9595consigneObjects4= [];
gdjs.jeuCode.GDtxt_9595consigneObjects5= [];
gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects1= [];
gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2= [];
gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects3= [];
gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects4= [];
gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects5= [];
gdjs.jeuCode.GDsp_9595faux_9595vraiObjects1= [];
gdjs.jeuCode.GDsp_9595faux_9595vraiObjects2= [];
gdjs.jeuCode.GDsp_9595faux_9595vraiObjects3= [];
gdjs.jeuCode.GDsp_9595faux_9595vraiObjects4= [];
gdjs.jeuCode.GDsp_9595faux_9595vraiObjects5= [];
gdjs.jeuCode.GDsp_9595carte_9595lamaObjects1= [];
gdjs.jeuCode.GDsp_9595carte_9595lamaObjects2= [];
gdjs.jeuCode.GDsp_9595carte_9595lamaObjects3= [];
gdjs.jeuCode.GDsp_9595carte_9595lamaObjects4= [];
gdjs.jeuCode.GDsp_9595carte_9595lamaObjects5= [];
gdjs.jeuCode.GDsp_9595bouton_9595retourObjects1= [];
gdjs.jeuCode.GDsp_9595bouton_9595retourObjects2= [];
gdjs.jeuCode.GDsp_9595bouton_9595retourObjects3= [];
gdjs.jeuCode.GDsp_9595bouton_9595retourObjects4= [];
gdjs.jeuCode.GDsp_9595bouton_9595retourObjects5= [];
gdjs.jeuCode.GDsp_9595bouton_9595validerObjects1= [];
gdjs.jeuCode.GDsp_9595bouton_9595validerObjects2= [];
gdjs.jeuCode.GDsp_9595bouton_9595validerObjects3= [];
gdjs.jeuCode.GDsp_9595bouton_9595validerObjects4= [];
gdjs.jeuCode.GDsp_9595bouton_9595validerObjects5= [];
gdjs.jeuCode.GDsp_9595jetonObjects1= [];
gdjs.jeuCode.GDsp_9595jetonObjects2= [];
gdjs.jeuCode.GDsp_9595jetonObjects3= [];
gdjs.jeuCode.GDsp_9595jetonObjects4= [];
gdjs.jeuCode.GDsp_9595jetonObjects5= [];
gdjs.jeuCode.GDsp_9595bouton_9595recommencerObjects1= [];
gdjs.jeuCode.GDsp_9595bouton_9595recommencerObjects2= [];
gdjs.jeuCode.GDsp_9595bouton_9595recommencerObjects3= [];
gdjs.jeuCode.GDsp_9595bouton_9595recommencerObjects4= [];
gdjs.jeuCode.GDsp_9595bouton_9595recommencerObjects5= [];
gdjs.jeuCode.GDsp_9595fondObjects1= [];
gdjs.jeuCode.GDsp_9595fondObjects2= [];
gdjs.jeuCode.GDsp_9595fondObjects3= [];
gdjs.jeuCode.GDsp_9595fondObjects4= [];
gdjs.jeuCode.GDsp_9595fondObjects5= [];


gdjs.jeuCode.eventsList0 = function(runtimeScene) {

{



}


{


let isConditionTrue_0 = false;
{
{runtimeScene.getScene().getVariables().getFromIndex(1).setNumber(runtimeScene.getGame().getVariables().getFromIndex(2).getAsNumber());
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 1;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("blanc").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("chapeau").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("lunettes").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("tapis").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("collier").setBoolean(true);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 2;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("blanc").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("chapeau").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("lunettes").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("tapis").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("collier").setBoolean(false);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 3;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("blanc").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("chapeau").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("lunettes").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("tapis").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("collier").setBoolean(false);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 4;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("blanc").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("chapeau").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("lunettes").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("tapis").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("collier").setBoolean(false);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 5;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("blanc").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("chapeau").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("lunettes").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("tapis").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("collier").setBoolean(false);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 6;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("blanc").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("chapeau").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("tapis").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("lunettes").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("collier").setBoolean(false);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 7;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("blanc").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("chapeau").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("lunettes").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("tapis").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("collier").setBoolean(true);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 8;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("blanc").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("chapeau").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("lunettes").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("tapis").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("collier").setBoolean(false);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 9;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("blanc").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("chapeau").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("lunettes").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("tapis").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("collier").setBoolean(false);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 10;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("blanc").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("chapeau").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("lunettes").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("tapis").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("collier").setBoolean(true);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 11;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("blanc").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("chapeau").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("lunettes").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("tapis").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("collier").setBoolean(true);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 12;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("blanc").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("chapeau").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("lunettes").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("tapis").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("collier").setBoolean(false);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 13;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("blanc").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("chapeau").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("lunettes").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("tapis").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("collier").setBoolean(true);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 14;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("blanc").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("chapeau").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("lunettes").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("tapis").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("collier").setBoolean(false);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 15;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("blanc").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("chapeau").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("lunettes").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("tapis").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("collier").setBoolean(true);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 16;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("blanc").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("chapeau").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("lunettes").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("tapis").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("collier").setBoolean(true);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 17;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("blanc").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("chapeau").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("lunettes").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("tapis").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("collier").setBoolean(false);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 18;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("blanc").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("chapeau").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("lunettes").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("tapis").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("collier").setBoolean(true);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 19;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("blanc").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("chapeau").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("lunettes").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("tapis").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("collier").setBoolean(true);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 20;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("blanc").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("chapeau").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("lunettes").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("tapis").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("collier").setBoolean(true);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 21;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("blanc").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("chapeau").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("lunettes").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("tapis").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("collier").setBoolean(true);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 22;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("blanc").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("chapeau").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("lunettes").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("tapis").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("collier").setBoolean(false);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 23;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("blanc").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("chapeau").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("lunettes").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("tapis").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("collier").setBoolean(true);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 24;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("blanc").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("chapeau").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("lunettes").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("tapis").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("collier").setBoolean(true);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 25;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("blanc").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("chapeau").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("lunettes").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("tapis").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("collier").setBoolean(true);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 26;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("blanc").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("chapeau").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("lunettes").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("tapis").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("collier").setBoolean(false);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 27;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("blanc").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("chapeau").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("lunettes").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("tapis").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("collier").setBoolean(true);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 28;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("blanc").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("chapeau").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("lunettes").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("tapis").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("collier").setBoolean(false);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 29;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("blanc").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("chapeau").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("lunettes").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("tapis").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("collier").setBoolean(false);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 30;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("blanc").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("chapeau").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("lunettes").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("tapis").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("collier").setBoolean(true);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 31;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("blanc").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("chapeau").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("lunettes").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("tapis").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("collier").setBoolean(false);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 32;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("blanc").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("chapeau").setBoolean(true);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("lunettes").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("tapis").setBoolean(false);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("collier").setBoolean(false);
}}

}


{



}


{

gdjs.copyArray(gdjs.jeuCode.GDsp_9595carte_9595lamaObjects1, gdjs.jeuCode.GDsp_9595carte_9595lamaObjects2);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeuCode.GDsp_9595carte_9595lamaObjects2.length;i<l;++i) {
    if ( gdjs.jeuCode.GDsp_9595carte_9595lamaObjects2[i].getVariableNumber(gdjs.jeuCode.GDsp_9595carte_9595lamaObjects2[i].getVariables().getFromIndex(0)) == runtimeScene.getScene().getVariables().getFromIndex(1).getAsNumber() ) {
        isConditionTrue_0 = true;
        gdjs.jeuCode.GDsp_9595carte_9595lamaObjects2[k] = gdjs.jeuCode.GDsp_9595carte_9595lamaObjects2[i];
        ++k;
    }
}
gdjs.jeuCode.GDsp_9595carte_9595lamaObjects2.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.jeuCode.GDsp_9595carte_9595lamaObjects2 */
{for(var i = 0, len = gdjs.jeuCode.GDsp_9595carte_9595lamaObjects2.length ;i < len;++i) {
    gdjs.jeuCode.GDsp_9595carte_9595lamaObjects2[i].returnVariable(gdjs.jeuCode.GDsp_9595carte_9595lamaObjects2[i].getVariables().getFromIndex(1)).setBoolean(true);
}
}}

}


};gdjs.jeuCode.eventsList1 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
{gdjs.evtsExt__ArrayTools__Shuffle.func(runtimeScene, runtimeScene.getScene().getVariables().getFromIndex(2), (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("sp_bouton_consigne"), gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2.length;i<l;++i) {
    if ( gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2[i].getVariableNumber(gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2[i].getVariables().getFromIndex(0)) == 1 ) {
        isConditionTrue_0 = true;
        gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2[k] = gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2[i];
        ++k;
    }
}
gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2 */
{for(var i = 0, len = gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2.length ;i < len;++i) {
    gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2[i].returnVariable(gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2[i].getVariables().getFromIndex(1)).setNumber(gdjs.evtsExt__ArrayTools__ShiftNumber.func(runtimeScene, runtimeScene.getScene().getVariables().getFromIndex(2), (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined)));
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("sp_bouton_consigne"), gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2.length;i<l;++i) {
    if ( gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2[i].getVariableNumber(gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2[i].getVariables().getFromIndex(0)) == 2 ) {
        isConditionTrue_0 = true;
        gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2[k] = gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2[i];
        ++k;
    }
}
gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2 */
{for(var i = 0, len = gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2.length ;i < len;++i) {
    gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2[i].returnVariable(gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2[i].getVariables().getFromIndex(1)).setNumber(gdjs.evtsExt__ArrayTools__ShiftNumber.func(runtimeScene, runtimeScene.getScene().getVariables().getFromIndex(2), (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined)));
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("sp_bouton_consigne"), gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2.length;i<l;++i) {
    if ( gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2[i].getVariableNumber(gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2[i].getVariables().getFromIndex(0)) == 3 ) {
        isConditionTrue_0 = true;
        gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2[k] = gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2[i];
        ++k;
    }
}
gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2 */
{for(var i = 0, len = gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2.length ;i < len;++i) {
    gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2[i].returnVariable(gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2[i].getVariables().getFromIndex(1)).setNumber(gdjs.evtsExt__ArrayTools__ShiftNumber.func(runtimeScene, runtimeScene.getScene().getVariables().getFromIndex(2), (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined)));
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("sp_bouton_consigne"), gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2.length;i<l;++i) {
    if ( gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2[i].getVariableNumber(gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2[i].getVariables().getFromIndex(0)) == 4 ) {
        isConditionTrue_0 = true;
        gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2[k] = gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2[i];
        ++k;
    }
}
gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2 */
{for(var i = 0, len = gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2.length ;i < len;++i) {
    gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2[i].returnVariable(gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2[i].getVariables().getFromIndex(1)).setNumber(gdjs.evtsExt__ArrayTools__ShiftNumber.func(runtimeScene, runtimeScene.getScene().getVariables().getFromIndex(2), (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined)));
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("sp_bouton_consigne"), gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects1.length;i<l;++i) {
    if ( gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects1[i].getVariableNumber(gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects1[i].getVariables().getFromIndex(0)) == 5 ) {
        isConditionTrue_0 = true;
        gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects1[k] = gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects1[i];
        ++k;
    }
}
gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects1.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects1 */
{for(var i = 0, len = gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects1.length ;i < len;++i) {
    gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects1[i].returnVariable(gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects1[i].getVariables().getFromIndex(1)).setNumber(gdjs.evtsExt__ArrayTools__ShiftNumber.func(runtimeScene, runtimeScene.getScene().getVariables().getFromIndex(2), (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined)));
}
}}

}


};gdjs.jeuCode.eventsList2 = function(runtimeScene) {

{


gdjs.jeuCode.eventsList0(runtimeScene);
}


{



}


{


gdjs.jeuCode.eventsList1(runtimeScene);
}


};gdjs.jeuCode.mapOfGDgdjs_9546jeuCode_9546GDsp_95959595carte_95959595lamaObjects1Objects = Hashtable.newFrom({"sp_carte_lama": gdjs.jeuCode.GDsp_9595carte_9595lamaObjects1});
gdjs.jeuCode.mapOfGDgdjs_9546jeuCode_9546GDsp_95959595jetonObjects1Objects = Hashtable.newFrom({"sp_jeton": gdjs.jeuCode.GDsp_9595jetonObjects1});
gdjs.jeuCode.mapOfGDgdjs_9546jeuCode_9546GDsp_95959595carte_95959595lamaObjects1Objects = Hashtable.newFrom({"sp_carte_lama": gdjs.jeuCode.GDsp_9595carte_9595lamaObjects1});
gdjs.jeuCode.eventsList3 = function(runtimeScene) {

{

gdjs.copyArray(gdjs.jeuCode.GDsp_9595carte_9595lamaObjects1, gdjs.jeuCode.GDsp_9595carte_9595lamaObjects2);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeuCode.GDsp_9595carte_9595lamaObjects2.length;i<l;++i) {
    if ( gdjs.jeuCode.GDsp_9595carte_9595lamaObjects2[i].getVariableBoolean(gdjs.jeuCode.GDsp_9595carte_9595lamaObjects2[i].getVariables().getFromIndex(1), false, false) ) {
        isConditionTrue_0 = true;
        gdjs.jeuCode.GDsp_9595carte_9595lamaObjects2[k] = gdjs.jeuCode.GDsp_9595carte_9595lamaObjects2[i];
        ++k;
    }
}
gdjs.jeuCode.GDsp_9595carte_9595lamaObjects2.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.jeuCode.GDsp_9595carte_9595lamaObjects2 */
{for(var i = 0, len = gdjs.jeuCode.GDsp_9595carte_9595lamaObjects2.length ;i < len;++i) {
    gdjs.jeuCode.GDsp_9595carte_9595lamaObjects2[i].setColor("53;53;53");
}
}}

}


{

/* Reuse gdjs.jeuCode.GDsp_9595carte_9595lamaObjects1 */

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeuCode.GDsp_9595carte_9595lamaObjects1.length;i<l;++i) {
    if ( gdjs.jeuCode.GDsp_9595carte_9595lamaObjects1[i].getVariableBoolean(gdjs.jeuCode.GDsp_9595carte_9595lamaObjects1[i].getVariables().getFromIndex(1), true, false) ) {
        isConditionTrue_0 = true;
        gdjs.jeuCode.GDsp_9595carte_9595lamaObjects1[k] = gdjs.jeuCode.GDsp_9595carte_9595lamaObjects1[i];
        ++k;
    }
}
gdjs.jeuCode.GDsp_9595carte_9595lamaObjects1.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.jeuCode.GDsp_9595carte_9595lamaObjects1 */
{for(var i = 0, len = gdjs.jeuCode.GDsp_9595carte_9595lamaObjects1.length ;i < len;++i) {
    gdjs.jeuCode.GDsp_9595carte_9595lamaObjects1[i].setColor("255;255;255");
}
}}

}


};gdjs.jeuCode.mapOfGDgdjs_9546jeuCode_9546GDsp_95959595bouton_95959595recommencerObjects1Objects = Hashtable.newFrom({"sp_bouton_recommencer": gdjs.jeuCode.GDsp_9595bouton_9595recommencerObjects1});
gdjs.jeuCode.eventsList4 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("sp_carte_lama"), gdjs.jeuCode.GDsp_9595carte_9595lamaObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeuCode.GDsp_9595carte_9595lamaObjects2.length;i<l;++i) {
    if ( gdjs.jeuCode.GDsp_9595carte_9595lamaObjects2[i].getVariableBoolean(gdjs.jeuCode.GDsp_9595carte_9595lamaObjects2[i].getVariables().getFromIndex(1), false, false) ) {
        isConditionTrue_0 = true;
        gdjs.jeuCode.GDsp_9595carte_9595lamaObjects2[k] = gdjs.jeuCode.GDsp_9595carte_9595lamaObjects2[i];
        ++k;
    }
}
gdjs.jeuCode.GDsp_9595carte_9595lamaObjects2.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.jeuCode.GDsp_9595carte_9595lamaObjects2 */
{for(var i = 0, len = gdjs.jeuCode.GDsp_9595carte_9595lamaObjects2.length ;i < len;++i) {
    gdjs.jeuCode.GDsp_9595carte_9595lamaObjects2[i].setColor("53;53;53");
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("sp_carte_lama"), gdjs.jeuCode.GDsp_9595carte_9595lamaObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeuCode.GDsp_9595carte_9595lamaObjects1.length;i<l;++i) {
    if ( gdjs.jeuCode.GDsp_9595carte_9595lamaObjects1[i].getVariableBoolean(gdjs.jeuCode.GDsp_9595carte_9595lamaObjects1[i].getVariables().getFromIndex(1), true, false) ) {
        isConditionTrue_0 = true;
        gdjs.jeuCode.GDsp_9595carte_9595lamaObjects1[k] = gdjs.jeuCode.GDsp_9595carte_9595lamaObjects1[i];
        ++k;
    }
}
gdjs.jeuCode.GDsp_9595carte_9595lamaObjects1.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.jeuCode.GDsp_9595carte_9595lamaObjects1 */
{for(var i = 0, len = gdjs.jeuCode.GDsp_9595carte_9595lamaObjects1.length ;i < len;++i) {
    gdjs.jeuCode.GDsp_9595carte_9595lamaObjects1[i].setColor("255;255;255");
}
}}

}


};gdjs.jeuCode.mapOfGDgdjs_9546jeuCode_9546GDsp_95959595bouton_95959595consigneObjects1Objects = Hashtable.newFrom({"sp_bouton_consigne": gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects1});
gdjs.jeuCode.eventsList5 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableBoolean(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("blanc"), true, false);
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/laine_blanche.mp3", 1, false, 100, 1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableBoolean(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("blanc"), false, false);
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/laine_marron.mp3", 1, false, 100, 1);
}}

}


};gdjs.jeuCode.eventsList6 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableBoolean(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("chapeau"), true, false);
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/chapeau_oui.mp3", 1, false, 100, 1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableBoolean(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("chapeau"), false, false);
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/chapeau_non.mp3", 1, false, 100, 1);
}}

}


};gdjs.jeuCode.eventsList7 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableBoolean(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("lunettes"), true, false);
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/lunettes_oui.mp3", 1, false, 100, 1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableBoolean(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("lunettes"), false, false);
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/lunettes_non.mp3", 1, false, 100, 1);
}}

}


};gdjs.jeuCode.eventsList8 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableBoolean(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("tapis"), true, false);
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/tapis_oui.mp3", 1, false, 100, 1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableBoolean(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("tapis"), false, false);
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/tapis_non.mp3", 1, false, 100, 1);
}}

}


};gdjs.jeuCode.eventsList9 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableBoolean(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("collier"), true, false);
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/collier_oui.mp3", 1, false, 100, 1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableBoolean(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").getChild("collier"), false, false);
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/collier_non.mp3", 1, false, 100, 1);
}}

}


};gdjs.jeuCode.eventsList10 = function(runtimeScene) {

{

gdjs.copyArray(gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects1, gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2.length;i<l;++i) {
    if ( gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2[i].getVariableNumber(gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2[i].getVariables().getFromIndex(1)) == 1 ) {
        isConditionTrue_0 = true;
        gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2[k] = gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2[i];
        ++k;
    }
}
gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2 */
{for(var i = 0, len = gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2.length ;i < len;++i) {
    gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2[i].getBehavior("Opacity").setOpacity(100);
}
}
{ //Subevents
gdjs.jeuCode.eventsList5(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects1, gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2.length;i<l;++i) {
    if ( gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2[i].getVariableNumber(gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2[i].getVariables().getFromIndex(1)) == 2 ) {
        isConditionTrue_0 = true;
        gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2[k] = gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2[i];
        ++k;
    }
}
gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2 */
{for(var i = 0, len = gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2.length ;i < len;++i) {
    gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2[i].getBehavior("Opacity").setOpacity(100);
}
}
{ //Subevents
gdjs.jeuCode.eventsList6(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects1, gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2.length;i<l;++i) {
    if ( gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2[i].getVariableNumber(gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2[i].getVariables().getFromIndex(1)) == 3 ) {
        isConditionTrue_0 = true;
        gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2[k] = gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2[i];
        ++k;
    }
}
gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2 */
{for(var i = 0, len = gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2.length ;i < len;++i) {
    gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2[i].getBehavior("Opacity").setOpacity(100);
}
}
{ //Subevents
gdjs.jeuCode.eventsList7(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects1, gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2.length;i<l;++i) {
    if ( gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2[i].getVariableNumber(gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2[i].getVariables().getFromIndex(1)) == 4 ) {
        isConditionTrue_0 = true;
        gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2[k] = gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2[i];
        ++k;
    }
}
gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2 */
{for(var i = 0, len = gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2.length ;i < len;++i) {
    gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2[i].getBehavior("Opacity").setOpacity(100);
}
}
{ //Subevents
gdjs.jeuCode.eventsList8(runtimeScene);} //End of subevents
}

}


{

/* Reuse gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects1 */

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects1.length;i<l;++i) {
    if ( gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects1[i].getVariableNumber(gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects1[i].getVariables().getFromIndex(1)) == 5 ) {
        isConditionTrue_0 = true;
        gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects1[k] = gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects1[i];
        ++k;
    }
}
gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects1.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects1 */
{for(var i = 0, len = gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects1.length ;i < len;++i) {
    gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects1[i].getBehavior("Opacity").setOpacity(100);
}
}
{ //Subevents
gdjs.jeuCode.eventsList9(runtimeScene);} //End of subevents
}

}


};gdjs.jeuCode.eventsList11 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("sp_bouton_consigne"), gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.jeuCode.mapOfGDgdjs_9546jeuCode_9546GDsp_95959595bouton_95959595consigneObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {

{ //Subevents
gdjs.jeuCode.eventsList10(runtimeScene);} //End of subevents
}

}


};gdjs.jeuCode.mapOfGDgdjs_9546jeuCode_9546GDsp_95959595bouton_95959595validerObjects1Objects = Hashtable.newFrom({"sp_bouton_valider": gdjs.jeuCode.GDsp_9595bouton_9595validerObjects1});
gdjs.jeuCode.mapOfGDgdjs_9546jeuCode_9546GDsp_95959595jetonObjects1Objects = Hashtable.newFrom({"sp_jeton": gdjs.jeuCode.GDsp_9595jetonObjects1});
gdjs.jeuCode.mapOfGDgdjs_9546jeuCode_9546GDsp_95959595carte_95959595lamaObjects1Objects = Hashtable.newFrom({"sp_carte_lama": gdjs.jeuCode.GDsp_9595carte_9595lamaObjects1});
gdjs.jeuCode.eventsList12 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) == 1;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(3).getChild("scores1").getChild("niv1").setNumber(1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) == 2;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(3).getChild("scores1").getChild("niv2").setNumber(1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) == 3;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(3).getChild("scores1").getChild("niv3").setNumber(1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) == 4;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(3).getChild("scores1").getChild("niv4").setNumber(1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) == 5;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(3).getChild("scores1").getChild("niv5").setNumber(1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) == 6;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(3).getChild("scores1").getChild("niv6").setNumber(1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) == 7;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(3).getChild("scores1").getChild("niv7").setNumber(1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) == 8;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(3).getChild("scores1").getChild("niv8").setNumber(1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) == 9;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(3).getChild("scores1").getChild("niv9").setNumber(1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) == 10;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(3).getChild("scores1").getChild("niv10").setNumber(1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) == 11;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(3).getChild("scores1").getChild("niv11").setNumber(1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) == 12;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(3).getChild("scores1").getChild("niv12").setNumber(1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) == 13;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(3).getChild("scores1").getChild("niv13").setNumber(1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) == 14;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(3).getChild("scores1").getChild("niv14").setNumber(1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) == 15;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(3).getChild("scores1").getChild("niv15").setNumber(1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) == 16;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(3).getChild("scores1").getChild("niv16").setNumber(1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) == 17;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(3).getChild("scores1").getChild("niv17").setNumber(1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) == 18;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(3).getChild("scores1").getChild("niv18").setNumber(1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) == 19;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(3).getChild("scores1").getChild("niv19").setNumber(1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) == 20;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(3).getChild("scores1").getChild("niv20").setNumber(1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) == 21;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(3).getChild("scores1").getChild("niv21").setNumber(1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) == 22;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(3).getChild("scores1").getChild("niv22").setNumber(1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) == 23;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(3).getChild("scores1").getChild("niv23").setNumber(1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) == 24;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(3).getChild("scores1").getChild("niv24").setNumber(1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) == 25;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(3).getChild("scores1").getChild("niv25").setNumber(1);
}}

}


};gdjs.jeuCode.eventsList13 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
{gdjs.evtTools.storage.clearJSONFile("sauvegarde_quelestlelama");
}{gdjs.evtTools.storage.writeStringInJSONFile("sauvegarde_quelestlelama", "reglages", gdjs.evtTools.network.variableStructureToJSON(runtimeScene.getGame().getVariables().getFromIndex(3)));
}}

}


};gdjs.jeuCode.eventsList14 = function(runtimeScene) {

{


gdjs.jeuCode.eventsList12(runtimeScene);
}


{


gdjs.jeuCode.eventsList13(runtimeScene);
}


};gdjs.jeuCode.eventsList15 = function(runtimeScene) {

{

gdjs.copyArray(gdjs.jeuCode.GDsp_9595carte_9595lamaObjects1, gdjs.jeuCode.GDsp_9595carte_9595lamaObjects2);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeuCode.GDsp_9595carte_9595lamaObjects2.length;i<l;++i) {
    if ( gdjs.jeuCode.GDsp_9595carte_9595lamaObjects2[i].getVariableNumber(gdjs.jeuCode.GDsp_9595carte_9595lamaObjects2[i].getVariables().getFromIndex(0)) == runtimeScene.getScene().getVariables().getFromIndex(1).getAsNumber() ) {
        isConditionTrue_0 = true;
        gdjs.jeuCode.GDsp_9595carte_9595lamaObjects2[k] = gdjs.jeuCode.GDsp_9595carte_9595lamaObjects2[i];
        ++k;
    }
}
gdjs.jeuCode.GDsp_9595carte_9595lamaObjects2.length = k;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("sp_bouton_retour"), gdjs.jeuCode.GDsp_9595bouton_9595retourObjects2);
gdjs.copyArray(gdjs.jeuCode.GDsp_9595bouton_9595validerObjects1, gdjs.jeuCode.GDsp_9595bouton_9595validerObjects2);

gdjs.copyArray(runtimeScene.getObjects("sp_faux_vrai"), gdjs.jeuCode.GDsp_9595faux_9595vraiObjects2);
gdjs.copyArray(gdjs.jeuCode.GDsp_9595jetonObjects1, gdjs.jeuCode.GDsp_9595jetonObjects2);

{for(var i = 0, len = gdjs.jeuCode.GDsp_9595faux_9595vraiObjects2.length ;i < len;++i) {
    gdjs.jeuCode.GDsp_9595faux_9595vraiObjects2[i].getBehavior("Animation").setAnimationName("vrai");
}
}{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/vrai.mp3", 1, false, 100, 1);
}{for(var i = 0, len = gdjs.jeuCode.GDsp_9595bouton_9595validerObjects2.length ;i < len;++i) {
    gdjs.jeuCode.GDsp_9595bouton_9595validerObjects2[i].deleteFromScene(runtimeScene);
}
}{for(var i = 0, len = gdjs.jeuCode.GDsp_9595jetonObjects2.length ;i < len;++i) {
    gdjs.jeuCode.GDsp_9595jetonObjects2[i].activateBehavior("Déplaçable", false);
}
}{for(var i = 0, len = gdjs.jeuCode.GDsp_9595bouton_9595retourObjects2.length ;i < len;++i) {
    gdjs.jeuCode.GDsp_9595bouton_9595retourObjects2[i].getBehavior("Animation").resumeAnimation();
}
}
{ //Subevents
gdjs.jeuCode.eventsList14(runtimeScene);} //End of subevents
}

}


{

/* Reuse gdjs.jeuCode.GDsp_9595carte_9595lamaObjects1 */

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeuCode.GDsp_9595carte_9595lamaObjects1.length;i<l;++i) {
    if ( gdjs.jeuCode.GDsp_9595carte_9595lamaObjects1[i].getVariableNumber(gdjs.jeuCode.GDsp_9595carte_9595lamaObjects1[i].getVariables().getFromIndex(0)) != runtimeScene.getScene().getVariables().getFromIndex(1).getAsNumber() ) {
        isConditionTrue_0 = true;
        gdjs.jeuCode.GDsp_9595carte_9595lamaObjects1[k] = gdjs.jeuCode.GDsp_9595carte_9595lamaObjects1[i];
        ++k;
    }
}
gdjs.jeuCode.GDsp_9595carte_9595lamaObjects1.length = k;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("sp_faux_vrai"), gdjs.jeuCode.GDsp_9595faux_9595vraiObjects1);
{for(var i = 0, len = gdjs.jeuCode.GDsp_9595faux_9595vraiObjects1.length ;i < len;++i) {
    gdjs.jeuCode.GDsp_9595faux_9595vraiObjects1[i].getBehavior("Animation").setAnimationName("faux");
}
}{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/faux.mp3", 1, false, 100, 1);
}}

}


};gdjs.jeuCode.eventsList16 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("sp_faux_vrai"), gdjs.jeuCode.GDsp_9595faux_9595vraiObjects2);
{for(var i = 0, len = gdjs.jeuCode.GDsp_9595faux_9595vraiObjects2.length ;i < len;++i) {
    gdjs.jeuCode.GDsp_9595faux_9595vraiObjects2[i].hide(false);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("sp_carte_lama"), gdjs.jeuCode.GDsp_9595carte_9595lamaObjects1);
gdjs.copyArray(runtimeScene.getObjects("sp_jeton"), gdjs.jeuCode.GDsp_9595jetonObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.jeuCode.mapOfGDgdjs_9546jeuCode_9546GDsp_95959595jetonObjects1Objects, gdjs.jeuCode.mapOfGDgdjs_9546jeuCode_9546GDsp_95959595carte_95959595lamaObjects1Objects, false, runtimeScene, false);
if (isConditionTrue_0) {

{ //Subevents
gdjs.jeuCode.eventsList15(runtimeScene);} //End of subevents
}

}


};gdjs.jeuCode.eventsList17 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("sp_bouton_valider"), gdjs.jeuCode.GDsp_9595bouton_9595validerObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.jeuCode.mapOfGDgdjs_9546jeuCode_9546GDsp_95959595bouton_95959595validerObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeuCode.GDsp_9595bouton_9595validerObjects1.length;i<l;++i) {
    if ( gdjs.jeuCode.GDsp_9595bouton_9595validerObjects1[i].getVariableBoolean(gdjs.jeuCode.GDsp_9595bouton_9595validerObjects1[i].getVariables().getFromIndex(0), true, false) ) {
        isConditionTrue_0 = true;
        gdjs.jeuCode.GDsp_9595bouton_9595validerObjects1[k] = gdjs.jeuCode.GDsp_9595bouton_9595validerObjects1[i];
        ++k;
    }
}
gdjs.jeuCode.GDsp_9595bouton_9595validerObjects1.length = k;
}
}
if (isConditionTrue_0) {

{ //Subevents
gdjs.jeuCode.eventsList16(runtimeScene);} //End of subevents
}

}


};gdjs.jeuCode.mapOfGDgdjs_9546jeuCode_9546GDsp_95959595bouton_95959595retourObjects1Objects = Hashtable.newFrom({"sp_bouton_retour": gdjs.jeuCode.GDsp_9595bouton_9595retourObjects1});
gdjs.jeuCode.mapOfGDgdjs_9546jeuCode_9546GDsp_95959595jetonObjects2Objects = Hashtable.newFrom({"sp_jeton": gdjs.jeuCode.GDsp_9595jetonObjects2});
gdjs.jeuCode.mapOfGDgdjs_9546jeuCode_9546GDsp_95959595carte_95959595lamaObjects2Objects = Hashtable.newFrom({"sp_carte_lama": gdjs.jeuCode.GDsp_9595carte_9595lamaObjects2});
gdjs.jeuCode.mapOfGDgdjs_9546jeuCode_9546GDsp_95959595jetonObjects1Objects = Hashtable.newFrom({"sp_jeton": gdjs.jeuCode.GDsp_9595jetonObjects1});
gdjs.jeuCode.mapOfGDgdjs_9546jeuCode_9546GDsp_95959595carte_95959595lamaObjects1Objects = Hashtable.newFrom({"sp_carte_lama": gdjs.jeuCode.GDsp_9595carte_9595lamaObjects1});
gdjs.jeuCode.eventsList18 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("sp_bouton_valider"), gdjs.jeuCode.GDsp_9595bouton_9595validerObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeuCode.GDsp_9595bouton_9595validerObjects2.length;i<l;++i) {
    if ( gdjs.jeuCode.GDsp_9595bouton_9595validerObjects2[i].getVariableBoolean(gdjs.jeuCode.GDsp_9595bouton_9595validerObjects2[i].getVariables().getFromIndex(0), false, false) ) {
        isConditionTrue_0 = true;
        gdjs.jeuCode.GDsp_9595bouton_9595validerObjects2[k] = gdjs.jeuCode.GDsp_9595bouton_9595validerObjects2[i];
        ++k;
    }
}
gdjs.jeuCode.GDsp_9595bouton_9595validerObjects2.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.jeuCode.GDsp_9595bouton_9595validerObjects2 */
{for(var i = 0, len = gdjs.jeuCode.GDsp_9595bouton_9595validerObjects2.length ;i < len;++i) {
    gdjs.jeuCode.GDsp_9595bouton_9595validerObjects2[i].getBehavior("Opacity").setOpacity(100);
}
}{for(var i = 0, len = gdjs.jeuCode.GDsp_9595bouton_9595validerObjects2.length ;i < len;++i) {
    gdjs.jeuCode.GDsp_9595bouton_9595validerObjects2[i].getBehavior("Animation").pauseAnimation();
}
}{for(var i = 0, len = gdjs.jeuCode.GDsp_9595bouton_9595validerObjects2.length ;i < len;++i) {
    gdjs.jeuCode.GDsp_9595bouton_9595validerObjects2[i].setAnimationFrame(0);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("sp_bouton_valider"), gdjs.jeuCode.GDsp_9595bouton_9595validerObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeuCode.GDsp_9595bouton_9595validerObjects2.length;i<l;++i) {
    if ( gdjs.jeuCode.GDsp_9595bouton_9595validerObjects2[i].getVariableBoolean(gdjs.jeuCode.GDsp_9595bouton_9595validerObjects2[i].getVariables().getFromIndex(0), true, false) ) {
        isConditionTrue_0 = true;
        gdjs.jeuCode.GDsp_9595bouton_9595validerObjects2[k] = gdjs.jeuCode.GDsp_9595bouton_9595validerObjects2[i];
        ++k;
    }
}
gdjs.jeuCode.GDsp_9595bouton_9595validerObjects2.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.jeuCode.GDsp_9595bouton_9595validerObjects2 */
{for(var i = 0, len = gdjs.jeuCode.GDsp_9595bouton_9595validerObjects2.length ;i < len;++i) {
    gdjs.jeuCode.GDsp_9595bouton_9595validerObjects2[i].getBehavior("Opacity").setOpacity(255);
}
}{for(var i = 0, len = gdjs.jeuCode.GDsp_9595bouton_9595validerObjects2.length ;i < len;++i) {
    gdjs.jeuCode.GDsp_9595bouton_9595validerObjects2[i].getBehavior("Animation").resumeAnimation();
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("sp_carte_lama"), gdjs.jeuCode.GDsp_9595carte_9595lamaObjects2);
gdjs.copyArray(runtimeScene.getObjects("sp_jeton"), gdjs.jeuCode.GDsp_9595jetonObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.jeuCode.mapOfGDgdjs_9546jeuCode_9546GDsp_95959595jetonObjects2Objects, gdjs.jeuCode.mapOfGDgdjs_9546jeuCode_9546GDsp_95959595carte_95959595lamaObjects2Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(14389020);
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("sp_bouton_valider"), gdjs.jeuCode.GDsp_9595bouton_9595validerObjects2);
{for(var i = 0, len = gdjs.jeuCode.GDsp_9595bouton_9595validerObjects2.length ;i < len;++i) {
    gdjs.jeuCode.GDsp_9595bouton_9595validerObjects2[i].returnVariable(gdjs.jeuCode.GDsp_9595bouton_9595validerObjects2[i].getVariables().getFromIndex(0)).setBoolean(true);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("sp_carte_lama"), gdjs.jeuCode.GDsp_9595carte_9595lamaObjects1);
gdjs.copyArray(runtimeScene.getObjects("sp_jeton"), gdjs.jeuCode.GDsp_9595jetonObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.jeuCode.mapOfGDgdjs_9546jeuCode_9546GDsp_95959595jetonObjects1Objects, gdjs.jeuCode.mapOfGDgdjs_9546jeuCode_9546GDsp_95959595carte_95959595lamaObjects1Objects, true, runtimeScene, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(14390236);
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("sp_bouton_valider"), gdjs.jeuCode.GDsp_9595bouton_9595validerObjects1);
{for(var i = 0, len = gdjs.jeuCode.GDsp_9595bouton_9595validerObjects1.length ;i < len;++i) {
    gdjs.jeuCode.GDsp_9595bouton_9595validerObjects1[i].returnVariable(gdjs.jeuCode.GDsp_9595bouton_9595validerObjects1[i].getVariables().getFromIndex(0)).setBoolean(false);
}
}}

}


};gdjs.jeuCode.eventsList19 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("sp_bouton_recommencer"), gdjs.jeuCode.GDsp_9595bouton_9595recommencerObjects1);
gdjs.copyArray(runtimeScene.getObjects("sp_bouton_retour"), gdjs.jeuCode.GDsp_9595bouton_9595retourObjects1);
gdjs.copyArray(runtimeScene.getObjects("sp_carte_lama"), gdjs.jeuCode.GDsp_9595carte_9595lamaObjects1);
gdjs.copyArray(runtimeScene.getObjects("sp_faux_vrai"), gdjs.jeuCode.GDsp_9595faux_9595vraiObjects1);
gdjs.copyArray(runtimeScene.getObjects("sp_fond"), gdjs.jeuCode.GDsp_9595fondObjects1);
{for(var i = 0, len = gdjs.jeuCode.GDsp_9595carte_9595lamaObjects1.length ;i < len;++i) {
    gdjs.jeuCode.GDsp_9595carte_9595lamaObjects1[i].getBehavior("Animation").pauseAnimation();
}
}{for(var i = 0, len = gdjs.jeuCode.GDsp_9595carte_9595lamaObjects1.length ;i < len;++i) {
    gdjs.jeuCode.GDsp_9595carte_9595lamaObjects1[i].setAnimationFrame(gdjs.jeuCode.GDsp_9595carte_9595lamaObjects1[i].getVariables().getFromIndex(0).getAsNumber());
}
}{for(var i = 0, len = gdjs.jeuCode.GDsp_9595carte_9595lamaObjects1.length ;i < len;++i) {
    gdjs.jeuCode.GDsp_9595carte_9595lamaObjects1[i].getBehavior("Effect").enableEffect("Effect", false);
}
}{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/question.mp3", 1, false, 100, 1);
}{for(var i = 0, len = gdjs.jeuCode.GDsp_9595faux_9595vraiObjects1.length ;i < len;++i) {
    gdjs.jeuCode.GDsp_9595faux_9595vraiObjects1[i].hide();
}
}{for(var i = 0, len = gdjs.jeuCode.GDsp_9595bouton_9595recommencerObjects1.length ;i < len;++i) {
    gdjs.jeuCode.GDsp_9595bouton_9595recommencerObjects1[i].getBehavior("Animation").pauseAnimation();
}
}{for(var i = 0, len = gdjs.jeuCode.GDsp_9595fondObjects1.length ;i < len;++i) {
    gdjs.jeuCode.GDsp_9595fondObjects1[i].getBehavior("Animation").setAnimationName("jeu");
}
}{for(var i = 0, len = gdjs.jeuCode.GDsp_9595fondObjects1.length ;i < len;++i) {
    gdjs.jeuCode.GDsp_9595fondObjects1[i].getBehavior("Opacity").setOpacity(100);
}
}{for(var i = 0, len = gdjs.jeuCode.GDsp_9595bouton_9595retourObjects1.length ;i < len;++i) {
    gdjs.jeuCode.GDsp_9595bouton_9595retourObjects1[i].getBehavior("Animation").pauseAnimation();
}
}
{ //Subevents
gdjs.jeuCode.eventsList2(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("sp_carte_lama"), gdjs.jeuCode.GDsp_9595carte_9595lamaObjects1);
gdjs.copyArray(runtimeScene.getObjects("sp_jeton"), gdjs.jeuCode.GDsp_9595jetonObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.jeuCode.mapOfGDgdjs_9546jeuCode_9546GDsp_95959595carte_95959595lamaObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.jeuCode.mapOfGDgdjs_9546jeuCode_9546GDsp_95959595jetonObjects1Objects, gdjs.jeuCode.mapOfGDgdjs_9546jeuCode_9546GDsp_95959595carte_95959595lamaObjects1Objects, true, runtimeScene, false);
}
}
if (isConditionTrue_0) {
/* Reuse gdjs.jeuCode.GDsp_9595carte_9595lamaObjects1 */
{runtimeScene.getScene().getVariables().getFromIndex(0).setNumber(1);
}{for(var i = 0, len = gdjs.jeuCode.GDsp_9595carte_9595lamaObjects1.length ;i < len;++i) {
    gdjs.jeuCode.GDsp_9595carte_9595lamaObjects1[i].returnVariable(gdjs.jeuCode.GDsp_9595carte_9595lamaObjects1[i].getVariables().getFromIndex(1)).toggle();
}
}
{ //Subevents
gdjs.jeuCode.eventsList3(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("sp_bouton_recommencer"), gdjs.jeuCode.GDsp_9595bouton_9595recommencerObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.jeuCode.mapOfGDgdjs_9546jeuCode_9546GDsp_95959595bouton_95959595recommencerObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "jeu", false);
}
{ //Subevents
gdjs.jeuCode.eventsList4(runtimeScene);} //End of subevents
}

}


{


gdjs.jeuCode.eventsList11(runtimeScene);
}


{


gdjs.jeuCode.eventsList17(runtimeScene);
}


{

gdjs.copyArray(runtimeScene.getObjects("sp_bouton_retour"), gdjs.jeuCode.GDsp_9595bouton_9595retourObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.jeuCode.mapOfGDgdjs_9546jeuCode_9546GDsp_95959595bouton_95959595retourObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "menu", false);
}}

}


{


gdjs.jeuCode.eventsList18(runtimeScene);
}


{


let isConditionTrue_0 = false;
{
}

}


};

gdjs.jeuCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.jeuCode.GDtxt_9595consigneObjects1.length = 0;
gdjs.jeuCode.GDtxt_9595consigneObjects2.length = 0;
gdjs.jeuCode.GDtxt_9595consigneObjects3.length = 0;
gdjs.jeuCode.GDtxt_9595consigneObjects4.length = 0;
gdjs.jeuCode.GDtxt_9595consigneObjects5.length = 0;
gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects1.length = 0;
gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2.length = 0;
gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects3.length = 0;
gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects4.length = 0;
gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects5.length = 0;
gdjs.jeuCode.GDsp_9595faux_9595vraiObjects1.length = 0;
gdjs.jeuCode.GDsp_9595faux_9595vraiObjects2.length = 0;
gdjs.jeuCode.GDsp_9595faux_9595vraiObjects3.length = 0;
gdjs.jeuCode.GDsp_9595faux_9595vraiObjects4.length = 0;
gdjs.jeuCode.GDsp_9595faux_9595vraiObjects5.length = 0;
gdjs.jeuCode.GDsp_9595carte_9595lamaObjects1.length = 0;
gdjs.jeuCode.GDsp_9595carte_9595lamaObjects2.length = 0;
gdjs.jeuCode.GDsp_9595carte_9595lamaObjects3.length = 0;
gdjs.jeuCode.GDsp_9595carte_9595lamaObjects4.length = 0;
gdjs.jeuCode.GDsp_9595carte_9595lamaObjects5.length = 0;
gdjs.jeuCode.GDsp_9595bouton_9595retourObjects1.length = 0;
gdjs.jeuCode.GDsp_9595bouton_9595retourObjects2.length = 0;
gdjs.jeuCode.GDsp_9595bouton_9595retourObjects3.length = 0;
gdjs.jeuCode.GDsp_9595bouton_9595retourObjects4.length = 0;
gdjs.jeuCode.GDsp_9595bouton_9595retourObjects5.length = 0;
gdjs.jeuCode.GDsp_9595bouton_9595validerObjects1.length = 0;
gdjs.jeuCode.GDsp_9595bouton_9595validerObjects2.length = 0;
gdjs.jeuCode.GDsp_9595bouton_9595validerObjects3.length = 0;
gdjs.jeuCode.GDsp_9595bouton_9595validerObjects4.length = 0;
gdjs.jeuCode.GDsp_9595bouton_9595validerObjects5.length = 0;
gdjs.jeuCode.GDsp_9595jetonObjects1.length = 0;
gdjs.jeuCode.GDsp_9595jetonObjects2.length = 0;
gdjs.jeuCode.GDsp_9595jetonObjects3.length = 0;
gdjs.jeuCode.GDsp_9595jetonObjects4.length = 0;
gdjs.jeuCode.GDsp_9595jetonObjects5.length = 0;
gdjs.jeuCode.GDsp_9595bouton_9595recommencerObjects1.length = 0;
gdjs.jeuCode.GDsp_9595bouton_9595recommencerObjects2.length = 0;
gdjs.jeuCode.GDsp_9595bouton_9595recommencerObjects3.length = 0;
gdjs.jeuCode.GDsp_9595bouton_9595recommencerObjects4.length = 0;
gdjs.jeuCode.GDsp_9595bouton_9595recommencerObjects5.length = 0;
gdjs.jeuCode.GDsp_9595fondObjects1.length = 0;
gdjs.jeuCode.GDsp_9595fondObjects2.length = 0;
gdjs.jeuCode.GDsp_9595fondObjects3.length = 0;
gdjs.jeuCode.GDsp_9595fondObjects4.length = 0;
gdjs.jeuCode.GDsp_9595fondObjects5.length = 0;

gdjs.jeuCode.eventsList19(runtimeScene);
gdjs.jeuCode.GDtxt_9595consigneObjects1.length = 0;
gdjs.jeuCode.GDtxt_9595consigneObjects2.length = 0;
gdjs.jeuCode.GDtxt_9595consigneObjects3.length = 0;
gdjs.jeuCode.GDtxt_9595consigneObjects4.length = 0;
gdjs.jeuCode.GDtxt_9595consigneObjects5.length = 0;
gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects1.length = 0;
gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects2.length = 0;
gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects3.length = 0;
gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects4.length = 0;
gdjs.jeuCode.GDsp_9595bouton_9595consigneObjects5.length = 0;
gdjs.jeuCode.GDsp_9595faux_9595vraiObjects1.length = 0;
gdjs.jeuCode.GDsp_9595faux_9595vraiObjects2.length = 0;
gdjs.jeuCode.GDsp_9595faux_9595vraiObjects3.length = 0;
gdjs.jeuCode.GDsp_9595faux_9595vraiObjects4.length = 0;
gdjs.jeuCode.GDsp_9595faux_9595vraiObjects5.length = 0;
gdjs.jeuCode.GDsp_9595carte_9595lamaObjects1.length = 0;
gdjs.jeuCode.GDsp_9595carte_9595lamaObjects2.length = 0;
gdjs.jeuCode.GDsp_9595carte_9595lamaObjects3.length = 0;
gdjs.jeuCode.GDsp_9595carte_9595lamaObjects4.length = 0;
gdjs.jeuCode.GDsp_9595carte_9595lamaObjects5.length = 0;
gdjs.jeuCode.GDsp_9595bouton_9595retourObjects1.length = 0;
gdjs.jeuCode.GDsp_9595bouton_9595retourObjects2.length = 0;
gdjs.jeuCode.GDsp_9595bouton_9595retourObjects3.length = 0;
gdjs.jeuCode.GDsp_9595bouton_9595retourObjects4.length = 0;
gdjs.jeuCode.GDsp_9595bouton_9595retourObjects5.length = 0;
gdjs.jeuCode.GDsp_9595bouton_9595validerObjects1.length = 0;
gdjs.jeuCode.GDsp_9595bouton_9595validerObjects2.length = 0;
gdjs.jeuCode.GDsp_9595bouton_9595validerObjects3.length = 0;
gdjs.jeuCode.GDsp_9595bouton_9595validerObjects4.length = 0;
gdjs.jeuCode.GDsp_9595bouton_9595validerObjects5.length = 0;
gdjs.jeuCode.GDsp_9595jetonObjects1.length = 0;
gdjs.jeuCode.GDsp_9595jetonObjects2.length = 0;
gdjs.jeuCode.GDsp_9595jetonObjects3.length = 0;
gdjs.jeuCode.GDsp_9595jetonObjects4.length = 0;
gdjs.jeuCode.GDsp_9595jetonObjects5.length = 0;
gdjs.jeuCode.GDsp_9595bouton_9595recommencerObjects1.length = 0;
gdjs.jeuCode.GDsp_9595bouton_9595recommencerObjects2.length = 0;
gdjs.jeuCode.GDsp_9595bouton_9595recommencerObjects3.length = 0;
gdjs.jeuCode.GDsp_9595bouton_9595recommencerObjects4.length = 0;
gdjs.jeuCode.GDsp_9595bouton_9595recommencerObjects5.length = 0;
gdjs.jeuCode.GDsp_9595fondObjects1.length = 0;
gdjs.jeuCode.GDsp_9595fondObjects2.length = 0;
gdjs.jeuCode.GDsp_9595fondObjects3.length = 0;
gdjs.jeuCode.GDsp_9595fondObjects4.length = 0;
gdjs.jeuCode.GDsp_9595fondObjects5.length = 0;


return;

}

gdjs['jeuCode'] = gdjs.jeuCode;
