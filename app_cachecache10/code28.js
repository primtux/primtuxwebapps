gdjs.infosCode = {};
gdjs.infosCode.GDfond_9595blancObjects1= [];
gdjs.infosCode.GDfond_9595blancObjects2= [];
gdjs.infosCode.GDinfosObjects1= [];
gdjs.infosCode.GDinfosObjects2= [];
gdjs.infosCode.GDnombre0Objects1= [];
gdjs.infosCode.GDnombre0Objects2= [];
gdjs.infosCode.GDniv_95951Objects1= [];
gdjs.infosCode.GDniv_95951Objects2= [];
gdjs.infosCode.GDnombre1Objects1= [];
gdjs.infosCode.GDnombre1Objects2= [];
gdjs.infosCode.GDniv_95952Objects1= [];
gdjs.infosCode.GDniv_95952Objects2= [];
gdjs.infosCode.GDnombre2Objects1= [];
gdjs.infosCode.GDnombre2Objects2= [];
gdjs.infosCode.GDnombre3Objects1= [];
gdjs.infosCode.GDnombre3Objects2= [];
gdjs.infosCode.GDnombre4Objects1= [];
gdjs.infosCode.GDnombre4Objects2= [];
gdjs.infosCode.GDnombre5Objects1= [];
gdjs.infosCode.GDnombre5Objects2= [];
gdjs.infosCode.GDnombre6Objects1= [];
gdjs.infosCode.GDnombre6Objects2= [];
gdjs.infosCode.GDnombre7Objects1= [];
gdjs.infosCode.GDnombre7Objects2= [];
gdjs.infosCode.GDnombre8Objects1= [];
gdjs.infosCode.GDnombre8Objects2= [];
gdjs.infosCode.GDnombre9Objects1= [];
gdjs.infosCode.GDnombre9Objects2= [];
gdjs.infosCode.GDnombre10_9595titreObjects1= [];
gdjs.infosCode.GDnombre10_9595titreObjects2= [];
gdjs.infosCode.GDnombre10Objects1= [];
gdjs.infosCode.GDnombre10Objects2= [];
gdjs.infosCode.GDchoix_9595nombre0Objects1= [];
gdjs.infosCode.GDchoix_9595nombre0Objects2= [];
gdjs.infosCode.GDchoix_9595nombre1Objects1= [];
gdjs.infosCode.GDchoix_9595nombre1Objects2= [];
gdjs.infosCode.GDchoix_9595nombre2Objects1= [];
gdjs.infosCode.GDchoix_9595nombre2Objects2= [];
gdjs.infosCode.GDchoix_9595nombre3Objects1= [];
gdjs.infosCode.GDchoix_9595nombre3Objects2= [];
gdjs.infosCode.GDchoix_9595nombre4Objects1= [];
gdjs.infosCode.GDchoix_9595nombre4Objects2= [];
gdjs.infosCode.GDchoix_9595nombre5Objects1= [];
gdjs.infosCode.GDchoix_9595nombre5Objects2= [];
gdjs.infosCode.GDchoix_9595nombre6Objects1= [];
gdjs.infosCode.GDchoix_9595nombre6Objects2= [];
gdjs.infosCode.GDchoix_9595nombre7Objects1= [];
gdjs.infosCode.GDchoix_9595nombre7Objects2= [];
gdjs.infosCode.GDchoix_9595nombre8Objects1= [];
gdjs.infosCode.GDchoix_9595nombre8Objects2= [];
gdjs.infosCode.GDchoix_9595nombre9Objects1= [];
gdjs.infosCode.GDchoix_9595nombre9Objects2= [];
gdjs.infosCode.GDchoix_9595nombre10Objects1= [];
gdjs.infosCode.GDchoix_9595nombre10Objects2= [];
gdjs.infosCode.GDaide_9595sergeObjects1= [];
gdjs.infosCode.GDaide_9595sergeObjects2= [];
gdjs.infosCode.GDserge_9595loupeObjects1= [];
gdjs.infosCode.GDserge_9595loupeObjects2= [];
gdjs.infosCode.GDbouton_9595suivant2Objects1= [];
gdjs.infosCode.GDbouton_9595suivant2Objects2= [];
gdjs.infosCode.GDbouton_9595suivant3Objects1= [];
gdjs.infosCode.GDbouton_9595suivant3Objects2= [];
gdjs.infosCode.GDbouton_9595suivantObjects1= [];
gdjs.infosCode.GDbouton_9595suivantObjects2= [];
gdjs.infosCode.GDscore_95951Objects1= [];
gdjs.infosCode.GDscore_95951Objects2= [];
gdjs.infosCode.GDaide_9595audioObjects1= [];
gdjs.infosCode.GDaide_9595audioObjects2= [];
gdjs.infosCode.GDscore_95952Objects1= [];
gdjs.infosCode.GDscore_95952Objects2= [];
gdjs.infosCode.GDfond_9595natureObjects1= [];
gdjs.infosCode.GDfond_9595natureObjects2= [];
gdjs.infosCode.GDfond_9595nature2Objects1= [];
gdjs.infosCode.GDfond_9595nature2Objects2= [];
gdjs.infosCode.GDbouton_9595retourObjects1= [];
gdjs.infosCode.GDbouton_9595retourObjects2= [];
gdjs.infosCode.GDfond_9595nature3Objects1= [];
gdjs.infosCode.GDfond_9595nature3Objects2= [];
gdjs.infosCode.GDbarre_9595progressionObjects1= [];
gdjs.infosCode.GDbarre_9595progressionObjects2= [];
gdjs.infosCode.GDetoileObjects1= [];
gdjs.infosCode.GDetoileObjects2= [];


gdjs.infosCode.mapOfGDgdjs_9546infosCode_9546GDbouton_95959595retourObjects1Objects = Hashtable.newFrom({"bouton_retour": gdjs.infosCode.GDbouton_9595retourObjects1});
gdjs.infosCode.eventsList0 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("bouton_retour"), gdjs.infosCode.GDbouton_9595retourObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.infosCode.mapOfGDgdjs_9546infosCode_9546GDbouton_95959595retourObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(23537860);
}
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "menu", false);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("fond_blanc"), gdjs.infosCode.GDfond_9595blancObjects1);
{for(var i = 0, len = gdjs.infosCode.GDfond_9595blancObjects1.length ;i < len;++i) {
    gdjs.infosCode.GDfond_9595blancObjects1[i].setOpacity(200);
}
}}

}


};

gdjs.infosCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.infosCode.GDfond_9595blancObjects1.length = 0;
gdjs.infosCode.GDfond_9595blancObjects2.length = 0;
gdjs.infosCode.GDinfosObjects1.length = 0;
gdjs.infosCode.GDinfosObjects2.length = 0;
gdjs.infosCode.GDnombre0Objects1.length = 0;
gdjs.infosCode.GDnombre0Objects2.length = 0;
gdjs.infosCode.GDniv_95951Objects1.length = 0;
gdjs.infosCode.GDniv_95951Objects2.length = 0;
gdjs.infosCode.GDnombre1Objects1.length = 0;
gdjs.infosCode.GDnombre1Objects2.length = 0;
gdjs.infosCode.GDniv_95952Objects1.length = 0;
gdjs.infosCode.GDniv_95952Objects2.length = 0;
gdjs.infosCode.GDnombre2Objects1.length = 0;
gdjs.infosCode.GDnombre2Objects2.length = 0;
gdjs.infosCode.GDnombre3Objects1.length = 0;
gdjs.infosCode.GDnombre3Objects2.length = 0;
gdjs.infosCode.GDnombre4Objects1.length = 0;
gdjs.infosCode.GDnombre4Objects2.length = 0;
gdjs.infosCode.GDnombre5Objects1.length = 0;
gdjs.infosCode.GDnombre5Objects2.length = 0;
gdjs.infosCode.GDnombre6Objects1.length = 0;
gdjs.infosCode.GDnombre6Objects2.length = 0;
gdjs.infosCode.GDnombre7Objects1.length = 0;
gdjs.infosCode.GDnombre7Objects2.length = 0;
gdjs.infosCode.GDnombre8Objects1.length = 0;
gdjs.infosCode.GDnombre8Objects2.length = 0;
gdjs.infosCode.GDnombre9Objects1.length = 0;
gdjs.infosCode.GDnombre9Objects2.length = 0;
gdjs.infosCode.GDnombre10_9595titreObjects1.length = 0;
gdjs.infosCode.GDnombre10_9595titreObjects2.length = 0;
gdjs.infosCode.GDnombre10Objects1.length = 0;
gdjs.infosCode.GDnombre10Objects2.length = 0;
gdjs.infosCode.GDchoix_9595nombre0Objects1.length = 0;
gdjs.infosCode.GDchoix_9595nombre0Objects2.length = 0;
gdjs.infosCode.GDchoix_9595nombre1Objects1.length = 0;
gdjs.infosCode.GDchoix_9595nombre1Objects2.length = 0;
gdjs.infosCode.GDchoix_9595nombre2Objects1.length = 0;
gdjs.infosCode.GDchoix_9595nombre2Objects2.length = 0;
gdjs.infosCode.GDchoix_9595nombre3Objects1.length = 0;
gdjs.infosCode.GDchoix_9595nombre3Objects2.length = 0;
gdjs.infosCode.GDchoix_9595nombre4Objects1.length = 0;
gdjs.infosCode.GDchoix_9595nombre4Objects2.length = 0;
gdjs.infosCode.GDchoix_9595nombre5Objects1.length = 0;
gdjs.infosCode.GDchoix_9595nombre5Objects2.length = 0;
gdjs.infosCode.GDchoix_9595nombre6Objects1.length = 0;
gdjs.infosCode.GDchoix_9595nombre6Objects2.length = 0;
gdjs.infosCode.GDchoix_9595nombre7Objects1.length = 0;
gdjs.infosCode.GDchoix_9595nombre7Objects2.length = 0;
gdjs.infosCode.GDchoix_9595nombre8Objects1.length = 0;
gdjs.infosCode.GDchoix_9595nombre8Objects2.length = 0;
gdjs.infosCode.GDchoix_9595nombre9Objects1.length = 0;
gdjs.infosCode.GDchoix_9595nombre9Objects2.length = 0;
gdjs.infosCode.GDchoix_9595nombre10Objects1.length = 0;
gdjs.infosCode.GDchoix_9595nombre10Objects2.length = 0;
gdjs.infosCode.GDaide_9595sergeObjects1.length = 0;
gdjs.infosCode.GDaide_9595sergeObjects2.length = 0;
gdjs.infosCode.GDserge_9595loupeObjects1.length = 0;
gdjs.infosCode.GDserge_9595loupeObjects2.length = 0;
gdjs.infosCode.GDbouton_9595suivant2Objects1.length = 0;
gdjs.infosCode.GDbouton_9595suivant2Objects2.length = 0;
gdjs.infosCode.GDbouton_9595suivant3Objects1.length = 0;
gdjs.infosCode.GDbouton_9595suivant3Objects2.length = 0;
gdjs.infosCode.GDbouton_9595suivantObjects1.length = 0;
gdjs.infosCode.GDbouton_9595suivantObjects2.length = 0;
gdjs.infosCode.GDscore_95951Objects1.length = 0;
gdjs.infosCode.GDscore_95951Objects2.length = 0;
gdjs.infosCode.GDaide_9595audioObjects1.length = 0;
gdjs.infosCode.GDaide_9595audioObjects2.length = 0;
gdjs.infosCode.GDscore_95952Objects1.length = 0;
gdjs.infosCode.GDscore_95952Objects2.length = 0;
gdjs.infosCode.GDfond_9595natureObjects1.length = 0;
gdjs.infosCode.GDfond_9595natureObjects2.length = 0;
gdjs.infosCode.GDfond_9595nature2Objects1.length = 0;
gdjs.infosCode.GDfond_9595nature2Objects2.length = 0;
gdjs.infosCode.GDbouton_9595retourObjects1.length = 0;
gdjs.infosCode.GDbouton_9595retourObjects2.length = 0;
gdjs.infosCode.GDfond_9595nature3Objects1.length = 0;
gdjs.infosCode.GDfond_9595nature3Objects2.length = 0;
gdjs.infosCode.GDbarre_9595progressionObjects1.length = 0;
gdjs.infosCode.GDbarre_9595progressionObjects2.length = 0;
gdjs.infosCode.GDetoileObjects1.length = 0;
gdjs.infosCode.GDetoileObjects2.length = 0;

gdjs.infosCode.eventsList0(runtimeScene);

return;

}

gdjs['infosCode'] = gdjs.infosCode;
