gdjs.infosCode = {};
gdjs.infosCode.GDnombre0Objects1= [];
gdjs.infosCode.GDnombre0Objects2= [];
gdjs.infosCode.GDniv_951Objects1= [];
gdjs.infosCode.GDniv_951Objects2= [];
gdjs.infosCode.GDnombre1Objects1= [];
gdjs.infosCode.GDnombre1Objects2= [];
gdjs.infosCode.GDniv_952Objects1= [];
gdjs.infosCode.GDniv_952Objects2= [];
gdjs.infosCode.GDnombre2Objects1= [];
gdjs.infosCode.GDnombre2Objects2= [];
gdjs.infosCode.GDnombre3Objects1= [];
gdjs.infosCode.GDnombre3Objects2= [];
gdjs.infosCode.GDnombre4Objects1= [];
gdjs.infosCode.GDnombre4Objects2= [];
gdjs.infosCode.GDnombre5Objects1= [];
gdjs.infosCode.GDnombre5Objects2= [];
gdjs.infosCode.GDnombre6Objects1= [];
gdjs.infosCode.GDnombre6Objects2= [];
gdjs.infosCode.GDnombre7Objects1= [];
gdjs.infosCode.GDnombre7Objects2= [];
gdjs.infosCode.GDnombre8Objects1= [];
gdjs.infosCode.GDnombre8Objects2= [];
gdjs.infosCode.GDnombre9Objects1= [];
gdjs.infosCode.GDnombre9Objects2= [];
gdjs.infosCode.GDnombre10_95titreObjects1= [];
gdjs.infosCode.GDnombre10_95titreObjects2= [];
gdjs.infosCode.GDnombre10Objects1= [];
gdjs.infosCode.GDnombre10Objects2= [];
gdjs.infosCode.GDchoix_95nombre0Objects1= [];
gdjs.infosCode.GDchoix_95nombre0Objects2= [];
gdjs.infosCode.GDchoix_95nombre1Objects1= [];
gdjs.infosCode.GDchoix_95nombre1Objects2= [];
gdjs.infosCode.GDchoix_95nombre2Objects1= [];
gdjs.infosCode.GDchoix_95nombre2Objects2= [];
gdjs.infosCode.GDchoix_95nombre3Objects1= [];
gdjs.infosCode.GDchoix_95nombre3Objects2= [];
gdjs.infosCode.GDchoix_95nombre4Objects1= [];
gdjs.infosCode.GDchoix_95nombre4Objects2= [];
gdjs.infosCode.GDchoix_95nombre5Objects1= [];
gdjs.infosCode.GDchoix_95nombre5Objects2= [];
gdjs.infosCode.GDchoix_95nombre6Objects1= [];
gdjs.infosCode.GDchoix_95nombre6Objects2= [];
gdjs.infosCode.GDchoix_95nombre7Objects1= [];
gdjs.infosCode.GDchoix_95nombre7Objects2= [];
gdjs.infosCode.GDchoix_95nombre8Objects1= [];
gdjs.infosCode.GDchoix_95nombre8Objects2= [];
gdjs.infosCode.GDchoix_95nombre9Objects1= [];
gdjs.infosCode.GDchoix_95nombre9Objects2= [];
gdjs.infosCode.GDchoix_95nombre10Objects1= [];
gdjs.infosCode.GDchoix_95nombre10Objects2= [];
gdjs.infosCode.GDaide_95sergeObjects1= [];
gdjs.infosCode.GDaide_95sergeObjects2= [];
gdjs.infosCode.GDserge_95loupeObjects1= [];
gdjs.infosCode.GDserge_95loupeObjects2= [];
gdjs.infosCode.GDbouton_95suivant2Objects1= [];
gdjs.infosCode.GDbouton_95suivant2Objects2= [];
gdjs.infosCode.GDbouton_95suivant3Objects1= [];
gdjs.infosCode.GDbouton_95suivant3Objects2= [];
gdjs.infosCode.GDbouton_95suivantObjects1= [];
gdjs.infosCode.GDbouton_95suivantObjects2= [];
gdjs.infosCode.GDscore_951Objects1= [];
gdjs.infosCode.GDscore_951Objects2= [];
gdjs.infosCode.GDaide_95audioObjects1= [];
gdjs.infosCode.GDaide_95audioObjects2= [];
gdjs.infosCode.GDscore_952Objects1= [];
gdjs.infosCode.GDscore_952Objects2= [];
gdjs.infosCode.GDfond_95natureObjects1= [];
gdjs.infosCode.GDfond_95natureObjects2= [];
gdjs.infosCode.GDfond_95nature2Objects1= [];
gdjs.infosCode.GDfond_95nature2Objects2= [];
gdjs.infosCode.GDbouton_95retourObjects1= [];
gdjs.infosCode.GDbouton_95retourObjects2= [];
gdjs.infosCode.GDfond_95nature3Objects1= [];
gdjs.infosCode.GDfond_95nature3Objects2= [];
gdjs.infosCode.GDbarre_95progressionObjects1= [];
gdjs.infosCode.GDbarre_95progressionObjects2= [];
gdjs.infosCode.GDetoileObjects1= [];
gdjs.infosCode.GDetoileObjects2= [];
gdjs.infosCode.GDfond_95blancObjects1= [];
gdjs.infosCode.GDfond_95blancObjects2= [];
gdjs.infosCode.GDinfosObjects1= [];
gdjs.infosCode.GDinfosObjects2= [];

gdjs.infosCode.conditionTrue_0 = {val:false};
gdjs.infosCode.condition0IsTrue_0 = {val:false};
gdjs.infosCode.condition1IsTrue_0 = {val:false};
gdjs.infosCode.condition2IsTrue_0 = {val:false};
gdjs.infosCode.condition3IsTrue_0 = {val:false};
gdjs.infosCode.conditionTrue_1 = {val:false};
gdjs.infosCode.condition0IsTrue_1 = {val:false};
gdjs.infosCode.condition1IsTrue_1 = {val:false};
gdjs.infosCode.condition2IsTrue_1 = {val:false};
gdjs.infosCode.condition3IsTrue_1 = {val:false};


gdjs.infosCode.mapOfGDgdjs_46infosCode_46GDbouton_9595retourObjects1Objects = Hashtable.newFrom({"bouton_retour": gdjs.infosCode.GDbouton_95retourObjects1});
gdjs.infosCode.eventsList0 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("bouton_retour"), gdjs.infosCode.GDbouton_95retourObjects1);

gdjs.infosCode.condition0IsTrue_0.val = false;
gdjs.infosCode.condition1IsTrue_0.val = false;
gdjs.infosCode.condition2IsTrue_0.val = false;
{
gdjs.infosCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.infosCode.mapOfGDgdjs_46infosCode_46GDbouton_9595retourObjects1Objects, runtimeScene, true, false);
}if ( gdjs.infosCode.condition0IsTrue_0.val ) {
{
gdjs.infosCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.infosCode.condition1IsTrue_0.val ) {
{
{gdjs.infosCode.conditionTrue_1 = gdjs.infosCode.condition2IsTrue_0;
gdjs.infosCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(33445948);
}
}}
}
if (gdjs.infosCode.condition2IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "menu", false);
}}

}


{


gdjs.infosCode.condition0IsTrue_0.val = false;
{
gdjs.infosCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.infosCode.condition0IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("fond_blanc"), gdjs.infosCode.GDfond_95blancObjects1);
{for(var i = 0, len = gdjs.infosCode.GDfond_95blancObjects1.length ;i < len;++i) {
    gdjs.infosCode.GDfond_95blancObjects1[i].setOpacity(200);
}
}}

}


};

gdjs.infosCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.infosCode.GDnombre0Objects1.length = 0;
gdjs.infosCode.GDnombre0Objects2.length = 0;
gdjs.infosCode.GDniv_951Objects1.length = 0;
gdjs.infosCode.GDniv_951Objects2.length = 0;
gdjs.infosCode.GDnombre1Objects1.length = 0;
gdjs.infosCode.GDnombre1Objects2.length = 0;
gdjs.infosCode.GDniv_952Objects1.length = 0;
gdjs.infosCode.GDniv_952Objects2.length = 0;
gdjs.infosCode.GDnombre2Objects1.length = 0;
gdjs.infosCode.GDnombre2Objects2.length = 0;
gdjs.infosCode.GDnombre3Objects1.length = 0;
gdjs.infosCode.GDnombre3Objects2.length = 0;
gdjs.infosCode.GDnombre4Objects1.length = 0;
gdjs.infosCode.GDnombre4Objects2.length = 0;
gdjs.infosCode.GDnombre5Objects1.length = 0;
gdjs.infosCode.GDnombre5Objects2.length = 0;
gdjs.infosCode.GDnombre6Objects1.length = 0;
gdjs.infosCode.GDnombre6Objects2.length = 0;
gdjs.infosCode.GDnombre7Objects1.length = 0;
gdjs.infosCode.GDnombre7Objects2.length = 0;
gdjs.infosCode.GDnombre8Objects1.length = 0;
gdjs.infosCode.GDnombre8Objects2.length = 0;
gdjs.infosCode.GDnombre9Objects1.length = 0;
gdjs.infosCode.GDnombre9Objects2.length = 0;
gdjs.infosCode.GDnombre10_95titreObjects1.length = 0;
gdjs.infosCode.GDnombre10_95titreObjects2.length = 0;
gdjs.infosCode.GDnombre10Objects1.length = 0;
gdjs.infosCode.GDnombre10Objects2.length = 0;
gdjs.infosCode.GDchoix_95nombre0Objects1.length = 0;
gdjs.infosCode.GDchoix_95nombre0Objects2.length = 0;
gdjs.infosCode.GDchoix_95nombre1Objects1.length = 0;
gdjs.infosCode.GDchoix_95nombre1Objects2.length = 0;
gdjs.infosCode.GDchoix_95nombre2Objects1.length = 0;
gdjs.infosCode.GDchoix_95nombre2Objects2.length = 0;
gdjs.infosCode.GDchoix_95nombre3Objects1.length = 0;
gdjs.infosCode.GDchoix_95nombre3Objects2.length = 0;
gdjs.infosCode.GDchoix_95nombre4Objects1.length = 0;
gdjs.infosCode.GDchoix_95nombre4Objects2.length = 0;
gdjs.infosCode.GDchoix_95nombre5Objects1.length = 0;
gdjs.infosCode.GDchoix_95nombre5Objects2.length = 0;
gdjs.infosCode.GDchoix_95nombre6Objects1.length = 0;
gdjs.infosCode.GDchoix_95nombre6Objects2.length = 0;
gdjs.infosCode.GDchoix_95nombre7Objects1.length = 0;
gdjs.infosCode.GDchoix_95nombre7Objects2.length = 0;
gdjs.infosCode.GDchoix_95nombre8Objects1.length = 0;
gdjs.infosCode.GDchoix_95nombre8Objects2.length = 0;
gdjs.infosCode.GDchoix_95nombre9Objects1.length = 0;
gdjs.infosCode.GDchoix_95nombre9Objects2.length = 0;
gdjs.infosCode.GDchoix_95nombre10Objects1.length = 0;
gdjs.infosCode.GDchoix_95nombre10Objects2.length = 0;
gdjs.infosCode.GDaide_95sergeObjects1.length = 0;
gdjs.infosCode.GDaide_95sergeObjects2.length = 0;
gdjs.infosCode.GDserge_95loupeObjects1.length = 0;
gdjs.infosCode.GDserge_95loupeObjects2.length = 0;
gdjs.infosCode.GDbouton_95suivant2Objects1.length = 0;
gdjs.infosCode.GDbouton_95suivant2Objects2.length = 0;
gdjs.infosCode.GDbouton_95suivant3Objects1.length = 0;
gdjs.infosCode.GDbouton_95suivant3Objects2.length = 0;
gdjs.infosCode.GDbouton_95suivantObjects1.length = 0;
gdjs.infosCode.GDbouton_95suivantObjects2.length = 0;
gdjs.infosCode.GDscore_951Objects1.length = 0;
gdjs.infosCode.GDscore_951Objects2.length = 0;
gdjs.infosCode.GDaide_95audioObjects1.length = 0;
gdjs.infosCode.GDaide_95audioObjects2.length = 0;
gdjs.infosCode.GDscore_952Objects1.length = 0;
gdjs.infosCode.GDscore_952Objects2.length = 0;
gdjs.infosCode.GDfond_95natureObjects1.length = 0;
gdjs.infosCode.GDfond_95natureObjects2.length = 0;
gdjs.infosCode.GDfond_95nature2Objects1.length = 0;
gdjs.infosCode.GDfond_95nature2Objects2.length = 0;
gdjs.infosCode.GDbouton_95retourObjects1.length = 0;
gdjs.infosCode.GDbouton_95retourObjects2.length = 0;
gdjs.infosCode.GDfond_95nature3Objects1.length = 0;
gdjs.infosCode.GDfond_95nature3Objects2.length = 0;
gdjs.infosCode.GDbarre_95progressionObjects1.length = 0;
gdjs.infosCode.GDbarre_95progressionObjects2.length = 0;
gdjs.infosCode.GDetoileObjects1.length = 0;
gdjs.infosCode.GDetoileObjects2.length = 0;
gdjs.infosCode.GDfond_95blancObjects1.length = 0;
gdjs.infosCode.GDfond_95blancObjects2.length = 0;
gdjs.infosCode.GDinfosObjects1.length = 0;
gdjs.infosCode.GDinfosObjects2.length = 0;

gdjs.infosCode.eventsList0(runtimeScene);
return;

}

gdjs['infosCode'] = gdjs.infosCode;
