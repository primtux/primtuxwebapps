gdjs.New_32sceneCode = {};
gdjs.New_32sceneCode.GDscoreObjects1= [];
gdjs.New_32sceneCode.GDscoreObjects2= [];
gdjs.New_32sceneCode.GDscore_953Objects1= [];
gdjs.New_32sceneCode.GDscore_953Objects2= [];
gdjs.New_32sceneCode.GDnombreObjects1= [];
gdjs.New_32sceneCode.GDnombreObjects2= [];

gdjs.New_32sceneCode.conditionTrue_0 = {val:false};
gdjs.New_32sceneCode.condition0IsTrue_0 = {val:false};
gdjs.New_32sceneCode.condition1IsTrue_0 = {val:false};
gdjs.New_32sceneCode.conditionTrue_1 = {val:false};
gdjs.New_32sceneCode.condition0IsTrue_1 = {val:false};
gdjs.New_32sceneCode.condition1IsTrue_1 = {val:false};


gdjs.New_32sceneCode.eventsList0 = function(runtimeScene) {

{



}


{



}


{


gdjs.New_32sceneCode.condition0IsTrue_0.val = false;
{
{gdjs.New_32sceneCode.conditionTrue_1 = gdjs.New_32sceneCode.condition0IsTrue_0;
gdjs.New_32sceneCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(16674212);
}
}if (gdjs.New_32sceneCode.condition0IsTrue_0.val) {
}

}


{


{
gdjs.copyArray(runtimeScene.getObjects("nombre"), gdjs.New_32sceneCode.GDnombreObjects1);
{for(var i = 0, len = gdjs.New_32sceneCode.GDnombreObjects1.length ;i < len;++i) {
    gdjs.New_32sceneCode.GDnombreObjects1[i].setBBText(gdjs.evtTools.variable.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(7).getChild("milliers")) + " " + gdjs.evtTools.variable.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(7).getChild("unites")));
}
}}

}


};

gdjs.New_32sceneCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.New_32sceneCode.GDscoreObjects1.length = 0;
gdjs.New_32sceneCode.GDscoreObjects2.length = 0;
gdjs.New_32sceneCode.GDscore_953Objects1.length = 0;
gdjs.New_32sceneCode.GDscore_953Objects2.length = 0;
gdjs.New_32sceneCode.GDnombreObjects1.length = 0;
gdjs.New_32sceneCode.GDnombreObjects2.length = 0;

gdjs.New_32sceneCode.eventsList0(runtimeScene);
return;

}

gdjs['New_32sceneCode'] = gdjs.New_32sceneCode;
