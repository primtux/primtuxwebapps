(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var MotsATrouver = /** @class */ (function () {
        function MotsATrouver() {
        }
        MotsATrouver.Liste = [
"SOURIS",
"POISSON",
"PANTALON",
"COUVERTURE",
"GOMME",
"ECOLE",
"LIVRE",
"MAITRE",
"TROUSSE",
"POESIE",
"FACILE",
"DICTEE",
"ECRITURE",
"GROUPE",
"STYLO",
"MATELOT",
"NAVIRE",
"POISSON",
"PLAGE",
"MATELOT",
"RIVAGE",
"SABLE",
"SARDINE",
"CRABE",
"OTARIE",
"MEDUSE",
"POULPE",
"MORUE",
"CREVETTE",
"COSTUME",
"INVITE",
"FESTIN",
"MARIAGE",
"MUSIQUE",
"FOIRE",
"JOIE",
"DOUCHE",
"PLONGER",
"BONNET",
"BASSIN",
"TOBOGGAN",
"FRITE",
"ECHELLE",
"REGARDER",
"DEVINER",
"ROULER",
"PORTER",
"AVANCER",
"MANGER",
"POUSSER",
"FANTOME",
"COSTUME",
"BONBON",
"POTIRON",
"DEMON",
"LANTERNE",
"BALAI",
        ];
        return MotsATrouver;
    }());
    exports.default = MotsATrouver;
});
/* Si vous trouvez ce fichier, et c'est très facile, ne le dites pas.
C'est le code initial de sutom qui code les solutions dans un fichier.
Je vais essayer de corriger (ou refaire) */ 
//# sourceMappingURL=motsATrouver.js.map