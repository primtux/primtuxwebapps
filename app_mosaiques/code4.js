gdjs.infosCode = {};
gdjs.infosCode.GDswitchObjects1= [];
gdjs.infosCode.GDswitchObjects2= [];
gdjs.infosCode.GDbouton_95retourObjects1= [];
gdjs.infosCode.GDbouton_95retourObjects2= [];
gdjs.infosCode.GDfaux_95vraiObjects1= [];
gdjs.infosCode.GDfaux_95vraiObjects2= [];
gdjs.infosCode.GDbouton_95precedentObjects1= [];
gdjs.infosCode.GDbouton_95precedentObjects2= [];
gdjs.infosCode.GDbouton_95suivantObjects1= [];
gdjs.infosCode.GDbouton_95suivantObjects2= [];
gdjs.infosCode.GDcase_95aObjects1= [];
gdjs.infosCode.GDcase_95aObjects2= [];
gdjs.infosCode.GDscore_95Objects1= [];
gdjs.infosCode.GDscore_95Objects2= [];
gdjs.infosCode.GDcase_95erreurObjects1= [];
gdjs.infosCode.GDcase_95erreurObjects2= [];
gdjs.infosCode.GDfond2Objects1= [];
gdjs.infosCode.GDfond2Objects2= [];
gdjs.infosCode.GDtriangle_95a_95Objects1= [];
gdjs.infosCode.GDtriangle_95a_95Objects2= [];
gdjs.infosCode.GDtriangle_95b_95Objects1= [];
gdjs.infosCode.GDtriangle_95b_95Objects2= [];
gdjs.infosCode.GDtriangle_95c_95Objects1= [];
gdjs.infosCode.GDtriangle_95c_95Objects2= [];
gdjs.infosCode.GDtriangle_95d_95Objects1= [];
gdjs.infosCode.GDtriangle_95d_95Objects2= [];
gdjs.infosCode.GDcadre_95motifObjects1= [];
gdjs.infosCode.GDcadre_95motifObjects2= [];
gdjs.infosCode.GDfondObjects1= [];
gdjs.infosCode.GDfondObjects2= [];
gdjs.infosCode.GDfond_95blancObjects1= [];
gdjs.infosCode.GDfond_95blancObjects2= [];
gdjs.infosCode.GDinfosObjects1= [];
gdjs.infosCode.GDinfosObjects2= [];

gdjs.infosCode.conditionTrue_0 = {val:false};
gdjs.infosCode.condition0IsTrue_0 = {val:false};
gdjs.infosCode.condition1IsTrue_0 = {val:false};
gdjs.infosCode.condition2IsTrue_0 = {val:false};


gdjs.infosCode.mapOfGDgdjs_46infosCode_46GDbouton_9595retourObjects1Objects = Hashtable.newFrom({"bouton_retour": gdjs.infosCode.GDbouton_95retourObjects1});
gdjs.infosCode.eventsList0 = function(runtimeScene) {

{


gdjs.infosCode.condition0IsTrue_0.val = false;
{
gdjs.infosCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.infosCode.condition0IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("fond_blanc"), gdjs.infosCode.GDfond_95blancObjects1);
{for(var i = 0, len = gdjs.infosCode.GDfond_95blancObjects1.length ;i < len;++i) {
    gdjs.infosCode.GDfond_95blancObjects1[i].setOpacity(200);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_retour"), gdjs.infosCode.GDbouton_95retourObjects1);

gdjs.infosCode.condition0IsTrue_0.val = false;
gdjs.infosCode.condition1IsTrue_0.val = false;
{
gdjs.infosCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.infosCode.mapOfGDgdjs_46infosCode_46GDbouton_9595retourObjects1Objects, runtimeScene, true, false);
}if ( gdjs.infosCode.condition0IsTrue_0.val ) {
{
gdjs.infosCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}}
if (gdjs.infosCode.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "menu", false);
}}

}


{


{
}

}


};

gdjs.infosCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.infosCode.GDswitchObjects1.length = 0;
gdjs.infosCode.GDswitchObjects2.length = 0;
gdjs.infosCode.GDbouton_95retourObjects1.length = 0;
gdjs.infosCode.GDbouton_95retourObjects2.length = 0;
gdjs.infosCode.GDfaux_95vraiObjects1.length = 0;
gdjs.infosCode.GDfaux_95vraiObjects2.length = 0;
gdjs.infosCode.GDbouton_95precedentObjects1.length = 0;
gdjs.infosCode.GDbouton_95precedentObjects2.length = 0;
gdjs.infosCode.GDbouton_95suivantObjects1.length = 0;
gdjs.infosCode.GDbouton_95suivantObjects2.length = 0;
gdjs.infosCode.GDcase_95aObjects1.length = 0;
gdjs.infosCode.GDcase_95aObjects2.length = 0;
gdjs.infosCode.GDscore_95Objects1.length = 0;
gdjs.infosCode.GDscore_95Objects2.length = 0;
gdjs.infosCode.GDcase_95erreurObjects1.length = 0;
gdjs.infosCode.GDcase_95erreurObjects2.length = 0;
gdjs.infosCode.GDfond2Objects1.length = 0;
gdjs.infosCode.GDfond2Objects2.length = 0;
gdjs.infosCode.GDtriangle_95a_95Objects1.length = 0;
gdjs.infosCode.GDtriangle_95a_95Objects2.length = 0;
gdjs.infosCode.GDtriangle_95b_95Objects1.length = 0;
gdjs.infosCode.GDtriangle_95b_95Objects2.length = 0;
gdjs.infosCode.GDtriangle_95c_95Objects1.length = 0;
gdjs.infosCode.GDtriangle_95c_95Objects2.length = 0;
gdjs.infosCode.GDtriangle_95d_95Objects1.length = 0;
gdjs.infosCode.GDtriangle_95d_95Objects2.length = 0;
gdjs.infosCode.GDcadre_95motifObjects1.length = 0;
gdjs.infosCode.GDcadre_95motifObjects2.length = 0;
gdjs.infosCode.GDfondObjects1.length = 0;
gdjs.infosCode.GDfondObjects2.length = 0;
gdjs.infosCode.GDfond_95blancObjects1.length = 0;
gdjs.infosCode.GDfond_95blancObjects2.length = 0;
gdjs.infosCode.GDinfosObjects1.length = 0;
gdjs.infosCode.GDinfosObjects2.length = 0;

gdjs.infosCode.eventsList0(runtimeScene);
return;

}

gdjs['infosCode'] = gdjs.infosCode;
