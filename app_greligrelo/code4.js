gdjs.infosCode = {};
gdjs.infosCode.GDinfosObjects1= [];
gdjs.infosCode.GDinfosObjects2= [];
gdjs.infosCode.GDspr_9595niv1Objects1= [];
gdjs.infosCode.GDspr_9595niv1Objects2= [];
gdjs.infosCode.GDspr_9595niv2Objects1= [];
gdjs.infosCode.GDspr_9595niv2Objects2= [];
gdjs.infosCode.GDspr_9595chiffre1Objects1= [];
gdjs.infosCode.GDspr_9595chiffre1Objects2= [];
gdjs.infosCode.GDspr_9595chiffre5Objects1= [];
gdjs.infosCode.GDspr_9595chiffre5Objects2= [];
gdjs.infosCode.GDspr_9595chiffre4Objects1= [];
gdjs.infosCode.GDspr_9595chiffre4Objects2= [];
gdjs.infosCode.GDspr_9595chiffre3Objects1= [];
gdjs.infosCode.GDspr_9595chiffre3Objects2= [];
gdjs.infosCode.GDspr_9595chiffre2Objects1= [];
gdjs.infosCode.GDspr_9595chiffre2Objects2= [];
gdjs.infosCode.GDspr_9595chiffre6Objects1= [];
gdjs.infosCode.GDspr_9595chiffre6Objects2= [];
gdjs.infosCode.GDspr_9595chiffre7Objects1= [];
gdjs.infosCode.GDspr_9595chiffre7Objects2= [];
gdjs.infosCode.GDspr_9595chiffre8Objects1= [];
gdjs.infosCode.GDspr_9595chiffre8Objects2= [];
gdjs.infosCode.GDspr_9595chiffre9Objects1= [];
gdjs.infosCode.GDspr_9595chiffre9Objects2= [];
gdjs.infosCode.GDspr_9595chiffre10Objects1= [];
gdjs.infosCode.GDspr_9595chiffre10Objects2= [];
gdjs.infosCode.GDspr_9595score2Objects1= [];
gdjs.infosCode.GDspr_9595score2Objects2= [];
gdjs.infosCode.GDspr_9595score1Objects1= [];
gdjs.infosCode.GDspr_9595score1Objects2= [];
gdjs.infosCode.GDspr_9595bouton_9595retourObjects1= [];
gdjs.infosCode.GDspr_9595bouton_9595retourObjects2= [];
gdjs.infosCode.GDfond_9595vertObjects1= [];
gdjs.infosCode.GDfond_9595vertObjects2= [];
gdjs.infosCode.GDspr_9595bouton_9595recommencerObjects1= [];
gdjs.infosCode.GDspr_9595bouton_9595recommencerObjects2= [];
gdjs.infosCode.GDspr_9595bouton_9595switchObjects1= [];
gdjs.infosCode.GDspr_9595bouton_9595switchObjects2= [];
gdjs.infosCode.GDsp_9595bille_9595bleueObjects1= [];
gdjs.infosCode.GDsp_9595bille_9595bleueObjects2= [];
gdjs.infosCode.GDsp_9595bille_9595rougeObjects1= [];
gdjs.infosCode.GDsp_9595bille_9595rougeObjects2= [];
gdjs.infosCode.GDspr_9595bouton_9595suivantObjects1= [];
gdjs.infosCode.GDspr_9595bouton_9595suivantObjects2= [];
gdjs.infosCode.GDsp_9595faux_9595vraiObjects1= [];
gdjs.infosCode.GDsp_9595faux_9595vraiObjects2= [];
gdjs.infosCode.GDsp_9595mainsObjects1= [];
gdjs.infosCode.GDsp_9595mainsObjects2= [];
gdjs.infosCode.GDsp_9595fleche_9595billeObjects1= [];
gdjs.infosCode.GDsp_9595fleche_9595billeObjects2= [];


gdjs.infosCode.mapOfGDgdjs_9546infosCode_9546GDspr_95959595bouton_95959595retourObjects1Objects = Hashtable.newFrom({"spr_bouton_retour": gdjs.infosCode.GDspr_9595bouton_9595retourObjects1});
gdjs.infosCode.eventsList0 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/audio_vide.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("spr_bouton_retour"), gdjs.infosCode.GDspr_9595bouton_9595retourObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.infosCode.mapOfGDgdjs_9546infosCode_9546GDspr_95959595bouton_95959595retourObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(10491460);
}
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "menu", false);
}{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/bulle_1.mp3", 1, false, 100, 1);
}}

}


};

gdjs.infosCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.infosCode.GDinfosObjects1.length = 0;
gdjs.infosCode.GDinfosObjects2.length = 0;
gdjs.infosCode.GDspr_9595niv1Objects1.length = 0;
gdjs.infosCode.GDspr_9595niv1Objects2.length = 0;
gdjs.infosCode.GDspr_9595niv2Objects1.length = 0;
gdjs.infosCode.GDspr_9595niv2Objects2.length = 0;
gdjs.infosCode.GDspr_9595chiffre1Objects1.length = 0;
gdjs.infosCode.GDspr_9595chiffre1Objects2.length = 0;
gdjs.infosCode.GDspr_9595chiffre5Objects1.length = 0;
gdjs.infosCode.GDspr_9595chiffre5Objects2.length = 0;
gdjs.infosCode.GDspr_9595chiffre4Objects1.length = 0;
gdjs.infosCode.GDspr_9595chiffre4Objects2.length = 0;
gdjs.infosCode.GDspr_9595chiffre3Objects1.length = 0;
gdjs.infosCode.GDspr_9595chiffre3Objects2.length = 0;
gdjs.infosCode.GDspr_9595chiffre2Objects1.length = 0;
gdjs.infosCode.GDspr_9595chiffre2Objects2.length = 0;
gdjs.infosCode.GDspr_9595chiffre6Objects1.length = 0;
gdjs.infosCode.GDspr_9595chiffre6Objects2.length = 0;
gdjs.infosCode.GDspr_9595chiffre7Objects1.length = 0;
gdjs.infosCode.GDspr_9595chiffre7Objects2.length = 0;
gdjs.infosCode.GDspr_9595chiffre8Objects1.length = 0;
gdjs.infosCode.GDspr_9595chiffre8Objects2.length = 0;
gdjs.infosCode.GDspr_9595chiffre9Objects1.length = 0;
gdjs.infosCode.GDspr_9595chiffre9Objects2.length = 0;
gdjs.infosCode.GDspr_9595chiffre10Objects1.length = 0;
gdjs.infosCode.GDspr_9595chiffre10Objects2.length = 0;
gdjs.infosCode.GDspr_9595score2Objects1.length = 0;
gdjs.infosCode.GDspr_9595score2Objects2.length = 0;
gdjs.infosCode.GDspr_9595score1Objects1.length = 0;
gdjs.infosCode.GDspr_9595score1Objects2.length = 0;
gdjs.infosCode.GDspr_9595bouton_9595retourObjects1.length = 0;
gdjs.infosCode.GDspr_9595bouton_9595retourObjects2.length = 0;
gdjs.infosCode.GDfond_9595vertObjects1.length = 0;
gdjs.infosCode.GDfond_9595vertObjects2.length = 0;
gdjs.infosCode.GDspr_9595bouton_9595recommencerObjects1.length = 0;
gdjs.infosCode.GDspr_9595bouton_9595recommencerObjects2.length = 0;
gdjs.infosCode.GDspr_9595bouton_9595switchObjects1.length = 0;
gdjs.infosCode.GDspr_9595bouton_9595switchObjects2.length = 0;
gdjs.infosCode.GDsp_9595bille_9595bleueObjects1.length = 0;
gdjs.infosCode.GDsp_9595bille_9595bleueObjects2.length = 0;
gdjs.infosCode.GDsp_9595bille_9595rougeObjects1.length = 0;
gdjs.infosCode.GDsp_9595bille_9595rougeObjects2.length = 0;
gdjs.infosCode.GDspr_9595bouton_9595suivantObjects1.length = 0;
gdjs.infosCode.GDspr_9595bouton_9595suivantObjects2.length = 0;
gdjs.infosCode.GDsp_9595faux_9595vraiObjects1.length = 0;
gdjs.infosCode.GDsp_9595faux_9595vraiObjects2.length = 0;
gdjs.infosCode.GDsp_9595mainsObjects1.length = 0;
gdjs.infosCode.GDsp_9595mainsObjects2.length = 0;
gdjs.infosCode.GDsp_9595fleche_9595billeObjects1.length = 0;
gdjs.infosCode.GDsp_9595fleche_9595billeObjects2.length = 0;

gdjs.infosCode.eventsList0(runtimeScene);

return;

}

gdjs['infosCode'] = gdjs.infosCode;
