gdjs.introCode = {};
gdjs.introCode.GDNewSpriteObjects1= [];
gdjs.introCode.GDNewSpriteObjects2= [];
gdjs.introCode.GDbouton_9595validerObjects1= [];
gdjs.introCode.GDbouton_9595validerObjects2= [];


gdjs.introCode.mapOfGDgdjs_9546introCode_9546GDbouton_95959595validerObjects1Objects = Hashtable.newFrom({"bouton_valider": gdjs.introCode.GDbouton_9595validerObjects1});
gdjs.introCode.eventsList0 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("bouton_valider"), gdjs.introCode.GDbouton_9595validerObjects1);
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "chrono");
}{for(var i = 0, len = gdjs.introCode.GDbouton_9595validerObjects1.length ;i < len;++i) {
    gdjs.introCode.GDbouton_9595validerObjects1[i].hide();
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "chrono") >= 2;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(13018220);
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("bouton_valider"), gdjs.introCode.GDbouton_9595validerObjects1);
{for(var i = 0, len = gdjs.introCode.GDbouton_9595validerObjects1.length ;i < len;++i) {
    gdjs.introCode.GDbouton_9595validerObjects1[i].hide(false);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_valider"), gdjs.introCode.GDbouton_9595validerObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.introCode.mapOfGDgdjs_9546introCode_9546GDbouton_95959595validerObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "menu", false);
}}

}


};

gdjs.introCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.introCode.GDNewSpriteObjects1.length = 0;
gdjs.introCode.GDNewSpriteObjects2.length = 0;
gdjs.introCode.GDbouton_9595validerObjects1.length = 0;
gdjs.introCode.GDbouton_9595validerObjects2.length = 0;

gdjs.introCode.eventsList0(runtimeScene);

return;

}

gdjs['introCode'] = gdjs.introCode;
