// Seyes est un logiciel libre
// sous licence GNU GPL
// écrit par Arnaud Champollion
// Il fait partie de la suite Éducajou

let prefixeAppli = 'seyes';

// Est-on dans Openboard ? ////
let openboard = Boolean(window.widget || window.sankore);
console.log('Openboard ? '+openboard);
if (openboard){
    document.body.classList.add('openboard');
}

// Constantes - Éléments DOM



// Structure
const body = document.body;
const site = document.documentElement;
const divApropos = document.getElementById('apropos');
console.log(divApropos); // Cela ne sera pas null
const darkbox = document.getElementById('darkbox');


// En-tête optionnel (mode cahier de textes)
const divEntete=document.getElementById('entete');
    const divMargeEntete=document.getElementById('marge-entete');
        const case1=document.getElementById('case1');
        const case2=document.getElementById('case2');
    const case3=document.getElementById('case3');

// Contenu général
const divContenu=document.getElementById('contenu');
    const divMarge=document.getElementById('marge');
    const voile_marge = document.getElementById('voile_marge');
        const marge=document.getElementById('marge1');
            const texte_marge = document.getElementById('texte_marge');
        const margeSecondaire= document.getElementById('marge2');
            const voile_marge2 = document.getElementById('voile_marge2');
            const texte_marge_complementaire = document.getElementById('texte_marge_complementaire');
    const page=document.getElementById('page');
    const voile_page = document.getElementById('voile_page');
        const graphismes=document.getElementById('graphismes');
        const texte_principal=document.getElementById('texte_principal');
        const texte_apropos = document.getElementById('texte_apropos');
    const divPanneauOptions=document.getElementById('panneauOptions');



// Boutons
const barre = document.getElementById('barre');
const confirmationNouveau=document.getElementById('confirmation-nouveau');
const fullscreen=document.getElementById('fullscreen');
const bouton_a_propos=document.getElementById('a_propos');
const bouton_ligatures=document.getElementById('bouton_ligatures');
const bouton_copier=document.getElementById('copier');
const choix_police = document.getElementById('choix_police');
const choixCouleurEntete = document.getElementById('choix-couleur-entete');
const couleurTexte = document.getElementById('couleur-texte');
const inputEspacement = document.getElementById('inputEspacement');
const inputEspacementMots = document.getElementById('inputEspacementMots');

const inputOpacite = document.getElementById('inputOpacite');
const checkboxAjouterDate = document.getElementById('checkboxAjouterDate');
const checkboxSoulignerDate = document.getElementById('checkboxSoulignerDate');

const zone_menu_police = document.getElementById('zone_menu_police');
const zone_menu_copie_lien = document.getElementById('zone_menu_copie_lien');
const zone_lien = document.getElementById('zone_lien');


// Réglages de départ
bouton_ligatures.style.display='none';


// Autres constantes
const isWindows = /Win/i.test(navigator.userAgent);
console.log("Sommes-nous sur Windows ? "+isWindows);

// jours et couleurs

let jours = {
    lundi: "#ed284c",
    mardi: "#56f068",
    mercredi: "#53b8e0",
    jeudi: "#e69130",
    vendredi: "#6171ff",
    samedi: "#85ff5c",
};

let jourActifDepart = document.getElementById("select-jours").value;
// Appliquer la couleur en arrière-plan de divEntete
let couleurDepart = jours[jourActifDepart];
console.log('couleur départ '+couleurDepart)
choixCouleurEntete.value = couleurDepart;
divEntete.style.backgroundColor = couleurDepart
choixCouleurEntete.style.backgroundColor = couleurDepart;
adapteCouleurBordureChoixCouleur();




// Pour les ligatures
let signes=['═','║']
let subsitutions=[
    ['on','═'],
    ['or','║'],
    ['s ','╝ '],
    ['s\\.','╝.'],
    ['s,','╝,'],
    ['s;','╝;'],
    ['s:','╝:'],
    ['s!','╝!'],
    ['s/','╝/'],
    ['s-','╝-'],
    ["s'","╝'"],
    [' p',' ╰'],
    [' j',' ╮'],
    [' s',' ╠'],
    [' i',' ╱']
]

let polices=['Arial','AA Cursive','Open Dyslexic','Belle Allure GS','Belle Allure CE','Belle Allure CM','Luciole'];

// Objet qui contient les propriétés pour chaque police
const typesPolices = {
    "Arial": {
        facteur_taille_police: 0.6,
        epaisseur_police: 'normal',
        coef_marge: 0.28,
        coef_marge_windows: 0.25,
        distance_soulignage: 0.3,
        ligatures: [1, 0],
        bouton_ligatures_display: 'none'
    },
    "AA Cursive": {
        facteur_taille_police: 0.75,
        epaisseur_police: 'normal',
        coef_marge: 0.35,
        coef_marge_windows: 0.3125,
        distance_soulignage: 0.3,
        ligatures: [0, 1],
        bouton_ligatures_display: null
    },
    "Open Dyslexic": {
        facteur_taille_police: 0.45,
        epaisseur_police: 'normal',
        coef_marge: 0.35,
        coef_marge_windows: 0.35,
        distance_soulignage: 0.5,
        ligatures: [1, 0],
        bouton_ligatures_display: 'none'
    },
    "Belle Allure CE": {
        facteur_taille_police: 0.6,
        epaisseur_police: 'bold',
        coef_marge: 0.375,
        coef_marge_windows: 0.334821429,
        distance_soulignage: 0.38,
        ligatures: [1, 0],
        bouton_ligatures_display: 'none'
    },
    "Belle Allure CM": {
        facteur_taille_police: 0.6,
        epaisseur_police: 'bold',
        coef_marge: 0.375,
        coef_marge_windows: 0.334821429,
        distance_soulignage: 0.38,
        ligatures: [1, 0],
        bouton_ligatures_display: 'none'
    },
    "Belle Allure GS": {
        facteur_taille_police: 0.6,
        epaisseur_police: 'bold',
        coef_marge: 0.375,
        coef_marge_windows: 0.334821429,
        distance_soulignage: 0.38,
        ligatures: [1, 0],
        bouton_ligatures_display: 'none'
    },
    "Acceseditionscursive": {
        facteur_taille_police: 0.674,
        epaisseur_police: 'normal',
        coef_marge: 0.272,
        coef_marge_windows: 0.242857143,
        distance_soulignage: 0.379,
        ligatures: [1, 0],
        bouton_ligatures_display: 'none'
    },
    "Luciole": {
        facteur_taille_police: 0.65,
        epaisseur_police: 'normal',
        coef_marge: 0.29,
        coef_marge_windows: 0.258928572,
        distance_soulignage: 0.379,
        ligatures: [1, 0],
        bouton_ligatures_display: 'none'
    }
};

// Variables globales
let largeur_carreau=80;
let largeur_marge_secondaire = largeur_carreau * 2.7;
let largeur_carreau_old = largeur_carreau;
let largeur_marge = 200;
let facteur_taille_police=0.75;
let taille_police=60;
let epaisseur_police='normal';
let pleinecran_on=false;
let a_propos_on=false;
let redim=false;
let ligatures_on=false;
let dragged=null;
let coef_marge=0.35;
let coef_marge_windows=0.3125;
let distance_soulignage=0.3;
let focus_texte=null;
let police_active='Belle Allure CE'
let typeCarreaux='seyes';
let opaciteLignes=1;
let regleActive=false;
let isDrawing = false;
let startX, startY;  // Position de départ du trait
let lineDiv = null;  // La div représentant le trait
let divDate = null;
let contenuGraphismes='';
let contenuTextePrincipal='';
let contenuTexteMarge='';
let contenuTexteMargeComplementaire='';
let dateDuDoc=false;
let espacementLettres = 0;
let espacementMots = 0;
let ajouterLaDate = false;
let soulignerLaDate = false;

function isNotNullOrUndefined(value) {
    return value !== null && value !== undefined;
}



///////////////////// Lancement du programme ///////////////////
async function executeFunctions() {   
    await checkReglages();
    await appliqueReglages();
}
executeFunctions();
//////////////////////////////////////////////////////////////

async function checkReglages() {
    console.log('--- Lecture URL et stockage local');
    let url = window.location.search;
    let urlParams = new URLSearchParams(url);
    let valeurARecuperer;


    let primtux = urlParams.get('primtuxmenu');
    if (primtux){
        body.classList.add('primtux');
    }

    valeurARecuperer = urlParams.get('seyes-couleurs-jours') || await litDepuisStockage('seyes-couleurs-jours');
    if (valeurARecuperer) {
        jours = JSON.parse(valeurARecuperer);
    }

    valeurARecuperer = urlParams.get('seyes-police') || await litDepuisStockage('seyes-police');
    if (valeurARecuperer) {
        police_active = valeurARecuperer;
    }

    valeurARecuperer = urlParams.get('largeur-marge') || await litDepuisStockage('largeur-marge');
    if (valeurARecuperer) {
        console.log('---LARGEUR MARGE',valeurARecuperer)
        largeur_marge = parseFloat(valeurARecuperer);
    }

    valeurARecuperer = urlParams.get('largeur-marge-secondaire') || await litDepuisStockage('largeur-marge-secondaire');
    if (valeurARecuperer) {
        largeur_marge_secondaire = parseFloat(valeurARecuperer);
    }

    valeurARecuperer = urlParams.get('graphismes') || await litDepuisStockage('graphismes');
    if (valeurARecuperer) {
        contenuGraphismes = valeurARecuperer;
    }

    valeurARecuperer = urlParams.get('texte-principal') || await litDepuisStockage('texte-principal');
    if (valeurARecuperer !='' && valeurARecuperer !='undefined' && valeurARecuperer !=null) {
        console.log('texte récupéré',valeurARecuperer)
        contenuTextePrincipal = valeurARecuperer;
    } else {
        console.log('Premiière utiilisation de SEYES');
        contenuTextePrincipal = texteDebut();
    }

    valeurARecuperer = urlParams.get('texte-marge') || await litDepuisStockage('texte-marge');
    if (valeurARecuperer) {
        contenuTexteMarge = valeurARecuperer;
    }

    valeurARecuperer = urlParams.get('texte-marge-complementaire') || await litDepuisStockage('texte-marge-complementaire');
    if (valeurARecuperer) {
        contenuTexteMargeComplementaire = valeurARecuperer;
    }

    valeurARecuperer = urlParams.get('type-carreaux') || await litDepuisStockage('type-carreaux');
    if (valeurARecuperer) {
        typeCarreaux = valeurARecuperer;
    }

    valeurARecuperer = urlParams.get('opacite-lignes') || await litDepuisStockage('opacite-lignes');
    if (valeurARecuperer) {
        opaciteLignes = parseFloat(valeurARecuperer);
    }

    valeurARecuperer = urlParams.get('largeur-carreau') || await litDepuisStockage('largeur-carreau');
    if (valeurARecuperer) {
        largeur_carreau = parseInt(valeurARecuperer);
    }

    valeurARecuperer = urlParams.get('espacement-lettres') || await litDepuisStockage('espacement-lettres');
    if (valeurARecuperer) {
        espacementLettres = parseInt(valeurARecuperer);
    }

    valeurARecuperer = urlParams.get('espacement-mots') || await litDepuisStockage('espacement-mots');
    if (valeurARecuperer) {
        espacementMots = parseInt(valeurARecuperer);
    }

    valeurARecuperer = urlParams.get('ajouter-date') || await litDepuisStockage('ajouter-date');
    if (valeurARecuperer) {
        ajouterLaDate = valeurARecuperer === true;
    }

    valeurARecuperer = urlParams.get('souligner-date') || await litDepuisStockage('souligner-date');
    if (valeurARecuperer) {
        soulignerLaDate = valeurARecuperer === true;
    }
         

}

function texteDebut() {
    let date = calculeDate();
    return `
        <div id="divDate"><span>${date}</span></div>
        <br>
        Modifiez-moi...    
    `;
}

function appliqueReglages() {

    console.log('---- Application des réglages ----'); 

    let jourActif = document.getElementById("select-jours").value;
    divEntete.style.backgroundColor = jours[jourActif];
    choixCouleurEntete.value = choixCouleurEntete.style.backgroundColor = jours[jourActif];
    inputEspacement.value = espacementLettres;
    inputEspacementMots.value = espacementMots;
    adapteCouleurBordureChoixCouleur();

    change_police(police_active);
    console.log('largeur marge',largeur_marge)
    regleMarge(largeur_marge);

    regleMargeSecondaire(largeur_marge_secondaire);

    changeCarreaux(typeCarreaux);

    opacite(opaciteLignes);

    zoom(false,largeur_carreau,police_active);

    changeModeDate();

    raz();

    checkHash();

    changeEspacementLettres(espacementLettres);

    changeEspacementMots(espacementMots);

    opacite(opaciteLignes);
    

    if(contenuTextePrincipal!='""' && contenuTextePrincipal!=null){console.log('**********');texte_principal.innerHTML = contenuTextePrincipal;}
    if(contenuTexteMarge!='""'){texte_marge.innerHTML = contenuTexteMarge;}
    if(contenuTexteMargeComplementaire!='""'){texte_marge_complementaire.innerHTML = contenuTexteMargeComplementaire;}
    if(contenuGraphismes!='""'){graphismes.innerHTML = contenuGraphismes;}

}


 
function changeModeDate() {
    ajouterLaDate = checkboxAjouterDate.checked;
    soulignerLaDate = checkboxSoulignerDate.checked;
    stocke('ajouter-date',ajouterLaDate);
    stocke('souligner-date',soulignerLaDate);
    const divDate = document.getElementById('divDate')
    if (divDate) {
        if (soulignerLaDate) {
            divDate.querySelector('span').style.textDecoration = 'underline red';
        } else {
            divDate.querySelector('span').style.textDecoration = null;
        }
    }

}
   

 function changeEspacementLettres(valeur) {
    espacementLettres = parseFloat(valeur);
    stocke('espacement-lettres',espacementLettres);
    texte_principal.style.letterSpacing = valeur/100 + 'em';
    if (espacementLettres === 0 ) {
        document.documentElement.style.setProperty('--ligatures', '"calt" 1');
    } else {
        document.documentElement.style.setProperty('--ligatures', '"calt" 0');
    }
 }

 function changeEspacementMots(valeur) {

    espacementMots = parseFloat(valeur);
    stocke('espacement-mots',espacementMots);

    if (espacementMots === 0) {
        texte_principal.style.wordSpacing = 'normal';
        texte_marge.style.wordSpacing = 'normal';
        texte_marge_complementaire.style.wordSpacing = 'normal';
    } else {
        texte_principal.style.wordSpacing = valeur + 'em';
        texte_marge.style.wordSpacing = valeur + 'em';
        texte_marge_complementaire.style.wordSpacing = valeur + 'em';
    }

 }
            







function align(cote) {
    // Récupérer la sélection de l'utilisateur
    const selection = document.getSelection();

    // Vérifier qu'il y a bien une sélection
    if (selection.rangeCount > 0) {
        // Récupérer le premier élément du range sélectionné
        let element = selection.anchorNode;

        // Remonter jusqu'à trouver le premier parent de type DIV
        while (element && element.tagName !== "DIV") {
            element = element.parentNode;
        }

        // S'assurer qu'on est bien sur un DIV avant d'appliquer l'alignement
        if (element) {
            element.style.textAlign = cote;
        } else {
            console.warn("Aucun élément de type DIV trouvé");
        }
    } else {
        console.warn("Aucune sélection trouvée");
    }
}

function changeCouleurEntete(couleur) {
    console.log('-------changement couleur en-tete-----')
    // Récupérer le jour sélectionné dans le select
    let jourActif = document.getElementById("select-jours").value;

    // Mettre à jour la couleur dans l'objet jours pour le jour sélectionné
    jours[jourActif] = couleur;

    // Appliquer la couleur en arrière-plan de divEntete
    divEntete.style.backgroundColor = couleur;
    choixCouleurEntete.style.backgroundColor = couleur;
    adapteCouleurBordureChoixCouleur();

    ;
}

function changeJour(jour) {
    // Récupérer la couleur du jour depuis l'objet jours
    let couleur = jours[jour];

    // Appliquer la couleur en arrière-plan de divEntete
    divEntete.style.backgroundColor = couleur;
    choixCouleurEntete.value = choixCouleurEntete.style.backgroundColor = couleur;
    adapteCouleurBordureChoixCouleur();
    
}

function adapteCouleurBordureChoixCouleur() {
    let couleurFond = choixCouleurEntete.value;
    
    // Convertir la couleur HEX en RGB
    function hexToRgb(hex) {
        let r = 0, g = 0, b = 0;

        // Si la couleur est définie en hexadécimal
        if (hex.length === 4) {
            r = parseInt(hex[1] + hex[1], 16);
            g = parseInt(hex[2] + hex[2], 16);
            b = parseInt(hex[3] + hex[3], 16);
        } else if (hex.length === 7) {
            r = parseInt(hex[1] + hex[2], 16);
            g = parseInt(hex[3] + hex[4], 16);
            b = parseInt(hex[5] + hex[6], 16);
        }

        return [r, g, b];
    }

    // Calculer la luminosité de la couleur
    function luminosite(rgb) {
        return (0.299 * rgb[0] + 0.587 * rgb[1] + 0.114 * rgb[2]);
    }

    // Changer la couleur de la bordure selon la luminosité
    let rgb = hexToRgb(couleurFond);
    let lumi = luminosite(rgb);
    
    // Si la luminosité est inférieure à un seuil, mettre la bordure en blanc
    if (lumi < 128) {
        choixCouleurEntete.style.borderColor = "white"; // Couleur de bordure blanche
    } else {
        choixCouleurEntete.style.borderColor = "black"; // Couleur de bordure noire
    }
}

function regleMarge(largeur) {
    largeur_marge = largeur;
    page.style.width = case3.style.width = 'calc(100% - ' + (largeur_marge) + 'px)';
    divMarge.style.width = (largeur_marge - 3) + 'px';
    divMargeEntete.style.width =  (largeur_marge) + 'px';
    console.log('largeur marge',largeur_marge)
    stocke('largeur-marge',largeur_marge);
}

function regleMargeSecondaire(largeur) {
    largeur_marge_secondaire = largeur;
    margeSecondaire.style.width = case2.style.width = (largeur_marge_secondaire - 3) + 'px';
    stocke('largeur-marge-secondaire',largeur_marge_secondaire);
}

function activeRegle() {
    regleActive=!regleActive;
    page.classList.toggle('trace');
    barre.classList.toggle('trace');
    document.getElementById('regle').classList.toggle('trace');

    if (regleActive){
        console.log('activation de la règle');
    }
    else {
        page.style.cursor=null;
        console.log('désactivation de la règle')
    }
}

function checkHash() {

    console.log('-----------------------Vérification du hash du lien')
   
    // Récupération de l'URL et des paramètres
    let url = window.location.search;

    // Remplacer les occurrences de '&amp;' par '&' uniquement dans les paramètres de l'URL
    if (url.includes('&amp;')) {
        url = url.replace(/&amp;/g, '&');
        console.log('Correction des &amp; dans les paramètres de l\'URL : ' + url);
    }
   
    if (window.location.hash) {
        // Enlève le '#' initial et décode le hash
        const hashValues = decodeURIComponent(window.location.hash.substring(1)).split('|_|');

        console.log(hashValues)
        
        // Vérifie que les deux parties sont bien présentes dans le hash
        if (hashValues.length === 2) {
            let textePrincipalRecupere = hashValues[0];
            let graphismesRecupere = hashValues[1];            
    
            // Met à jour le contenu des éléments avec les valeurs récupérées
            contenuTextePrincipal = textePrincipalRecupere;
            contenuGraphismes = graphismesRecupere;
        } else {
            // Si seulement une partie est présente (par exemple si le hash n'a pas "|_|"), gère cela
            contenuTextePrincipal = hashValues[0];
        }
        
    }
    
    // Nettoyage de l'URL après traitement des paramètres
    const baseUrl = window.location.protocol + "//" + window.location.host + window.location.pathname;
    
    // Remplace l'URL actuelle avec l'URL de base (sans les paramètres et sans hash) sans recharger la page
    window.history.replaceState({}, document.title, baseUrl);
    

}




function majLien() {
    // Base URL (tu peux la modifier selon tes besoins)
    let baseURL = window.location.protocol + "//" + window.location.host + window.location.pathname + '?';
    if (window.widget){baseURL='https://educajou.forge.apps.education.fr/seyes?';}

    // Construction des paramètres de l'URL en encodant les contenus des textes et les variables globales
    const params = new URLSearchParams({
        marge: texte_marge.innerHTML,
        marge_secondaire: texte_marge_complementaire.innerHTML,
        police: police_active,
        largeur_carreau: largeur_carreau,
        type_carreaux: typeCarreaux,
        opacite_lignes: opaciteLignes,
        largeur_marge: largeur_marge,
        largeur_marge_secondaire: largeur_marge_secondaire,
    });

    // Encodage du texte principal et des graphismes dans un seul hash, séparés par "|_|"
    const hash = encodeURIComponent(texte_principal.innerHTML) + '|_|' + encodeURIComponent(graphismes.innerHTML);
    const hashGraphismes = encodeURIComponent(graphismes.innerHTML);

    // Construction du lien final avec le hash pour le texte principal
    const lienFinal = baseURL + params.toString() + '#' + hash;

    console.log(lienFinal);
    zone_lien.innerText = lienFinal;
}


// Fonction pour copier le lien et afficher un popup
function copier() {
    // Récupération du contenu de la zone de lien
    const zoneLien = document.getElementById('zone_lien');
    const texteLien = zoneLien.innerText;

    // Copie du texte dans le presse-papiers
    navigator.clipboard.writeText(texteLien)
        .then(() => {
            // Création du popup "Lien copié"
            const popup = document.createElement('div');
            popup.innerHTML = "Lien copié";
            popup.style.position = "absolute";
            popup.style.backgroundColor = "black";
            popup.style.color = "white";
            popup.style.fontSize = "12px";
            popup.style.padding = "5px 10px";
            popup.style.borderRadius = "5px";
            popup.style.zIndex = "1000";
            popup.style.opacity = "0";
            popup.style.transition = "opacity 0.3s";

            // Position du popup près du bouton "copier"
            const bouton = document.getElementById('copier');
            const rect = bouton.getBoundingClientRect();
            popup.style.left = rect.left + window.scrollX + "px";
            popup.style.top = rect.top + window.scrollY - 30 + "px";  // 30px au-dessus du bouton

            // Ajout du popup au document
            contenu.appendChild(popup);

            // Faire apparaître le popup
            setTimeout(() => {
                popup.style.opacity = "1";
            }, 10);

            // Faire disparaître le popup après 2 secondes
            setTimeout(() => {
                popup.style.opacity = "0";
                setTimeout(() => {
                    contenu.removeChild(popup);
                }, 300);  // Retire le popup du DOM après la transition
            }, 2000);
        })
        .catch((err) => {
            console.error('Erreur lors de la copie : ', err);
        });
}

function ouvreOptions() {
    divPanneauOptions.classList.toggle('hide');
}


// Sélectionner la bonne option dans le menu déroulant
choix_police.value = police_active;

function adapteMargeCahierDeTexte(valeur){
    console.log("Adaptation marge secondaire "+valeur+" carreaux")
    largeur_marge = largeur_carreau*valeur;
    page.style.width = case3.style.width = 'calc(100% - ' + (largeur_marge) + 'px)';
    divMarge.style.width = (largeur_marge - 3) + 'px';
    console.log("nouvelle largeur marge = "+largeur_marge);
    divMargeEntete.style.width = (largeur_marge) + 'px';     
    document.documentElement.style.setProperty('--largeur-marge-secondaire', largeur_carreau*2.7 + 'px');
}

function zoom(coef,largeur,police){

    largeur_carreau_old = largeur_carreau;
    if (coef) {
    largeur_carreau=largeur_carreau+coef;
    } else if (largeur) {
        largeur_carreau = largeur;
    }
    taille_police=largeur_carreau*facteur_taille_police;
    texte_principal.style.fontWeight=epaisseur_police;
    texte_marge.style.fontWeight=epaisseur_police;
    texte_marge_complementaire.style.fontWeight=epaisseur_police;
    if (isWindows){calc_marge=largeur_carreau*coef_marge_windows}
    else {calc_marge=largeur_carreau*coef_marge}
    if (police==='Open Dyslexic'){calc_marge=calc_marge*0.90}
    page.style.backgroundSize=largeur_carreau+'px';
    marge.style.backgroundSize=largeur_carreau+'px';
    margeSecondaire.style.backgroundSize=largeur_carreau+'px';
    texte_principal.style.fontSize=taille_police+'px';
    texte_principal.style.marginLeft=largeur_carreau/5+'px';
    texte_principal.style.marginRight=largeur_carreau/5+'px';
    texte_marge.style.fontSize=texte_marge_complementaire.style.fontSize=taille_police+'px';
    texte_marge.style.marginTop=texte_marge_complementaire.style.marginTop=calc_marge+'px'
    texte_marge.style.marginLeft=texte_marge_complementaire.style.marginLeft=largeur_carreau/5+'px';
    texte_marge.style.marginRight=texte_marge_complementaire.style.marginRight=largeur_carreau/5+'px';
    if (typeCarreaux==='terre'){
        console.log('Carreaux terre');
        texte_marge.style.lineHeight=2*largeur_carreau+'px';
        texte_principal.style.lineHeight=2*largeur_carreau+'px';
        texte_principal.style.marginTop=-(calc_marge/3)+'px';
    } else {
        texte_marge_complementaire.style.lineHeight=texte_marge.style.lineHeight=largeur_carreau+'px';
        texte_principal.style.lineHeight=largeur_carreau+'px';
        texte_principal.style.marginTop=calc_marge+'px';
    }

    document.documentElement.style.setProperty('--largeur-marge-secondaire', largeur_carreau*2.7 + 'px');


    const tousLesSpanCustomFont = document.querySelectorAll('.customfont');
    tousLesSpanCustomFont.forEach(span => {
        let policeCustom = span.style.fontFamily.trim();  // trim() pour éviter les espaces indésirables
        let facteurCustom = typesPolices[policeCustom]?.facteur_taille_police;  // Utilisation de la clé policeCustom dans l'objet typesPolices
    
        if (facteurCustom) {
            span.style.fontSize = span.style.lineHeight = largeur_carreau * facteurCustom + "px";
        } else {
            console.warn(`La police ${policeCustom} n'a pas été trouvée dans l'objet typesPolices.`);
        }
    });
    


    // Adaptation des graphismes

    const tousLesGraphismes = graphismes.querySelectorAll('.trait');
    tousLesGraphismes.forEach(graphisme => {

        graphisme.style.transition='none ';

        let computedStyle = window.getComputedStyle(graphisme);
      
        let positionX = parseInt(parseFloat(computedStyle.getPropertyValue('left')) / largeur_carreau_old);
        let positionY = parseInt(parseFloat(computedStyle.getPropertyValue('top')) / largeur_carreau_old);
        let width = parseInt(parseFloat(computedStyle.getPropertyValue('width')) / largeur_carreau_old);
        let height = parseInt(parseFloat(computedStyle.getPropertyValue('height')) / largeur_carreau_old);

        console.log(width)
    
        // Vérifie que les valeurs ne sont pas NaN
        if (isNaN(positionX)) positionX = 0;
        if (isNaN(positionY)) positionY = 0;
        if (isNaN(width)) width = 0;
        if (isNaN(height)) height = 0;
    
        // Ajustement de 'left' et 'top' en fonction de la proportion unique
        let newPositionX = positionX * largeur_carreau;
        let newPositionY = positionY * largeur_carreau;
    
        // Applique les nouvelles valeurs de 'left' et 'top' à l'élément
        graphisme.style.left = `${newPositionX}px`;
        graphisme.style.top = `${newPositionY}px`;
    
        console.log(`Ancienne position X: ${positionX}, Nouvelle position X: ${newPositionX}`);
        console.log(`Ancienne position Y: ${positionY}, Nouvelle position Y: ${newPositionY}`);
    
        // Si l'élément a la classe 'horizontal', on ajuste la largeur
        if (graphisme.classList.contains('horizontal')) {
            let newWidth = width * largeur_carreau + largeur_carreau*0.02117171;  // Calcule la nouvelle largeur
    
            // Applique la nouvelle largeur à l'élément
            graphisme.style.width = `${newWidth}px`;
    
            console.log(`Ancienne largeur: ${width}, Nouvelle largeur: ${newWidth}`);
        }
    
        // Si l'élément a la classe 'vertical', on ajuste la hauteur
        if (graphisme.classList.contains('vertical')) {
            let newHeight = height * largeur_carreau + largeur_carreau*0.02117171;  // Calcule la nouvelle hauteur
    
            // Applique la nouvelle hauteur à l'élément
            graphisme.style.height = `${newHeight}px`;
    
            console.log(`Ancienne hauteur: ${height}, Nouvelle hauteur: ${newHeight}`);
        }

        graphisme.style.transition='';


    });

    updateTransform();
    stocke('largeur-carrreau',largeur_carreau);
}

function pleinecran(){
    if (pleinecran_on){
        fullscreen.style.backgroundPosition=null;
        pleinecran_sortir();
    } else {
        fullscreen.style.backgroundPosition='0% 0% !important';
        pleinecran_ouvrir();
    }
    pleinecran_on=!pleinecran_on;
}

function pleinecran_ouvrir(){   
    // Demande le mode plein écran pour l'élément
    if (site.requestFullscreen) {
        site.requestFullscreen();        
    } else if (site.mozRequestFullScreen) { // Pour Firefox
        site.mozRequestFullScreen();
    } else if (site.webkitRequestFullscreen) { // Pour Chrome, Safari et Opera
        site.webkitRequestFullscreen();
    } else if (site.msRequestFullscreen) { // Pour Internet Explorer et Edge
        site.msRequestFullscreen();
    }
}

function pleinecran_sortir(){
    // Pour sortir du mode plein écran
    if (document.exitFullscreen) {
        document.exitFullscreen();
    } else if (document.mozCancelFullScreen) { // Pour Firefox
        document.mozCancelFullScreen();        
    } else if (document.webkitExitFullscreen) { // Pour Chrome, Safari et Opera
        document.webkitExitFullscreen();
    } else if (document.msExitFullscreen) { // Pour Internet Explorer et Edge
        document.msExitFullscreen();
    }
}

function nouveauTexte(){
    confirmationNouveau.style.display='block';
}

function ferme_confirmation(){
    confirmationNouveau.style.display='none';
}

async function prepareEnregistrement() {
    await save_texte();
    let nomDuFichier = `texte`;
    if (dateDuDoc) {nomDuFichier = `${nomDuFichier}-${dateDuDoc}`;}
    enregistrer(`${nomDuFichier}.sey`);
}

function raz() {
    // Réinitialiser le contenu HTML de divers éléments
    texte_principal.innerHTML = '';
    graphismes.innerHTML = '';
    texte_marge.innerHTML = texte_marge_complementaire.innerHTML = '';

    if (ajouterLaDate) {
        creeDate();
    }

    // Mettre le focus sur l'élément texte_principal
    texte_principal.focus();

    // Créer un objet Range pour manipuler la position du curseur
    const range = document.createRange();
    const selection = window.getSelection();

    // Trouver la dernière balise <br> et positionner le curseur juste après
    const brElements = texte_principal.getElementsByTagName('br');
    if (brElements.length > 0) {
        const lastBr = brElements[brElements.length - 1];
        range.setStartAfter(lastBr);  // Place le curseur après le dernier <br>
        range.collapse(true);  // Fusionne le point de début et de fin pour positionner le curseur
        selection.removeAllRanges();  // Vide la sélection actuelle
        selection.addRange(range);  // Ajoute la nouvelle plage
    }
}





function changeDate(date = null) {
    console.log("changeDate",date);

    let dateAutiliser;

    const divDate = document.getElementById('divDate');


    if (!divDate) {
    console.log('pas de date --> création d une date')
    creeDate();


    } else {
    const nouveauSpan = document.createElement('span');

    if (date) {
        dateAutiliser = calculeDate('complet', date);
    }
    
    else {
        dateAutiliser = calculeDate();
    }


    nouveauSpan.innerHTML = dateAutiliser;

    if (soulignerLaDate) {nouveauSpan.style.textDecoration = 'underline red';}

    dateDuDoc = dateAutiliser;
    divDate.innerHTML = '';
    divDate.appendChild(nouveauSpan);

    }
}

function creeDate() {
    console.log("Création de la date");

    const nouveauSpan = document.createElement('span');
    nouveauSpan.innerHTML = calculeDate(); // Par défaut, utilise la date courante

    const divDate = document.createElement('div');
    divDate.id = "divDate";
    divDate.appendChild(nouveauSpan);

    if (soulignerLaDate) {
        nouveauSpan.style.textDecoration = 'underline red';
    }

    texte_principal.prepend(divDate);

    const br1 = document.createElement('br');
    const br2 = document.createElement('br');

    texte_principal.insertBefore(br2, divDate.nextSibling);
    texte_principal.insertBefore(br1, br2);
    console.log("ajout de la date",texte_principal.innerHTML)
}

function calculeDate(format = 'complet', date = null) {
    let options;
    console.log(`Format demandé : ${format}`);

    switch (format) {
    case 'complet':
        options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
        break;
    case 'sansJour':
        options = { year: 'numeric', month: 'long', day: 'numeric' };
        break;
    case 'sansAnnee':
        options = { weekday: 'long', month: 'long', day: 'numeric' };
        break;
    case 'court':
        const dateObj = date || new Date();
        const dateCourt = `${dateObj.getDate().toString().padStart(2, '0')}/${(dateObj.getMonth() + 1).toString().padStart(2, '0')}/${dateObj.getFullYear()}`;
        console.log(`Date au format court : ${dateCourt}`);
        return dateCourt;
    default:
        options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
    }

    const dateObj = date || new Date();
    let dateDuJour = dateObj.toLocaleDateString('fr-FR', options);
    dateDuJour = dateDuJour.replace(/\b1\b/, '1<span class="exposant">er</span>');
    console.log(`Date calculée : ${dateDuJour}`);
    const date_formatee = dateDuJour.charAt(0).toUpperCase() + dateDuJour.slice(1);
    console.log("date formatée = " + date_formatee);
    return date_formatee;
}

let calendar; // Déclarez cette variable en dehors de la fonction pour garder la référence de l'instance

function ouvreSelecteurDate() {
    const datepickerDiv = document.getElementById('datepicker');
    const inputDate = document.getElementById('date-input'); // Assurez-vous d'avoir le bon ID du champ de saisie

    const rect = document.getElementById('date').getBoundingClientRect();
    let positionLeft = rect.left;
    

    // Affiche le calendrier (en activant le div)
    datepickerDiv.classList.remove('hide');
    // Montre le calendrier en utilisant la classe de la div générée par Flatpickr
    const flatpickrCalendar = document.querySelector('.flatpickr-calendar');
    if (flatpickrCalendar) {
        flatpickrCalendar.style.left = positionLeft + 'px';
        flatpickrCalendar.classList.remove('hide'); // Montre le calendrier
    }

    inputDate.focus();  // Donne le focus sur le champ de saisie pour ouvrir le calendrier

    if (!calendar) {
        // Initialisation de Flatpickr
        calendar = flatpickr(inputDate, {
            inline: true, // Le calendrier s'affiche dans le div et non en pop-up
            dateFormat: 'Y-m-d', // Format de la date (année-mois-jour)
            locale: 'fr',
            onChange: function (selectedDates) {
                // Une fois qu'une date est sélectionnée
                changeDate(selectedDates[0]);
                datepickerDiv.classList.add('hide'); // Cache le calendrier après sélection
                // Cache le calendrier en utilisant la classe de la div générée par Flatpickr
                const flatpickrCalendar = document.querySelector('.flatpickr-calendar');
                if (flatpickrCalendar) {
                    flatpickrCalendar.classList.add('hide'); // Cache le calendrier
                }
            },
        });

        // Montre le calendrier en utilisant la classe de la div générée par Flatpickr
        const flatpickrCalendar = document.querySelector('.flatpickr-calendar');
        if (flatpickrCalendar) {
            flatpickrCalendar.style.left = positionLeft + 'px';
            flatpickrCalendar.classList.remove('hide'); // Montre le calendrier
        }
    }
}






function ligatures(mode,sens_force){
    if (ligatures_on){
        if (mode!='auto'){
            bouton_ligatures.style.backgroundPosition=null;
            ligatures_on=false;
            texte_principal.spellcheck = true;
            texte_marge.spellcheck = true;
            texte_marge_complementaire.spellcheck = true;
        }
        sens=[1,0];
    } else {
        if (mode!='auto'){
            bouton_ligatures.style.backgroundPosition='0% 0%';
            ligatures_on=true;
            texte_principal.spellcheck = false;
            texte_marge.spellcheck = false;
            texte_marge_complementaire.spellcheck = false;
        }
        sens=[0,1];
    }
    if (sens_force){sens=sens_force}    
    let texte=texte_principal.innerHTML;
    for (let i = 0; i < subsitutions.length; i++) {
        let recherche = subsitutions[i][sens[0]];
        let subsitution = subsitutions[i][sens[1]];
        texte = texte.replace(new RegExp(recherche, 'g'), subsitution);
    }
    texte_principal.innerHTML=texte;
    texte=texte_marge.innerHTML;
    for (let i = 0; i < subsitutions.length; i++) {
        let recherche = subsitutions[i][sens[0]];
        let subsitution = subsitutions[i][sens[1]];
        texte = texte.replace(new RegExp(recherche, 'g'), subsitution);
    }
    texte_marge.innerHTML=texte;
}


function ouvreBoutonsSouligne(event) {
    event.stopPropagation();
    const rect = document.getElementById('souligne').getBoundingClientRect();
    let positionLeft = rect.left;
    document.getElementById('boutons_soulignement').style.left = positionLeft + 'px';
    document.getElementById('boutons_soulignement').classList.toggle('hide');
}

function ouvreBoutonsCarreau(event) {
    event.stopPropagation();
    const rect = document.getElementById('carreaux').getBoundingClientRect();
    let positionLeft = rect.left;
    document.getElementById('boutons_carreaux').style.left = positionLeft + 'px';
    document.getElementById('boutons_carreaux').classList.toggle('hide');
}

function ouvreBoutonsCouleurs(event) {
    event.stopPropagation();
    const rect = document.getElementById('couleur-texte').getBoundingClientRect();
    let positionLeft = rect.left;
    document.getElementById('boutons-couleur-texte').style.left = positionLeft + 'px';
    document.getElementById('boutons-couleur-texte').classList.toggle('hide');
}




function update_souligne() {
    texte_principal.style.textUnderlineOffset=distance_soulignage+'em';
    texte_marge.style.textUnderlineOffset=distance_soulignage+'em';
    texte_marge_complementaire.style.textUnderlineOffset=distance_soulignage+'em';
}

function couleurs(couleur) {
    console.log("couleur demandée "+couleur);
    couleurTexte.style.color = couleur;
    couleurTexte.value = couleur;
    document.getElementById('boutons-couleur-texte').classList.add('hide');
    if (focus_texte){focus_texte.focus();}
    document.execCommand('foreColor', false, couleur);
        // Récupérer la sélection actuelle
        var selection = window.getSelection();
        if (selection.rangeCount > 0) {
            var range = selection.getRangeAt(0);
        }
}


//Opacité
function opacite(valeur){
    opaciteLignes=parseFloat(valeur);
    inputOpacite.value=valeur;
    voile_page.style.backgroundColor='rgba(255,255,255,'+(+1-valeur)+')';
    voile_marge.style.backgroundColor='rgba(255,255,255,'+(+1-valeur)+')';
    voile_marge2.style.backgroundColor='rgba(255,255,255,'+(+1-valeur)+')';
    stocke('opacite-lignes',opaciteLignes);
}


// Fonction pour positionner le curseur
function positionCursor(position) {
    const range = document.createRange();
    const select = window.getSelection();
    console.log("function positioncursor "+position)
    range.setStart(texte_principal.firstChild, position);
    range.collapse(true);

    select.removeAllRanges();
    select.addRange(range);

    texte_principal.focus();
}

// Fonction pour ajuster la position de la souris au quadrillage
function snapToGrid(value, gridSize = largeur_carreau) {
    return Math.round(value / gridSize) * gridSize;
}

selected=false;

window.addEventListener('keydown', function(event) {
    // Vérifie si la touche "Suppr" (Delete) est appuyée
    if (event.key === 'Delete') {
        // Si un élément est sélectionné, le supprimer
        if (selected) {
            selected.remove(); // Supprime l'élément du DOM
            selected = false; // Réinitialise la variable
            console.log("Élément supprimé et sélection réinitialisée.");
        }
    }
});

function changeCouleurTrait(couleur) {
    if (selected) {
        selected.style.backgroundColor=couleur;
        selected.classList.remove('select');
        selected=null;
    } else {
        alert("Aucun trait n'est sélectionné.")
    }
}

function playAudio(event) {

    if (event.target.classList.contains('bouton-musique')) {

        var audio = document.getElementById('audio');
        var button = document.getElementById('bouton-audio');
        
        if (audio.paused) {
            audio.play();
            button.classList.add('pause');
        } else {
            audio.pause();
            button.classList.remove('pause');
        }

    }
}


function stopAudio(event) {

    if (event.target.classList.contains('bouton-musique')) {

        var audio = document.getElementById('audio');
        var button = document.getElementById('bouton-audio');
        
        audio.pause();  // Met l'audio en pause
        audio.currentTime = 0;  // Remet à zéro le temps de lecture
        button.classList.remove('pause');  // Change l'icône du bouton à "Play"

    }
}
page.addEventListener('click', (event) => {
    // Vérifiez si le clic est directement sur le parent
    if (event.target === page) {
        texte_principal.focus();
    }
});

function clic(event) {
    console.log('clic',event.target);

    save_texte();

    if (event.target.classList.contains('gestion-texte')) {
        event.preventDefault();
    }



    // FERMETURE AUTO DES MENUS
    const menuSoulignement = document.getElementById('boutons_soulignement');
    const menuCarreaux = document.getElementById('boutons_carreaux');
    const menuCouleurTexte = document.getElementById('boutons-couleur-texte');
    const datepickerDiv = document.getElementById('datepicker');
    const flatpickrCalendar = document.querySelector('.flatpickr-calendar');

    const clicSurEnfantdeCalendar = objetOuEnfantDe(event.target,flatpickrCalendar);

    // Si le clic n'a pas été fait sur un des menus déroulants ou le calendrier, fermer les menus
    if (!clicSurEnfantdeCalendar && !event.target.classList.contains('deroule-couleur') && !event.target.classList.contains('deroule_souligne') && !event.target.classList.contains('deroule_carreaux') && !event.target.classList.contains('flatpickr-calendar')) {
        console.log('Fermeture auto');
        
        // Ferme tous les menus
        menuSoulignement.classList.add('hide');
        menuCarreaux.classList.add('hide');
        menuCouleurTexte.classList.add('hide');
        datepickerDiv.classList.add('hide'); // Cache le calendrier après sélection
        
        if (flatpickrCalendar) {
            flatpickrCalendar.classList.add('hide'); // Cache le calendrier
        }
    }
    // FIN DE LA FERMETURE AUTO DES MENUS


    if (event.target.classList.contains('bouton-supprimer-trait') && !regleActive && selected) {
        selected.remove();
        selected=null;
    }

    if (event.target.classList.contains('bouton-couleur-trait') && !regleActive && selected) {
        console.log("Clic sur bouton couleur")
        document.getElementById('inputCouleurTrait').click();
    }

    if (event.target.classList.contains('trait') && !regleActive) {
        if (selected) {
            selected.classList.remove('select');
        }
        event.target.classList.add('select');
        selected=event.target;
    } else if (selected && !event.target.classList.contains('bouton-couleur-trait')){
        selected.classList.remove('select');
        selected=null;
    }

    const borderWidth = 10; // Largeur de la bordure (en pixels)
    const rect = page.getBoundingClientRect(); // Récupère les dimensions et la position de la div "page"
    const rect2 = margeSecondaire.getBoundingClientRect(); // Récupère les dimensions et la position de la div "page"
    posX = event?.targetTouches?.[0]?.clientX || event.clientX;
    posY = event?.targetTouches?.[0]?.clientY || event.clientY;
    const mouseX = posX - rect.left; // Position de la souris horizontalement par rapport à la div "page"
    const mouseY = posY - rect.top;
    const mouseXmarge = posX - rect2.left; // Position de la souris horizontalement par rapport à la div "page"
    const mouseYmarge = posY - rect2.top;

    let draggable_item = false;
    if (event.target.classList.contains('draggable_item')) { draggable_item = true; }

    if (event.target.classList.contains('draggable') || draggable_item) {
        console.log("clic sur draggable");
        event.preventDefault();
        if (draggable_item) { dragged = event.target.parentNode; }
        else { dragged = event.target; }
        diffsourisx = posX - dragged.offsetLeft;
        diffsourisy = posY - dragged.offsetTop;
        posX_objet = dragged.offsetLeft;
        posY_objet = dragged.offsetTop;
        dragged.classList.add('dragged');    
    } else if (mouseX <= borderWidth && mouseX > -borderWidth) { // Vérifie si le clic est dans une zone de 5px de la bordure gauche
        redim = page;
        body.style.cursor="ew-resize";
        posXDepartSouris = posX;
        largeur_page = page.offsetWidth;
        largeur_marge = divMarge.offsetWidth;
    } else if (mouseXmarge <= borderWidth && mouseXmarge > -borderWidth) { // Vérifie si le clic est dans une zone de 5px de la bordure gauche
        redim = margeSecondaire;
        body.style.cursor="ew-resize";
        posXDepartSouris = posX;
        largeur_marge_secondaire = margeSecondaire.offsetWidth;
    } else if (regleActive) {  // Gérer le traçage des lignes si regleActive est vrai
        console.log('gestion du trait')
        const snappedX = snapToGrid(mouseX);
        const snappedY = snapToGrid(mouseY);
        if (Math.abs(mouseX - snappedX) < largeur_carreau/8 && Math.abs(mouseY - snappedY) < largeur_carreau/8) {
            isDrawing = true;
            startX = snappedX;
            startY = snappedY;

            // Créer une div pour représenter le trait
            lineDiv = document.createElement('div');
            lineDiv.classList.add('trait');
            lineDiv.classList.add('horizontal');
            lineDiv.classList.add('draggable');

            lineDiv.style.position = 'absolute';
            lineDiv.style.backgroundColor = 'red';
            lineDiv.style.width = '6px';  // Par défaut, un trait vertical
            lineDiv.style.height = '6px'; // Minimum taille
            lineDiv.style.left = startX + 'px';
            lineDiv.style.top = startY + 'px';

            let boutonSupprimer = document.createElement('button');
            boutonSupprimer.classList.add('bouton-supprimer-trait');
            lineDiv.appendChild(boutonSupprimer);

            let boutonCouleur = document.createElement('button');
            boutonCouleur.classList.add('bouton-couleur-trait');
            lineDiv.appendChild(boutonCouleur);


            graphismes.appendChild(lineDiv);
        }
    }
}

function move(event) {
    posXSouris = event?.targetTouches?.[0]?.clientX || event?.clientX;
    posYSouris = event?.targetTouches?.[0]?.clientY || event?.clientY;
    const rect = page.getBoundingClientRect();
    const mouseX = posXSouris - rect.left;
    const mouseY = posYSouris - rect.top;
    const rect2 = page.getBoundingClientRect();
    const mouseXmarge = posXSouris - rect2.left;
    const mouseYmarge = posYSouris - rect2.top;

    if (redim===page) {
        diffX = posXSouris - posXDepartSouris;
        if (largeur_marge + diffX >= 60) {
            page.style.width = case3.style.width = (largeur_page - diffX) + 'px';
            divMarge.style.width = (largeur_marge + diffX - 3) + 'px';
            divMargeEntete.style.width = (largeur_marge + diffX) + 'px';

        } else {
            page.style.width = case3.style.width = 'calc(100% - 60px)';
            divMarge.style.width = divMargeEntete.style.width =  '57px';
            divMargeEntete.style.width = divMargeEntete.style.width =  '60px';
        }
    } else if (redim===margeSecondaire) {
        diffX = posXSouris - posXDepartSouris;
        if (largeur_marge_secondaire - diffX >= largeur_carreau * 2.7) {
            margeSecondaire.style.width = (largeur_marge_secondaire - diffX) + 'px';            
            case2.style.width = (largeur_marge_secondaire - diffX) + 'px';

        } else {
            margeSecondaire.style.width = (largeur_carreau * 2.7) + 'px';            
            case2.style.width = (largeur_carreau * 2.7) + 'px';
        }
    }

    if (dragged) {
        dragged.style.right = null;
        dragged.style.bottom = null;
        event.preventDefault();
        const pageX = event?.targetTouches?.[0]?.clientX || event?.clientX || 0;
        const pageY = event?.targetTouches?.[0]?.clientY || event?.clientY || 0;
        dragged.style.left = pageX - diffsourisx + "px";
        dragged.style.top = pageY - diffsourisy + "px";
    } else if (isDrawing && lineDiv) {  // Gérer le traçage de la ligne
        const snappedX = snapToGrid(mouseX);
        const snappedY = snapToGrid(mouseY);

        // Calculer la direction du trait (horizontal ou vertical)
        const diffX = snappedX - startX;
        const diffY = snappedY - startY;

        if (Math.abs(diffX) > Math.abs(diffY)) {
            // Traçage horizontal
            let longueur = Math.abs(diffX);
            let nbCarreaux = parseInt(longueur/largeur_carreau);
            lineDiv.style.width = longueur + largeur_carreau*0.02117171 + 'px';
            lineDiv.style.height = '6px';
            lineDiv.style.top = startY + 'px';
            lineDiv.style.left = startX + 'px';
            lineDiv.classList.remove('vertical');
            lineDiv.classList.add('horizontal');

            if (diffX < 0) {
                lineDiv.style.left = snappedX + 'px';  // Ajuster la position si on dessine vers la gauche  
            }
        } else {
            // Traçage vertical
            lineDiv.style.height = Math.abs(diffY) + largeur_carreau*0.02117171 + 'px';
            lineDiv.style.width = '6px';
            lineDiv.classList.remove('horizontal');
            lineDiv.classList.add('vertical');
            if (diffY < 0) {
                lineDiv.style.top = snappedY + 'px';  // Ajuster la position si on dessine vers le haut
            }
        }
    }
}

function release(event) {
    redim = false;
    body.style.cursor=null;
    largeur_page = page.offsetWidth;
    largeur_marge = divMarge.offsetWidth;
    largeur_marge_secondaire = margeSecondaire.offsetWidth;
    stocke('largeur-marge',largeur_marge);
    stocke('largeur-marge-secondaire',largeur_marge_secondaire);
    stocke('graphismes',graphismes.innerHTML);

    // Fin du déplacement d'élément
    if (dragged) {
        dragged.classList.remove('dragged');

        if (dragged.classList.contains('trait')) {
            // On récupère la position actuelle de l'élément dragged (position absolute)
            let currentLeft = parseInt(dragged.style.left, 10);
            let currentTop = parseInt(dragged.style.top, 10);
            
            // Calculer les nouvelles coordonnées alignées sur la grille
            let newLeft = Math.round(currentLeft / largeur_carreau) * largeur_carreau;
            let newTop = Math.round(currentTop / largeur_carreau) * largeur_carreau;
            
            // Repositionner dragged sur la grille invisible
            dragged.style.left = `${newLeft}px`;
            dragged.style.top = `${newTop}px`;
        }
        

        dragged = null;
    }

    // Fin du traçage de la ligne
    if (isDrawing) {
        isDrawing = false;
        lineDiv = null;
        activeRegle();
    }

    ;

    
}

// Fonction pour changer le curseur lors du survol des points d'accroche
function moveCursor(event) {
    const rect = page.getBoundingClientRect();
    const mouseX = (event?.targetTouches?.[0]?.clientX || event.clientX) - rect.left;
    const mouseY = (event?.targetTouches?.[0]?.clientY || event.clientY) - rect.top;

    if (regleActive) {
        const snappedX = snapToGrid(mouseX);
        const snappedY = snapToGrid(mouseY);

        console.log(snappedX)


        if (Math.abs(mouseX - snappedX) < largeur_carreau/8 && Math.abs(mouseY - snappedY) < largeur_carreau/8) {
            page.style.cursor = 'url(images/crayonrouge.png) 0 30, auto';
        } else if (!isDrawing){
            page.style.cursor = 'url(images/crayon.png) 0 30, auto ';
        }
    }
}

document.addEventListener('mousemove', moveCursor);



function menu_police() {
    if (zone_menu_police.style.display==='block'){ferme_menu_police()}
    else {zone_menu_police.style.display='block';}
}

function ferme_menu_police() {
    zone_menu_police.style.display='none';
}

function menu_copier_lien() {
    majLien();
    if (zone_menu_copie_lien.style.display==='block'){ferme_menu_copie_lien()}
    else {zone_menu_copie_lien.style.display='block';}
}

function ferme_menu_copie_lien() {
    zone_menu_copie_lien.style.display='none';
}




// Fonction pour changer la police
function change_police(police) {
    // Récupérer le texte sélectionné
    let texteSelectionne = window.getSelection().toString();

    if (texteSelectionne) {
        changePoliceSelection(police);
    } else {
        texte_principal.style.fontFamily = police;
        texte_marge.style.fontFamily = police;
        texte_marge_complementaire.style.fontFamily = police;
        police_active = police;

        if (!window.widget) {
            localStorage.setItem('seyes-police', police);
        }

        // Si la police existe dans notre objet typesPolices
        if (typesPolices[police]) {
            let config = typesPolices[police];
            coef_marge = config.coef_marge;
            coef_marge_windows = config.coef_marge_windows;
            facteur_taille_police = config.facteur_taille_police;
            epaisseur_police = config.epaisseur_police;
            distance_soulignage = config.distance_soulignage;
            bouton_ligatures.style.display = config.bouton_ligatures_display;
            ligatures('auto', config.ligatures);
        }
    }
    zoom(0,false,police_active);
    update_souligne();
}


function cree_image(event){
    let input=event.target;
    let nouvelle_image=new Image();

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            nouvelle_image.src = e.target.result;
        };

        reader.readAsDataURL(input.files[0]);
    }

    nouvelle_image.style.position='absolute';
    nouvelle_image.style.top='100px';
    nouvelle_image.style.left='100px';
    nouvelle_image.classList.add('draggable');
    nouvelle_image.style.width='300px';

    contenu.appendChild(nouvelle_image);
}

function souligne(couleur) {
    console.log('Soulignement demandé avec la couleur : ' + couleur);
    document.getElementById('boutons_soulignement').classList.add('hide');

    // Mettre à jour l'aperçu de la couleur
    if (couleur) {
        document.getElementById('souligne').style.textDecorationColor = couleur;
        document.getElementById('souligne').value = couleur;
    }

    // Récupérer la sélection en cours
    const selection = window.getSelection();

    if (selection.rangeCount > 0) {
        // Récupérer le premier range (zone sélectionnée)
        const range = selection.getRangeAt(0);

        // Utiliser DocumentFragment pour manipuler le contenu sans perturber le DOM
        const fragment = range.extractContents();

        // Itérer sur les nœuds du fragment pour les traiter
        const processedFragment = document.createDocumentFragment();

        fragment.childNodes.forEach(node => {
            if (node.nodeType === Node.TEXT_NODE) {
                // Si c'est un texte brut, on applique le style (ou on le nettoie pour 'white')
                const span = document.createElement('span');
                if (couleur === 'white') {
                    // Supprimer le soulignement (pas de <span> dans ce cas)
                    processedFragment.appendChild(document.createTextNode(node.textContent));
                } else {
                    span.style.textDecoration = 'underline';
                    span.style.textDecorationColor = couleur;
                    span.textContent = node.textContent;
                    processedFragment.appendChild(span);
                }
            } else if (node.nodeType === Node.ELEMENT_NODE) {
                // Si c'est un élément HTML, traiter ses enfants
                const clonedNode = node.cloneNode(true);
                if (couleur === 'white') {
                    // Supprimer tout soulignement des enfants existants
                    clonedNode.style.textDecoration = '';
                } else {
                    clonedNode.style.textDecoration = 'underline';
                    clonedNode.style.textDecorationColor = couleur;
                }
                processedFragment.appendChild(clonedNode);
            }
        });

        // Insérer le contenu modifié dans le DOM
        range.insertNode(processedFragment);

        // Désélectionner le texte
        selection.removeAllRanges();
    } else {
        console.log('Aucune sélection détectée.');
    }
}








function changePoliceSelection(police) {
    console.log("changement de police sur une sélection");
    // Récupérer la sélection actuelle
    let selection = window.getSelection();
    let range = selection.rangeCount > 0 ? selection.getRangeAt(0) : null;
    let texteSelectionne = selection.toString();
    console.log('Texte sélectionné : ' + texteSelectionne);
    console.log('Police demandée : ' + police);
    console.log('Range : ' + range);


    // Vérifier si la police existe dans l'objet typesPolices
    if (texteSelectionne && range && typesPolices[police]) {
        console.log("La police existe");

        let config = typesPolices[police];

        // Calcul de la nouvelle taille de police et des marges en fonction du facteur
        let taille_police_selection = largeur_carreau * config.facteur_taille_police;

        // Appliquer le style de la police
        let style = `
            font-family: ${police}; 
            font-size: ${taille_police_selection}px; 
            font-weight: ${config.epaisseur_police}; 
            line-height: ${taille_police_selection}px; 
        `;

        let startContainer = range.startContainer;
        let endContainer = range.endContainer;

        // Vérifier si la sélection s'étend sur plusieurs éléments
        if (startContainer !== endContainer) {
            // Gestion de la sélection sur plusieurs divs ou éléments
            console.log("Sélection sur plusieurs éléments");

            let fragment = range.cloneContents(); // Copier le contenu de la sélection
            let elements = fragment.querySelectorAll('*');

            elements.forEach(el => {
                // Appliquer le style uniquement au texte si l'élément n'est pas vide
                if (el.textContent.trim() !== '') {
                    let spanStyled = document.createElement('span');
                    spanStyled.className = 'customfont';
                    spanStyled.style.cssText = style;
                    spanStyled.innerHTML = el.innerHTML; // Conserver le contenu original
                    el.innerHTML = '';  // Remplacer par le span stylé
                    el.appendChild(spanStyled);
                }
            });

            // Remplacer la sélection par le contenu modifié
            range.deleteContents();
            range.insertNode(fragment);
        } else {
            // Cas où la sélection est dans un seul élément
            let spanStyled = document.createElement('span');
            spanStyled.className = 'customfont';
            spanStyled.style.cssText = style;
            spanStyled.textContent = texteSelectionne;

            // Supprimer la sélection initiale et insérer le span avec le style appliqué
            range.deleteContents();
            range.insertNode(spanStyled);

            // Réinitialiser la sélection pour correspondre au nouveau <span>
            selection.removeAllRanges();
            let newRange = document.createRange();
            newRange.setStart(spanStyled.firstChild, 0);
            newRange.setEnd(spanStyled.firstChild, spanStyled.firstChild.length);
            selection.addRange(newRange);
        }
    }
}



function date() {
    let options = {
        weekday: 'long',
        year: 'numeric',
        month: 'long',
        day: 'numeric'
    };
    let dateDuJour = new Date().toLocaleDateString('fr-FR', options); // Obtient la date actuelle au format français (jour/mois/année)
    return dateDuJour;
}


function save_texte() {
    console.log('--SAUVEGARDE DES TEXTES');
    console.log("texte-principal.innerHTML", texte_principal.innerHTML);
    console.log("texte-marge.innerHTML", texte_marge.innerHTML);
    console.log("texte-marge-complementaire.innerHTML", texte_marge_complementaire.innerHTML);

    stocke('texte-principal', texte_principal.innerHTML);
    stocke('texte-marge', texte_marge.innerHTML);
    stocke('texte-marge-complementaire', texte_marge_complementaire.innerHTML);
}

texte_principal.addEventListener("focusout", save_texte);
texte_marge.addEventListener("focusout", save_texte);
texte_marge_complementaire.addEventListener("focusout", save_texte);

// Écouteurs de souris
document.addEventListener("touchstart", clic);
document.addEventListener("touchmove", move);
document.addEventListener("touchend", release);
// Pour le tactile
document.addEventListener("mousedown", clic);
document.addEventListener("mousemove", move);
document.addEventListener("mouseup", release);

//Raccourcis clavier


function changeCarreaux(type) {
    let ancienTypeCareaux = typeCarreaux;
    console.log('Changement des carreaux '+type)
    typeCarreaux=type;
    marge.style.backgroundImage="url('images/carreau_marge_"+typeCarreaux+".svg";
    margeSecondaire.style.backgroundImage="url('images/carreau_marge_"+typeCarreaux+".svg";

    page.style.backgroundImage="url('images/carreau_"+typeCarreaux+".svg";
    document.getElementById('carreaux').style.backgroundImage=`url(images/carreau_${type}.svg`;
    if (type==='terre'){
        page.style.borderLeft='none';
    } else {
        page.style.borderLeft=null;
    }
    if (type==='cahier_de_textes'){
        margeSecondaire.style.display='block';
        adapteMargeCahierDeTexte(7);
        divMarge.style.borderRightColor = '#000000';
        divEntete.style.display='flex';
    } else {
        margeSecondaire.style.display=null;
        divEntete.style.display='none';
        if (ancienTypeCareaux==='cahier_de_textes'){
            adapteMargeCahierDeTexte(4);
        }
        divMarge.style.borderRightColor = null;
    }
    zoom(0,false,police_active);
    ;
    stocke('type-carreaux',typeCarreaux)
}



texte_principal.addEventListener('blur', function() {
    focus_texte=texte_principal;
});
texte_principal.addEventListener('focus', function() {
    focus_texte=texte_principal;
});

texte_marge.addEventListener('blur', function() {
    focus_texte=texte_marge;
});
texte_marge.addEventListener('focus', function() {
    focus_texte=texte_marge;
});

texte_marge_complementaire.addEventListener('blur', function() {
    focus_texte=texte_marge_complementaire;
});

texte_marge_complementaire.addEventListener('focus', function() {
    focus_texte=texte_marge_complementaire;
});


page.addEventListener('paste', function(e) {    
    // Quand un "coller" est détecté

    e.preventDefault(); // On inhibe le comportement "normal".

    //On récupère le contenu du presse-papiers
    var text = (e.originalEvent || e).clipboardData.getData('text/plain');

    // On supprime le formatage HTML.
    text = text.replace(/<[^>]*>/g, '');
    
    // On insère le texte sans styles.
    document.execCommand('insertText', false, text);
});







function ajouterBackgroundImages() {
    // Sélectionner tous les boutons dans la div boutons_soulignement
    const boutons = document.querySelectorAll('#boutons_soulignement button');

    // Parcourir chaque bouton
    boutons.forEach(bouton => {
        // Récupérer l'ID du bouton
        const boutonId = bouton.id;

        // Construire l'URL de l'image en fonction de l'ID du bouton
        const imageUrl = `images/${boutonId}.svg`;

        // Appliquer le background-image au bouton
        bouton.style.backgroundImage = `url('${imageUrl}')`;

    });

        // Sélectionner tous les boutons dans la div boutons_soulignement
        const boutonsCarreau = document.querySelectorAll('#boutons_carreaux button:not(.bouton-fermer)');

        // Parcourir chaque bouton
        boutonsCarreau.forEach(bouton => {
            // Récupérer l'ID du bouton
            const boutonId = bouton.id;
    
            // Construire l'URL de l'image en fonction de l'ID du bouton
            let imageUrl = `images/${boutonId}.svg`;
            // Appliquer le background-image au bouton
            if (imageUrl===`images/carreau_cahier_de_textes.svg`){
                imageUrl='images/cahier_de_textes.svg';
            }
            bouton.style.backgroundImage = `url('${imageUrl}')`;
    
        });
}

// Appeler la fonction pour appliquer les images après le chargement du DOM
window.onload = ajouterBackgroundImages;

  // Fonction pour modifier la position
  function updateTransform() {
    document.documentElement.style.setProperty('--decalage-depart', - largeur_carreau*0.02117171 + 'px');
    document.documentElement.style.setProperty('--decalage-position', (-2 - largeur_carreau*0.02117171) + 'px');
  }

async function handleFileSelect(event) {
    const file = event.target.files[0];

    if (!file) {
        alert('Aucun fichier sélectionné.');
        return;
    }

    const validExtensions = ['.json', '.sey', '.odt', '.doc', '.docx', '.txt'];
    if (!validExtensions.some(ext => file.name.endsWith(ext))) {
        alert('Erreur : fichier non valide. Extensions acceptées : ' + validExtensions.join(', '));
        return;
    }

    try {
        if (file.name.endsWith('.doc') || file.name.endsWith('.docx')) {
            const content = await readDocxFile(file);
            insereContenu(content);
        } else if (file.name.endsWith('.odt')) {
            const content = await readOdtFile(file);
            insereContenu(content);
        } else {
            const content = await readFileAsText(file);
            if (file.name.endsWith('.json') || file.name.endsWith('.sey')) {
                handleJSONFile(content);
            } else {
                insereContenu(content);
            }
        }
    } catch (error) {
        alert('Erreur lors du traitement du fichier : ' + error.message);
        console.error(error);
    }
}

// Gestion des fichiers JSON ou SEY
async function handleJSONFile(content) {
    try {
        const data = JSON.parse(content);
        console.log('Fichier JSON/SEY chargé avec succès :', data);

        // Stockage des données dans localStorage
        for (const [key, value] of Object.entries(data)) {
            localStorage.setItem(key, value);
        }

        // Appeler checkReglages et appliqueReglages
        await checkReglages();
        appliqueReglages();
    } catch (error) {
        alert('Erreur : le contenu du fichier JSON est invalide.');
        console.error(error);
    }
}

// Lire un fichier DOC/DOCX avec Mammoth.js et récupérer le formatage
async function readDocxFile(file) {
    return new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.onload = (event) => {
            const arrayBuffer = event.target.result;
            mammoth.convertToHtml({ arrayBuffer: arrayBuffer })
                .then((result) => resolve(result.value)) // Récupère le contenu HTML avec le formatage
                .catch((err) => reject(err));
        };
        reader.onerror = () => reject(new Error('Impossible de lire le fichier DOC/DOCX.'));
        reader.readAsArrayBuffer(file);
    });
}


async function readOdtFile(file) {
    return new Promise((resolve, reject) => {
      // Vérifier si odf.js est chargé
      if (typeof ODF === 'undefined') {
        reject(new Error('La bibliothèque odf.js n\'est pas chargée.'));
        return;
      }
  
      const reader = new FileReader();
      reader.onload = async (event) => {
        try {
          const arrayBuffer = event.target.result;
          
          // Convertir l'ArrayBuffer en Uint8Array
          const uint8Array = new Uint8Array(arrayBuffer);
          
          // Utiliser odf.js pour lire le contenu du fichier ODT
          const odfDocument = await ODF.load(uint8Array);
          
          // Extraire le texte du document
          const textContent = await odfDocument.getText();
          
          resolve(textContent);
        } catch (error) {
          reject(new Error('Impossible de lire le fichier ODT : ' + error.message));
        }
      };
  
      reader.onerror = () => reject(new Error('Erreur lors de la lecture du fichier ODT.'));
      
      // Lire le fichier comme un ArrayBuffer
      reader.readAsArrayBuffer(file);
    });
  }

// Lire un fichier texte simple
function readFileAsText(file) {
    return new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.onload = (e) => resolve(e.target.result);
        reader.onerror = (e) => reject(new Error('Échec de la lecture du fichier.'));
        reader.readAsText(file);
    });
}

    
function insereContenu (content) {
   raz();
   texte_principal.innerHTML=content;
}

