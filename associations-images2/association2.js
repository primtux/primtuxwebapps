"use strict";
/*Tableau à double entrée :
- le No de la collection en 1er indice (3 collections)
- les images de la collection en 2ème indice (5 images par collection)
mémorisant les images telles qu'elles doivent être classées.
Contient les objets image */
const collection = [
  [],
  [],
  []
]
/* Tableau symétrique au précédent pour mémoriser les propositions faites lors de l'exercice.
Ne contient que les id des images */
const proposition = [
  [],
  [],
  []
]

// Paramètres de suivi des séries faites
const paramSeries = {
  numeroSerie: 0,
  seriesFaites: []
}

function alea(min,max) {
  let nb = min + (max-min+1) * Math.random();
  return Math.floor(nb);
}

function melange(tableau) {
  let tirage;
  let tab_alea = [];
  let i;
  for (i = 0; i < tableau.length; i++) {
    do {
      tirage = alea(0, tableau.length -1);
    } while (tableau[tirage] === 0);
    tab_alea[i] = tableau[tirage];
    tableau[tirage] = 0;
  }
  return tab_alea;
}

function effaceElement(tableau, element) {
  let i = 0;
  while ((i <= tableau.length) && (tableau[i] !== element)) {
    i++;
  }
  if (tableau[i] === element) {
    tableau.splice(i,1);
  }
  return tableau;
}

function creeCollectionImages() {
  const reserveImages = document.getElementById("zone-images");
  let indice1, indice2, compteur;
  let indices = [];
  let listeImages = [];
  let nomImg;
  compteur = 0;
  let i, j;
  /* Remplit la variable tableau collection avec les images de la série */
  for (i=0; i<3; i++) {
    indice1 = i + 1;
    for (j=0; j<5; j++) {
      indice2 = j + 1;
      nomImg = "img" + paramSeries.numeroSerie + "-" + indice1 + "-" + indice2;
      collection[i][j] = new Image();
      collection[i][j].src = "img/" + nomImg + ".png";
      collection[i][j].id = nomImg;
      collection[i][j].className = "imgCollection";
      collection[i][j].addEventListener("dragstart", drag);
      listeImages[compteur] = i + "-" + j;
      compteur += 1;
    }
  }
  /* crée une liste en désordre des images à classer, et les ajoute à la réserve d'images à classer */
  listeImages = melange(listeImages);
  for (compteur=0; compteur<15; compteur++) {
    indices = listeImages[compteur].split("-");
    reserveImages.appendChild(collection[indices[0]][indices[1]]);
  }
}

function effaceCollectionImages() {
  const tab = document.getElementsByClassName("imgCollection");
  let i;
  for (i = 0; i < tab.length; i++) {
    while (tab[i].firstChild) {
      tab[i].removeChild(tab[i].firstChild);
    }
  }
}

function effaceImage(NoCollection, Image) {
  proposition[NoCollection] = effaceElement(proposition[NoCollection], Image);
}

function getNoCollection(idObjet) {
  return parseInt(idObjet.charAt(10) - 1);
}

function getIdImage(objet) {
  let idImage = "";
  const tableau = objet.split("_");
  idImage = tableau[0];
  return idImage;
}

function getIdDepart(objet) {
  let IdDepart = "";
  const tableau = objet.split("_");
  IdDepart = tableau[1];
  return IdDepart;
}

function desactiveBtSerie() {
  let i;
  for (i=1; i<6; i++) {
    document.getElementById("serie" + i).disabled = true;
  }
}

function activeBtSerie() {
  let i;
  for (i=1; i<6; i++) {
    if (paramSeries.seriesFaites.indexOf(i) < 0) {
      document.getElementById("serie" + i).disabled = false;
    }
  }
}

function defSerie(serie) {
  if (paramSeries.seriesFaites.length > 0) {
    reinitialise();
  }
  paramSeries.numeroSerie = serie;
  desactiveBtSerie();
  commencer();
}

function effaceImages() {
  const tab = document.getElementsByClassName("collection");
  let i;
  for (i = 0; i < tab.length; i++) {
    while (tab[i].firstChild) {
      tab[i].removeChild(tab[i].firstChild);
    }
  }
}

function commencer() {
  creeCollectionImages();
  document.getElementById("verifier").disabled = false;
}

function  reinitialise() {
  let idErreur;
  let numeroErreur;
  for (var i = 1; i < 4; i++) {
    while (proposition[i - 1].length > 0) {
      proposition[i - 1].pop();
    }
    numeroErreur = i;
    idErreur = "erreur" + numeroErreur;
    document.getElementById(idErreur).innerHTML = "";
  }
  effaceImages();
  paramSeries.numeroSerie = 0;
}

function verification() {
  let erreurGlobale = false;
  let erreurPartielle;
  let idErreur;
  let serie;
  let numeroCollection;
  let i, j;
  for(i=0; i<3; i++) {
    erreurPartielle = false;
    numeroCollection = i + 1;
    idErreur = "erreur" + numeroCollection;
    if (proposition[i].length !== 5) {
      erreurPartielle = true;
      erreurGlobale = true;
      document.getElementById(IdErreur).innerHTML = "X";
      }
      else {
        serie = proposition[i][0].charAt(5);
        for (j=0; j<5; j++) {
          if (proposition[i][j].charAt(5) !== serie) {
            erreurPartielle = true;
            erreurGlobale = true;
            document.getElementById(idErreur).innerHTML = "X";
            break;
          }
        }
      }
    if (! erreurPartielle) {
      document.getElementById(idErreur).innerHTML = "";
    }
  }
  if (! erreurGlobale) {
    document.getElementById("verifier").disabled = true;
    paramSeries.seriesFaites.push(parseInt(paramSeries.numeroSerie));
    activeBtSerie();
    if (paramSeries.seriesFaites.length < 5) {showDialog('Bravo !',0.5,'img/happy-tux.png', 89, 91, 'left')}
    else {showDialog("Félicitations ! <br/>Tu as réussi à classer correctement les 5 séries d'images.",0.5,'img/trophee.png', 128, 128, 'left');}
  }
}

function drag(ev) {
  let infosObjet;
  infosObjet = ev.target.id + "_" + ev.target.parentNode.id;
  ev.dataTransfer.setData("text", infosObjet);
}

function allowDrop(ev) {
  ev.preventDefault();
}

function drop(ev) {
  let numeroCollection;
  let idImage, idDepart;
  let numeroCollectionDepart = [];
  ev.preventDefault();
  numeroCollection = getNoCollection(ev.target.id);
  if ((ev.target.className === "collection") && (proposition[numeroCollection].length < 10)) {
    idImage = getIdImage(ev.dataTransfer.getData("text"));
    idDepart = getIdDepart(ev.dataTransfer.getData("text"));
    numeroCollectionDepart = getNoCollection(idDepart);
    ev.target.appendChild(document.getElementById(idImage));
    proposition[numeroCollection].push(idImage);
    if (idDepart !== "zone-images") {
      effaceImage(numeroCollectionDepart, idImage);
    }
  }
}
