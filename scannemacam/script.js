// script.js

// Accès aux éléments du DOM
const video = document.getElementById('video');
const canvas = document.getElementById('canvas');
const captureButton = document.getElementById('capture');
const restartVideoButton = document.getElementById('restartVideo');
const cropButton = document.getElementById('crop');
const undoCropButton = document.getElementById('undoCrop');
const rotateLeftButton = document.getElementById('rotateLeft');
const rotateRightButton = document.getElementById('rotateRight');
const symHorizButton = document.getElementById('symHoriz');
const symVertButton = document.getElementById('symVert');
const flipHorizButton = document.getElementById('flipHorizButton');
const flipVertButton = document.getElementById('flipVertButton');
const rotateVideoLeftButton = document.getElementById('rotateVideoLeftButton'); // Nouveau bouton
const rotateVideoRightButton = document.getElementById('rotateVideoRightButton'); // Nouveau bouton
const centerCameraButton = document.getElementById('centerCameraButton');
const brightnessSlider = document.getElementById('brightness');
const contrastSlider = document.getElementById('contrast');
const saturationSlider = document.getElementById('saturation');
const exportImageButton = document.getElementById('exportImage');
const exportPDFButton = document.getElementById('exportPDF');
const copyImageButton = document.getElementById('copyImage');

let cropper;
let originalImage = null;
let preCropImage = null;
let rotationAngle = 0;
let flipHorizontal = 1;
let flipVertical = 1;
let videoRotationAngle = 0; // Variable pour la rotation de la vidéo
let videoFlipHorizontal = 1;
let videoFlipVertical = 1;

// Fonction pour démarrer la vidéo avec la résolution maximale
function startVideo() {
    const constraints = { video: { width: { ideal: 1920 }, height: { ideal: 1080 } } };

    navigator.mediaDevices.getUserMedia(constraints)
    .then(stream => {
        video.srcObject = stream;
        video.style.display = 'block';
        canvas.style.display = 'none'; // Cacher le canvas lors du démarrage de la vidéo
        controls.style.display = 'none'; // Cacher les contrôles lors du démarrage de la vidéo
        applyVideoTransforms();
        // Activer le bouton de capture, désactiver le bouton de réactivation
        captureButton.disabled = false;
        restartVideoButton.disabled = true;
    })
    .catch(err => {
        console.error('Erreur d\'accès à la webcam:', err);
    });
}

// Démarrer la vidéo au chargement de la page
startVideo();

// Fonction pour centrer la caméra verticalement dans la fenêtre
function centerCamera() {
    // Obtenir la hauteur de la fenêtre
    const windowHeight = window.innerHeight;
    // Obtenir la position du 'camera' par rapport au document
    const cameraElement = document.getElementById('camera');
    const cameraRect = cameraElement.getBoundingClientRect();
    const cameraTop = cameraRect.top + window.pageYOffset;
    const cameraHeight = cameraRect.height;
    // Calculer la position pour centrer le 'camera' verticalement
    const scrollToPosition = cameraTop - (windowHeight / 2) + (cameraHeight / 2);
    // Faire défiler la page jusqu'à cette position avec un comportement doux
    window.scrollTo({
        top: scrollToPosition,
        behavior: 'smooth'
    });
}

// Ajouter un écouteur pour le bouton de centrage de la caméra
centerCameraButton.addEventListener('click', centerCamera);

// Ajouter un écouteur pour l'événement 'load' de la fenêtre pour centrer au chargement
window.addEventListener('load', centerCamera);

// Fonction pour appliquer les transformations sur la vidéo
function applyVideoTransforms() {
    video.style.transform = `rotate(${videoRotationAngle}deg) scale(${videoFlipHorizontal}, ${videoFlipVertical})`;
}

// Écouteurs pour les boutons de rotation de la vidéo
rotateVideoLeftButton.addEventListener('click', () => {
    videoRotationAngle = (videoRotationAngle - 90) % 360;
    applyVideoTransforms();
});

rotateVideoRightButton.addEventListener('click', () => {
    videoRotationAngle = (videoRotationAngle + 90) % 360;
    applyVideoTransforms();
});

// Écouteurs pour les boutons de symétrie de la vidéo
flipHorizButton.addEventListener('click', () => {
    videoFlipHorizontal *= -1; // Inverser la valeur pour effectuer le miroir horizontal
    applyVideoTransforms();
});

flipVertButton.addEventListener('click', () => {
    videoFlipVertical *= -1; // Inverser la valeur pour effectuer le miroir vertical
    applyVideoTransforms();
});

// Capturer l'image
captureButton.addEventListener('click', () => {
    const context = canvas.getContext('2d');

    // Obtenir les dimensions de la vidéo
    const videoWidth = video.videoWidth;
    const videoHeight = video.videoHeight;

    // Calculer les dimensions du canvas en fonction de la rotation
    if (videoRotationAngle % 180 !== 0) {
        canvas.width = videoHeight;
        canvas.height = videoWidth;
    } else {
        canvas.width = videoWidth;
        canvas.height = videoHeight;
    }

    // Sauvegarder le contexte avant les transformations
    context.save();

    // Déplacer le contexte au centre du canvas
    context.translate(canvas.width / 2, canvas.height / 2);

    // Appliquer les transformations de rotation et de symétrie
    context.rotate((videoRotationAngle * Math.PI) / 180);
    context.scale(videoFlipHorizontal, videoFlipVertical);

    // Dessiner la vidéo avec les transformations appliquées
    context.drawImage(
        video,
        -videoWidth / 2,
        -videoHeight / 2,
        videoWidth,
        videoHeight
    );

    // Restaurer le contexte à son état initial
    context.restore();

    originalImage = new Image();
    originalImage.src = canvas.toDataURL();
    canvas.style.display = 'block'; // Afficher le canvas lors de la capture
    controls.style.display = 'block'; // Afficher les contrôles lors de la capture

    // Arrêter le flux vidéo
    const stream = video.srcObject;
    const tracks = stream.getTracks();
    tracks.forEach(track => track.stop());
    video.srcObject = null;
    video.style.display = 'none';

    // Désactiver le bouton de capture, activer le bouton de réactivation
    captureButton.disabled = true;
    restartVideoButton.disabled = false;

    // Réinitialiser les transformations pour l'image capturée
    rotationAngle = 0;
    flipHorizontal = 1;
    flipVertical = 1;

    // Désactiver les boutons et curseurs
    rotateLeftButton.disabled = true;
    rotateRightButton.disabled = true;
    symHorizButton.disabled = true;
    symVertButton.disabled = true;
    brightnessSlider.disabled = true;
    contrastSlider.disabled = true;
    saturationSlider.disabled = true;
    copyImageButton.disabled = true;
    exportPDFButton.disabled = true;
    exportImageButton.disabled = true;
    undoCropButton.disabled = true;

    // Faire défiler la page vers les contrôles
    document.getElementById('controls').scrollIntoView({ behavior: 'smooth' });

    if (cropper) {
        cropper.destroy();
    }
    cropper = new Cropper(canvas, {
        aspectRatio: NaN,
        viewMode: 1,
    });
});

// Relancer la caméra
restartVideoButton.addEventListener('click', () => {
    startVideo();
    // Activer le bouton de capture, désactiver le bouton de réactivation
    captureButton.disabled = false;
    restartVideoButton.disabled = true;

    // Cacher le canvas lors du redémarrage de la caméra
    canvas.style.display = 'none';

    // Réinitialiser les transformations de la vidéo
    videoRotationAngle = 0;
    videoFlipHorizontal = 1;
    videoFlipVertical = 1;
    applyVideoTransforms();

    // Réinitialiser le cropper si nécessaire
    if (cropper) {
        cropper.destroy();
        cropper = null;
    }
});

// Recadrer l'image
cropButton.addEventListener('click', () => {
    if (cropper) {
        // Sauvegarder l'image originale avant le recadrage
        preCropImage = originalImage;

        const croppedCanvas = cropper.getCroppedCanvas();
        canvas.width = croppedCanvas.width;
        canvas.height = croppedCanvas.height;
        const context = canvas.getContext('2d');
        context.clearRect(0, 0, canvas.width, canvas.height);
        context.drawImage(croppedCanvas, 0, 0);

        cropper.destroy();
        cropper = null;

        // Mettre à jour l'image originale
        originalImage = new Image();
        originalImage.onload = function() {
            // Appliquer les filtres après que l'image soit chargée
            applyFilters();
        };
        originalImage.src = canvas.toDataURL();

        // Activer les boutons et curseurs
        rotateLeftButton.disabled = false;
        rotateRightButton.disabled = false;
        symHorizButton.disabled = false;
        symVertButton.disabled = false;
        brightnessSlider.disabled = false;
        contrastSlider.disabled = false;
        saturationSlider.disabled = false;
        copyImageButton.disabled = false;
        exportPDFButton.disabled = false;
        exportImageButton.disabled = false;
        undoCropButton.disabled = false;
    }
});

// Annuler le recadrage
undoCropButton.addEventListener('click', () => {
    if (preCropImage) {
        originalImage = preCropImage;
        canvas.width = originalImage.width;
        canvas.height = originalImage.height;
        const context = canvas.getContext('2d');
        context.clearRect(0, 0, canvas.width, canvas.height);

        // Réinitialiser les transformations
        rotationAngle = 0;
        flipHorizontal = 1;
        flipVertical = 1;

        // Afficher l'image originale sans recadrage
        originalImage.onload = function() {
            context.drawImage(originalImage, 0, 0, canvas.width, canvas.height);

            // Réinitialiser les curseurs
            brightnessSlider.value = 0;
            contrastSlider.value = 0;
            saturationSlider.value = 0;

            // Désactiver les boutons et curseurs
            rotateLeftButton.disabled = true;
            rotateRightButton.disabled = true;
            symHorizButton.disabled = true;
            symVertButton.disabled = true;
            brightnessSlider.disabled = true;
            contrastSlider.disabled = true;
            saturationSlider.disabled = true;
            copyImageButton.disabled = true;
            exportPDFButton.disabled = true;
            exportImageButton.disabled = true;
            undoCropButton.disabled = true;

            // Réinitialiser les filtres
            context.filter = 'none';

            // Recréer le cropper pour permettre de recadrer à nouveau
            if (cropper) {
                cropper.destroy();
            }
            cropper = new Cropper(canvas, {
                aspectRatio: NaN,
                viewMode: 1,
            });
        };
        originalImage.src = preCropImage.src;

        // Supprimer preCropImage pour éviter plusieurs annulations successives
        preCropImage = null;
    }
});

// Fonction pour appliquer les filtres et les transformations sur l'image capturée
function applyFilters() {
    if (!originalImage) return;
    const brightness = parseInt(brightnessSlider.value);
    const contrast = parseInt(contrastSlider.value);
    const saturation = parseInt(saturationSlider.value);
    const context = canvas.getContext('2d');

    // Effacer le canvas
    context.clearRect(0, 0, canvas.width, canvas.height);

    // Sauvegarder le contexte avant les transformations
    context.save();

    // Déplacer le contexte au centre de l'image pour les transformations
    context.translate(canvas.width / 2, canvas.height / 2);

    // Appliquer la rotation
    context.rotate((rotationAngle * Math.PI) / 180);

    // Appliquer les symétries
    context.scale(flipHorizontal, flipVertical);

    // Définir les filtres
    context.filter = `brightness(${brightness + 100}%) contrast(${contrast + 100}%) saturate(${saturation + 100}%)`;

    // Dessiner l'image avec les transformations appliquées
    context.drawImage(
        originalImage,
        -originalImage.width / 2,
        -originalImage.height / 2,
        originalImage.width,
        originalImage.height
    );

    // Restaurer le contexte à son état initial
    context.restore();

    // Réinitialiser le filtre
    context.filter = 'none';
}

// Écouteurs pour les curseurs
brightnessSlider.addEventListener('input', applyFilters);
contrastSlider.addEventListener('input', applyFilters);
saturationSlider.addEventListener('input', applyFilters);

// Écouteurs pour les boutons de rotation sur l'image capturée
rotateLeftButton.addEventListener('click', () => {
    rotationAngle = (rotationAngle - 90) % 360;
    adjustCanvasSize();
    applyFilters();
});

rotateRightButton.addEventListener('click', () => {
    rotationAngle = (rotationAngle + 90) % 360;
    adjustCanvasSize();
    applyFilters();
});

// Écouteurs pour les boutons de symétrie sur l'image capturée
symHorizButton.addEventListener('click', () => {
    flipVertical *= -1; // Inverser la valeur pour effectuer le miroir vertical
    applyFilters();
});

symVertButton.addEventListener('click', () => {
    flipHorizontal *= -1; // Inverser la valeur pour effectuer le miroir horizontal
    applyFilters();
});

// Fonction pour ajuster la taille du canvas après rotation
function adjustCanvasSize() {
    // Inverser largeur et hauteur si nécessaire
    if (rotationAngle % 180 !== 0) {
        [canvas.width, canvas.height] = [canvas.height, canvas.width];
    } else {
        canvas.width = originalImage.width;
        canvas.height = originalImage.height;
    }
}

// Exporter l'image en PNG
exportImageButton.addEventListener('click', () => {
    const link = document.createElement('a');
    link.download = 'image.png';
    link.href = canvas.toDataURL('image/png');
    link.click();
});

// Copier l'image dans le presse-papiers
copyImageButton.addEventListener('click', async () => {
    try {
        // Convertir le canvas en Blob
        canvas.toBlob(async (blob) => {
            const item = new ClipboardItem({ 'image/png': blob });
            await navigator.clipboard.write([item]);
            alert('Image copiée dans le presse-papiers !');
        }, 'image/png');
    } catch (err) {
        console.error('Erreur lors de la copie de l\'image :', err);
        alert('Erreur lors de la copie de l\'image. Veuillez réessayer.');
    }
});

// Exporter l'image en PDF
exportPDFButton.addEventListener('click', () => {
    const { jsPDF } = window.jspdf;
    const pdf = new jsPDF();
    const imgData = canvas.toDataURL('image/png');
    const imgWidth = canvas.width;
    const imgHeight = canvas.height;
    // Ajuster la taille de l'image dans le PDF si nécessaire
    const pdfWidth = pdf.internal.pageSize.getWidth();
    const pdfHeight = (imgHeight * pdfWidth) / imgWidth;
    pdf.addImage(imgData, 'PNG', 0, 0, pdfWidth, pdfHeight);
    pdf.save('document.pdf');
});

// Ajout d'un écouteur de clic sur la vidéo pour effectuer la capture
video.addEventListener('click', () => {
    // On simule le clic sur le bouton "capture"
    captureButton.click();
});

// JS pour le bouton de masquage/affichage des contrôles

const toggleControlsButton = document.getElementById('toggleControlsButton');
const controlsDiv = document.getElementById('controls');
let controlsVisible = true;

toggleControlsButton.addEventListener('click', () => {
    controlsVisible = !controlsVisible;
    if (controlsVisible) {
        controlsDiv.style.display = 'block';
        toggleControlsButton.innerHTML = '<i class="fas fa-eye-slash"></i> Masquer les contrôles';
    } else {
        controlsDiv.style.display = 'none';
        toggleControlsButton.innerHTML = '<i class="fas fa-eye"></i> Afficher les contrôles';
    }
});