gdjs.infosCode = {};
gdjs.infosCode.GDfond_95arbreObjects1= [];
gdjs.infosCode.GDfond_95arbreObjects2= [];
gdjs.infosCode.GDfond_95arbreObjects3= [];
gdjs.infosCode.GDyeuxObjects1= [];
gdjs.infosCode.GDyeuxObjects2= [];
gdjs.infosCode.GDyeuxObjects3= [];
gdjs.infosCode.GDfond_95bleuObjects1= [];
gdjs.infosCode.GDfond_95bleuObjects2= [];
gdjs.infosCode.GDfond_95bleuObjects3= [];
gdjs.infosCode.GDsolObjects1= [];
gdjs.infosCode.GDsolObjects2= [];
gdjs.infosCode.GDsolObjects3= [];
gdjs.infosCode.GDnuage1Objects1= [];
gdjs.infosCode.GDnuage1Objects2= [];
gdjs.infosCode.GDnuage1Objects3= [];
gdjs.infosCode.GDnuage2Objects1= [];
gdjs.infosCode.GDnuage2Objects2= [];
gdjs.infosCode.GDnuage2Objects3= [];
gdjs.infosCode.GDnuage3Objects1= [];
gdjs.infosCode.GDnuage3Objects2= [];
gdjs.infosCode.GDnuage3Objects3= [];
gdjs.infosCode.GDpomme_95Objects1= [];
gdjs.infosCode.GDpomme_95Objects2= [];
gdjs.infosCode.GDpomme_95Objects3= [];
gdjs.infosCode.GDpomme_953Objects1= [];
gdjs.infosCode.GDpomme_953Objects2= [];
gdjs.infosCode.GDpomme_953Objects3= [];
gdjs.infosCode.GDpomme_952Objects1= [];
gdjs.infosCode.GDpomme_952Objects2= [];
gdjs.infosCode.GDpomme_952Objects3= [];
gdjs.infosCode.GDcaisse_953Objects1= [];
gdjs.infosCode.GDcaisse_953Objects2= [];
gdjs.infosCode.GDcaisse_953Objects3= [];
gdjs.infosCode.GDcaisse_952Objects1= [];
gdjs.infosCode.GDcaisse_952Objects2= [];
gdjs.infosCode.GDcaisse_952Objects3= [];
gdjs.infosCode.GDcaisse_95Objects1= [];
gdjs.infosCode.GDcaisse_95Objects2= [];
gdjs.infosCode.GDcaisse_95Objects3= [];
gdjs.infosCode.GDscoreObjects1= [];
gdjs.infosCode.GDscoreObjects2= [];
gdjs.infosCode.GDscoreObjects3= [];
gdjs.infosCode.GDbouton_95retourObjects1= [];
gdjs.infosCode.GDbouton_95retourObjects2= [];
gdjs.infosCode.GDbouton_95retourObjects3= [];
gdjs.infosCode.GDnb_95pommesObjects1= [];
gdjs.infosCode.GDnb_95pommesObjects2= [];
gdjs.infosCode.GDnb_95pommesObjects3= [];
gdjs.infosCode.GDnb_95pommes2Objects1= [];
gdjs.infosCode.GDnb_95pommes2Objects2= [];
gdjs.infosCode.GDnb_95pommes2Objects3= [];
gdjs.infosCode.GDtitreObjects1= [];
gdjs.infosCode.GDtitreObjects2= [];
gdjs.infosCode.GDtitreObjects3= [];
gdjs.infosCode.GDserge_95niveau4Objects1= [];
gdjs.infosCode.GDserge_95niveau4Objects2= [];
gdjs.infosCode.GDserge_95niveau4Objects3= [];
gdjs.infosCode.GDserge_95lamaObjects1= [];
gdjs.infosCode.GDserge_95lamaObjects2= [];
gdjs.infosCode.GDserge_95lamaObjects3= [];
gdjs.infosCode.GDcreditsObjects1= [];
gdjs.infosCode.GDcreditsObjects2= [];
gdjs.infosCode.GDcreditsObjects3= [];
gdjs.infosCode.GDversionObjects1= [];
gdjs.infosCode.GDversionObjects2= [];
gdjs.infosCode.GDversionObjects3= [];
gdjs.infosCode.GDauteurObjects1= [];
gdjs.infosCode.GDauteurObjects2= [];
gdjs.infosCode.GDauteurObjects3= [];
gdjs.infosCode.GDinfoObjects1= [];
gdjs.infosCode.GDinfoObjects2= [];
gdjs.infosCode.GDinfoObjects3= [];
gdjs.infosCode.GDfond_95blancObjects1= [];
gdjs.infosCode.GDfond_95blancObjects2= [];
gdjs.infosCode.GDfond_95blancObjects3= [];
gdjs.infosCode.GDinfo1Objects1= [];
gdjs.infosCode.GDinfo1Objects2= [];
gdjs.infosCode.GDinfo1Objects3= [];

gdjs.infosCode.conditionTrue_0 = {val:false};
gdjs.infosCode.condition0IsTrue_0 = {val:false};
gdjs.infosCode.condition1IsTrue_0 = {val:false};
gdjs.infosCode.condition2IsTrue_0 = {val:false};


gdjs.infosCode.eventsList0 = function(runtimeScene) {

{


{
gdjs.copyArray(runtimeScene.getObjects("nuage1"), gdjs.infosCode.GDnuage1Objects2);
gdjs.copyArray(runtimeScene.getObjects("nuage2"), gdjs.infosCode.GDnuage2Objects2);
gdjs.copyArray(runtimeScene.getObjects("nuage3"), gdjs.infosCode.GDnuage3Objects2);
{for(var i = 0, len = gdjs.infosCode.GDnuage1Objects2.length ;i < len;++i) {
    gdjs.infosCode.GDnuage1Objects2[i].setX(gdjs.infosCode.GDnuage1Objects2[i].getX() + (0.2));
}
}{for(var i = 0, len = gdjs.infosCode.GDnuage2Objects2.length ;i < len;++i) {
    gdjs.infosCode.GDnuage2Objects2[i].setX(gdjs.infosCode.GDnuage2Objects2[i].getX() + (0.2));
}
}{for(var i = 0, len = gdjs.infosCode.GDnuage3Objects2.length ;i < len;++i) {
    gdjs.infosCode.GDnuage3Objects2[i].setX(gdjs.infosCode.GDnuage3Objects2[i].getX() + (0.2));
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("nuage1"), gdjs.infosCode.GDnuage1Objects2);

gdjs.infosCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.infosCode.GDnuage1Objects2.length;i<l;++i) {
    if ( gdjs.infosCode.GDnuage1Objects2[i].getX() > 1300 ) {
        gdjs.infosCode.condition0IsTrue_0.val = true;
        gdjs.infosCode.GDnuage1Objects2[k] = gdjs.infosCode.GDnuage1Objects2[i];
        ++k;
    }
}
gdjs.infosCode.GDnuage1Objects2.length = k;}if (gdjs.infosCode.condition0IsTrue_0.val) {
/* Reuse gdjs.infosCode.GDnuage1Objects2 */
{for(var i = 0, len = gdjs.infosCode.GDnuage1Objects2.length ;i < len;++i) {
    gdjs.infosCode.GDnuage1Objects2[i].setX(-(300));
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("nuage2"), gdjs.infosCode.GDnuage2Objects2);

gdjs.infosCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.infosCode.GDnuage2Objects2.length;i<l;++i) {
    if ( gdjs.infosCode.GDnuage2Objects2[i].getX() > 1300 ) {
        gdjs.infosCode.condition0IsTrue_0.val = true;
        gdjs.infosCode.GDnuage2Objects2[k] = gdjs.infosCode.GDnuage2Objects2[i];
        ++k;
    }
}
gdjs.infosCode.GDnuage2Objects2.length = k;}if (gdjs.infosCode.condition0IsTrue_0.val) {
/* Reuse gdjs.infosCode.GDnuage2Objects2 */
{for(var i = 0, len = gdjs.infosCode.GDnuage2Objects2.length ;i < len;++i) {
    gdjs.infosCode.GDnuage2Objects2[i].setX(-(300));
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("nuage3"), gdjs.infosCode.GDnuage3Objects1);

gdjs.infosCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.infosCode.GDnuage3Objects1.length;i<l;++i) {
    if ( gdjs.infosCode.GDnuage3Objects1[i].getX() > 1300 ) {
        gdjs.infosCode.condition0IsTrue_0.val = true;
        gdjs.infosCode.GDnuage3Objects1[k] = gdjs.infosCode.GDnuage3Objects1[i];
        ++k;
    }
}
gdjs.infosCode.GDnuage3Objects1.length = k;}if (gdjs.infosCode.condition0IsTrue_0.val) {
/* Reuse gdjs.infosCode.GDnuage3Objects1 */
{for(var i = 0, len = gdjs.infosCode.GDnuage3Objects1.length ;i < len;++i) {
    gdjs.infosCode.GDnuage3Objects1[i].setX(-(300));
}
}}

}


};gdjs.infosCode.mapOfGDgdjs_46infosCode_46GDbouton_9595retourObjects1Objects = Hashtable.newFrom({"bouton_retour": gdjs.infosCode.GDbouton_95retourObjects1});gdjs.infosCode.eventsList1 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("bouton_retour"), gdjs.infosCode.GDbouton_95retourObjects1);

gdjs.infosCode.condition0IsTrue_0.val = false;
gdjs.infosCode.condition1IsTrue_0.val = false;
{
gdjs.infosCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.infosCode.mapOfGDgdjs_46infosCode_46GDbouton_9595retourObjects1Objects, runtimeScene, true, false);
}if ( gdjs.infosCode.condition0IsTrue_0.val ) {
{
gdjs.infosCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}}
if (gdjs.infosCode.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "menu", true);
}}

}


};gdjs.infosCode.eventsList2 = function(runtimeScene) {

{


gdjs.infosCode.condition0IsTrue_0.val = false;
{
gdjs.infosCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.infosCode.condition0IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("fond_blanc"), gdjs.infosCode.GDfond_95blancObjects1);
gdjs.copyArray(runtimeScene.getObjects("yeux"), gdjs.infosCode.GDyeuxObjects1);
{for(var i = 0, len = gdjs.infosCode.GDyeuxObjects1.length ;i < len;++i) {
    gdjs.infosCode.GDyeuxObjects1[i].pauseAnimation();
}
}{for(var i = 0, len = gdjs.infosCode.GDfond_95blancObjects1.length ;i < len;++i) {
    gdjs.infosCode.GDfond_95blancObjects1[i].setOpacity(200);
}
}}

}


{


gdjs.infosCode.eventsList0(runtimeScene);
}


{


gdjs.infosCode.eventsList1(runtimeScene);
}


{


{
}

}


};

gdjs.infosCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.infosCode.GDfond_95arbreObjects1.length = 0;
gdjs.infosCode.GDfond_95arbreObjects2.length = 0;
gdjs.infosCode.GDfond_95arbreObjects3.length = 0;
gdjs.infosCode.GDyeuxObjects1.length = 0;
gdjs.infosCode.GDyeuxObjects2.length = 0;
gdjs.infosCode.GDyeuxObjects3.length = 0;
gdjs.infosCode.GDfond_95bleuObjects1.length = 0;
gdjs.infosCode.GDfond_95bleuObjects2.length = 0;
gdjs.infosCode.GDfond_95bleuObjects3.length = 0;
gdjs.infosCode.GDsolObjects1.length = 0;
gdjs.infosCode.GDsolObjects2.length = 0;
gdjs.infosCode.GDsolObjects3.length = 0;
gdjs.infosCode.GDnuage1Objects1.length = 0;
gdjs.infosCode.GDnuage1Objects2.length = 0;
gdjs.infosCode.GDnuage1Objects3.length = 0;
gdjs.infosCode.GDnuage2Objects1.length = 0;
gdjs.infosCode.GDnuage2Objects2.length = 0;
gdjs.infosCode.GDnuage2Objects3.length = 0;
gdjs.infosCode.GDnuage3Objects1.length = 0;
gdjs.infosCode.GDnuage3Objects2.length = 0;
gdjs.infosCode.GDnuage3Objects3.length = 0;
gdjs.infosCode.GDpomme_95Objects1.length = 0;
gdjs.infosCode.GDpomme_95Objects2.length = 0;
gdjs.infosCode.GDpomme_95Objects3.length = 0;
gdjs.infosCode.GDpomme_953Objects1.length = 0;
gdjs.infosCode.GDpomme_953Objects2.length = 0;
gdjs.infosCode.GDpomme_953Objects3.length = 0;
gdjs.infosCode.GDpomme_952Objects1.length = 0;
gdjs.infosCode.GDpomme_952Objects2.length = 0;
gdjs.infosCode.GDpomme_952Objects3.length = 0;
gdjs.infosCode.GDcaisse_953Objects1.length = 0;
gdjs.infosCode.GDcaisse_953Objects2.length = 0;
gdjs.infosCode.GDcaisse_953Objects3.length = 0;
gdjs.infosCode.GDcaisse_952Objects1.length = 0;
gdjs.infosCode.GDcaisse_952Objects2.length = 0;
gdjs.infosCode.GDcaisse_952Objects3.length = 0;
gdjs.infosCode.GDcaisse_95Objects1.length = 0;
gdjs.infosCode.GDcaisse_95Objects2.length = 0;
gdjs.infosCode.GDcaisse_95Objects3.length = 0;
gdjs.infosCode.GDscoreObjects1.length = 0;
gdjs.infosCode.GDscoreObjects2.length = 0;
gdjs.infosCode.GDscoreObjects3.length = 0;
gdjs.infosCode.GDbouton_95retourObjects1.length = 0;
gdjs.infosCode.GDbouton_95retourObjects2.length = 0;
gdjs.infosCode.GDbouton_95retourObjects3.length = 0;
gdjs.infosCode.GDnb_95pommesObjects1.length = 0;
gdjs.infosCode.GDnb_95pommesObjects2.length = 0;
gdjs.infosCode.GDnb_95pommesObjects3.length = 0;
gdjs.infosCode.GDnb_95pommes2Objects1.length = 0;
gdjs.infosCode.GDnb_95pommes2Objects2.length = 0;
gdjs.infosCode.GDnb_95pommes2Objects3.length = 0;
gdjs.infosCode.GDtitreObjects1.length = 0;
gdjs.infosCode.GDtitreObjects2.length = 0;
gdjs.infosCode.GDtitreObjects3.length = 0;
gdjs.infosCode.GDserge_95niveau4Objects1.length = 0;
gdjs.infosCode.GDserge_95niveau4Objects2.length = 0;
gdjs.infosCode.GDserge_95niveau4Objects3.length = 0;
gdjs.infosCode.GDserge_95lamaObjects1.length = 0;
gdjs.infosCode.GDserge_95lamaObjects2.length = 0;
gdjs.infosCode.GDserge_95lamaObjects3.length = 0;
gdjs.infosCode.GDcreditsObjects1.length = 0;
gdjs.infosCode.GDcreditsObjects2.length = 0;
gdjs.infosCode.GDcreditsObjects3.length = 0;
gdjs.infosCode.GDversionObjects1.length = 0;
gdjs.infosCode.GDversionObjects2.length = 0;
gdjs.infosCode.GDversionObjects3.length = 0;
gdjs.infosCode.GDauteurObjects1.length = 0;
gdjs.infosCode.GDauteurObjects2.length = 0;
gdjs.infosCode.GDauteurObjects3.length = 0;
gdjs.infosCode.GDinfoObjects1.length = 0;
gdjs.infosCode.GDinfoObjects2.length = 0;
gdjs.infosCode.GDinfoObjects3.length = 0;
gdjs.infosCode.GDfond_95blancObjects1.length = 0;
gdjs.infosCode.GDfond_95blancObjects2.length = 0;
gdjs.infosCode.GDfond_95blancObjects3.length = 0;
gdjs.infosCode.GDinfo1Objects1.length = 0;
gdjs.infosCode.GDinfo1Objects2.length = 0;
gdjs.infosCode.GDinfo1Objects3.length = 0;

gdjs.infosCode.eventsList2(runtimeScene);
return;

}

gdjs['infosCode'] = gdjs.infosCode;
