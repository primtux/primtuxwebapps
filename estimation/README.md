# Estimation

**Estimation** est une application éducative libre et gratuite faisant partie de la suite **Éducajou**. Elle permet aux élèves de s'exercer à estimer des valeurs à partir d'une position sur une droite.

## Fonctionnalités

- **Deux modes de réponse** : l'élève peut répondre via un clavier virtuel ou choisir une réponse parmi plusieurs options dans un QCM.
- **Personnalisation des valeurs** : possibilité d'ajuster les bornes minimale et maximale des exercices.
- **Affichage des résultats** : une section statistiques récapitule les performances de l'élève.
- **Trois niveaux d'étayage** pour aider à la représentation mentale :
  - **Première graduation** : affichage d'une seule graduation pour aider à visualiser l'échelle.
  - **Bloc unité déplaçable** : un bloc que l'élève peut déplacer pour faciliter la compréhension des unités.
  - **Affichage de toutes les graduations** : toutes les graduations sont affichées pour un soutien plus complet.

## Utilisation en ligne

L'application **Estimation** est accessible sans installation à l'adresse suivante :

[https://educajou.forge.apps.education.fr/estimation](https://educajou.forge.apps.education.fr/estimation)

## Utilisation hors ligne

Cette application est également intégrée à la distribution Linux **Primtux**, une distribution éducative destinée aux écoles. Dans **Primtux**, l'application est utilisable sans connexion Internet, directement depuis l'environnement de bureau.

## Suite Éducajou

Cette application fait partie de la suite **Éducajou**, une collection d'outils éducatifs libres visant à soutenir l'apprentissage.

## Prérequis

- Un navigateur web moderne (Chrome, Firefox, Edge, etc.)

## Installation (pour usage local)

1. Téléchargez ou clonez les fichiers du projet.
2. Placez tous les fichiers (HTML, CSS, JavaScript, et images) dans le même répertoire.
3. Ouvrez `index.html` dans votre navigateur.

## Utilisation en local

1. **Lancer l'application** : Ouvrez `index.html` dans le navigateur.
2. **Choisir un mode de réponse** : Sélectionnez le mode clavier ou QCM dans le menu des options.
3. **Exercices** :
   - En mode **clavier**, saisissez un nombre via les touches virtuelles.
   - En mode **QCM**, sélectionnez la bonne réponse parmi les options proposées.
4. **Aide à l'estimation** : Utilisez l'un des trois niveaux d'étayage (première graduation, bloc unité déplaçable, ou toutes les graduations) pour faciliter l'estimation.
5. **Consulter les statistiques** : Accédez à la section "Statistiques" pour suivre vos résultats.

## Personnalisation

- Vous pouvez ajuster les valeurs minimale et maximale des exercices dans le menu des options.
- Les fichiers `style.css` et `script.js` peuvent être modifiés pour adapter l'apparence ou le comportement de l'application.

## Contribution

Les contributions sont les bienvenues ! Vous pouvez contribuer au développement de l'application de plusieurs façons :

- **Proposer des améliorations ou des fonctionnalités** : clonez le projet, faites vos modifications, et soumettez une pull request.
- **Signaler des bugs ou suggérer des améliorations** : si vous n'êtes pas développeur, vous pouvez créer un ticket pour signaler un problème ou proposer une idée. Cela aidera la communauté à améliorer l'application. Les tickets peuvent être créés ou consultés ici :  
  [https://forge.apps.education.fr/educajou/estimation/-/issues](https://forge.apps.education.fr/educajou/estimation/-/issues)

N'hésitez pas à consulter les tickets existants et à y participer en ajoutant vos commentaires ou en proposant des solutions.

## Licence

Ce projet est sous licence **GPLv3**. Vous êtes libre de l'utiliser, de le modifier et de le redistribuer, tant que toute version dérivée est publiée sous la même licence.

Pour plus de détails sur la licence GPL, consultez le fichier [LICENSE](https://www.gnu.org/licenses/gpl-3.0.fr.html).

---

### À propos d'Éducajou

**Éducajou** est une suite d'outils éducatifs libres qui vise à faciliter l'apprentissage à travers des applications adaptées.
