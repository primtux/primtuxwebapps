gdjs.infosCode = {};
gdjs.infosCode.GDbouton_95retourObjects1= [];
gdjs.infosCode.GDbouton_95retourObjects2= [];
gdjs.infosCode.GDscoreObjects1= [];
gdjs.infosCode.GDscoreObjects2= [];
gdjs.infosCode.GDileObjects1= [];
gdjs.infosCode.GDileObjects2= [];
gdjs.infosCode.GDile2Objects1= [];
gdjs.infosCode.GDile2Objects2= [];
gdjs.infosCode.GDile3Objects1= [];
gdjs.infosCode.GDile3Objects2= [];
gdjs.infosCode.GDile4Objects1= [];
gdjs.infosCode.GDile4Objects2= [];
gdjs.infosCode.GDile5Objects1= [];
gdjs.infosCode.GDile5Objects2= [];
gdjs.infosCode.GDile6Objects1= [];
gdjs.infosCode.GDile6Objects2= [];
gdjs.infosCode.GDfaux_95vraiObjects1= [];
gdjs.infosCode.GDfaux_95vraiObjects2= [];
gdjs.infosCode.GDfond_95blancObjects1= [];
gdjs.infosCode.GDfond_95blancObjects2= [];
gdjs.infosCode.GDfond_95boisObjects1= [];
gdjs.infosCode.GDfond_95boisObjects2= [];
gdjs.infosCode.GDvieObjects1= [];
gdjs.infosCode.GDvieObjects2= [];
gdjs.infosCode.GDdrapeauObjects1= [];
gdjs.infosCode.GDdrapeauObjects2= [];
gdjs.infosCode.GDserge_95capitaineObjects1= [];
gdjs.infosCode.GDserge_95capitaineObjects2= [];
gdjs.infosCode.GDaide_95sergeObjects1= [];
gdjs.infosCode.GDaide_95sergeObjects2= [];
gdjs.infosCode.GDvert_95abscisseObjects1= [];
gdjs.infosCode.GDvert_95abscisseObjects2= [];
gdjs.infosCode.GDjaune_95ordonneeObjects1= [];
gdjs.infosCode.GDjaune_95ordonneeObjects2= [];
gdjs.infosCode.GDpelle_95carteObjects1= [];
gdjs.infosCode.GDpelle_95carteObjects2= [];
gdjs.infosCode.GDbouton_95continuerObjects1= [];
gdjs.infosCode.GDbouton_95continuerObjects2= [];
gdjs.infosCode.GDbarreObjects1= [];
gdjs.infosCode.GDbarreObjects2= [];
gdjs.infosCode.GDfond_95menuObjects1= [];
gdjs.infosCode.GDfond_95menuObjects2= [];
gdjs.infosCode.GDinfosObjects1= [];
gdjs.infosCode.GDinfosObjects2= [];

gdjs.infosCode.conditionTrue_0 = {val:false};
gdjs.infosCode.condition0IsTrue_0 = {val:false};
gdjs.infosCode.condition1IsTrue_0 = {val:false};
gdjs.infosCode.condition2IsTrue_0 = {val:false};


gdjs.infosCode.mapOfGDgdjs_46infosCode_46GDbouton_9595retourObjects1Objects = Hashtable.newFrom({"bouton_retour": gdjs.infosCode.GDbouton_95retourObjects1});
gdjs.infosCode.eventsList0 = function(runtimeScene) {

{


gdjs.infosCode.condition0IsTrue_0.val = false;
{
gdjs.infosCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.infosCode.condition0IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("fond_blanc"), gdjs.infosCode.GDfond_95blancObjects1);
{for(var i = 0, len = gdjs.infosCode.GDfond_95blancObjects1.length ;i < len;++i) {
    gdjs.infosCode.GDfond_95blancObjects1[i].setOpacity(150);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_retour"), gdjs.infosCode.GDbouton_95retourObjects1);

gdjs.infosCode.condition0IsTrue_0.val = false;
gdjs.infosCode.condition1IsTrue_0.val = false;
{
gdjs.infosCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.infosCode.mapOfGDgdjs_46infosCode_46GDbouton_9595retourObjects1Objects, runtimeScene, true, false);
}if ( gdjs.infosCode.condition0IsTrue_0.val ) {
{
gdjs.infosCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}}
if (gdjs.infosCode.condition1IsTrue_0.val) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/bulle_1.mp3", 1, false, 100, 1);
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "menu", false);
}}

}


{


{
}

}


};

gdjs.infosCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.infosCode.GDbouton_95retourObjects1.length = 0;
gdjs.infosCode.GDbouton_95retourObjects2.length = 0;
gdjs.infosCode.GDscoreObjects1.length = 0;
gdjs.infosCode.GDscoreObjects2.length = 0;
gdjs.infosCode.GDileObjects1.length = 0;
gdjs.infosCode.GDileObjects2.length = 0;
gdjs.infosCode.GDile2Objects1.length = 0;
gdjs.infosCode.GDile2Objects2.length = 0;
gdjs.infosCode.GDile3Objects1.length = 0;
gdjs.infosCode.GDile3Objects2.length = 0;
gdjs.infosCode.GDile4Objects1.length = 0;
gdjs.infosCode.GDile4Objects2.length = 0;
gdjs.infosCode.GDile5Objects1.length = 0;
gdjs.infosCode.GDile5Objects2.length = 0;
gdjs.infosCode.GDile6Objects1.length = 0;
gdjs.infosCode.GDile6Objects2.length = 0;
gdjs.infosCode.GDfaux_95vraiObjects1.length = 0;
gdjs.infosCode.GDfaux_95vraiObjects2.length = 0;
gdjs.infosCode.GDfond_95blancObjects1.length = 0;
gdjs.infosCode.GDfond_95blancObjects2.length = 0;
gdjs.infosCode.GDfond_95boisObjects1.length = 0;
gdjs.infosCode.GDfond_95boisObjects2.length = 0;
gdjs.infosCode.GDvieObjects1.length = 0;
gdjs.infosCode.GDvieObjects2.length = 0;
gdjs.infosCode.GDdrapeauObjects1.length = 0;
gdjs.infosCode.GDdrapeauObjects2.length = 0;
gdjs.infosCode.GDserge_95capitaineObjects1.length = 0;
gdjs.infosCode.GDserge_95capitaineObjects2.length = 0;
gdjs.infosCode.GDaide_95sergeObjects1.length = 0;
gdjs.infosCode.GDaide_95sergeObjects2.length = 0;
gdjs.infosCode.GDvert_95abscisseObjects1.length = 0;
gdjs.infosCode.GDvert_95abscisseObjects2.length = 0;
gdjs.infosCode.GDjaune_95ordonneeObjects1.length = 0;
gdjs.infosCode.GDjaune_95ordonneeObjects2.length = 0;
gdjs.infosCode.GDpelle_95carteObjects1.length = 0;
gdjs.infosCode.GDpelle_95carteObjects2.length = 0;
gdjs.infosCode.GDbouton_95continuerObjects1.length = 0;
gdjs.infosCode.GDbouton_95continuerObjects2.length = 0;
gdjs.infosCode.GDbarreObjects1.length = 0;
gdjs.infosCode.GDbarreObjects2.length = 0;
gdjs.infosCode.GDfond_95menuObjects1.length = 0;
gdjs.infosCode.GDfond_95menuObjects2.length = 0;
gdjs.infosCode.GDinfosObjects1.length = 0;
gdjs.infosCode.GDinfosObjects2.length = 0;

gdjs.infosCode.eventsList0(runtimeScene);

return;

}

gdjs['infosCode'] = gdjs.infosCode;
