gdjs.introCode = {};
gdjs.introCode.GDintroObjects1= [];
gdjs.introCode.GDintroObjects2= [];
gdjs.introCode.GDbouton_9595verifierObjects1= [];
gdjs.introCode.GDbouton_9595verifierObjects2= [];
gdjs.introCode.GDbouton_9595retourObjects1= [];
gdjs.introCode.GDbouton_9595retourObjects2= [];
gdjs.introCode.GDMetalRedBarObjects1= [];
gdjs.introCode.GDMetalRedBarObjects2= [];


gdjs.introCode.mapOfGDgdjs_9546introCode_9546GDbouton_95959595verifierObjects1Objects = Hashtable.newFrom({"bouton_verifier": gdjs.introCode.GDbouton_9595verifierObjects1});
gdjs.introCode.eventsList0 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("bouton_verifier"), gdjs.introCode.GDbouton_9595verifierObjects1);
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "chrono");
}{for(var i = 0, len = gdjs.introCode.GDbouton_9595verifierObjects1.length ;i < len;++i) {
    gdjs.introCode.GDbouton_9595verifierObjects1[i].hide();
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "chrono") >= 3;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(16656852);
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("bouton_verifier"), gdjs.introCode.GDbouton_9595verifierObjects1);
{for(var i = 0, len = gdjs.introCode.GDbouton_9595verifierObjects1.length ;i < len;++i) {
    gdjs.introCode.GDbouton_9595verifierObjects1[i].hide(false);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_verifier"), gdjs.introCode.GDbouton_9595verifierObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.introCode.mapOfGDgdjs_9546introCode_9546GDbouton_95959595verifierObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "chrono") >= 3;
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "menu", false);
}}

}


};

gdjs.introCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.introCode.GDintroObjects1.length = 0;
gdjs.introCode.GDintroObjects2.length = 0;
gdjs.introCode.GDbouton_9595verifierObjects1.length = 0;
gdjs.introCode.GDbouton_9595verifierObjects2.length = 0;
gdjs.introCode.GDbouton_9595retourObjects1.length = 0;
gdjs.introCode.GDbouton_9595retourObjects2.length = 0;
gdjs.introCode.GDMetalRedBarObjects1.length = 0;
gdjs.introCode.GDMetalRedBarObjects2.length = 0;

gdjs.introCode.eventsList0(runtimeScene);

return;

}

gdjs['introCode'] = gdjs.introCode;
