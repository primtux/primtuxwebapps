gdjs.tirage1Code = {};
gdjs.tirage1Code.GDbouton_95recommencerObjects1= [];
gdjs.tirage1Code.GDbouton_95recommencerObjects2= [];
gdjs.tirage1Code.GDbouton_95recommencerObjects3= [];
gdjs.tirage1Code.GDbouton_95suivantObjects1= [];
gdjs.tirage1Code.GDbouton_95suivantObjects2= [];
gdjs.tirage1Code.GDbouton_95suivantObjects3= [];
gdjs.tirage1Code.GDfantome1Objects1= [];
gdjs.tirage1Code.GDfantome1Objects2= [];
gdjs.tirage1Code.GDfantome1Objects3= [];
gdjs.tirage1Code.GDscore4Objects1= [];
gdjs.tirage1Code.GDscore4Objects2= [];
gdjs.tirage1Code.GDscore4Objects3= [];
gdjs.tirage1Code.GDscore3Objects1= [];
gdjs.tirage1Code.GDscore3Objects2= [];
gdjs.tirage1Code.GDscore3Objects3= [];
gdjs.tirage1Code.GDscore2Objects1= [];
gdjs.tirage1Code.GDscore2Objects2= [];
gdjs.tirage1Code.GDscore2Objects3= [];
gdjs.tirage1Code.GDscore1Objects1= [];
gdjs.tirage1Code.GDscore1Objects2= [];
gdjs.tirage1Code.GDscore1Objects3= [];
gdjs.tirage1Code.GDbouton_95retourObjects1= [];
gdjs.tirage1Code.GDbouton_95retourObjects2= [];
gdjs.tirage1Code.GDbouton_95retourObjects3= [];
gdjs.tirage1Code.GDfantome_95bleuObjects1= [];
gdjs.tirage1Code.GDfantome_95bleuObjects2= [];
gdjs.tirage1Code.GDfantome_95bleuObjects3= [];
gdjs.tirage1Code.GDfantome_95rougeObjects1= [];
gdjs.tirage1Code.GDfantome_95rougeObjects2= [];
gdjs.tirage1Code.GDfantome_95rougeObjects3= [];
gdjs.tirage1Code.GDfantome_95jauneObjects1= [];
gdjs.tirage1Code.GDfantome_95jauneObjects2= [];
gdjs.tirage1Code.GDfantome_95jauneObjects3= [];
gdjs.tirage1Code.GDfantome_95vertObjects1= [];
gdjs.tirage1Code.GDfantome_95vertObjects2= [];
gdjs.tirage1Code.GDfantome_95vertObjects3= [];
gdjs.tirage1Code.GDfantome5Objects1= [];
gdjs.tirage1Code.GDfantome5Objects2= [];
gdjs.tirage1Code.GDfantome5Objects3= [];

gdjs.tirage1Code.conditionTrue_0 = {val:false};
gdjs.tirage1Code.condition0IsTrue_0 = {val:false};
gdjs.tirage1Code.condition1IsTrue_0 = {val:false};
gdjs.tirage1Code.condition2IsTrue_0 = {val:false};
gdjs.tirage1Code.condition3IsTrue_0 = {val:false};
gdjs.tirage1Code.condition4IsTrue_0 = {val:false};
gdjs.tirage1Code.condition5IsTrue_0 = {val:false};
gdjs.tirage1Code.condition6IsTrue_0 = {val:false};
gdjs.tirage1Code.conditionTrue_1 = {val:false};
gdjs.tirage1Code.condition0IsTrue_1 = {val:false};
gdjs.tirage1Code.condition1IsTrue_1 = {val:false};
gdjs.tirage1Code.condition2IsTrue_1 = {val:false};
gdjs.tirage1Code.condition3IsTrue_1 = {val:false};
gdjs.tirage1Code.condition4IsTrue_1 = {val:false};
gdjs.tirage1Code.condition5IsTrue_1 = {val:false};
gdjs.tirage1Code.condition6IsTrue_1 = {val:false};


gdjs.tirage1Code.eventsList0 = function(runtimeScene) {

{


gdjs.tirage1Code.condition0IsTrue_0.val = false;
gdjs.tirage1Code.condition1IsTrue_0.val = false;
{
gdjs.tirage1Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 1;
}if ( gdjs.tirage1Code.condition0IsTrue_0.val ) {
{
gdjs.tirage1Code.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a")) == 0;
}}
if (gdjs.tirage1Code.condition1IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(4).setString("a");
}{runtimeScene.getVariables().getFromIndex(0).add(1);
}}

}


{


gdjs.tirage1Code.condition0IsTrue_0.val = false;
gdjs.tirage1Code.condition1IsTrue_0.val = false;
{
gdjs.tirage1Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 2;
}if ( gdjs.tirage1Code.condition0IsTrue_0.val ) {
{
gdjs.tirage1Code.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b")) == 0;
}}
if (gdjs.tirage1Code.condition1IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(4).setString("b");
}{runtimeScene.getVariables().getFromIndex(0).add(1);
}}

}


{


gdjs.tirage1Code.condition0IsTrue_0.val = false;
gdjs.tirage1Code.condition1IsTrue_0.val = false;
{
gdjs.tirage1Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 3;
}if ( gdjs.tirage1Code.condition0IsTrue_0.val ) {
{
gdjs.tirage1Code.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("c")) == 0;
}}
if (gdjs.tirage1Code.condition1IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(4).setString("c");
}{runtimeScene.getVariables().getFromIndex(0).add(1);
}}

}


{


gdjs.tirage1Code.condition0IsTrue_0.val = false;
gdjs.tirage1Code.condition1IsTrue_0.val = false;
{
gdjs.tirage1Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 4;
}if ( gdjs.tirage1Code.condition0IsTrue_0.val ) {
{
gdjs.tirage1Code.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("d")) == 0;
}}
if (gdjs.tirage1Code.condition1IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(4).setString("d");
}{runtimeScene.getVariables().getFromIndex(0).add(1);
}}

}


{


gdjs.tirage1Code.condition0IsTrue_0.val = false;
gdjs.tirage1Code.condition1IsTrue_0.val = false;
{
gdjs.tirage1Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 5;
}if ( gdjs.tirage1Code.condition0IsTrue_0.val ) {
{
gdjs.tirage1Code.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("e")) == 0;
}}
if (gdjs.tirage1Code.condition1IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(4).setString("e");
}{runtimeScene.getVariables().getFromIndex(0).add(1);
}}

}


{


gdjs.tirage1Code.condition0IsTrue_0.val = false;
gdjs.tirage1Code.condition1IsTrue_0.val = false;
{
gdjs.tirage1Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 6;
}if ( gdjs.tirage1Code.condition0IsTrue_0.val ) {
{
gdjs.tirage1Code.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("f")) == 0;
}}
if (gdjs.tirage1Code.condition1IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(4).setString("f");
}{runtimeScene.getVariables().getFromIndex(0).add(1);
}}

}


{


gdjs.tirage1Code.condition0IsTrue_0.val = false;
gdjs.tirage1Code.condition1IsTrue_0.val = false;
{
gdjs.tirage1Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 7;
}if ( gdjs.tirage1Code.condition0IsTrue_0.val ) {
{
gdjs.tirage1Code.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("g")) == 0;
}}
if (gdjs.tirage1Code.condition1IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(4).setString("g");
}{runtimeScene.getVariables().getFromIndex(0).add(1);
}}

}


{


gdjs.tirage1Code.condition0IsTrue_0.val = false;
gdjs.tirage1Code.condition1IsTrue_0.val = false;
{
gdjs.tirage1Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 8;
}if ( gdjs.tirage1Code.condition0IsTrue_0.val ) {
{
gdjs.tirage1Code.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("h")) == 0;
}}
if (gdjs.tirage1Code.condition1IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(4).setString("h");
}{runtimeScene.getVariables().getFromIndex(0).add(1);
}}

}


{


gdjs.tirage1Code.condition0IsTrue_0.val = false;
gdjs.tirage1Code.condition1IsTrue_0.val = false;
{
gdjs.tirage1Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 9;
}if ( gdjs.tirage1Code.condition0IsTrue_0.val ) {
{
gdjs.tirage1Code.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("i")) == 0;
}}
if (gdjs.tirage1Code.condition1IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(4).setString("i");
}{runtimeScene.getVariables().getFromIndex(0).add(1);
}}

}


{


gdjs.tirage1Code.condition0IsTrue_0.val = false;
gdjs.tirage1Code.condition1IsTrue_0.val = false;
{
gdjs.tirage1Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 10;
}if ( gdjs.tirage1Code.condition0IsTrue_0.val ) {
{
gdjs.tirage1Code.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("j")) == 0;
}}
if (gdjs.tirage1Code.condition1IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(4).setString("j");
}{runtimeScene.getVariables().getFromIndex(0).add(1);
}}

}


{


gdjs.tirage1Code.condition0IsTrue_0.val = false;
gdjs.tirage1Code.condition1IsTrue_0.val = false;
{
gdjs.tirage1Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 11;
}if ( gdjs.tirage1Code.condition0IsTrue_0.val ) {
{
gdjs.tirage1Code.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("k")) == 0;
}}
if (gdjs.tirage1Code.condition1IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(4).setString("k");
}{runtimeScene.getVariables().getFromIndex(0).add(1);
}}

}


{


gdjs.tirage1Code.condition0IsTrue_0.val = false;
gdjs.tirage1Code.condition1IsTrue_0.val = false;
{
gdjs.tirage1Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 12;
}if ( gdjs.tirage1Code.condition0IsTrue_0.val ) {
{
gdjs.tirage1Code.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("l")) == 0;
}}
if (gdjs.tirage1Code.condition1IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(4).setString("l");
}{runtimeScene.getVariables().getFromIndex(0).add(1);
}}

}


{


gdjs.tirage1Code.condition0IsTrue_0.val = false;
gdjs.tirage1Code.condition1IsTrue_0.val = false;
{
gdjs.tirage1Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 13;
}if ( gdjs.tirage1Code.condition0IsTrue_0.val ) {
{
gdjs.tirage1Code.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("m")) == 0;
}}
if (gdjs.tirage1Code.condition1IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(4).setString("m");
}{runtimeScene.getVariables().getFromIndex(0).add(1);
}}

}


{


gdjs.tirage1Code.condition0IsTrue_0.val = false;
gdjs.tirage1Code.condition1IsTrue_0.val = false;
{
gdjs.tirage1Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 14;
}if ( gdjs.tirage1Code.condition0IsTrue_0.val ) {
{
gdjs.tirage1Code.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("n")) == 0;
}}
if (gdjs.tirage1Code.condition1IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(4).setString("n");
}{runtimeScene.getVariables().getFromIndex(0).add(1);
}}

}


{


gdjs.tirage1Code.condition0IsTrue_0.val = false;
gdjs.tirage1Code.condition1IsTrue_0.val = false;
{
gdjs.tirage1Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 15;
}if ( gdjs.tirage1Code.condition0IsTrue_0.val ) {
{
gdjs.tirage1Code.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("o")) == 0;
}}
if (gdjs.tirage1Code.condition1IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(4).setString("o");
}{runtimeScene.getVariables().getFromIndex(0).add(1);
}}

}


{


gdjs.tirage1Code.condition0IsTrue_0.val = false;
gdjs.tirage1Code.condition1IsTrue_0.val = false;
{
gdjs.tirage1Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 16;
}if ( gdjs.tirage1Code.condition0IsTrue_0.val ) {
{
gdjs.tirage1Code.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("p")) == 0;
}}
if (gdjs.tirage1Code.condition1IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(4).setString("p");
}{runtimeScene.getVariables().getFromIndex(0).add(1);
}}

}


{


gdjs.tirage1Code.condition0IsTrue_0.val = false;
gdjs.tirage1Code.condition1IsTrue_0.val = false;
{
gdjs.tirage1Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 17;
}if ( gdjs.tirage1Code.condition0IsTrue_0.val ) {
{
gdjs.tirage1Code.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("q")) == 0;
}}
if (gdjs.tirage1Code.condition1IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(4).setString("q");
}{runtimeScene.getVariables().getFromIndex(0).add(1);
}}

}


{


gdjs.tirage1Code.condition0IsTrue_0.val = false;
gdjs.tirage1Code.condition1IsTrue_0.val = false;
{
gdjs.tirage1Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 18;
}if ( gdjs.tirage1Code.condition0IsTrue_0.val ) {
{
gdjs.tirage1Code.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("r")) == 0;
}}
if (gdjs.tirage1Code.condition1IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(4).setString("r");
}{runtimeScene.getVariables().getFromIndex(0).add(1);
}}

}


{


gdjs.tirage1Code.condition0IsTrue_0.val = false;
gdjs.tirage1Code.condition1IsTrue_0.val = false;
{
gdjs.tirage1Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 19;
}if ( gdjs.tirage1Code.condition0IsTrue_0.val ) {
{
gdjs.tirage1Code.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("s")) == 0;
}}
if (gdjs.tirage1Code.condition1IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(4).setString("s");
}{runtimeScene.getVariables().getFromIndex(0).add(1);
}}

}


{


gdjs.tirage1Code.condition0IsTrue_0.val = false;
gdjs.tirage1Code.condition1IsTrue_0.val = false;
{
gdjs.tirage1Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 20;
}if ( gdjs.tirage1Code.condition0IsTrue_0.val ) {
{
gdjs.tirage1Code.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("t")) == 0;
}}
if (gdjs.tirage1Code.condition1IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(4).setString("t");
}{runtimeScene.getVariables().getFromIndex(0).add(1);
}}

}


{


gdjs.tirage1Code.condition0IsTrue_0.val = false;
gdjs.tirage1Code.condition1IsTrue_0.val = false;
{
gdjs.tirage1Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 21;
}if ( gdjs.tirage1Code.condition0IsTrue_0.val ) {
{
gdjs.tirage1Code.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("u")) == 0;
}}
if (gdjs.tirage1Code.condition1IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(4).setString("u");
}{runtimeScene.getVariables().getFromIndex(0).add(1);
}}

}


{


gdjs.tirage1Code.condition0IsTrue_0.val = false;
gdjs.tirage1Code.condition1IsTrue_0.val = false;
{
gdjs.tirage1Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 22;
}if ( gdjs.tirage1Code.condition0IsTrue_0.val ) {
{
gdjs.tirage1Code.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("v")) == 0;
}}
if (gdjs.tirage1Code.condition1IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(4).setString("v");
}{runtimeScene.getVariables().getFromIndex(0).add(1);
}}

}


{


gdjs.tirage1Code.condition0IsTrue_0.val = false;
gdjs.tirage1Code.condition1IsTrue_0.val = false;
{
gdjs.tirage1Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 23;
}if ( gdjs.tirage1Code.condition0IsTrue_0.val ) {
{
gdjs.tirage1Code.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("w")) == 0;
}}
if (gdjs.tirage1Code.condition1IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(4).setString("w");
}{runtimeScene.getVariables().getFromIndex(0).add(1);
}}

}


{


gdjs.tirage1Code.condition0IsTrue_0.val = false;
gdjs.tirage1Code.condition1IsTrue_0.val = false;
{
gdjs.tirage1Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 24;
}if ( gdjs.tirage1Code.condition0IsTrue_0.val ) {
{
gdjs.tirage1Code.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("x")) == 0;
}}
if (gdjs.tirage1Code.condition1IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(4).setString("x");
}{runtimeScene.getVariables().getFromIndex(0).add(1);
}}

}


{


gdjs.tirage1Code.condition0IsTrue_0.val = false;
gdjs.tirage1Code.condition1IsTrue_0.val = false;
{
gdjs.tirage1Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 25;
}if ( gdjs.tirage1Code.condition0IsTrue_0.val ) {
{
gdjs.tirage1Code.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("y")) == 0;
}}
if (gdjs.tirage1Code.condition1IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(4).setString("y");
}{runtimeScene.getVariables().getFromIndex(0).add(1);
}}

}


{


gdjs.tirage1Code.condition0IsTrue_0.val = false;
gdjs.tirage1Code.condition1IsTrue_0.val = false;
{
gdjs.tirage1Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 26;
}if ( gdjs.tirage1Code.condition0IsTrue_0.val ) {
{
gdjs.tirage1Code.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("z")) == 0;
}}
if (gdjs.tirage1Code.condition1IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(4).setString("z");
}{runtimeScene.getVariables().getFromIndex(0).add(1);
}}

}


};gdjs.tirage1Code.eventsList1 = function(runtimeScene) {

{


gdjs.tirage1Code.condition0IsTrue_0.val = false;
{
gdjs.tirage1Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 1;
}if (gdjs.tirage1Code.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(4).setString("m");
}{runtimeScene.getVariables().getFromIndex(0).add(1);
}}

}


{


gdjs.tirage1Code.condition0IsTrue_0.val = false;
{
gdjs.tirage1Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 2;
}if (gdjs.tirage1Code.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(4).setString("n");
}{runtimeScene.getVariables().getFromIndex(0).add(1);
}}

}


{


gdjs.tirage1Code.condition0IsTrue_0.val = false;
{
gdjs.tirage1Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 3;
}if (gdjs.tirage1Code.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(4).setString("v");
}{runtimeScene.getVariables().getFromIndex(0).add(1);
}}

}


{


gdjs.tirage1Code.condition0IsTrue_0.val = false;
{
gdjs.tirage1Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 4;
}if (gdjs.tirage1Code.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(4).setString("w");
}{runtimeScene.getVariables().getFromIndex(0).add(1);
}}

}


{


gdjs.tirage1Code.condition0IsTrue_0.val = false;
{
gdjs.tirage1Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 5;
}if (gdjs.tirage1Code.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(4).setString("u");
}{runtimeScene.getVariables().getFromIndex(0).add(1);
}}

}


};gdjs.tirage1Code.eventsList2 = function(runtimeScene) {

{


gdjs.tirage1Code.condition0IsTrue_0.val = false;
{
gdjs.tirage1Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 1;
}if (gdjs.tirage1Code.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(4).setString("a");
}{runtimeScene.getVariables().getFromIndex(0).add(1);
}}

}


{


gdjs.tirage1Code.condition0IsTrue_0.val = false;
{
gdjs.tirage1Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 2;
}if (gdjs.tirage1Code.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(4).setString("c");
}{runtimeScene.getVariables().getFromIndex(0).add(1);
}}

}


{


gdjs.tirage1Code.condition0IsTrue_0.val = false;
{
gdjs.tirage1Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 3;
}if (gdjs.tirage1Code.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(4).setString("e");
}{runtimeScene.getVariables().getFromIndex(0).add(1);
}}

}


{


gdjs.tirage1Code.condition0IsTrue_0.val = false;
{
gdjs.tirage1Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 4;
}if (gdjs.tirage1Code.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(4).setString("o");
}{runtimeScene.getVariables().getFromIndex(0).add(1);
}}

}


{


gdjs.tirage1Code.condition0IsTrue_0.val = false;
{
gdjs.tirage1Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 5;
}if (gdjs.tirage1Code.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(4).setString("u");
}{runtimeScene.getVariables().getFromIndex(0).add(1);
}}

}


};gdjs.tirage1Code.eventsList3 = function(runtimeScene) {

{


gdjs.tirage1Code.condition0IsTrue_0.val = false;
{
gdjs.tirage1Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 1;
}if (gdjs.tirage1Code.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(4).setString("b");
}{runtimeScene.getVariables().getFromIndex(0).add(1);
}}

}


{


gdjs.tirage1Code.condition0IsTrue_0.val = false;
{
gdjs.tirage1Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 2;
}if (gdjs.tirage1Code.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(4).setString("d");
}{runtimeScene.getVariables().getFromIndex(0).add(1);
}}

}


{


gdjs.tirage1Code.condition0IsTrue_0.val = false;
{
gdjs.tirage1Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 3;
}if (gdjs.tirage1Code.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(4).setString("p");
}{runtimeScene.getVariables().getFromIndex(0).add(1);
}}

}


{


gdjs.tirage1Code.condition0IsTrue_0.val = false;
{
gdjs.tirage1Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 4;
}if (gdjs.tirage1Code.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(4).setString("q");
}{runtimeScene.getVariables().getFromIndex(0).add(1);
}}

}


{


gdjs.tirage1Code.condition0IsTrue_0.val = false;
{
gdjs.tirage1Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 5;
}if (gdjs.tirage1Code.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(4).setString("g");
}{runtimeScene.getVariables().getFromIndex(0).add(1);
}}

}


};gdjs.tirage1Code.eventsList4 = function(runtimeScene) {

{


gdjs.tirage1Code.condition0IsTrue_0.val = false;
{
gdjs.tirage1Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(4)) != gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(4).getChild("1"));
}if (gdjs.tirage1Code.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(4).getChild("2").setString(gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(4)));
}{runtimeScene.getVariables().getFromIndex(0).setNumber(5);
}}

}


{


gdjs.tirage1Code.condition0IsTrue_0.val = false;
{
{gdjs.tirage1Code.conditionTrue_1 = gdjs.tirage1Code.condition0IsTrue_0;
gdjs.tirage1Code.condition0IsTrue_1.val = false;
{
gdjs.tirage1Code.condition0IsTrue_1.val = gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(4)) == gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(4).getChild("1"));
if( gdjs.tirage1Code.condition0IsTrue_1.val ) {
    gdjs.tirage1Code.conditionTrue_1.val = true;
}
}
{
}
}
}if (gdjs.tirage1Code.condition0IsTrue_0.val) {
{runtimeScene.getVariables().getFromIndex(0).setNumber(3);
}}

}


};gdjs.tirage1Code.eventsList5 = function(runtimeScene) {

{


gdjs.tirage1Code.condition0IsTrue_0.val = false;
gdjs.tirage1Code.condition1IsTrue_0.val = false;
{
gdjs.tirage1Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(4)) != gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(4).getChild("1"));
}if ( gdjs.tirage1Code.condition0IsTrue_0.val ) {
{
gdjs.tirage1Code.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(4)) != gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(4).getChild("2"));
}}
if (gdjs.tirage1Code.condition1IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(4).getChild("3").setString(gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(4)));
}{runtimeScene.getVariables().getFromIndex(0).setNumber(7);
}}

}


{


gdjs.tirage1Code.condition0IsTrue_0.val = false;
{
{gdjs.tirage1Code.conditionTrue_1 = gdjs.tirage1Code.condition0IsTrue_0;
gdjs.tirage1Code.condition0IsTrue_1.val = false;
gdjs.tirage1Code.condition1IsTrue_1.val = false;
{
gdjs.tirage1Code.condition0IsTrue_1.val = gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(4)) == gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(4).getChild("1"));
if( gdjs.tirage1Code.condition0IsTrue_1.val ) {
    gdjs.tirage1Code.conditionTrue_1.val = true;
}
}
{
gdjs.tirage1Code.condition1IsTrue_1.val = gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(4)) == gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(4).getChild("2"));
if( gdjs.tirage1Code.condition1IsTrue_1.val ) {
    gdjs.tirage1Code.conditionTrue_1.val = true;
}
}
{
}
}
}if (gdjs.tirage1Code.condition0IsTrue_0.val) {
{runtimeScene.getVariables().getFromIndex(0).setNumber(5);
}}

}


};gdjs.tirage1Code.eventsList6 = function(runtimeScene) {

{


gdjs.tirage1Code.condition0IsTrue_0.val = false;
gdjs.tirage1Code.condition1IsTrue_0.val = false;
gdjs.tirage1Code.condition2IsTrue_0.val = false;
{
gdjs.tirage1Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(4)) != gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(4).getChild("1"));
}if ( gdjs.tirage1Code.condition0IsTrue_0.val ) {
{
gdjs.tirage1Code.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(4)) != gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(4).getChild("2"));
}if ( gdjs.tirage1Code.condition1IsTrue_0.val ) {
{
gdjs.tirage1Code.condition2IsTrue_0.val = gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(4)) != gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(4).getChild("3"));
}}
}
if (gdjs.tirage1Code.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(4).getChild("4").setString(gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(4)));
}{runtimeScene.getVariables().getFromIndex(0).setNumber(9);
}}

}


{


gdjs.tirage1Code.condition0IsTrue_0.val = false;
{
{gdjs.tirage1Code.conditionTrue_1 = gdjs.tirage1Code.condition0IsTrue_0;
gdjs.tirage1Code.condition0IsTrue_1.val = false;
gdjs.tirage1Code.condition1IsTrue_1.val = false;
gdjs.tirage1Code.condition2IsTrue_1.val = false;
{
gdjs.tirage1Code.condition0IsTrue_1.val = gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(4)) == gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(4).getChild("1"));
if( gdjs.tirage1Code.condition0IsTrue_1.val ) {
    gdjs.tirage1Code.conditionTrue_1.val = true;
}
}
{
gdjs.tirage1Code.condition1IsTrue_1.val = gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(4)) == gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(4).getChild("2"));
if( gdjs.tirage1Code.condition1IsTrue_1.val ) {
    gdjs.tirage1Code.conditionTrue_1.val = true;
}
}
{
gdjs.tirage1Code.condition2IsTrue_1.val = gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(4)) == gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(4).getChild("3"));
if( gdjs.tirage1Code.condition2IsTrue_1.val ) {
    gdjs.tirage1Code.conditionTrue_1.val = true;
}
}
{
}
}
}if (gdjs.tirage1Code.condition0IsTrue_0.val) {
{runtimeScene.getVariables().getFromIndex(0).setNumber(7);
}}

}


};gdjs.tirage1Code.eventsList7 = function(runtimeScene) {

{


gdjs.tirage1Code.condition0IsTrue_0.val = false;
gdjs.tirage1Code.condition1IsTrue_0.val = false;
gdjs.tirage1Code.condition2IsTrue_0.val = false;
gdjs.tirage1Code.condition3IsTrue_0.val = false;
{
gdjs.tirage1Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(4)) != gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(4).getChild("1"));
}if ( gdjs.tirage1Code.condition0IsTrue_0.val ) {
{
gdjs.tirage1Code.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(4)) != gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(4).getChild("2"));
}if ( gdjs.tirage1Code.condition1IsTrue_0.val ) {
{
gdjs.tirage1Code.condition2IsTrue_0.val = gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(4)) != gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(4).getChild("3"));
}if ( gdjs.tirage1Code.condition2IsTrue_0.val ) {
{
gdjs.tirage1Code.condition3IsTrue_0.val = gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(4)) != gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(4).getChild("4"));
}}
}
}
if (gdjs.tirage1Code.condition3IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(4).getChild("5").setString(gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(4)));
}{runtimeScene.getVariables().getFromIndex(0).setNumber(11);
}}

}


{


gdjs.tirage1Code.condition0IsTrue_0.val = false;
{
{gdjs.tirage1Code.conditionTrue_1 = gdjs.tirage1Code.condition0IsTrue_0;
gdjs.tirage1Code.condition0IsTrue_1.val = false;
gdjs.tirage1Code.condition1IsTrue_1.val = false;
gdjs.tirage1Code.condition2IsTrue_1.val = false;
gdjs.tirage1Code.condition3IsTrue_1.val = false;
{
gdjs.tirage1Code.condition0IsTrue_1.val = gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(4)) == gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(4).getChild("1"));
if( gdjs.tirage1Code.condition0IsTrue_1.val ) {
    gdjs.tirage1Code.conditionTrue_1.val = true;
}
}
{
gdjs.tirage1Code.condition1IsTrue_1.val = gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(4)) == gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(4).getChild("2"));
if( gdjs.tirage1Code.condition1IsTrue_1.val ) {
    gdjs.tirage1Code.conditionTrue_1.val = true;
}
}
{
gdjs.tirage1Code.condition2IsTrue_1.val = gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(4)) == gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(4).getChild("3"));
if( gdjs.tirage1Code.condition2IsTrue_1.val ) {
    gdjs.tirage1Code.conditionTrue_1.val = true;
}
}
{
gdjs.tirage1Code.condition3IsTrue_1.val = gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(4)) == gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(4).getChild("4"));
if( gdjs.tirage1Code.condition3IsTrue_1.val ) {
    gdjs.tirage1Code.conditionTrue_1.val = true;
}
}
{
}
}
}if (gdjs.tirage1Code.condition0IsTrue_0.val) {
{runtimeScene.getVariables().getFromIndex(0).setNumber(9);
}}

}


};gdjs.tirage1Code.mapOfGDgdjs_46tirage1Code_46GDbouton_9595recommencerObjects1Objects = Hashtable.newFrom({"bouton_recommencer": gdjs.tirage1Code.GDbouton_95recommencerObjects1});gdjs.tirage1Code.mapOfGDgdjs_46tirage1Code_46GDbouton_9595suivantObjects1Objects = Hashtable.newFrom({"bouton_suivant": gdjs.tirage1Code.GDbouton_95suivantObjects1});gdjs.tirage1Code.eventsList8 = function(runtimeScene) {

{


gdjs.tirage1Code.condition0IsTrue_0.val = false;
{
gdjs.tirage1Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.tirage1Code.condition0IsTrue_0.val) {
{runtimeScene.getVariables().getFromIndex(0).setNumber(1);
}{runtimeScene.getGame().getVariables().getFromIndex(7).setNumber(1);
}}

}


{



}


{


gdjs.tirage1Code.condition0IsTrue_0.val = false;
gdjs.tirage1Code.condition1IsTrue_0.val = false;
{
gdjs.tirage1Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(5).getChild("selection")) == 1;
}if ( gdjs.tirage1Code.condition0IsTrue_0.val ) {
{
{gdjs.tirage1Code.conditionTrue_1 = gdjs.tirage1Code.condition1IsTrue_0;
gdjs.tirage1Code.condition0IsTrue_1.val = false;
gdjs.tirage1Code.condition1IsTrue_1.val = false;
gdjs.tirage1Code.condition2IsTrue_1.val = false;
gdjs.tirage1Code.condition3IsTrue_1.val = false;
gdjs.tirage1Code.condition4IsTrue_1.val = false;
gdjs.tirage1Code.condition5IsTrue_1.val = false;
{
gdjs.tirage1Code.condition0IsTrue_1.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) == 1;
if( gdjs.tirage1Code.condition0IsTrue_1.val ) {
    gdjs.tirage1Code.conditionTrue_1.val = true;
}
}
{
gdjs.tirage1Code.condition1IsTrue_1.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) == 3;
if( gdjs.tirage1Code.condition1IsTrue_1.val ) {
    gdjs.tirage1Code.conditionTrue_1.val = true;
}
}
{
gdjs.tirage1Code.condition2IsTrue_1.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) == 5;
if( gdjs.tirage1Code.condition2IsTrue_1.val ) {
    gdjs.tirage1Code.conditionTrue_1.val = true;
}
}
{
gdjs.tirage1Code.condition3IsTrue_1.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) == 7;
if( gdjs.tirage1Code.condition3IsTrue_1.val ) {
    gdjs.tirage1Code.conditionTrue_1.val = true;
}
}
{
gdjs.tirage1Code.condition4IsTrue_1.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) == 7;
if( gdjs.tirage1Code.condition4IsTrue_1.val ) {
    gdjs.tirage1Code.conditionTrue_1.val = true;
}
}
{
gdjs.tirage1Code.condition5IsTrue_1.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) == 9;
if( gdjs.tirage1Code.condition5IsTrue_1.val ) {
    gdjs.tirage1Code.conditionTrue_1.val = true;
}
}
{
}
}
}}
if (gdjs.tirage1Code.condition1IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(gdjs.randomInRange(1, 26));
}
{ //Subevents
gdjs.tirage1Code.eventsList0(runtimeScene);} //End of subevents
}

}


{



}


{


gdjs.tirage1Code.condition0IsTrue_0.val = false;
gdjs.tirage1Code.condition1IsTrue_0.val = false;
{
gdjs.tirage1Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(5).getChild("selection")) == 2;
}if ( gdjs.tirage1Code.condition0IsTrue_0.val ) {
{
{gdjs.tirage1Code.conditionTrue_1 = gdjs.tirage1Code.condition1IsTrue_0;
gdjs.tirage1Code.condition0IsTrue_1.val = false;
gdjs.tirage1Code.condition1IsTrue_1.val = false;
gdjs.tirage1Code.condition2IsTrue_1.val = false;
gdjs.tirage1Code.condition3IsTrue_1.val = false;
gdjs.tirage1Code.condition4IsTrue_1.val = false;
gdjs.tirage1Code.condition5IsTrue_1.val = false;
{
gdjs.tirage1Code.condition0IsTrue_1.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) == 1;
if( gdjs.tirage1Code.condition0IsTrue_1.val ) {
    gdjs.tirage1Code.conditionTrue_1.val = true;
}
}
{
gdjs.tirage1Code.condition1IsTrue_1.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) == 3;
if( gdjs.tirage1Code.condition1IsTrue_1.val ) {
    gdjs.tirage1Code.conditionTrue_1.val = true;
}
}
{
gdjs.tirage1Code.condition2IsTrue_1.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) == 5;
if( gdjs.tirage1Code.condition2IsTrue_1.val ) {
    gdjs.tirage1Code.conditionTrue_1.val = true;
}
}
{
gdjs.tirage1Code.condition3IsTrue_1.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) == 7;
if( gdjs.tirage1Code.condition3IsTrue_1.val ) {
    gdjs.tirage1Code.conditionTrue_1.val = true;
}
}
{
gdjs.tirage1Code.condition4IsTrue_1.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) == 7;
if( gdjs.tirage1Code.condition4IsTrue_1.val ) {
    gdjs.tirage1Code.conditionTrue_1.val = true;
}
}
{
gdjs.tirage1Code.condition5IsTrue_1.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) == 9;
if( gdjs.tirage1Code.condition5IsTrue_1.val ) {
    gdjs.tirage1Code.conditionTrue_1.val = true;
}
}
{
}
}
}}
if (gdjs.tirage1Code.condition1IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(gdjs.randomInRange(1, 5));
}
{ //Subevents
gdjs.tirage1Code.eventsList1(runtimeScene);} //End of subevents
}

}


{



}


{


gdjs.tirage1Code.condition0IsTrue_0.val = false;
gdjs.tirage1Code.condition1IsTrue_0.val = false;
{
gdjs.tirage1Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(5).getChild("selection")) == 3;
}if ( gdjs.tirage1Code.condition0IsTrue_0.val ) {
{
{gdjs.tirage1Code.conditionTrue_1 = gdjs.tirage1Code.condition1IsTrue_0;
gdjs.tirage1Code.condition0IsTrue_1.val = false;
gdjs.tirage1Code.condition1IsTrue_1.val = false;
gdjs.tirage1Code.condition2IsTrue_1.val = false;
gdjs.tirage1Code.condition3IsTrue_1.val = false;
gdjs.tirage1Code.condition4IsTrue_1.val = false;
gdjs.tirage1Code.condition5IsTrue_1.val = false;
{
gdjs.tirage1Code.condition0IsTrue_1.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) == 1;
if( gdjs.tirage1Code.condition0IsTrue_1.val ) {
    gdjs.tirage1Code.conditionTrue_1.val = true;
}
}
{
gdjs.tirage1Code.condition1IsTrue_1.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) == 3;
if( gdjs.tirage1Code.condition1IsTrue_1.val ) {
    gdjs.tirage1Code.conditionTrue_1.val = true;
}
}
{
gdjs.tirage1Code.condition2IsTrue_1.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) == 5;
if( gdjs.tirage1Code.condition2IsTrue_1.val ) {
    gdjs.tirage1Code.conditionTrue_1.val = true;
}
}
{
gdjs.tirage1Code.condition3IsTrue_1.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) == 7;
if( gdjs.tirage1Code.condition3IsTrue_1.val ) {
    gdjs.tirage1Code.conditionTrue_1.val = true;
}
}
{
gdjs.tirage1Code.condition4IsTrue_1.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) == 7;
if( gdjs.tirage1Code.condition4IsTrue_1.val ) {
    gdjs.tirage1Code.conditionTrue_1.val = true;
}
}
{
gdjs.tirage1Code.condition5IsTrue_1.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) == 9;
if( gdjs.tirage1Code.condition5IsTrue_1.val ) {
    gdjs.tirage1Code.conditionTrue_1.val = true;
}
}
{
}
}
}}
if (gdjs.tirage1Code.condition1IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(gdjs.randomInRange(1, 5));
}
{ //Subevents
gdjs.tirage1Code.eventsList2(runtimeScene);} //End of subevents
}

}


{



}


{


gdjs.tirage1Code.condition0IsTrue_0.val = false;
gdjs.tirage1Code.condition1IsTrue_0.val = false;
{
gdjs.tirage1Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(5).getChild("selection")) == 4;
}if ( gdjs.tirage1Code.condition0IsTrue_0.val ) {
{
{gdjs.tirage1Code.conditionTrue_1 = gdjs.tirage1Code.condition1IsTrue_0;
gdjs.tirage1Code.condition0IsTrue_1.val = false;
gdjs.tirage1Code.condition1IsTrue_1.val = false;
gdjs.tirage1Code.condition2IsTrue_1.val = false;
gdjs.tirage1Code.condition3IsTrue_1.val = false;
gdjs.tirage1Code.condition4IsTrue_1.val = false;
gdjs.tirage1Code.condition5IsTrue_1.val = false;
{
gdjs.tirage1Code.condition0IsTrue_1.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) == 1;
if( gdjs.tirage1Code.condition0IsTrue_1.val ) {
    gdjs.tirage1Code.conditionTrue_1.val = true;
}
}
{
gdjs.tirage1Code.condition1IsTrue_1.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) == 3;
if( gdjs.tirage1Code.condition1IsTrue_1.val ) {
    gdjs.tirage1Code.conditionTrue_1.val = true;
}
}
{
gdjs.tirage1Code.condition2IsTrue_1.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) == 5;
if( gdjs.tirage1Code.condition2IsTrue_1.val ) {
    gdjs.tirage1Code.conditionTrue_1.val = true;
}
}
{
gdjs.tirage1Code.condition3IsTrue_1.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) == 7;
if( gdjs.tirage1Code.condition3IsTrue_1.val ) {
    gdjs.tirage1Code.conditionTrue_1.val = true;
}
}
{
gdjs.tirage1Code.condition4IsTrue_1.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) == 7;
if( gdjs.tirage1Code.condition4IsTrue_1.val ) {
    gdjs.tirage1Code.conditionTrue_1.val = true;
}
}
{
gdjs.tirage1Code.condition5IsTrue_1.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) == 9;
if( gdjs.tirage1Code.condition5IsTrue_1.val ) {
    gdjs.tirage1Code.conditionTrue_1.val = true;
}
}
{
}
}
}}
if (gdjs.tirage1Code.condition1IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(gdjs.randomInRange(1, 5));
}
{ //Subevents
gdjs.tirage1Code.eventsList3(runtimeScene);} //End of subevents
}

}


{


gdjs.tirage1Code.condition0IsTrue_0.val = false;
{
gdjs.tirage1Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) == 2;
}if (gdjs.tirage1Code.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(4).getChild("1").setString(gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(4)));
}{runtimeScene.getVariables().getFromIndex(0).setNumber(3);
}}

}


{


gdjs.tirage1Code.condition0IsTrue_0.val = false;
{
gdjs.tirage1Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) == 4;
}if (gdjs.tirage1Code.condition0IsTrue_0.val) {

{ //Subevents
gdjs.tirage1Code.eventsList4(runtimeScene);} //End of subevents
}

}


{


gdjs.tirage1Code.condition0IsTrue_0.val = false;
{
gdjs.tirage1Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) == 6;
}if (gdjs.tirage1Code.condition0IsTrue_0.val) {

{ //Subevents
gdjs.tirage1Code.eventsList5(runtimeScene);} //End of subevents
}

}


{


gdjs.tirage1Code.condition0IsTrue_0.val = false;
{
gdjs.tirage1Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) == 8;
}if (gdjs.tirage1Code.condition0IsTrue_0.val) {

{ //Subevents
gdjs.tirage1Code.eventsList6(runtimeScene);} //End of subevents
}

}


{


gdjs.tirage1Code.condition0IsTrue_0.val = false;
{
gdjs.tirage1Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) == 10;
}if (gdjs.tirage1Code.condition0IsTrue_0.val) {

{ //Subevents
gdjs.tirage1Code.eventsList7(runtimeScene);} //End of subevents
}

}


{


gdjs.tirage1Code.condition0IsTrue_0.val = false;
{
gdjs.tirage1Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) == 11;
}if (gdjs.tirage1Code.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "jeu1", true);
}}

}


{



}


{


gdjs.tirage1Code.condition0IsTrue_0.val = false;
{
gdjs.tirage1Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) == 30;
}if (gdjs.tirage1Code.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "jeu1", true);
}}

}


{

gdjs.tirage1Code.GDbouton_95recommencerObjects1.createFrom(runtimeScene.getObjects("bouton_recommencer"));

gdjs.tirage1Code.condition0IsTrue_0.val = false;
gdjs.tirage1Code.condition1IsTrue_0.val = false;
{
gdjs.tirage1Code.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.tirage1Code.condition0IsTrue_0.val ) {
{
gdjs.tirage1Code.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.tirage1Code.mapOfGDgdjs_46tirage1Code_46GDbouton_9595recommencerObjects1Objects, runtimeScene, true, false);
}}
if (gdjs.tirage1Code.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "tirage1", true);
}}

}


{

gdjs.tirage1Code.GDbouton_95suivantObjects1.createFrom(runtimeScene.getObjects("bouton_suivant"));

gdjs.tirage1Code.condition0IsTrue_0.val = false;
gdjs.tirage1Code.condition1IsTrue_0.val = false;
{
gdjs.tirage1Code.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.tirage1Code.condition0IsTrue_0.val ) {
{
gdjs.tirage1Code.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.tirage1Code.mapOfGDgdjs_46tirage1Code_46GDbouton_9595suivantObjects1Objects, runtimeScene, true, false);
}}
if (gdjs.tirage1Code.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "jeu1", true);
}}

}


};

gdjs.tirage1Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.tirage1Code.GDbouton_95recommencerObjects1.length = 0;
gdjs.tirage1Code.GDbouton_95recommencerObjects2.length = 0;
gdjs.tirage1Code.GDbouton_95recommencerObjects3.length = 0;
gdjs.tirage1Code.GDbouton_95suivantObjects1.length = 0;
gdjs.tirage1Code.GDbouton_95suivantObjects2.length = 0;
gdjs.tirage1Code.GDbouton_95suivantObjects3.length = 0;
gdjs.tirage1Code.GDfantome1Objects1.length = 0;
gdjs.tirage1Code.GDfantome1Objects2.length = 0;
gdjs.tirage1Code.GDfantome1Objects3.length = 0;
gdjs.tirage1Code.GDscore4Objects1.length = 0;
gdjs.tirage1Code.GDscore4Objects2.length = 0;
gdjs.tirage1Code.GDscore4Objects3.length = 0;
gdjs.tirage1Code.GDscore3Objects1.length = 0;
gdjs.tirage1Code.GDscore3Objects2.length = 0;
gdjs.tirage1Code.GDscore3Objects3.length = 0;
gdjs.tirage1Code.GDscore2Objects1.length = 0;
gdjs.tirage1Code.GDscore2Objects2.length = 0;
gdjs.tirage1Code.GDscore2Objects3.length = 0;
gdjs.tirage1Code.GDscore1Objects1.length = 0;
gdjs.tirage1Code.GDscore1Objects2.length = 0;
gdjs.tirage1Code.GDscore1Objects3.length = 0;
gdjs.tirage1Code.GDbouton_95retourObjects1.length = 0;
gdjs.tirage1Code.GDbouton_95retourObjects2.length = 0;
gdjs.tirage1Code.GDbouton_95retourObjects3.length = 0;
gdjs.tirage1Code.GDfantome_95bleuObjects1.length = 0;
gdjs.tirage1Code.GDfantome_95bleuObjects2.length = 0;
gdjs.tirage1Code.GDfantome_95bleuObjects3.length = 0;
gdjs.tirage1Code.GDfantome_95rougeObjects1.length = 0;
gdjs.tirage1Code.GDfantome_95rougeObjects2.length = 0;
gdjs.tirage1Code.GDfantome_95rougeObjects3.length = 0;
gdjs.tirage1Code.GDfantome_95jauneObjects1.length = 0;
gdjs.tirage1Code.GDfantome_95jauneObjects2.length = 0;
gdjs.tirage1Code.GDfantome_95jauneObjects3.length = 0;
gdjs.tirage1Code.GDfantome_95vertObjects1.length = 0;
gdjs.tirage1Code.GDfantome_95vertObjects2.length = 0;
gdjs.tirage1Code.GDfantome_95vertObjects3.length = 0;
gdjs.tirage1Code.GDfantome5Objects1.length = 0;
gdjs.tirage1Code.GDfantome5Objects2.length = 0;
gdjs.tirage1Code.GDfantome5Objects3.length = 0;

gdjs.tirage1Code.eventsList8(runtimeScene);
return;

}

gdjs['tirage1Code'] = gdjs.tirage1Code;
