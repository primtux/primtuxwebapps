import toml


class App:
    template = 'index.html'

    def __init__(self, app_name, toml_entry):
        self.name = app_name
        if 'path' in toml_entry:
            self.name = toml_entry['path']
        if 'template' not in toml_entry:
            return
        self.template = toml_entry['template']


def apps():
    with open('apps.toml', 'r') as f:
        allowed_apps = toml.loads(f.read())

    for app in allowed_apps['app']:
        yield App(app, allowed_apps['app'][app])
