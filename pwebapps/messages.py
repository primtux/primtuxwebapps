from stringcolor import cs


def success(message):
    print(
        cs("Succès : ", "white", "green"),
        message
    )


def error(message):
    print(
        cs("Erreur : ", "yellow", "red"),
        message
    )


def warning(message):
    print(
        cs("Alerte : ", "white", "orange"),
        message
    )
