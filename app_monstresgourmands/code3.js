gdjs.tirageCode = {};
gdjs.tirageCode.GDscore_951Objects1= [];
gdjs.tirageCode.GDscore_951Objects2= [];
gdjs.tirageCode.GDscore_951Objects3= [];
gdjs.tirageCode.GDscore_952Objects1= [];
gdjs.tirageCode.GDscore_952Objects2= [];
gdjs.tirageCode.GDscore_952Objects3= [];
gdjs.tirageCode.GDscore_953Objects1= [];
gdjs.tirageCode.GDscore_953Objects2= [];
gdjs.tirageCode.GDscore_953Objects3= [];
gdjs.tirageCode.GDmonstreObjects1= [];
gdjs.tirageCode.GDmonstreObjects2= [];
gdjs.tirageCode.GDmonstreObjects3= [];
gdjs.tirageCode.GDpanierObjects1= [];
gdjs.tirageCode.GDpanierObjects2= [];
gdjs.tirageCode.GDpanierObjects3= [];
gdjs.tirageCode.GDbouton_95recommencerObjects1= [];
gdjs.tirageCode.GDbouton_95recommencerObjects2= [];
gdjs.tirageCode.GDbouton_95recommencerObjects3= [];
gdjs.tirageCode.GDmonstre3Objects1= [];
gdjs.tirageCode.GDmonstre3Objects2= [];
gdjs.tirageCode.GDmonstre3Objects3= [];
gdjs.tirageCode.GDmonstre2Objects1= [];
gdjs.tirageCode.GDmonstre2Objects2= [];
gdjs.tirageCode.GDmonstre2Objects3= [];
gdjs.tirageCode.GDfondObjects1= [];
gdjs.tirageCode.GDfondObjects2= [];
gdjs.tirageCode.GDfondObjects3= [];
gdjs.tirageCode.GDbouton_95suiteObjects1= [];
gdjs.tirageCode.GDbouton_95suiteObjects2= [];
gdjs.tirageCode.GDbouton_95suiteObjects3= [];
gdjs.tirageCode.GDserge_95aideObjects1= [];
gdjs.tirageCode.GDserge_95aideObjects2= [];
gdjs.tirageCode.GDserge_95aideObjects3= [];
gdjs.tirageCode.GDbouton_95retourObjects1= [];
gdjs.tirageCode.GDbouton_95retourObjects2= [];
gdjs.tirageCode.GDbouton_95retourObjects3= [];

gdjs.tirageCode.conditionTrue_0 = {val:false};
gdjs.tirageCode.condition0IsTrue_0 = {val:false};
gdjs.tirageCode.condition1IsTrue_0 = {val:false};
gdjs.tirageCode.condition2IsTrue_0 = {val:false};
gdjs.tirageCode.condition3IsTrue_0 = {val:false};
gdjs.tirageCode.condition4IsTrue_0 = {val:false};
gdjs.tirageCode.condition5IsTrue_0 = {val:false};
gdjs.tirageCode.conditionTrue_1 = {val:false};
gdjs.tirageCode.condition0IsTrue_1 = {val:false};
gdjs.tirageCode.condition1IsTrue_1 = {val:false};
gdjs.tirageCode.condition2IsTrue_1 = {val:false};
gdjs.tirageCode.condition3IsTrue_1 = {val:false};
gdjs.tirageCode.condition4IsTrue_1 = {val:false};
gdjs.tirageCode.condition5IsTrue_1 = {val:false};


gdjs.tirageCode.eventsList0 = function(runtimeScene) {

{


gdjs.tirageCode.condition0IsTrue_0.val = false;
{
gdjs.tirageCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("1")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0"));
}if (gdjs.tirageCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("2").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0")));
}{runtimeScene.getScene().getVariables().getFromIndex(0).add(1);
}}

}


{


gdjs.tirageCode.condition0IsTrue_0.val = false;
{
gdjs.tirageCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("1")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0"));
}if (gdjs.tirageCode.condition0IsTrue_0.val) {
{runtimeScene.getScene().getVariables().getFromIndex(0).sub(1);
}}

}


};gdjs.tirageCode.eventsList1 = function(runtimeScene) {

{


gdjs.tirageCode.condition0IsTrue_0.val = false;
gdjs.tirageCode.condition1IsTrue_0.val = false;
{
gdjs.tirageCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("2")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0"));
}if ( gdjs.tirageCode.condition0IsTrue_0.val ) {
{
gdjs.tirageCode.condition1IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("1")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0"));
}}
if (gdjs.tirageCode.condition1IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("3").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0")));
}{runtimeScene.getScene().getVariables().getFromIndex(0).add(1);
}}

}


{


gdjs.tirageCode.condition0IsTrue_0.val = false;
{
{gdjs.tirageCode.conditionTrue_1 = gdjs.tirageCode.condition0IsTrue_0;
gdjs.tirageCode.condition0IsTrue_1.val = false;
gdjs.tirageCode.condition1IsTrue_1.val = false;
{
gdjs.tirageCode.condition0IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("2")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0"));
if( gdjs.tirageCode.condition0IsTrue_1.val ) {
    gdjs.tirageCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirageCode.condition1IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("1")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0"));
if( gdjs.tirageCode.condition1IsTrue_1.val ) {
    gdjs.tirageCode.conditionTrue_1.val = true;
}
}
{
}
}
}if (gdjs.tirageCode.condition0IsTrue_0.val) {
{runtimeScene.getScene().getVariables().getFromIndex(0).sub(1);
}}

}


};gdjs.tirageCode.eventsList2 = function(runtimeScene) {

{


gdjs.tirageCode.condition0IsTrue_0.val = false;
gdjs.tirageCode.condition1IsTrue_0.val = false;
gdjs.tirageCode.condition2IsTrue_0.val = false;
{
gdjs.tirageCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("3")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0"));
}if ( gdjs.tirageCode.condition0IsTrue_0.val ) {
{
gdjs.tirageCode.condition1IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("2")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0"));
}if ( gdjs.tirageCode.condition1IsTrue_0.val ) {
{
gdjs.tirageCode.condition2IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("1")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0"));
}}
}
if (gdjs.tirageCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("4").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0")));
}{runtimeScene.getScene().getVariables().getFromIndex(0).add(1);
}}

}


{


gdjs.tirageCode.condition0IsTrue_0.val = false;
{
{gdjs.tirageCode.conditionTrue_1 = gdjs.tirageCode.condition0IsTrue_0;
gdjs.tirageCode.condition0IsTrue_1.val = false;
gdjs.tirageCode.condition1IsTrue_1.val = false;
gdjs.tirageCode.condition2IsTrue_1.val = false;
{
gdjs.tirageCode.condition0IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("3")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0"));
if( gdjs.tirageCode.condition0IsTrue_1.val ) {
    gdjs.tirageCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirageCode.condition1IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("2")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0"));
if( gdjs.tirageCode.condition1IsTrue_1.val ) {
    gdjs.tirageCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirageCode.condition2IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("1")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0"));
if( gdjs.tirageCode.condition2IsTrue_1.val ) {
    gdjs.tirageCode.conditionTrue_1.val = true;
}
}
{
}
}
}if (gdjs.tirageCode.condition0IsTrue_0.val) {
{runtimeScene.getScene().getVariables().getFromIndex(0).sub(1);
}}

}


};gdjs.tirageCode.eventsList3 = function(runtimeScene) {

{


gdjs.tirageCode.condition0IsTrue_0.val = false;
gdjs.tirageCode.condition1IsTrue_0.val = false;
gdjs.tirageCode.condition2IsTrue_0.val = false;
gdjs.tirageCode.condition3IsTrue_0.val = false;
{
gdjs.tirageCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("4")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0"));
}if ( gdjs.tirageCode.condition0IsTrue_0.val ) {
{
gdjs.tirageCode.condition1IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("3")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0"));
}if ( gdjs.tirageCode.condition1IsTrue_0.val ) {
{
gdjs.tirageCode.condition2IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("2")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0"));
}if ( gdjs.tirageCode.condition2IsTrue_0.val ) {
{
gdjs.tirageCode.condition3IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("1")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0"));
}}
}
}
if (gdjs.tirageCode.condition3IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("5").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0")));
}{runtimeScene.getScene().getVariables().getFromIndex(0).add(1);
}}

}


{


gdjs.tirageCode.condition0IsTrue_0.val = false;
{
{gdjs.tirageCode.conditionTrue_1 = gdjs.tirageCode.condition0IsTrue_0;
gdjs.tirageCode.condition0IsTrue_1.val = false;
gdjs.tirageCode.condition1IsTrue_1.val = false;
gdjs.tirageCode.condition2IsTrue_1.val = false;
gdjs.tirageCode.condition3IsTrue_1.val = false;
{
gdjs.tirageCode.condition0IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("4")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0"));
if( gdjs.tirageCode.condition0IsTrue_1.val ) {
    gdjs.tirageCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirageCode.condition1IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("3")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0"));
if( gdjs.tirageCode.condition1IsTrue_1.val ) {
    gdjs.tirageCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirageCode.condition2IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("2")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0"));
if( gdjs.tirageCode.condition2IsTrue_1.val ) {
    gdjs.tirageCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirageCode.condition3IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("1")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0"));
if( gdjs.tirageCode.condition3IsTrue_1.val ) {
    gdjs.tirageCode.conditionTrue_1.val = true;
}
}
{
}
}
}if (gdjs.tirageCode.condition0IsTrue_0.val) {
{runtimeScene.getScene().getVariables().getFromIndex(0).sub(1);
}}

}


};gdjs.tirageCode.eventsList4 = function(runtimeScene) {

{


gdjs.tirageCode.condition0IsTrue_0.val = false;
{
gdjs.tirageCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4)) == 1;
}if (gdjs.tirageCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "jeu_niv1", false);
}}

}


{


gdjs.tirageCode.condition0IsTrue_0.val = false;
{
gdjs.tirageCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4)) == 2;
}if (gdjs.tirageCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "jeu_niv2", false);
}}

}


{


gdjs.tirageCode.condition0IsTrue_0.val = false;
{
gdjs.tirageCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4)) == 3;
}if (gdjs.tirageCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "jeu_niv3", false);
}}

}


};gdjs.tirageCode.eventsList5 = function(runtimeScene) {

{


gdjs.tirageCode.condition0IsTrue_0.val = false;
{
gdjs.tirageCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.tirageCode.condition0IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("fond"), gdjs.tirageCode.GDfondObjects1);
{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(0);
}{runtimeScene.getScene().getVariables().getFromIndex(0).setNumber(1);
}{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/audio_vide.mp3", 1, false, 100, 1);
}{for(var i = 0, len = gdjs.tirageCode.GDfondObjects1.length ;i < len;++i) {
    gdjs.tirageCode.GDfondObjects1[i].setOpacity(150);
}
}}

}


{


gdjs.tirageCode.condition0IsTrue_0.val = false;
gdjs.tirageCode.condition1IsTrue_0.val = false;
{
gdjs.tirageCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4)) == 3;
}if ( gdjs.tirageCode.condition0IsTrue_0.val ) {
{
{gdjs.tirageCode.conditionTrue_1 = gdjs.tirageCode.condition1IsTrue_0;
gdjs.tirageCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(18495868);
}
}}
if (gdjs.tirageCode.condition1IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("monstre"), gdjs.tirageCode.GDmonstreObjects1);
gdjs.copyArray(runtimeScene.getObjects("monstre2"), gdjs.tirageCode.GDmonstre2Objects1);
gdjs.copyArray(runtimeScene.getObjects("monstre3"), gdjs.tirageCode.GDmonstre3Objects1);
{for(var i = 0, len = gdjs.tirageCode.GDmonstre2Objects1.length ;i < len;++i) {
    gdjs.tirageCode.GDmonstre2Objects1[i].hide();
}
}{for(var i = 0, len = gdjs.tirageCode.GDmonstreObjects1.length ;i < len;++i) {
    gdjs.tirageCode.GDmonstreObjects1[i].hide();
}
}{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/monstre3_faim.mp3", 1, false, 100, 1);
}{for(var i = 0, len = gdjs.tirageCode.GDmonstre3Objects1.length ;i < len;++i) {
    gdjs.tirageCode.GDmonstre3Objects1[i].setAnimation(1);
}
}}

}


{


gdjs.tirageCode.condition0IsTrue_0.val = false;
gdjs.tirageCode.condition1IsTrue_0.val = false;
{
gdjs.tirageCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4)) == 2;
}if ( gdjs.tirageCode.condition0IsTrue_0.val ) {
{
{gdjs.tirageCode.conditionTrue_1 = gdjs.tirageCode.condition1IsTrue_0;
gdjs.tirageCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(18497516);
}
}}
if (gdjs.tirageCode.condition1IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("monstre"), gdjs.tirageCode.GDmonstreObjects1);
gdjs.copyArray(runtimeScene.getObjects("monstre2"), gdjs.tirageCode.GDmonstre2Objects1);
gdjs.copyArray(runtimeScene.getObjects("monstre3"), gdjs.tirageCode.GDmonstre3Objects1);
{for(var i = 0, len = gdjs.tirageCode.GDmonstreObjects1.length ;i < len;++i) {
    gdjs.tirageCode.GDmonstreObjects1[i].hide();
}
}{for(var i = 0, len = gdjs.tirageCode.GDmonstre3Objects1.length ;i < len;++i) {
    gdjs.tirageCode.GDmonstre3Objects1[i].hide();
}
}{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/monstre2_j_ai_faim.mp3", 1, false, 100, 1);
}{for(var i = 0, len = gdjs.tirageCode.GDmonstre2Objects1.length ;i < len;++i) {
    gdjs.tirageCode.GDmonstre2Objects1[i].setAnimation(1);
}
}}

}


{


gdjs.tirageCode.condition0IsTrue_0.val = false;
gdjs.tirageCode.condition1IsTrue_0.val = false;
{
gdjs.tirageCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4)) == 1;
}if ( gdjs.tirageCode.condition0IsTrue_0.val ) {
{
{gdjs.tirageCode.conditionTrue_1 = gdjs.tirageCode.condition1IsTrue_0;
gdjs.tirageCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(18499156);
}
}}
if (gdjs.tirageCode.condition1IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("monstre"), gdjs.tirageCode.GDmonstreObjects1);
gdjs.copyArray(runtimeScene.getObjects("monstre2"), gdjs.tirageCode.GDmonstre2Objects1);
gdjs.copyArray(runtimeScene.getObjects("monstre3"), gdjs.tirageCode.GDmonstre3Objects1);
{for(var i = 0, len = gdjs.tirageCode.GDmonstre2Objects1.length ;i < len;++i) {
    gdjs.tirageCode.GDmonstre2Objects1[i].hide();
}
}{for(var i = 0, len = gdjs.tirageCode.GDmonstre3Objects1.length ;i < len;++i) {
    gdjs.tirageCode.GDmonstre3Objects1[i].hide();
}
}{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/j_ai_faim.mp3", 1, false, 100, 1);
}{for(var i = 0, len = gdjs.tirageCode.GDmonstreObjects1.length ;i < len;++i) {
    gdjs.tirageCode.GDmonstreObjects1[i].setAnimation(1);
}
}}

}


{


gdjs.tirageCode.condition0IsTrue_0.val = false;
{
{gdjs.tirageCode.conditionTrue_1 = gdjs.tirageCode.condition0IsTrue_0;
gdjs.tirageCode.condition0IsTrue_1.val = false;
gdjs.tirageCode.condition1IsTrue_1.val = false;
gdjs.tirageCode.condition2IsTrue_1.val = false;
gdjs.tirageCode.condition3IsTrue_1.val = false;
gdjs.tirageCode.condition4IsTrue_1.val = false;
{
gdjs.tirageCode.condition0IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 1;
if( gdjs.tirageCode.condition0IsTrue_1.val ) {
    gdjs.tirageCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirageCode.condition1IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 3;
if( gdjs.tirageCode.condition1IsTrue_1.val ) {
    gdjs.tirageCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirageCode.condition2IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 5;
if( gdjs.tirageCode.condition2IsTrue_1.val ) {
    gdjs.tirageCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirageCode.condition3IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 7;
if( gdjs.tirageCode.condition3IsTrue_1.val ) {
    gdjs.tirageCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirageCode.condition4IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 9;
if( gdjs.tirageCode.condition4IsTrue_1.val ) {
    gdjs.tirageCode.conditionTrue_1.val = true;
}
}
{
}
}
}if (gdjs.tirageCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").setNumber(gdjs.randomInRange(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(6).getChild("nb").getChild("mini")), gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(6).getChild("nb").getChild("maxi"))));
}{runtimeScene.getScene().getVariables().getFromIndex(0).add(1);
}}

}


{


gdjs.tirageCode.condition0IsTrue_0.val = false;
{
gdjs.tirageCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 2;
}if (gdjs.tirageCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("1").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0")));
}{runtimeScene.getScene().getVariables().getFromIndex(0).add(1);
}}

}


{


gdjs.tirageCode.condition0IsTrue_0.val = false;
{
gdjs.tirageCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 4;
}if (gdjs.tirageCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.tirageCode.eventsList0(runtimeScene);} //End of subevents
}

}


{


gdjs.tirageCode.condition0IsTrue_0.val = false;
{
gdjs.tirageCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 6;
}if (gdjs.tirageCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.tirageCode.eventsList1(runtimeScene);} //End of subevents
}

}


{


gdjs.tirageCode.condition0IsTrue_0.val = false;
{
gdjs.tirageCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 8;
}if (gdjs.tirageCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.tirageCode.eventsList2(runtimeScene);} //End of subevents
}

}


{


gdjs.tirageCode.condition0IsTrue_0.val = false;
{
gdjs.tirageCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 10;
}if (gdjs.tirageCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.tirageCode.eventsList3(runtimeScene);} //End of subevents
}

}


{


gdjs.tirageCode.condition0IsTrue_0.val = false;
gdjs.tirageCode.condition1IsTrue_0.val = false;
{
gdjs.tirageCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 11;
}if ( gdjs.tirageCode.condition0IsTrue_0.val ) {
{
gdjs.tirageCode.condition1IsTrue_0.val = gdjs.evtTools.sound.isMusicOnChannelStopped(runtimeScene, 1);
}}
if (gdjs.tirageCode.condition1IsTrue_0.val) {

{ //Subevents
gdjs.tirageCode.eventsList4(runtimeScene);} //End of subevents
}

}


};

gdjs.tirageCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.tirageCode.GDscore_951Objects1.length = 0;
gdjs.tirageCode.GDscore_951Objects2.length = 0;
gdjs.tirageCode.GDscore_951Objects3.length = 0;
gdjs.tirageCode.GDscore_952Objects1.length = 0;
gdjs.tirageCode.GDscore_952Objects2.length = 0;
gdjs.tirageCode.GDscore_952Objects3.length = 0;
gdjs.tirageCode.GDscore_953Objects1.length = 0;
gdjs.tirageCode.GDscore_953Objects2.length = 0;
gdjs.tirageCode.GDscore_953Objects3.length = 0;
gdjs.tirageCode.GDmonstreObjects1.length = 0;
gdjs.tirageCode.GDmonstreObjects2.length = 0;
gdjs.tirageCode.GDmonstreObjects3.length = 0;
gdjs.tirageCode.GDpanierObjects1.length = 0;
gdjs.tirageCode.GDpanierObjects2.length = 0;
gdjs.tirageCode.GDpanierObjects3.length = 0;
gdjs.tirageCode.GDbouton_95recommencerObjects1.length = 0;
gdjs.tirageCode.GDbouton_95recommencerObjects2.length = 0;
gdjs.tirageCode.GDbouton_95recommencerObjects3.length = 0;
gdjs.tirageCode.GDmonstre3Objects1.length = 0;
gdjs.tirageCode.GDmonstre3Objects2.length = 0;
gdjs.tirageCode.GDmonstre3Objects3.length = 0;
gdjs.tirageCode.GDmonstre2Objects1.length = 0;
gdjs.tirageCode.GDmonstre2Objects2.length = 0;
gdjs.tirageCode.GDmonstre2Objects3.length = 0;
gdjs.tirageCode.GDfondObjects1.length = 0;
gdjs.tirageCode.GDfondObjects2.length = 0;
gdjs.tirageCode.GDfondObjects3.length = 0;
gdjs.tirageCode.GDbouton_95suiteObjects1.length = 0;
gdjs.tirageCode.GDbouton_95suiteObjects2.length = 0;
gdjs.tirageCode.GDbouton_95suiteObjects3.length = 0;
gdjs.tirageCode.GDserge_95aideObjects1.length = 0;
gdjs.tirageCode.GDserge_95aideObjects2.length = 0;
gdjs.tirageCode.GDserge_95aideObjects3.length = 0;
gdjs.tirageCode.GDbouton_95retourObjects1.length = 0;
gdjs.tirageCode.GDbouton_95retourObjects2.length = 0;
gdjs.tirageCode.GDbouton_95retourObjects3.length = 0;

gdjs.tirageCode.eventsList5(runtimeScene);

return;

}

gdjs['tirageCode'] = gdjs.tirageCode;
