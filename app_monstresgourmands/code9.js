gdjs.finCode = {};
gdjs.finCode.GDscore_951Objects1= [];
gdjs.finCode.GDscore_951Objects2= [];
gdjs.finCode.GDscore_951Objects3= [];
gdjs.finCode.GDscore_952Objects1= [];
gdjs.finCode.GDscore_952Objects2= [];
gdjs.finCode.GDscore_952Objects3= [];
gdjs.finCode.GDscore_953Objects1= [];
gdjs.finCode.GDscore_953Objects2= [];
gdjs.finCode.GDscore_953Objects3= [];
gdjs.finCode.GDmonstreObjects1= [];
gdjs.finCode.GDmonstreObjects2= [];
gdjs.finCode.GDmonstreObjects3= [];
gdjs.finCode.GDpanierObjects1= [];
gdjs.finCode.GDpanierObjects2= [];
gdjs.finCode.GDpanierObjects3= [];
gdjs.finCode.GDbouton_95recommencerObjects1= [];
gdjs.finCode.GDbouton_95recommencerObjects2= [];
gdjs.finCode.GDbouton_95recommencerObjects3= [];
gdjs.finCode.GDmonstre3Objects1= [];
gdjs.finCode.GDmonstre3Objects2= [];
gdjs.finCode.GDmonstre3Objects3= [];
gdjs.finCode.GDmonstre2Objects1= [];
gdjs.finCode.GDmonstre2Objects2= [];
gdjs.finCode.GDmonstre2Objects3= [];
gdjs.finCode.GDfondObjects1= [];
gdjs.finCode.GDfondObjects2= [];
gdjs.finCode.GDfondObjects3= [];
gdjs.finCode.GDbouton_95suiteObjects1= [];
gdjs.finCode.GDbouton_95suiteObjects2= [];
gdjs.finCode.GDbouton_95suiteObjects3= [];
gdjs.finCode.GDserge_95aideObjects1= [];
gdjs.finCode.GDserge_95aideObjects2= [];
gdjs.finCode.GDserge_95aideObjects3= [];
gdjs.finCode.GDbouton_95retourObjects1= [];
gdjs.finCode.GDbouton_95retourObjects2= [];
gdjs.finCode.GDbouton_95retourObjects3= [];

gdjs.finCode.conditionTrue_0 = {val:false};
gdjs.finCode.condition0IsTrue_0 = {val:false};
gdjs.finCode.condition1IsTrue_0 = {val:false};
gdjs.finCode.condition2IsTrue_0 = {val:false};


gdjs.finCode.eventsList0 = function(runtimeScene) {

{


gdjs.finCode.condition0IsTrue_0.val = false;
{
gdjs.finCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4)) == 1;
}if (gdjs.finCode.condition0IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("monstre"), gdjs.finCode.GDmonstreObjects2);
gdjs.copyArray(runtimeScene.getObjects("monstre2"), gdjs.finCode.GDmonstre2Objects2);
gdjs.copyArray(runtimeScene.getObjects("monstre3"), gdjs.finCode.GDmonstre3Objects2);
{for(var i = 0, len = gdjs.finCode.GDmonstre3Objects2.length ;i < len;++i) {
    gdjs.finCode.GDmonstre3Objects2[i].hide();
}
}{for(var i = 0, len = gdjs.finCode.GDmonstre2Objects2.length ;i < len;++i) {
    gdjs.finCode.GDmonstre2Objects2[i].hide();
}
}{for(var i = 0, len = gdjs.finCode.GDmonstreObjects2.length ;i < len;++i) {
    gdjs.finCode.GDmonstreObjects2[i].setAnimation(1);
}
}{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/fin.mp3", 1, false, 100, 1);
}{runtimeScene.getScene().getVariables().getFromIndex(0).setNumber(1);
}}

}


{


gdjs.finCode.condition0IsTrue_0.val = false;
{
gdjs.finCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4)) == 2;
}if (gdjs.finCode.condition0IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("monstre"), gdjs.finCode.GDmonstreObjects2);
gdjs.copyArray(runtimeScene.getObjects("monstre2"), gdjs.finCode.GDmonstre2Objects2);
gdjs.copyArray(runtimeScene.getObjects("monstre3"), gdjs.finCode.GDmonstre3Objects2);
{for(var i = 0, len = gdjs.finCode.GDmonstre3Objects2.length ;i < len;++i) {
    gdjs.finCode.GDmonstre3Objects2[i].hide();
}
}{for(var i = 0, len = gdjs.finCode.GDmonstreObjects2.length ;i < len;++i) {
    gdjs.finCode.GDmonstreObjects2[i].hide();
}
}{for(var i = 0, len = gdjs.finCode.GDmonstre2Objects2.length ;i < len;++i) {
    gdjs.finCode.GDmonstre2Objects2[i].setAnimation(1);
}
}{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/monstre2_regale.mp3", 1, false, 100, 1);
}{runtimeScene.getScene().getVariables().getFromIndex(0).setNumber(1);
}}

}


{


gdjs.finCode.condition0IsTrue_0.val = false;
{
gdjs.finCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4)) == 3;
}if (gdjs.finCode.condition0IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("monstre"), gdjs.finCode.GDmonstreObjects1);
gdjs.copyArray(runtimeScene.getObjects("monstre2"), gdjs.finCode.GDmonstre2Objects1);
gdjs.copyArray(runtimeScene.getObjects("monstre3"), gdjs.finCode.GDmonstre3Objects1);
{for(var i = 0, len = gdjs.finCode.GDmonstre2Objects1.length ;i < len;++i) {
    gdjs.finCode.GDmonstre2Objects1[i].hide();
}
}{for(var i = 0, len = gdjs.finCode.GDmonstreObjects1.length ;i < len;++i) {
    gdjs.finCode.GDmonstreObjects1[i].hide();
}
}{for(var i = 0, len = gdjs.finCode.GDmonstre3Objects1.length ;i < len;++i) {
    gdjs.finCode.GDmonstre3Objects1[i].setAnimation(1);
}
}{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/monstre3_fin.mp3", 1, false, 100, 1);
}{runtimeScene.getScene().getVariables().getFromIndex(0).setNumber(1);
}}

}


};gdjs.finCode.eventsList1 = function(runtimeScene) {

{


gdjs.finCode.condition0IsTrue_0.val = false;
{
gdjs.finCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.finCode.condition0IsTrue_0.val) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/audio_vide.mp3", 1, false, 100, 1);
}
{ //Subevents
gdjs.finCode.eventsList0(runtimeScene);} //End of subevents
}

}


{


gdjs.finCode.condition0IsTrue_0.val = false;
gdjs.finCode.condition1IsTrue_0.val = false;
{
gdjs.finCode.condition0IsTrue_0.val = gdjs.evtTools.sound.isMusicOnChannelStopped(runtimeScene, 1);
}if ( gdjs.finCode.condition0IsTrue_0.val ) {
{
gdjs.finCode.condition1IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 1;
}}
if (gdjs.finCode.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "menu", false);
}}

}


};

gdjs.finCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.finCode.GDscore_951Objects1.length = 0;
gdjs.finCode.GDscore_951Objects2.length = 0;
gdjs.finCode.GDscore_951Objects3.length = 0;
gdjs.finCode.GDscore_952Objects1.length = 0;
gdjs.finCode.GDscore_952Objects2.length = 0;
gdjs.finCode.GDscore_952Objects3.length = 0;
gdjs.finCode.GDscore_953Objects1.length = 0;
gdjs.finCode.GDscore_953Objects2.length = 0;
gdjs.finCode.GDscore_953Objects3.length = 0;
gdjs.finCode.GDmonstreObjects1.length = 0;
gdjs.finCode.GDmonstreObjects2.length = 0;
gdjs.finCode.GDmonstreObjects3.length = 0;
gdjs.finCode.GDpanierObjects1.length = 0;
gdjs.finCode.GDpanierObjects2.length = 0;
gdjs.finCode.GDpanierObjects3.length = 0;
gdjs.finCode.GDbouton_95recommencerObjects1.length = 0;
gdjs.finCode.GDbouton_95recommencerObjects2.length = 0;
gdjs.finCode.GDbouton_95recommencerObjects3.length = 0;
gdjs.finCode.GDmonstre3Objects1.length = 0;
gdjs.finCode.GDmonstre3Objects2.length = 0;
gdjs.finCode.GDmonstre3Objects3.length = 0;
gdjs.finCode.GDmonstre2Objects1.length = 0;
gdjs.finCode.GDmonstre2Objects2.length = 0;
gdjs.finCode.GDmonstre2Objects3.length = 0;
gdjs.finCode.GDfondObjects1.length = 0;
gdjs.finCode.GDfondObjects2.length = 0;
gdjs.finCode.GDfondObjects3.length = 0;
gdjs.finCode.GDbouton_95suiteObjects1.length = 0;
gdjs.finCode.GDbouton_95suiteObjects2.length = 0;
gdjs.finCode.GDbouton_95suiteObjects3.length = 0;
gdjs.finCode.GDserge_95aideObjects1.length = 0;
gdjs.finCode.GDserge_95aideObjects2.length = 0;
gdjs.finCode.GDserge_95aideObjects3.length = 0;
gdjs.finCode.GDbouton_95retourObjects1.length = 0;
gdjs.finCode.GDbouton_95retourObjects2.length = 0;
gdjs.finCode.GDbouton_95retourObjects3.length = 0;

gdjs.finCode.eventsList1(runtimeScene);

return;

}

gdjs['finCode'] = gdjs.finCode;
