gdjs.codeCode = {};
gdjs.codeCode.GDscore_951Objects1= [];
gdjs.codeCode.GDscore_951Objects2= [];
gdjs.codeCode.GDscore_951Objects3= [];
gdjs.codeCode.GDscore_952Objects1= [];
gdjs.codeCode.GDscore_952Objects2= [];
gdjs.codeCode.GDscore_952Objects3= [];
gdjs.codeCode.GDscore_953Objects1= [];
gdjs.codeCode.GDscore_953Objects2= [];
gdjs.codeCode.GDscore_953Objects3= [];
gdjs.codeCode.GDmonstreObjects1= [];
gdjs.codeCode.GDmonstreObjects2= [];
gdjs.codeCode.GDmonstreObjects3= [];
gdjs.codeCode.GDpanierObjects1= [];
gdjs.codeCode.GDpanierObjects2= [];
gdjs.codeCode.GDpanierObjects3= [];
gdjs.codeCode.GDbouton_95recommencerObjects1= [];
gdjs.codeCode.GDbouton_95recommencerObjects2= [];
gdjs.codeCode.GDbouton_95recommencerObjects3= [];
gdjs.codeCode.GDmonstre3Objects1= [];
gdjs.codeCode.GDmonstre3Objects2= [];
gdjs.codeCode.GDmonstre3Objects3= [];
gdjs.codeCode.GDmonstre2Objects1= [];
gdjs.codeCode.GDmonstre2Objects2= [];
gdjs.codeCode.GDmonstre2Objects3= [];
gdjs.codeCode.GDfondObjects1= [];
gdjs.codeCode.GDfondObjects2= [];
gdjs.codeCode.GDfondObjects3= [];
gdjs.codeCode.GDbouton_95suiteObjects1= [];
gdjs.codeCode.GDbouton_95suiteObjects2= [];
gdjs.codeCode.GDbouton_95suiteObjects3= [];
gdjs.codeCode.GDserge_95aideObjects1= [];
gdjs.codeCode.GDserge_95aideObjects2= [];
gdjs.codeCode.GDserge_95aideObjects3= [];
gdjs.codeCode.GDbouton_95retourObjects1= [];
gdjs.codeCode.GDbouton_95retourObjects2= [];
gdjs.codeCode.GDbouton_95retourObjects3= [];
gdjs.codeCode.GDtexte_95codeObjects1= [];
gdjs.codeCode.GDtexte_95codeObjects2= [];
gdjs.codeCode.GDtexte_95codeObjects3= [];
gdjs.codeCode.GDchoix_95nombre1Objects1= [];
gdjs.codeCode.GDchoix_95nombre1Objects2= [];
gdjs.codeCode.GDchoix_95nombre1Objects3= [];
gdjs.codeCode.GDchoix_95nombre2Objects1= [];
gdjs.codeCode.GDchoix_95nombre2Objects2= [];
gdjs.codeCode.GDchoix_95nombre2Objects3= [];
gdjs.codeCode.GDchoix_95nombre3Objects1= [];
gdjs.codeCode.GDchoix_95nombre3Objects2= [];
gdjs.codeCode.GDchoix_95nombre3Objects3= [];
gdjs.codeCode.GDchoix_95nombre4Objects1= [];
gdjs.codeCode.GDchoix_95nombre4Objects2= [];
gdjs.codeCode.GDchoix_95nombre4Objects3= [];
gdjs.codeCode.GDchoix_95nombre5Objects1= [];
gdjs.codeCode.GDchoix_95nombre5Objects2= [];
gdjs.codeCode.GDchoix_95nombre5Objects3= [];
gdjs.codeCode.GDNewSpriteObjects1= [];
gdjs.codeCode.GDNewSpriteObjects2= [];
gdjs.codeCode.GDNewSpriteObjects3= [];

gdjs.codeCode.conditionTrue_0 = {val:false};
gdjs.codeCode.condition0IsTrue_0 = {val:false};
gdjs.codeCode.condition1IsTrue_0 = {val:false};
gdjs.codeCode.condition2IsTrue_0 = {val:false};
gdjs.codeCode.condition3IsTrue_0 = {val:false};
gdjs.codeCode.conditionTrue_1 = {val:false};
gdjs.codeCode.condition0IsTrue_1 = {val:false};
gdjs.codeCode.condition1IsTrue_1 = {val:false};
gdjs.codeCode.condition2IsTrue_1 = {val:false};
gdjs.codeCode.condition3IsTrue_1 = {val:false};


gdjs.codeCode.mapOfGDgdjs_46codeCode_46GDchoix_9595nombre3Objects1Objects = Hashtable.newFrom({"choix_nombre3": gdjs.codeCode.GDchoix_95nombre3Objects1});
gdjs.codeCode.eventsList0 = function(runtimeScene) {

{


gdjs.codeCode.condition0IsTrue_0.val = false;
gdjs.codeCode.condition1IsTrue_0.val = false;
{
gdjs.codeCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) != 0;
}if ( gdjs.codeCode.condition0IsTrue_0.val ) {
{
{gdjs.codeCode.conditionTrue_1 = gdjs.codeCode.condition1IsTrue_0;
gdjs.codeCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(18405612);
}
}}
if (gdjs.codeCode.condition1IsTrue_0.val) {
{runtimeScene.getScene().getVariables().getFromIndex(1).setNumber(0);
}}

}


{


gdjs.codeCode.condition0IsTrue_0.val = false;
gdjs.codeCode.condition1IsTrue_0.val = false;
{
gdjs.codeCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 0;
}if ( gdjs.codeCode.condition0IsTrue_0.val ) {
{
{gdjs.codeCode.conditionTrue_1 = gdjs.codeCode.condition1IsTrue_0;
gdjs.codeCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(18406692);
}
}}
if (gdjs.codeCode.condition1IsTrue_0.val) {
{runtimeScene.getScene().getVariables().getFromIndex(1).add(1);
}}

}


};gdjs.codeCode.mapOfGDgdjs_46codeCode_46GDchoix_9595nombre5Objects1Objects = Hashtable.newFrom({"choix_nombre5": gdjs.codeCode.GDchoix_95nombre5Objects1});
gdjs.codeCode.eventsList1 = function(runtimeScene) {

{


gdjs.codeCode.condition0IsTrue_0.val = false;
{
gdjs.codeCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) != 1;
}if (gdjs.codeCode.condition0IsTrue_0.val) {
{runtimeScene.getScene().getVariables().getFromIndex(1).setNumber(0);
}}

}


{


gdjs.codeCode.condition0IsTrue_0.val = false;
{
gdjs.codeCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 1;
}if (gdjs.codeCode.condition0IsTrue_0.val) {
{runtimeScene.getScene().getVariables().getFromIndex(1).add(1);
}}

}


};gdjs.codeCode.mapOfGDgdjs_46codeCode_46GDchoix_9595nombre2Objects1Objects = Hashtable.newFrom({"choix_nombre2": gdjs.codeCode.GDchoix_95nombre2Objects1});
gdjs.codeCode.eventsList2 = function(runtimeScene) {

{


gdjs.codeCode.condition0IsTrue_0.val = false;
{
gdjs.codeCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) != 2;
}if (gdjs.codeCode.condition0IsTrue_0.val) {
{runtimeScene.getScene().getVariables().getFromIndex(1).setNumber(0);
}}

}


{


gdjs.codeCode.condition0IsTrue_0.val = false;
{
gdjs.codeCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 2;
}if (gdjs.codeCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "options", false);
}}

}


};gdjs.codeCode.mapOfGDgdjs_46codeCode_46GDchoix_9595nombre1Objects1ObjectsGDgdjs_46codeCode_46GDchoix_9595nombre4Objects1Objects = Hashtable.newFrom({"choix_nombre1": gdjs.codeCode.GDchoix_95nombre1Objects1, "choix_nombre4": gdjs.codeCode.GDchoix_95nombre4Objects1});
gdjs.codeCode.mapOfGDgdjs_46codeCode_46GDbouton_9595retourObjects1Objects = Hashtable.newFrom({"bouton_retour": gdjs.codeCode.GDbouton_95retourObjects1});
gdjs.codeCode.eventsList3 = function(runtimeScene) {

{


gdjs.codeCode.condition0IsTrue_0.val = false;
{
gdjs.codeCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.codeCode.condition0IsTrue_0.val) {
{runtimeScene.getScene().getVariables().getFromIndex(1).setNumber(0);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("choix_nombre3"), gdjs.codeCode.GDchoix_95nombre3Objects1);

gdjs.codeCode.condition0IsTrue_0.val = false;
gdjs.codeCode.condition1IsTrue_0.val = false;
gdjs.codeCode.condition2IsTrue_0.val = false;
{
gdjs.codeCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.codeCode.mapOfGDgdjs_46codeCode_46GDchoix_9595nombre3Objects1Objects, runtimeScene, true, false);
}if ( gdjs.codeCode.condition0IsTrue_0.val ) {
{
gdjs.codeCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.codeCode.condition1IsTrue_0.val ) {
{
{gdjs.codeCode.conditionTrue_1 = gdjs.codeCode.condition2IsTrue_0;
gdjs.codeCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(18404580);
}
}}
}
if (gdjs.codeCode.condition2IsTrue_0.val) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "bulle_1.mp3", 1, false, 100, 1);
}
{ //Subevents
gdjs.codeCode.eventsList0(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("choix_nombre5"), gdjs.codeCode.GDchoix_95nombre5Objects1);

gdjs.codeCode.condition0IsTrue_0.val = false;
gdjs.codeCode.condition1IsTrue_0.val = false;
gdjs.codeCode.condition2IsTrue_0.val = false;
{
gdjs.codeCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.codeCode.mapOfGDgdjs_46codeCode_46GDchoix_9595nombre5Objects1Objects, runtimeScene, true, false);
}if ( gdjs.codeCode.condition0IsTrue_0.val ) {
{
gdjs.codeCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.codeCode.condition1IsTrue_0.val ) {
{
{gdjs.codeCode.conditionTrue_1 = gdjs.codeCode.condition2IsTrue_0;
gdjs.codeCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(18407972);
}
}}
}
if (gdjs.codeCode.condition2IsTrue_0.val) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "bulle_1.mp3", 1, false, 100, 1);
}
{ //Subevents
gdjs.codeCode.eventsList1(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("choix_nombre2"), gdjs.codeCode.GDchoix_95nombre2Objects1);

gdjs.codeCode.condition0IsTrue_0.val = false;
gdjs.codeCode.condition1IsTrue_0.val = false;
gdjs.codeCode.condition2IsTrue_0.val = false;
{
gdjs.codeCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.codeCode.mapOfGDgdjs_46codeCode_46GDchoix_9595nombre2Objects1Objects, runtimeScene, true, false);
}if ( gdjs.codeCode.condition0IsTrue_0.val ) {
{
gdjs.codeCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.codeCode.condition1IsTrue_0.val ) {
{
{gdjs.codeCode.conditionTrue_1 = gdjs.codeCode.condition2IsTrue_0;
gdjs.codeCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(18410972);
}
}}
}
if (gdjs.codeCode.condition2IsTrue_0.val) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "bulle_1.mp3", 1, false, 100, 1);
}
{ //Subevents
gdjs.codeCode.eventsList2(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("choix_nombre1"), gdjs.codeCode.GDchoix_95nombre1Objects1);
gdjs.copyArray(runtimeScene.getObjects("choix_nombre4"), gdjs.codeCode.GDchoix_95nombre4Objects1);

gdjs.codeCode.condition0IsTrue_0.val = false;
gdjs.codeCode.condition1IsTrue_0.val = false;
gdjs.codeCode.condition2IsTrue_0.val = false;
{
gdjs.codeCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.codeCode.mapOfGDgdjs_46codeCode_46GDchoix_9595nombre1Objects1ObjectsGDgdjs_46codeCode_46GDchoix_9595nombre4Objects1Objects, runtimeScene, true, false);
}if ( gdjs.codeCode.condition0IsTrue_0.val ) {
{
gdjs.codeCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.codeCode.condition1IsTrue_0.val ) {
{
{gdjs.codeCode.conditionTrue_1 = gdjs.codeCode.condition2IsTrue_0;
gdjs.codeCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(18414132);
}
}}
}
if (gdjs.codeCode.condition2IsTrue_0.val) {
{runtimeScene.getScene().getVariables().getFromIndex(1).setNumber(0);
}{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "bulle_1.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_retour"), gdjs.codeCode.GDbouton_95retourObjects1);

gdjs.codeCode.condition0IsTrue_0.val = false;
gdjs.codeCode.condition1IsTrue_0.val = false;
{
gdjs.codeCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.codeCode.mapOfGDgdjs_46codeCode_46GDbouton_9595retourObjects1Objects, runtimeScene, true, false);
}if ( gdjs.codeCode.condition0IsTrue_0.val ) {
{
gdjs.codeCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}}
if (gdjs.codeCode.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "menu", false);
}}

}


};

gdjs.codeCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.codeCode.GDscore_951Objects1.length = 0;
gdjs.codeCode.GDscore_951Objects2.length = 0;
gdjs.codeCode.GDscore_951Objects3.length = 0;
gdjs.codeCode.GDscore_952Objects1.length = 0;
gdjs.codeCode.GDscore_952Objects2.length = 0;
gdjs.codeCode.GDscore_952Objects3.length = 0;
gdjs.codeCode.GDscore_953Objects1.length = 0;
gdjs.codeCode.GDscore_953Objects2.length = 0;
gdjs.codeCode.GDscore_953Objects3.length = 0;
gdjs.codeCode.GDmonstreObjects1.length = 0;
gdjs.codeCode.GDmonstreObjects2.length = 0;
gdjs.codeCode.GDmonstreObjects3.length = 0;
gdjs.codeCode.GDpanierObjects1.length = 0;
gdjs.codeCode.GDpanierObjects2.length = 0;
gdjs.codeCode.GDpanierObjects3.length = 0;
gdjs.codeCode.GDbouton_95recommencerObjects1.length = 0;
gdjs.codeCode.GDbouton_95recommencerObjects2.length = 0;
gdjs.codeCode.GDbouton_95recommencerObjects3.length = 0;
gdjs.codeCode.GDmonstre3Objects1.length = 0;
gdjs.codeCode.GDmonstre3Objects2.length = 0;
gdjs.codeCode.GDmonstre3Objects3.length = 0;
gdjs.codeCode.GDmonstre2Objects1.length = 0;
gdjs.codeCode.GDmonstre2Objects2.length = 0;
gdjs.codeCode.GDmonstre2Objects3.length = 0;
gdjs.codeCode.GDfondObjects1.length = 0;
gdjs.codeCode.GDfondObjects2.length = 0;
gdjs.codeCode.GDfondObjects3.length = 0;
gdjs.codeCode.GDbouton_95suiteObjects1.length = 0;
gdjs.codeCode.GDbouton_95suiteObjects2.length = 0;
gdjs.codeCode.GDbouton_95suiteObjects3.length = 0;
gdjs.codeCode.GDserge_95aideObjects1.length = 0;
gdjs.codeCode.GDserge_95aideObjects2.length = 0;
gdjs.codeCode.GDserge_95aideObjects3.length = 0;
gdjs.codeCode.GDbouton_95retourObjects1.length = 0;
gdjs.codeCode.GDbouton_95retourObjects2.length = 0;
gdjs.codeCode.GDbouton_95retourObjects3.length = 0;
gdjs.codeCode.GDtexte_95codeObjects1.length = 0;
gdjs.codeCode.GDtexte_95codeObjects2.length = 0;
gdjs.codeCode.GDtexte_95codeObjects3.length = 0;
gdjs.codeCode.GDchoix_95nombre1Objects1.length = 0;
gdjs.codeCode.GDchoix_95nombre1Objects2.length = 0;
gdjs.codeCode.GDchoix_95nombre1Objects3.length = 0;
gdjs.codeCode.GDchoix_95nombre2Objects1.length = 0;
gdjs.codeCode.GDchoix_95nombre2Objects2.length = 0;
gdjs.codeCode.GDchoix_95nombre2Objects3.length = 0;
gdjs.codeCode.GDchoix_95nombre3Objects1.length = 0;
gdjs.codeCode.GDchoix_95nombre3Objects2.length = 0;
gdjs.codeCode.GDchoix_95nombre3Objects3.length = 0;
gdjs.codeCode.GDchoix_95nombre4Objects1.length = 0;
gdjs.codeCode.GDchoix_95nombre4Objects2.length = 0;
gdjs.codeCode.GDchoix_95nombre4Objects3.length = 0;
gdjs.codeCode.GDchoix_95nombre5Objects1.length = 0;
gdjs.codeCode.GDchoix_95nombre5Objects2.length = 0;
gdjs.codeCode.GDchoix_95nombre5Objects3.length = 0;
gdjs.codeCode.GDNewSpriteObjects1.length = 0;
gdjs.codeCode.GDNewSpriteObjects2.length = 0;
gdjs.codeCode.GDNewSpriteObjects3.length = 0;

gdjs.codeCode.eventsList3(runtimeScene);

return;

}

gdjs['codeCode'] = gdjs.codeCode;
