# Syllabux

## Présentation

Cette application reproduit le fonctionnement du syllabaire papier qu'on trouve sous forme de classeurs, avec les pages consonnes et voyelles qui se tournent indépendamment pour former les syllabes.

- 4 polices de caractères au choix

- choix des phonèmes

- 3 modèles de syllabe : CV, VC, CVC

- composition manuelle avec le syllabaire affiché, semi-automatique (tirage au sort contrôlé par un bouton "suivant") ou automatique (mode diaporama avec choix du tempo.

![](images/screenshot.png)

## Utilisation

Une instance de démonstration est disponible à l'adresse suivante :
https://educajou.forge.apps.education.fr/syllabux/
Elles est automatiquement mise à jour en fonction des évlolutions du projet.

## Hors-ligne

Pour une utilisation hors, ligne :
- récupérer le ZIP du projet : https://forge.apps.education.fr/educajou/syllabux/-/archive/main/syllabux-main.zip
- extraire le ZIP (clic droit, extraire tout ...)
- dans le dossier Syllabux-main, ouvrir le fichier index.html

## Installation d'une autre instance

Syllabux ne requiert qu'un serveur web, il n'utilise ni PHP ni base de données.
- récupérer le ZIP du projet : https://forge.apps.education.fr/educajou/syllabux/-/archive/main/syllabux-main.zip
- extraire le ZIP (clic droit, extraire tout ...)
- mettre en ligne le dossier (après l'avoir éventuellement renommé) via un transfert FTP.

Je vous remercie dans ce cas de :
- laisser les mentions d'auteur / licence
- personnaliser ou supprimer la section "mentions" en bas de page qui contient le logo de la forge

## Personnalisation

### Liste des graphèmes

Les listes sont facilement éditables au début du fichier /scripts/main.js

