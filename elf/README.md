## ELF

*Ceci est la présentation de l'application.
Pour l'utiliser directement, rendez-vous sur https://educajou.forge.apps.education.fr/elf*

## Présentation

ELF (Éducajou Lecture Flash) permet de lancer instantanément un diaporama à partir d'une liste de mots, en adaptant leur fréquence d'apparition en fonction de la facilité de lecture. Cette application, libre et gratuite, entraîne à l'identification rapide des mots.

C'est un outil d'aide à la fluence, à utiliser avec des mots déjà déchiffrables (et déchiffrés) par les élèves.

L'ordre d'apparition, la police de caractère et la casse (minuscule ou majuscules) est aléatoire.

1. Entrez ou collez une liste de mots (ou expression) en les séparant par des virgules, points, points-virgules ou retours à la ligne.
2. Entrez éventuellement une liste secondaire de mots à réviser, qui seront affichés moins souvent.
3. Lancez le diaporama.

Chaque mot sera présenté 3 fois (1 fois pour les mots de révision) - ces valeurs sont réglables.

Le mot est présenté brièvement (1 seconde - délai réglable) puis disparaît automatiquement.

On peut le revoir à la demande, et écouter une synthèse vocale.

On peut ensuite ajuster en cours de diaporama, selon le niveau de réussite :
- vert : le mot ne sera plus proposé au cours de la session
- jaune : le mot continue d'être proposé au rythme prévu
- orange : le mot sera re-proposé une fois de plus
- rouge : le mot sera re-proposé deux fois de plus

Il est possible de partager la liste de mots par lien ou codeQR.

## Accès à l'application

### En ligne

Une instance fonctionnelle peut être utilisée sur https://educajou.forge.apps.education.fr/elf

### Hors ligne

- Télécharger l'application **[📦 elf-main.zip](https://forge.apps.education.fr/educajou/elf/-/archive/main/elf-main.zip)**
- Extraire le ZIP
- Ouvrir le fichier **index.html**



## Signaler un bug ou proposer une amélioration

Pour participer au développement en rapportant des bugs ou en proposant une fonctionnalité, vous pouvez déposer un ticket sur cette page :
https://forge.apps.education.fr/educajou/elf/-/issues

### Comment créer un ticket ?

<iframe title="Créer un ticket sur La Forge des Communs Numériques Éducatifs - Vidéo Tuto" width="560" height="315" src="https://tube-sciences-technologies.apps.education.fr/videos/embed/058b9ab7-ab65-4d25-b24f-6a34feda013d" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups allow-forms"></iframe>

https://tube-sciences-technologies.apps.education.fr/w/1FHx5ntDrd9mwo5k6dAB2F

