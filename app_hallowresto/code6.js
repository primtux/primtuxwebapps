gdjs.optionsCode = {};
gdjs.optionsCode.GDscore_95952Objects1= [];
gdjs.optionsCode.GDscore_95952Objects2= [];
gdjs.optionsCode.GDscore_95952Objects3= [];
gdjs.optionsCode.GDscore_9595Objects1= [];
gdjs.optionsCode.GDscore_9595Objects2= [];
gdjs.optionsCode.GDscore_9595Objects3= [];
gdjs.optionsCode.GDfondObjects1= [];
gdjs.optionsCode.GDfondObjects2= [];
gdjs.optionsCode.GDfondObjects3= [];
gdjs.optionsCode.GDfond_9595blancObjects1= [];
gdjs.optionsCode.GDfond_9595blancObjects2= [];
gdjs.optionsCode.GDfond_9595blancObjects3= [];
gdjs.optionsCode.GDsandwichObjects1= [];
gdjs.optionsCode.GDsandwichObjects2= [];
gdjs.optionsCode.GDsandwichObjects3= [];
gdjs.optionsCode.GDbol_9595soupeObjects1= [];
gdjs.optionsCode.GDbol_9595soupeObjects2= [];
gdjs.optionsCode.GDbol_9595soupeObjects3= [];
gdjs.optionsCode.GDquantite_9595Objects1= [];
gdjs.optionsCode.GDquantite_9595Objects2= [];
gdjs.optionsCode.GDquantite_9595Objects3= [];
gdjs.optionsCode.GDquantite_95953Objects1= [];
gdjs.optionsCode.GDquantite_95953Objects2= [];
gdjs.optionsCode.GDquantite_95953Objects3= [];
gdjs.optionsCode.GDquantite_95952Objects1= [];
gdjs.optionsCode.GDquantite_95952Objects2= [];
gdjs.optionsCode.GDquantite_95952Objects3= [];
gdjs.optionsCode.GDbouton_9595plus_95952Objects1= [];
gdjs.optionsCode.GDbouton_9595plus_95952Objects2= [];
gdjs.optionsCode.GDbouton_9595plus_95952Objects3= [];
gdjs.optionsCode.GDbouton_9595plus_9595Objects1= [];
gdjs.optionsCode.GDbouton_9595plus_9595Objects2= [];
gdjs.optionsCode.GDbouton_9595plus_9595Objects3= [];
gdjs.optionsCode.GDbouton_9595moins2Objects1= [];
gdjs.optionsCode.GDbouton_9595moins2Objects2= [];
gdjs.optionsCode.GDbouton_9595moins2Objects3= [];
gdjs.optionsCode.GDbouton_9595moinsObjects1= [];
gdjs.optionsCode.GDbouton_9595moinsObjects2= [];
gdjs.optionsCode.GDbouton_9595moinsObjects3= [];
gdjs.optionsCode.GDtravailler_9595entreObjects1= [];
gdjs.optionsCode.GDtravailler_9595entreObjects2= [];
gdjs.optionsCode.GDtravailler_9595entreObjects3= [];
gdjs.optionsCode.GDnb_9595minObjects1= [];
gdjs.optionsCode.GDnb_9595minObjects2= [];
gdjs.optionsCode.GDnb_9595minObjects3= [];
gdjs.optionsCode.GDetObjects1= [];
gdjs.optionsCode.GDetObjects2= [];
gdjs.optionsCode.GDetObjects3= [];
gdjs.optionsCode.GDnb_9595maxObjects1= [];
gdjs.optionsCode.GDnb_9595maxObjects2= [];
gdjs.optionsCode.GDnb_9595maxObjects3= [];
gdjs.optionsCode.GDbouton_9595retourObjects1= [];
gdjs.optionsCode.GDbouton_9595retourObjects2= [];
gdjs.optionsCode.GDbouton_9595retourObjects3= [];
gdjs.optionsCode.GDbouton_9595optionsObjects1= [];
gdjs.optionsCode.GDbouton_9595optionsObjects2= [];
gdjs.optionsCode.GDbouton_9595optionsObjects3= [];


gdjs.optionsCode.mapOfGDgdjs_9546optionsCode_9546GDbouton_95959595plus_95959595Objects2Objects = Hashtable.newFrom({"bouton_plus_": gdjs.optionsCode.GDbouton_9595plus_9595Objects2});
gdjs.optionsCode.mapOfGDgdjs_9546optionsCode_9546GDbouton_95959595moinsObjects2Objects = Hashtable.newFrom({"bouton_moins": gdjs.optionsCode.GDbouton_9595moinsObjects2});
gdjs.optionsCode.mapOfGDgdjs_9546optionsCode_9546GDbouton_95959595plus_959595952Objects2Objects = Hashtable.newFrom({"bouton_plus_2": gdjs.optionsCode.GDbouton_9595plus_95952Objects2});
gdjs.optionsCode.mapOfGDgdjs_9546optionsCode_9546GDbouton_95959595moins2Objects1Objects = Hashtable.newFrom({"bouton_moins2": gdjs.optionsCode.GDbouton_9595moins2Objects1});
gdjs.optionsCode.eventsList0 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("bouton_plus_"), gdjs.optionsCode.GDbouton_9595plus_9595Objects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.optionsCode.mapOfGDgdjs_9546optionsCode_9546GDbouton_95959595plus_95959595Objects2Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(13781748);
}
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3).getChild("nombre").getChild("mini")) < gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3).getChild("nombre").getChild("maxi")) - 4;
}
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("nb_min"), gdjs.optionsCode.GDnb_9595minObjects2);
{runtimeScene.getGame().getVariables().getFromIndex(3).getChild("nombre").getChild("mini").add(1);
}{for(var i = 0, len = gdjs.optionsCode.GDnb_9595minObjects2.length ;i < len;++i) {
    gdjs.optionsCode.GDnb_9595minObjects2[i].setString(gdjs.evtTools.common.toString(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3).getChild("nombre").getChild("mini"))));
}
}{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/son_bouton.mp3", 1, false, 50, 1);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_moins"), gdjs.optionsCode.GDbouton_9595moinsObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.optionsCode.mapOfGDgdjs_9546optionsCode_9546GDbouton_95959595moinsObjects2Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(13780828);
}
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3).getChild("nombre").getChild("mini")) > 1;
}
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("nb_min"), gdjs.optionsCode.GDnb_9595minObjects2);
{runtimeScene.getGame().getVariables().getFromIndex(3).getChild("nombre").getChild("mini").sub(1);
}{for(var i = 0, len = gdjs.optionsCode.GDnb_9595minObjects2.length ;i < len;++i) {
    gdjs.optionsCode.GDnb_9595minObjects2[i].setString(gdjs.evtTools.common.toString(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3).getChild("nombre").getChild("mini"))));
}
}{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/son_bouton.mp3", 1, false, 50, 1);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_plus_2"), gdjs.optionsCode.GDbouton_9595plus_95952Objects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.optionsCode.mapOfGDgdjs_9546optionsCode_9546GDbouton_95959595plus_959595952Objects2Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(13788516);
}
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3).getChild("nombre").getChild("maxi")) < 10;
}
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("nb_max"), gdjs.optionsCode.GDnb_9595maxObjects2);
{runtimeScene.getGame().getVariables().getFromIndex(3).getChild("nombre").getChild("maxi").add(1);
}{for(var i = 0, len = gdjs.optionsCode.GDnb_9595maxObjects2.length ;i < len;++i) {
    gdjs.optionsCode.GDnb_9595maxObjects2[i].setString(gdjs.evtTools.common.toString(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3).getChild("nombre").getChild("maxi"))));
}
}{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/son_bouton.mp3", 1, false, 50, 1);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_moins2"), gdjs.optionsCode.GDbouton_9595moins2Objects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.optionsCode.mapOfGDgdjs_9546optionsCode_9546GDbouton_95959595moins2Objects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(13790860);
}
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3).getChild("nombre").getChild("maxi")) > gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3).getChild("nombre").getChild("mini")) + 4;
}
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("nb_max"), gdjs.optionsCode.GDnb_9595maxObjects1);
{runtimeScene.getGame().getVariables().getFromIndex(3).getChild("nombre").getChild("maxi").sub(1);
}{for(var i = 0, len = gdjs.optionsCode.GDnb_9595maxObjects1.length ;i < len;++i) {
    gdjs.optionsCode.GDnb_9595maxObjects1[i].setString(gdjs.evtTools.common.toString(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3).getChild("nombre").getChild("maxi"))));
}
}{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/son_bouton.mp3", 1, false, 50, 1);
}}

}


};gdjs.optionsCode.mapOfGDgdjs_9546optionsCode_9546GDbouton_95959595retourObjects1Objects = Hashtable.newFrom({"bouton_retour": gdjs.optionsCode.GDbouton_9595retourObjects1});
gdjs.optionsCode.eventsList1 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
{gdjs.evtTools.storage.clearJSONFile("sauvegarde_hallowresto");
}{gdjs.evtTools.storage.writeStringInJSONFile("sauvegarde_hallowresto", "reglages", gdjs.evtTools.network.variableStructureToJSON(runtimeScene.getGame().getVariables().getFromIndex(3)));
}}

}


};gdjs.optionsCode.eventsList2 = function(runtimeScene) {

{


gdjs.optionsCode.eventsList1(runtimeScene);
}


{


let isConditionTrue_0 = false;
{
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "menu", false);
}}

}


};gdjs.optionsCode.eventsList3 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("nb_max"), gdjs.optionsCode.GDnb_9595maxObjects1);
gdjs.copyArray(runtimeScene.getObjects("nb_min"), gdjs.optionsCode.GDnb_9595minObjects1);
{for(var i = 0, len = gdjs.optionsCode.GDnb_9595maxObjects1.length ;i < len;++i) {
    gdjs.optionsCode.GDnb_9595maxObjects1[i].setString(gdjs.evtTools.common.toString(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3).getChild("nombre").getChild("maxi"))));
}
}{for(var i = 0, len = gdjs.optionsCode.GDnb_9595minObjects1.length ;i < len;++i) {
    gdjs.optionsCode.GDnb_9595minObjects1[i].setString(gdjs.evtTools.common.toString(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3).getChild("nombre").getChild("mini"))));
}
}}

}


{


gdjs.optionsCode.eventsList0(runtimeScene);
}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_retour"), gdjs.optionsCode.GDbouton_9595retourObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.optionsCode.mapOfGDgdjs_9546optionsCode_9546GDbouton_95959595retourObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {

{ //Subevents
gdjs.optionsCode.eventsList2(runtimeScene);} //End of subevents
}

}


};

gdjs.optionsCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.optionsCode.GDscore_95952Objects1.length = 0;
gdjs.optionsCode.GDscore_95952Objects2.length = 0;
gdjs.optionsCode.GDscore_95952Objects3.length = 0;
gdjs.optionsCode.GDscore_9595Objects1.length = 0;
gdjs.optionsCode.GDscore_9595Objects2.length = 0;
gdjs.optionsCode.GDscore_9595Objects3.length = 0;
gdjs.optionsCode.GDfondObjects1.length = 0;
gdjs.optionsCode.GDfondObjects2.length = 0;
gdjs.optionsCode.GDfondObjects3.length = 0;
gdjs.optionsCode.GDfond_9595blancObjects1.length = 0;
gdjs.optionsCode.GDfond_9595blancObjects2.length = 0;
gdjs.optionsCode.GDfond_9595blancObjects3.length = 0;
gdjs.optionsCode.GDsandwichObjects1.length = 0;
gdjs.optionsCode.GDsandwichObjects2.length = 0;
gdjs.optionsCode.GDsandwichObjects3.length = 0;
gdjs.optionsCode.GDbol_9595soupeObjects1.length = 0;
gdjs.optionsCode.GDbol_9595soupeObjects2.length = 0;
gdjs.optionsCode.GDbol_9595soupeObjects3.length = 0;
gdjs.optionsCode.GDquantite_9595Objects1.length = 0;
gdjs.optionsCode.GDquantite_9595Objects2.length = 0;
gdjs.optionsCode.GDquantite_9595Objects3.length = 0;
gdjs.optionsCode.GDquantite_95953Objects1.length = 0;
gdjs.optionsCode.GDquantite_95953Objects2.length = 0;
gdjs.optionsCode.GDquantite_95953Objects3.length = 0;
gdjs.optionsCode.GDquantite_95952Objects1.length = 0;
gdjs.optionsCode.GDquantite_95952Objects2.length = 0;
gdjs.optionsCode.GDquantite_95952Objects3.length = 0;
gdjs.optionsCode.GDbouton_9595plus_95952Objects1.length = 0;
gdjs.optionsCode.GDbouton_9595plus_95952Objects2.length = 0;
gdjs.optionsCode.GDbouton_9595plus_95952Objects3.length = 0;
gdjs.optionsCode.GDbouton_9595plus_9595Objects1.length = 0;
gdjs.optionsCode.GDbouton_9595plus_9595Objects2.length = 0;
gdjs.optionsCode.GDbouton_9595plus_9595Objects3.length = 0;
gdjs.optionsCode.GDbouton_9595moins2Objects1.length = 0;
gdjs.optionsCode.GDbouton_9595moins2Objects2.length = 0;
gdjs.optionsCode.GDbouton_9595moins2Objects3.length = 0;
gdjs.optionsCode.GDbouton_9595moinsObjects1.length = 0;
gdjs.optionsCode.GDbouton_9595moinsObjects2.length = 0;
gdjs.optionsCode.GDbouton_9595moinsObjects3.length = 0;
gdjs.optionsCode.GDtravailler_9595entreObjects1.length = 0;
gdjs.optionsCode.GDtravailler_9595entreObjects2.length = 0;
gdjs.optionsCode.GDtravailler_9595entreObjects3.length = 0;
gdjs.optionsCode.GDnb_9595minObjects1.length = 0;
gdjs.optionsCode.GDnb_9595minObjects2.length = 0;
gdjs.optionsCode.GDnb_9595minObjects3.length = 0;
gdjs.optionsCode.GDetObjects1.length = 0;
gdjs.optionsCode.GDetObjects2.length = 0;
gdjs.optionsCode.GDetObjects3.length = 0;
gdjs.optionsCode.GDnb_9595maxObjects1.length = 0;
gdjs.optionsCode.GDnb_9595maxObjects2.length = 0;
gdjs.optionsCode.GDnb_9595maxObjects3.length = 0;
gdjs.optionsCode.GDbouton_9595retourObjects1.length = 0;
gdjs.optionsCode.GDbouton_9595retourObjects2.length = 0;
gdjs.optionsCode.GDbouton_9595retourObjects3.length = 0;
gdjs.optionsCode.GDbouton_9595optionsObjects1.length = 0;
gdjs.optionsCode.GDbouton_9595optionsObjects2.length = 0;
gdjs.optionsCode.GDbouton_9595optionsObjects3.length = 0;

gdjs.optionsCode.eventsList3(runtimeScene);

return;

}

gdjs['optionsCode'] = gdjs.optionsCode;
