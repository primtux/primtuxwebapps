gdjs.optionsCode = {};
gdjs.optionsCode.GDscore_952Objects1= [];
gdjs.optionsCode.GDscore_952Objects2= [];
gdjs.optionsCode.GDscore_952Objects3= [];
gdjs.optionsCode.GDscore_95Objects1= [];
gdjs.optionsCode.GDscore_95Objects2= [];
gdjs.optionsCode.GDscore_95Objects3= [];
gdjs.optionsCode.GDfondObjects1= [];
gdjs.optionsCode.GDfondObjects2= [];
gdjs.optionsCode.GDfondObjects3= [];
gdjs.optionsCode.GDfond_95blancObjects1= [];
gdjs.optionsCode.GDfond_95blancObjects2= [];
gdjs.optionsCode.GDfond_95blancObjects3= [];
gdjs.optionsCode.GDsandwichObjects1= [];
gdjs.optionsCode.GDsandwichObjects2= [];
gdjs.optionsCode.GDsandwichObjects3= [];
gdjs.optionsCode.GDbol_95soupeObjects1= [];
gdjs.optionsCode.GDbol_95soupeObjects2= [];
gdjs.optionsCode.GDbol_95soupeObjects3= [];
gdjs.optionsCode.GDquantite_95Objects1= [];
gdjs.optionsCode.GDquantite_95Objects2= [];
gdjs.optionsCode.GDquantite_95Objects3= [];
gdjs.optionsCode.GDquantite_953Objects1= [];
gdjs.optionsCode.GDquantite_953Objects2= [];
gdjs.optionsCode.GDquantite_953Objects3= [];
gdjs.optionsCode.GDquantite_952Objects1= [];
gdjs.optionsCode.GDquantite_952Objects2= [];
gdjs.optionsCode.GDquantite_952Objects3= [];
gdjs.optionsCode.GDbouton_95plus_952Objects1= [];
gdjs.optionsCode.GDbouton_95plus_952Objects2= [];
gdjs.optionsCode.GDbouton_95plus_952Objects3= [];
gdjs.optionsCode.GDbouton_95plus_95Objects1= [];
gdjs.optionsCode.GDbouton_95plus_95Objects2= [];
gdjs.optionsCode.GDbouton_95plus_95Objects3= [];
gdjs.optionsCode.GDbouton_95moins2Objects1= [];
gdjs.optionsCode.GDbouton_95moins2Objects2= [];
gdjs.optionsCode.GDbouton_95moins2Objects3= [];
gdjs.optionsCode.GDbouton_95moinsObjects1= [];
gdjs.optionsCode.GDbouton_95moinsObjects2= [];
gdjs.optionsCode.GDbouton_95moinsObjects3= [];
gdjs.optionsCode.GDtravailler_95entreObjects1= [];
gdjs.optionsCode.GDtravailler_95entreObjects2= [];
gdjs.optionsCode.GDtravailler_95entreObjects3= [];
gdjs.optionsCode.GDnb_95minObjects1= [];
gdjs.optionsCode.GDnb_95minObjects2= [];
gdjs.optionsCode.GDnb_95minObjects3= [];
gdjs.optionsCode.GDetObjects1= [];
gdjs.optionsCode.GDetObjects2= [];
gdjs.optionsCode.GDetObjects3= [];
gdjs.optionsCode.GDnb_95maxObjects1= [];
gdjs.optionsCode.GDnb_95maxObjects2= [];
gdjs.optionsCode.GDnb_95maxObjects3= [];
gdjs.optionsCode.GDbouton_95retourObjects1= [];
gdjs.optionsCode.GDbouton_95retourObjects2= [];
gdjs.optionsCode.GDbouton_95retourObjects3= [];
gdjs.optionsCode.GDbouton_95optionsObjects1= [];
gdjs.optionsCode.GDbouton_95optionsObjects2= [];
gdjs.optionsCode.GDbouton_95optionsObjects3= [];

gdjs.optionsCode.conditionTrue_0 = {val:false};
gdjs.optionsCode.condition0IsTrue_0 = {val:false};
gdjs.optionsCode.condition1IsTrue_0 = {val:false};
gdjs.optionsCode.condition2IsTrue_0 = {val:false};
gdjs.optionsCode.condition3IsTrue_0 = {val:false};
gdjs.optionsCode.condition4IsTrue_0 = {val:false};
gdjs.optionsCode.conditionTrue_1 = {val:false};
gdjs.optionsCode.condition0IsTrue_1 = {val:false};
gdjs.optionsCode.condition1IsTrue_1 = {val:false};
gdjs.optionsCode.condition2IsTrue_1 = {val:false};
gdjs.optionsCode.condition3IsTrue_1 = {val:false};
gdjs.optionsCode.condition4IsTrue_1 = {val:false};


gdjs.optionsCode.mapOfGDgdjs_46optionsCode_46GDbouton_9595plus_9595Objects2Objects = Hashtable.newFrom({"bouton_plus_": gdjs.optionsCode.GDbouton_95plus_95Objects2});
gdjs.optionsCode.mapOfGDgdjs_46optionsCode_46GDbouton_9595moinsObjects2Objects = Hashtable.newFrom({"bouton_moins": gdjs.optionsCode.GDbouton_95moinsObjects2});
gdjs.optionsCode.mapOfGDgdjs_46optionsCode_46GDbouton_9595plus_95952Objects2Objects = Hashtable.newFrom({"bouton_plus_2": gdjs.optionsCode.GDbouton_95plus_952Objects2});
gdjs.optionsCode.mapOfGDgdjs_46optionsCode_46GDbouton_9595moins2Objects1Objects = Hashtable.newFrom({"bouton_moins2": gdjs.optionsCode.GDbouton_95moins2Objects1});
gdjs.optionsCode.eventsList0 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("bouton_plus_"), gdjs.optionsCode.GDbouton_95plus_95Objects2);

gdjs.optionsCode.condition0IsTrue_0.val = false;
gdjs.optionsCode.condition1IsTrue_0.val = false;
gdjs.optionsCode.condition2IsTrue_0.val = false;
gdjs.optionsCode.condition3IsTrue_0.val = false;
{
gdjs.optionsCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.optionsCode.mapOfGDgdjs_46optionsCode_46GDbouton_9595plus_9595Objects2Objects, runtimeScene, true, false);
}if ( gdjs.optionsCode.condition0IsTrue_0.val ) {
{
gdjs.optionsCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.optionsCode.condition1IsTrue_0.val ) {
{
{gdjs.optionsCode.conditionTrue_1 = gdjs.optionsCode.condition2IsTrue_0;
gdjs.optionsCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(14217740);
}
}if ( gdjs.optionsCode.condition2IsTrue_0.val ) {
{
gdjs.optionsCode.condition3IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3).getChild("nombre").getChild("mini")) < gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3).getChild("nombre").getChild("maxi")) - 4;
}}
}
}
if (gdjs.optionsCode.condition3IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("nb_min"), gdjs.optionsCode.GDnb_95minObjects2);
{runtimeScene.getGame().getVariables().getFromIndex(3).getChild("nombre").getChild("mini").add(1);
}{for(var i = 0, len = gdjs.optionsCode.GDnb_95minObjects2.length ;i < len;++i) {
    gdjs.optionsCode.GDnb_95minObjects2[i].setString(gdjs.evtTools.common.toString(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3).getChild("nombre").getChild("mini"))));
}
}{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/son_bouton.mp3", 1, false, 50, 1);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_moins"), gdjs.optionsCode.GDbouton_95moinsObjects2);

gdjs.optionsCode.condition0IsTrue_0.val = false;
gdjs.optionsCode.condition1IsTrue_0.val = false;
gdjs.optionsCode.condition2IsTrue_0.val = false;
gdjs.optionsCode.condition3IsTrue_0.val = false;
{
gdjs.optionsCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.optionsCode.mapOfGDgdjs_46optionsCode_46GDbouton_9595moinsObjects2Objects, runtimeScene, true, false);
}if ( gdjs.optionsCode.condition0IsTrue_0.val ) {
{
gdjs.optionsCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.optionsCode.condition1IsTrue_0.val ) {
{
{gdjs.optionsCode.conditionTrue_1 = gdjs.optionsCode.condition2IsTrue_0;
gdjs.optionsCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(14220804);
}
}if ( gdjs.optionsCode.condition2IsTrue_0.val ) {
{
gdjs.optionsCode.condition3IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3).getChild("nombre").getChild("mini")) > 1;
}}
}
}
if (gdjs.optionsCode.condition3IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("nb_min"), gdjs.optionsCode.GDnb_95minObjects2);
{runtimeScene.getGame().getVariables().getFromIndex(3).getChild("nombre").getChild("mini").sub(1);
}{for(var i = 0, len = gdjs.optionsCode.GDnb_95minObjects2.length ;i < len;++i) {
    gdjs.optionsCode.GDnb_95minObjects2[i].setString(gdjs.evtTools.common.toString(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3).getChild("nombre").getChild("mini"))));
}
}{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/son_bouton.mp3", 1, false, 50, 1);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_plus_2"), gdjs.optionsCode.GDbouton_95plus_952Objects2);

gdjs.optionsCode.condition0IsTrue_0.val = false;
gdjs.optionsCode.condition1IsTrue_0.val = false;
gdjs.optionsCode.condition2IsTrue_0.val = false;
gdjs.optionsCode.condition3IsTrue_0.val = false;
{
gdjs.optionsCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.optionsCode.mapOfGDgdjs_46optionsCode_46GDbouton_9595plus_95952Objects2Objects, runtimeScene, true, false);
}if ( gdjs.optionsCode.condition0IsTrue_0.val ) {
{
gdjs.optionsCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.optionsCode.condition1IsTrue_0.val ) {
{
{gdjs.optionsCode.conditionTrue_1 = gdjs.optionsCode.condition2IsTrue_0;
gdjs.optionsCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(14223556);
}
}if ( gdjs.optionsCode.condition2IsTrue_0.val ) {
{
gdjs.optionsCode.condition3IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3).getChild("nombre").getChild("maxi")) < 10;
}}
}
}
if (gdjs.optionsCode.condition3IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("nb_max"), gdjs.optionsCode.GDnb_95maxObjects2);
{runtimeScene.getGame().getVariables().getFromIndex(3).getChild("nombre").getChild("maxi").add(1);
}{for(var i = 0, len = gdjs.optionsCode.GDnb_95maxObjects2.length ;i < len;++i) {
    gdjs.optionsCode.GDnb_95maxObjects2[i].setString(gdjs.evtTools.common.toString(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3).getChild("nombre").getChild("maxi"))));
}
}{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/son_bouton.mp3", 1, false, 50, 1);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_moins2"), gdjs.optionsCode.GDbouton_95moins2Objects1);

gdjs.optionsCode.condition0IsTrue_0.val = false;
gdjs.optionsCode.condition1IsTrue_0.val = false;
gdjs.optionsCode.condition2IsTrue_0.val = false;
gdjs.optionsCode.condition3IsTrue_0.val = false;
{
gdjs.optionsCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.optionsCode.mapOfGDgdjs_46optionsCode_46GDbouton_9595moins2Objects1Objects, runtimeScene, true, false);
}if ( gdjs.optionsCode.condition0IsTrue_0.val ) {
{
gdjs.optionsCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.optionsCode.condition1IsTrue_0.val ) {
{
{gdjs.optionsCode.conditionTrue_1 = gdjs.optionsCode.condition2IsTrue_0;
gdjs.optionsCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(14226300);
}
}if ( gdjs.optionsCode.condition2IsTrue_0.val ) {
{
gdjs.optionsCode.condition3IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3).getChild("nombre").getChild("maxi")) > gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3).getChild("nombre").getChild("mini")) + 4;
}}
}
}
if (gdjs.optionsCode.condition3IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("nb_max"), gdjs.optionsCode.GDnb_95maxObjects1);
{runtimeScene.getGame().getVariables().getFromIndex(3).getChild("nombre").getChild("maxi").sub(1);
}{for(var i = 0, len = gdjs.optionsCode.GDnb_95maxObjects1.length ;i < len;++i) {
    gdjs.optionsCode.GDnb_95maxObjects1[i].setString(gdjs.evtTools.common.toString(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3).getChild("nombre").getChild("maxi"))));
}
}{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/son_bouton.mp3", 1, false, 50, 1);
}}

}


};gdjs.optionsCode.mapOfGDgdjs_46optionsCode_46GDbouton_9595retourObjects1Objects = Hashtable.newFrom({"bouton_retour": gdjs.optionsCode.GDbouton_95retourObjects1});
gdjs.optionsCode.eventsList1 = function(runtimeScene) {

{


{
{gdjs.evtTools.storage.clearJSONFile("sauvegarde");
}{gdjs.evtTools.storage.writeStringInJSONFile("sauvegarde", "reglages", gdjs.evtTools.network.variableStructureToJSON(runtimeScene.getGame().getVariables().getFromIndex(3)));
}}

}


};gdjs.optionsCode.eventsList2 = function(runtimeScene) {

{


gdjs.optionsCode.eventsList1(runtimeScene);
}


{


{
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "menu", false);
}}

}


};gdjs.optionsCode.eventsList3 = function(runtimeScene) {

{


gdjs.optionsCode.condition0IsTrue_0.val = false;
{
gdjs.optionsCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.optionsCode.condition0IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("nb_max"), gdjs.optionsCode.GDnb_95maxObjects1);
gdjs.copyArray(runtimeScene.getObjects("nb_min"), gdjs.optionsCode.GDnb_95minObjects1);
{for(var i = 0, len = gdjs.optionsCode.GDnb_95maxObjects1.length ;i < len;++i) {
    gdjs.optionsCode.GDnb_95maxObjects1[i].setString(gdjs.evtTools.common.toString(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3).getChild("nombre").getChild("maxi"))));
}
}{for(var i = 0, len = gdjs.optionsCode.GDnb_95minObjects1.length ;i < len;++i) {
    gdjs.optionsCode.GDnb_95minObjects1[i].setString(gdjs.evtTools.common.toString(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3).getChild("nombre").getChild("mini"))));
}
}}

}


{


gdjs.optionsCode.eventsList0(runtimeScene);
}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_retour"), gdjs.optionsCode.GDbouton_95retourObjects1);

gdjs.optionsCode.condition0IsTrue_0.val = false;
gdjs.optionsCode.condition1IsTrue_0.val = false;
{
gdjs.optionsCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.optionsCode.mapOfGDgdjs_46optionsCode_46GDbouton_9595retourObjects1Objects, runtimeScene, true, false);
}if ( gdjs.optionsCode.condition0IsTrue_0.val ) {
{
gdjs.optionsCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}}
if (gdjs.optionsCode.condition1IsTrue_0.val) {

{ //Subevents
gdjs.optionsCode.eventsList2(runtimeScene);} //End of subevents
}

}


};

gdjs.optionsCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.optionsCode.GDscore_952Objects1.length = 0;
gdjs.optionsCode.GDscore_952Objects2.length = 0;
gdjs.optionsCode.GDscore_952Objects3.length = 0;
gdjs.optionsCode.GDscore_95Objects1.length = 0;
gdjs.optionsCode.GDscore_95Objects2.length = 0;
gdjs.optionsCode.GDscore_95Objects3.length = 0;
gdjs.optionsCode.GDfondObjects1.length = 0;
gdjs.optionsCode.GDfondObjects2.length = 0;
gdjs.optionsCode.GDfondObjects3.length = 0;
gdjs.optionsCode.GDfond_95blancObjects1.length = 0;
gdjs.optionsCode.GDfond_95blancObjects2.length = 0;
gdjs.optionsCode.GDfond_95blancObjects3.length = 0;
gdjs.optionsCode.GDsandwichObjects1.length = 0;
gdjs.optionsCode.GDsandwichObjects2.length = 0;
gdjs.optionsCode.GDsandwichObjects3.length = 0;
gdjs.optionsCode.GDbol_95soupeObjects1.length = 0;
gdjs.optionsCode.GDbol_95soupeObjects2.length = 0;
gdjs.optionsCode.GDbol_95soupeObjects3.length = 0;
gdjs.optionsCode.GDquantite_95Objects1.length = 0;
gdjs.optionsCode.GDquantite_95Objects2.length = 0;
gdjs.optionsCode.GDquantite_95Objects3.length = 0;
gdjs.optionsCode.GDquantite_953Objects1.length = 0;
gdjs.optionsCode.GDquantite_953Objects2.length = 0;
gdjs.optionsCode.GDquantite_953Objects3.length = 0;
gdjs.optionsCode.GDquantite_952Objects1.length = 0;
gdjs.optionsCode.GDquantite_952Objects2.length = 0;
gdjs.optionsCode.GDquantite_952Objects3.length = 0;
gdjs.optionsCode.GDbouton_95plus_952Objects1.length = 0;
gdjs.optionsCode.GDbouton_95plus_952Objects2.length = 0;
gdjs.optionsCode.GDbouton_95plus_952Objects3.length = 0;
gdjs.optionsCode.GDbouton_95plus_95Objects1.length = 0;
gdjs.optionsCode.GDbouton_95plus_95Objects2.length = 0;
gdjs.optionsCode.GDbouton_95plus_95Objects3.length = 0;
gdjs.optionsCode.GDbouton_95moins2Objects1.length = 0;
gdjs.optionsCode.GDbouton_95moins2Objects2.length = 0;
gdjs.optionsCode.GDbouton_95moins2Objects3.length = 0;
gdjs.optionsCode.GDbouton_95moinsObjects1.length = 0;
gdjs.optionsCode.GDbouton_95moinsObjects2.length = 0;
gdjs.optionsCode.GDbouton_95moinsObjects3.length = 0;
gdjs.optionsCode.GDtravailler_95entreObjects1.length = 0;
gdjs.optionsCode.GDtravailler_95entreObjects2.length = 0;
gdjs.optionsCode.GDtravailler_95entreObjects3.length = 0;
gdjs.optionsCode.GDnb_95minObjects1.length = 0;
gdjs.optionsCode.GDnb_95minObjects2.length = 0;
gdjs.optionsCode.GDnb_95minObjects3.length = 0;
gdjs.optionsCode.GDetObjects1.length = 0;
gdjs.optionsCode.GDetObjects2.length = 0;
gdjs.optionsCode.GDetObjects3.length = 0;
gdjs.optionsCode.GDnb_95maxObjects1.length = 0;
gdjs.optionsCode.GDnb_95maxObjects2.length = 0;
gdjs.optionsCode.GDnb_95maxObjects3.length = 0;
gdjs.optionsCode.GDbouton_95retourObjects1.length = 0;
gdjs.optionsCode.GDbouton_95retourObjects2.length = 0;
gdjs.optionsCode.GDbouton_95retourObjects3.length = 0;
gdjs.optionsCode.GDbouton_95optionsObjects1.length = 0;
gdjs.optionsCode.GDbouton_95optionsObjects2.length = 0;
gdjs.optionsCode.GDbouton_95optionsObjects3.length = 0;

gdjs.optionsCode.eventsList3(runtimeScene);
return;

}

gdjs['optionsCode'] = gdjs.optionsCode;
