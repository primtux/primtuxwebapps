gdjs.codeCode = {};
gdjs.codeCode.GDscore_952Objects1= [];
gdjs.codeCode.GDscore_952Objects2= [];
gdjs.codeCode.GDscore_952Objects3= [];
gdjs.codeCode.GDscore_95Objects1= [];
gdjs.codeCode.GDscore_95Objects2= [];
gdjs.codeCode.GDscore_95Objects3= [];
gdjs.codeCode.GDfondObjects1= [];
gdjs.codeCode.GDfondObjects2= [];
gdjs.codeCode.GDfondObjects3= [];
gdjs.codeCode.GDfond_95blancObjects1= [];
gdjs.codeCode.GDfond_95blancObjects2= [];
gdjs.codeCode.GDfond_95blancObjects3= [];
gdjs.codeCode.GDsandwichObjects1= [];
gdjs.codeCode.GDsandwichObjects2= [];
gdjs.codeCode.GDsandwichObjects3= [];
gdjs.codeCode.GDbol_95soupeObjects1= [];
gdjs.codeCode.GDbol_95soupeObjects2= [];
gdjs.codeCode.GDbol_95soupeObjects3= [];
gdjs.codeCode.GDquantite_95Objects1= [];
gdjs.codeCode.GDquantite_95Objects2= [];
gdjs.codeCode.GDquantite_95Objects3= [];
gdjs.codeCode.GDquantite_953Objects1= [];
gdjs.codeCode.GDquantite_953Objects2= [];
gdjs.codeCode.GDquantite_953Objects3= [];
gdjs.codeCode.GDquantite_952Objects1= [];
gdjs.codeCode.GDquantite_952Objects2= [];
gdjs.codeCode.GDquantite_952Objects3= [];
gdjs.codeCode.GDbouton_95plus_952Objects1= [];
gdjs.codeCode.GDbouton_95plus_952Objects2= [];
gdjs.codeCode.GDbouton_95plus_952Objects3= [];
gdjs.codeCode.GDbouton_95plus_95Objects1= [];
gdjs.codeCode.GDbouton_95plus_95Objects2= [];
gdjs.codeCode.GDbouton_95plus_95Objects3= [];
gdjs.codeCode.GDbouton_95moins2Objects1= [];
gdjs.codeCode.GDbouton_95moins2Objects2= [];
gdjs.codeCode.GDbouton_95moins2Objects3= [];
gdjs.codeCode.GDbouton_95moinsObjects1= [];
gdjs.codeCode.GDbouton_95moinsObjects2= [];
gdjs.codeCode.GDbouton_95moinsObjects3= [];
gdjs.codeCode.GDtravailler_95entreObjects1= [];
gdjs.codeCode.GDtravailler_95entreObjects2= [];
gdjs.codeCode.GDtravailler_95entreObjects3= [];
gdjs.codeCode.GDnb_95minObjects1= [];
gdjs.codeCode.GDnb_95minObjects2= [];
gdjs.codeCode.GDnb_95minObjects3= [];
gdjs.codeCode.GDetObjects1= [];
gdjs.codeCode.GDetObjects2= [];
gdjs.codeCode.GDetObjects3= [];
gdjs.codeCode.GDnb_95maxObjects1= [];
gdjs.codeCode.GDnb_95maxObjects2= [];
gdjs.codeCode.GDnb_95maxObjects3= [];
gdjs.codeCode.GDbouton_95retourObjects1= [];
gdjs.codeCode.GDbouton_95retourObjects2= [];
gdjs.codeCode.GDbouton_95retourObjects3= [];
gdjs.codeCode.GDbouton_95optionsObjects1= [];
gdjs.codeCode.GDbouton_95optionsObjects2= [];
gdjs.codeCode.GDbouton_95optionsObjects3= [];
gdjs.codeCode.GDtexte_95codeObjects1= [];
gdjs.codeCode.GDtexte_95codeObjects2= [];
gdjs.codeCode.GDtexte_95codeObjects3= [];
gdjs.codeCode.GDchoix_95nombre2Objects1= [];
gdjs.codeCode.GDchoix_95nombre2Objects2= [];
gdjs.codeCode.GDchoix_95nombre2Objects3= [];
gdjs.codeCode.GDchoix_95nombre3Objects1= [];
gdjs.codeCode.GDchoix_95nombre3Objects2= [];
gdjs.codeCode.GDchoix_95nombre3Objects3= [];
gdjs.codeCode.GDchoix_95nombre4Objects1= [];
gdjs.codeCode.GDchoix_95nombre4Objects2= [];
gdjs.codeCode.GDchoix_95nombre4Objects3= [];
gdjs.codeCode.GDchoix_95nombre5Objects1= [];
gdjs.codeCode.GDchoix_95nombre5Objects2= [];
gdjs.codeCode.GDchoix_95nombre5Objects3= [];
gdjs.codeCode.GDchoix_95nombre1Objects1= [];
gdjs.codeCode.GDchoix_95nombre1Objects2= [];
gdjs.codeCode.GDchoix_95nombre1Objects3= [];

gdjs.codeCode.conditionTrue_0 = {val:false};
gdjs.codeCode.condition0IsTrue_0 = {val:false};
gdjs.codeCode.condition1IsTrue_0 = {val:false};
gdjs.codeCode.condition2IsTrue_0 = {val:false};
gdjs.codeCode.condition3IsTrue_0 = {val:false};
gdjs.codeCode.conditionTrue_1 = {val:false};
gdjs.codeCode.condition0IsTrue_1 = {val:false};
gdjs.codeCode.condition1IsTrue_1 = {val:false};
gdjs.codeCode.condition2IsTrue_1 = {val:false};
gdjs.codeCode.condition3IsTrue_1 = {val:false};


gdjs.codeCode.mapOfGDgdjs_46codeCode_46GDchoix_9595nombre3Objects1Objects = Hashtable.newFrom({"choix_nombre3": gdjs.codeCode.GDchoix_95nombre3Objects1});
gdjs.codeCode.eventsList0 = function(runtimeScene) {

{


gdjs.codeCode.condition0IsTrue_0.val = false;
gdjs.codeCode.condition1IsTrue_0.val = false;
{
gdjs.codeCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) != 0;
}if ( gdjs.codeCode.condition0IsTrue_0.val ) {
{
{gdjs.codeCode.conditionTrue_1 = gdjs.codeCode.condition1IsTrue_0;
gdjs.codeCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(14197756);
}
}}
if (gdjs.codeCode.condition1IsTrue_0.val) {
{runtimeScene.getVariables().getFromIndex(1).setNumber(0);
}}

}


{


gdjs.codeCode.condition0IsTrue_0.val = false;
gdjs.codeCode.condition1IsTrue_0.val = false;
{
gdjs.codeCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) == 0;
}if ( gdjs.codeCode.condition0IsTrue_0.val ) {
{
{gdjs.codeCode.conditionTrue_1 = gdjs.codeCode.condition1IsTrue_0;
gdjs.codeCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(14198908);
}
}}
if (gdjs.codeCode.condition1IsTrue_0.val) {
{runtimeScene.getVariables().getFromIndex(1).add(1);
}}

}


};gdjs.codeCode.mapOfGDgdjs_46codeCode_46GDchoix_9595nombre5Objects1Objects = Hashtable.newFrom({"choix_nombre5": gdjs.codeCode.GDchoix_95nombre5Objects1});
gdjs.codeCode.eventsList1 = function(runtimeScene) {

{


gdjs.codeCode.condition0IsTrue_0.val = false;
{
gdjs.codeCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) != 1;
}if (gdjs.codeCode.condition0IsTrue_0.val) {
{runtimeScene.getVariables().getFromIndex(1).setNumber(0);
}}

}


{


gdjs.codeCode.condition0IsTrue_0.val = false;
{
gdjs.codeCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) == 1;
}if (gdjs.codeCode.condition0IsTrue_0.val) {
{runtimeScene.getVariables().getFromIndex(1).add(1);
}}

}


};gdjs.codeCode.mapOfGDgdjs_46codeCode_46GDchoix_9595nombre2Objects1Objects = Hashtable.newFrom({"choix_nombre2": gdjs.codeCode.GDchoix_95nombre2Objects1});
gdjs.codeCode.eventsList2 = function(runtimeScene) {

{


gdjs.codeCode.condition0IsTrue_0.val = false;
{
gdjs.codeCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) != 2;
}if (gdjs.codeCode.condition0IsTrue_0.val) {
{runtimeScene.getVariables().getFromIndex(1).setNumber(0);
}}

}


{


gdjs.codeCode.condition0IsTrue_0.val = false;
{
gdjs.codeCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) == 2;
}if (gdjs.codeCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "options", false);
}}

}


};gdjs.codeCode.mapOfGDgdjs_46codeCode_46GDchoix_9595nombre1Objects1ObjectsGDgdjs_46codeCode_46GDchoix_9595nombre4Objects1Objects = Hashtable.newFrom({"choix_nombre1": gdjs.codeCode.GDchoix_95nombre1Objects1, "choix_nombre4": gdjs.codeCode.GDchoix_95nombre4Objects1});
gdjs.codeCode.mapOfGDgdjs_46codeCode_46GDbouton_9595retourObjects1Objects = Hashtable.newFrom({"bouton_retour": gdjs.codeCode.GDbouton_95retourObjects1});
gdjs.codeCode.eventsList3 = function(runtimeScene) {

{


gdjs.codeCode.condition0IsTrue_0.val = false;
{
gdjs.codeCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.codeCode.condition0IsTrue_0.val) {
{runtimeScene.getVariables().getFromIndex(1).setNumber(0);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("choix_nombre3"), gdjs.codeCode.GDchoix_95nombre3Objects1);

gdjs.codeCode.condition0IsTrue_0.val = false;
gdjs.codeCode.condition1IsTrue_0.val = false;
gdjs.codeCode.condition2IsTrue_0.val = false;
{
gdjs.codeCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.codeCode.mapOfGDgdjs_46codeCode_46GDchoix_9595nombre3Objects1Objects, runtimeScene, true, false);
}if ( gdjs.codeCode.condition0IsTrue_0.val ) {
{
gdjs.codeCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.codeCode.condition1IsTrue_0.val ) {
{
{gdjs.codeCode.conditionTrue_1 = gdjs.codeCode.condition2IsTrue_0;
gdjs.codeCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(14197028);
}
}}
}
if (gdjs.codeCode.condition2IsTrue_0.val) {

{ //Subevents
gdjs.codeCode.eventsList0(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("choix_nombre5"), gdjs.codeCode.GDchoix_95nombre5Objects1);

gdjs.codeCode.condition0IsTrue_0.val = false;
gdjs.codeCode.condition1IsTrue_0.val = false;
gdjs.codeCode.condition2IsTrue_0.val = false;
{
gdjs.codeCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.codeCode.mapOfGDgdjs_46codeCode_46GDchoix_9595nombre5Objects1Objects, runtimeScene, true, false);
}if ( gdjs.codeCode.condition0IsTrue_0.val ) {
{
gdjs.codeCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.codeCode.condition1IsTrue_0.val ) {
{
{gdjs.codeCode.conditionTrue_1 = gdjs.codeCode.condition2IsTrue_0;
gdjs.codeCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(14200188);
}
}}
}
if (gdjs.codeCode.condition2IsTrue_0.val) {

{ //Subevents
gdjs.codeCode.eventsList1(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("choix_nombre2"), gdjs.codeCode.GDchoix_95nombre2Objects1);

gdjs.codeCode.condition0IsTrue_0.val = false;
gdjs.codeCode.condition1IsTrue_0.val = false;
gdjs.codeCode.condition2IsTrue_0.val = false;
{
gdjs.codeCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.codeCode.mapOfGDgdjs_46codeCode_46GDchoix_9595nombre2Objects1Objects, runtimeScene, true, false);
}if ( gdjs.codeCode.condition0IsTrue_0.val ) {
{
gdjs.codeCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.codeCode.condition1IsTrue_0.val ) {
{
{gdjs.codeCode.conditionTrue_1 = gdjs.codeCode.condition2IsTrue_0;
gdjs.codeCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(14202612);
}
}}
}
if (gdjs.codeCode.condition2IsTrue_0.val) {

{ //Subevents
gdjs.codeCode.eventsList2(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("choix_nombre1"), gdjs.codeCode.GDchoix_95nombre1Objects1);
gdjs.copyArray(runtimeScene.getObjects("choix_nombre4"), gdjs.codeCode.GDchoix_95nombre4Objects1);

gdjs.codeCode.condition0IsTrue_0.val = false;
gdjs.codeCode.condition1IsTrue_0.val = false;
gdjs.codeCode.condition2IsTrue_0.val = false;
{
gdjs.codeCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.codeCode.mapOfGDgdjs_46codeCode_46GDchoix_9595nombre1Objects1ObjectsGDgdjs_46codeCode_46GDchoix_9595nombre4Objects1Objects, runtimeScene, true, false);
}if ( gdjs.codeCode.condition0IsTrue_0.val ) {
{
gdjs.codeCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.codeCode.condition1IsTrue_0.val ) {
{
{gdjs.codeCode.conditionTrue_1 = gdjs.codeCode.condition2IsTrue_0;
gdjs.codeCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(14205196);
}
}}
}
if (gdjs.codeCode.condition2IsTrue_0.val) {
{runtimeScene.getVariables().getFromIndex(1).setNumber(0);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_retour"), gdjs.codeCode.GDbouton_95retourObjects1);

gdjs.codeCode.condition0IsTrue_0.val = false;
gdjs.codeCode.condition1IsTrue_0.val = false;
{
gdjs.codeCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.codeCode.mapOfGDgdjs_46codeCode_46GDbouton_9595retourObjects1Objects, runtimeScene, true, false);
}if ( gdjs.codeCode.condition0IsTrue_0.val ) {
{
gdjs.codeCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}}
if (gdjs.codeCode.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "menu", false);
}}

}


};

gdjs.codeCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.codeCode.GDscore_952Objects1.length = 0;
gdjs.codeCode.GDscore_952Objects2.length = 0;
gdjs.codeCode.GDscore_952Objects3.length = 0;
gdjs.codeCode.GDscore_95Objects1.length = 0;
gdjs.codeCode.GDscore_95Objects2.length = 0;
gdjs.codeCode.GDscore_95Objects3.length = 0;
gdjs.codeCode.GDfondObjects1.length = 0;
gdjs.codeCode.GDfondObjects2.length = 0;
gdjs.codeCode.GDfondObjects3.length = 0;
gdjs.codeCode.GDfond_95blancObjects1.length = 0;
gdjs.codeCode.GDfond_95blancObjects2.length = 0;
gdjs.codeCode.GDfond_95blancObjects3.length = 0;
gdjs.codeCode.GDsandwichObjects1.length = 0;
gdjs.codeCode.GDsandwichObjects2.length = 0;
gdjs.codeCode.GDsandwichObjects3.length = 0;
gdjs.codeCode.GDbol_95soupeObjects1.length = 0;
gdjs.codeCode.GDbol_95soupeObjects2.length = 0;
gdjs.codeCode.GDbol_95soupeObjects3.length = 0;
gdjs.codeCode.GDquantite_95Objects1.length = 0;
gdjs.codeCode.GDquantite_95Objects2.length = 0;
gdjs.codeCode.GDquantite_95Objects3.length = 0;
gdjs.codeCode.GDquantite_953Objects1.length = 0;
gdjs.codeCode.GDquantite_953Objects2.length = 0;
gdjs.codeCode.GDquantite_953Objects3.length = 0;
gdjs.codeCode.GDquantite_952Objects1.length = 0;
gdjs.codeCode.GDquantite_952Objects2.length = 0;
gdjs.codeCode.GDquantite_952Objects3.length = 0;
gdjs.codeCode.GDbouton_95plus_952Objects1.length = 0;
gdjs.codeCode.GDbouton_95plus_952Objects2.length = 0;
gdjs.codeCode.GDbouton_95plus_952Objects3.length = 0;
gdjs.codeCode.GDbouton_95plus_95Objects1.length = 0;
gdjs.codeCode.GDbouton_95plus_95Objects2.length = 0;
gdjs.codeCode.GDbouton_95plus_95Objects3.length = 0;
gdjs.codeCode.GDbouton_95moins2Objects1.length = 0;
gdjs.codeCode.GDbouton_95moins2Objects2.length = 0;
gdjs.codeCode.GDbouton_95moins2Objects3.length = 0;
gdjs.codeCode.GDbouton_95moinsObjects1.length = 0;
gdjs.codeCode.GDbouton_95moinsObjects2.length = 0;
gdjs.codeCode.GDbouton_95moinsObjects3.length = 0;
gdjs.codeCode.GDtravailler_95entreObjects1.length = 0;
gdjs.codeCode.GDtravailler_95entreObjects2.length = 0;
gdjs.codeCode.GDtravailler_95entreObjects3.length = 0;
gdjs.codeCode.GDnb_95minObjects1.length = 0;
gdjs.codeCode.GDnb_95minObjects2.length = 0;
gdjs.codeCode.GDnb_95minObjects3.length = 0;
gdjs.codeCode.GDetObjects1.length = 0;
gdjs.codeCode.GDetObjects2.length = 0;
gdjs.codeCode.GDetObjects3.length = 0;
gdjs.codeCode.GDnb_95maxObjects1.length = 0;
gdjs.codeCode.GDnb_95maxObjects2.length = 0;
gdjs.codeCode.GDnb_95maxObjects3.length = 0;
gdjs.codeCode.GDbouton_95retourObjects1.length = 0;
gdjs.codeCode.GDbouton_95retourObjects2.length = 0;
gdjs.codeCode.GDbouton_95retourObjects3.length = 0;
gdjs.codeCode.GDbouton_95optionsObjects1.length = 0;
gdjs.codeCode.GDbouton_95optionsObjects2.length = 0;
gdjs.codeCode.GDbouton_95optionsObjects3.length = 0;
gdjs.codeCode.GDtexte_95codeObjects1.length = 0;
gdjs.codeCode.GDtexte_95codeObjects2.length = 0;
gdjs.codeCode.GDtexte_95codeObjects3.length = 0;
gdjs.codeCode.GDchoix_95nombre2Objects1.length = 0;
gdjs.codeCode.GDchoix_95nombre2Objects2.length = 0;
gdjs.codeCode.GDchoix_95nombre2Objects3.length = 0;
gdjs.codeCode.GDchoix_95nombre3Objects1.length = 0;
gdjs.codeCode.GDchoix_95nombre3Objects2.length = 0;
gdjs.codeCode.GDchoix_95nombre3Objects3.length = 0;
gdjs.codeCode.GDchoix_95nombre4Objects1.length = 0;
gdjs.codeCode.GDchoix_95nombre4Objects2.length = 0;
gdjs.codeCode.GDchoix_95nombre4Objects3.length = 0;
gdjs.codeCode.GDchoix_95nombre5Objects1.length = 0;
gdjs.codeCode.GDchoix_95nombre5Objects2.length = 0;
gdjs.codeCode.GDchoix_95nombre5Objects3.length = 0;
gdjs.codeCode.GDchoix_95nombre1Objects1.length = 0;
gdjs.codeCode.GDchoix_95nombre1Objects2.length = 0;
gdjs.codeCode.GDchoix_95nombre1Objects3.length = 0;

gdjs.codeCode.eventsList3(runtimeScene);
return;

}

gdjs['codeCode'] = gdjs.codeCode;
