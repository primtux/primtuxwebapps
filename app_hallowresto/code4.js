gdjs.infosCode = {};
gdjs.infosCode.GDbouton_9595retourObjects1= [];
gdjs.infosCode.GDbouton_9595retourObjects2= [];
gdjs.infosCode.GDtitre2Objects1= [];
gdjs.infosCode.GDtitre2Objects2= [];
gdjs.infosCode.GDtitre1Objects1= [];
gdjs.infosCode.GDtitre1Objects2= [];
gdjs.infosCode.GDinfo5Objects1= [];
gdjs.infosCode.GDinfo5Objects2= [];
gdjs.infosCode.GDinfo4Objects1= [];
gdjs.infosCode.GDinfo4Objects2= [];
gdjs.infosCode.GDinfo6Objects1= [];
gdjs.infosCode.GDinfo6Objects2= [];
gdjs.infosCode.GDinfo3Objects1= [];
gdjs.infosCode.GDinfo3Objects2= [];
gdjs.infosCode.GDinfo2Objects1= [];
gdjs.infosCode.GDinfo2Objects2= [];
gdjs.infosCode.GDinfo1Objects1= [];
gdjs.infosCode.GDinfo1Objects2= [];
gdjs.infosCode.GDcompetenceObjects1= [];
gdjs.infosCode.GDcompetenceObjects2= [];
gdjs.infosCode.GDjeu1Objects1= [];
gdjs.infosCode.GDjeu1Objects2= [];
gdjs.infosCode.GDNewObjectObjects1= [];
gdjs.infosCode.GDNewObjectObjects2= [];
gdjs.infosCode.GDNewObject2Objects1= [];
gdjs.infosCode.GDNewObject2Objects2= [];
gdjs.infosCode.GDjeu2Objects1= [];
gdjs.infosCode.GDjeu2Objects2= [];
gdjs.infosCode.GDserge1Objects1= [];
gdjs.infosCode.GDserge1Objects2= [];
gdjs.infosCode.GDserge2Objects1= [];
gdjs.infosCode.GDserge2Objects2= [];
gdjs.infosCode.GDflecheObjects1= [];
gdjs.infosCode.GDflecheObjects2= [];
gdjs.infosCode.GDnb_95952Objects1= [];
gdjs.infosCode.GDnb_95952Objects2= [];
gdjs.infosCode.GDnb_95953Objects1= [];
gdjs.infosCode.GDnb_95953Objects2= [];
gdjs.infosCode.GDnb_95951Objects1= [];
gdjs.infosCode.GDnb_95951Objects2= [];
gdjs.infosCode.GDintervalleObjects1= [];
gdjs.infosCode.GDintervalleObjects2= [];
gdjs.infosCode.GDscore_95952Objects1= [];
gdjs.infosCode.GDscore_95952Objects2= [];
gdjs.infosCode.GDscore_9595Objects1= [];
gdjs.infosCode.GDscore_9595Objects2= [];
gdjs.infosCode.GDfondObjects1= [];
gdjs.infosCode.GDfondObjects2= [];
gdjs.infosCode.GDfond_9595blancObjects1= [];
gdjs.infosCode.GDfond_9595blancObjects2= [];
gdjs.infosCode.GDsandwichObjects1= [];
gdjs.infosCode.GDsandwichObjects2= [];
gdjs.infosCode.GDbol_9595soupeObjects1= [];
gdjs.infosCode.GDbol_9595soupeObjects2= [];
gdjs.infosCode.GDquantite_9595Objects1= [];
gdjs.infosCode.GDquantite_9595Objects2= [];
gdjs.infosCode.GDquantite_95953Objects1= [];
gdjs.infosCode.GDquantite_95953Objects2= [];
gdjs.infosCode.GDquantite_95952Objects1= [];
gdjs.infosCode.GDquantite_95952Objects2= [];
gdjs.infosCode.GDbouton_9595plus_95952Objects1= [];
gdjs.infosCode.GDbouton_9595plus_95952Objects2= [];
gdjs.infosCode.GDbouton_9595plus_9595Objects1= [];
gdjs.infosCode.GDbouton_9595plus_9595Objects2= [];
gdjs.infosCode.GDbouton_9595moins2Objects1= [];
gdjs.infosCode.GDbouton_9595moins2Objects2= [];
gdjs.infosCode.GDbouton_9595moinsObjects1= [];
gdjs.infosCode.GDbouton_9595moinsObjects2= [];
gdjs.infosCode.GDtravailler_9595entreObjects1= [];
gdjs.infosCode.GDtravailler_9595entreObjects2= [];
gdjs.infosCode.GDnb_9595minObjects1= [];
gdjs.infosCode.GDnb_9595minObjects2= [];
gdjs.infosCode.GDetObjects1= [];
gdjs.infosCode.GDetObjects2= [];
gdjs.infosCode.GDnb_9595maxObjects1= [];
gdjs.infosCode.GDnb_9595maxObjects2= [];
gdjs.infosCode.GDbouton_9595retourObjects1= [];
gdjs.infosCode.GDbouton_9595retourObjects2= [];
gdjs.infosCode.GDbouton_9595optionsObjects1= [];
gdjs.infosCode.GDbouton_9595optionsObjects2= [];


gdjs.infosCode.mapOfGDgdjs_9546infosCode_9546GDbouton_95959595retourObjects1Objects = Hashtable.newFrom({"bouton_retour": gdjs.infosCode.GDbouton_9595retourObjects1});
gdjs.infosCode.eventsList0 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("fond_blanc"), gdjs.infosCode.GDfond_9595blancObjects1);
{for(var i = 0, len = gdjs.infosCode.GDfond_9595blancObjects1.length ;i < len;++i) {
    gdjs.infosCode.GDfond_9595blancObjects1[i].setOpacity(200);
}
}{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/audio_vide.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_retour"), gdjs.infosCode.GDbouton_9595retourObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.infosCode.mapOfGDgdjs_9546infosCode_9546GDbouton_95959595retourObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(13707060);
}
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/son_bouton.mp3", 1, false, 50, 1);
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "menu", false);
}}

}


};

gdjs.infosCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.infosCode.GDbouton_9595retourObjects1.length = 0;
gdjs.infosCode.GDbouton_9595retourObjects2.length = 0;
gdjs.infosCode.GDtitre2Objects1.length = 0;
gdjs.infosCode.GDtitre2Objects2.length = 0;
gdjs.infosCode.GDtitre1Objects1.length = 0;
gdjs.infosCode.GDtitre1Objects2.length = 0;
gdjs.infosCode.GDinfo5Objects1.length = 0;
gdjs.infosCode.GDinfo5Objects2.length = 0;
gdjs.infosCode.GDinfo4Objects1.length = 0;
gdjs.infosCode.GDinfo4Objects2.length = 0;
gdjs.infosCode.GDinfo6Objects1.length = 0;
gdjs.infosCode.GDinfo6Objects2.length = 0;
gdjs.infosCode.GDinfo3Objects1.length = 0;
gdjs.infosCode.GDinfo3Objects2.length = 0;
gdjs.infosCode.GDinfo2Objects1.length = 0;
gdjs.infosCode.GDinfo2Objects2.length = 0;
gdjs.infosCode.GDinfo1Objects1.length = 0;
gdjs.infosCode.GDinfo1Objects2.length = 0;
gdjs.infosCode.GDcompetenceObjects1.length = 0;
gdjs.infosCode.GDcompetenceObjects2.length = 0;
gdjs.infosCode.GDjeu1Objects1.length = 0;
gdjs.infosCode.GDjeu1Objects2.length = 0;
gdjs.infosCode.GDNewObjectObjects1.length = 0;
gdjs.infosCode.GDNewObjectObjects2.length = 0;
gdjs.infosCode.GDNewObject2Objects1.length = 0;
gdjs.infosCode.GDNewObject2Objects2.length = 0;
gdjs.infosCode.GDjeu2Objects1.length = 0;
gdjs.infosCode.GDjeu2Objects2.length = 0;
gdjs.infosCode.GDserge1Objects1.length = 0;
gdjs.infosCode.GDserge1Objects2.length = 0;
gdjs.infosCode.GDserge2Objects1.length = 0;
gdjs.infosCode.GDserge2Objects2.length = 0;
gdjs.infosCode.GDflecheObjects1.length = 0;
gdjs.infosCode.GDflecheObjects2.length = 0;
gdjs.infosCode.GDnb_95952Objects1.length = 0;
gdjs.infosCode.GDnb_95952Objects2.length = 0;
gdjs.infosCode.GDnb_95953Objects1.length = 0;
gdjs.infosCode.GDnb_95953Objects2.length = 0;
gdjs.infosCode.GDnb_95951Objects1.length = 0;
gdjs.infosCode.GDnb_95951Objects2.length = 0;
gdjs.infosCode.GDintervalleObjects1.length = 0;
gdjs.infosCode.GDintervalleObjects2.length = 0;
gdjs.infosCode.GDscore_95952Objects1.length = 0;
gdjs.infosCode.GDscore_95952Objects2.length = 0;
gdjs.infosCode.GDscore_9595Objects1.length = 0;
gdjs.infosCode.GDscore_9595Objects2.length = 0;
gdjs.infosCode.GDfondObjects1.length = 0;
gdjs.infosCode.GDfondObjects2.length = 0;
gdjs.infosCode.GDfond_9595blancObjects1.length = 0;
gdjs.infosCode.GDfond_9595blancObjects2.length = 0;
gdjs.infosCode.GDsandwichObjects1.length = 0;
gdjs.infosCode.GDsandwichObjects2.length = 0;
gdjs.infosCode.GDbol_9595soupeObjects1.length = 0;
gdjs.infosCode.GDbol_9595soupeObjects2.length = 0;
gdjs.infosCode.GDquantite_9595Objects1.length = 0;
gdjs.infosCode.GDquantite_9595Objects2.length = 0;
gdjs.infosCode.GDquantite_95953Objects1.length = 0;
gdjs.infosCode.GDquantite_95953Objects2.length = 0;
gdjs.infosCode.GDquantite_95952Objects1.length = 0;
gdjs.infosCode.GDquantite_95952Objects2.length = 0;
gdjs.infosCode.GDbouton_9595plus_95952Objects1.length = 0;
gdjs.infosCode.GDbouton_9595plus_95952Objects2.length = 0;
gdjs.infosCode.GDbouton_9595plus_9595Objects1.length = 0;
gdjs.infosCode.GDbouton_9595plus_9595Objects2.length = 0;
gdjs.infosCode.GDbouton_9595moins2Objects1.length = 0;
gdjs.infosCode.GDbouton_9595moins2Objects2.length = 0;
gdjs.infosCode.GDbouton_9595moinsObjects1.length = 0;
gdjs.infosCode.GDbouton_9595moinsObjects2.length = 0;
gdjs.infosCode.GDtravailler_9595entreObjects1.length = 0;
gdjs.infosCode.GDtravailler_9595entreObjects2.length = 0;
gdjs.infosCode.GDnb_9595minObjects1.length = 0;
gdjs.infosCode.GDnb_9595minObjects2.length = 0;
gdjs.infosCode.GDetObjects1.length = 0;
gdjs.infosCode.GDetObjects2.length = 0;
gdjs.infosCode.GDnb_9595maxObjects1.length = 0;
gdjs.infosCode.GDnb_9595maxObjects2.length = 0;
gdjs.infosCode.GDbouton_9595retourObjects1.length = 0;
gdjs.infosCode.GDbouton_9595retourObjects2.length = 0;
gdjs.infosCode.GDbouton_9595optionsObjects1.length = 0;
gdjs.infosCode.GDbouton_9595optionsObjects2.length = 0;

gdjs.infosCode.eventsList0(runtimeScene);

return;

}

gdjs['infosCode'] = gdjs.infosCode;
