gdjs.codeCode = {};
gdjs.codeCode.GDtexte_9595codeObjects1= [];
gdjs.codeCode.GDtexte_9595codeObjects2= [];
gdjs.codeCode.GDtexte_9595codeObjects3= [];
gdjs.codeCode.GDchoix_9595nombre2Objects1= [];
gdjs.codeCode.GDchoix_9595nombre2Objects2= [];
gdjs.codeCode.GDchoix_9595nombre2Objects3= [];
gdjs.codeCode.GDchoix_9595nombre3Objects1= [];
gdjs.codeCode.GDchoix_9595nombre3Objects2= [];
gdjs.codeCode.GDchoix_9595nombre3Objects3= [];
gdjs.codeCode.GDchoix_9595nombre4Objects1= [];
gdjs.codeCode.GDchoix_9595nombre4Objects2= [];
gdjs.codeCode.GDchoix_9595nombre4Objects3= [];
gdjs.codeCode.GDchoix_9595nombre5Objects1= [];
gdjs.codeCode.GDchoix_9595nombre5Objects2= [];
gdjs.codeCode.GDchoix_9595nombre5Objects3= [];
gdjs.codeCode.GDchoix_9595nombre1Objects1= [];
gdjs.codeCode.GDchoix_9595nombre1Objects2= [];
gdjs.codeCode.GDchoix_9595nombre1Objects3= [];
gdjs.codeCode.GDscore_95952Objects1= [];
gdjs.codeCode.GDscore_95952Objects2= [];
gdjs.codeCode.GDscore_95952Objects3= [];
gdjs.codeCode.GDscore_9595Objects1= [];
gdjs.codeCode.GDscore_9595Objects2= [];
gdjs.codeCode.GDscore_9595Objects3= [];
gdjs.codeCode.GDfondObjects1= [];
gdjs.codeCode.GDfondObjects2= [];
gdjs.codeCode.GDfondObjects3= [];
gdjs.codeCode.GDfond_9595blancObjects1= [];
gdjs.codeCode.GDfond_9595blancObjects2= [];
gdjs.codeCode.GDfond_9595blancObjects3= [];
gdjs.codeCode.GDsandwichObjects1= [];
gdjs.codeCode.GDsandwichObjects2= [];
gdjs.codeCode.GDsandwichObjects3= [];
gdjs.codeCode.GDbol_9595soupeObjects1= [];
gdjs.codeCode.GDbol_9595soupeObjects2= [];
gdjs.codeCode.GDbol_9595soupeObjects3= [];
gdjs.codeCode.GDquantite_9595Objects1= [];
gdjs.codeCode.GDquantite_9595Objects2= [];
gdjs.codeCode.GDquantite_9595Objects3= [];
gdjs.codeCode.GDquantite_95953Objects1= [];
gdjs.codeCode.GDquantite_95953Objects2= [];
gdjs.codeCode.GDquantite_95953Objects3= [];
gdjs.codeCode.GDquantite_95952Objects1= [];
gdjs.codeCode.GDquantite_95952Objects2= [];
gdjs.codeCode.GDquantite_95952Objects3= [];
gdjs.codeCode.GDbouton_9595plus_95952Objects1= [];
gdjs.codeCode.GDbouton_9595plus_95952Objects2= [];
gdjs.codeCode.GDbouton_9595plus_95952Objects3= [];
gdjs.codeCode.GDbouton_9595plus_9595Objects1= [];
gdjs.codeCode.GDbouton_9595plus_9595Objects2= [];
gdjs.codeCode.GDbouton_9595plus_9595Objects3= [];
gdjs.codeCode.GDbouton_9595moins2Objects1= [];
gdjs.codeCode.GDbouton_9595moins2Objects2= [];
gdjs.codeCode.GDbouton_9595moins2Objects3= [];
gdjs.codeCode.GDbouton_9595moinsObjects1= [];
gdjs.codeCode.GDbouton_9595moinsObjects2= [];
gdjs.codeCode.GDbouton_9595moinsObjects3= [];
gdjs.codeCode.GDtravailler_9595entreObjects1= [];
gdjs.codeCode.GDtravailler_9595entreObjects2= [];
gdjs.codeCode.GDtravailler_9595entreObjects3= [];
gdjs.codeCode.GDnb_9595minObjects1= [];
gdjs.codeCode.GDnb_9595minObjects2= [];
gdjs.codeCode.GDnb_9595minObjects3= [];
gdjs.codeCode.GDetObjects1= [];
gdjs.codeCode.GDetObjects2= [];
gdjs.codeCode.GDetObjects3= [];
gdjs.codeCode.GDnb_9595maxObjects1= [];
gdjs.codeCode.GDnb_9595maxObjects2= [];
gdjs.codeCode.GDnb_9595maxObjects3= [];
gdjs.codeCode.GDbouton_9595retourObjects1= [];
gdjs.codeCode.GDbouton_9595retourObjects2= [];
gdjs.codeCode.GDbouton_9595retourObjects3= [];
gdjs.codeCode.GDbouton_9595optionsObjects1= [];
gdjs.codeCode.GDbouton_9595optionsObjects2= [];
gdjs.codeCode.GDbouton_9595optionsObjects3= [];


gdjs.codeCode.mapOfGDgdjs_9546codeCode_9546GDchoix_95959595nombre3Objects1Objects = Hashtable.newFrom({"choix_nombre3": gdjs.codeCode.GDchoix_9595nombre3Objects1});
gdjs.codeCode.eventsList0 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) != 0;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(13766580);
}
}
if (isConditionTrue_0) {
{runtimeScene.getScene().getVariables().getFromIndex(1).setNumber(0);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 0;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(13767732);
}
}
if (isConditionTrue_0) {
{runtimeScene.getScene().getVariables().getFromIndex(1).add(1);
}}

}


};gdjs.codeCode.mapOfGDgdjs_9546codeCode_9546GDchoix_95959595nombre5Objects1Objects = Hashtable.newFrom({"choix_nombre5": gdjs.codeCode.GDchoix_9595nombre5Objects1});
gdjs.codeCode.eventsList1 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) != 1;
if (isConditionTrue_0) {
{runtimeScene.getScene().getVariables().getFromIndex(1).setNumber(0);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 1;
if (isConditionTrue_0) {
{runtimeScene.getScene().getVariables().getFromIndex(1).add(1);
}}

}


};gdjs.codeCode.mapOfGDgdjs_9546codeCode_9546GDchoix_95959595nombre2Objects1Objects = Hashtable.newFrom({"choix_nombre2": gdjs.codeCode.GDchoix_9595nombre2Objects1});
gdjs.codeCode.eventsList2 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) != 2;
if (isConditionTrue_0) {
{runtimeScene.getScene().getVariables().getFromIndex(1).setNumber(0);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 2;
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "options", false);
}}

}


};gdjs.codeCode.mapOfGDgdjs_9546codeCode_9546GDchoix_95959595nombre1Objects1ObjectsGDgdjs_9546codeCode_9546GDchoix_95959595nombre4Objects1Objects = Hashtable.newFrom({"choix_nombre1": gdjs.codeCode.GDchoix_9595nombre1Objects1, "choix_nombre4": gdjs.codeCode.GDchoix_9595nombre4Objects1});
gdjs.codeCode.mapOfGDgdjs_9546codeCode_9546GDbouton_95959595retourObjects1Objects = Hashtable.newFrom({"bouton_retour": gdjs.codeCode.GDbouton_9595retourObjects1});
gdjs.codeCode.eventsList3 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
{runtimeScene.getScene().getVariables().getFromIndex(1).setNumber(0);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("choix_nombre3"), gdjs.codeCode.GDchoix_9595nombre3Objects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.codeCode.mapOfGDgdjs_9546codeCode_9546GDchoix_95959595nombre3Objects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(13765276);
}
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/son_bouton.mp3", 1, false, 50, 1);
}
{ //Subevents
gdjs.codeCode.eventsList0(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("choix_nombre5"), gdjs.codeCode.GDchoix_9595nombre5Objects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.codeCode.mapOfGDgdjs_9546codeCode_9546GDchoix_95959595nombre5Objects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(13769012);
}
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/son_bouton.mp3", 1, false, 50, 1);
}
{ //Subevents
gdjs.codeCode.eventsList1(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("choix_nombre2"), gdjs.codeCode.GDchoix_9595nombre2Objects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.codeCode.mapOfGDgdjs_9546codeCode_9546GDchoix_95959595nombre2Objects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(13772012);
}
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/son_bouton.mp3", 1, false, 50, 1);
}
{ //Subevents
gdjs.codeCode.eventsList2(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("choix_nombre1"), gdjs.codeCode.GDchoix_9595nombre1Objects1);
gdjs.copyArray(runtimeScene.getObjects("choix_nombre4"), gdjs.codeCode.GDchoix_9595nombre4Objects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.codeCode.mapOfGDgdjs_9546codeCode_9546GDchoix_95959595nombre1Objects1ObjectsGDgdjs_9546codeCode_9546GDchoix_95959595nombre4Objects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(13775172);
}
}
}
if (isConditionTrue_0) {
{runtimeScene.getScene().getVariables().getFromIndex(1).setNumber(0);
}{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/son_bouton.mp3", 1, false, 50, 1);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_retour"), gdjs.codeCode.GDbouton_9595retourObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.codeCode.mapOfGDgdjs_9546codeCode_9546GDbouton_95959595retourObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "menu", false);
}}

}


};

gdjs.codeCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.codeCode.GDtexte_9595codeObjects1.length = 0;
gdjs.codeCode.GDtexte_9595codeObjects2.length = 0;
gdjs.codeCode.GDtexte_9595codeObjects3.length = 0;
gdjs.codeCode.GDchoix_9595nombre2Objects1.length = 0;
gdjs.codeCode.GDchoix_9595nombre2Objects2.length = 0;
gdjs.codeCode.GDchoix_9595nombre2Objects3.length = 0;
gdjs.codeCode.GDchoix_9595nombre3Objects1.length = 0;
gdjs.codeCode.GDchoix_9595nombre3Objects2.length = 0;
gdjs.codeCode.GDchoix_9595nombre3Objects3.length = 0;
gdjs.codeCode.GDchoix_9595nombre4Objects1.length = 0;
gdjs.codeCode.GDchoix_9595nombre4Objects2.length = 0;
gdjs.codeCode.GDchoix_9595nombre4Objects3.length = 0;
gdjs.codeCode.GDchoix_9595nombre5Objects1.length = 0;
gdjs.codeCode.GDchoix_9595nombre5Objects2.length = 0;
gdjs.codeCode.GDchoix_9595nombre5Objects3.length = 0;
gdjs.codeCode.GDchoix_9595nombre1Objects1.length = 0;
gdjs.codeCode.GDchoix_9595nombre1Objects2.length = 0;
gdjs.codeCode.GDchoix_9595nombre1Objects3.length = 0;
gdjs.codeCode.GDscore_95952Objects1.length = 0;
gdjs.codeCode.GDscore_95952Objects2.length = 0;
gdjs.codeCode.GDscore_95952Objects3.length = 0;
gdjs.codeCode.GDscore_9595Objects1.length = 0;
gdjs.codeCode.GDscore_9595Objects2.length = 0;
gdjs.codeCode.GDscore_9595Objects3.length = 0;
gdjs.codeCode.GDfondObjects1.length = 0;
gdjs.codeCode.GDfondObjects2.length = 0;
gdjs.codeCode.GDfondObjects3.length = 0;
gdjs.codeCode.GDfond_9595blancObjects1.length = 0;
gdjs.codeCode.GDfond_9595blancObjects2.length = 0;
gdjs.codeCode.GDfond_9595blancObjects3.length = 0;
gdjs.codeCode.GDsandwichObjects1.length = 0;
gdjs.codeCode.GDsandwichObjects2.length = 0;
gdjs.codeCode.GDsandwichObjects3.length = 0;
gdjs.codeCode.GDbol_9595soupeObjects1.length = 0;
gdjs.codeCode.GDbol_9595soupeObjects2.length = 0;
gdjs.codeCode.GDbol_9595soupeObjects3.length = 0;
gdjs.codeCode.GDquantite_9595Objects1.length = 0;
gdjs.codeCode.GDquantite_9595Objects2.length = 0;
gdjs.codeCode.GDquantite_9595Objects3.length = 0;
gdjs.codeCode.GDquantite_95953Objects1.length = 0;
gdjs.codeCode.GDquantite_95953Objects2.length = 0;
gdjs.codeCode.GDquantite_95953Objects3.length = 0;
gdjs.codeCode.GDquantite_95952Objects1.length = 0;
gdjs.codeCode.GDquantite_95952Objects2.length = 0;
gdjs.codeCode.GDquantite_95952Objects3.length = 0;
gdjs.codeCode.GDbouton_9595plus_95952Objects1.length = 0;
gdjs.codeCode.GDbouton_9595plus_95952Objects2.length = 0;
gdjs.codeCode.GDbouton_9595plus_95952Objects3.length = 0;
gdjs.codeCode.GDbouton_9595plus_9595Objects1.length = 0;
gdjs.codeCode.GDbouton_9595plus_9595Objects2.length = 0;
gdjs.codeCode.GDbouton_9595plus_9595Objects3.length = 0;
gdjs.codeCode.GDbouton_9595moins2Objects1.length = 0;
gdjs.codeCode.GDbouton_9595moins2Objects2.length = 0;
gdjs.codeCode.GDbouton_9595moins2Objects3.length = 0;
gdjs.codeCode.GDbouton_9595moinsObjects1.length = 0;
gdjs.codeCode.GDbouton_9595moinsObjects2.length = 0;
gdjs.codeCode.GDbouton_9595moinsObjects3.length = 0;
gdjs.codeCode.GDtravailler_9595entreObjects1.length = 0;
gdjs.codeCode.GDtravailler_9595entreObjects2.length = 0;
gdjs.codeCode.GDtravailler_9595entreObjects3.length = 0;
gdjs.codeCode.GDnb_9595minObjects1.length = 0;
gdjs.codeCode.GDnb_9595minObjects2.length = 0;
gdjs.codeCode.GDnb_9595minObjects3.length = 0;
gdjs.codeCode.GDetObjects1.length = 0;
gdjs.codeCode.GDetObjects2.length = 0;
gdjs.codeCode.GDetObjects3.length = 0;
gdjs.codeCode.GDnb_9595maxObjects1.length = 0;
gdjs.codeCode.GDnb_9595maxObjects2.length = 0;
gdjs.codeCode.GDnb_9595maxObjects3.length = 0;
gdjs.codeCode.GDbouton_9595retourObjects1.length = 0;
gdjs.codeCode.GDbouton_9595retourObjects2.length = 0;
gdjs.codeCode.GDbouton_9595retourObjects3.length = 0;
gdjs.codeCode.GDbouton_9595optionsObjects1.length = 0;
gdjs.codeCode.GDbouton_9595optionsObjects2.length = 0;
gdjs.codeCode.GDbouton_9595optionsObjects3.length = 0;

gdjs.codeCode.eventsList3(runtimeScene);

return;

}

gdjs['codeCode'] = gdjs.codeCode;
