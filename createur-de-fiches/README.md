## Utilisation

Une instance est déployée sur https://professeurherve.forge.apps.education.fr/createur-de-fiches

## Licence
Cette application est sous licence  [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.fr)

Elle a été créée par chatGPT, sur des prompts de Hervé ALLESANT, 
puis mise en forme et en ligne avec l'aide d'Arnaud CHAMPOLLION.

