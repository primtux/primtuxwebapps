gdjs.decouvreCode = {};
gdjs.decouvreCode.GDcercle_9595droitObjects1= [];
gdjs.decouvreCode.GDcercle_9595droitObjects2= [];
gdjs.decouvreCode.GDcercle_9595droitObjects3= [];
gdjs.decouvreCode.GDcercle_9595droitObjects4= [];
gdjs.decouvreCode.GDcercle_9595gaucheObjects1= [];
gdjs.decouvreCode.GDcercle_9595gaucheObjects2= [];
gdjs.decouvreCode.GDcercle_9595gaucheObjects3= [];
gdjs.decouvreCode.GDcercle_9595gaucheObjects4= [];
gdjs.decouvreCode.GDsprite_9595syllabe1Objects1= [];
gdjs.decouvreCode.GDsprite_9595syllabe1Objects2= [];
gdjs.decouvreCode.GDsprite_9595syllabe1Objects3= [];
gdjs.decouvreCode.GDsprite_9595syllabe1Objects4= [];
gdjs.decouvreCode.GDsprite_9595syllabe2Objects1= [];
gdjs.decouvreCode.GDsprite_9595syllabe2Objects2= [];
gdjs.decouvreCode.GDsprite_9595syllabe2Objects3= [];
gdjs.decouvreCode.GDsprite_9595syllabe2Objects4= [];
gdjs.decouvreCode.GDsprite_9595minusculesObjects1= [];
gdjs.decouvreCode.GDsprite_9595minusculesObjects2= [];
gdjs.decouvreCode.GDsprite_9595minusculesObjects3= [];
gdjs.decouvreCode.GDsprite_9595minusculesObjects4= [];
gdjs.decouvreCode.GDsprite_9595majusculesObjects1= [];
gdjs.decouvreCode.GDsprite_9595majusculesObjects2= [];
gdjs.decouvreCode.GDsprite_9595majusculesObjects3= [];
gdjs.decouvreCode.GDsprite_9595majusculesObjects4= [];
gdjs.decouvreCode.GDsprite_9595bouton_9595majusculesObjects1= [];
gdjs.decouvreCode.GDsprite_9595bouton_9595majusculesObjects2= [];
gdjs.decouvreCode.GDsprite_9595bouton_9595majusculesObjects3= [];
gdjs.decouvreCode.GDsprite_9595bouton_9595majusculesObjects4= [];
gdjs.decouvreCode.GDsprite_9595bouton_9595minusculesObjects1= [];
gdjs.decouvreCode.GDsprite_9595bouton_9595minusculesObjects2= [];
gdjs.decouvreCode.GDsprite_9595bouton_9595minusculesObjects3= [];
gdjs.decouvreCode.GDsprite_9595bouton_9595minusculesObjects4= [];
gdjs.decouvreCode.GDsprite_9595bouton_9595imagesObjects1= [];
gdjs.decouvreCode.GDsprite_9595bouton_9595imagesObjects2= [];
gdjs.decouvreCode.GDsprite_9595bouton_9595imagesObjects3= [];
gdjs.decouvreCode.GDsprite_9595bouton_9595imagesObjects4= [];
gdjs.decouvreCode.GDsprite_9595imagesObjects1= [];
gdjs.decouvreCode.GDsprite_9595imagesObjects2= [];
gdjs.decouvreCode.GDsprite_9595imagesObjects3= [];
gdjs.decouvreCode.GDsprite_9595imagesObjects4= [];
gdjs.decouvreCode.GDsprite_9595oreilleObjects1= [];
gdjs.decouvreCode.GDsprite_9595oreilleObjects2= [];
gdjs.decouvreCode.GDsprite_9595oreilleObjects3= [];
gdjs.decouvreCode.GDsprite_9595oreilleObjects4= [];
gdjs.decouvreCode.GDcercle_9595orangeObjects1= [];
gdjs.decouvreCode.GDcercle_9595orangeObjects2= [];
gdjs.decouvreCode.GDcercle_9595orangeObjects3= [];
gdjs.decouvreCode.GDcercle_9595orangeObjects4= [];
gdjs.decouvreCode.GDcercle_9595Objects1= [];
gdjs.decouvreCode.GDcercle_9595Objects2= [];
gdjs.decouvreCode.GDcercle_9595Objects3= [];
gdjs.decouvreCode.GDcercle_9595Objects4= [];
gdjs.decouvreCode.GDsprite_9595retourObjects1= [];
gdjs.decouvreCode.GDsprite_9595retourObjects2= [];
gdjs.decouvreCode.GDsprite_9595retourObjects3= [];
gdjs.decouvreCode.GDsprite_9595retourObjects4= [];
gdjs.decouvreCode.GDfond_9595blancObjects1= [];
gdjs.decouvreCode.GDfond_9595blancObjects2= [];
gdjs.decouvreCode.GDfond_9595blancObjects3= [];
gdjs.decouvreCode.GDfond_9595blancObjects4= [];
gdjs.decouvreCode.GDcase_9595minusculesObjects1= [];
gdjs.decouvreCode.GDcase_9595minusculesObjects2= [];
gdjs.decouvreCode.GDcase_9595minusculesObjects3= [];
gdjs.decouvreCode.GDcase_9595minusculesObjects4= [];
gdjs.decouvreCode.GDcase_9595majusculesObjects1= [];
gdjs.decouvreCode.GDcase_9595majusculesObjects2= [];
gdjs.decouvreCode.GDcase_9595majusculesObjects3= [];
gdjs.decouvreCode.GDcase_9595majusculesObjects4= [];
gdjs.decouvreCode.GDtexte_9595majusculesObjects1= [];
gdjs.decouvreCode.GDtexte_9595majusculesObjects2= [];
gdjs.decouvreCode.GDtexte_9595majusculesObjects3= [];
gdjs.decouvreCode.GDtexte_9595majusculesObjects4= [];
gdjs.decouvreCode.GDtexte_9595minusculesObjects1= [];
gdjs.decouvreCode.GDtexte_9595minusculesObjects2= [];
gdjs.decouvreCode.GDtexte_9595minusculesObjects3= [];
gdjs.decouvreCode.GDtexte_9595minusculesObjects4= [];
gdjs.decouvreCode.GDtexte_9595choisir_9595motsObjects1= [];
gdjs.decouvreCode.GDtexte_9595choisir_9595motsObjects2= [];
gdjs.decouvreCode.GDtexte_9595choisir_9595motsObjects3= [];
gdjs.decouvreCode.GDtexte_9595choisir_9595motsObjects4= [];
gdjs.decouvreCode.GDsprite_9595baguetteObjects1= [];
gdjs.decouvreCode.GDsprite_9595baguetteObjects2= [];
gdjs.decouvreCode.GDsprite_9595baguetteObjects3= [];
gdjs.decouvreCode.GDsprite_9595baguetteObjects4= [];
gdjs.decouvreCode.GDcredit_9595imagesObjects1= [];
gdjs.decouvreCode.GDcredit_9595imagesObjects2= [];
gdjs.decouvreCode.GDcredit_9595imagesObjects3= [];
gdjs.decouvreCode.GDcredit_9595imagesObjects4= [];
gdjs.decouvreCode.GDtexte_9595auteurObjects1= [];
gdjs.decouvreCode.GDtexte_9595auteurObjects2= [];
gdjs.decouvreCode.GDtexte_9595auteurObjects3= [];
gdjs.decouvreCode.GDtexte_9595auteurObjects4= [];
gdjs.decouvreCode.GDsprite_9595serge_9595lamaObjects1= [];
gdjs.decouvreCode.GDsprite_9595serge_9595lamaObjects2= [];
gdjs.decouvreCode.GDsprite_9595serge_9595lamaObjects3= [];
gdjs.decouvreCode.GDsprite_9595serge_9595lamaObjects4= [];


gdjs.decouvreCode.mapOfGDgdjs_9546decouvreCode_9546GDsprite_95959595bouton_95959595majusculesObjects2ObjectsGDgdjs_9546decouvreCode_9546GDsprite_95959595bouton_95959595minusculesObjects2ObjectsGDgdjs_9546decouvreCode_9546GDsprite_95959595bouton_95959595imagesObjects2Objects = Hashtable.newFrom({"sprite_bouton_majuscules": gdjs.decouvreCode.GDsprite_9595bouton_9595majusculesObjects2, "sprite_bouton_minuscules": gdjs.decouvreCode.GDsprite_9595bouton_9595minusculesObjects2, "sprite_bouton_images": gdjs.decouvreCode.GDsprite_9595bouton_9595imagesObjects2});
gdjs.decouvreCode.eventsList0 = function(runtimeScene) {

{

gdjs.copyArray(gdjs.decouvreCode.GDsprite_9595bouton_9595imagesObjects2, gdjs.decouvreCode.GDsprite_9595bouton_9595imagesObjects3);

gdjs.copyArray(gdjs.decouvreCode.GDsprite_9595bouton_9595majusculesObjects2, gdjs.decouvreCode.GDsprite_9595bouton_9595majusculesObjects3);

gdjs.copyArray(gdjs.decouvreCode.GDsprite_9595bouton_9595minusculesObjects2, gdjs.decouvreCode.GDsprite_9595bouton_9595minusculesObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.decouvreCode.GDsprite_9595bouton_9595majusculesObjects3.length;i<l;++i) {
    if ( gdjs.decouvreCode.GDsprite_9595bouton_9595majusculesObjects3[i].getAnimation() == 0 ) {
        isConditionTrue_0 = true;
        gdjs.decouvreCode.GDsprite_9595bouton_9595majusculesObjects3[k] = gdjs.decouvreCode.GDsprite_9595bouton_9595majusculesObjects3[i];
        ++k;
    }
}
gdjs.decouvreCode.GDsprite_9595bouton_9595majusculesObjects3.length = k;
for (var i = 0, k = 0, l = gdjs.decouvreCode.GDsprite_9595bouton_9595minusculesObjects3.length;i<l;++i) {
    if ( gdjs.decouvreCode.GDsprite_9595bouton_9595minusculesObjects3[i].getAnimation() == 0 ) {
        isConditionTrue_0 = true;
        gdjs.decouvreCode.GDsprite_9595bouton_9595minusculesObjects3[k] = gdjs.decouvreCode.GDsprite_9595bouton_9595minusculesObjects3[i];
        ++k;
    }
}
gdjs.decouvreCode.GDsprite_9595bouton_9595minusculesObjects3.length = k;
for (var i = 0, k = 0, l = gdjs.decouvreCode.GDsprite_9595bouton_9595imagesObjects3.length;i<l;++i) {
    if ( gdjs.decouvreCode.GDsprite_9595bouton_9595imagesObjects3[i].getAnimation() == 0 ) {
        isConditionTrue_0 = true;
        gdjs.decouvreCode.GDsprite_9595bouton_9595imagesObjects3[k] = gdjs.decouvreCode.GDsprite_9595bouton_9595imagesObjects3[i];
        ++k;
    }
}
gdjs.decouvreCode.GDsprite_9595bouton_9595imagesObjects3.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.decouvreCode.GDsprite_9595bouton_9595imagesObjects3 */
/* Reuse gdjs.decouvreCode.GDsprite_9595bouton_9595majusculesObjects3 */
/* Reuse gdjs.decouvreCode.GDsprite_9595bouton_9595minusculesObjects3 */
{for(var i = 0, len = gdjs.decouvreCode.GDsprite_9595bouton_9595majusculesObjects3.length ;i < len;++i) {
    gdjs.decouvreCode.GDsprite_9595bouton_9595majusculesObjects3[i].setAnimation(1);
}
for(var i = 0, len = gdjs.decouvreCode.GDsprite_9595bouton_9595minusculesObjects3.length ;i < len;++i) {
    gdjs.decouvreCode.GDsprite_9595bouton_9595minusculesObjects3[i].setAnimation(1);
}
for(var i = 0, len = gdjs.decouvreCode.GDsprite_9595bouton_9595imagesObjects3.length ;i < len;++i) {
    gdjs.decouvreCode.GDsprite_9595bouton_9595imagesObjects3[i].setAnimation(1);
}
}{runtimeScene.getScene().getVariables().getFromIndex(0).setNumber(0);
}}

}


{

/* Reuse gdjs.decouvreCode.GDsprite_9595bouton_9595imagesObjects2 */
/* Reuse gdjs.decouvreCode.GDsprite_9595bouton_9595majusculesObjects2 */
/* Reuse gdjs.decouvreCode.GDsprite_9595bouton_9595minusculesObjects2 */

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.decouvreCode.GDsprite_9595bouton_9595majusculesObjects2.length;i<l;++i) {
    if ( gdjs.decouvreCode.GDsprite_9595bouton_9595majusculesObjects2[i].getAnimation() == 1 ) {
        isConditionTrue_0 = true;
        gdjs.decouvreCode.GDsprite_9595bouton_9595majusculesObjects2[k] = gdjs.decouvreCode.GDsprite_9595bouton_9595majusculesObjects2[i];
        ++k;
    }
}
gdjs.decouvreCode.GDsprite_9595bouton_9595majusculesObjects2.length = k;
for (var i = 0, k = 0, l = gdjs.decouvreCode.GDsprite_9595bouton_9595minusculesObjects2.length;i<l;++i) {
    if ( gdjs.decouvreCode.GDsprite_9595bouton_9595minusculesObjects2[i].getAnimation() == 1 ) {
        isConditionTrue_0 = true;
        gdjs.decouvreCode.GDsprite_9595bouton_9595minusculesObjects2[k] = gdjs.decouvreCode.GDsprite_9595bouton_9595minusculesObjects2[i];
        ++k;
    }
}
gdjs.decouvreCode.GDsprite_9595bouton_9595minusculesObjects2.length = k;
for (var i = 0, k = 0, l = gdjs.decouvreCode.GDsprite_9595bouton_9595imagesObjects2.length;i<l;++i) {
    if ( gdjs.decouvreCode.GDsprite_9595bouton_9595imagesObjects2[i].getAnimation() == 1 ) {
        isConditionTrue_0 = true;
        gdjs.decouvreCode.GDsprite_9595bouton_9595imagesObjects2[k] = gdjs.decouvreCode.GDsprite_9595bouton_9595imagesObjects2[i];
        ++k;
    }
}
gdjs.decouvreCode.GDsprite_9595bouton_9595imagesObjects2.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 1;
}
if (isConditionTrue_0) {
/* Reuse gdjs.decouvreCode.GDsprite_9595bouton_9595imagesObjects2 */
/* Reuse gdjs.decouvreCode.GDsprite_9595bouton_9595majusculesObjects2 */
/* Reuse gdjs.decouvreCode.GDsprite_9595bouton_9595minusculesObjects2 */
{for(var i = 0, len = gdjs.decouvreCode.GDsprite_9595bouton_9595majusculesObjects2.length ;i < len;++i) {
    gdjs.decouvreCode.GDsprite_9595bouton_9595majusculesObjects2[i].setAnimation(0);
}
for(var i = 0, len = gdjs.decouvreCode.GDsprite_9595bouton_9595minusculesObjects2.length ;i < len;++i) {
    gdjs.decouvreCode.GDsprite_9595bouton_9595minusculesObjects2[i].setAnimation(0);
}
for(var i = 0, len = gdjs.decouvreCode.GDsprite_9595bouton_9595imagesObjects2.length ;i < len;++i) {
    gdjs.decouvreCode.GDsprite_9595bouton_9595imagesObjects2[i].setAnimation(0);
}
}{runtimeScene.getScene().getVariables().getFromIndex(0).setNumber(0);
}}

}


};gdjs.decouvreCode.eventsList1 = function(runtimeScene) {

{



}


{

gdjs.copyArray(runtimeScene.getObjects("sprite_bouton_images"), gdjs.decouvreCode.GDsprite_9595bouton_9595imagesObjects2);
gdjs.copyArray(runtimeScene.getObjects("sprite_bouton_majuscules"), gdjs.decouvreCode.GDsprite_9595bouton_9595majusculesObjects2);
gdjs.copyArray(runtimeScene.getObjects("sprite_bouton_minuscules"), gdjs.decouvreCode.GDsprite_9595bouton_9595minusculesObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.decouvreCode.mapOfGDgdjs_9546decouvreCode_9546GDsprite_95959595bouton_95959595majusculesObjects2ObjectsGDgdjs_9546decouvreCode_9546GDsprite_95959595bouton_95959595minusculesObjects2ObjectsGDgdjs_9546decouvreCode_9546GDsprite_95959595bouton_95959595imagesObjects2Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(13436100);
}
}
}
if (isConditionTrue_0) {
{runtimeScene.getScene().getVariables().getFromIndex(0).setNumber(1);
}
{ //Subevents
gdjs.decouvreCode.eventsList0(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("sprite_bouton_majuscules"), gdjs.decouvreCode.GDsprite_9595bouton_9595majusculesObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.decouvreCode.GDsprite_9595bouton_9595majusculesObjects2.length;i<l;++i) {
    if ( gdjs.decouvreCode.GDsprite_9595bouton_9595majusculesObjects2[i].getAnimation() == 0 ) {
        isConditionTrue_0 = true;
        gdjs.decouvreCode.GDsprite_9595bouton_9595majusculesObjects2[k] = gdjs.decouvreCode.GDsprite_9595bouton_9595majusculesObjects2[i];
        ++k;
    }
}
gdjs.decouvreCode.GDsprite_9595bouton_9595majusculesObjects2.length = k;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(2).getChild("majuscules").setNumber(0);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("sprite_bouton_majuscules"), gdjs.decouvreCode.GDsprite_9595bouton_9595majusculesObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.decouvreCode.GDsprite_9595bouton_9595majusculesObjects2.length;i<l;++i) {
    if ( gdjs.decouvreCode.GDsprite_9595bouton_9595majusculesObjects2[i].getAnimation() == 1 ) {
        isConditionTrue_0 = true;
        gdjs.decouvreCode.GDsprite_9595bouton_9595majusculesObjects2[k] = gdjs.decouvreCode.GDsprite_9595bouton_9595majusculesObjects2[i];
        ++k;
    }
}
gdjs.decouvreCode.GDsprite_9595bouton_9595majusculesObjects2.length = k;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(2).getChild("majuscules").setNumber(1);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("sprite_bouton_minuscules"), gdjs.decouvreCode.GDsprite_9595bouton_9595minusculesObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.decouvreCode.GDsprite_9595bouton_9595minusculesObjects2.length;i<l;++i) {
    if ( gdjs.decouvreCode.GDsprite_9595bouton_9595minusculesObjects2[i].getAnimation() == 0 ) {
        isConditionTrue_0 = true;
        gdjs.decouvreCode.GDsprite_9595bouton_9595minusculesObjects2[k] = gdjs.decouvreCode.GDsprite_9595bouton_9595minusculesObjects2[i];
        ++k;
    }
}
gdjs.decouvreCode.GDsprite_9595bouton_9595minusculesObjects2.length = k;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(2).getChild("minuscules").setNumber(0);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("sprite_bouton_minuscules"), gdjs.decouvreCode.GDsprite_9595bouton_9595minusculesObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.decouvreCode.GDsprite_9595bouton_9595minusculesObjects2.length;i<l;++i) {
    if ( gdjs.decouvreCode.GDsprite_9595bouton_9595minusculesObjects2[i].getAnimation() == 1 ) {
        isConditionTrue_0 = true;
        gdjs.decouvreCode.GDsprite_9595bouton_9595minusculesObjects2[k] = gdjs.decouvreCode.GDsprite_9595bouton_9595minusculesObjects2[i];
        ++k;
    }
}
gdjs.decouvreCode.GDsprite_9595bouton_9595minusculesObjects2.length = k;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(2).getChild("minuscules").setNumber(1);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("sprite_bouton_images"), gdjs.decouvreCode.GDsprite_9595bouton_9595imagesObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.decouvreCode.GDsprite_9595bouton_9595imagesObjects2.length;i<l;++i) {
    if ( gdjs.decouvreCode.GDsprite_9595bouton_9595imagesObjects2[i].getAnimation() == 0 ) {
        isConditionTrue_0 = true;
        gdjs.decouvreCode.GDsprite_9595bouton_9595imagesObjects2[k] = gdjs.decouvreCode.GDsprite_9595bouton_9595imagesObjects2[i];
        ++k;
    }
}
gdjs.decouvreCode.GDsprite_9595bouton_9595imagesObjects2.length = k;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(2).getChild("images").setNumber(0);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("sprite_bouton_images"), gdjs.decouvreCode.GDsprite_9595bouton_9595imagesObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.decouvreCode.GDsprite_9595bouton_9595imagesObjects1.length;i<l;++i) {
    if ( gdjs.decouvreCode.GDsprite_9595bouton_9595imagesObjects1[i].getAnimation() == 1 ) {
        isConditionTrue_0 = true;
        gdjs.decouvreCode.GDsprite_9595bouton_9595imagesObjects1[k] = gdjs.decouvreCode.GDsprite_9595bouton_9595imagesObjects1[i];
        ++k;
    }
}
gdjs.decouvreCode.GDsprite_9595bouton_9595imagesObjects1.length = k;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(2).getChild("images").setNumber(1);
}}

}


};gdjs.decouvreCode.mapOfGDgdjs_9546decouvreCode_9546GDsprite_95959595retourObjects1Objects = Hashtable.newFrom({"sprite_retour": gdjs.decouvreCode.GDsprite_9595retourObjects1});
gdjs.decouvreCode.eventsList2 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("sprite_retour"), gdjs.decouvreCode.GDsprite_9595retourObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.decouvreCode.mapOfGDgdjs_9546decouvreCode_9546GDsprite_95959595retourObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "menu", true);
}}

}


};gdjs.decouvreCode.eventsList3 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("sprite_syllabe1"), gdjs.decouvreCode.GDsprite_9595syllabe1Objects2);
gdjs.copyArray(runtimeScene.getObjects("sprite_syllabe2"), gdjs.decouvreCode.GDsprite_9595syllabe2Objects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.decouvreCode.GDsprite_9595syllabe1Objects2.length;i<l;++i) {
    if ( gdjs.decouvreCode.GDsprite_9595syllabe1Objects2[i].getAnimation() == 1 ) {
        isConditionTrue_0 = true;
        gdjs.decouvreCode.GDsprite_9595syllabe1Objects2[k] = gdjs.decouvreCode.GDsprite_9595syllabe1Objects2[i];
        ++k;
    }
}
gdjs.decouvreCode.GDsprite_9595syllabe1Objects2.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.decouvreCode.GDsprite_9595syllabe2Objects2.length;i<l;++i) {
    if ( gdjs.decouvreCode.GDsprite_9595syllabe2Objects2[i].getAnimation() == 0 ) {
        isConditionTrue_0 = true;
        gdjs.decouvreCode.GDsprite_9595syllabe2Objects2[k] = gdjs.decouvreCode.GDsprite_9595syllabe2Objects2[i];
        ++k;
    }
}
gdjs.decouvreCode.GDsprite_9595syllabe2Objects2.length = k;
}
if (isConditionTrue_0) {
/* Reuse gdjs.decouvreCode.GDsprite_9595syllabe2Objects2 */
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe2").add(1);
}{for(var i = 0, len = gdjs.decouvreCode.GDsprite_9595syllabe2Objects2.length ;i < len;++i) {
    gdjs.decouvreCode.GDsprite_9595syllabe2Objects2[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe2")));
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("sprite_syllabe1"), gdjs.decouvreCode.GDsprite_9595syllabe1Objects1);
gdjs.copyArray(runtimeScene.getObjects("sprite_syllabe2"), gdjs.decouvreCode.GDsprite_9595syllabe2Objects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.decouvreCode.GDsprite_9595syllabe1Objects1.length;i<l;++i) {
    if ( gdjs.decouvreCode.GDsprite_9595syllabe1Objects1[i].getAnimation() == 1 ) {
        isConditionTrue_0 = true;
        gdjs.decouvreCode.GDsprite_9595syllabe1Objects1[k] = gdjs.decouvreCode.GDsprite_9595syllabe1Objects1[i];
        ++k;
    }
}
gdjs.decouvreCode.GDsprite_9595syllabe1Objects1.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.decouvreCode.GDsprite_9595syllabe2Objects1.length;i<l;++i) {
    if ( gdjs.decouvreCode.GDsprite_9595syllabe2Objects1[i].getAnimation() == 0 ) {
        isConditionTrue_0 = true;
        gdjs.decouvreCode.GDsprite_9595syllabe2Objects1[k] = gdjs.decouvreCode.GDsprite_9595syllabe2Objects1[i];
        ++k;
    }
}
gdjs.decouvreCode.GDsprite_9595syllabe2Objects1.length = k;
}
if (isConditionTrue_0) {
/* Reuse gdjs.decouvreCode.GDsprite_9595syllabe2Objects1 */
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe2").add(1);
}{for(var i = 0, len = gdjs.decouvreCode.GDsprite_9595syllabe2Objects1.length ;i < len;++i) {
    gdjs.decouvreCode.GDsprite_9595syllabe2Objects1[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe2")));
}
}}

}


};gdjs.decouvreCode.mapOfGDgdjs_9546decouvreCode_9546GDcercle_95959595orangeObjects2Objects = Hashtable.newFrom({"cercle_orange": gdjs.decouvreCode.GDcercle_9595orangeObjects2});
gdjs.decouvreCode.eventsList4 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("cercle_orange"), gdjs.decouvreCode.GDcercle_9595orangeObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.decouvreCode.mapOfGDgdjs_9546decouvreCode_9546GDcercle_95959595orangeObjects2Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("sprite_syllabe1"), gdjs.decouvreCode.GDsprite_9595syllabe1Objects2);
gdjs.copyArray(runtimeScene.getObjects("sprite_syllabe2"), gdjs.decouvreCode.GDsprite_9595syllabe2Objects2);
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe1").add(1);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe2").add(1);
}{for(var i = 0, len = gdjs.decouvreCode.GDsprite_9595syllabe1Objects2.length ;i < len;++i) {
    gdjs.decouvreCode.GDsprite_9595syllabe1Objects2[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe1")));
}
}{for(var i = 0, len = gdjs.decouvreCode.GDsprite_9595syllabe2Objects2.length ;i < len;++i) {
    gdjs.decouvreCode.GDsprite_9595syllabe2Objects2[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe2")));
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe1")) == 24;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("sprite_syllabe1"), gdjs.decouvreCode.GDsprite_9595syllabe1Objects2);
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe1").setNumber(0);
}{for(var i = 0, len = gdjs.decouvreCode.GDsprite_9595syllabe1Objects2.length ;i < len;++i) {
    gdjs.decouvreCode.GDsprite_9595syllabe1Objects2[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe1")));
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe2")) == 24;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("sprite_syllabe2"), gdjs.decouvreCode.GDsprite_9595syllabe2Objects1);
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe2").setNumber(0);
}{for(var i = 0, len = gdjs.decouvreCode.GDsprite_9595syllabe2Objects1.length ;i < len;++i) {
    gdjs.decouvreCode.GDsprite_9595syllabe2Objects1[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe2")));
}
}}

}


};gdjs.decouvreCode.eventsList5 = function(runtimeScene) {

{



}


};gdjs.decouvreCode.eventsList6 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe1")) == 0;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("sprite_syllabe1"), gdjs.decouvreCode.GDsprite_9595syllabe1Objects3);
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe1").add(1);
}{for(var i = 0, len = gdjs.decouvreCode.GDsprite_9595syllabe1Objects3.length ;i < len;++i) {
    gdjs.decouvreCode.GDsprite_9595syllabe1Objects3[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe1")));
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe2")) == 0;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("sprite_syllabe2"), gdjs.decouvreCode.GDsprite_9595syllabe2Objects2);
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe2").add(1);
}{for(var i = 0, len = gdjs.decouvreCode.GDsprite_9595syllabe2Objects2.length ;i < len;++i) {
    gdjs.decouvreCode.GDsprite_9595syllabe2Objects2[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe2")));
}
}}

}


};gdjs.decouvreCode.eventsList7 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe1")) == 1;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("sprite_syllabe1"), gdjs.decouvreCode.GDsprite_9595syllabe1Objects3);
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe1").add(1);
}{for(var i = 0, len = gdjs.decouvreCode.GDsprite_9595syllabe1Objects3.length ;i < len;++i) {
    gdjs.decouvreCode.GDsprite_9595syllabe1Objects3[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe1")));
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe2")) == 1;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("sprite_syllabe2"), gdjs.decouvreCode.GDsprite_9595syllabe2Objects2);
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe2").add(1);
}{for(var i = 0, len = gdjs.decouvreCode.GDsprite_9595syllabe2Objects2.length ;i < len;++i) {
    gdjs.decouvreCode.GDsprite_9595syllabe2Objects2[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe2")));
}
}}

}


};gdjs.decouvreCode.eventsList8 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe1")) == 2;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("sprite_syllabe1"), gdjs.decouvreCode.GDsprite_9595syllabe1Objects3);
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe1").add(1);
}{for(var i = 0, len = gdjs.decouvreCode.GDsprite_9595syllabe1Objects3.length ;i < len;++i) {
    gdjs.decouvreCode.GDsprite_9595syllabe1Objects3[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe1")));
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe2")) == 2;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("sprite_syllabe2"), gdjs.decouvreCode.GDsprite_9595syllabe2Objects2);
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe2").add(1);
}{for(var i = 0, len = gdjs.decouvreCode.GDsprite_9595syllabe2Objects2.length ;i < len;++i) {
    gdjs.decouvreCode.GDsprite_9595syllabe2Objects2[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe2")));
}
}}

}


};gdjs.decouvreCode.eventsList9 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe1")) == 3;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("sprite_syllabe1"), gdjs.decouvreCode.GDsprite_9595syllabe1Objects3);
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe1").add(1);
}{for(var i = 0, len = gdjs.decouvreCode.GDsprite_9595syllabe1Objects3.length ;i < len;++i) {
    gdjs.decouvreCode.GDsprite_9595syllabe1Objects3[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe1")));
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe2")) == 3;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("sprite_syllabe2"), gdjs.decouvreCode.GDsprite_9595syllabe2Objects2);
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe2").add(1);
}{for(var i = 0, len = gdjs.decouvreCode.GDsprite_9595syllabe2Objects2.length ;i < len;++i) {
    gdjs.decouvreCode.GDsprite_9595syllabe2Objects2[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe2")));
}
}}

}


};gdjs.decouvreCode.eventsList10 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe1")) == 4;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("sprite_syllabe1"), gdjs.decouvreCode.GDsprite_9595syllabe1Objects3);
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe1").add(1);
}{for(var i = 0, len = gdjs.decouvreCode.GDsprite_9595syllabe1Objects3.length ;i < len;++i) {
    gdjs.decouvreCode.GDsprite_9595syllabe1Objects3[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe1")));
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe2")) == 4;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("sprite_syllabe2"), gdjs.decouvreCode.GDsprite_9595syllabe2Objects2);
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe2").add(1);
}{for(var i = 0, len = gdjs.decouvreCode.GDsprite_9595syllabe2Objects2.length ;i < len;++i) {
    gdjs.decouvreCode.GDsprite_9595syllabe2Objects2[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe2")));
}
}}

}


};gdjs.decouvreCode.eventsList11 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe1")) == 5;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("sprite_syllabe1"), gdjs.decouvreCode.GDsprite_9595syllabe1Objects3);
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe1").add(1);
}{for(var i = 0, len = gdjs.decouvreCode.GDsprite_9595syllabe1Objects3.length ;i < len;++i) {
    gdjs.decouvreCode.GDsprite_9595syllabe1Objects3[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe1")));
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe2")) == 5;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("sprite_syllabe2"), gdjs.decouvreCode.GDsprite_9595syllabe2Objects2);
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe2").add(1);
}{for(var i = 0, len = gdjs.decouvreCode.GDsprite_9595syllabe2Objects2.length ;i < len;++i) {
    gdjs.decouvreCode.GDsprite_9595syllabe2Objects2[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe2")));
}
}}

}


};gdjs.decouvreCode.eventsList12 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe1")) == 6;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("sprite_syllabe1"), gdjs.decouvreCode.GDsprite_9595syllabe1Objects3);
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe1").add(1);
}{for(var i = 0, len = gdjs.decouvreCode.GDsprite_9595syllabe1Objects3.length ;i < len;++i) {
    gdjs.decouvreCode.GDsprite_9595syllabe1Objects3[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe1")));
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe2")) == 6;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("sprite_syllabe2"), gdjs.decouvreCode.GDsprite_9595syllabe2Objects2);
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe2").add(1);
}{for(var i = 0, len = gdjs.decouvreCode.GDsprite_9595syllabe2Objects2.length ;i < len;++i) {
    gdjs.decouvreCode.GDsprite_9595syllabe2Objects2[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe2")));
}
}}

}


};gdjs.decouvreCode.eventsList13 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe1")) == 7;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("sprite_syllabe1"), gdjs.decouvreCode.GDsprite_9595syllabe1Objects3);
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe1").add(1);
}{for(var i = 0, len = gdjs.decouvreCode.GDsprite_9595syllabe1Objects3.length ;i < len;++i) {
    gdjs.decouvreCode.GDsprite_9595syllabe1Objects3[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe1")));
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe2")) == 7;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("sprite_syllabe2"), gdjs.decouvreCode.GDsprite_9595syllabe2Objects2);
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe2").add(1);
}{for(var i = 0, len = gdjs.decouvreCode.GDsprite_9595syllabe2Objects2.length ;i < len;++i) {
    gdjs.decouvreCode.GDsprite_9595syllabe2Objects2[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe2")));
}
}}

}


};gdjs.decouvreCode.eventsList14 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe1")) == 8;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("sprite_syllabe1"), gdjs.decouvreCode.GDsprite_9595syllabe1Objects3);
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe1").add(1);
}{for(var i = 0, len = gdjs.decouvreCode.GDsprite_9595syllabe1Objects3.length ;i < len;++i) {
    gdjs.decouvreCode.GDsprite_9595syllabe1Objects3[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe1")));
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe2")) == 8;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("sprite_syllabe2"), gdjs.decouvreCode.GDsprite_9595syllabe2Objects2);
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe2").add(1);
}{for(var i = 0, len = gdjs.decouvreCode.GDsprite_9595syllabe2Objects2.length ;i < len;++i) {
    gdjs.decouvreCode.GDsprite_9595syllabe2Objects2[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe2")));
}
}}

}


};gdjs.decouvreCode.eventsList15 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe1")) == 9;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("sprite_syllabe1"), gdjs.decouvreCode.GDsprite_9595syllabe1Objects3);
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe1").add(1);
}{for(var i = 0, len = gdjs.decouvreCode.GDsprite_9595syllabe1Objects3.length ;i < len;++i) {
    gdjs.decouvreCode.GDsprite_9595syllabe1Objects3[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe1")));
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe2")) == 9;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("sprite_syllabe2"), gdjs.decouvreCode.GDsprite_9595syllabe2Objects2);
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe2").add(1);
}{for(var i = 0, len = gdjs.decouvreCode.GDsprite_9595syllabe2Objects2.length ;i < len;++i) {
    gdjs.decouvreCode.GDsprite_9595syllabe2Objects2[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe2")));
}
}}

}


};gdjs.decouvreCode.eventsList16 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe1")) == 10;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("sprite_syllabe1"), gdjs.decouvreCode.GDsprite_9595syllabe1Objects3);
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe1").add(1);
}{for(var i = 0, len = gdjs.decouvreCode.GDsprite_9595syllabe1Objects3.length ;i < len;++i) {
    gdjs.decouvreCode.GDsprite_9595syllabe1Objects3[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe1")));
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe2")) == 10;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("sprite_syllabe2"), gdjs.decouvreCode.GDsprite_9595syllabe2Objects2);
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe2").add(1);
}{for(var i = 0, len = gdjs.decouvreCode.GDsprite_9595syllabe2Objects2.length ;i < len;++i) {
    gdjs.decouvreCode.GDsprite_9595syllabe2Objects2[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe2")));
}
}}

}


};gdjs.decouvreCode.eventsList17 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe1")) == 11;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("sprite_syllabe1"), gdjs.decouvreCode.GDsprite_9595syllabe1Objects3);
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe1").add(1);
}{for(var i = 0, len = gdjs.decouvreCode.GDsprite_9595syllabe1Objects3.length ;i < len;++i) {
    gdjs.decouvreCode.GDsprite_9595syllabe1Objects3[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe1")));
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe2")) == 11;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("sprite_syllabe2"), gdjs.decouvreCode.GDsprite_9595syllabe2Objects2);
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe2").add(1);
}{for(var i = 0, len = gdjs.decouvreCode.GDsprite_9595syllabe2Objects2.length ;i < len;++i) {
    gdjs.decouvreCode.GDsprite_9595syllabe2Objects2[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe2")));
}
}}

}


};gdjs.decouvreCode.eventsList18 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe1")) == 12;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("sprite_syllabe1"), gdjs.decouvreCode.GDsprite_9595syllabe1Objects3);
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe1").add(1);
}{for(var i = 0, len = gdjs.decouvreCode.GDsprite_9595syllabe1Objects3.length ;i < len;++i) {
    gdjs.decouvreCode.GDsprite_9595syllabe1Objects3[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe1")));
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe2")) == 12;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("sprite_syllabe2"), gdjs.decouvreCode.GDsprite_9595syllabe2Objects2);
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe2").add(1);
}{for(var i = 0, len = gdjs.decouvreCode.GDsprite_9595syllabe2Objects2.length ;i < len;++i) {
    gdjs.decouvreCode.GDsprite_9595syllabe2Objects2[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe2")));
}
}}

}


};gdjs.decouvreCode.eventsList19 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe1")) == 13;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("sprite_syllabe1"), gdjs.decouvreCode.GDsprite_9595syllabe1Objects3);
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe1").add(1);
}{for(var i = 0, len = gdjs.decouvreCode.GDsprite_9595syllabe1Objects3.length ;i < len;++i) {
    gdjs.decouvreCode.GDsprite_9595syllabe1Objects3[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe1")));
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe2")) == 13;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("sprite_syllabe2"), gdjs.decouvreCode.GDsprite_9595syllabe2Objects2);
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe2").add(1);
}{for(var i = 0, len = gdjs.decouvreCode.GDsprite_9595syllabe2Objects2.length ;i < len;++i) {
    gdjs.decouvreCode.GDsprite_9595syllabe2Objects2[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe2")));
}
}}

}


};gdjs.decouvreCode.eventsList20 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe1")) == 14;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("sprite_syllabe1"), gdjs.decouvreCode.GDsprite_9595syllabe1Objects3);
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe1").add(1);
}{for(var i = 0, len = gdjs.decouvreCode.GDsprite_9595syllabe1Objects3.length ;i < len;++i) {
    gdjs.decouvreCode.GDsprite_9595syllabe1Objects3[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe1")));
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe2")) == 14;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("sprite_syllabe2"), gdjs.decouvreCode.GDsprite_9595syllabe2Objects2);
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe2").add(1);
}{for(var i = 0, len = gdjs.decouvreCode.GDsprite_9595syllabe2Objects2.length ;i < len;++i) {
    gdjs.decouvreCode.GDsprite_9595syllabe2Objects2[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe2")));
}
}}

}


};gdjs.decouvreCode.eventsList21 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe1")) == 15;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("sprite_syllabe1"), gdjs.decouvreCode.GDsprite_9595syllabe1Objects3);
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe1").add(1);
}{for(var i = 0, len = gdjs.decouvreCode.GDsprite_9595syllabe1Objects3.length ;i < len;++i) {
    gdjs.decouvreCode.GDsprite_9595syllabe1Objects3[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe1")));
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe2")) == 15;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("sprite_syllabe2"), gdjs.decouvreCode.GDsprite_9595syllabe2Objects2);
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe2").add(1);
}{for(var i = 0, len = gdjs.decouvreCode.GDsprite_9595syllabe2Objects2.length ;i < len;++i) {
    gdjs.decouvreCode.GDsprite_9595syllabe2Objects2[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe2")));
}
}}

}


};gdjs.decouvreCode.eventsList22 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe1")) == 16;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("sprite_syllabe1"), gdjs.decouvreCode.GDsprite_9595syllabe1Objects3);
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe1").add(1);
}{for(var i = 0, len = gdjs.decouvreCode.GDsprite_9595syllabe1Objects3.length ;i < len;++i) {
    gdjs.decouvreCode.GDsprite_9595syllabe1Objects3[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe1")));
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe2")) == 16;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("sprite_syllabe2"), gdjs.decouvreCode.GDsprite_9595syllabe2Objects2);
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe2").add(1);
}{for(var i = 0, len = gdjs.decouvreCode.GDsprite_9595syllabe2Objects2.length ;i < len;++i) {
    gdjs.decouvreCode.GDsprite_9595syllabe2Objects2[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe2")));
}
}}

}


};gdjs.decouvreCode.eventsList23 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe1")) == 17;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("sprite_syllabe1"), gdjs.decouvreCode.GDsprite_9595syllabe1Objects3);
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe1").add(1);
}{for(var i = 0, len = gdjs.decouvreCode.GDsprite_9595syllabe1Objects3.length ;i < len;++i) {
    gdjs.decouvreCode.GDsprite_9595syllabe1Objects3[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe1")));
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe2")) == 17;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("sprite_syllabe2"), gdjs.decouvreCode.GDsprite_9595syllabe2Objects2);
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe2").add(1);
}{for(var i = 0, len = gdjs.decouvreCode.GDsprite_9595syllabe2Objects2.length ;i < len;++i) {
    gdjs.decouvreCode.GDsprite_9595syllabe2Objects2[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe2")));
}
}}

}


};gdjs.decouvreCode.eventsList24 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe1")) == 18;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("sprite_syllabe1"), gdjs.decouvreCode.GDsprite_9595syllabe1Objects3);
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe1").add(1);
}{for(var i = 0, len = gdjs.decouvreCode.GDsprite_9595syllabe1Objects3.length ;i < len;++i) {
    gdjs.decouvreCode.GDsprite_9595syllabe1Objects3[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe1")));
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe2")) == 18;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("sprite_syllabe2"), gdjs.decouvreCode.GDsprite_9595syllabe2Objects2);
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe2").add(1);
}{for(var i = 0, len = gdjs.decouvreCode.GDsprite_9595syllabe2Objects2.length ;i < len;++i) {
    gdjs.decouvreCode.GDsprite_9595syllabe2Objects2[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe2")));
}
}}

}


};gdjs.decouvreCode.eventsList25 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe1")) == 19;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("sprite_syllabe1"), gdjs.decouvreCode.GDsprite_9595syllabe1Objects3);
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe1").add(1);
}{for(var i = 0, len = gdjs.decouvreCode.GDsprite_9595syllabe1Objects3.length ;i < len;++i) {
    gdjs.decouvreCode.GDsprite_9595syllabe1Objects3[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe1")));
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe2")) == 19;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("sprite_syllabe2"), gdjs.decouvreCode.GDsprite_9595syllabe2Objects2);
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe2").add(1);
}{for(var i = 0, len = gdjs.decouvreCode.GDsprite_9595syllabe2Objects2.length ;i < len;++i) {
    gdjs.decouvreCode.GDsprite_9595syllabe2Objects2[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe2")));
}
}}

}


};gdjs.decouvreCode.eventsList26 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe1")) == 20;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("sprite_syllabe1"), gdjs.decouvreCode.GDsprite_9595syllabe1Objects3);
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe1").add(1);
}{for(var i = 0, len = gdjs.decouvreCode.GDsprite_9595syllabe1Objects3.length ;i < len;++i) {
    gdjs.decouvreCode.GDsprite_9595syllabe1Objects3[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe1")));
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe2")) == 20;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("sprite_syllabe2"), gdjs.decouvreCode.GDsprite_9595syllabe2Objects2);
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe2").add(1);
}{for(var i = 0, len = gdjs.decouvreCode.GDsprite_9595syllabe2Objects2.length ;i < len;++i) {
    gdjs.decouvreCode.GDsprite_9595syllabe2Objects2[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe2")));
}
}}

}


};gdjs.decouvreCode.eventsList27 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe1")) == 21;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("sprite_syllabe1"), gdjs.decouvreCode.GDsprite_9595syllabe1Objects3);
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe1").add(1);
}{for(var i = 0, len = gdjs.decouvreCode.GDsprite_9595syllabe1Objects3.length ;i < len;++i) {
    gdjs.decouvreCode.GDsprite_9595syllabe1Objects3[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe1")));
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe2")) == 21;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("sprite_syllabe2"), gdjs.decouvreCode.GDsprite_9595syllabe2Objects2);
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe2").add(1);
}{for(var i = 0, len = gdjs.decouvreCode.GDsprite_9595syllabe2Objects2.length ;i < len;++i) {
    gdjs.decouvreCode.GDsprite_9595syllabe2Objects2[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe2")));
}
}}

}


};gdjs.decouvreCode.eventsList28 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe1")) == 22;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("sprite_syllabe1"), gdjs.decouvreCode.GDsprite_9595syllabe1Objects3);
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe1").add(1);
}{for(var i = 0, len = gdjs.decouvreCode.GDsprite_9595syllabe1Objects3.length ;i < len;++i) {
    gdjs.decouvreCode.GDsprite_9595syllabe1Objects3[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe1")));
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe2")) == 22;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("sprite_syllabe2"), gdjs.decouvreCode.GDsprite_9595syllabe2Objects2);
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe2").add(1);
}{for(var i = 0, len = gdjs.decouvreCode.GDsprite_9595syllabe2Objects2.length ;i < len;++i) {
    gdjs.decouvreCode.GDsprite_9595syllabe2Objects2[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe2")));
}
}}

}


};gdjs.decouvreCode.eventsList29 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe1")) == 23;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("sprite_syllabe1"), gdjs.decouvreCode.GDsprite_9595syllabe1Objects2);
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe1").add(1);
}{for(var i = 0, len = gdjs.decouvreCode.GDsprite_9595syllabe1Objects2.length ;i < len;++i) {
    gdjs.decouvreCode.GDsprite_9595syllabe1Objects2[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe1")));
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe2")) == 23;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("sprite_syllabe2"), gdjs.decouvreCode.GDsprite_9595syllabe2Objects1);
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe2").add(1);
}{for(var i = 0, len = gdjs.decouvreCode.GDsprite_9595syllabe2Objects1.length ;i < len;++i) {
    gdjs.decouvreCode.GDsprite_9595syllabe2Objects1[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe2")));
}
}}

}


};gdjs.decouvreCode.eventsList30 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("case_canard")) == 1;
if (isConditionTrue_0) {

{ //Subevents
gdjs.decouvreCode.eventsList6(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("case_cochon")) == 1;
if (isConditionTrue_0) {

{ //Subevents
gdjs.decouvreCode.eventsList7(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("case_chameau")) == 1;
if (isConditionTrue_0) {

{ //Subevents
gdjs.decouvreCode.eventsList8(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("case_fourmi")) == 1;
if (isConditionTrue_0) {

{ //Subevents
gdjs.decouvreCode.eventsList9(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("case_lapin")) == 1;
if (isConditionTrue_0) {

{ //Subevents
gdjs.decouvreCode.eventsList10(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("case_mouton")) == 1;
if (isConditionTrue_0) {

{ //Subevents
gdjs.decouvreCode.eventsList11(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("case_souris")) == 1;
if (isConditionTrue_0) {

{ //Subevents
gdjs.decouvreCode.eventsList12(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("case_tortue")) == 1;
if (isConditionTrue_0) {

{ //Subevents
gdjs.decouvreCode.eventsList13(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("case_hibou")) == 1;
if (isConditionTrue_0) {

{ //Subevents
gdjs.decouvreCode.eventsList14(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("case_pigeon")) == 1;
if (isConditionTrue_0) {

{ //Subevents
gdjs.decouvreCode.eventsList15(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("case_tatou")) == 1;
if (isConditionTrue_0) {

{ //Subevents
gdjs.decouvreCode.eventsList16(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("case_flaman")) == 1;
if (isConditionTrue_0) {

{ //Subevents
gdjs.decouvreCode.eventsList17(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("case_pingouin")) == 1;
if (isConditionTrue_0) {

{ //Subevents
gdjs.decouvreCode.eventsList18(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("case_dauphin")) == 1;
if (isConditionTrue_0) {

{ //Subevents
gdjs.decouvreCode.eventsList19(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("case_lezard")) == 1;
if (isConditionTrue_0) {

{ //Subevents
gdjs.decouvreCode.eventsList20(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("case_cheval")) == 1;
if (isConditionTrue_0) {

{ //Subevents
gdjs.decouvreCode.eventsList21(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("case_zebu")) == 1;
if (isConditionTrue_0) {

{ //Subevents
gdjs.decouvreCode.eventsList22(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("case_panda")) == 1;
if (isConditionTrue_0) {

{ //Subevents
gdjs.decouvreCode.eventsList23(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("case_dindon")) == 1;
if (isConditionTrue_0) {

{ //Subevents
gdjs.decouvreCode.eventsList24(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("case_requin")) == 1;
if (isConditionTrue_0) {

{ //Subevents
gdjs.decouvreCode.eventsList25(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("case_corbeau")) == 1;
if (isConditionTrue_0) {

{ //Subevents
gdjs.decouvreCode.eventsList26(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("case_homard")) == 1;
if (isConditionTrue_0) {

{ //Subevents
gdjs.decouvreCode.eventsList27(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("case_blaireau")) == 1;
if (isConditionTrue_0) {

{ //Subevents
gdjs.decouvreCode.eventsList28(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("case_lama")) == 1;
if (isConditionTrue_0) {

{ //Subevents
gdjs.decouvreCode.eventsList29(runtimeScene);} //End of subevents
}

}


};gdjs.decouvreCode.eventsList31 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2).getChild("majuscules")) == 1;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("sprite_majuscules"), gdjs.decouvreCode.GDsprite_9595majusculesObjects2);
{for(var i = 0, len = gdjs.decouvreCode.GDsprite_9595majusculesObjects2.length ;i < len;++i) {
    gdjs.decouvreCode.GDsprite_9595majusculesObjects2[i].setAnimation(0);
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2).getChild("majuscules")) == 0;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("sprite_majuscules"), gdjs.decouvreCode.GDsprite_9595majusculesObjects2);
{for(var i = 0, len = gdjs.decouvreCode.GDsprite_9595majusculesObjects2.length ;i < len;++i) {
    gdjs.decouvreCode.GDsprite_9595majusculesObjects2[i].setAnimation(1);
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2).getChild("minuscules")) == 1;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("sprite_minuscules"), gdjs.decouvreCode.GDsprite_9595minusculesObjects2);
{for(var i = 0, len = gdjs.decouvreCode.GDsprite_9595minusculesObjects2.length ;i < len;++i) {
    gdjs.decouvreCode.GDsprite_9595minusculesObjects2[i].setAnimation(0);
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2).getChild("minuscules")) == 0;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("sprite_minuscules"), gdjs.decouvreCode.GDsprite_9595minusculesObjects2);
{for(var i = 0, len = gdjs.decouvreCode.GDsprite_9595minusculesObjects2.length ;i < len;++i) {
    gdjs.decouvreCode.GDsprite_9595minusculesObjects2[i].setAnimation(1);
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2).getChild("images")) == 0;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("sprite_images"), gdjs.decouvreCode.GDsprite_9595imagesObjects2);
{for(var i = 0, len = gdjs.decouvreCode.GDsprite_9595imagesObjects2.length ;i < len;++i) {
    gdjs.decouvreCode.GDsprite_9595imagesObjects2[i].setAnimation(0);
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2).getChild("images")) == 1;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("sprite_images"), gdjs.decouvreCode.GDsprite_9595imagesObjects1);
{for(var i = 0, len = gdjs.decouvreCode.GDsprite_9595imagesObjects1.length ;i < len;++i) {
    gdjs.decouvreCode.GDsprite_9595imagesObjects1[i].setAnimation(1);
}
}}

}


};gdjs.decouvreCode.mapOfGDgdjs_9546decouvreCode_9546GDsprite_95959595oreilleObjects2Objects = Hashtable.newFrom({"sprite_oreille": gdjs.decouvreCode.GDsprite_9595oreilleObjects2});
gdjs.decouvreCode.eventsList32 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
}

}


};gdjs.decouvreCode.eventsList33 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("sprite_oreille"), gdjs.decouvreCode.GDsprite_9595oreilleObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.decouvreCode.mapOfGDgdjs_9546decouvreCode_9546GDsprite_95959595oreilleObjects2Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(11125868);
}
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("sprite_syllabe1"), gdjs.decouvreCode.GDsprite_9595syllabe1Objects2);
gdjs.copyArray(runtimeScene.getObjects("sprite_syllabe2"), gdjs.decouvreCode.GDsprite_9595syllabe2Objects2);
{runtimeScene.getScene().getVariables().getFromIndex(1).setNumber((( gdjs.decouvreCode.GDsprite_9595syllabe1Objects2.length === 0 ) ? 0 :gdjs.decouvreCode.GDsprite_9595syllabe1Objects2[0].getAnimation()));
}{runtimeScene.getScene().getVariables().getFromIndex(2).setNumber((( gdjs.decouvreCode.GDsprite_9595syllabe2Objects2.length === 0 ) ? 0 :gdjs.decouvreCode.GDsprite_9595syllabe2Objects2[0].getAnimation()));
}{runtimeScene.getScene().getVariables().getFromIndex(3).setNumber(1);
}{runtimeScene.getScene().getVariables().getFromIndex(4).setNumber(1);
}
{ //Subevents
gdjs.decouvreCode.eventsList32(runtimeScene);} //End of subevents
}

}


{



}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(3)) == 1;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 0;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.sound.isMusicOnChannelStopped(runtimeScene, 1);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/ca_canard.mp3", 1, false, 100, 1);
}{runtimeScene.getScene().getVariables().getFromIndex(3).setNumber(0);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(3)) == 1;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 1;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.sound.isMusicOnChannelStopped(runtimeScene, 1);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/co_cochon.mp3", 1, false, 100, 1);
}{runtimeScene.getScene().getVariables().getFromIndex(3).setNumber(0);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(3)) == 1;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 2;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.sound.isMusicOnChannelStopped(runtimeScene, 1);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/cha_chameau.mp3", 1, false, 100, 1);
}{runtimeScene.getScene().getVariables().getFromIndex(3).setNumber(0);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(3)) == 1;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 3;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.sound.isMusicOnChannelStopped(runtimeScene, 1);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/four_fourmi.mp3", 1, false, 100, 1);
}{runtimeScene.getScene().getVariables().getFromIndex(3).setNumber(0);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(3)) == 1;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 4;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.sound.isMusicOnChannelStopped(runtimeScene, 1);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/la_lapin.mp3", 1, false, 100, 1);
}{runtimeScene.getScene().getVariables().getFromIndex(3).setNumber(0);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(3)) == 1;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 5;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.sound.isMusicOnChannelStopped(runtimeScene, 1);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/mou_mouton.mp3", 1, false, 100, 1);
}{runtimeScene.getScene().getVariables().getFromIndex(3).setNumber(0);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(3)) == 1;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 6;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.sound.isMusicOnChannelStopped(runtimeScene, 1);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/sou_souris.mp3", 1, false, 100, 1);
}{runtimeScene.getScene().getVariables().getFromIndex(3).setNumber(0);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(3)) == 1;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 7;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.sound.isMusicOnChannelStopped(runtimeScene, 1);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/tor_tortue.mp3", 1, false, 100, 1);
}{runtimeScene.getScene().getVariables().getFromIndex(3).setNumber(0);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(3)) == 1;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 8;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.sound.isMusicOnChannelStopped(runtimeScene, 1);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/hi_hibou.mp3", 1, false, 100, 1);
}{runtimeScene.getScene().getVariables().getFromIndex(3).setNumber(0);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(3)) == 1;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 9;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.sound.isMusicOnChannelStopped(runtimeScene, 1);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/pi_pigeon.mp3", 1, false, 100, 1);
}{runtimeScene.getScene().getVariables().getFromIndex(3).setNumber(0);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(3)) == 1;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 10;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.sound.isMusicOnChannelStopped(runtimeScene, 1);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/ta_tatou.mp3", 1, false, 100, 1);
}{runtimeScene.getScene().getVariables().getFromIndex(3).setNumber(0);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(3)) == 1;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 11;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.sound.isMusicOnChannelStopped(runtimeScene, 1);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/fla_flaman.mp3", 1, false, 100, 1);
}{runtimeScene.getScene().getVariables().getFromIndex(3).setNumber(0);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(3)) == 1;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 12;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.sound.isMusicOnChannelStopped(runtimeScene, 1);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/pin_pingouin.mp3", 1, false, 100, 1);
}{runtimeScene.getScene().getVariables().getFromIndex(3).setNumber(0);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(3)) == 1;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 13;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.sound.isMusicOnChannelStopped(runtimeScene, 1);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/dau_dauphin.mp3", 1, false, 100, 1);
}{runtimeScene.getScene().getVariables().getFromIndex(3).setNumber(0);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(3)) == 1;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 14;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.sound.isMusicOnChannelStopped(runtimeScene, 1);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/le_lezard.mp3", 1, false, 100, 1);
}{runtimeScene.getScene().getVariables().getFromIndex(3).setNumber(0);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(3)) == 1;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 15;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.sound.isMusicOnChannelStopped(runtimeScene, 1);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/che_cheval.mp3", 1, false, 100, 1);
}{runtimeScene.getScene().getVariables().getFromIndex(3).setNumber(0);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(3)) == 1;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 16;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.sound.isMusicOnChannelStopped(runtimeScene, 1);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/ze_zebu.mp3", 1, false, 100, 1);
}{runtimeScene.getScene().getVariables().getFromIndex(3).setNumber(0);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(3)) == 1;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 17;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.sound.isMusicOnChannelStopped(runtimeScene, 1);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/pan_panda.mp3", 1, false, 100, 1);
}{runtimeScene.getScene().getVariables().getFromIndex(3).setNumber(0);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(3)) == 1;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 18;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.sound.isMusicOnChannelStopped(runtimeScene, 1);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/din_dindon.mp3", 1, false, 100, 1);
}{runtimeScene.getScene().getVariables().getFromIndex(3).setNumber(0);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(3)) == 1;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 19;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.sound.isMusicOnChannelStopped(runtimeScene, 1);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.sound.isMusicOnChannelStopped(runtimeScene, 1);
}
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/re_requin.mp3", 1, false, 100, 1);
}{runtimeScene.getScene().getVariables().getFromIndex(3).setNumber(0);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(3)) == 1;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 20;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.sound.isMusicOnChannelStopped(runtimeScene, 1);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/cor_corbeau.mp3", 1, false, 100, 1);
}{runtimeScene.getScene().getVariables().getFromIndex(3).setNumber(0);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(3)) == 1;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 21;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.sound.isMusicOnChannelStopped(runtimeScene, 1);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/ho_homard.mp3", 1, false, 100, 1);
}{runtimeScene.getScene().getVariables().getFromIndex(3).setNumber(0);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(3)) == 1;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 22;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.sound.isMusicOnChannelStopped(runtimeScene, 1);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/blai_blaireau.mp3", 1, false, 100, 1);
}{runtimeScene.getScene().getVariables().getFromIndex(3).setNumber(0);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(3)) == 1;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 23;
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/la_lama.mp3", 1, false, 100, 1);
}{runtimeScene.getScene().getVariables().getFromIndex(3).setNumber(0);
}}

}


{



}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(4)) == 1;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(2)) == 0;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.sound.isMusicOnChannelStopped(runtimeScene, 1);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/nard_canard.mp3", 1, false, 100, 1);
}{runtimeScene.getScene().getVariables().getFromIndex(4).setNumber(0);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(4)) == 1;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(2)) == 1;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.sound.isMusicOnChannelStopped(runtimeScene, 1);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/chon_cochon.mp3", 1, false, 100, 1);
}{runtimeScene.getScene().getVariables().getFromIndex(4).setNumber(0);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(4)) == 1;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(2)) == 2;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.sound.isMusicOnChannelStopped(runtimeScene, 1);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/meau_chameau.mp3", 1, false, 100, 1);
}{runtimeScene.getScene().getVariables().getFromIndex(4).setNumber(0);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(4)) == 1;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(2)) == 3;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.sound.isMusicOnChannelStopped(runtimeScene, 1);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/mi_fourmi.mp3", 1, false, 100, 1);
}{runtimeScene.getScene().getVariables().getFromIndex(4).setNumber(0);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(4)) == 1;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(2)) == 4;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.sound.isMusicOnChannelStopped(runtimeScene, 1);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/pin_lapin.mp3", 1, false, 100, 1);
}{runtimeScene.getScene().getVariables().getFromIndex(4).setNumber(0);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(4)) == 1;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(2)) == 5;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.sound.isMusicOnChannelStopped(runtimeScene, 1);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/ton_mouton.mp3", 1, false, 100, 1);
}{runtimeScene.getScene().getVariables().getFromIndex(4).setNumber(0);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(4)) == 1;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(2)) == 6;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.sound.isMusicOnChannelStopped(runtimeScene, 1);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/ri_souris.mp3", 1, false, 100, 1);
}{runtimeScene.getScene().getVariables().getFromIndex(4).setNumber(0);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(4)) == 1;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(2)) == 7;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.sound.isMusicOnChannelStopped(runtimeScene, 1);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/tue_tortue.mp3", 1, false, 100, 1);
}{runtimeScene.getScene().getVariables().getFromIndex(4).setNumber(0);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(4)) == 1;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(2)) == 8;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.sound.isMusicOnChannelStopped(runtimeScene, 1);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/bou_hibou.mp3", 1, false, 100, 1);
}{runtimeScene.getScene().getVariables().getFromIndex(4).setNumber(0);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(4)) == 1;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(2)) == 9;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.sound.isMusicOnChannelStopped(runtimeScene, 1);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/geon_pigeon.mp3", 1, false, 100, 1);
}{runtimeScene.getScene().getVariables().getFromIndex(4).setNumber(0);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(4)) == 1;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(2)) == 10;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.sound.isMusicOnChannelStopped(runtimeScene, 1);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/tou_tatou.mp3", 1, false, 100, 1);
}{runtimeScene.getScene().getVariables().getFromIndex(4).setNumber(0);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(4)) == 1;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(2)) == 11;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.sound.isMusicOnChannelStopped(runtimeScene, 1);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/man_flaman.mp3", 1, false, 100, 1);
}{runtimeScene.getScene().getVariables().getFromIndex(4).setNumber(0);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(4)) == 1;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(2)) == 12;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.sound.isMusicOnChannelStopped(runtimeScene, 1);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/gouin_pingouin.mp3", 1, false, 100, 1);
}{runtimeScene.getScene().getVariables().getFromIndex(4).setNumber(0);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(4)) == 1;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(2)) == 13;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.sound.isMusicOnChannelStopped(runtimeScene, 1);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/phin_dauphin.mp3", 1, false, 100, 1);
}{runtimeScene.getScene().getVariables().getFromIndex(4).setNumber(0);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(4)) == 1;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(2)) == 14;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.sound.isMusicOnChannelStopped(runtimeScene, 1);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/zard_lezard.mp3", 1, false, 100, 1);
}{runtimeScene.getScene().getVariables().getFromIndex(4).setNumber(0);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(4)) == 1;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(2)) == 15;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.sound.isMusicOnChannelStopped(runtimeScene, 1);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/val_cheval.mp3", 1, false, 100, 1);
}{runtimeScene.getScene().getVariables().getFromIndex(4).setNumber(0);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(4)) == 1;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(2)) == 16;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.sound.isMusicOnChannelStopped(runtimeScene, 1);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/bu_zebu.mp3", 1, false, 100, 1);
}{runtimeScene.getScene().getVariables().getFromIndex(4).setNumber(0);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(4)) == 1;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(2)) == 17;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.sound.isMusicOnChannelStopped(runtimeScene, 1);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/da_panda.mp3", 1, false, 100, 1);
}{runtimeScene.getScene().getVariables().getFromIndex(4).setNumber(0);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(4)) == 1;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(2)) == 18;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.sound.isMusicOnChannelStopped(runtimeScene, 1);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/don_dindon.mp3", 1, false, 100, 1);
}{runtimeScene.getScene().getVariables().getFromIndex(4).setNumber(0);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(4)) == 1;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(2)) == 19;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.sound.isMusicOnChannelStopped(runtimeScene, 1);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/quin_requin.mp3", 1, false, 100, 1);
}{runtimeScene.getScene().getVariables().getFromIndex(4).setNumber(0);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(4)) == 1;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(2)) == 20;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.sound.isMusicOnChannelStopped(runtimeScene, 1);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/beau_corbeau.mp3", 1, false, 100, 1);
}{runtimeScene.getScene().getVariables().getFromIndex(4).setNumber(0);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(4)) == 1;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(2)) == 21;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.sound.isMusicOnChannelStopped(runtimeScene, 1);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/mard_homard.mp3", 1, false, 100, 1);
}{runtimeScene.getScene().getVariables().getFromIndex(4).setNumber(0);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(4)) == 1;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(2)) == 22;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.sound.isMusicOnChannelStopped(runtimeScene, 1);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/reau_blaireau.mp3", 1, false, 100, 1);
}{runtimeScene.getScene().getVariables().getFromIndex(4).setNumber(0);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(4)) == 1;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(2)) == 23;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.sound.isMusicOnChannelStopped(runtimeScene, 1);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/ma_lama.mp3", 1, false, 100, 1);
}{runtimeScene.getScene().getVariables().getFromIndex(4).setNumber(0);
}}

}


};gdjs.decouvreCode.eventsList34 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("case_majuscules"), gdjs.decouvreCode.GDcase_9595majusculesObjects1);
gdjs.copyArray(runtimeScene.getObjects("case_minuscules"), gdjs.decouvreCode.GDcase_9595minusculesObjects1);
gdjs.copyArray(runtimeScene.getObjects("sprite_bouton_majuscules"), gdjs.decouvreCode.GDsprite_9595bouton_9595majusculesObjects1);
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe1").setNumber(0);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("syllabe2").setNumber(0);
}{for(var i = 0, len = gdjs.decouvreCode.GDcase_9595majusculesObjects1.length ;i < len;++i) {
    gdjs.decouvreCode.GDcase_9595majusculesObjects1[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2).getChild("majuscules")));
}
}{for(var i = 0, len = gdjs.decouvreCode.GDcase_9595minusculesObjects1.length ;i < len;++i) {
    gdjs.decouvreCode.GDcase_9595minusculesObjects1[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2).getChild("minuscules")));
}
}{for(var i = 0, len = gdjs.decouvreCode.GDsprite_9595bouton_9595majusculesObjects1.length ;i < len;++i) {
    gdjs.decouvreCode.GDsprite_9595bouton_9595majusculesObjects1[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2).getChild("majuscules")));
}
}{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/audio_vide.mp3", 1, false, 100, 1);
}}

}


{


gdjs.decouvreCode.eventsList1(runtimeScene);
}


{


gdjs.decouvreCode.eventsList2(runtimeScene);
}


{


let isConditionTrue_0 = false;
{
}

}


{


gdjs.decouvreCode.eventsList3(runtimeScene);
}


{


gdjs.decouvreCode.eventsList4(runtimeScene);
}


{


gdjs.decouvreCode.eventsList5(runtimeScene);
}


{



}


{


gdjs.decouvreCode.eventsList30(runtimeScene);
}


{


gdjs.decouvreCode.eventsList31(runtimeScene);
}


{



}


{



}


{



}


{



}


{


gdjs.decouvreCode.eventsList33(runtimeScene);
}


{



}


};

gdjs.decouvreCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.decouvreCode.GDcercle_9595droitObjects1.length = 0;
gdjs.decouvreCode.GDcercle_9595droitObjects2.length = 0;
gdjs.decouvreCode.GDcercle_9595droitObjects3.length = 0;
gdjs.decouvreCode.GDcercle_9595droitObjects4.length = 0;
gdjs.decouvreCode.GDcercle_9595gaucheObjects1.length = 0;
gdjs.decouvreCode.GDcercle_9595gaucheObjects2.length = 0;
gdjs.decouvreCode.GDcercle_9595gaucheObjects3.length = 0;
gdjs.decouvreCode.GDcercle_9595gaucheObjects4.length = 0;
gdjs.decouvreCode.GDsprite_9595syllabe1Objects1.length = 0;
gdjs.decouvreCode.GDsprite_9595syllabe1Objects2.length = 0;
gdjs.decouvreCode.GDsprite_9595syllabe1Objects3.length = 0;
gdjs.decouvreCode.GDsprite_9595syllabe1Objects4.length = 0;
gdjs.decouvreCode.GDsprite_9595syllabe2Objects1.length = 0;
gdjs.decouvreCode.GDsprite_9595syllabe2Objects2.length = 0;
gdjs.decouvreCode.GDsprite_9595syllabe2Objects3.length = 0;
gdjs.decouvreCode.GDsprite_9595syllabe2Objects4.length = 0;
gdjs.decouvreCode.GDsprite_9595minusculesObjects1.length = 0;
gdjs.decouvreCode.GDsprite_9595minusculesObjects2.length = 0;
gdjs.decouvreCode.GDsprite_9595minusculesObjects3.length = 0;
gdjs.decouvreCode.GDsprite_9595minusculesObjects4.length = 0;
gdjs.decouvreCode.GDsprite_9595majusculesObjects1.length = 0;
gdjs.decouvreCode.GDsprite_9595majusculesObjects2.length = 0;
gdjs.decouvreCode.GDsprite_9595majusculesObjects3.length = 0;
gdjs.decouvreCode.GDsprite_9595majusculesObjects4.length = 0;
gdjs.decouvreCode.GDsprite_9595bouton_9595majusculesObjects1.length = 0;
gdjs.decouvreCode.GDsprite_9595bouton_9595majusculesObjects2.length = 0;
gdjs.decouvreCode.GDsprite_9595bouton_9595majusculesObjects3.length = 0;
gdjs.decouvreCode.GDsprite_9595bouton_9595majusculesObjects4.length = 0;
gdjs.decouvreCode.GDsprite_9595bouton_9595minusculesObjects1.length = 0;
gdjs.decouvreCode.GDsprite_9595bouton_9595minusculesObjects2.length = 0;
gdjs.decouvreCode.GDsprite_9595bouton_9595minusculesObjects3.length = 0;
gdjs.decouvreCode.GDsprite_9595bouton_9595minusculesObjects4.length = 0;
gdjs.decouvreCode.GDsprite_9595bouton_9595imagesObjects1.length = 0;
gdjs.decouvreCode.GDsprite_9595bouton_9595imagesObjects2.length = 0;
gdjs.decouvreCode.GDsprite_9595bouton_9595imagesObjects3.length = 0;
gdjs.decouvreCode.GDsprite_9595bouton_9595imagesObjects4.length = 0;
gdjs.decouvreCode.GDsprite_9595imagesObjects1.length = 0;
gdjs.decouvreCode.GDsprite_9595imagesObjects2.length = 0;
gdjs.decouvreCode.GDsprite_9595imagesObjects3.length = 0;
gdjs.decouvreCode.GDsprite_9595imagesObjects4.length = 0;
gdjs.decouvreCode.GDsprite_9595oreilleObjects1.length = 0;
gdjs.decouvreCode.GDsprite_9595oreilleObjects2.length = 0;
gdjs.decouvreCode.GDsprite_9595oreilleObjects3.length = 0;
gdjs.decouvreCode.GDsprite_9595oreilleObjects4.length = 0;
gdjs.decouvreCode.GDcercle_9595orangeObjects1.length = 0;
gdjs.decouvreCode.GDcercle_9595orangeObjects2.length = 0;
gdjs.decouvreCode.GDcercle_9595orangeObjects3.length = 0;
gdjs.decouvreCode.GDcercle_9595orangeObjects4.length = 0;
gdjs.decouvreCode.GDcercle_9595Objects1.length = 0;
gdjs.decouvreCode.GDcercle_9595Objects2.length = 0;
gdjs.decouvreCode.GDcercle_9595Objects3.length = 0;
gdjs.decouvreCode.GDcercle_9595Objects4.length = 0;
gdjs.decouvreCode.GDsprite_9595retourObjects1.length = 0;
gdjs.decouvreCode.GDsprite_9595retourObjects2.length = 0;
gdjs.decouvreCode.GDsprite_9595retourObjects3.length = 0;
gdjs.decouvreCode.GDsprite_9595retourObjects4.length = 0;
gdjs.decouvreCode.GDfond_9595blancObjects1.length = 0;
gdjs.decouvreCode.GDfond_9595blancObjects2.length = 0;
gdjs.decouvreCode.GDfond_9595blancObjects3.length = 0;
gdjs.decouvreCode.GDfond_9595blancObjects4.length = 0;
gdjs.decouvreCode.GDcase_9595minusculesObjects1.length = 0;
gdjs.decouvreCode.GDcase_9595minusculesObjects2.length = 0;
gdjs.decouvreCode.GDcase_9595minusculesObjects3.length = 0;
gdjs.decouvreCode.GDcase_9595minusculesObjects4.length = 0;
gdjs.decouvreCode.GDcase_9595majusculesObjects1.length = 0;
gdjs.decouvreCode.GDcase_9595majusculesObjects2.length = 0;
gdjs.decouvreCode.GDcase_9595majusculesObjects3.length = 0;
gdjs.decouvreCode.GDcase_9595majusculesObjects4.length = 0;
gdjs.decouvreCode.GDtexte_9595majusculesObjects1.length = 0;
gdjs.decouvreCode.GDtexte_9595majusculesObjects2.length = 0;
gdjs.decouvreCode.GDtexte_9595majusculesObjects3.length = 0;
gdjs.decouvreCode.GDtexte_9595majusculesObjects4.length = 0;
gdjs.decouvreCode.GDtexte_9595minusculesObjects1.length = 0;
gdjs.decouvreCode.GDtexte_9595minusculesObjects2.length = 0;
gdjs.decouvreCode.GDtexte_9595minusculesObjects3.length = 0;
gdjs.decouvreCode.GDtexte_9595minusculesObjects4.length = 0;
gdjs.decouvreCode.GDtexte_9595choisir_9595motsObjects1.length = 0;
gdjs.decouvreCode.GDtexte_9595choisir_9595motsObjects2.length = 0;
gdjs.decouvreCode.GDtexte_9595choisir_9595motsObjects3.length = 0;
gdjs.decouvreCode.GDtexte_9595choisir_9595motsObjects4.length = 0;
gdjs.decouvreCode.GDsprite_9595baguetteObjects1.length = 0;
gdjs.decouvreCode.GDsprite_9595baguetteObjects2.length = 0;
gdjs.decouvreCode.GDsprite_9595baguetteObjects3.length = 0;
gdjs.decouvreCode.GDsprite_9595baguetteObjects4.length = 0;
gdjs.decouvreCode.GDcredit_9595imagesObjects1.length = 0;
gdjs.decouvreCode.GDcredit_9595imagesObjects2.length = 0;
gdjs.decouvreCode.GDcredit_9595imagesObjects3.length = 0;
gdjs.decouvreCode.GDcredit_9595imagesObjects4.length = 0;
gdjs.decouvreCode.GDtexte_9595auteurObjects1.length = 0;
gdjs.decouvreCode.GDtexte_9595auteurObjects2.length = 0;
gdjs.decouvreCode.GDtexte_9595auteurObjects3.length = 0;
gdjs.decouvreCode.GDtexte_9595auteurObjects4.length = 0;
gdjs.decouvreCode.GDsprite_9595serge_9595lamaObjects1.length = 0;
gdjs.decouvreCode.GDsprite_9595serge_9595lamaObjects2.length = 0;
gdjs.decouvreCode.GDsprite_9595serge_9595lamaObjects3.length = 0;
gdjs.decouvreCode.GDsprite_9595serge_9595lamaObjects4.length = 0;

gdjs.decouvreCode.eventsList34(runtimeScene);

return;

}

gdjs['decouvreCode'] = gdjs.decouvreCode;
