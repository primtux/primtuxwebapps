gdjs.infosCode = {};
gdjs.infosCode.GDinfo2Objects1= [];
gdjs.infosCode.GDinfo2Objects2= [];
gdjs.infosCode.GDinfo1Objects1= [];
gdjs.infosCode.GDinfo1Objects2= [];
gdjs.infosCode.GDfond_9595blancObjects1= [];
gdjs.infosCode.GDfond_9595blancObjects2= [];
gdjs.infosCode.GDlivret_95951Objects1= [];
gdjs.infosCode.GDlivret_95951Objects2= [];
gdjs.infosCode.GDlivret_95952Objects1= [];
gdjs.infosCode.GDlivret_95952Objects2= [];
gdjs.infosCode.GDbouton_9595retourObjects1= [];
gdjs.infosCode.GDbouton_9595retourObjects2= [];
gdjs.infosCode.GDfond_9595Objects1= [];
gdjs.infosCode.GDfond_9595Objects2= [];
gdjs.infosCode.GDpersonnageObjects1= [];
gdjs.infosCode.GDpersonnageObjects2= [];
gdjs.infosCode.GDobstacle_95951Objects1= [];
gdjs.infosCode.GDobstacle_95951Objects2= [];
gdjs.infosCode.GDobstacle_95952Objects1= [];
gdjs.infosCode.GDobstacle_95952Objects2= [];
gdjs.infosCode.GDfleche_9595hautObjects1= [];
gdjs.infosCode.GDfleche_9595hautObjects2= [];
gdjs.infosCode.GDfleche_9595basObjects1= [];
gdjs.infosCode.GDfleche_9595basObjects2= [];
gdjs.infosCode.GDfleche_9595gaucheObjects1= [];
gdjs.infosCode.GDfleche_9595gaucheObjects2= [];
gdjs.infosCode.GDfleche_9595droiteObjects1= [];
gdjs.infosCode.GDfleche_9595droiteObjects2= [];
gdjs.infosCode.GDfleche_9595haut_9595pObjects1= [];
gdjs.infosCode.GDfleche_9595haut_9595pObjects2= [];
gdjs.infosCode.GDfleche_9595haut_9595traceObjects1= [];
gdjs.infosCode.GDfleche_9595haut_9595traceObjects2= [];
gdjs.infosCode.GDfleche_9595bas_9595pObjects1= [];
gdjs.infosCode.GDfleche_9595bas_9595pObjects2= [];
gdjs.infosCode.GDfleche_9595bas_9595traceObjects1= [];
gdjs.infosCode.GDfleche_9595bas_9595traceObjects2= [];
gdjs.infosCode.GDarriveeObjects1= [];
gdjs.infosCode.GDarriveeObjects2= [];
gdjs.infosCode.GDfleche_9595gauche_9595pObjects1= [];
gdjs.infosCode.GDfleche_9595gauche_9595pObjects2= [];
gdjs.infosCode.GDfleche_9595gauche_9595traceObjects1= [];
gdjs.infosCode.GDfleche_9595gauche_9595traceObjects2= [];
gdjs.infosCode.GDfleche_9595droite_9595pObjects1= [];
gdjs.infosCode.GDfleche_9595droite_9595pObjects2= [];
gdjs.infosCode.GDfleche_9595droite_9595traceObjects1= [];
gdjs.infosCode.GDfleche_9595droite_9595traceObjects2= [];
gdjs.infosCode.GDcase_9595blanche_959548Objects1= [];
gdjs.infosCode.GDcase_9595blanche_959548Objects2= [];
gdjs.infosCode.GDcase_9595blanche_959564Objects1= [];
gdjs.infosCode.GDcase_9595blanche_959564Objects2= [];
gdjs.infosCode.GDcase_9595blancheObjects1= [];
gdjs.infosCode.GDcase_9595blancheObjects2= [];
gdjs.infosCode.GDempreintesObjects1= [];
gdjs.infosCode.GDempreintesObjects2= [];
gdjs.infosCode.GDvitesseObjects1= [];
gdjs.infosCode.GDvitesseObjects2= [];
gdjs.infosCode.GDaide_9595sergeObjects1= [];
gdjs.infosCode.GDaide_9595sergeObjects2= [];


gdjs.infosCode.mapOfGDgdjs_9546infosCode_9546GDbouton_95959595retourObjects1Objects = Hashtable.newFrom({"bouton_retour": gdjs.infosCode.GDbouton_9595retourObjects1});
gdjs.infosCode.eventsList0 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("fond_blanc"), gdjs.infosCode.GDfond_9595blancObjects1);
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/audio_vide.mp3", 1, false, 100, 1);
}{for(var i = 0, len = gdjs.infosCode.GDfond_9595blancObjects1.length ;i < len;++i) {
    gdjs.infosCode.GDfond_9595blancObjects1[i].setOpacity(150);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_retour"), gdjs.infosCode.GDbouton_9595retourObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.infosCode.mapOfGDgdjs_9546infosCode_9546GDbouton_95959595retourObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(29778020);
}
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/bulle_1.mp3", 1, false, 100, 1);
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "menu", true);
}}

}


{


let isConditionTrue_0 = false;
{
}

}


};

gdjs.infosCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.infosCode.GDinfo2Objects1.length = 0;
gdjs.infosCode.GDinfo2Objects2.length = 0;
gdjs.infosCode.GDinfo1Objects1.length = 0;
gdjs.infosCode.GDinfo1Objects2.length = 0;
gdjs.infosCode.GDfond_9595blancObjects1.length = 0;
gdjs.infosCode.GDfond_9595blancObjects2.length = 0;
gdjs.infosCode.GDlivret_95951Objects1.length = 0;
gdjs.infosCode.GDlivret_95951Objects2.length = 0;
gdjs.infosCode.GDlivret_95952Objects1.length = 0;
gdjs.infosCode.GDlivret_95952Objects2.length = 0;
gdjs.infosCode.GDbouton_9595retourObjects1.length = 0;
gdjs.infosCode.GDbouton_9595retourObjects2.length = 0;
gdjs.infosCode.GDfond_9595Objects1.length = 0;
gdjs.infosCode.GDfond_9595Objects2.length = 0;
gdjs.infosCode.GDpersonnageObjects1.length = 0;
gdjs.infosCode.GDpersonnageObjects2.length = 0;
gdjs.infosCode.GDobstacle_95951Objects1.length = 0;
gdjs.infosCode.GDobstacle_95951Objects2.length = 0;
gdjs.infosCode.GDobstacle_95952Objects1.length = 0;
gdjs.infosCode.GDobstacle_95952Objects2.length = 0;
gdjs.infosCode.GDfleche_9595hautObjects1.length = 0;
gdjs.infosCode.GDfleche_9595hautObjects2.length = 0;
gdjs.infosCode.GDfleche_9595basObjects1.length = 0;
gdjs.infosCode.GDfleche_9595basObjects2.length = 0;
gdjs.infosCode.GDfleche_9595gaucheObjects1.length = 0;
gdjs.infosCode.GDfleche_9595gaucheObjects2.length = 0;
gdjs.infosCode.GDfleche_9595droiteObjects1.length = 0;
gdjs.infosCode.GDfleche_9595droiteObjects2.length = 0;
gdjs.infosCode.GDfleche_9595haut_9595pObjects1.length = 0;
gdjs.infosCode.GDfleche_9595haut_9595pObjects2.length = 0;
gdjs.infosCode.GDfleche_9595haut_9595traceObjects1.length = 0;
gdjs.infosCode.GDfleche_9595haut_9595traceObjects2.length = 0;
gdjs.infosCode.GDfleche_9595bas_9595pObjects1.length = 0;
gdjs.infosCode.GDfleche_9595bas_9595pObjects2.length = 0;
gdjs.infosCode.GDfleche_9595bas_9595traceObjects1.length = 0;
gdjs.infosCode.GDfleche_9595bas_9595traceObjects2.length = 0;
gdjs.infosCode.GDarriveeObjects1.length = 0;
gdjs.infosCode.GDarriveeObjects2.length = 0;
gdjs.infosCode.GDfleche_9595gauche_9595pObjects1.length = 0;
gdjs.infosCode.GDfleche_9595gauche_9595pObjects2.length = 0;
gdjs.infosCode.GDfleche_9595gauche_9595traceObjects1.length = 0;
gdjs.infosCode.GDfleche_9595gauche_9595traceObjects2.length = 0;
gdjs.infosCode.GDfleche_9595droite_9595pObjects1.length = 0;
gdjs.infosCode.GDfleche_9595droite_9595pObjects2.length = 0;
gdjs.infosCode.GDfleche_9595droite_9595traceObjects1.length = 0;
gdjs.infosCode.GDfleche_9595droite_9595traceObjects2.length = 0;
gdjs.infosCode.GDcase_9595blanche_959548Objects1.length = 0;
gdjs.infosCode.GDcase_9595blanche_959548Objects2.length = 0;
gdjs.infosCode.GDcase_9595blanche_959564Objects1.length = 0;
gdjs.infosCode.GDcase_9595blanche_959564Objects2.length = 0;
gdjs.infosCode.GDcase_9595blancheObjects1.length = 0;
gdjs.infosCode.GDcase_9595blancheObjects2.length = 0;
gdjs.infosCode.GDempreintesObjects1.length = 0;
gdjs.infosCode.GDempreintesObjects2.length = 0;
gdjs.infosCode.GDvitesseObjects1.length = 0;
gdjs.infosCode.GDvitesseObjects2.length = 0;
gdjs.infosCode.GDaide_9595sergeObjects1.length = 0;
gdjs.infosCode.GDaide_9595sergeObjects2.length = 0;

gdjs.infosCode.eventsList0(runtimeScene);

return;

}

gdjs['infosCode'] = gdjs.infosCode;
