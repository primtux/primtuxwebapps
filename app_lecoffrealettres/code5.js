gdjs.configurationCode = {};
gdjs.configurationCode.localVariables = [];
gdjs.configurationCode.GDinfo_95951Objects1= [];
gdjs.configurationCode.GDinfo_95951Objects2= [];
gdjs.configurationCode.GDinfo_95951Objects3= [];
gdjs.configurationCode.GDinfo_95951Objects4= [];
gdjs.configurationCode.GDinfo_95952Objects1= [];
gdjs.configurationCode.GDinfo_95952Objects2= [];
gdjs.configurationCode.GDinfo_95952Objects3= [];
gdjs.configurationCode.GDinfo_95952Objects4= [];
gdjs.configurationCode.GDinfo_95953Objects1= [];
gdjs.configurationCode.GDinfo_95953Objects2= [];
gdjs.configurationCode.GDinfo_95953Objects3= [];
gdjs.configurationCode.GDinfo_95953Objects4= [];
gdjs.configurationCode.GDbouton_9595retourObjects1= [];
gdjs.configurationCode.GDbouton_9595retourObjects2= [];
gdjs.configurationCode.GDbouton_9595retourObjects3= [];
gdjs.configurationCode.GDbouton_9595retourObjects4= [];
gdjs.configurationCode.GDcoffreObjects1= [];
gdjs.configurationCode.GDcoffreObjects2= [];
gdjs.configurationCode.GDcoffreObjects3= [];
gdjs.configurationCode.GDcoffreObjects4= [];
gdjs.configurationCode.GDsp_9595diamantObjects1= [];
gdjs.configurationCode.GDsp_9595diamantObjects2= [];
gdjs.configurationCode.GDsp_9595diamantObjects3= [];
gdjs.configurationCode.GDsp_9595diamantObjects4= [];
gdjs.configurationCode.GDGreenDotBarObjects1= [];
gdjs.configurationCode.GDGreenDotBarObjects2= [];
gdjs.configurationCode.GDGreenDotBarObjects3= [];
gdjs.configurationCode.GDGreenDotBarObjects4= [];
gdjs.configurationCode.GDsp_9595cadre0Objects1= [];
gdjs.configurationCode.GDsp_9595cadre0Objects2= [];
gdjs.configurationCode.GDsp_9595cadre0Objects3= [];
gdjs.configurationCode.GDsp_9595cadre0Objects4= [];
gdjs.configurationCode.GDpapierObjects1= [];
gdjs.configurationCode.GDpapierObjects2= [];
gdjs.configurationCode.GDpapierObjects3= [];
gdjs.configurationCode.GDpapierObjects4= [];
gdjs.configurationCode.GDtext_9595modeleObjects1= [];
gdjs.configurationCode.GDtext_9595modeleObjects2= [];
gdjs.configurationCode.GDtext_9595modeleObjects3= [];
gdjs.configurationCode.GDtext_9595modeleObjects4= [];
gdjs.configurationCode.GDclavierObjects1= [];
gdjs.configurationCode.GDclavierObjects2= [];
gdjs.configurationCode.GDclavierObjects3= [];
gdjs.configurationCode.GDclavierObjects4= [];


gdjs.configurationCode.eventsList0 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("type_caracteres").getChild("modele")) == 0;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("text_modele"), gdjs.configurationCode.GDtext_9595modeleObjects3);
{for(var i = 0, len = gdjs.configurationCode.GDtext_9595modeleObjects3.length ;i < len;++i) {
    gdjs.configurationCode.GDtext_9595modeleObjects3[i].setFontName("polices/BelleAllureScript2i-Gros.otf");
}
}{for(var i = 0, len = gdjs.configurationCode.GDtext_9595modeleObjects3.length ;i < len;++i) {
    gdjs.configurationCode.GDtext_9595modeleObjects3[i].getBehavior("Text").setText(gdjs.evtTools.string.toUpperCase((gdjs.configurationCode.GDtext_9595modeleObjects3[i].getBehavior("Text").getText())));
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("type_caracteres").getChild("modele")) == 1;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("text_modele"), gdjs.configurationCode.GDtext_9595modeleObjects3);
{for(var i = 0, len = gdjs.configurationCode.GDtext_9595modeleObjects3.length ;i < len;++i) {
    gdjs.configurationCode.GDtext_9595modeleObjects3[i].setFontName("polices/BelleAllureScript2i-Gros.otf");
}
}{for(var i = 0, len = gdjs.configurationCode.GDtext_9595modeleObjects3.length ;i < len;++i) {
    gdjs.configurationCode.GDtext_9595modeleObjects3[i].getBehavior("Text").setText(gdjs.evtTools.string.toLowerCase((gdjs.configurationCode.GDtext_9595modeleObjects3[i].getBehavior("Text").getText())));
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("type_caracteres").getChild("modele")) == 2;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("text_modele"), gdjs.configurationCode.GDtext_9595modeleObjects3);
{for(var i = 0, len = gdjs.configurationCode.GDtext_9595modeleObjects3.length ;i < len;++i) {
    gdjs.configurationCode.GDtext_9595modeleObjects3[i].setFontName("polices/BelleAllureGS-Gros.otf");
}
}}

}


{


let isConditionTrue_0 = false;
{
}

}


};gdjs.configurationCode.eventsList1 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("type_caracteres").getChild("clavier")) == 0;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("clavier"), gdjs.configurationCode.GDclavierObjects3);
{for(var i = 0, len = gdjs.configurationCode.GDclavierObjects3.length ;i < len;++i) {
    gdjs.configurationCode.GDclavierObjects3[i].setAnimationFrame(0);
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("type_caracteres").getChild("clavier")) == 1;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("clavier"), gdjs.configurationCode.GDclavierObjects3);
{for(var i = 0, len = gdjs.configurationCode.GDclavierObjects3.length ;i < len;++i) {
    gdjs.configurationCode.GDclavierObjects3[i].setAnimationFrame(1);
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("type_caracteres").getChild("clavier")) == 2;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("clavier"), gdjs.configurationCode.GDclavierObjects3);
{for(var i = 0, len = gdjs.configurationCode.GDclavierObjects3.length ;i < len;++i) {
    gdjs.configurationCode.GDclavierObjects3[i].setAnimationFrame(2);
}
}}

}


{


let isConditionTrue_0 = false;
{
}

}


};gdjs.configurationCode.eventsList2 = function(runtimeScene) {

{


gdjs.configurationCode.eventsList0(runtimeScene);
}


{


gdjs.configurationCode.eventsList1(runtimeScene);
}


{


let isConditionTrue_0 = false;
{
{runtimeScene.getScene().getVariables().getFromIndex(0).setNumber(0);
}}

}


};gdjs.configurationCode.eventsList3 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 1;
if (isConditionTrue_0) {

{ //Subevents
gdjs.configurationCode.eventsList2(runtimeScene);} //End of subevents
}

}


};gdjs.configurationCode.mapOfGDgdjs_9546configurationCode_9546GDbouton_95959595retourObjects1Objects = Hashtable.newFrom({"bouton_retour": gdjs.configurationCode.GDbouton_9595retourObjects1});
gdjs.configurationCode.eventsList4 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
{gdjs.evtTools.storage.clearJSONFile("sauvegarde_lecoffrealettres");
}{gdjs.evtTools.storage.writeStringInJSONFile("sauvegarde_lecoffrealettres", "reglages", gdjs.evtTools.network.variableStructureToJSON(runtimeScene.getGame().getVariables().getFromIndex(1)));
}}

}


};gdjs.configurationCode.eventsList5 = function(runtimeScene) {

{


gdjs.configurationCode.eventsList4(runtimeScene);
}


{


let isConditionTrue_0 = false;
{
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "menu", false);
}}

}


};gdjs.configurationCode.mapOfGDgdjs_9546configurationCode_9546GDclavierObjects1Objects = Hashtable.newFrom({"clavier": gdjs.configurationCode.GDclavierObjects1});
gdjs.configurationCode.eventsList6 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("type_caracteres").getChild("clavier")) > 2;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(1).getChild("type_caracteres").getChild("clavier").setNumber(0);
}}

}


{


let isConditionTrue_0 = false;
{
{runtimeScene.getScene().getVariables().getFromIndex(0).setNumber(1);
}}

}


};gdjs.configurationCode.mapOfGDgdjs_9546configurationCode_9546GDtext_95959595modeleObjects1Objects = Hashtable.newFrom({"text_modele": gdjs.configurationCode.GDtext_9595modeleObjects1});
gdjs.configurationCode.eventsList7 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("type_caracteres").getChild("modele")) > 2;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(1).getChild("type_caracteres").getChild("modele").setNumber(0);
}}

}


{


let isConditionTrue_0 = false;
{
{runtimeScene.getScene().getVariables().getFromIndex(0).setNumber(1);
}}

}


};gdjs.configurationCode.eventsList8 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("clavier"), gdjs.configurationCode.GDclavierObjects1);
gdjs.copyArray(runtimeScene.getObjects("text_modele"), gdjs.configurationCode.GDtext_9595modeleObjects1);
{for(var i = 0, len = gdjs.configurationCode.GDtext_9595modeleObjects1.length ;i < len;++i) {
    gdjs.configurationCode.GDtext_9595modeleObjects1[i].getBehavior("Text").setText("modèle");
}
}{for(var i = 0, len = gdjs.configurationCode.GDclavierObjects1.length ;i < len;++i) {
    gdjs.configurationCode.GDclavierObjects1[i].getBehavior("Animation").pauseAnimation();
}
}{runtimeScene.getScene().getVariables().getFromIndex(0).setNumber(1);
}{for(var i = 0, len = gdjs.configurationCode.GDclavierObjects1.length ;i < len;++i) {
    gdjs.configurationCode.GDclavierObjects1[i].activateBehavior("ButtonScaleTween", false);
}
}{for(var i = 0, len = gdjs.configurationCode.GDtext_9595modeleObjects1.length ;i < len;++i) {
    gdjs.configurationCode.GDtext_9595modeleObjects1[i].setPadding(100);
}
}}

}


{


gdjs.configurationCode.eventsList3(runtimeScene);
}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_retour"), gdjs.configurationCode.GDbouton_9595retourObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.configurationCode.mapOfGDgdjs_9546configurationCode_9546GDbouton_95959595retourObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {

{ //Subevents
gdjs.configurationCode.eventsList5(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("clavier"), gdjs.configurationCode.GDclavierObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.configurationCode.mapOfGDgdjs_9546configurationCode_9546GDclavierObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(14008396);
}
}
}
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(1).getChild("type_caracteres").getChild("clavier").add(1);
}
{ //Subevents
gdjs.configurationCode.eventsList6(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("text_modele"), gdjs.configurationCode.GDtext_9595modeleObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.configurationCode.mapOfGDgdjs_9546configurationCode_9546GDtext_95959595modeleObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(14010020);
}
}
}
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(1).getChild("type_caracteres").getChild("modele").add(1);
}
{ //Subevents
gdjs.configurationCode.eventsList7(runtimeScene);} //End of subevents
}

}


};

gdjs.configurationCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.configurationCode.GDinfo_95951Objects1.length = 0;
gdjs.configurationCode.GDinfo_95951Objects2.length = 0;
gdjs.configurationCode.GDinfo_95951Objects3.length = 0;
gdjs.configurationCode.GDinfo_95951Objects4.length = 0;
gdjs.configurationCode.GDinfo_95952Objects1.length = 0;
gdjs.configurationCode.GDinfo_95952Objects2.length = 0;
gdjs.configurationCode.GDinfo_95952Objects3.length = 0;
gdjs.configurationCode.GDinfo_95952Objects4.length = 0;
gdjs.configurationCode.GDinfo_95953Objects1.length = 0;
gdjs.configurationCode.GDinfo_95953Objects2.length = 0;
gdjs.configurationCode.GDinfo_95953Objects3.length = 0;
gdjs.configurationCode.GDinfo_95953Objects4.length = 0;
gdjs.configurationCode.GDbouton_9595retourObjects1.length = 0;
gdjs.configurationCode.GDbouton_9595retourObjects2.length = 0;
gdjs.configurationCode.GDbouton_9595retourObjects3.length = 0;
gdjs.configurationCode.GDbouton_9595retourObjects4.length = 0;
gdjs.configurationCode.GDcoffreObjects1.length = 0;
gdjs.configurationCode.GDcoffreObjects2.length = 0;
gdjs.configurationCode.GDcoffreObjects3.length = 0;
gdjs.configurationCode.GDcoffreObjects4.length = 0;
gdjs.configurationCode.GDsp_9595diamantObjects1.length = 0;
gdjs.configurationCode.GDsp_9595diamantObjects2.length = 0;
gdjs.configurationCode.GDsp_9595diamantObjects3.length = 0;
gdjs.configurationCode.GDsp_9595diamantObjects4.length = 0;
gdjs.configurationCode.GDGreenDotBarObjects1.length = 0;
gdjs.configurationCode.GDGreenDotBarObjects2.length = 0;
gdjs.configurationCode.GDGreenDotBarObjects3.length = 0;
gdjs.configurationCode.GDGreenDotBarObjects4.length = 0;
gdjs.configurationCode.GDsp_9595cadre0Objects1.length = 0;
gdjs.configurationCode.GDsp_9595cadre0Objects2.length = 0;
gdjs.configurationCode.GDsp_9595cadre0Objects3.length = 0;
gdjs.configurationCode.GDsp_9595cadre0Objects4.length = 0;
gdjs.configurationCode.GDpapierObjects1.length = 0;
gdjs.configurationCode.GDpapierObjects2.length = 0;
gdjs.configurationCode.GDpapierObjects3.length = 0;
gdjs.configurationCode.GDpapierObjects4.length = 0;
gdjs.configurationCode.GDtext_9595modeleObjects1.length = 0;
gdjs.configurationCode.GDtext_9595modeleObjects2.length = 0;
gdjs.configurationCode.GDtext_9595modeleObjects3.length = 0;
gdjs.configurationCode.GDtext_9595modeleObjects4.length = 0;
gdjs.configurationCode.GDclavierObjects1.length = 0;
gdjs.configurationCode.GDclavierObjects2.length = 0;
gdjs.configurationCode.GDclavierObjects3.length = 0;
gdjs.configurationCode.GDclavierObjects4.length = 0;

gdjs.configurationCode.eventsList8(runtimeScene);
gdjs.configurationCode.GDinfo_95951Objects1.length = 0;
gdjs.configurationCode.GDinfo_95951Objects2.length = 0;
gdjs.configurationCode.GDinfo_95951Objects3.length = 0;
gdjs.configurationCode.GDinfo_95951Objects4.length = 0;
gdjs.configurationCode.GDinfo_95952Objects1.length = 0;
gdjs.configurationCode.GDinfo_95952Objects2.length = 0;
gdjs.configurationCode.GDinfo_95952Objects3.length = 0;
gdjs.configurationCode.GDinfo_95952Objects4.length = 0;
gdjs.configurationCode.GDinfo_95953Objects1.length = 0;
gdjs.configurationCode.GDinfo_95953Objects2.length = 0;
gdjs.configurationCode.GDinfo_95953Objects3.length = 0;
gdjs.configurationCode.GDinfo_95953Objects4.length = 0;
gdjs.configurationCode.GDbouton_9595retourObjects1.length = 0;
gdjs.configurationCode.GDbouton_9595retourObjects2.length = 0;
gdjs.configurationCode.GDbouton_9595retourObjects3.length = 0;
gdjs.configurationCode.GDbouton_9595retourObjects4.length = 0;
gdjs.configurationCode.GDcoffreObjects1.length = 0;
gdjs.configurationCode.GDcoffreObjects2.length = 0;
gdjs.configurationCode.GDcoffreObjects3.length = 0;
gdjs.configurationCode.GDcoffreObjects4.length = 0;
gdjs.configurationCode.GDsp_9595diamantObjects1.length = 0;
gdjs.configurationCode.GDsp_9595diamantObjects2.length = 0;
gdjs.configurationCode.GDsp_9595diamantObjects3.length = 0;
gdjs.configurationCode.GDsp_9595diamantObjects4.length = 0;
gdjs.configurationCode.GDGreenDotBarObjects1.length = 0;
gdjs.configurationCode.GDGreenDotBarObjects2.length = 0;
gdjs.configurationCode.GDGreenDotBarObjects3.length = 0;
gdjs.configurationCode.GDGreenDotBarObjects4.length = 0;
gdjs.configurationCode.GDsp_9595cadre0Objects1.length = 0;
gdjs.configurationCode.GDsp_9595cadre0Objects2.length = 0;
gdjs.configurationCode.GDsp_9595cadre0Objects3.length = 0;
gdjs.configurationCode.GDsp_9595cadre0Objects4.length = 0;
gdjs.configurationCode.GDpapierObjects1.length = 0;
gdjs.configurationCode.GDpapierObjects2.length = 0;
gdjs.configurationCode.GDpapierObjects3.length = 0;
gdjs.configurationCode.GDpapierObjects4.length = 0;
gdjs.configurationCode.GDtext_9595modeleObjects1.length = 0;
gdjs.configurationCode.GDtext_9595modeleObjects2.length = 0;
gdjs.configurationCode.GDtext_9595modeleObjects3.length = 0;
gdjs.configurationCode.GDtext_9595modeleObjects4.length = 0;
gdjs.configurationCode.GDclavierObjects1.length = 0;
gdjs.configurationCode.GDclavierObjects2.length = 0;
gdjs.configurationCode.GDclavierObjects3.length = 0;
gdjs.configurationCode.GDclavierObjects4.length = 0;


return;

}

gdjs['configurationCode'] = gdjs.configurationCode;
