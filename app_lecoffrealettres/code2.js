gdjs.jeuCode = {};
gdjs.jeuCode.localVariables = [];
gdjs.jeuCode.GDtext_9595modeleObjects1_1final = [];

gdjs.jeuCode.GDtext_9595modeleObjects2_1final = [];

gdjs.jeuCode.GDtext_9595entree_95951Objects1= [];
gdjs.jeuCode.GDtext_9595entree_95951Objects2= [];
gdjs.jeuCode.GDtext_9595entree_95951Objects3= [];
gdjs.jeuCode.GDtext_9595entree_95951Objects4= [];
gdjs.jeuCode.GDclic_9595caractereObjects1= [];
gdjs.jeuCode.GDclic_9595caractereObjects2= [];
gdjs.jeuCode.GDclic_9595caractereObjects3= [];
gdjs.jeuCode.GDclic_9595caractereObjects4= [];
gdjs.jeuCode.GDcadranObjects1= [];
gdjs.jeuCode.GDcadranObjects2= [];
gdjs.jeuCode.GDcadranObjects3= [];
gdjs.jeuCode.GDcadranObjects4= [];
gdjs.jeuCode.GDfaux_9595vraiObjects1= [];
gdjs.jeuCode.GDfaux_9595vraiObjects2= [];
gdjs.jeuCode.GDfaux_9595vraiObjects3= [];
gdjs.jeuCode.GDfaux_9595vraiObjects4= [];
gdjs.jeuCode.GDbbtext_9595modeleObjects1= [];
gdjs.jeuCode.GDbbtext_9595modeleObjects2= [];
gdjs.jeuCode.GDbbtext_9595modeleObjects3= [];
gdjs.jeuCode.GDbbtext_9595modeleObjects4= [];
gdjs.jeuCode.GDsp_9595bouton_9595recommencerObjects1= [];
gdjs.jeuCode.GDsp_9595bouton_9595recommencerObjects2= [];
gdjs.jeuCode.GDsp_9595bouton_9595recommencerObjects3= [];
gdjs.jeuCode.GDsp_9595bouton_9595recommencerObjects4= [];
gdjs.jeuCode.GDbouton_9595retourObjects1= [];
gdjs.jeuCode.GDbouton_9595retourObjects2= [];
gdjs.jeuCode.GDbouton_9595retourObjects3= [];
gdjs.jeuCode.GDbouton_9595retourObjects4= [];
gdjs.jeuCode.GDcoffreObjects1= [];
gdjs.jeuCode.GDcoffreObjects2= [];
gdjs.jeuCode.GDcoffreObjects3= [];
gdjs.jeuCode.GDcoffreObjects4= [];
gdjs.jeuCode.GDsp_9595diamantObjects1= [];
gdjs.jeuCode.GDsp_9595diamantObjects2= [];
gdjs.jeuCode.GDsp_9595diamantObjects3= [];
gdjs.jeuCode.GDsp_9595diamantObjects4= [];
gdjs.jeuCode.GDGreenDotBarObjects1= [];
gdjs.jeuCode.GDGreenDotBarObjects2= [];
gdjs.jeuCode.GDGreenDotBarObjects3= [];
gdjs.jeuCode.GDGreenDotBarObjects4= [];
gdjs.jeuCode.GDsp_9595cadre0Objects1= [];
gdjs.jeuCode.GDsp_9595cadre0Objects2= [];
gdjs.jeuCode.GDsp_9595cadre0Objects3= [];
gdjs.jeuCode.GDsp_9595cadre0Objects4= [];
gdjs.jeuCode.GDpapierObjects1= [];
gdjs.jeuCode.GDpapierObjects2= [];
gdjs.jeuCode.GDpapierObjects3= [];
gdjs.jeuCode.GDpapierObjects4= [];
gdjs.jeuCode.GDtext_9595modeleObjects1= [];
gdjs.jeuCode.GDtext_9595modeleObjects2= [];
gdjs.jeuCode.GDtext_9595modeleObjects3= [];
gdjs.jeuCode.GDtext_9595modeleObjects4= [];
gdjs.jeuCode.GDclavierObjects1= [];
gdjs.jeuCode.GDclavierObjects2= [];
gdjs.jeuCode.GDclavierObjects3= [];
gdjs.jeuCode.GDclavierObjects4= [];


gdjs.jeuCode.eventsList0 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("clavier"), gdjs.jeuCode.GDclavierObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeuCode.GDclavierObjects3.length;i<l;++i) {
    if ( gdjs.jeuCode.GDclavierObjects3[i].getBehavior("Animation").getAnimationName() != runtimeScene.getScene().getVariables().getFromIndex(0).getChild(0).getAsString() ) {
        isConditionTrue_0 = true;
        gdjs.jeuCode.GDclavierObjects3[k] = gdjs.jeuCode.GDclavierObjects3[i];
        ++k;
    }
}
gdjs.jeuCode.GDclavierObjects3.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeuCode.GDclavierObjects3.length;i<l;++i) {
    if ( gdjs.jeuCode.GDclavierObjects3[i].getBehavior("Animation").getAnimationName() != runtimeScene.getScene().getVariables().getFromIndex(0).getChild(1).getAsString() ) {
        isConditionTrue_0 = true;
        gdjs.jeuCode.GDclavierObjects3[k] = gdjs.jeuCode.GDclavierObjects3[i];
        ++k;
    }
}
gdjs.jeuCode.GDclavierObjects3.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeuCode.GDclavierObjects3.length;i<l;++i) {
    if ( gdjs.jeuCode.GDclavierObjects3[i].getBehavior("Animation").getAnimationName() != runtimeScene.getScene().getVariables().getFromIndex(0).getChild(2).getAsString() ) {
        isConditionTrue_0 = true;
        gdjs.jeuCode.GDclavierObjects3[k] = gdjs.jeuCode.GDclavierObjects3[i];
        ++k;
    }
}
gdjs.jeuCode.GDclavierObjects3.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeuCode.GDclavierObjects3.length;i<l;++i) {
    if ( gdjs.jeuCode.GDclavierObjects3[i].getBehavior("Animation").getAnimationName() != runtimeScene.getScene().getVariables().getFromIndex(0).getChild(3).getAsString() ) {
        isConditionTrue_0 = true;
        gdjs.jeuCode.GDclavierObjects3[k] = gdjs.jeuCode.GDclavierObjects3[i];
        ++k;
    }
}
gdjs.jeuCode.GDclavierObjects3.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeuCode.GDclavierObjects3.length;i<l;++i) {
    if ( gdjs.jeuCode.GDclavierObjects3[i].getBehavior("Animation").getAnimationName() != runtimeScene.getScene().getVariables().getFromIndex(0).getChild(4).getAsString() ) {
        isConditionTrue_0 = true;
        gdjs.jeuCode.GDclavierObjects3[k] = gdjs.jeuCode.GDclavierObjects3[i];
        ++k;
    }
}
gdjs.jeuCode.GDclavierObjects3.length = k;
}
}
}
}
if (isConditionTrue_0) {
/* Reuse gdjs.jeuCode.GDclavierObjects3 */
{for(var i = 0, len = gdjs.jeuCode.GDclavierObjects3.length ;i < len;++i) {
    gdjs.jeuCode.GDclavierObjects3[i].deleteFromScene(runtimeScene);
}
}}

}


};gdjs.jeuCode.eventsList1 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("clavier"), gdjs.jeuCode.GDclavierObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeuCode.GDclavierObjects2.length;i<l;++i) {
    if ( gdjs.jeuCode.GDclavierObjects2[i].getBehavior("Animation").getAnimationName() != runtimeScene.getScene().getVariables().getFromIndex(0).getChild(0).getAsString() ) {
        isConditionTrue_0 = true;
        gdjs.jeuCode.GDclavierObjects2[k] = gdjs.jeuCode.GDclavierObjects2[i];
        ++k;
    }
}
gdjs.jeuCode.GDclavierObjects2.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeuCode.GDclavierObjects2.length;i<l;++i) {
    if ( gdjs.jeuCode.GDclavierObjects2[i].getBehavior("Animation").getAnimationName() != runtimeScene.getScene().getVariables().getFromIndex(0).getChild(1).getAsString() ) {
        isConditionTrue_0 = true;
        gdjs.jeuCode.GDclavierObjects2[k] = gdjs.jeuCode.GDclavierObjects2[i];
        ++k;
    }
}
gdjs.jeuCode.GDclavierObjects2.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeuCode.GDclavierObjects2.length;i<l;++i) {
    if ( gdjs.jeuCode.GDclavierObjects2[i].getBehavior("Animation").getAnimationName() != runtimeScene.getScene().getVariables().getFromIndex(0).getChild(2).getAsString() ) {
        isConditionTrue_0 = true;
        gdjs.jeuCode.GDclavierObjects2[k] = gdjs.jeuCode.GDclavierObjects2[i];
        ++k;
    }
}
gdjs.jeuCode.GDclavierObjects2.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeuCode.GDclavierObjects2.length;i<l;++i) {
    if ( gdjs.jeuCode.GDclavierObjects2[i].getBehavior("Animation").getAnimationName() != runtimeScene.getScene().getVariables().getFromIndex(0).getChild(3).getAsString() ) {
        isConditionTrue_0 = true;
        gdjs.jeuCode.GDclavierObjects2[k] = gdjs.jeuCode.GDclavierObjects2[i];
        ++k;
    }
}
gdjs.jeuCode.GDclavierObjects2.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeuCode.GDclavierObjects2.length;i<l;++i) {
    if ( gdjs.jeuCode.GDclavierObjects2[i].getBehavior("Animation").getAnimationName() != runtimeScene.getScene().getVariables().getFromIndex(0).getChild(4).getAsString() ) {
        isConditionTrue_0 = true;
        gdjs.jeuCode.GDclavierObjects2[k] = gdjs.jeuCode.GDclavierObjects2[i];
        ++k;
    }
}
gdjs.jeuCode.GDclavierObjects2.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeuCode.GDclavierObjects2.length;i<l;++i) {
    if ( gdjs.jeuCode.GDclavierObjects2[i].getBehavior("Animation").getAnimationName() != runtimeScene.getScene().getVariables().getFromIndex(0).getChild(5).getAsString() ) {
        isConditionTrue_0 = true;
        gdjs.jeuCode.GDclavierObjects2[k] = gdjs.jeuCode.GDclavierObjects2[i];
        ++k;
    }
}
gdjs.jeuCode.GDclavierObjects2.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeuCode.GDclavierObjects2.length;i<l;++i) {
    if ( gdjs.jeuCode.GDclavierObjects2[i].getBehavior("Animation").getAnimationName() != runtimeScene.getScene().getVariables().getFromIndex(0).getChild(6).getAsString() ) {
        isConditionTrue_0 = true;
        gdjs.jeuCode.GDclavierObjects2[k] = gdjs.jeuCode.GDclavierObjects2[i];
        ++k;
    }
}
gdjs.jeuCode.GDclavierObjects2.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeuCode.GDclavierObjects2.length;i<l;++i) {
    if ( gdjs.jeuCode.GDclavierObjects2[i].getBehavior("Animation").getAnimationName() != runtimeScene.getScene().getVariables().getFromIndex(0).getChild(7).getAsString() ) {
        isConditionTrue_0 = true;
        gdjs.jeuCode.GDclavierObjects2[k] = gdjs.jeuCode.GDclavierObjects2[i];
        ++k;
    }
}
gdjs.jeuCode.GDclavierObjects2.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeuCode.GDclavierObjects2.length;i<l;++i) {
    if ( gdjs.jeuCode.GDclavierObjects2[i].getBehavior("Animation").getAnimationName() != runtimeScene.getScene().getVariables().getFromIndex(0).getChild(8).getAsString() ) {
        isConditionTrue_0 = true;
        gdjs.jeuCode.GDclavierObjects2[k] = gdjs.jeuCode.GDclavierObjects2[i];
        ++k;
    }
}
gdjs.jeuCode.GDclavierObjects2.length = k;
}
}
}
}
}
}
}
}
if (isConditionTrue_0) {
/* Reuse gdjs.jeuCode.GDclavierObjects2 */
{for(var i = 0, len = gdjs.jeuCode.GDclavierObjects2.length ;i < len;++i) {
    gdjs.jeuCode.GDclavierObjects2[i].deleteFromScene(runtimeScene);
}
}}

}


};gdjs.jeuCode.eventsList2 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 1;
if (isConditionTrue_0) {

{ //Subevents
gdjs.jeuCode.eventsList0(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 2;
if (isConditionTrue_0) {

{ //Subevents
gdjs.jeuCode.eventsList1(runtimeScene);} //End of subevents
}

}


};gdjs.jeuCode.eventsList3 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
{runtimeScene.getScene().getVariables().getFromIndex(3).setNumber(1);
}}

}


};gdjs.jeuCode.eventsList4 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
{gdjs.evtsExt__ArrayTools__Shuffle.func(runtimeScene, runtimeScene.getScene().getVariables().getFromIndex(0), (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("text_entree_1"), gdjs.jeuCode.GDtext_9595entree_95951Objects2);
gdjs.copyArray(runtimeScene.getObjects("text_modele"), gdjs.jeuCode.GDtext_9595modeleObjects2);
{for(var i = 0, len = gdjs.jeuCode.GDtext_9595modeleObjects2.length ;i < len;++i) {
    gdjs.jeuCode.GDtext_9595modeleObjects2[i].setPadding(80);
}
}{for(var i = 0, len = gdjs.jeuCode.GDtext_9595entree_95951Objects2.length ;i < len;++i) {
    gdjs.jeuCode.GDtext_9595entree_95951Objects2[i].setPadding(80);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("text_modele"), gdjs.jeuCode.GDtext_9595modeleObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeuCode.GDtext_9595modeleObjects2.length;i<l;++i) {
    if ( gdjs.jeuCode.GDtext_9595modeleObjects2[i].getVariableNumber(gdjs.jeuCode.GDtext_9595modeleObjects2[i].getVariables().getFromIndex(0)) == 0 ) {
        isConditionTrue_0 = true;
        gdjs.jeuCode.GDtext_9595modeleObjects2[k] = gdjs.jeuCode.GDtext_9595modeleObjects2[i];
        ++k;
    }
}
gdjs.jeuCode.GDtext_9595modeleObjects2.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.jeuCode.GDtext_9595modeleObjects2 */
{for(var i = 0, len = gdjs.jeuCode.GDtext_9595modeleObjects2.length ;i < len;++i) {
    gdjs.jeuCode.GDtext_9595modeleObjects2[i].getBehavior("Text").setText(runtimeScene.getScene().getVariables().getFromIndex(0).getChild(0).getAsString());
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("text_modele"), gdjs.jeuCode.GDtext_9595modeleObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeuCode.GDtext_9595modeleObjects2.length;i<l;++i) {
    if ( gdjs.jeuCode.GDtext_9595modeleObjects2[i].getVariableNumber(gdjs.jeuCode.GDtext_9595modeleObjects2[i].getVariables().getFromIndex(0)) == 1 ) {
        isConditionTrue_0 = true;
        gdjs.jeuCode.GDtext_9595modeleObjects2[k] = gdjs.jeuCode.GDtext_9595modeleObjects2[i];
        ++k;
    }
}
gdjs.jeuCode.GDtext_9595modeleObjects2.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.jeuCode.GDtext_9595modeleObjects2 */
{for(var i = 0, len = gdjs.jeuCode.GDtext_9595modeleObjects2.length ;i < len;++i) {
    gdjs.jeuCode.GDtext_9595modeleObjects2[i].getBehavior("Text").setText(runtimeScene.getScene().getVariables().getFromIndex(0).getChild(1).getAsString());
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("text_modele"), gdjs.jeuCode.GDtext_9595modeleObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeuCode.GDtext_9595modeleObjects2.length;i<l;++i) {
    if ( gdjs.jeuCode.GDtext_9595modeleObjects2[i].getVariableNumber(gdjs.jeuCode.GDtext_9595modeleObjects2[i].getVariables().getFromIndex(0)) == 2 ) {
        isConditionTrue_0 = true;
        gdjs.jeuCode.GDtext_9595modeleObjects2[k] = gdjs.jeuCode.GDtext_9595modeleObjects2[i];
        ++k;
    }
}
gdjs.jeuCode.GDtext_9595modeleObjects2.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.jeuCode.GDtext_9595modeleObjects2 */
{for(var i = 0, len = gdjs.jeuCode.GDtext_9595modeleObjects2.length ;i < len;++i) {
    gdjs.jeuCode.GDtext_9595modeleObjects2[i].getBehavior("Text").setText(runtimeScene.getScene().getVariables().getFromIndex(0).getChild(2).getAsString());
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("text_modele"), gdjs.jeuCode.GDtext_9595modeleObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeuCode.GDtext_9595modeleObjects2.length;i<l;++i) {
    if ( gdjs.jeuCode.GDtext_9595modeleObjects2[i].getVariableNumber(gdjs.jeuCode.GDtext_9595modeleObjects2[i].getVariables().getFromIndex(0)) == 3 ) {
        isConditionTrue_0 = true;
        gdjs.jeuCode.GDtext_9595modeleObjects2[k] = gdjs.jeuCode.GDtext_9595modeleObjects2[i];
        ++k;
    }
}
gdjs.jeuCode.GDtext_9595modeleObjects2.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.jeuCode.GDtext_9595modeleObjects2 */
{for(var i = 0, len = gdjs.jeuCode.GDtext_9595modeleObjects2.length ;i < len;++i) {
    gdjs.jeuCode.GDtext_9595modeleObjects2[i].getBehavior("Text").setText(runtimeScene.getScene().getVariables().getFromIndex(0).getChild(3).getAsString());
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("text_modele"), gdjs.jeuCode.GDtext_9595modeleObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeuCode.GDtext_9595modeleObjects2.length;i<l;++i) {
    if ( gdjs.jeuCode.GDtext_9595modeleObjects2[i].getVariableNumber(gdjs.jeuCode.GDtext_9595modeleObjects2[i].getVariables().getFromIndex(0)) == 4 ) {
        isConditionTrue_0 = true;
        gdjs.jeuCode.GDtext_9595modeleObjects2[k] = gdjs.jeuCode.GDtext_9595modeleObjects2[i];
        ++k;
    }
}
gdjs.jeuCode.GDtext_9595modeleObjects2.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.jeuCode.GDtext_9595modeleObjects2 */
{for(var i = 0, len = gdjs.jeuCode.GDtext_9595modeleObjects2.length ;i < len;++i) {
    gdjs.jeuCode.GDtext_9595modeleObjects2[i].getBehavior("Text").setText(runtimeScene.getScene().getVariables().getFromIndex(0).getChild(4).getAsString());
}
}}

}


{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("text_entree_1"), gdjs.jeuCode.GDtext_9595entree_95951Objects2);
{for(var i = 0, len = gdjs.jeuCode.GDtext_9595entree_95951Objects2.length ;i < len;++i) {
    gdjs.jeuCode.GDtext_9595entree_95951Objects2[i].getBehavior("Opacity").setOpacity(50);
}
}}

}


{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("clavier"), gdjs.jeuCode.GDclavierObjects2);
{for(var i = 0, len = gdjs.jeuCode.GDclavierObjects2.length ;i < len;++i) {
    gdjs.jeuCode.GDclavierObjects2[i].getBehavior("Animation").pauseAnimation();
}
}{for(var i = 0, len = gdjs.jeuCode.GDclavierObjects2.length ;i < len;++i) {
    gdjs.jeuCode.GDclavierObjects2[i].getBehavior("Animation").setAnimationIndex(gdjs.jeuCode.GDclavierObjects2[i].getVariables().getFromIndex(0).getAsNumber());
}
}{runtimeScene.getScene().getVariables().getFromIndex(6).setNumber(1);
}}

}


{


gdjs.jeuCode.eventsList2(runtimeScene);
}


{


gdjs.jeuCode.eventsList3(runtimeScene);
}


};gdjs.jeuCode.eventsList5 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 1;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("GreenDotBar"), gdjs.jeuCode.GDGreenDotBarObjects2);
{for(var i = 0, len = gdjs.jeuCode.GDGreenDotBarObjects2.length ;i < len;++i) {
    gdjs.jeuCode.GDGreenDotBarObjects2[i].SetValue(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("score").getChild("niv1").getAsNumber(), (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 2;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("GreenDotBar"), gdjs.jeuCode.GDGreenDotBarObjects2);
{for(var i = 0, len = gdjs.jeuCode.GDGreenDotBarObjects2.length ;i < len;++i) {
    gdjs.jeuCode.GDGreenDotBarObjects2[i].SetValue(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("score").getChild("niv2").getAsNumber(), (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 3;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("GreenDotBar"), gdjs.jeuCode.GDGreenDotBarObjects2);
{for(var i = 0, len = gdjs.jeuCode.GDGreenDotBarObjects2.length ;i < len;++i) {
    gdjs.jeuCode.GDGreenDotBarObjects2[i].SetValue(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("score").getChild("niv3").getAsNumber(), (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
}}

}


{


let isConditionTrue_0 = false;
{
{runtimeScene.getScene().getVariables().getFromIndex(3).setNumber(0);
}}

}


};gdjs.jeuCode.eventsList6 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(3)) == 1;
if (isConditionTrue_0) {

{ //Subevents
gdjs.jeuCode.eventsList5(runtimeScene);} //End of subevents
}

}


};gdjs.jeuCode.mapOfGDgdjs_9546jeuCode_9546GDclavierObjects1Objects = Hashtable.newFrom({"clavier": gdjs.jeuCode.GDclavierObjects1});
gdjs.jeuCode.eventsList7 = function(runtimeScene) {

{

gdjs.copyArray(gdjs.jeuCode.GDtext_9595entree_95951Objects1, gdjs.jeuCode.GDtext_9595entree_95951Objects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeuCode.GDtext_9595entree_95951Objects3.length;i<l;++i) {
    if ( gdjs.jeuCode.GDtext_9595entree_95951Objects3[i].getBehavior("Text").getText() != runtimeScene.getScene().getVariables().getFromIndex(0).getChild(runtimeScene.getScene().getVariables().getFromIndex(1).getAsNumber()).getAsString() ) {
        isConditionTrue_0 = true;
        gdjs.jeuCode.GDtext_9595entree_95951Objects3[k] = gdjs.jeuCode.GDtext_9595entree_95951Objects3[i];
        ++k;
    }
}
gdjs.jeuCode.GDtext_9595entree_95951Objects3.length = k;
if (isConditionTrue_0) {
{runtimeScene.getScene().getVariables().getFromIndex(4).add(1);
}}

}


{

gdjs.copyArray(gdjs.jeuCode.GDtext_9595entree_95951Objects1, gdjs.jeuCode.GDtext_9595entree_95951Objects2);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeuCode.GDtext_9595entree_95951Objects2.length;i<l;++i) {
    if ( gdjs.jeuCode.GDtext_9595entree_95951Objects2[i].getBehavior("Text").getText() == runtimeScene.getScene().getVariables().getFromIndex(0).getChild(runtimeScene.getScene().getVariables().getFromIndex(1).getAsNumber()).getAsString() ) {
        isConditionTrue_0 = true;
        gdjs.jeuCode.GDtext_9595entree_95951Objects2[k] = gdjs.jeuCode.GDtext_9595entree_95951Objects2[i];
        ++k;
    }
}
gdjs.jeuCode.GDtext_9595entree_95951Objects2.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeuCode.GDtext_9595entree_95951Objects2.length;i<l;++i) {
    if ( gdjs.jeuCode.GDtext_9595entree_95951Objects2[i].getBehavior("Text").getText() == runtimeScene.getScene().getVariables().getFromIndex(0).getChild(runtimeScene.getScene().getVariables().getFromIndex(1).getAsNumber()).getAsString() ) {
        isConditionTrue_0 = true;
        gdjs.jeuCode.GDtext_9595entree_95951Objects2[k] = gdjs.jeuCode.GDtext_9595entree_95951Objects2[i];
        ++k;
    }
}
gdjs.jeuCode.GDtext_9595entree_95951Objects2.length = k;
}
if (isConditionTrue_0) {
/* Reuse gdjs.jeuCode.GDfaux_9595vraiObjects2 */
/* Reuse gdjs.jeuCode.GDtext_9595entree_95951Objects2 */
{for(var i = 0, len = gdjs.jeuCode.GDtext_9595entree_95951Objects2.length ;i < len;++i) {
    gdjs.jeuCode.GDtext_9595entree_95951Objects2[i].setColor("74;144;226");
}
}{for(var i = 0, len = gdjs.jeuCode.GDfaux_9595vraiObjects2.length ;i < len;++i) {
    gdjs.jeuCode.GDfaux_9595vraiObjects2[i].getBehavior("Animation").setAnimationIndex(1);
}
}{runtimeScene.getScene().getVariables().getFromIndex(1).add(1);
}}

}


};gdjs.jeuCode.eventsList8 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("faux_vrai"), gdjs.jeuCode.GDfaux_9595vraiObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeuCode.GDfaux_9595vraiObjects2.length;i<l;++i) {
    if ( gdjs.jeuCode.GDfaux_9595vraiObjects2[i].getVariableNumber(gdjs.jeuCode.GDfaux_9595vraiObjects2[i].getVariables().getFromIndex(0)) == runtimeScene.getScene().getVariables().getFromIndex(1).getAsNumber() ) {
        isConditionTrue_0 = true;
        gdjs.jeuCode.GDfaux_9595vraiObjects2[k] = gdjs.jeuCode.GDfaux_9595vraiObjects2[i];
        ++k;
    }
}
gdjs.jeuCode.GDfaux_9595vraiObjects2.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.jeuCode.GDfaux_9595vraiObjects2 */
{for(var i = 0, len = gdjs.jeuCode.GDfaux_9595vraiObjects2.length ;i < len;++i) {
    gdjs.jeuCode.GDfaux_9595vraiObjects2[i].hide(false);
}
}
{ //Subevents
gdjs.jeuCode.eventsList7(runtimeScene);} //End of subevents
}

}


{



}


{


let isConditionTrue_0 = false;
{
{runtimeScene.getScene().getVariables().getFromIndex(6).setNumber(1);
}}

}


};gdjs.jeuCode.eventsList9 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("text_entree_1"), gdjs.jeuCode.GDtext_9595entree_95951Objects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeuCode.GDtext_9595entree_95951Objects1.length;i<l;++i) {
    if ( gdjs.jeuCode.GDtext_9595entree_95951Objects1[i].getVariableNumber(gdjs.jeuCode.GDtext_9595entree_95951Objects1[i].getVariables().getFromIndex(0)) == runtimeScene.getScene().getVariables().getFromIndex(1).getAsNumber() ) {
        isConditionTrue_0 = true;
        gdjs.jeuCode.GDtext_9595entree_95951Objects1[k] = gdjs.jeuCode.GDtext_9595entree_95951Objects1[i];
        ++k;
    }
}
gdjs.jeuCode.GDtext_9595entree_95951Objects1.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.jeuCode.GDclavierObjects1 */
/* Reuse gdjs.jeuCode.GDtext_9595entree_95951Objects1 */
{for(var i = 0, len = gdjs.jeuCode.GDtext_9595entree_95951Objects1.length ;i < len;++i) {
    gdjs.jeuCode.GDtext_9595entree_95951Objects1[i].getBehavior("Text").setText((( gdjs.jeuCode.GDclavierObjects1.length === 0 ) ? "" :gdjs.jeuCode.GDclavierObjects1[0].getBehavior("Animation").getAnimationName()));
}
}
{ //Subevents
gdjs.jeuCode.eventsList8(runtimeScene);} //End of subevents
}

}


};gdjs.jeuCode.eventsList10 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("clavier"), gdjs.jeuCode.GDclavierObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.jeuCode.mapOfGDgdjs_9546jeuCode_9546GDclavierObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {

{ //Subevents
gdjs.jeuCode.eventsList9(runtimeScene);} //End of subevents
}

}


};gdjs.jeuCode.eventsList11 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("type_caracteres").getChild("modele")) == 0;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("text_modele"), gdjs.jeuCode.GDtext_9595modeleObjects3);
{for(var i = 0, len = gdjs.jeuCode.GDtext_9595modeleObjects3.length ;i < len;++i) {
    gdjs.jeuCode.GDtext_9595modeleObjects3[i].setFontName("polices/BelleAllureScript2i-Gros.otf");
}
}{for(var i = 0, len = gdjs.jeuCode.GDtext_9595modeleObjects3.length ;i < len;++i) {
    gdjs.jeuCode.GDtext_9595modeleObjects3[i].getBehavior("Text").setText(gdjs.evtTools.string.toUpperCase((gdjs.jeuCode.GDtext_9595modeleObjects3[i].getBehavior("Text").getText())));
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("type_caracteres").getChild("modele")) == 1;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("text_modele"), gdjs.jeuCode.GDtext_9595modeleObjects3);
{for(var i = 0, len = gdjs.jeuCode.GDtext_9595modeleObjects3.length ;i < len;++i) {
    gdjs.jeuCode.GDtext_9595modeleObjects3[i].setFontName("polices/BelleAllureScript2i-Gros.otf");
}
}{for(var i = 0, len = gdjs.jeuCode.GDtext_9595modeleObjects3.length ;i < len;++i) {
    gdjs.jeuCode.GDtext_9595modeleObjects3[i].getBehavior("Text").setText(gdjs.evtTools.string.toLowerCase((gdjs.jeuCode.GDtext_9595modeleObjects3[i].getBehavior("Text").getText())));
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("type_caracteres").getChild("modele")) == 2;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("text_modele"), gdjs.jeuCode.GDtext_9595modeleObjects3);
{for(var i = 0, len = gdjs.jeuCode.GDtext_9595modeleObjects3.length ;i < len;++i) {
    gdjs.jeuCode.GDtext_9595modeleObjects3[i].setFontName("polices/BelleAllureGS-Gros.otf");
}
}}

}


{


let isConditionTrue_0 = false;
{
}

}


};gdjs.jeuCode.eventsList12 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("type_caracteres").getChild("clavier")) == 0;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("clavier"), gdjs.jeuCode.GDclavierObjects3);
gdjs.copyArray(runtimeScene.getObjects("text_entree_1"), gdjs.jeuCode.GDtext_9595entree_95951Objects3);
{for(var i = 0, len = gdjs.jeuCode.GDclavierObjects3.length ;i < len;++i) {
    gdjs.jeuCode.GDclavierObjects3[i].setAnimationFrame(0);
}
}{for(var i = 0, len = gdjs.jeuCode.GDtext_9595entree_95951Objects3.length ;i < len;++i) {
    gdjs.jeuCode.GDtext_9595entree_95951Objects3[i].setFontName("polices/BelleAllureScript2i-Gros.otf");
}
}{for(var i = 0, len = gdjs.jeuCode.GDtext_9595entree_95951Objects3.length ;i < len;++i) {
    gdjs.jeuCode.GDtext_9595entree_95951Objects3[i].getBehavior("Text").setText(gdjs.evtTools.string.toUpperCase((gdjs.jeuCode.GDtext_9595entree_95951Objects3[i].getBehavior("Text").getText())));
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("type_caracteres").getChild("clavier")) == 1;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("clavier"), gdjs.jeuCode.GDclavierObjects3);
gdjs.copyArray(runtimeScene.getObjects("text_entree_1"), gdjs.jeuCode.GDtext_9595entree_95951Objects3);
{for(var i = 0, len = gdjs.jeuCode.GDclavierObjects3.length ;i < len;++i) {
    gdjs.jeuCode.GDclavierObjects3[i].setAnimationFrame(1);
}
}{for(var i = 0, len = gdjs.jeuCode.GDtext_9595entree_95951Objects3.length ;i < len;++i) {
    gdjs.jeuCode.GDtext_9595entree_95951Objects3[i].setFontName("polices/BelleAllureScript2i-Gros.otf");
}
}{for(var i = 0, len = gdjs.jeuCode.GDtext_9595entree_95951Objects3.length ;i < len;++i) {
    gdjs.jeuCode.GDtext_9595entree_95951Objects3[i].getBehavior("Text").setText(gdjs.evtTools.string.toLowerCase((gdjs.jeuCode.GDtext_9595entree_95951Objects3[i].getBehavior("Text").getText())));
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("type_caracteres").getChild("clavier")) == 2;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("clavier"), gdjs.jeuCode.GDclavierObjects3);
gdjs.copyArray(runtimeScene.getObjects("text_entree_1"), gdjs.jeuCode.GDtext_9595entree_95951Objects3);
{for(var i = 0, len = gdjs.jeuCode.GDclavierObjects3.length ;i < len;++i) {
    gdjs.jeuCode.GDclavierObjects3[i].setAnimationFrame(2);
}
}{for(var i = 0, len = gdjs.jeuCode.GDtext_9595entree_95951Objects3.length ;i < len;++i) {
    gdjs.jeuCode.GDtext_9595entree_95951Objects3[i].setFontName("polices/BelleAllureGS-Gros.otf");
}
}}

}


{


let isConditionTrue_0 = false;
{
}

}


};gdjs.jeuCode.eventsList13 = function(runtimeScene) {

{


gdjs.jeuCode.eventsList11(runtimeScene);
}


{


gdjs.jeuCode.eventsList12(runtimeScene);
}


{


let isConditionTrue_0 = false;
{
{runtimeScene.getScene().getVariables().getFromIndex(6).setNumber(0);
}}

}


};gdjs.jeuCode.eventsList14 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(6)) == 1;
if (isConditionTrue_0) {

{ //Subevents
gdjs.jeuCode.eventsList13(runtimeScene);} //End of subevents
}

}


};gdjs.jeuCode.mapOfGDgdjs_9546jeuCode_9546GDclic_95959595caractereObjects1Objects = Hashtable.newFrom({"clic_caractere": gdjs.jeuCode.GDclic_9595caractereObjects1});
gdjs.jeuCode.eventsList15 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(2).getChild("modele")) > 2;
if (isConditionTrue_0) {
{runtimeScene.getScene().getVariables().getFromIndex(2).getChild("modele").setNumber(0);
}}

}


};gdjs.jeuCode.eventsList16 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(2).getChild("entree")) > 2;
if (isConditionTrue_0) {
{runtimeScene.getScene().getVariables().getFromIndex(2).getChild("entree").setNumber(0);
}}

}


};gdjs.jeuCode.eventsList17 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(2).getChild("clavier")) > 2;
if (isConditionTrue_0) {
{runtimeScene.getScene().getVariables().getFromIndex(2).getChild("clavier").setNumber(0);
}}

}


};gdjs.jeuCode.eventsList18 = function(runtimeScene) {

{

gdjs.copyArray(gdjs.jeuCode.GDclic_9595caractereObjects1, gdjs.jeuCode.GDclic_9595caractereObjects2);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeuCode.GDclic_9595caractereObjects2.length;i<l;++i) {
    if ( gdjs.jeuCode.GDclic_9595caractereObjects2[i].getVariableNumber(gdjs.jeuCode.GDclic_9595caractereObjects2[i].getVariables().getFromIndex(0)) == 0 ) {
        isConditionTrue_0 = true;
        gdjs.jeuCode.GDclic_9595caractereObjects2[k] = gdjs.jeuCode.GDclic_9595caractereObjects2[i];
        ++k;
    }
}
gdjs.jeuCode.GDclic_9595caractereObjects2.length = k;
if (isConditionTrue_0) {
{runtimeScene.getScene().getVariables().getFromIndex(2).getChild("modele").add(1);
}
{ //Subevents
gdjs.jeuCode.eventsList15(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(gdjs.jeuCode.GDclic_9595caractereObjects1, gdjs.jeuCode.GDclic_9595caractereObjects2);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeuCode.GDclic_9595caractereObjects2.length;i<l;++i) {
    if ( gdjs.jeuCode.GDclic_9595caractereObjects2[i].getVariableNumber(gdjs.jeuCode.GDclic_9595caractereObjects2[i].getVariables().getFromIndex(0)) == 1 ) {
        isConditionTrue_0 = true;
        gdjs.jeuCode.GDclic_9595caractereObjects2[k] = gdjs.jeuCode.GDclic_9595caractereObjects2[i];
        ++k;
    }
}
gdjs.jeuCode.GDclic_9595caractereObjects2.length = k;
if (isConditionTrue_0) {
{runtimeScene.getScene().getVariables().getFromIndex(2).getChild("entree").add(1);
}
{ //Subevents
gdjs.jeuCode.eventsList16(runtimeScene);} //End of subevents
}

}


{

/* Reuse gdjs.jeuCode.GDclic_9595caractereObjects1 */

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeuCode.GDclic_9595caractereObjects1.length;i<l;++i) {
    if ( gdjs.jeuCode.GDclic_9595caractereObjects1[i].getVariableNumber(gdjs.jeuCode.GDclic_9595caractereObjects1[i].getVariables().getFromIndex(0)) == 2 ) {
        isConditionTrue_0 = true;
        gdjs.jeuCode.GDclic_9595caractereObjects1[k] = gdjs.jeuCode.GDclic_9595caractereObjects1[i];
        ++k;
    }
}
gdjs.jeuCode.GDclic_9595caractereObjects1.length = k;
if (isConditionTrue_0) {
{runtimeScene.getScene().getVariables().getFromIndex(2).getChild("clavier").add(1);
}
{ //Subevents
gdjs.jeuCode.eventsList17(runtimeScene);} //End of subevents
}

}


};gdjs.jeuCode.eventsList19 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("clic_caractere"), gdjs.jeuCode.GDclic_9595caractereObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.jeuCode.mapOfGDgdjs_9546jeuCode_9546GDclic_95959595caractereObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {

{ //Subevents
gdjs.jeuCode.eventsList18(runtimeScene);} //End of subevents
}

}


};gdjs.jeuCode.mapOfGDgdjs_9546jeuCode_9546GDbouton_95959595retourObjects1Objects = Hashtable.newFrom({"bouton_retour": gdjs.jeuCode.GDbouton_9595retourObjects1});
gdjs.jeuCode.eventsList20 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
{gdjs.evtTools.storage.clearJSONFile("sauvegarde_lecoffrealettres");
}{gdjs.evtTools.storage.writeStringInJSONFile("sauvegarde_lecoffrealettres", "reglages", gdjs.evtTools.network.variableStructureToJSON(runtimeScene.getGame().getVariables().getFromIndex(1)));
}}

}


};gdjs.jeuCode.eventsList21 = function(runtimeScene) {

{


gdjs.jeuCode.eventsList20(runtimeScene);
}


{


let isConditionTrue_0 = false;
{
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "menu", false);
}}

}


};gdjs.jeuCode.mapOfGDgdjs_9546jeuCode_9546GDsp_95959595bouton_95959595recommencerObjects1Objects = Hashtable.newFrom({"sp_bouton_recommencer": gdjs.jeuCode.GDsp_9595bouton_9595recommencerObjects1});
gdjs.jeuCode.eventsList22 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
{gdjs.evtTools.storage.clearJSONFile("sauvegarde_lecoffrealettres");
}{gdjs.evtTools.storage.writeStringInJSONFile("sauvegarde_lecoffrealettres", "reglages", gdjs.evtTools.network.variableStructureToJSON(runtimeScene.getGame().getVariables().getFromIndex(1)));
}}

}


};gdjs.jeuCode.eventsList23 = function(runtimeScene) {

{


gdjs.jeuCode.eventsList22(runtimeScene);
}


{


let isConditionTrue_0 = false;
{
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "jeu", false);
}}

}


};gdjs.jeuCode.eventsList24 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 1;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(1).getChild("score").getChild("niv1").add(runtimeScene.getScene().getVariables().getFromIndex(5).getAsNumber());
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 2;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(1).getChild("score").getChild("niv2").add(runtimeScene.getScene().getVariables().getFromIndex(5).getAsNumber());
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 3;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(1).getChild("score").getChild("niv3").add(runtimeScene.getScene().getVariables().getFromIndex(5).getAsNumber());
}}

}


{


let isConditionTrue_0 = false;
{
{runtimeScene.getScene().getVariables().getFromIndex(3).setNumber(1);
}}

}


};gdjs.jeuCode.eventsList25 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("faux_vrai"), gdjs.jeuCode.GDfaux_9595vraiObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeuCode.GDfaux_9595vraiObjects2.length;i<l;++i) {
    if ( gdjs.jeuCode.GDfaux_9595vraiObjects2[i].getVariableNumber(gdjs.jeuCode.GDfaux_9595vraiObjects2[i].getVariables().getFromIndex(0)) == 5 ) {
        isConditionTrue_0 = true;
        gdjs.jeuCode.GDfaux_9595vraiObjects2[k] = gdjs.jeuCode.GDfaux_9595vraiObjects2[i];
        ++k;
    }
}
gdjs.jeuCode.GDfaux_9595vraiObjects2.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.jeuCode.GDfaux_9595vraiObjects2 */
{for(var i = 0, len = gdjs.jeuCode.GDfaux_9595vraiObjects2.length ;i < len;++i) {
    gdjs.jeuCode.GDfaux_9595vraiObjects2[i].hide(false);
}
}{for(var i = 0, len = gdjs.jeuCode.GDfaux_9595vraiObjects2.length ;i < len;++i) {
    gdjs.jeuCode.GDfaux_9595vraiObjects2[i].getBehavior("Animation").setAnimationIndex(1);
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(5)) > 0;
if (isConditionTrue_0) {

{ //Subevents
gdjs.jeuCode.eventsList24(runtimeScene);} //End of subevents
}

}


};gdjs.jeuCode.mapOfGDgdjs_9546jeuCode_9546GDclavierObjects2Objects = Hashtable.newFrom({"clavier": gdjs.jeuCode.GDclavierObjects2});
gdjs.jeuCode.eventsList26 = function(runtimeScene) {

{

gdjs.copyArray(gdjs.jeuCode.GDclavierObjects2, gdjs.jeuCode.GDclavierObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeuCode.GDclavierObjects3.length;i<l;++i) {
    if ( gdjs.jeuCode.GDclavierObjects3[i].getBehavior("Animation").getAnimationName() == "a" ) {
        isConditionTrue_0 = true;
        gdjs.jeuCode.GDclavierObjects3[k] = gdjs.jeuCode.GDclavierObjects3[i];
        ++k;
    }
}
gdjs.jeuCode.GDclavierObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/a.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeuCode.GDclavierObjects2, gdjs.jeuCode.GDclavierObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeuCode.GDclavierObjects3.length;i<l;++i) {
    if ( gdjs.jeuCode.GDclavierObjects3[i].getBehavior("Animation").getAnimationName() == "b" ) {
        isConditionTrue_0 = true;
        gdjs.jeuCode.GDclavierObjects3[k] = gdjs.jeuCode.GDclavierObjects3[i];
        ++k;
    }
}
gdjs.jeuCode.GDclavierObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/b.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeuCode.GDclavierObjects2, gdjs.jeuCode.GDclavierObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeuCode.GDclavierObjects3.length;i<l;++i) {
    if ( gdjs.jeuCode.GDclavierObjects3[i].getBehavior("Animation").getAnimationName() == "c" ) {
        isConditionTrue_0 = true;
        gdjs.jeuCode.GDclavierObjects3[k] = gdjs.jeuCode.GDclavierObjects3[i];
        ++k;
    }
}
gdjs.jeuCode.GDclavierObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/c.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeuCode.GDclavierObjects2, gdjs.jeuCode.GDclavierObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeuCode.GDclavierObjects3.length;i<l;++i) {
    if ( gdjs.jeuCode.GDclavierObjects3[i].getBehavior("Animation").getAnimationName() == "d" ) {
        isConditionTrue_0 = true;
        gdjs.jeuCode.GDclavierObjects3[k] = gdjs.jeuCode.GDclavierObjects3[i];
        ++k;
    }
}
gdjs.jeuCode.GDclavierObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/d.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeuCode.GDclavierObjects2, gdjs.jeuCode.GDclavierObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeuCode.GDclavierObjects3.length;i<l;++i) {
    if ( gdjs.jeuCode.GDclavierObjects3[i].getBehavior("Animation").getAnimationName() == "e" ) {
        isConditionTrue_0 = true;
        gdjs.jeuCode.GDclavierObjects3[k] = gdjs.jeuCode.GDclavierObjects3[i];
        ++k;
    }
}
gdjs.jeuCode.GDclavierObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/e.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeuCode.GDclavierObjects2, gdjs.jeuCode.GDclavierObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeuCode.GDclavierObjects3.length;i<l;++i) {
    if ( gdjs.jeuCode.GDclavierObjects3[i].getBehavior("Animation").getAnimationName() == "f" ) {
        isConditionTrue_0 = true;
        gdjs.jeuCode.GDclavierObjects3[k] = gdjs.jeuCode.GDclavierObjects3[i];
        ++k;
    }
}
gdjs.jeuCode.GDclavierObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/f.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeuCode.GDclavierObjects2, gdjs.jeuCode.GDclavierObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeuCode.GDclavierObjects3.length;i<l;++i) {
    if ( gdjs.jeuCode.GDclavierObjects3[i].getBehavior("Animation").getAnimationName() == "g" ) {
        isConditionTrue_0 = true;
        gdjs.jeuCode.GDclavierObjects3[k] = gdjs.jeuCode.GDclavierObjects3[i];
        ++k;
    }
}
gdjs.jeuCode.GDclavierObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/g.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeuCode.GDclavierObjects2, gdjs.jeuCode.GDclavierObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeuCode.GDclavierObjects3.length;i<l;++i) {
    if ( gdjs.jeuCode.GDclavierObjects3[i].getBehavior("Animation").getAnimationName() == "h" ) {
        isConditionTrue_0 = true;
        gdjs.jeuCode.GDclavierObjects3[k] = gdjs.jeuCode.GDclavierObjects3[i];
        ++k;
    }
}
gdjs.jeuCode.GDclavierObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/h.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeuCode.GDclavierObjects2, gdjs.jeuCode.GDclavierObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeuCode.GDclavierObjects3.length;i<l;++i) {
    if ( gdjs.jeuCode.GDclavierObjects3[i].getBehavior("Animation").getAnimationName() == "i" ) {
        isConditionTrue_0 = true;
        gdjs.jeuCode.GDclavierObjects3[k] = gdjs.jeuCode.GDclavierObjects3[i];
        ++k;
    }
}
gdjs.jeuCode.GDclavierObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/i.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeuCode.GDclavierObjects2, gdjs.jeuCode.GDclavierObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeuCode.GDclavierObjects3.length;i<l;++i) {
    if ( gdjs.jeuCode.GDclavierObjects3[i].getBehavior("Animation").getAnimationName() == "j" ) {
        isConditionTrue_0 = true;
        gdjs.jeuCode.GDclavierObjects3[k] = gdjs.jeuCode.GDclavierObjects3[i];
        ++k;
    }
}
gdjs.jeuCode.GDclavierObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/j.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeuCode.GDclavierObjects2, gdjs.jeuCode.GDclavierObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeuCode.GDclavierObjects3.length;i<l;++i) {
    if ( gdjs.jeuCode.GDclavierObjects3[i].getBehavior("Animation").getAnimationName() == "k" ) {
        isConditionTrue_0 = true;
        gdjs.jeuCode.GDclavierObjects3[k] = gdjs.jeuCode.GDclavierObjects3[i];
        ++k;
    }
}
gdjs.jeuCode.GDclavierObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/k.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeuCode.GDclavierObjects2, gdjs.jeuCode.GDclavierObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeuCode.GDclavierObjects3.length;i<l;++i) {
    if ( gdjs.jeuCode.GDclavierObjects3[i].getBehavior("Animation").getAnimationName() == "l" ) {
        isConditionTrue_0 = true;
        gdjs.jeuCode.GDclavierObjects3[k] = gdjs.jeuCode.GDclavierObjects3[i];
        ++k;
    }
}
gdjs.jeuCode.GDclavierObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/l.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeuCode.GDclavierObjects2, gdjs.jeuCode.GDclavierObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeuCode.GDclavierObjects3.length;i<l;++i) {
    if ( gdjs.jeuCode.GDclavierObjects3[i].getBehavior("Animation").getAnimationName() == "m" ) {
        isConditionTrue_0 = true;
        gdjs.jeuCode.GDclavierObjects3[k] = gdjs.jeuCode.GDclavierObjects3[i];
        ++k;
    }
}
gdjs.jeuCode.GDclavierObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/m.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeuCode.GDclavierObjects2, gdjs.jeuCode.GDclavierObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeuCode.GDclavierObjects3.length;i<l;++i) {
    if ( gdjs.jeuCode.GDclavierObjects3[i].getBehavior("Animation").getAnimationName() == "n" ) {
        isConditionTrue_0 = true;
        gdjs.jeuCode.GDclavierObjects3[k] = gdjs.jeuCode.GDclavierObjects3[i];
        ++k;
    }
}
gdjs.jeuCode.GDclavierObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/n.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeuCode.GDclavierObjects2, gdjs.jeuCode.GDclavierObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeuCode.GDclavierObjects3.length;i<l;++i) {
    if ( gdjs.jeuCode.GDclavierObjects3[i].getBehavior("Animation").getAnimationName() == "o" ) {
        isConditionTrue_0 = true;
        gdjs.jeuCode.GDclavierObjects3[k] = gdjs.jeuCode.GDclavierObjects3[i];
        ++k;
    }
}
gdjs.jeuCode.GDclavierObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/o.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeuCode.GDclavierObjects2, gdjs.jeuCode.GDclavierObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeuCode.GDclavierObjects3.length;i<l;++i) {
    if ( gdjs.jeuCode.GDclavierObjects3[i].getBehavior("Animation").getAnimationName() == "p" ) {
        isConditionTrue_0 = true;
        gdjs.jeuCode.GDclavierObjects3[k] = gdjs.jeuCode.GDclavierObjects3[i];
        ++k;
    }
}
gdjs.jeuCode.GDclavierObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/p.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeuCode.GDclavierObjects2, gdjs.jeuCode.GDclavierObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeuCode.GDclavierObjects3.length;i<l;++i) {
    if ( gdjs.jeuCode.GDclavierObjects3[i].getBehavior("Animation").getAnimationName() == "q" ) {
        isConditionTrue_0 = true;
        gdjs.jeuCode.GDclavierObjects3[k] = gdjs.jeuCode.GDclavierObjects3[i];
        ++k;
    }
}
gdjs.jeuCode.GDclavierObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/q.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeuCode.GDclavierObjects2, gdjs.jeuCode.GDclavierObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeuCode.GDclavierObjects3.length;i<l;++i) {
    if ( gdjs.jeuCode.GDclavierObjects3[i].getBehavior("Animation").getAnimationName() == "r" ) {
        isConditionTrue_0 = true;
        gdjs.jeuCode.GDclavierObjects3[k] = gdjs.jeuCode.GDclavierObjects3[i];
        ++k;
    }
}
gdjs.jeuCode.GDclavierObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/r.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeuCode.GDclavierObjects2, gdjs.jeuCode.GDclavierObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeuCode.GDclavierObjects3.length;i<l;++i) {
    if ( gdjs.jeuCode.GDclavierObjects3[i].getBehavior("Animation").getAnimationName() == "s" ) {
        isConditionTrue_0 = true;
        gdjs.jeuCode.GDclavierObjects3[k] = gdjs.jeuCode.GDclavierObjects3[i];
        ++k;
    }
}
gdjs.jeuCode.GDclavierObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/s.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeuCode.GDclavierObjects2, gdjs.jeuCode.GDclavierObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeuCode.GDclavierObjects3.length;i<l;++i) {
    if ( gdjs.jeuCode.GDclavierObjects3[i].getBehavior("Animation").getAnimationName() == "t" ) {
        isConditionTrue_0 = true;
        gdjs.jeuCode.GDclavierObjects3[k] = gdjs.jeuCode.GDclavierObjects3[i];
        ++k;
    }
}
gdjs.jeuCode.GDclavierObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/t.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeuCode.GDclavierObjects2, gdjs.jeuCode.GDclavierObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeuCode.GDclavierObjects3.length;i<l;++i) {
    if ( gdjs.jeuCode.GDclavierObjects3[i].getBehavior("Animation").getAnimationName() == "u" ) {
        isConditionTrue_0 = true;
        gdjs.jeuCode.GDclavierObjects3[k] = gdjs.jeuCode.GDclavierObjects3[i];
        ++k;
    }
}
gdjs.jeuCode.GDclavierObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/u.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeuCode.GDclavierObjects2, gdjs.jeuCode.GDclavierObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeuCode.GDclavierObjects3.length;i<l;++i) {
    if ( gdjs.jeuCode.GDclavierObjects3[i].getBehavior("Animation").getAnimationName() == "v" ) {
        isConditionTrue_0 = true;
        gdjs.jeuCode.GDclavierObjects3[k] = gdjs.jeuCode.GDclavierObjects3[i];
        ++k;
    }
}
gdjs.jeuCode.GDclavierObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/v.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeuCode.GDclavierObjects2, gdjs.jeuCode.GDclavierObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeuCode.GDclavierObjects3.length;i<l;++i) {
    if ( gdjs.jeuCode.GDclavierObjects3[i].getBehavior("Animation").getAnimationName() == "w" ) {
        isConditionTrue_0 = true;
        gdjs.jeuCode.GDclavierObjects3[k] = gdjs.jeuCode.GDclavierObjects3[i];
        ++k;
    }
}
gdjs.jeuCode.GDclavierObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/w.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeuCode.GDclavierObjects2, gdjs.jeuCode.GDclavierObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeuCode.GDclavierObjects3.length;i<l;++i) {
    if ( gdjs.jeuCode.GDclavierObjects3[i].getBehavior("Animation").getAnimationName() == "x" ) {
        isConditionTrue_0 = true;
        gdjs.jeuCode.GDclavierObjects3[k] = gdjs.jeuCode.GDclavierObjects3[i];
        ++k;
    }
}
gdjs.jeuCode.GDclavierObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/x.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeuCode.GDclavierObjects2, gdjs.jeuCode.GDclavierObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeuCode.GDclavierObjects3.length;i<l;++i) {
    if ( gdjs.jeuCode.GDclavierObjects3[i].getBehavior("Animation").getAnimationName() == "y" ) {
        isConditionTrue_0 = true;
        gdjs.jeuCode.GDclavierObjects3[k] = gdjs.jeuCode.GDclavierObjects3[i];
        ++k;
    }
}
gdjs.jeuCode.GDclavierObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/y.mp3", 1, false, 100, 1);
}}

}


{

/* Reuse gdjs.jeuCode.GDclavierObjects2 */

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeuCode.GDclavierObjects2.length;i<l;++i) {
    if ( gdjs.jeuCode.GDclavierObjects2[i].getBehavior("Animation").getAnimationName() == "z" ) {
        isConditionTrue_0 = true;
        gdjs.jeuCode.GDclavierObjects2[k] = gdjs.jeuCode.GDclavierObjects2[i];
        ++k;
    }
}
gdjs.jeuCode.GDclavierObjects2.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/z.mp3", 1, false, 100, 1);
}}

}


};gdjs.jeuCode.mapOfGDgdjs_9546jeuCode_9546GDtext_95959595modeleObjects1Objects = Hashtable.newFrom({"text_modele": gdjs.jeuCode.GDtext_9595modeleObjects1});
gdjs.jeuCode.eventsList27 = function(runtimeScene) {

{

gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects1, gdjs.jeuCode.GDtext_9595modeleObjects2);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{gdjs.jeuCode.GDtext_9595modeleObjects2_1final.length = 0;
let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects1, gdjs.jeuCode.GDtext_9595modeleObjects3);

for (var i = 0, k = 0, l = gdjs.jeuCode.GDtext_9595modeleObjects3.length;i<l;++i) {
    if ( gdjs.jeuCode.GDtext_9595modeleObjects3[i].getBehavior("Text").getText() == "a" ) {
        isConditionTrue_1 = true;
        gdjs.jeuCode.GDtext_9595modeleObjects3[k] = gdjs.jeuCode.GDtext_9595modeleObjects3[i];
        ++k;
    }
}
gdjs.jeuCode.GDtext_9595modeleObjects3.length = k;
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.jeuCode.GDtext_9595modeleObjects3.length; j < jLen ; ++j) {
        if ( gdjs.jeuCode.GDtext_9595modeleObjects2_1final.indexOf(gdjs.jeuCode.GDtext_9595modeleObjects3[j]) === -1 )
            gdjs.jeuCode.GDtext_9595modeleObjects2_1final.push(gdjs.jeuCode.GDtext_9595modeleObjects3[j]);
    }
}
}
{
gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects1, gdjs.jeuCode.GDtext_9595modeleObjects3);

for (var i = 0, k = 0, l = gdjs.jeuCode.GDtext_9595modeleObjects3.length;i<l;++i) {
    if ( gdjs.jeuCode.GDtext_9595modeleObjects3[i].getBehavior("Text").getText() == "A" ) {
        isConditionTrue_1 = true;
        gdjs.jeuCode.GDtext_9595modeleObjects3[k] = gdjs.jeuCode.GDtext_9595modeleObjects3[i];
        ++k;
    }
}
gdjs.jeuCode.GDtext_9595modeleObjects3.length = k;
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.jeuCode.GDtext_9595modeleObjects3.length; j < jLen ; ++j) {
        if ( gdjs.jeuCode.GDtext_9595modeleObjects2_1final.indexOf(gdjs.jeuCode.GDtext_9595modeleObjects3[j]) === -1 )
            gdjs.jeuCode.GDtext_9595modeleObjects2_1final.push(gdjs.jeuCode.GDtext_9595modeleObjects3[j]);
    }
}
}
{
gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects2_1final, gdjs.jeuCode.GDtext_9595modeleObjects2);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/a.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects1, gdjs.jeuCode.GDtext_9595modeleObjects2);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{gdjs.jeuCode.GDtext_9595modeleObjects2_1final.length = 0;
let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects1, gdjs.jeuCode.GDtext_9595modeleObjects3);

for (var i = 0, k = 0, l = gdjs.jeuCode.GDtext_9595modeleObjects3.length;i<l;++i) {
    if ( gdjs.jeuCode.GDtext_9595modeleObjects3[i].getBehavior("Text").getText() == "b" ) {
        isConditionTrue_1 = true;
        gdjs.jeuCode.GDtext_9595modeleObjects3[k] = gdjs.jeuCode.GDtext_9595modeleObjects3[i];
        ++k;
    }
}
gdjs.jeuCode.GDtext_9595modeleObjects3.length = k;
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.jeuCode.GDtext_9595modeleObjects3.length; j < jLen ; ++j) {
        if ( gdjs.jeuCode.GDtext_9595modeleObjects2_1final.indexOf(gdjs.jeuCode.GDtext_9595modeleObjects3[j]) === -1 )
            gdjs.jeuCode.GDtext_9595modeleObjects2_1final.push(gdjs.jeuCode.GDtext_9595modeleObjects3[j]);
    }
}
}
{
gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects1, gdjs.jeuCode.GDtext_9595modeleObjects3);

for (var i = 0, k = 0, l = gdjs.jeuCode.GDtext_9595modeleObjects3.length;i<l;++i) {
    if ( gdjs.jeuCode.GDtext_9595modeleObjects3[i].getBehavior("Text").getText() == "B" ) {
        isConditionTrue_1 = true;
        gdjs.jeuCode.GDtext_9595modeleObjects3[k] = gdjs.jeuCode.GDtext_9595modeleObjects3[i];
        ++k;
    }
}
gdjs.jeuCode.GDtext_9595modeleObjects3.length = k;
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.jeuCode.GDtext_9595modeleObjects3.length; j < jLen ; ++j) {
        if ( gdjs.jeuCode.GDtext_9595modeleObjects2_1final.indexOf(gdjs.jeuCode.GDtext_9595modeleObjects3[j]) === -1 )
            gdjs.jeuCode.GDtext_9595modeleObjects2_1final.push(gdjs.jeuCode.GDtext_9595modeleObjects3[j]);
    }
}
}
{
gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects2_1final, gdjs.jeuCode.GDtext_9595modeleObjects2);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/b.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects1, gdjs.jeuCode.GDtext_9595modeleObjects2);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{gdjs.jeuCode.GDtext_9595modeleObjects2_1final.length = 0;
let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects1, gdjs.jeuCode.GDtext_9595modeleObjects3);

for (var i = 0, k = 0, l = gdjs.jeuCode.GDtext_9595modeleObjects3.length;i<l;++i) {
    if ( gdjs.jeuCode.GDtext_9595modeleObjects3[i].getBehavior("Text").getText() == "c" ) {
        isConditionTrue_1 = true;
        gdjs.jeuCode.GDtext_9595modeleObjects3[k] = gdjs.jeuCode.GDtext_9595modeleObjects3[i];
        ++k;
    }
}
gdjs.jeuCode.GDtext_9595modeleObjects3.length = k;
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.jeuCode.GDtext_9595modeleObjects3.length; j < jLen ; ++j) {
        if ( gdjs.jeuCode.GDtext_9595modeleObjects2_1final.indexOf(gdjs.jeuCode.GDtext_9595modeleObjects3[j]) === -1 )
            gdjs.jeuCode.GDtext_9595modeleObjects2_1final.push(gdjs.jeuCode.GDtext_9595modeleObjects3[j]);
    }
}
}
{
gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects1, gdjs.jeuCode.GDtext_9595modeleObjects3);

for (var i = 0, k = 0, l = gdjs.jeuCode.GDtext_9595modeleObjects3.length;i<l;++i) {
    if ( gdjs.jeuCode.GDtext_9595modeleObjects3[i].getBehavior("Text").getText() == "C" ) {
        isConditionTrue_1 = true;
        gdjs.jeuCode.GDtext_9595modeleObjects3[k] = gdjs.jeuCode.GDtext_9595modeleObjects3[i];
        ++k;
    }
}
gdjs.jeuCode.GDtext_9595modeleObjects3.length = k;
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.jeuCode.GDtext_9595modeleObjects3.length; j < jLen ; ++j) {
        if ( gdjs.jeuCode.GDtext_9595modeleObjects2_1final.indexOf(gdjs.jeuCode.GDtext_9595modeleObjects3[j]) === -1 )
            gdjs.jeuCode.GDtext_9595modeleObjects2_1final.push(gdjs.jeuCode.GDtext_9595modeleObjects3[j]);
    }
}
}
{
gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects2_1final, gdjs.jeuCode.GDtext_9595modeleObjects2);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/c.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects1, gdjs.jeuCode.GDtext_9595modeleObjects2);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{gdjs.jeuCode.GDtext_9595modeleObjects2_1final.length = 0;
let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects1, gdjs.jeuCode.GDtext_9595modeleObjects3);

for (var i = 0, k = 0, l = gdjs.jeuCode.GDtext_9595modeleObjects3.length;i<l;++i) {
    if ( gdjs.jeuCode.GDtext_9595modeleObjects3[i].getBehavior("Text").getText() == "d" ) {
        isConditionTrue_1 = true;
        gdjs.jeuCode.GDtext_9595modeleObjects3[k] = gdjs.jeuCode.GDtext_9595modeleObjects3[i];
        ++k;
    }
}
gdjs.jeuCode.GDtext_9595modeleObjects3.length = k;
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.jeuCode.GDtext_9595modeleObjects3.length; j < jLen ; ++j) {
        if ( gdjs.jeuCode.GDtext_9595modeleObjects2_1final.indexOf(gdjs.jeuCode.GDtext_9595modeleObjects3[j]) === -1 )
            gdjs.jeuCode.GDtext_9595modeleObjects2_1final.push(gdjs.jeuCode.GDtext_9595modeleObjects3[j]);
    }
}
}
{
gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects1, gdjs.jeuCode.GDtext_9595modeleObjects3);

for (var i = 0, k = 0, l = gdjs.jeuCode.GDtext_9595modeleObjects3.length;i<l;++i) {
    if ( gdjs.jeuCode.GDtext_9595modeleObjects3[i].getBehavior("Text").getText() == "D" ) {
        isConditionTrue_1 = true;
        gdjs.jeuCode.GDtext_9595modeleObjects3[k] = gdjs.jeuCode.GDtext_9595modeleObjects3[i];
        ++k;
    }
}
gdjs.jeuCode.GDtext_9595modeleObjects3.length = k;
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.jeuCode.GDtext_9595modeleObjects3.length; j < jLen ; ++j) {
        if ( gdjs.jeuCode.GDtext_9595modeleObjects2_1final.indexOf(gdjs.jeuCode.GDtext_9595modeleObjects3[j]) === -1 )
            gdjs.jeuCode.GDtext_9595modeleObjects2_1final.push(gdjs.jeuCode.GDtext_9595modeleObjects3[j]);
    }
}
}
{
gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects2_1final, gdjs.jeuCode.GDtext_9595modeleObjects2);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/d.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects1, gdjs.jeuCode.GDtext_9595modeleObjects2);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{gdjs.jeuCode.GDtext_9595modeleObjects2_1final.length = 0;
let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects1, gdjs.jeuCode.GDtext_9595modeleObjects3);

for (var i = 0, k = 0, l = gdjs.jeuCode.GDtext_9595modeleObjects3.length;i<l;++i) {
    if ( gdjs.jeuCode.GDtext_9595modeleObjects3[i].getBehavior("Text").getText() == "e" ) {
        isConditionTrue_1 = true;
        gdjs.jeuCode.GDtext_9595modeleObjects3[k] = gdjs.jeuCode.GDtext_9595modeleObjects3[i];
        ++k;
    }
}
gdjs.jeuCode.GDtext_9595modeleObjects3.length = k;
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.jeuCode.GDtext_9595modeleObjects3.length; j < jLen ; ++j) {
        if ( gdjs.jeuCode.GDtext_9595modeleObjects2_1final.indexOf(gdjs.jeuCode.GDtext_9595modeleObjects3[j]) === -1 )
            gdjs.jeuCode.GDtext_9595modeleObjects2_1final.push(gdjs.jeuCode.GDtext_9595modeleObjects3[j]);
    }
}
}
{
gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects1, gdjs.jeuCode.GDtext_9595modeleObjects3);

for (var i = 0, k = 0, l = gdjs.jeuCode.GDtext_9595modeleObjects3.length;i<l;++i) {
    if ( gdjs.jeuCode.GDtext_9595modeleObjects3[i].getBehavior("Text").getText() == "E" ) {
        isConditionTrue_1 = true;
        gdjs.jeuCode.GDtext_9595modeleObjects3[k] = gdjs.jeuCode.GDtext_9595modeleObjects3[i];
        ++k;
    }
}
gdjs.jeuCode.GDtext_9595modeleObjects3.length = k;
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.jeuCode.GDtext_9595modeleObjects3.length; j < jLen ; ++j) {
        if ( gdjs.jeuCode.GDtext_9595modeleObjects2_1final.indexOf(gdjs.jeuCode.GDtext_9595modeleObjects3[j]) === -1 )
            gdjs.jeuCode.GDtext_9595modeleObjects2_1final.push(gdjs.jeuCode.GDtext_9595modeleObjects3[j]);
    }
}
}
{
gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects2_1final, gdjs.jeuCode.GDtext_9595modeleObjects2);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/e.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects1, gdjs.jeuCode.GDtext_9595modeleObjects2);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{gdjs.jeuCode.GDtext_9595modeleObjects2_1final.length = 0;
let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects1, gdjs.jeuCode.GDtext_9595modeleObjects3);

for (var i = 0, k = 0, l = gdjs.jeuCode.GDtext_9595modeleObjects3.length;i<l;++i) {
    if ( gdjs.jeuCode.GDtext_9595modeleObjects3[i].getBehavior("Text").getText() == "f" ) {
        isConditionTrue_1 = true;
        gdjs.jeuCode.GDtext_9595modeleObjects3[k] = gdjs.jeuCode.GDtext_9595modeleObjects3[i];
        ++k;
    }
}
gdjs.jeuCode.GDtext_9595modeleObjects3.length = k;
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.jeuCode.GDtext_9595modeleObjects3.length; j < jLen ; ++j) {
        if ( gdjs.jeuCode.GDtext_9595modeleObjects2_1final.indexOf(gdjs.jeuCode.GDtext_9595modeleObjects3[j]) === -1 )
            gdjs.jeuCode.GDtext_9595modeleObjects2_1final.push(gdjs.jeuCode.GDtext_9595modeleObjects3[j]);
    }
}
}
{
gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects1, gdjs.jeuCode.GDtext_9595modeleObjects3);

for (var i = 0, k = 0, l = gdjs.jeuCode.GDtext_9595modeleObjects3.length;i<l;++i) {
    if ( gdjs.jeuCode.GDtext_9595modeleObjects3[i].getBehavior("Text").getText() == "F" ) {
        isConditionTrue_1 = true;
        gdjs.jeuCode.GDtext_9595modeleObjects3[k] = gdjs.jeuCode.GDtext_9595modeleObjects3[i];
        ++k;
    }
}
gdjs.jeuCode.GDtext_9595modeleObjects3.length = k;
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.jeuCode.GDtext_9595modeleObjects3.length; j < jLen ; ++j) {
        if ( gdjs.jeuCode.GDtext_9595modeleObjects2_1final.indexOf(gdjs.jeuCode.GDtext_9595modeleObjects3[j]) === -1 )
            gdjs.jeuCode.GDtext_9595modeleObjects2_1final.push(gdjs.jeuCode.GDtext_9595modeleObjects3[j]);
    }
}
}
{
gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects2_1final, gdjs.jeuCode.GDtext_9595modeleObjects2);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/f.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects1, gdjs.jeuCode.GDtext_9595modeleObjects2);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{gdjs.jeuCode.GDtext_9595modeleObjects2_1final.length = 0;
let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects1, gdjs.jeuCode.GDtext_9595modeleObjects3);

for (var i = 0, k = 0, l = gdjs.jeuCode.GDtext_9595modeleObjects3.length;i<l;++i) {
    if ( gdjs.jeuCode.GDtext_9595modeleObjects3[i].getBehavior("Text").getText() == "g" ) {
        isConditionTrue_1 = true;
        gdjs.jeuCode.GDtext_9595modeleObjects3[k] = gdjs.jeuCode.GDtext_9595modeleObjects3[i];
        ++k;
    }
}
gdjs.jeuCode.GDtext_9595modeleObjects3.length = k;
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.jeuCode.GDtext_9595modeleObjects3.length; j < jLen ; ++j) {
        if ( gdjs.jeuCode.GDtext_9595modeleObjects2_1final.indexOf(gdjs.jeuCode.GDtext_9595modeleObjects3[j]) === -1 )
            gdjs.jeuCode.GDtext_9595modeleObjects2_1final.push(gdjs.jeuCode.GDtext_9595modeleObjects3[j]);
    }
}
}
{
gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects1, gdjs.jeuCode.GDtext_9595modeleObjects3);

for (var i = 0, k = 0, l = gdjs.jeuCode.GDtext_9595modeleObjects3.length;i<l;++i) {
    if ( gdjs.jeuCode.GDtext_9595modeleObjects3[i].getBehavior("Text").getText() == "G" ) {
        isConditionTrue_1 = true;
        gdjs.jeuCode.GDtext_9595modeleObjects3[k] = gdjs.jeuCode.GDtext_9595modeleObjects3[i];
        ++k;
    }
}
gdjs.jeuCode.GDtext_9595modeleObjects3.length = k;
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.jeuCode.GDtext_9595modeleObjects3.length; j < jLen ; ++j) {
        if ( gdjs.jeuCode.GDtext_9595modeleObjects2_1final.indexOf(gdjs.jeuCode.GDtext_9595modeleObjects3[j]) === -1 )
            gdjs.jeuCode.GDtext_9595modeleObjects2_1final.push(gdjs.jeuCode.GDtext_9595modeleObjects3[j]);
    }
}
}
{
gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects2_1final, gdjs.jeuCode.GDtext_9595modeleObjects2);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/g.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects1, gdjs.jeuCode.GDtext_9595modeleObjects2);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{gdjs.jeuCode.GDtext_9595modeleObjects2_1final.length = 0;
let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects1, gdjs.jeuCode.GDtext_9595modeleObjects3);

for (var i = 0, k = 0, l = gdjs.jeuCode.GDtext_9595modeleObjects3.length;i<l;++i) {
    if ( gdjs.jeuCode.GDtext_9595modeleObjects3[i].getBehavior("Text").getText() == "h" ) {
        isConditionTrue_1 = true;
        gdjs.jeuCode.GDtext_9595modeleObjects3[k] = gdjs.jeuCode.GDtext_9595modeleObjects3[i];
        ++k;
    }
}
gdjs.jeuCode.GDtext_9595modeleObjects3.length = k;
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.jeuCode.GDtext_9595modeleObjects3.length; j < jLen ; ++j) {
        if ( gdjs.jeuCode.GDtext_9595modeleObjects2_1final.indexOf(gdjs.jeuCode.GDtext_9595modeleObjects3[j]) === -1 )
            gdjs.jeuCode.GDtext_9595modeleObjects2_1final.push(gdjs.jeuCode.GDtext_9595modeleObjects3[j]);
    }
}
}
{
gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects1, gdjs.jeuCode.GDtext_9595modeleObjects3);

for (var i = 0, k = 0, l = gdjs.jeuCode.GDtext_9595modeleObjects3.length;i<l;++i) {
    if ( gdjs.jeuCode.GDtext_9595modeleObjects3[i].getBehavior("Text").getText() == "H" ) {
        isConditionTrue_1 = true;
        gdjs.jeuCode.GDtext_9595modeleObjects3[k] = gdjs.jeuCode.GDtext_9595modeleObjects3[i];
        ++k;
    }
}
gdjs.jeuCode.GDtext_9595modeleObjects3.length = k;
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.jeuCode.GDtext_9595modeleObjects3.length; j < jLen ; ++j) {
        if ( gdjs.jeuCode.GDtext_9595modeleObjects2_1final.indexOf(gdjs.jeuCode.GDtext_9595modeleObjects3[j]) === -1 )
            gdjs.jeuCode.GDtext_9595modeleObjects2_1final.push(gdjs.jeuCode.GDtext_9595modeleObjects3[j]);
    }
}
}
{
gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects2_1final, gdjs.jeuCode.GDtext_9595modeleObjects2);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/h.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects1, gdjs.jeuCode.GDtext_9595modeleObjects2);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{gdjs.jeuCode.GDtext_9595modeleObjects2_1final.length = 0;
let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects1, gdjs.jeuCode.GDtext_9595modeleObjects3);

for (var i = 0, k = 0, l = gdjs.jeuCode.GDtext_9595modeleObjects3.length;i<l;++i) {
    if ( gdjs.jeuCode.GDtext_9595modeleObjects3[i].getBehavior("Text").getText() == "i" ) {
        isConditionTrue_1 = true;
        gdjs.jeuCode.GDtext_9595modeleObjects3[k] = gdjs.jeuCode.GDtext_9595modeleObjects3[i];
        ++k;
    }
}
gdjs.jeuCode.GDtext_9595modeleObjects3.length = k;
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.jeuCode.GDtext_9595modeleObjects3.length; j < jLen ; ++j) {
        if ( gdjs.jeuCode.GDtext_9595modeleObjects2_1final.indexOf(gdjs.jeuCode.GDtext_9595modeleObjects3[j]) === -1 )
            gdjs.jeuCode.GDtext_9595modeleObjects2_1final.push(gdjs.jeuCode.GDtext_9595modeleObjects3[j]);
    }
}
}
{
gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects1, gdjs.jeuCode.GDtext_9595modeleObjects3);

for (var i = 0, k = 0, l = gdjs.jeuCode.GDtext_9595modeleObjects3.length;i<l;++i) {
    if ( gdjs.jeuCode.GDtext_9595modeleObjects3[i].getBehavior("Text").getText() == "I" ) {
        isConditionTrue_1 = true;
        gdjs.jeuCode.GDtext_9595modeleObjects3[k] = gdjs.jeuCode.GDtext_9595modeleObjects3[i];
        ++k;
    }
}
gdjs.jeuCode.GDtext_9595modeleObjects3.length = k;
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.jeuCode.GDtext_9595modeleObjects3.length; j < jLen ; ++j) {
        if ( gdjs.jeuCode.GDtext_9595modeleObjects2_1final.indexOf(gdjs.jeuCode.GDtext_9595modeleObjects3[j]) === -1 )
            gdjs.jeuCode.GDtext_9595modeleObjects2_1final.push(gdjs.jeuCode.GDtext_9595modeleObjects3[j]);
    }
}
}
{
gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects2_1final, gdjs.jeuCode.GDtext_9595modeleObjects2);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/i.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects1, gdjs.jeuCode.GDtext_9595modeleObjects2);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{gdjs.jeuCode.GDtext_9595modeleObjects2_1final.length = 0;
let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects1, gdjs.jeuCode.GDtext_9595modeleObjects3);

for (var i = 0, k = 0, l = gdjs.jeuCode.GDtext_9595modeleObjects3.length;i<l;++i) {
    if ( gdjs.jeuCode.GDtext_9595modeleObjects3[i].getBehavior("Text").getText() == "j" ) {
        isConditionTrue_1 = true;
        gdjs.jeuCode.GDtext_9595modeleObjects3[k] = gdjs.jeuCode.GDtext_9595modeleObjects3[i];
        ++k;
    }
}
gdjs.jeuCode.GDtext_9595modeleObjects3.length = k;
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.jeuCode.GDtext_9595modeleObjects3.length; j < jLen ; ++j) {
        if ( gdjs.jeuCode.GDtext_9595modeleObjects2_1final.indexOf(gdjs.jeuCode.GDtext_9595modeleObjects3[j]) === -1 )
            gdjs.jeuCode.GDtext_9595modeleObjects2_1final.push(gdjs.jeuCode.GDtext_9595modeleObjects3[j]);
    }
}
}
{
gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects1, gdjs.jeuCode.GDtext_9595modeleObjects3);

for (var i = 0, k = 0, l = gdjs.jeuCode.GDtext_9595modeleObjects3.length;i<l;++i) {
    if ( gdjs.jeuCode.GDtext_9595modeleObjects3[i].getBehavior("Text").getText() == "J" ) {
        isConditionTrue_1 = true;
        gdjs.jeuCode.GDtext_9595modeleObjects3[k] = gdjs.jeuCode.GDtext_9595modeleObjects3[i];
        ++k;
    }
}
gdjs.jeuCode.GDtext_9595modeleObjects3.length = k;
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.jeuCode.GDtext_9595modeleObjects3.length; j < jLen ; ++j) {
        if ( gdjs.jeuCode.GDtext_9595modeleObjects2_1final.indexOf(gdjs.jeuCode.GDtext_9595modeleObjects3[j]) === -1 )
            gdjs.jeuCode.GDtext_9595modeleObjects2_1final.push(gdjs.jeuCode.GDtext_9595modeleObjects3[j]);
    }
}
}
{
gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects2_1final, gdjs.jeuCode.GDtext_9595modeleObjects2);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/j.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects1, gdjs.jeuCode.GDtext_9595modeleObjects2);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{gdjs.jeuCode.GDtext_9595modeleObjects2_1final.length = 0;
let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects1, gdjs.jeuCode.GDtext_9595modeleObjects3);

for (var i = 0, k = 0, l = gdjs.jeuCode.GDtext_9595modeleObjects3.length;i<l;++i) {
    if ( gdjs.jeuCode.GDtext_9595modeleObjects3[i].getBehavior("Text").getText() == "k" ) {
        isConditionTrue_1 = true;
        gdjs.jeuCode.GDtext_9595modeleObjects3[k] = gdjs.jeuCode.GDtext_9595modeleObjects3[i];
        ++k;
    }
}
gdjs.jeuCode.GDtext_9595modeleObjects3.length = k;
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.jeuCode.GDtext_9595modeleObjects3.length; j < jLen ; ++j) {
        if ( gdjs.jeuCode.GDtext_9595modeleObjects2_1final.indexOf(gdjs.jeuCode.GDtext_9595modeleObjects3[j]) === -1 )
            gdjs.jeuCode.GDtext_9595modeleObjects2_1final.push(gdjs.jeuCode.GDtext_9595modeleObjects3[j]);
    }
}
}
{
gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects1, gdjs.jeuCode.GDtext_9595modeleObjects3);

for (var i = 0, k = 0, l = gdjs.jeuCode.GDtext_9595modeleObjects3.length;i<l;++i) {
    if ( gdjs.jeuCode.GDtext_9595modeleObjects3[i].getBehavior("Text").getText() == "K" ) {
        isConditionTrue_1 = true;
        gdjs.jeuCode.GDtext_9595modeleObjects3[k] = gdjs.jeuCode.GDtext_9595modeleObjects3[i];
        ++k;
    }
}
gdjs.jeuCode.GDtext_9595modeleObjects3.length = k;
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.jeuCode.GDtext_9595modeleObjects3.length; j < jLen ; ++j) {
        if ( gdjs.jeuCode.GDtext_9595modeleObjects2_1final.indexOf(gdjs.jeuCode.GDtext_9595modeleObjects3[j]) === -1 )
            gdjs.jeuCode.GDtext_9595modeleObjects2_1final.push(gdjs.jeuCode.GDtext_9595modeleObjects3[j]);
    }
}
}
{
gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects2_1final, gdjs.jeuCode.GDtext_9595modeleObjects2);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/k.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects1, gdjs.jeuCode.GDtext_9595modeleObjects2);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{gdjs.jeuCode.GDtext_9595modeleObjects2_1final.length = 0;
let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects1, gdjs.jeuCode.GDtext_9595modeleObjects3);

for (var i = 0, k = 0, l = gdjs.jeuCode.GDtext_9595modeleObjects3.length;i<l;++i) {
    if ( gdjs.jeuCode.GDtext_9595modeleObjects3[i].getBehavior("Text").getText() == "l" ) {
        isConditionTrue_1 = true;
        gdjs.jeuCode.GDtext_9595modeleObjects3[k] = gdjs.jeuCode.GDtext_9595modeleObjects3[i];
        ++k;
    }
}
gdjs.jeuCode.GDtext_9595modeleObjects3.length = k;
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.jeuCode.GDtext_9595modeleObjects3.length; j < jLen ; ++j) {
        if ( gdjs.jeuCode.GDtext_9595modeleObjects2_1final.indexOf(gdjs.jeuCode.GDtext_9595modeleObjects3[j]) === -1 )
            gdjs.jeuCode.GDtext_9595modeleObjects2_1final.push(gdjs.jeuCode.GDtext_9595modeleObjects3[j]);
    }
}
}
{
gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects1, gdjs.jeuCode.GDtext_9595modeleObjects3);

for (var i = 0, k = 0, l = gdjs.jeuCode.GDtext_9595modeleObjects3.length;i<l;++i) {
    if ( gdjs.jeuCode.GDtext_9595modeleObjects3[i].getBehavior("Text").getText() == "L" ) {
        isConditionTrue_1 = true;
        gdjs.jeuCode.GDtext_9595modeleObjects3[k] = gdjs.jeuCode.GDtext_9595modeleObjects3[i];
        ++k;
    }
}
gdjs.jeuCode.GDtext_9595modeleObjects3.length = k;
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.jeuCode.GDtext_9595modeleObjects3.length; j < jLen ; ++j) {
        if ( gdjs.jeuCode.GDtext_9595modeleObjects2_1final.indexOf(gdjs.jeuCode.GDtext_9595modeleObjects3[j]) === -1 )
            gdjs.jeuCode.GDtext_9595modeleObjects2_1final.push(gdjs.jeuCode.GDtext_9595modeleObjects3[j]);
    }
}
}
{
gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects2_1final, gdjs.jeuCode.GDtext_9595modeleObjects2);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/l.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects1, gdjs.jeuCode.GDtext_9595modeleObjects2);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{gdjs.jeuCode.GDtext_9595modeleObjects2_1final.length = 0;
let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects1, gdjs.jeuCode.GDtext_9595modeleObjects3);

for (var i = 0, k = 0, l = gdjs.jeuCode.GDtext_9595modeleObjects3.length;i<l;++i) {
    if ( gdjs.jeuCode.GDtext_9595modeleObjects3[i].getBehavior("Text").getText() == "m" ) {
        isConditionTrue_1 = true;
        gdjs.jeuCode.GDtext_9595modeleObjects3[k] = gdjs.jeuCode.GDtext_9595modeleObjects3[i];
        ++k;
    }
}
gdjs.jeuCode.GDtext_9595modeleObjects3.length = k;
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.jeuCode.GDtext_9595modeleObjects3.length; j < jLen ; ++j) {
        if ( gdjs.jeuCode.GDtext_9595modeleObjects2_1final.indexOf(gdjs.jeuCode.GDtext_9595modeleObjects3[j]) === -1 )
            gdjs.jeuCode.GDtext_9595modeleObjects2_1final.push(gdjs.jeuCode.GDtext_9595modeleObjects3[j]);
    }
}
}
{
gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects1, gdjs.jeuCode.GDtext_9595modeleObjects3);

for (var i = 0, k = 0, l = gdjs.jeuCode.GDtext_9595modeleObjects3.length;i<l;++i) {
    if ( gdjs.jeuCode.GDtext_9595modeleObjects3[i].getBehavior("Text").getText() == "M" ) {
        isConditionTrue_1 = true;
        gdjs.jeuCode.GDtext_9595modeleObjects3[k] = gdjs.jeuCode.GDtext_9595modeleObjects3[i];
        ++k;
    }
}
gdjs.jeuCode.GDtext_9595modeleObjects3.length = k;
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.jeuCode.GDtext_9595modeleObjects3.length; j < jLen ; ++j) {
        if ( gdjs.jeuCode.GDtext_9595modeleObjects2_1final.indexOf(gdjs.jeuCode.GDtext_9595modeleObjects3[j]) === -1 )
            gdjs.jeuCode.GDtext_9595modeleObjects2_1final.push(gdjs.jeuCode.GDtext_9595modeleObjects3[j]);
    }
}
}
{
gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects2_1final, gdjs.jeuCode.GDtext_9595modeleObjects2);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/m.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects1, gdjs.jeuCode.GDtext_9595modeleObjects2);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{gdjs.jeuCode.GDtext_9595modeleObjects2_1final.length = 0;
let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects1, gdjs.jeuCode.GDtext_9595modeleObjects3);

for (var i = 0, k = 0, l = gdjs.jeuCode.GDtext_9595modeleObjects3.length;i<l;++i) {
    if ( gdjs.jeuCode.GDtext_9595modeleObjects3[i].getBehavior("Text").getText() == "n" ) {
        isConditionTrue_1 = true;
        gdjs.jeuCode.GDtext_9595modeleObjects3[k] = gdjs.jeuCode.GDtext_9595modeleObjects3[i];
        ++k;
    }
}
gdjs.jeuCode.GDtext_9595modeleObjects3.length = k;
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.jeuCode.GDtext_9595modeleObjects3.length; j < jLen ; ++j) {
        if ( gdjs.jeuCode.GDtext_9595modeleObjects2_1final.indexOf(gdjs.jeuCode.GDtext_9595modeleObjects3[j]) === -1 )
            gdjs.jeuCode.GDtext_9595modeleObjects2_1final.push(gdjs.jeuCode.GDtext_9595modeleObjects3[j]);
    }
}
}
{
gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects1, gdjs.jeuCode.GDtext_9595modeleObjects3);

for (var i = 0, k = 0, l = gdjs.jeuCode.GDtext_9595modeleObjects3.length;i<l;++i) {
    if ( gdjs.jeuCode.GDtext_9595modeleObjects3[i].getBehavior("Text").getText() == "N" ) {
        isConditionTrue_1 = true;
        gdjs.jeuCode.GDtext_9595modeleObjects3[k] = gdjs.jeuCode.GDtext_9595modeleObjects3[i];
        ++k;
    }
}
gdjs.jeuCode.GDtext_9595modeleObjects3.length = k;
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.jeuCode.GDtext_9595modeleObjects3.length; j < jLen ; ++j) {
        if ( gdjs.jeuCode.GDtext_9595modeleObjects2_1final.indexOf(gdjs.jeuCode.GDtext_9595modeleObjects3[j]) === -1 )
            gdjs.jeuCode.GDtext_9595modeleObjects2_1final.push(gdjs.jeuCode.GDtext_9595modeleObjects3[j]);
    }
}
}
{
gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects2_1final, gdjs.jeuCode.GDtext_9595modeleObjects2);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/n.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects1, gdjs.jeuCode.GDtext_9595modeleObjects2);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{gdjs.jeuCode.GDtext_9595modeleObjects2_1final.length = 0;
let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects1, gdjs.jeuCode.GDtext_9595modeleObjects3);

for (var i = 0, k = 0, l = gdjs.jeuCode.GDtext_9595modeleObjects3.length;i<l;++i) {
    if ( gdjs.jeuCode.GDtext_9595modeleObjects3[i].getBehavior("Text").getText() == "o" ) {
        isConditionTrue_1 = true;
        gdjs.jeuCode.GDtext_9595modeleObjects3[k] = gdjs.jeuCode.GDtext_9595modeleObjects3[i];
        ++k;
    }
}
gdjs.jeuCode.GDtext_9595modeleObjects3.length = k;
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.jeuCode.GDtext_9595modeleObjects3.length; j < jLen ; ++j) {
        if ( gdjs.jeuCode.GDtext_9595modeleObjects2_1final.indexOf(gdjs.jeuCode.GDtext_9595modeleObjects3[j]) === -1 )
            gdjs.jeuCode.GDtext_9595modeleObjects2_1final.push(gdjs.jeuCode.GDtext_9595modeleObjects3[j]);
    }
}
}
{
gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects1, gdjs.jeuCode.GDtext_9595modeleObjects3);

for (var i = 0, k = 0, l = gdjs.jeuCode.GDtext_9595modeleObjects3.length;i<l;++i) {
    if ( gdjs.jeuCode.GDtext_9595modeleObjects3[i].getBehavior("Text").getText() == "O" ) {
        isConditionTrue_1 = true;
        gdjs.jeuCode.GDtext_9595modeleObjects3[k] = gdjs.jeuCode.GDtext_9595modeleObjects3[i];
        ++k;
    }
}
gdjs.jeuCode.GDtext_9595modeleObjects3.length = k;
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.jeuCode.GDtext_9595modeleObjects3.length; j < jLen ; ++j) {
        if ( gdjs.jeuCode.GDtext_9595modeleObjects2_1final.indexOf(gdjs.jeuCode.GDtext_9595modeleObjects3[j]) === -1 )
            gdjs.jeuCode.GDtext_9595modeleObjects2_1final.push(gdjs.jeuCode.GDtext_9595modeleObjects3[j]);
    }
}
}
{
gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects2_1final, gdjs.jeuCode.GDtext_9595modeleObjects2);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/o.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects1, gdjs.jeuCode.GDtext_9595modeleObjects2);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{gdjs.jeuCode.GDtext_9595modeleObjects2_1final.length = 0;
let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects1, gdjs.jeuCode.GDtext_9595modeleObjects3);

for (var i = 0, k = 0, l = gdjs.jeuCode.GDtext_9595modeleObjects3.length;i<l;++i) {
    if ( gdjs.jeuCode.GDtext_9595modeleObjects3[i].getBehavior("Text").getText() == "p" ) {
        isConditionTrue_1 = true;
        gdjs.jeuCode.GDtext_9595modeleObjects3[k] = gdjs.jeuCode.GDtext_9595modeleObjects3[i];
        ++k;
    }
}
gdjs.jeuCode.GDtext_9595modeleObjects3.length = k;
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.jeuCode.GDtext_9595modeleObjects3.length; j < jLen ; ++j) {
        if ( gdjs.jeuCode.GDtext_9595modeleObjects2_1final.indexOf(gdjs.jeuCode.GDtext_9595modeleObjects3[j]) === -1 )
            gdjs.jeuCode.GDtext_9595modeleObjects2_1final.push(gdjs.jeuCode.GDtext_9595modeleObjects3[j]);
    }
}
}
{
gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects1, gdjs.jeuCode.GDtext_9595modeleObjects3);

for (var i = 0, k = 0, l = gdjs.jeuCode.GDtext_9595modeleObjects3.length;i<l;++i) {
    if ( gdjs.jeuCode.GDtext_9595modeleObjects3[i].getBehavior("Text").getText() == "P" ) {
        isConditionTrue_1 = true;
        gdjs.jeuCode.GDtext_9595modeleObjects3[k] = gdjs.jeuCode.GDtext_9595modeleObjects3[i];
        ++k;
    }
}
gdjs.jeuCode.GDtext_9595modeleObjects3.length = k;
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.jeuCode.GDtext_9595modeleObjects3.length; j < jLen ; ++j) {
        if ( gdjs.jeuCode.GDtext_9595modeleObjects2_1final.indexOf(gdjs.jeuCode.GDtext_9595modeleObjects3[j]) === -1 )
            gdjs.jeuCode.GDtext_9595modeleObjects2_1final.push(gdjs.jeuCode.GDtext_9595modeleObjects3[j]);
    }
}
}
{
gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects2_1final, gdjs.jeuCode.GDtext_9595modeleObjects2);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/p.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects1, gdjs.jeuCode.GDtext_9595modeleObjects2);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{gdjs.jeuCode.GDtext_9595modeleObjects2_1final.length = 0;
let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects1, gdjs.jeuCode.GDtext_9595modeleObjects3);

for (var i = 0, k = 0, l = gdjs.jeuCode.GDtext_9595modeleObjects3.length;i<l;++i) {
    if ( gdjs.jeuCode.GDtext_9595modeleObjects3[i].getBehavior("Text").getText() == "q" ) {
        isConditionTrue_1 = true;
        gdjs.jeuCode.GDtext_9595modeleObjects3[k] = gdjs.jeuCode.GDtext_9595modeleObjects3[i];
        ++k;
    }
}
gdjs.jeuCode.GDtext_9595modeleObjects3.length = k;
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.jeuCode.GDtext_9595modeleObjects3.length; j < jLen ; ++j) {
        if ( gdjs.jeuCode.GDtext_9595modeleObjects2_1final.indexOf(gdjs.jeuCode.GDtext_9595modeleObjects3[j]) === -1 )
            gdjs.jeuCode.GDtext_9595modeleObjects2_1final.push(gdjs.jeuCode.GDtext_9595modeleObjects3[j]);
    }
}
}
{
gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects1, gdjs.jeuCode.GDtext_9595modeleObjects3);

for (var i = 0, k = 0, l = gdjs.jeuCode.GDtext_9595modeleObjects3.length;i<l;++i) {
    if ( gdjs.jeuCode.GDtext_9595modeleObjects3[i].getBehavior("Text").getText() == "Q" ) {
        isConditionTrue_1 = true;
        gdjs.jeuCode.GDtext_9595modeleObjects3[k] = gdjs.jeuCode.GDtext_9595modeleObjects3[i];
        ++k;
    }
}
gdjs.jeuCode.GDtext_9595modeleObjects3.length = k;
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.jeuCode.GDtext_9595modeleObjects3.length; j < jLen ; ++j) {
        if ( gdjs.jeuCode.GDtext_9595modeleObjects2_1final.indexOf(gdjs.jeuCode.GDtext_9595modeleObjects3[j]) === -1 )
            gdjs.jeuCode.GDtext_9595modeleObjects2_1final.push(gdjs.jeuCode.GDtext_9595modeleObjects3[j]);
    }
}
}
{
gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects2_1final, gdjs.jeuCode.GDtext_9595modeleObjects2);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/q.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects1, gdjs.jeuCode.GDtext_9595modeleObjects2);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{gdjs.jeuCode.GDtext_9595modeleObjects2_1final.length = 0;
let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects1, gdjs.jeuCode.GDtext_9595modeleObjects3);

for (var i = 0, k = 0, l = gdjs.jeuCode.GDtext_9595modeleObjects3.length;i<l;++i) {
    if ( gdjs.jeuCode.GDtext_9595modeleObjects3[i].getBehavior("Text").getText() == "r" ) {
        isConditionTrue_1 = true;
        gdjs.jeuCode.GDtext_9595modeleObjects3[k] = gdjs.jeuCode.GDtext_9595modeleObjects3[i];
        ++k;
    }
}
gdjs.jeuCode.GDtext_9595modeleObjects3.length = k;
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.jeuCode.GDtext_9595modeleObjects3.length; j < jLen ; ++j) {
        if ( gdjs.jeuCode.GDtext_9595modeleObjects2_1final.indexOf(gdjs.jeuCode.GDtext_9595modeleObjects3[j]) === -1 )
            gdjs.jeuCode.GDtext_9595modeleObjects2_1final.push(gdjs.jeuCode.GDtext_9595modeleObjects3[j]);
    }
}
}
{
gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects1, gdjs.jeuCode.GDtext_9595modeleObjects3);

for (var i = 0, k = 0, l = gdjs.jeuCode.GDtext_9595modeleObjects3.length;i<l;++i) {
    if ( gdjs.jeuCode.GDtext_9595modeleObjects3[i].getBehavior("Text").getText() == "R" ) {
        isConditionTrue_1 = true;
        gdjs.jeuCode.GDtext_9595modeleObjects3[k] = gdjs.jeuCode.GDtext_9595modeleObjects3[i];
        ++k;
    }
}
gdjs.jeuCode.GDtext_9595modeleObjects3.length = k;
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.jeuCode.GDtext_9595modeleObjects3.length; j < jLen ; ++j) {
        if ( gdjs.jeuCode.GDtext_9595modeleObjects2_1final.indexOf(gdjs.jeuCode.GDtext_9595modeleObjects3[j]) === -1 )
            gdjs.jeuCode.GDtext_9595modeleObjects2_1final.push(gdjs.jeuCode.GDtext_9595modeleObjects3[j]);
    }
}
}
{
gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects2_1final, gdjs.jeuCode.GDtext_9595modeleObjects2);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/r.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects1, gdjs.jeuCode.GDtext_9595modeleObjects2);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{gdjs.jeuCode.GDtext_9595modeleObjects2_1final.length = 0;
let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects1, gdjs.jeuCode.GDtext_9595modeleObjects3);

for (var i = 0, k = 0, l = gdjs.jeuCode.GDtext_9595modeleObjects3.length;i<l;++i) {
    if ( gdjs.jeuCode.GDtext_9595modeleObjects3[i].getBehavior("Text").getText() == "s" ) {
        isConditionTrue_1 = true;
        gdjs.jeuCode.GDtext_9595modeleObjects3[k] = gdjs.jeuCode.GDtext_9595modeleObjects3[i];
        ++k;
    }
}
gdjs.jeuCode.GDtext_9595modeleObjects3.length = k;
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.jeuCode.GDtext_9595modeleObjects3.length; j < jLen ; ++j) {
        if ( gdjs.jeuCode.GDtext_9595modeleObjects2_1final.indexOf(gdjs.jeuCode.GDtext_9595modeleObjects3[j]) === -1 )
            gdjs.jeuCode.GDtext_9595modeleObjects2_1final.push(gdjs.jeuCode.GDtext_9595modeleObjects3[j]);
    }
}
}
{
gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects1, gdjs.jeuCode.GDtext_9595modeleObjects3);

for (var i = 0, k = 0, l = gdjs.jeuCode.GDtext_9595modeleObjects3.length;i<l;++i) {
    if ( gdjs.jeuCode.GDtext_9595modeleObjects3[i].getBehavior("Text").getText() == "S" ) {
        isConditionTrue_1 = true;
        gdjs.jeuCode.GDtext_9595modeleObjects3[k] = gdjs.jeuCode.GDtext_9595modeleObjects3[i];
        ++k;
    }
}
gdjs.jeuCode.GDtext_9595modeleObjects3.length = k;
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.jeuCode.GDtext_9595modeleObjects3.length; j < jLen ; ++j) {
        if ( gdjs.jeuCode.GDtext_9595modeleObjects2_1final.indexOf(gdjs.jeuCode.GDtext_9595modeleObjects3[j]) === -1 )
            gdjs.jeuCode.GDtext_9595modeleObjects2_1final.push(gdjs.jeuCode.GDtext_9595modeleObjects3[j]);
    }
}
}
{
gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects2_1final, gdjs.jeuCode.GDtext_9595modeleObjects2);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/s.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects1, gdjs.jeuCode.GDtext_9595modeleObjects2);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{gdjs.jeuCode.GDtext_9595modeleObjects2_1final.length = 0;
let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects1, gdjs.jeuCode.GDtext_9595modeleObjects3);

for (var i = 0, k = 0, l = gdjs.jeuCode.GDtext_9595modeleObjects3.length;i<l;++i) {
    if ( gdjs.jeuCode.GDtext_9595modeleObjects3[i].getBehavior("Text").getText() == "t" ) {
        isConditionTrue_1 = true;
        gdjs.jeuCode.GDtext_9595modeleObjects3[k] = gdjs.jeuCode.GDtext_9595modeleObjects3[i];
        ++k;
    }
}
gdjs.jeuCode.GDtext_9595modeleObjects3.length = k;
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.jeuCode.GDtext_9595modeleObjects3.length; j < jLen ; ++j) {
        if ( gdjs.jeuCode.GDtext_9595modeleObjects2_1final.indexOf(gdjs.jeuCode.GDtext_9595modeleObjects3[j]) === -1 )
            gdjs.jeuCode.GDtext_9595modeleObjects2_1final.push(gdjs.jeuCode.GDtext_9595modeleObjects3[j]);
    }
}
}
{
gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects1, gdjs.jeuCode.GDtext_9595modeleObjects3);

for (var i = 0, k = 0, l = gdjs.jeuCode.GDtext_9595modeleObjects3.length;i<l;++i) {
    if ( gdjs.jeuCode.GDtext_9595modeleObjects3[i].getBehavior("Text").getText() == "T" ) {
        isConditionTrue_1 = true;
        gdjs.jeuCode.GDtext_9595modeleObjects3[k] = gdjs.jeuCode.GDtext_9595modeleObjects3[i];
        ++k;
    }
}
gdjs.jeuCode.GDtext_9595modeleObjects3.length = k;
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.jeuCode.GDtext_9595modeleObjects3.length; j < jLen ; ++j) {
        if ( gdjs.jeuCode.GDtext_9595modeleObjects2_1final.indexOf(gdjs.jeuCode.GDtext_9595modeleObjects3[j]) === -1 )
            gdjs.jeuCode.GDtext_9595modeleObjects2_1final.push(gdjs.jeuCode.GDtext_9595modeleObjects3[j]);
    }
}
}
{
gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects2_1final, gdjs.jeuCode.GDtext_9595modeleObjects2);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/t.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects1, gdjs.jeuCode.GDtext_9595modeleObjects2);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{gdjs.jeuCode.GDtext_9595modeleObjects2_1final.length = 0;
let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects1, gdjs.jeuCode.GDtext_9595modeleObjects3);

for (var i = 0, k = 0, l = gdjs.jeuCode.GDtext_9595modeleObjects3.length;i<l;++i) {
    if ( gdjs.jeuCode.GDtext_9595modeleObjects3[i].getBehavior("Text").getText() == "u" ) {
        isConditionTrue_1 = true;
        gdjs.jeuCode.GDtext_9595modeleObjects3[k] = gdjs.jeuCode.GDtext_9595modeleObjects3[i];
        ++k;
    }
}
gdjs.jeuCode.GDtext_9595modeleObjects3.length = k;
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.jeuCode.GDtext_9595modeleObjects3.length; j < jLen ; ++j) {
        if ( gdjs.jeuCode.GDtext_9595modeleObjects2_1final.indexOf(gdjs.jeuCode.GDtext_9595modeleObjects3[j]) === -1 )
            gdjs.jeuCode.GDtext_9595modeleObjects2_1final.push(gdjs.jeuCode.GDtext_9595modeleObjects3[j]);
    }
}
}
{
gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects1, gdjs.jeuCode.GDtext_9595modeleObjects3);

for (var i = 0, k = 0, l = gdjs.jeuCode.GDtext_9595modeleObjects3.length;i<l;++i) {
    if ( gdjs.jeuCode.GDtext_9595modeleObjects3[i].getBehavior("Text").getText() == "U" ) {
        isConditionTrue_1 = true;
        gdjs.jeuCode.GDtext_9595modeleObjects3[k] = gdjs.jeuCode.GDtext_9595modeleObjects3[i];
        ++k;
    }
}
gdjs.jeuCode.GDtext_9595modeleObjects3.length = k;
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.jeuCode.GDtext_9595modeleObjects3.length; j < jLen ; ++j) {
        if ( gdjs.jeuCode.GDtext_9595modeleObjects2_1final.indexOf(gdjs.jeuCode.GDtext_9595modeleObjects3[j]) === -1 )
            gdjs.jeuCode.GDtext_9595modeleObjects2_1final.push(gdjs.jeuCode.GDtext_9595modeleObjects3[j]);
    }
}
}
{
gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects2_1final, gdjs.jeuCode.GDtext_9595modeleObjects2);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/u.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects1, gdjs.jeuCode.GDtext_9595modeleObjects2);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{gdjs.jeuCode.GDtext_9595modeleObjects2_1final.length = 0;
let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects1, gdjs.jeuCode.GDtext_9595modeleObjects3);

for (var i = 0, k = 0, l = gdjs.jeuCode.GDtext_9595modeleObjects3.length;i<l;++i) {
    if ( gdjs.jeuCode.GDtext_9595modeleObjects3[i].getBehavior("Text").getText() == "v" ) {
        isConditionTrue_1 = true;
        gdjs.jeuCode.GDtext_9595modeleObjects3[k] = gdjs.jeuCode.GDtext_9595modeleObjects3[i];
        ++k;
    }
}
gdjs.jeuCode.GDtext_9595modeleObjects3.length = k;
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.jeuCode.GDtext_9595modeleObjects3.length; j < jLen ; ++j) {
        if ( gdjs.jeuCode.GDtext_9595modeleObjects2_1final.indexOf(gdjs.jeuCode.GDtext_9595modeleObjects3[j]) === -1 )
            gdjs.jeuCode.GDtext_9595modeleObjects2_1final.push(gdjs.jeuCode.GDtext_9595modeleObjects3[j]);
    }
}
}
{
gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects1, gdjs.jeuCode.GDtext_9595modeleObjects3);

for (var i = 0, k = 0, l = gdjs.jeuCode.GDtext_9595modeleObjects3.length;i<l;++i) {
    if ( gdjs.jeuCode.GDtext_9595modeleObjects3[i].getBehavior("Text").getText() == "V" ) {
        isConditionTrue_1 = true;
        gdjs.jeuCode.GDtext_9595modeleObjects3[k] = gdjs.jeuCode.GDtext_9595modeleObjects3[i];
        ++k;
    }
}
gdjs.jeuCode.GDtext_9595modeleObjects3.length = k;
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.jeuCode.GDtext_9595modeleObjects3.length; j < jLen ; ++j) {
        if ( gdjs.jeuCode.GDtext_9595modeleObjects2_1final.indexOf(gdjs.jeuCode.GDtext_9595modeleObjects3[j]) === -1 )
            gdjs.jeuCode.GDtext_9595modeleObjects2_1final.push(gdjs.jeuCode.GDtext_9595modeleObjects3[j]);
    }
}
}
{
gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects2_1final, gdjs.jeuCode.GDtext_9595modeleObjects2);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/v.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects1, gdjs.jeuCode.GDtext_9595modeleObjects2);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{gdjs.jeuCode.GDtext_9595modeleObjects2_1final.length = 0;
let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects1, gdjs.jeuCode.GDtext_9595modeleObjects3);

for (var i = 0, k = 0, l = gdjs.jeuCode.GDtext_9595modeleObjects3.length;i<l;++i) {
    if ( gdjs.jeuCode.GDtext_9595modeleObjects3[i].getBehavior("Text").getText() == "w" ) {
        isConditionTrue_1 = true;
        gdjs.jeuCode.GDtext_9595modeleObjects3[k] = gdjs.jeuCode.GDtext_9595modeleObjects3[i];
        ++k;
    }
}
gdjs.jeuCode.GDtext_9595modeleObjects3.length = k;
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.jeuCode.GDtext_9595modeleObjects3.length; j < jLen ; ++j) {
        if ( gdjs.jeuCode.GDtext_9595modeleObjects2_1final.indexOf(gdjs.jeuCode.GDtext_9595modeleObjects3[j]) === -1 )
            gdjs.jeuCode.GDtext_9595modeleObjects2_1final.push(gdjs.jeuCode.GDtext_9595modeleObjects3[j]);
    }
}
}
{
gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects1, gdjs.jeuCode.GDtext_9595modeleObjects3);

for (var i = 0, k = 0, l = gdjs.jeuCode.GDtext_9595modeleObjects3.length;i<l;++i) {
    if ( gdjs.jeuCode.GDtext_9595modeleObjects3[i].getBehavior("Text").getText() == "W" ) {
        isConditionTrue_1 = true;
        gdjs.jeuCode.GDtext_9595modeleObjects3[k] = gdjs.jeuCode.GDtext_9595modeleObjects3[i];
        ++k;
    }
}
gdjs.jeuCode.GDtext_9595modeleObjects3.length = k;
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.jeuCode.GDtext_9595modeleObjects3.length; j < jLen ; ++j) {
        if ( gdjs.jeuCode.GDtext_9595modeleObjects2_1final.indexOf(gdjs.jeuCode.GDtext_9595modeleObjects3[j]) === -1 )
            gdjs.jeuCode.GDtext_9595modeleObjects2_1final.push(gdjs.jeuCode.GDtext_9595modeleObjects3[j]);
    }
}
}
{
gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects2_1final, gdjs.jeuCode.GDtext_9595modeleObjects2);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/w.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects1, gdjs.jeuCode.GDtext_9595modeleObjects2);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{gdjs.jeuCode.GDtext_9595modeleObjects2_1final.length = 0;
let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects1, gdjs.jeuCode.GDtext_9595modeleObjects3);

for (var i = 0, k = 0, l = gdjs.jeuCode.GDtext_9595modeleObjects3.length;i<l;++i) {
    if ( gdjs.jeuCode.GDtext_9595modeleObjects3[i].getBehavior("Text").getText() == "x" ) {
        isConditionTrue_1 = true;
        gdjs.jeuCode.GDtext_9595modeleObjects3[k] = gdjs.jeuCode.GDtext_9595modeleObjects3[i];
        ++k;
    }
}
gdjs.jeuCode.GDtext_9595modeleObjects3.length = k;
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.jeuCode.GDtext_9595modeleObjects3.length; j < jLen ; ++j) {
        if ( gdjs.jeuCode.GDtext_9595modeleObjects2_1final.indexOf(gdjs.jeuCode.GDtext_9595modeleObjects3[j]) === -1 )
            gdjs.jeuCode.GDtext_9595modeleObjects2_1final.push(gdjs.jeuCode.GDtext_9595modeleObjects3[j]);
    }
}
}
{
gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects1, gdjs.jeuCode.GDtext_9595modeleObjects3);

for (var i = 0, k = 0, l = gdjs.jeuCode.GDtext_9595modeleObjects3.length;i<l;++i) {
    if ( gdjs.jeuCode.GDtext_9595modeleObjects3[i].getBehavior("Text").getText() == "X" ) {
        isConditionTrue_1 = true;
        gdjs.jeuCode.GDtext_9595modeleObjects3[k] = gdjs.jeuCode.GDtext_9595modeleObjects3[i];
        ++k;
    }
}
gdjs.jeuCode.GDtext_9595modeleObjects3.length = k;
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.jeuCode.GDtext_9595modeleObjects3.length; j < jLen ; ++j) {
        if ( gdjs.jeuCode.GDtext_9595modeleObjects2_1final.indexOf(gdjs.jeuCode.GDtext_9595modeleObjects3[j]) === -1 )
            gdjs.jeuCode.GDtext_9595modeleObjects2_1final.push(gdjs.jeuCode.GDtext_9595modeleObjects3[j]);
    }
}
}
{
gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects2_1final, gdjs.jeuCode.GDtext_9595modeleObjects2);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/x.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects1, gdjs.jeuCode.GDtext_9595modeleObjects2);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{gdjs.jeuCode.GDtext_9595modeleObjects2_1final.length = 0;
let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects1, gdjs.jeuCode.GDtext_9595modeleObjects3);

for (var i = 0, k = 0, l = gdjs.jeuCode.GDtext_9595modeleObjects3.length;i<l;++i) {
    if ( gdjs.jeuCode.GDtext_9595modeleObjects3[i].getBehavior("Text").getText() == "y" ) {
        isConditionTrue_1 = true;
        gdjs.jeuCode.GDtext_9595modeleObjects3[k] = gdjs.jeuCode.GDtext_9595modeleObjects3[i];
        ++k;
    }
}
gdjs.jeuCode.GDtext_9595modeleObjects3.length = k;
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.jeuCode.GDtext_9595modeleObjects3.length; j < jLen ; ++j) {
        if ( gdjs.jeuCode.GDtext_9595modeleObjects2_1final.indexOf(gdjs.jeuCode.GDtext_9595modeleObjects3[j]) === -1 )
            gdjs.jeuCode.GDtext_9595modeleObjects2_1final.push(gdjs.jeuCode.GDtext_9595modeleObjects3[j]);
    }
}
}
{
gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects1, gdjs.jeuCode.GDtext_9595modeleObjects3);

for (var i = 0, k = 0, l = gdjs.jeuCode.GDtext_9595modeleObjects3.length;i<l;++i) {
    if ( gdjs.jeuCode.GDtext_9595modeleObjects3[i].getBehavior("Text").getText() == "Y" ) {
        isConditionTrue_1 = true;
        gdjs.jeuCode.GDtext_9595modeleObjects3[k] = gdjs.jeuCode.GDtext_9595modeleObjects3[i];
        ++k;
    }
}
gdjs.jeuCode.GDtext_9595modeleObjects3.length = k;
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.jeuCode.GDtext_9595modeleObjects3.length; j < jLen ; ++j) {
        if ( gdjs.jeuCode.GDtext_9595modeleObjects2_1final.indexOf(gdjs.jeuCode.GDtext_9595modeleObjects3[j]) === -1 )
            gdjs.jeuCode.GDtext_9595modeleObjects2_1final.push(gdjs.jeuCode.GDtext_9595modeleObjects3[j]);
    }
}
}
{
gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects2_1final, gdjs.jeuCode.GDtext_9595modeleObjects2);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/y.mp3", 1, false, 100, 1);
}}

}


{

/* Reuse gdjs.jeuCode.GDtext_9595modeleObjects1 */

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{gdjs.jeuCode.GDtext_9595modeleObjects1_1final.length = 0;
let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects1, gdjs.jeuCode.GDtext_9595modeleObjects2);

for (var i = 0, k = 0, l = gdjs.jeuCode.GDtext_9595modeleObjects2.length;i<l;++i) {
    if ( gdjs.jeuCode.GDtext_9595modeleObjects2[i].getBehavior("Text").getText() == "z" ) {
        isConditionTrue_1 = true;
        gdjs.jeuCode.GDtext_9595modeleObjects2[k] = gdjs.jeuCode.GDtext_9595modeleObjects2[i];
        ++k;
    }
}
gdjs.jeuCode.GDtext_9595modeleObjects2.length = k;
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.jeuCode.GDtext_9595modeleObjects2.length; j < jLen ; ++j) {
        if ( gdjs.jeuCode.GDtext_9595modeleObjects1_1final.indexOf(gdjs.jeuCode.GDtext_9595modeleObjects2[j]) === -1 )
            gdjs.jeuCode.GDtext_9595modeleObjects1_1final.push(gdjs.jeuCode.GDtext_9595modeleObjects2[j]);
    }
}
}
{
gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects1, gdjs.jeuCode.GDtext_9595modeleObjects2);

for (var i = 0, k = 0, l = gdjs.jeuCode.GDtext_9595modeleObjects2.length;i<l;++i) {
    if ( gdjs.jeuCode.GDtext_9595modeleObjects2[i].getBehavior("Text").getText() == "Z" ) {
        isConditionTrue_1 = true;
        gdjs.jeuCode.GDtext_9595modeleObjects2[k] = gdjs.jeuCode.GDtext_9595modeleObjects2[i];
        ++k;
    }
}
gdjs.jeuCode.GDtext_9595modeleObjects2.length = k;
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.jeuCode.GDtext_9595modeleObjects2.length; j < jLen ; ++j) {
        if ( gdjs.jeuCode.GDtext_9595modeleObjects1_1final.indexOf(gdjs.jeuCode.GDtext_9595modeleObjects2[j]) === -1 )
            gdjs.jeuCode.GDtext_9595modeleObjects1_1final.push(gdjs.jeuCode.GDtext_9595modeleObjects2[j]);
    }
}
}
{
gdjs.copyArray(gdjs.jeuCode.GDtext_9595modeleObjects1_1final, gdjs.jeuCode.GDtext_9595modeleObjects1);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/z.mp3", 1, false, 100, 1);
}}

}


};gdjs.jeuCode.eventsList28 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("clavier"), gdjs.jeuCode.GDclavierObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.jeuCode.mapOfGDgdjs_9546jeuCode_9546GDclavierObjects2Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {

{ //Subevents
gdjs.jeuCode.eventsList26(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("text_modele"), gdjs.jeuCode.GDtext_9595modeleObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.jeuCode.mapOfGDgdjs_9546jeuCode_9546GDtext_95959595modeleObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {

{ //Subevents
gdjs.jeuCode.eventsList27(runtimeScene);} //End of subevents
}

}


};gdjs.jeuCode.eventsList29 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("bouton_retour"), gdjs.jeuCode.GDbouton_9595retourObjects1);
gdjs.copyArray(runtimeScene.getObjects("faux_vrai"), gdjs.jeuCode.GDfaux_9595vraiObjects1);
gdjs.copyArray(runtimeScene.getObjects("sp_bouton_recommencer"), gdjs.jeuCode.GDsp_9595bouton_9595recommencerObjects1);
gdjs.copyArray(runtimeScene.getObjects("sp_cadre0"), gdjs.jeuCode.GDsp_9595cadre0Objects1);
gdjs.copyArray(runtimeScene.getObjects("sp_diamant"), gdjs.jeuCode.GDsp_9595diamantObjects1);
{runtimeScene.getScene().getVariables().getFromIndex(1).setNumber(0);
}{runtimeScene.getScene().getVariables().getFromIndex(2).getChild("entree").setNumber(0);
}{runtimeScene.getScene().getVariables().getFromIndex(2).getChild("modele").setNumber(1);
}{for(var i = 0, len = gdjs.jeuCode.GDsp_9595cadre0Objects1.length ;i < len;++i) {
    gdjs.jeuCode.GDsp_9595cadre0Objects1[i].getBehavior("Animation").pauseAnimation();
}
}{for(var i = 0, len = gdjs.jeuCode.GDsp_9595cadre0Objects1.length ;i < len;++i) {
    gdjs.jeuCode.GDsp_9595cadre0Objects1[i].setAnimationFrame(runtimeScene.getGame().getVariables().getFromIndex(0).getAsNumber() - 1);
}
}{for(var i = 0, len = gdjs.jeuCode.GDbouton_9595retourObjects1.length ;i < len;++i) {
    gdjs.jeuCode.GDbouton_9595retourObjects1[i].getBehavior("Animation").pauseAnimation();
}
}{for(var i = 0, len = gdjs.jeuCode.GDsp_9595bouton_9595recommencerObjects1.length ;i < len;++i) {
    gdjs.jeuCode.GDsp_9595bouton_9595recommencerObjects1[i].hide();
}
}{for(var i = 0, len = gdjs.jeuCode.GDfaux_9595vraiObjects1.length ;i < len;++i) {
    gdjs.jeuCode.GDfaux_9595vraiObjects1[i].hide();
}
}{for(var i = 0, len = gdjs.jeuCode.GDsp_9595diamantObjects1.length ;i < len;++i) {
    gdjs.jeuCode.GDsp_9595diamantObjects1[i].hide();
}
}
{ //Subevents
gdjs.jeuCode.eventsList4(runtimeScene);} //End of subevents
}

}


{


gdjs.jeuCode.eventsList6(runtimeScene);
}


{


gdjs.jeuCode.eventsList10(runtimeScene);
}


{



}


{


gdjs.jeuCode.eventsList14(runtimeScene);
}


{



}


{



}


{



}


{


gdjs.jeuCode.eventsList19(runtimeScene);
}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_retour"), gdjs.jeuCode.GDbouton_9595retourObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.jeuCode.mapOfGDgdjs_9546jeuCode_9546GDbouton_95959595retourObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {

{ //Subevents
gdjs.jeuCode.eventsList21(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("sp_bouton_recommencer"), gdjs.jeuCode.GDsp_9595bouton_9595recommencerObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.jeuCode.mapOfGDgdjs_9546jeuCode_9546GDsp_95959595bouton_95959595recommencerObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeuCode.GDsp_9595bouton_9595recommencerObjects1.length;i<l;++i) {
    if ( gdjs.jeuCode.GDsp_9595bouton_9595recommencerObjects1[i].isVisible() ) {
        isConditionTrue_0 = true;
        gdjs.jeuCode.GDsp_9595bouton_9595recommencerObjects1[k] = gdjs.jeuCode.GDsp_9595bouton_9595recommencerObjects1[i];
        ++k;
    }
}
gdjs.jeuCode.GDsp_9595bouton_9595recommencerObjects1.length = k;
}
}
if (isConditionTrue_0) {

{ //Subevents
gdjs.jeuCode.eventsList23(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("text_entree_1"), gdjs.jeuCode.GDtext_9595entree_95951Objects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeuCode.GDtext_9595entree_95951Objects1.length;i<l;++i) {
    if ( gdjs.jeuCode.GDtext_9595entree_95951Objects1[i].getVariableNumber(gdjs.jeuCode.GDtext_9595entree_95951Objects1[i].getVariables().getFromIndex(0)) == runtimeScene.getScene().getVariables().getFromIndex(1).getAsNumber() ) {
        isConditionTrue_0 = true;
        gdjs.jeuCode.GDtext_9595entree_95951Objects1[k] = gdjs.jeuCode.GDtext_9595entree_95951Objects1[i];
        ++k;
    }
}
gdjs.jeuCode.GDtext_9595entree_95951Objects1.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeuCode.GDtext_9595entree_95951Objects1.length;i<l;++i) {
    if ( gdjs.jeuCode.GDtext_9595entree_95951Objects1[i].getBehavior("Opacity").getOpacity() != 256 ) {
        isConditionTrue_0 = true;
        gdjs.jeuCode.GDtext_9595entree_95951Objects1[k] = gdjs.jeuCode.GDtext_9595entree_95951Objects1[i];
        ++k;
    }
}
gdjs.jeuCode.GDtext_9595entree_95951Objects1.length = k;
}
if (isConditionTrue_0) {
/* Reuse gdjs.jeuCode.GDtext_9595entree_95951Objects1 */
{for(var i = 0, len = gdjs.jeuCode.GDtext_9595entree_95951Objects1.length ;i < len;++i) {
    gdjs.jeuCode.GDtext_9595entree_95951Objects1[i].getBehavior("Opacity").setOpacity(256);
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 5;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(13854500);
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("bouton_retour"), gdjs.jeuCode.GDbouton_9595retourObjects1);
gdjs.copyArray(runtimeScene.getObjects("coffre"), gdjs.jeuCode.GDcoffreObjects1);
gdjs.copyArray(runtimeScene.getObjects("sp_bouton_recommencer"), gdjs.jeuCode.GDsp_9595bouton_9595recommencerObjects1);
gdjs.copyArray(runtimeScene.getObjects("sp_diamant"), gdjs.jeuCode.GDsp_9595diamantObjects1);
{for(var i = 0, len = gdjs.jeuCode.GDcoffreObjects1.length ;i < len;++i) {
    gdjs.jeuCode.GDcoffreObjects1[i].getBehavior("Animation").setAnimationIndex(1);
}
}{for(var i = 0, len = gdjs.jeuCode.GDbouton_9595retourObjects1.length ;i < len;++i) {
    gdjs.jeuCode.GDbouton_9595retourObjects1[i].getBehavior("Animation").resumeAnimation();
}
}{for(var i = 0, len = gdjs.jeuCode.GDsp_9595diamantObjects1.length ;i < len;++i) {
    gdjs.jeuCode.GDsp_9595diamantObjects1[i].hide(false);
}
}{runtimeScene.getScene().getVariables().getFromIndex(5).setNumber(5 - runtimeScene.getScene().getVariables().getFromIndex(4).getAsNumber());
}{for(var i = 0, len = gdjs.jeuCode.GDsp_9595bouton_9595recommencerObjects1.length ;i < len;++i) {
    gdjs.jeuCode.GDsp_9595bouton_9595recommencerObjects1[i].hide(false);
}
}{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/applaudissements.mp3", 2, false, 100, 1);
}
{ //Subevents
gdjs.jeuCode.eventsList25(runtimeScene);} //End of subevents
}

}


{


gdjs.jeuCode.eventsList28(runtimeScene);
}


};

gdjs.jeuCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.jeuCode.GDtext_9595entree_95951Objects1.length = 0;
gdjs.jeuCode.GDtext_9595entree_95951Objects2.length = 0;
gdjs.jeuCode.GDtext_9595entree_95951Objects3.length = 0;
gdjs.jeuCode.GDtext_9595entree_95951Objects4.length = 0;
gdjs.jeuCode.GDclic_9595caractereObjects1.length = 0;
gdjs.jeuCode.GDclic_9595caractereObjects2.length = 0;
gdjs.jeuCode.GDclic_9595caractereObjects3.length = 0;
gdjs.jeuCode.GDclic_9595caractereObjects4.length = 0;
gdjs.jeuCode.GDcadranObjects1.length = 0;
gdjs.jeuCode.GDcadranObjects2.length = 0;
gdjs.jeuCode.GDcadranObjects3.length = 0;
gdjs.jeuCode.GDcadranObjects4.length = 0;
gdjs.jeuCode.GDfaux_9595vraiObjects1.length = 0;
gdjs.jeuCode.GDfaux_9595vraiObjects2.length = 0;
gdjs.jeuCode.GDfaux_9595vraiObjects3.length = 0;
gdjs.jeuCode.GDfaux_9595vraiObjects4.length = 0;
gdjs.jeuCode.GDbbtext_9595modeleObjects1.length = 0;
gdjs.jeuCode.GDbbtext_9595modeleObjects2.length = 0;
gdjs.jeuCode.GDbbtext_9595modeleObjects3.length = 0;
gdjs.jeuCode.GDbbtext_9595modeleObjects4.length = 0;
gdjs.jeuCode.GDsp_9595bouton_9595recommencerObjects1.length = 0;
gdjs.jeuCode.GDsp_9595bouton_9595recommencerObjects2.length = 0;
gdjs.jeuCode.GDsp_9595bouton_9595recommencerObjects3.length = 0;
gdjs.jeuCode.GDsp_9595bouton_9595recommencerObjects4.length = 0;
gdjs.jeuCode.GDbouton_9595retourObjects1.length = 0;
gdjs.jeuCode.GDbouton_9595retourObjects2.length = 0;
gdjs.jeuCode.GDbouton_9595retourObjects3.length = 0;
gdjs.jeuCode.GDbouton_9595retourObjects4.length = 0;
gdjs.jeuCode.GDcoffreObjects1.length = 0;
gdjs.jeuCode.GDcoffreObjects2.length = 0;
gdjs.jeuCode.GDcoffreObjects3.length = 0;
gdjs.jeuCode.GDcoffreObjects4.length = 0;
gdjs.jeuCode.GDsp_9595diamantObjects1.length = 0;
gdjs.jeuCode.GDsp_9595diamantObjects2.length = 0;
gdjs.jeuCode.GDsp_9595diamantObjects3.length = 0;
gdjs.jeuCode.GDsp_9595diamantObjects4.length = 0;
gdjs.jeuCode.GDGreenDotBarObjects1.length = 0;
gdjs.jeuCode.GDGreenDotBarObjects2.length = 0;
gdjs.jeuCode.GDGreenDotBarObjects3.length = 0;
gdjs.jeuCode.GDGreenDotBarObjects4.length = 0;
gdjs.jeuCode.GDsp_9595cadre0Objects1.length = 0;
gdjs.jeuCode.GDsp_9595cadre0Objects2.length = 0;
gdjs.jeuCode.GDsp_9595cadre0Objects3.length = 0;
gdjs.jeuCode.GDsp_9595cadre0Objects4.length = 0;
gdjs.jeuCode.GDpapierObjects1.length = 0;
gdjs.jeuCode.GDpapierObjects2.length = 0;
gdjs.jeuCode.GDpapierObjects3.length = 0;
gdjs.jeuCode.GDpapierObjects4.length = 0;
gdjs.jeuCode.GDtext_9595modeleObjects1.length = 0;
gdjs.jeuCode.GDtext_9595modeleObjects2.length = 0;
gdjs.jeuCode.GDtext_9595modeleObjects3.length = 0;
gdjs.jeuCode.GDtext_9595modeleObjects4.length = 0;
gdjs.jeuCode.GDclavierObjects1.length = 0;
gdjs.jeuCode.GDclavierObjects2.length = 0;
gdjs.jeuCode.GDclavierObjects3.length = 0;
gdjs.jeuCode.GDclavierObjects4.length = 0;

gdjs.jeuCode.eventsList29(runtimeScene);
gdjs.jeuCode.GDtext_9595entree_95951Objects1.length = 0;
gdjs.jeuCode.GDtext_9595entree_95951Objects2.length = 0;
gdjs.jeuCode.GDtext_9595entree_95951Objects3.length = 0;
gdjs.jeuCode.GDtext_9595entree_95951Objects4.length = 0;
gdjs.jeuCode.GDclic_9595caractereObjects1.length = 0;
gdjs.jeuCode.GDclic_9595caractereObjects2.length = 0;
gdjs.jeuCode.GDclic_9595caractereObjects3.length = 0;
gdjs.jeuCode.GDclic_9595caractereObjects4.length = 0;
gdjs.jeuCode.GDcadranObjects1.length = 0;
gdjs.jeuCode.GDcadranObjects2.length = 0;
gdjs.jeuCode.GDcadranObjects3.length = 0;
gdjs.jeuCode.GDcadranObjects4.length = 0;
gdjs.jeuCode.GDfaux_9595vraiObjects1.length = 0;
gdjs.jeuCode.GDfaux_9595vraiObjects2.length = 0;
gdjs.jeuCode.GDfaux_9595vraiObjects3.length = 0;
gdjs.jeuCode.GDfaux_9595vraiObjects4.length = 0;
gdjs.jeuCode.GDbbtext_9595modeleObjects1.length = 0;
gdjs.jeuCode.GDbbtext_9595modeleObjects2.length = 0;
gdjs.jeuCode.GDbbtext_9595modeleObjects3.length = 0;
gdjs.jeuCode.GDbbtext_9595modeleObjects4.length = 0;
gdjs.jeuCode.GDsp_9595bouton_9595recommencerObjects1.length = 0;
gdjs.jeuCode.GDsp_9595bouton_9595recommencerObjects2.length = 0;
gdjs.jeuCode.GDsp_9595bouton_9595recommencerObjects3.length = 0;
gdjs.jeuCode.GDsp_9595bouton_9595recommencerObjects4.length = 0;
gdjs.jeuCode.GDbouton_9595retourObjects1.length = 0;
gdjs.jeuCode.GDbouton_9595retourObjects2.length = 0;
gdjs.jeuCode.GDbouton_9595retourObjects3.length = 0;
gdjs.jeuCode.GDbouton_9595retourObjects4.length = 0;
gdjs.jeuCode.GDcoffreObjects1.length = 0;
gdjs.jeuCode.GDcoffreObjects2.length = 0;
gdjs.jeuCode.GDcoffreObjects3.length = 0;
gdjs.jeuCode.GDcoffreObjects4.length = 0;
gdjs.jeuCode.GDsp_9595diamantObjects1.length = 0;
gdjs.jeuCode.GDsp_9595diamantObjects2.length = 0;
gdjs.jeuCode.GDsp_9595diamantObjects3.length = 0;
gdjs.jeuCode.GDsp_9595diamantObjects4.length = 0;
gdjs.jeuCode.GDGreenDotBarObjects1.length = 0;
gdjs.jeuCode.GDGreenDotBarObjects2.length = 0;
gdjs.jeuCode.GDGreenDotBarObjects3.length = 0;
gdjs.jeuCode.GDGreenDotBarObjects4.length = 0;
gdjs.jeuCode.GDsp_9595cadre0Objects1.length = 0;
gdjs.jeuCode.GDsp_9595cadre0Objects2.length = 0;
gdjs.jeuCode.GDsp_9595cadre0Objects3.length = 0;
gdjs.jeuCode.GDsp_9595cadre0Objects4.length = 0;
gdjs.jeuCode.GDpapierObjects1.length = 0;
gdjs.jeuCode.GDpapierObjects2.length = 0;
gdjs.jeuCode.GDpapierObjects3.length = 0;
gdjs.jeuCode.GDpapierObjects4.length = 0;
gdjs.jeuCode.GDtext_9595modeleObjects1.length = 0;
gdjs.jeuCode.GDtext_9595modeleObjects2.length = 0;
gdjs.jeuCode.GDtext_9595modeleObjects3.length = 0;
gdjs.jeuCode.GDtext_9595modeleObjects4.length = 0;
gdjs.jeuCode.GDclavierObjects1.length = 0;
gdjs.jeuCode.GDclavierObjects2.length = 0;
gdjs.jeuCode.GDclavierObjects3.length = 0;
gdjs.jeuCode.GDclavierObjects4.length = 0;


return;

}

gdjs['jeuCode'] = gdjs.jeuCode;
