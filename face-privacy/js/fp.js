// =======================
// Définition des variables globales et constantes
// =======================

// Constantes définissant les limites et références pour le canvas et le flou
const MAX_CANVAS_SIZE = 2000; // Taille maximale du canvas en pixels
const MIN_BLUR_RADIUS = 5;    // Rayon minimal pour l'effet de flou
const MAX_BLUR_RADIUS = 50;   // Rayon maximal pour l'effet de flou
const REFERENCE_FACE_WIDTH = 100; // Largeur de référence d'un visage pour calculer le flou

// Variables d'état pour gérer les modes de traitement et de dessin
let processingMode = 'blur'; // Mode de traitement actuel ('blur' ou 'emoji')
let drawingMode = 'cursor';  // Mode de dessin actuel ('cursor', 'brush', 'eraser', 'emoji')
let isDrawing = false;       // Indicateur si l'utilisateur est en train de dessiner
let isDrawingEmoji = false;  // Indicateur si l'utilisateur est en train de dessiner un emoji
let isModified = false;      // Indicateur si l'image a été modifiée
let allSelected = false;     // Indicateur si toutes les détections de visage sont sélectionnées

// Variables pour stocker les données de l'image originale et les détections de visage
let originalImageData;       // Données d'image originales du canvas
let originalWidth, originalHeight; // Dimensions originales de l'image
let aspectRatio;             // Rapport d'aspect de l'image (largeur/hauteur)
let scaleFactor = 1;         // Facteur d'échelle pour le redimensionnement
let originalImage, faceDetections, uniqueDetections; // Image originale et détections de visage

// Variables pour gérer les outils de dessin
let brushSize;               // Taille du pinceau ou de la gomme
let selectedEmoji = '😊';    // Emoji sélectionné pour l'outil emoji
let emojiSize = 30;          // Taille de l'emoji

let faces = [];              // Tableau pour stocker l'état de sélection des visages détectés

// =======================
// Sélecteurs d'éléments DOM
// =======================

// Sélection des éléments du DOM par leur ID
const imageInput = document.getElementById('imageInput');
const downloadButton = document.getElementById('downloadButton');
const selectAllButton = document.getElementById('selectAllButton');
const widthInput = document.getElementById('widthInput');
const heightInput = document.getElementById('heightInput');
const sizeSlider = document.getElementById('sizeSlider');
const sizeSliderZone = document.getElementById('sizeSlider_zone');
const sizeSliderLabel = document.getElementById('sizeSlider_label');
const customCursor = document.getElementById('customCursor');
const outputCanvas = document.getElementById('outputCanvas');
const ctx = outputCanvas.getContext('2d');
const facesContainer = document.getElementById('facesContainer');
const loadingOverlay = document.getElementById('loadingOverlay');

// =======================
// Modules de l'application
// =======================

/**
 * Module principal de l'application qui gère l'initialisation,
 * le chargement des modèles et la configuration des écouteurs d'événements.
 */
const App = {
    /**
     * Fonction d'initialisation de l'application.
     * Charge les modèles nécessaires, configure les écouteurs d'événements
     * et initialise les éléments interactifs.
     */
    init: async function() {
        await this.loadModels();          // Charge les modèles de détection de visage
        this.setupEventListeners();       // Configure les écouteurs d'événements
        this.setupCollapsibles();         // Configure les éléments collapsibles
    },

    /**
     * Charge les modèles nécessaires pour la détection de visage en utilisant face-api.js.
     * Les modèles sont chargés depuis le répertoire './weights/'.
     */
    loadModels: async function() {
        await faceapi.nets.ssdMobilenetv1.loadFromUri('./weights/');      // Modèle de détection de visage
        await faceapi.nets.faceLandmark68Net.loadFromUri('./weights/');   // Modèle de détection des landmarks faciaux
    },

    /**
     * Configure les écouteurs d'événements pour les interactions utilisateur,
     * tels que le téléchargement d'image, le téléchargement de l'image modifiée,
     * la sélection de toutes les détections, les modifications de dimension,
     * les ajustements de taille du pinceau/gomme/emoji, et les interactions avec le canvas.
     */
    setupEventListeners: function() {
        // Événements liés aux boutons principaux
        imageInput.addEventListener('change', Handlers.handleImageUpload);
        downloadButton.addEventListener('click', Handlers.downloadImage);
        selectAllButton.addEventListener('click', Handlers.toggleSelectAll);

        // Événements liés aux champs de dimension pour maintenir le rapport d'aspect
        widthInput.addEventListener('input', () => Utils.updateDimensionProportionally('width'));
        heightInput.addEventListener('input', () => Utils.updateDimensionProportionally('height'));

        // Événements liés au slider de taille
        sizeSlider.addEventListener('input', Handlers.handleSizeSliderInput);
        sizeSlider.addEventListener('mouseenter', (event) => {UI.toggleCustomCursorDisplay(event,'sliderView')});
        sizeSlider.addEventListener('touchstart', (event) => {UI.toggleCustomCursorDisplay(event,'sliderView')});

        // Événements liés au canvas pour afficher/masquer le curseur personnalisé
        outputCanvas.addEventListener('mouseenter', (event) => {UI.toggleCustomCursorDisplay(event,'')});
        outputCanvas.addEventListener('mouseleave', () => { customCursor.style.display = 'none'; });

        // Écouteurs pour les événements de pointeur (souris, touch)
        outputCanvas.addEventListener('pointerdown', Handlers.handlePointerDown, { passive: false });
        outputCanvas.addEventListener('pointermove', Handlers.handlePointerMove, { passive: false });
        outputCanvas.addEventListener('pointerup', Handlers.handlePointerUp, { passive: false });
        outputCanvas.addEventListener('pointerout', Handlers.handlePointerOut, { passive: false });

        // Configuration des boutons d'outils (curseur, pinceau, gomme, emoji)
        const toolButtons = [
            { id: 'cursorButton', mode: 'cursor', sizeDisplay: 'none', label: '' },
            { id: 'brushButton', mode: 'brush', sizeDisplay: 'flex', label: 'Taille du pinceau' },
            { id: 'eraserButton', mode: 'eraser', sizeDisplay: 'flex', label: 'Taille de la gomme' },
            { id: 'emojiButton', mode: 'emoji', sizeDisplay: 'flex', label: 'Taille de l\'émoji' },
        ];

        // Ajout des écouteurs pour chaque bouton d'outil
        toolButtons.forEach(({ id, mode, sizeDisplay, label }) => {
            document.getElementById(id).addEventListener('click', (event) => {
                UI.setActiveButton(id, mode, sizeDisplay, label); // Met à jour l'état de l'outil actif
                Drawing.updateCursorPosition(event,'sliderView'); // Met à jour la position du curseur
            });
        });

        // Configuration des boutons du mode de traitement (flou, emoji)
        document.getElementById('blurModeButton').addEventListener('click', () => {
            UI.setProcessingMode('blur'); // Active le mode de flou
        });

        document.getElementById('emojiModeButton').addEventListener('click', () => {
            UI.setProcessingMode('emoji'); // Active le mode d'emoji
        });

        // Configuration des éléments accordéon pour les sections déroulantes du footer
        const accordions = document.querySelectorAll(".accordion");
        accordions.forEach(accordion => {
            accordion.addEventListener("click", function () {
                this.classList.toggle("active"); // Alterne la classe 'active' pour le style
                const panel = this.nextElementSibling; // Sélectionne le panneau associé
                if (panel.style.maxHeight) {
                    panel.style.maxHeight = null; // Réduit le panneau s'il est ouvert
                } else {
                    panel.style.maxHeight = panel.scrollHeight + "px"; // Ouvre le panneau
                }
            });
        });
    },

    /**
     * Configure les éléments collapsibles pour les sections
     * déroulantes du menu du haut
     */
    setupCollapsibles: function() {
        const coll = document.querySelectorAll(".collapsible");
        coll.forEach(item => {
            item.addEventListener("click", function () {
                this.classList.toggle("active"); // Alterne la classe 'active' pour le style
                const content = this.nextElementSibling; // Sélectionne le contenu associé
                content.style.display = (content.style.display === "grid" || content.style.display === "block") ? "none" : "grid"; // Alterne l'affichage
            });
        });
    }
};

/**
 * Module responsable de la gestion des outils de dessin,
 * y compris le curseur personnalisé et les actions de dessin.
 */
const Drawing = {
    /**
     * Définit le mode de dessin actuel et met à jour le curseur du canvas.
     */
    setDrawingMode: function(mode) {
        drawingMode = mode; // Met à jour le mode de dessin global
        outputCanvas.style.cursor = mode === 'cursor' ? 'default' : 'none'; // Change le curseur du canvas
    },

    /**
     * Met à jour la position et l'apparence du curseur personnalisé en fonction de l'événement.
     */
    updateCursorPosition: function(event, mode) {
        const rect = outputCanvas.getBoundingClientRect(); // Récupère les dimensions et la position du canvas

        // Calcule le facteur d'échelle entre la taille réelle du canvas et sa taille affichée
        const scaleX = rect.width / outputCanvas.width;
        const scaleY = rect.height / outputCanvas.height;
        const scale = Math.min(scaleX, scaleY); // Utilise le facteur le plus petit pour conserver les proportions

        // Ajuste la taille du curseur en fonction du facteur d'échelle
        const cursorSize = brushSize * 1.2 * scale;

        // Récupère les coordonnées du pointeur
        const clientX = event.clientX || event.pageX;
        const clientY = event.clientY || event.pageY;

        // Positionne le curseur personnalisé
        if (mode == 'sliderView') {
            // Position spécifique lorsque le curseur est sur le slider
            customCursor.style.left = `${window.scrollX + rect.left + rect.width / 2 - cursorSize / 2}px`;
            customCursor.style.top = `${window.scrollY + rect.top + 100 - cursorSize / 2}px`;
        } else {
            // Positionnement normal du curseur
            customCursor.style.left = `${clientX + window.scrollX - cursorSize / 2}px`;
            customCursor.style.top = `${clientY + window.scrollY - cursorSize / 2}px`;
        }

        // Met à jour l'apparence du curseur personnalisé en fonction du mode de dessin
        if (drawingMode !== 'cursor') {
            customCursor.style.border = 'block'; // Affiche la bordure du curseur
            if (drawingMode === 'emoji') {
                // Mode emoji : affiche l'emoji sélectionné
                customCursor.classList.remove('customCursor');
                customCursor.innerHTML = selectedEmoji;
                customCursor.style.fontSize = `${cursorSize}px`;
                customCursor.style.width = `${cursorSize}px`;
                customCursor.style.height = `${cursorSize}px`;
                customCursor.style.border = 'none';
            } else {
                // Modes pinceau et gomme : affiche un cercle avec une bordure colorée
                customCursor.classList.add('customCursor');
                customCursor.innerHTML = '';
                customCursor.style.width = `${cursorSize}px`;
                customCursor.style.height = `${cursorSize}px`;
                customCursor.style.borderRadius = '50%';
                customCursor.style.border = `3px solid ${drawingMode === 'brush' ? 'blue' : 'red'}`; // Bleu pour pinceau, rouge pour gomme
            }
        } else {
            // Mode curseur : masque le curseur personnalisé
            customCursor.style.display = 'none';
        }
    },

    /**
     * Démarre le processus de dessin
     * en appelant la fonction de dessin initiale.
     */
    startDrawing: function(event) {
        isDrawing = true; // Indique que l'utilisateur commence à dessiner
        this.draw(event);  // Appelle la fonction de dessin
    },


    stopDrawing: function() {
        isDrawing = false; // Indique que l'utilisateur a cessé de dessiner
    },

    /**
     * Gère le dessin en fonction du mode de dessin actuel.
     * Applique le flou ou efface une zone en fonction du mode.
     */
    draw: function(event) {
        if (!isDrawing) return; // Si l'utilisateur n'est pas en train de dessiner, ne rien faire

        isModified = true; // Indique que l'image a été modifiée
        const ctx = outputCanvas.getContext('2d'); // Récupère le contexte du canvas
        const { offsetX, offsetY } = Utils.getRelativePosition(event, outputCanvas); // Récupère les positions relatives sur le canvas
        const radius = brushSize * 0.6; // Calcule le rayon de l'outil de dessin

        switch (drawingMode) {
            case 'brush':
                // Applique le flou à la zone spécifiée
                const tempCanvas = document.createElement('canvas'); // Crée un canvas temporaire
                const tempCtx = tempCanvas.getContext('2d');
                tempCanvas.width = radius * 2; // Définit la largeur du canvas temporaire
                tempCanvas.height = radius * 2; // Définit la hauteur du canvas temporaire

                // Dessine une partie du canvas principal sur le canvas temporaire
                tempCtx.drawImage(
                    outputCanvas,
                    offsetX - radius, offsetY - radius, radius * 2, radius * 2, // Source sur le canvas principal
                    0, 0, radius * 2, radius * 2 // Destination sur le canvas temporaire
                );

                // Récupère les données d'image du canvas temporaire
                const imageData = tempCtx.getImageData(0, 0, radius * 2, radius * 2);
                // Applique un flou gaussien aux données d'image
                const blurredData = ImageProcessing.applyGaussianBlur(imageData, 5);
                // Remplace les données d'image du canvas temporaire par les données floutées
                tempCtx.putImageData(blurredData, 0, 0);

                // Enregistre l'état actuel du contexte
                ctx.save();
                // Crée un chemin circulaire pour définir la zone de flou
                ctx.beginPath();
                ctx.arc(offsetX, offsetY, radius, 0, 2 * Math.PI);
                ctx.closePath();
                ctx.clip(); // Applique le chemin comme masque de découpe

                // Dessine le canvas temporaire flouté sur le canvas principal
                ctx.drawImage(tempCanvas, offsetX - radius, offsetY - radius);

                // Restaure l'état précédent du contexte
                ctx.restore();
                break;

            case 'eraser':
                // Efface la zone en restaurant les données d'image originales
                const scaleX = originalImage.width / outputCanvas.width; // Facteur d'échelle en largeur
                const scaleY = originalImage.height / outputCanvas.height; // Facteur d'échelle en hauteur

                // Calcule les coordonnées originales dans l'image originale
                const originalX = Math.floor(offsetX * scaleX);
                const originalY = Math.floor(offsetY * scaleY);

                // Récupère les données d'image actuelles de la zone à effacer
                const eraserImageData = ctx.getImageData(offsetX - radius, offsetY - radius, radius * 2, radius * 2);

                // Parcourt chaque pixel de la zone à effacer
                for (let y = 0; y < radius * 2; y++) {
                    for (let x = 0; x < radius * 2; x++) {
                        // Calcule les coordonnées originales correspondantes
                        const srcX = originalX - Math.floor(radius * scaleX) + Math.floor(x * scaleX);
                        const srcY = originalY - Math.floor(radius * scaleY) + Math.floor(y * scaleY);

                        // Vérifie si les coordonnées originales sont valides
                        if (srcX >= 0 && srcX < originalImage.width && srcY >= 0 && srcY < originalImage.height) {
                            const srcIndex = (srcY * originalImage.width + srcX) * 4; // Index des données originales
                            const destIndex = (y * eraserImageData.width + x) * 4; // Index des données à effacer

                            // Calcule la distance par rapport au centre pour un effet d'effacement progressif
                            const distanceFromCenter = Math.sqrt((x - radius) ** 2 + (y - radius) ** 2);
                            const opacity = Math.max(0, Math.min(1, distanceFromCenter / radius));

                            // Mélange les couleurs pour effacer progressivement
                            for (let i = 0; i < 3; i++) {
                                eraserImageData.data[destIndex + i] =
                                    opacity * eraserImageData.data[destIndex + i] +
                                    (1 - opacity) * originalImageData.data[srcIndex + i];
                            }
                            // Maintient l'opacité alpha
                            eraserImageData.data[destIndex + 3] = eraserImageData.data[destIndex + 3];
                        }
                    }
                }

                // Met à jour les données d'image du canvas principal avec les données effacées
                ctx.putImageData(eraserImageData, offsetX - radius, offsetY - radius);
                break;
        }
    },

    /**
     * Dessine un emoji à la position spécifiée sur le canvas.
     */
    drawEmoji: function(ctx, x, y) {
        const cursorSize = brushSize * 1.2; // Taille de l'emoji basée sur la taille du pinceau
        ctx.font = `${cursorSize}px Arial`; // Définit la police et la taille de l'emoji
        ctx.textAlign = 'center';            // Centre le texte horizontalement
        ctx.textBaseline = 'middle';         // Centre le texte verticalement
        ctx.fillText(selectedEmoji, x, y + cursorSize * 0.15); // Dessine l'emoji sur le canvas
    }
};

/**
 * Module responsable du traitement d'image, notamment l'application de flou gaussien
 * et la génération de l'image floutée ou avec emojis appliqués.
 */
const ImageProcessing = {
    /**
     * Applique un flou gaussien aux données d'image fournies.
     * Utilise fx.js pour le traitement du flou à l'aide du GPU.
     */
    applyGaussianBlur: function(imageData, blurRadius) {
        const fxCanvas = fx.canvas(); // Crée un canvas WebGL via fx.js
        const texture = fxCanvas.texture(imageData); // Crée une texture à partir des données d'image

        // Applique le flou gaussien avec le rayon spécifié
        fxCanvas.draw(texture).triangleBlur(blurRadius).update();

        // Crée un canvas 2D temporaire pour récupérer les données floutées
        const tempCanvas = document.createElement('canvas');
        tempCanvas.width = imageData.width;
        tempCanvas.height = imageData.height;
        const tempCtx = tempCanvas.getContext('2d');

        // Dessine le fxCanvas (WebGL) sur le canvas 2D temporaire
        tempCtx.drawImage(fxCanvas, 0, 0);

        // Récupère les données d'image floutées
        const blurredImageData = tempCtx.getImageData(0, 0, tempCanvas.width, tempCanvas.height);

        return blurredImageData; // Retourne les données floutées
    },

    /**
     * Calcule le rayon de flou basé sur la largeur du visage détecté.
     */
    calculateBlurRadius: function(faceWidth) {
        return Math.round(
            Math.max(MIN_BLUR_RADIUS, Math.min(MAX_BLUR_RADIUS, (faceWidth / REFERENCE_FACE_WIDTH) * 50))
        ); // Calcule le rayon de flou en respectant les limites définies
    },

    /**
     * Génère l'image traitée avec les effets de flou ou d'emoji appliqués
     * aux visages détectés et sélectionnés.
     */
    generateBlurredImage: function() {
        UI.showLoadingOverlay(); // Affiche une superposition de chargement
        isModified = true;       // Indique que l'image a été modifiée

        setTimeout(() => { // Utilise un timeout pour permettre au navigateur de mettre à jour l'interface
            const ctx = outputCanvas.getContext('2d'); // Récupère le contexte du canvas principal

            // Redimensionne le canvas principal pour correspondre à l'image originale
            outputCanvas.width = originalImage.width;
            outputCanvas.height = originalImage.height;

            ctx.drawImage(originalImage, 0, 0); // Dessine l'image originale sur le canvas

            // Crée un canvas temporaire pour le traitement
            const tempCanvas = document.createElement('canvas');
            const tempCtx = tempCanvas.getContext('2d');
            tempCanvas.width = outputCanvas.width;
            tempCanvas.height = outputCanvas.height;

            // Parcourt chaque détection de visage unique
            uniqueDetections.forEach((detection, index) => {
                if (faces[index]) { // Si le visage est sélectionné pour le traitement
                    const { x, y, width, height } = detection.detection.box; // Récupère les coordonnées du visage
                    if (processingMode === 'blur') {
                        // Applique le flou au visage
                        const centerX = x + width / 2; // Calcul du centre X du visage
                        const centerY = y + height / 2 - height / 10; // Calcul du centre Y du visage avec ajustement

                        const scale = 1.2; // Facteur d'agrandissement pour le rayon de flou
                        const radius = (Math.max(width, height) / 2) * scale; // Calcul du rayon de flou

                        tempCtx.clearRect(0, 0, tempCanvas.width, tempCanvas.height); // Efface le canvas temporaire

                        // Dessine la zone du visage sur le canvas temporaire
                        tempCtx.drawImage(
                            outputCanvas,
                            centerX - radius, centerY - radius, radius * 2, radius * 2, // Source sur le canvas principal
                            centerX - radius, centerY - radius, radius * 2, radius * 2  // Destination sur le canvas temporaire
                        );

                        // Récupère les données d'image du visage
                        const imageData = tempCtx.getImageData(centerX - radius, centerY - radius, radius * 2, radius * 2);
                        // Applique le flou gaussien aux données d'image
                        const blurredData = this.applyGaussianBlur(imageData, this.calculateBlurRadius(width));
                        // Remplace les données d'image du canvas temporaire par les données floutées
                        tempCtx.putImageData(blurredData, centerX - radius, centerY - radius);

                        // Crée un dégradé radial pour adoucir les bords du flou
                        const gradient = tempCtx.createRadialGradient(
                            centerX, centerY, 0,
                            centerX, centerY, radius
                        );
                        gradient.addColorStop(0, 'rgba(255, 255, 255, 1)');
                        gradient.addColorStop(0.8, 'rgba(255, 255, 255, 1)');
                        gradient.addColorStop(1, 'rgba(255, 255, 255, 0)');

                        tempCtx.globalCompositeOperation = 'destination-in'; // Change le mode de composition
                        tempCtx.fillStyle = gradient; // Applique le dégradé comme remplissage
                        tempCtx.beginPath();
                        tempCtx.arc(centerX, centerY, radius, 0, 2 * Math.PI); // Crée un chemin circulaire
                        tempCtx.fill(); // Remplit le cercle avec le dégradé

                        ctx.save(); // Sauvegarde l'état actuel du contexte
                        ctx.beginPath();
                        ctx.arc(centerX, centerY, radius, 0, 2 * Math.PI); // Crée un chemin circulaire sur le canvas principal
                        ctx.closePath();
                        ctx.clip(); // Applique le chemin comme masque de découpe

                        ctx.drawImage(tempCanvas, 0, 0); // Dessine le canvas temporaire flouté sur le canvas principal

                        ctx.restore(); // Restaure l'état précédent du contexte
                        tempCtx.globalCompositeOperation = 'source-over'; // Réinitialise le mode de composition
                    } else if (processingMode === 'emoji') {
                        // Dessine un emoji sur le visage
                        const emoji = selectedEmoji; // Emoji sélectionné
                        const fontSize = width * 1.5; // Taille de l'emoji basée sur la largeur du visage
                        const centerX = x + width / 2; // Centre X du visage
                        const centerY = y + height / 2; // Centre Y du visage
                        const leftEye = detection.landmarks.getLeftEye();   // Coordonnées de l'œil gauche
                        const rightEye = detection.landmarks.getRightEye(); // Coordonnées de l'œil droit
                        const eyeAngle = Math.atan2(
                            rightEye[0].y - leftEye[0].y,
                            rightEye[0].x - leftEye[0].x
                        ); // Calcul de l'angle des yeux pour orienter l'emoji

                        ctx.save(); // Sauvegarde l'état actuel du contexte
                        ctx.translate(centerX, centerY); // Déplace le contexte au centre du visage
                        ctx.rotate(eyeAngle); // Oriente le contexte selon l'angle des yeux
                        ctx.font = `${fontSize}px Arial`; // Définit la police et la taille de l'emoji
                        ctx.textAlign = 'center'; // Centre le texte horizontalement
                        ctx.textBaseline = 'middle'; // Centre le texte verticalement
                        ctx.fillText(emoji, 0, 0); // Dessine l'emoji sur le canvas
                        ctx.restore(); // Restaure l'état précédent du contexte
                    }
                }
            });

            downloadButton.style.display = 'inline-block'; // Affiche le bouton de téléchargement
            UI.hideLoadingOverlay(); // Masque la superposition de chargement
        }, 10); // Délai minimal pour permettre le rendu de l'interface utilisateur
    }
};

/**
 * Module responsable de la gestion des interactions utilisateur,
 * tel que le téléchargement d'images, les événements de pointeur,
 * l'ajustement des tailles d'outils, la sélection/désélection des visages,
 * et le téléchargement de l'image modifiée.
 */
const Handlers = {
    /**
     * Gère le téléchargement et le traitement de l'image uploadée par l'utilisateur.
     */
    handleImageUpload: async function(event) {
        if (isModified) { // Vérifie si l'image a déjà été modifiée
            const confirmReset = confirm("Attention : le travail déjà effectué sera perdu. Voulez-vous continuer ?");
            if (!confirmReset) { // Si l'utilisateur annule, réinitialise le champ de fichier
                event.target.value = '';
                return;
            } else {
                isModified = false; // Réinitialise l'état de modification
            }
        }
        UI.showLoadingOverlay(); // Affiche une superposition de chargement
        const file = event.target.files[0]; // Récupère le fichier téléchargé
        window.filename = file.name.split('.').slice(0, -1).join('.'); // Extrait le nom du fichier sans l'extension
        const img = new Image(); // Crée un nouvel objet Image
        selectAllButton.textContent = "Sélectionner tout"; // Réinitialise le texte du bouton "Sélectionner tout"
        img.onload = async () => { // Lorsque l'image est chargée
            faces = [];          // Réinitialise le tableau des visages
            allSelected = false; // Réinitialise l'état de sélection globale
            document.getElementById("content").style.display = "block"; // Affiche le contenu principal

            const tempCanvas = document.createElement('canvas'); // Crée un canvas temporaire
            const tempCtx = tempCanvas.getContext('2d');

            let width = img.width;   // Largeur de l'image
            let height = img.height; // Hauteur de l'image

            // Redimensionne l'image si elle dépasse les dimensions maximales
            if (width > MAX_CANVAS_SIZE || height > MAX_CANVAS_SIZE) {
                const imgAspectRatio = width / height; // Calcule le rapport d'aspect de l'image
                if (width > height) {
                    width = MAX_CANVAS_SIZE;
                    height = Math.round(MAX_CANVAS_SIZE / imgAspectRatio);
                } else {
                    height = MAX_CANVAS_SIZE;
                    width = Math.round(MAX_CANVAS_SIZE * imgAspectRatio);
                }
            }

            tempCanvas.width = width;   // Définit la largeur du canvas temporaire
            tempCanvas.height = height; // Définit la hauteur du canvas temporaire

            tempCtx.drawImage(img, 0, 0, width, height); // Dessine l'image redimensionnée sur le canvas temporaire

            originalImage = new Image(); // Crée un nouvel objet Image pour stocker l'image originale
            originalImage.src = tempCanvas.toDataURL(); // Définit la source de l'image originale à partir du canvas temporaire

            originalImage.onload = async () => { // Lorsque l'image originale est chargée
                outputCanvas.width = width;    // Définit la largeur du canvas principal
                outputCanvas.height = height;  // Définit la hauteur du canvas principal
                ctx.drawImage(originalImage, 0, 0); // Dessine l'image originale sur le canvas principal

                originalImageData = ctx.getImageData(0, 0, outputCanvas.width, outputCanvas.height); // Récupère les données d'image originales

                originalWidth = width;        // Stocke la largeur originale
                originalHeight = height;      // Stocke la hauteur originale
                aspectRatio = originalWidth / originalHeight; // Calcule le rapport d'aspect

                const rect = outputCanvas.getBoundingClientRect(); // Récupère les dimensions et la position du canvas
                scaleFactor = outputCanvas.width / rect.width;     // Calcule le facteur d'échelle

                sizeSlider.value = 30; // Définit la valeur initiale du slider de taille
                brushSize = parseInt(30 * originalWidth / 400, 10); // Calcule la taille initiale du pinceau

                widthInput.value = originalWidth;   // Remplit le champ de largeur
                heightInput.value = originalHeight; // Remplit le champ de hauteur

                // Configure les options pour la détection des visages avec face-api.js
                const ssdOptions = new faceapi.SsdMobilenetv1Options({
                    minConfidence: 0.4, // Confiance minimale pour considérer une détection valide
                    maxResults: 200     // Nombre maximal de détections
                });
                
                // Détecte tous les visages dans le canvas avec leurs landmarks
                faceDetections = await faceapi.detectAllFaces(outputCanvas, ssdOptions).withFaceLandmarks();

                // Filtre les détections pour éliminer les doublons en utilisant un seuil IoU
                uniqueDetections = Utils.filterDuplicateDetections(faceDetections, 0.1);

                // Définit la taille d'affichage pour redimensionner les résultats des détections
                const displaySize = {
                    width: outputCanvas.width,
                    height: outputCanvas.height
                };
                const resizedDetections = faceapi.resizeResults(uniqueDetections, displaySize);

                // Réinitialise le conteneur des visages détectés
                facesContainer.innerHTML = '';

                // Génère l'image floutée initiale en fonction des détections
                ImageProcessing.generateBlurredImage();

                // Itère sur chaque détection unique pour créer des miniatures des visages
                resizedDetections.forEach((detection, index) => {
                    // Crée un canvas pour la miniature du visage
                    const faceCanvas = document.createElement('canvas');
                    const faceContext = faceCanvas.getContext('2d');

                    // Agrandit la boîte de détection pour inclure un peu plus de contexte autour du visage
                    const { x, y, width, height } = Utils.enlargeBox(detection.detection.box, 1.4, outputCanvas);

                    // Définit les dimensions du canvas de la miniature
                    faceCanvas.width = 110;
                    faceCanvas.height = Math.floor(faceCanvas.width * height / width);

                    // Dessine la portion agrandie du visage sur le canvas de la miniature
                    faceContext.drawImage(outputCanvas, x, y, width, height, 0, 0, faceCanvas.width, faceCanvas.height);

                    // Crée un conteneur div pour la miniature du visage
                    const faceBox = document.createElement('div');
                    faceBox.classList.add('faceBox');
                    faceBox.id = `face${index}`;
                    faceBox.appendChild(faceCanvas);

                    // Ajoute la miniature au conteneur principal des visages
                    facesContainer.appendChild(faceBox);

                    // Initialise l'état de sélection du visage (non sélectionné par défaut)
                    faces[index] = false;
                    
                    // Définit la bordure de la miniature pour indiquer son état (vert = non sélectionné)
                    faceBox.style.border = `10px solid green`;

                    // Ajoute un écouteur d'événement pour permettre à l'utilisateur de sélectionner/désélectionner le visage
                    faceBox.addEventListener('click', function () {
                        Handlers.toggleBlur(index);
                    });
                });

                // Configure le mode de dessin par défaut
                Drawing.setDrawingMode('cursor');
                UI.resetToolButtons();
                sizeSliderZone.style.display = 'none';

                // Masque la superposition de chargement une fois le traitement terminé
                UI.hideLoadingOverlay();
            };
        };

        img.src = URL.createObjectURL(file);
    },

     /**
     * Gère l'événement de pression du pointeur (souris ou doigt) sur le canvas.
     */
    handlePointerDown: function(event) {
        event.preventDefault(); // Empêche le comportement par défaut (comme le défilement sur les appareils tactiles)

        // Affiche le curseur personnalisé si le mode de dessin n'est pas 'cursor'
        customCursor.style.display = drawingMode !== "cursor" ? 'block' : 'none';

        // Met à jour la position du curseur personnalisé en fonction de l'événement
        Drawing.updateCursorPosition(event, '');

        // Vérifie si le mode de dessin est 'emoji' et si l'utilisateur n'est pas déjà en train de dessiner un emoji
        if (drawingMode === 'emoji' && !isDrawingEmoji) {
            const ctx = outputCanvas.getContext('2d');
            const { offsetX, offsetY } = Utils.getRelativePosition(event, outputCanvas);

            // Dessine l'emoji à la position spécifiée
            Drawing.drawEmoji(ctx, offsetX, offsetY);

            // Met à jour l'état pour indiquer qu'un emoji est en cours de dessin
            isDrawingEmoji = true;
            isModified = true;
        } else if (drawingMode !== 'emoji') {
            // Démarre le processus de dessin pour les modes 'brush' ou 'eraser'
            Drawing.startDrawing(event);
        }
    },

    /**
     * Gère l'événement de déplacement du pointeur sur le canvas.
     */
    handlePointerMove: function(event) {
        event.preventDefault(); // Empêche le comportement par défaut

        // Met à jour la position du curseur personnalisé
        Drawing.updateCursorPosition(event, '');

        // Continue le processus de dessin si l'utilisateur est en train de dessiner
        Drawing.draw(event);
    },

    /**
     * Gère l'événement de relâchement du pointeur sur le canvas.
     */
    handlePointerUp: function(event) {
        event.preventDefault(); // Empêche le comportement par défaut

        // Réinitialise l'état de dessin d'emoji et arrête le dessin
        isDrawingEmoji = false;
        Drawing.stopDrawing();
    },

    /**
     * Gère l'événement lorsque le pointeur quitte le canvas.
     */
    handlePointerOut: function(event) {
        event.preventDefault(); // Empêche le comportement par défaut

        // Réinitialise l'état de dessin d'emoji, masque le curseur personnalisé et arrête le dessin
        isDrawingEmoji = false;
        customCursor.style.display = 'none';
        Drawing.stopDrawing();
    },

    /**
     * Gère l'événement de changement de valeur du slider de taille.
     */
    handleSizeSliderInput: function(event) {
        // Calcule la nouvelle taille du pinceau en fonction de la largeur originale de l'image
        brushSize = parseInt(event.target.value * originalWidth / 400, 10);

        // Met à jour la position et la taille du curseur personnalisé en mode 'sliderView'
        Drawing.updateCursorPosition(event, 'sliderView');
    },

    /**
     * Bascule la sélection de tous les visages détectés.
     * Si tous les visages sont sélectionnés, ils seront désélectionnés et vice versa.
     */
    toggleSelectAll: function() {
        // Inverse l'état de sélection globale
        allSelected = !allSelected;

        // Parcourt tous les visages et met à jour leur état de sélection
        for (let i = 0; i < faces.length; i++) {
            faces[i] = allSelected; // Définit la sélection du visage
            const faceBox = document.getElementById(`face${i}`); // Récupère l'élément DOM correspondant au visage

            // Met à jour la bordure pour indiquer l'état de sélection (rouge = sélectionné, vert = non sélectionné)
            faceBox.style.border = faces[i] ? `10px solid red` : `10px solid green`;
        }

        // Met à jour le texte du bouton en fonction de l'état de sélection globale
        selectAllButton.textContent = allSelected ? 'Désélectionner tout' : 'Sélectionner tout';

        // Génère l'image floutée en fonction des visages sélectionnés
        ImageProcessing.generateBlurredImage();
    },

    /**
     * Bascule l'effet de flou sur un visage spécifique.
     */
    toggleBlur: function(index) {
        // Inverse l'état de sélection du visage spécifié
        faces[index] = !faces[index];

        // Récupère l'élément DOM correspondant au visage
        const faceBox = document.getElementById(`face${index}`);

        // Met à jour la bordure pour indiquer l'état de sélection (rouge = sélectionné, vert = non sélectionné)
        faceBox.style.border = faces[index] ? `10px solid red` : `10px solid green`;

        // Génère l'image floutée en fonction des visages sélectionnés
        ImageProcessing.generateBlurredImage();
    },

    /**
     * Gère le téléchargement de l'image modifiée par l'utilisateur.
     * L'utilisateur peut télécharger l'image avec les modifications appliquées.
     */
    downloadImage: function() {
        // Récupère les nouvelles dimensions à partir des champs de saisie
        const newWidth = parseInt(widthInput.value, 10);
        const newHeight = parseInt(heightInput.value, 10);

        // Vérifie si les dimensions sont valides
        if (isNaN(newWidth) || isNaN(newHeight)) {
            alert("Veuillez entrer des dimensions valides.");
            return;
        }

        // Crée un canvas temporaire pour redimensionner l'image avant le téléchargement
        const tempCanvas = document.createElement('canvas');
        const tempCtx = tempCanvas.getContext('2d');
        tempCanvas.width = newWidth;
        tempCanvas.height = newHeight;

        // Dessine l'image du canvas principal sur le canvas temporaire avec les nouvelles dimensions
        tempCtx.drawImage(outputCanvas, 0, 0, outputCanvas.width, outputCanvas.height, 0, 0, newWidth, newHeight);

        // Convertit le canvas temporaire en un blob JPEG
        tempCanvas.toBlob((blob) => {
            const url = URL.createObjectURL(blob); // Crée une URL pour le blob

            // Crée un élément de lien pour déclencher le téléchargement
            const link = document.createElement('a');
            link.href = url;
            link.download = filename + '_blur.jpg'; // Définit le nom du fichier téléchargé

            // Vérifie si l'utilisateur est sur un appareil mobile
            if (/Mobi|Android/i.test(navigator.userAgent)) {
                // Ouvre l'image dans une nouvelle fenêtre/tabulation
                const newWindow = window.open(url, '_blank');
                if (!newWindow) {
                    alert("Veuillez autoriser les pop-ups pour afficher l'image.");
                }
            } else {
                // Simule un clic sur le lien pour déclencher le téléchargement
                link.click();
            }

            // Libère l'URL créée pour le blob
            URL.revokeObjectURL(url);
        }, 'image/jpeg');
    }
};

const Utils = {
    /**
     * Calcule la position relative du pointeur (souris ou toucher) par rapport au canvas.
     */
    getRelativePosition: function(event, canvas) {
        // Récupère les dimensions et la position du canvas par rapport à la fenêtre
        const rect = canvas.getBoundingClientRect();
        
        // Calcule le facteur d'échelle entre la taille réelle du canvas et sa taille affichée
        const scaleX = canvas.width / rect.width;
        const scaleY = canvas.height / rect.height;

        let clientX, clientY;

        // Détecte si l'événement est un toucher (touch event)
        if (event.touches && event.touches.length > 0) {
            clientX = event.touches[0].clientX;
            clientY = event.touches[0].clientY;
        } 
        // Sinon, vérifie si les propriétés clientX et clientY existent (mouse event)
        else if (event.clientX != null && event.clientY != null) {
            clientX = event.clientX;
            clientY = event.clientY;
        } 
        // Si aucune position n'est disponible, retourne des coordonnées nulles
        else {
            return { offsetX: 0, offsetY: 0 };
        }

        // Calcule la position relative en tenant compte de l'échelle
        return {
            offsetX: (clientX - rect.left) * scaleX,
            offsetY: (clientY - rect.top) * scaleY,
        };
    },

    /**
     * Met à jour les dimensions du canvas de manière proportionnelle en fonction de la dimension modifiée.
     */
    updateDimensionProportionally: function(changedDimension) {
        // Récupère les nouvelles valeurs de largeur et de hauteur à partir des champs de saisie
        const newWidth = parseInt(widthInput.value, 10);
        const newHeight = parseInt(heightInput.value, 10);

        if (changedDimension === 'width' && !isNaN(newWidth)) {
            // Si la largeur a changé et est valide, calcule et met à jour la hauteur en maintenant le rapport d'aspect
            heightInput.value = Math.round(newWidth / aspectRatio);
        } else if (changedDimension === 'height' && !isNaN(newHeight)) {
            // Si la hauteur a changé et est valide, calcule et met à jour la largeur en maintenant le rapport d'aspect
            widthInput.value = Math.round(newHeight * aspectRatio);
        }
    },

    /**
     * Agrandit une boîte de détection en appliquant un facteur d'échelle.
     * Assure que la nouvelle boîte reste à l'intérieur des limites du canvas.
     */
    enlargeBox: function(box, scale, canvas) {
        // Calcule les nouvelles dimensions de la boîte en appliquant le facteur d'échelle
        const newWidth = box.width * scale;
        const newHeight = box.height * scale * 0.8; // Ajuste la hauteur pour un meilleur ajustement

        // Calcule les nouvelles coordonnées x et y en centrant la boîte agrandie
        const newX = Math.max(0, box.x - (newWidth - box.width) / 2);
        const newY = Math.max(0, box.y - (newHeight - box.height) / 2);

        // Retourne la nouvelle boîte tout en s'assurant qu'elle ne dépasse pas les limites du canvas
        return {
            x: Math.min(newX, canvas.width - newWidth),
            y: Math.min(newY, canvas.height - newHeight),
            width: Math.min(newWidth, canvas.width - newX),
            height: Math.min(newHeight, canvas.height - newY)
        };
    },

    /**
     * Filtre les détections de visages en supprimant les doublons basés sur un seuil IoU (Intersection over Union).
     */
    filterDuplicateDetections: function(detections, iouThreshold) {
        const filteredDetections = [];

        /**
         * Calcule l'Intersection over Union (IoU) entre deux boîtes de détection.
         */
        function computeIoU(boxA, boxB) {
            // Calcule les coordonnées de la zone d'intersection
            const xA = Math.max(boxA.x, boxB.x);
            const yA = Math.max(boxA.y, boxB.y);
            const xB = Math.min(boxA.x + boxA.width, boxB.x + boxB.width);
            const yB = Math.min(boxA.y + boxA.height, boxB.y + boxB.height);

            // Calcule l'aire d'intersection
            const interArea = Math.max(0, xB - xA) * Math.max(0, yB - yA);
            
            // Calcule les aires des boîtes individuelles
            const boxAArea = boxA.width * boxA.height;
            const boxBArea = boxB.width * boxB.height;

            // Calcule l'IoU
            const iou = interArea / (boxAArea + boxBArea - interArea);
            return iou;
        }

        // Parcourt chaque détection pour vérifier les doublons
        detections.forEach((detection) => {
            let isDuplicate = false;

            // Compare la détection actuelle avec toutes les détections déjà filtrées
            for (let i = 0; i < filteredDetections.length; i++) {
                const filteredDetection = filteredDetections[i];

                // Si l'IoU dépasse le seuil, considère la détection comme un doublon
                if (computeIoU(detection.detection.box, filteredDetection.detection.box) > iouThreshold) {
                    isDuplicate = true;
                    break;
                }
            }

            // Si la détection n'est pas un doublon, l'ajoute au tableau filtré
            if (!isDuplicate) {
                filteredDetections.push(detection);
            }
        });

        return filteredDetections;
    }
};

const UI = {
    /**
     * Définit un bouton d'outil comme actif et met à jour le mode de dessin.
     */
    setActiveButton: function(buttonId, mode, sizeDisplay, sizeSliderLabelText) {
        // Liste de tous les boutons d'outils
        const buttons = ['cursorButton', 'brushButton', 'eraserButton', 'emojiButton'];
        
        // Parcourt chaque bouton et active celui correspondant à buttonId, désactive les autres
        buttons.forEach(id => {
            document.getElementById(id).classList.toggle('active', id === buttonId);
        });

        // Définit le mode de dessin dans le module Drawing
        Drawing.setDrawingMode(mode);

        // Affiche ou masque la zone du slider de taille en fonction de sizeDisplay
        sizeSliderZone.style.display = sizeDisplay;

        // Met à jour le texte du label du slider de taille si un texte est fourni
        if (sizeSliderLabelText) {
            sizeSliderLabel.innerText = sizeSliderLabelText;
        }
    },

    /**
     * Définit le mode de traitement (flou ou emoji) et met à jour l'interface utilisateur en conséquence.
     */
    setProcessingMode: function(mode) {
        // Met à jour la variable globale processingMode
        processingMode = mode;

        // Active le bouton correspondant au mode de traitement sélectionné et désactive l'autre
        document.getElementById('blurModeButton').classList.toggle('selected', mode === 'blur');
        document.getElementById('emojiModeButton').classList.toggle('selected', mode === 'emoji');

        // Génère l'image floutée ou emoji en fonction du mode de traitement sélectionné
        ImageProcessing.generateBlurredImage();
    },

    /**
     * Affiche ou masque le curseur personnalisé en fonction du mode de dessin et met à jour sa position.
     */
    toggleCustomCursorDisplay: function(event, mode) {
        // Affiche le curseur personnalisé si le mode de dessin n'est pas 'cursor', sinon le masque
        customCursor.style.display = drawingMode !== "cursor" ? 'block' : 'none';

        // Met à jour la position du curseur personnalisé en fonction de l'événement et du mode
        Drawing.updateCursorPosition(event, mode);
    },

    /**
     * Réinitialise tous les boutons d'outils en les désactivant et active le bouton 'cursor' par défaut.
     */
    resetToolButtons: function() {
        const buttons = ['cursorButton', 'brushButton', 'eraserButton', 'emojiButton'];
        
        // Parcourt chaque bouton et le désactive
        buttons.forEach(id => {
            document.getElementById(id).classList.remove('active');
        });

        // Active le bouton 'cursor' par défaut
        document.getElementById('cursorButton').classList.add('active');
    },

    /**
     * Affiche la superposition de chargement pour indiquer que le traitement est en cours.
     */
    showLoadingOverlay: function() {
        loadingOverlay.style.display = 'flex';
    },

    /**
     * Masque la superposition de chargement une fois le traitement terminé.
     */
    hideLoadingOverlay: function() {
        loadingOverlay.style.display = 'none';
    }
};

// =======================
// Initialisation de l'application
// =======================

document.addEventListener('DOMContentLoaded', () => {
    App.init(); // Appelle la fonction d'initialisation du module App une fois que le DOM est prêt
});