Accessible ici : https://faceprivacy.forge.apps.education.fr/app/

# Face Privacy

**Face Privacy** est une application web qui permet de flouter facilement les visages et les zones spécifiques sur les photos, le tout entièrement dans le navigateur. Elle garantit la confidentialité des données en traitant toutes les opérations localement sur l'appareil de l'utilisateur, sans téléchargement de photos sur des serveurs externes.
<p align="center">
  <img src="Screenshot1.png" alt="Capture d'écran" style="width: 45%; margin-right: 10px;">
  <img src="Screenshot2.png" alt="Capture d'écran" style="width: 45%;">
</p>
<p><small>Photo utilisée pour la capture : https://www.pexels.com/fr-fr/photo/mignon-ecole-amusement-enfants-8422142/</small></p>

## Table des matières

- [Fonctionnalités](#fonctionnalités)
- [Installation](#installation)
- [Utilisation](#utilisation)
- [Technologie Utilisée](#technologie-utilisée)
- [Droit à l'image et RGPD](#droit-à-limage-et-rgpd)
- [Confidentialité et Sécurité](#confidentialité-et-sécurité)
- [Contribution](#contribution)
- [Licence](#licence)

## Fonctionnalités

- **Floutage des visages** : Détecte automatiquement les visages présents sur une photo et applique un floutage pour protéger l'identité des personnes.
- **Floutage de zones personnalisées** : Permet de sélectionner manuellement des zones spécifiques de l'image à flouter.
- **Traitement entièrement local** : Toutes les opérations de détection et de floutage se font directement sur l'appareil de l'utilisateur, garantissant que les images ne quittent jamais le navigateur.
- **Exportation en différentes résolutions** : Les utilisateurs peuvent récupérer les photos modifiées dans la résolution de leur choix.
- **Interface intuitive** : Une interface utilisateur simple et facile à utiliser pour des résultats rapides.

## Installation

Aucune installation n'est nécessaire pour l'utilisateur. L'application **Face Privacy** fonctionne entièrement dans le navigateur. Il suffit de visiter le site web de l'application et de commencer à l'utiliser.

## Utilisation

1. **Télécharger une photo** : Cliquez sur "Choisir la photo" pour importer la photo que vous souhaitez modifier.
2. **Flouter les visages automatiquement** : Sélectionner les visages à flouter qui ont été détectés lors de l'import dans la zone "Visages à flouter". Il est possible de tous les sélectionner en une seule fois en utilisant le bouton dédié.
3. **Flouter des zones personnalisées** : Sélectionnez manuellement des zones sur l'image à flouter à l'aide des outils incluant un crayon et une gomme.
4. **Exporter l'image** : Choisissez la résolution souhaitée et cliquez sur "Télécharger la nouvelle photo" pour télécharger l'image modifiée.

## Technologie Utilisée

**Face Privacy** utilise l'API [face-api.js](https://github.com/justadudewhohacks/face-api.js), une bibliothèque JavaScript performante pour la reconnaissance faciale et le traitement d'images, directement dans le navigateur. Cette API permet de détecter les visages dans une image et d'appliquer un floutage localement, sans envoyer de données à des serveurs externes. Cela garantit une détection rapide et sécurisée des visages tout en préservant la confidentialité de l'utilisateur.


## Droit à l'image et RGPD

L'application **Face Privacy** respecte les droits à l'image de chacun et est conforme aux dispositions du Règlement Général sur la Protection des Données (RGPD) de l'Union Européenne (Règlement (UE) 2016/679). 

### Droit à l'image

Selon l'article 4 du RGPD, les données biométriques telles que les visages sont considérées comme des données à caractère personnel sensibles. Toute captation, traitement ou diffusion de ces données doit être effectuée avec le consentement explicite de la personne concernée ou de son représentant légal. En utilisant l'application **Face Privacy**, vous vous engagez à respecter ces obligations légales.

### Loi sur la captation et la diffusion de l’image d’un mineur

En France, la captation et la diffusion de l’image d’un mineur sont encadrées par des lois strictes pour protéger leur vie privée et leur intégrité. Conformément à l’article 9 du Code civil et à l’article 226-1 du Code pénal, il est interdit de diffuser des images de mineurs sans l’autorisation préalable des titulaires de l’autorité parentale. Cette interdiction est renforcée dans le cadre de l’éducation, où la capture et la publication d’images de mineurs dans les écoles ou lors d’événements scolaires nécessitent une autorisation écrite des parents ou des représentants légaux.

## Confidentialité et sécurité

**Face Privacy** est conçue pour garantir la confidentialité et la sécurité des images de ses utilisateurs :

- **Aucune photo n’est téléchargée sur un serveur** : Toutes les opérations se déroulent localement sur l'appareil de l'utilisateur.
- **Traitement local des données** : L’application utilise un réseau neuronal intégré directement sur l’appareil de l’utilisateur pour détecter et flouter les visages, sans transfert de données vers des serveurs externes.
- **Protection de la vie privée** : En utilisant **Face Privacy**, vous avez la certitude que vos photos restent privées et sécurisées, tout en respectant pleinement les réglementations relatives à la vie privée.

## Contribution

Les contributions sont les bienvenues ! Si vous souhaitez contribuer à l'amélioration de **Face Privacy**, veuillez suivre les étapes suivantes :

1. Fork ce dépôt.
2. Créez une branche avec vos modifications : `git checkout -b ma-nouvelle-fonctionnalite`.
3. Commitez vos changements : `git commit -m 'Ajout de ma nouvelle fonctionnalité'`.
4. Poussez votre branche : `git push origin ma-nouvelle-fonctionnalite`.
5. Soumettez une Pull Request.

## Licence

Ce projet est sous licence MIT.
