function affichage()
{
		largeur = window.innerWidth;
		valeurgauche=valeurgauche+((largeur-850)/4.24);
		valeurzoom=largeur/20;
		//valeurzoom=42;
		document.getElementById("conteneurpays").style.left = (valeurgauche/10)+"vw";
		document.getElementById("conteneurpays").style.top = (valeurtop/10)+"vw";
	    document.getElementById("conteneurpays").style.transform="scale("+(valeurzoom/100)+")";
}

function verificationnom() {
    let userInput = document.getElementById("cadrequestion").value;
    let formattedInput = formatCityName(userInput);

	if(droitrepondre=="on")
		{
			droitrepondre="off";
			switch(listevillestirage[tour])
				{
					case "Paris" : essaiparis++;break;
					case "Lyon" : essailyon++;break;
					case "Marseille" : essaimarseille++;break;
					case "Lille" : essailille++;break;
					case "Nantes" : essainantes++;break;
					case "Toulouse" : essaitoulouse++;break;
					case "Bordeaux" : essaibordeaux++;break;
					case "Nice" : essainice++;break;
					case "Montpellier" : essaimontpellier++;break;
					case "Strasbourg" : essaistrasbourg++;break;
				}
			if(listevillestirage[tour]==formattedInput)
			{
				affichereussite();

			}
			else
			{
				afficheerreur();	
			}	
		}
}

function formatCityName(name) {
    let trimmedAndNoSpaces = name.trim().replace(/\s/g, '');
    let formattedName = trimmedAndNoSpaces.charAt(0).toUpperCase() + trimmedAndNoSpaces.slice(1).toLowerCase();
    return formattedName;
}



function sonperdu()
{
	if(sonactif=="on")
	{
	player = document.getElementById("perdu");player.play();
	}	
}

function ding()
{
	if(sonactif=="on")
	{
	player = document.getElementById("ding");player.play();
	}
}

function plusdroite()
{
	valeurgauche=valeurgauche+10;
		document.getElementById("conteneurpays").style.left = (valeurgauche/10)+"vw";

}

function plusgauche()
{
	valeurgauche=valeurgauche-10;
		document.getElementById("conteneurpays").style.left = (valeurgauche/10)+"vw";

}

function plushaut()
{
	valeurtop=valeurtop-10;
		document.getElementById("conteneurpays").style.top = (valeurtop/10)+"vw";


}

function plusbas()
{
	valeurtop=valeurtop+10;
		document.getElementById("conteneurpays").style.top = (valeurtop/10)+"vw";


}
   
function dezoom() {
		valeurzoom=valeurzoom-5;
		valeurgauche=valeurgauche-(19.5*(1+(100/largeur)));
		valeurtop=valeurtop+(10.5*(1+(100/largeur)));
	    document.getElementById("conteneurpays").style.transform="scale("+(valeurzoom/100)+")";
		document.getElementById("conteneurpays").style.left = (valeurgauche/10)+"vw";
		document.getElementById("conteneurpays").style.top = (valeurtop/10)+"vw";

} 
   
function zoom()
{
		valeurzoom=valeurzoom+5;	
		valeurgauche=valeurgauche+(19.5*(1+(100/largeur)));
		valeurtop=valeurtop-(10.5*(1+(100/largeur))); 
	    document.getElementById("conteneurpays").style.transform="scale("+(valeurzoom/100)+")";
		document.getElementById("conteneurpays").style.left = (valeurgauche/10)+"vw";
		document.getElementById("conteneurpays").style.top = (valeurtop/10)+"vw";

}

function tirageglobal()
{
	if(nomrapide=="France-villes")
	{
		for(var i=1;i<11;i++)
		{
		tirage=Math.floor(Math.random()*listevilles.length);
		listevillestirage[i]=listevilles[tirage];
		remove=listevilles.splice(tirage,1);	
		}
	}
	
	affichequestion();
}

function affichequestion()
{
	switch(listevillestirage[tour])
	{
		case "Paris" : document.getElementById("paris").classList.add("trouvernomville");break;
		case "Lyon" : document.getElementById("lyon").classList.add("trouvernomville");break;
		case "Marseille" : document.getElementById("marseille").classList.add("trouvernomville");break;
		case "Lille" : document.getElementById("lille").classList.add("trouvernomville");break;
		case "Nantes" : document.getElementById("nantes").classList.add("trouvernomville");break;
		case "Toulouse" : document.getElementById("toulouse").classList.add("trouvernomville");break;
		case "Bordeaux" : document.getElementById("bordeaux").classList.add("trouvernomville");break;
		case "Nice" : document.getElementById("nice").classList.add("trouvernomville");break;
		case "Montpellier" : document.getElementById("montpellier").classList.add("trouvernomville");break;
		case "Strasbourg" : document.getElementById("strasbourg").classList.add("trouvernomville");break;
	}
	
}

function clicville01()
{
	if(droitrepondre=="on")
	{
		droitrepondre="off";
		if(listevillestirage[tour]=="Paris")
		{
			affichereussite();

		}
		else
		{
			afficheerreur();	
		}	
	}
}

function clicville02()
{
	if(droitrepondre=="on")
	{
		droitrepondre="off";
		if(listevillestirage[tour]=="Lyon")
		{
			affichereussite();

		}
		else
		{
			afficheerreur();	
		}	
	}
}

function clicville03()
{
	if(droitrepondre=="on")
	{
		droitrepondre="off";
		if(listevillestirage[tour]=="Marseille")
		{
			affichereussite();

		}
		else
		{
			afficheerreur();	
		}	
	}
}

function clicville04()
{
	if(droitrepondre=="on")
	{
		droitrepondre="off";
		if(listevillestirage[tour]=="Lille")
		{
			affichereussite();

		}
		else
		{
			afficheerreur();	
		}	
	}
}

function clicville05()
{
	if(droitrepondre=="on")
	{
		droitrepondre="off";
		if(listevillestirage[tour]=="Toulouse")
		{
			affichereussite();

		}
		else
		{
			afficheerreur();	
		}	
	}
}

function clicville06()
{
	if(droitrepondre=="on")
	{
		droitrepondre="off";
		if(listevillestirage[tour]=="Nice")
		{
			affichereussite();

		}
		else
		{
			afficheerreur();	
		}	
	}
}

function clicville07()
{
	if(droitrepondre=="on")
	{
		droitrepondre="off";
		if(listevillestirage[tour]=="Nantes")
		{
			affichereussite();

		}
		else
		{
			afficheerreur();	
		}	
	}
}

function clicville08()
{
	if(droitrepondre=="on")
	{
		droitrepondre="off";
		if(listevillestirage[tour]=="Montpellier")
		{
			affichereussite();

		}
		else
		{
			afficheerreur();	
		}	
	}
}

function clicville09()
{
	if(droitrepondre=="on")
	{
		droitrepondre="off";
		if(listevillestirage[tour]=="Strasbourg")
		{
			affichereussite();

		}
		else
		{
			afficheerreur();	
		}	
	}
}

function clicville10()
{
	if(droitrepondre=="on")
	{
		droitrepondre="off";
		if(listevillestirage[tour]=="Bordeaux")
		{
			affichereussite();

		}
		else
		{
			afficheerreur();	
		}	
	}
}


function affichageessais()
{
	if(listevillestirage[tour]=="Paris")
	{
		document.getElementById("paris-essais").innerHTML=essaiparis+" essai";
		if(essaiparis>1)
			{
			document.getElementById("paris-essais").innerHTML=essaiparis+" essais";
			}
		if(essaiparis>2)
			{
					
			}
	}
	if(listevillestirage[tour]=="Lyon")
	{
		document.getElementById("lyon-essais").innerHTML=essailyon+" essai";
		if(essailyon>1)
			{
			document.getElementById("lyon-essais").innerHTML=essailyon+" essais";
			}
		if(essailyon>2)
			{
			
			}
	}
	if(listevillestirage[tour]=="Marseille")
	{
		document.getElementById("marseille-essais").innerHTML=essaimarseille+" essai";
		if(essaimarseille>1)
			{
			document.getElementById("marseille-essais").innerHTML=essaimarseille+" essais";
			}
		if(essaimarseille>2)
			{
			
			}
	}

	if(listevillestirage[tour]=="Lille")
	{
		document.getElementById("lille-essais").innerHTML=essailille+" essai";
		if(essailille>1)
			{
			document.getElementById("lille-essais").innerHTML=essailille+" essais";
			}
		if(essailille>2)
			{
			
			}
	}
	if(listevillestirage[tour]=="Toulouse")
	{
		document.getElementById("toulouse-essais").innerHTML=essaitoulouse+" essai";
		if(essaitoulouse>1)
			{
			document.getElementById("toulouse-essais").innerHTML=essaitoulouse+" essais";
			}
		if(essaitoulouse>2)
			{
			
			}
	}
	if(listevillestirage[tour]=="Nice")
	{
		document.getElementById("nice-essais").innerHTML=essainice+" essai";
		if(essainice>1)
			{
			document.getElementById("nice-essais").innerHTML=essainice+" essais";
			}
		if(essainice>2)
			{
				
			}
	}
	

	if(listevillestirage[tour]=="Nantes")
	{
		document.getElementById("nantes-essais").innerHTML=essainantes+" essai";
		if(essainantes>1)
			{
			document.getElementById("nantes-essais").innerHTML=essainantes+" essais";
			}
		if(essainantes>2)
			{
			
			}
	}
	if(listevillestirage[tour]=="Montpellier")
	{
		document.getElementById("montpellier-essais").innerHTML=essaimontpellier+" essai";
		if(essaimontpellier>1)
			{
			document.getElementById("montpellier-essais").innerHTML=essaimontpellier+" essais";
			}
		if(essaimontpellier>2)
			{
					
			}
	}
	
	if(listevillestirage[tour]=="Strasbourg")
	{
		document.getElementById("strasbourg-essais").innerHTML=essaistrasbourg+" essai";
		if(essaistrasbourg>1)
			{
			document.getElementById("strasbourg-essais").innerHTML=essaistrasbourg+" essais";
			}
		if(essaistrasbourg>2)
			{
			
			}
	}


	if(listevillestirage[tour]=="Bordeaux")
		{
			document.getElementById("bordeaux-essais").innerHTML=essaibordeaux+" essai";
			if(essaibordeaux>1)
				{
				document.getElementById("bordeaux-essais").innerHTML=essaibordeaux+" essais";
				}
			if(essaibordeaux>2)
				{
				
				}
		}

	
	
}

function afficheerreur()
{

	document.getElementById("cadrequestion").classList.add("erreur");
	sonperdu();
	affichageessais();
	setTimeout(retourcadrequestion,1000);

	if(essaiparis>2 && listevillestirage[tour]=="Paris")
		{
		let indice = listevillestirage[tour].substring(0, essaiparis - 1);
            document.getElementById("cadrequestion").value = indice;
		}
	if(essailyon>2 && listevillestirage[tour]=="Lyon")
		{
		let indice = listevillestirage[tour].substring(0, essailyon - 1);
            document.getElementById("cadrequestion").value = indice;
		}
	if(essaimarseille>2 && listevillestirage[tour]=="Marseille")
		{
		let indice = listevillestirage[tour].substring(0, essaimarseille - 1);
            document.getElementById("cadrequestion").value = indice;
		}
	if(essailille>2 && listevillestirage[tour]=="Lille")
		{
		let indice = listevillestirage[tour].substring(0, essailille - 1);
            document.getElementById("cadrequestion").value = indice;
		}
	if(essaitoulouse>2 && listevillestirage[tour]=="Toulouse")
		{
		let indice = listevillestirage[tour].substring(0, essaitoulouse - 1);
            document.getElementById("cadrequestion").value = indice;
		}
	if(essainice>2 && listevillestirage[tour]=="Nice")
		{
		let indice = listevillestirage[tour].substring(0, essainice - 1);
            document.getElementById("cadrequestion").value = indice;
		}
	if(essainantes>2 && listevillestirage[tour]=="Nantes")
		{
		let indice = listevillestirage[tour].substring(0, essainantes - 1);
            document.getElementById("cadrequestion").value = indice;
		}
	if(essaimontpellier>2 && listevillestirage[tour]=="Montpellier")
		{
		let indice = listevillestirage[tour].substring(0, essaimontpellier - 1);
            document.getElementById("cadrequestion").value = indice;
		}
	if(essaistrasbourg>2 && listevillestirage[tour]=="Strasbourg")
		{
		let indice = listevillestirage[tour].substring(0, essaistrasbourg - 1);
            document.getElementById("cadrequestion").value = indice;
		}
	if(essaibordeaux>2 && listevillestirage[tour]=="Bordeaux")
		{
		let indice = listevillestirage[tour].substring(0, essaibordeaux - 1);
            document.getElementById("cadrequestion").value = indice;
		}
	
}

function affichereussite()
{
	document.getElementById("cadrequestion").classList.add("reussite");
	ding();
	affichageessais();
	switch(listevillestirage[tour])
		{
			case "Paris" : document.getElementById("path-paris").style.fill = "green";document.getElementById("paris").classList.remove("trouvernomville");document.getElementById("paris").offsetWidth;break;
			case "Lyon" : document.getElementById("path-lyon").style.fill = "green";document.getElementById("lyon").classList.remove("trouvernomville");document.getElementById("lyon").offsetWidth;break;
			case "Marseille" : document.getElementById("path-marseille").style.fill = "green";document.getElementById("marseille").classList.remove("trouvernomville");document.getElementById("marseille").offsetWidth;break;
			case "Lille" : document.getElementById("path-lille").style.fill = "green";document.getElementById("lille").classList.remove("trouvernomville");document.getElementById("lille").offsetWidth;break;
			case "Toulouse" : document.getElementById("path-toulouse").style.fill = "green";document.getElementById("toulouse").classList.remove("trouvernomville");document.getElementById("toulouse").offsetWidth;break;
			case "Nice" : document.getElementById("path-nice").style.fill = "green";document.getElementById("nice").classList.remove("trouvernomville");document.getElementById("nice").offsetWidth;break;
			case "Nantes" : document.getElementById("path-nantes").style.fill = "green";document.getElementById("nantes").classList.remove("trouvernomville");document.getElementById("nantes").offsetWidth;break;
			case "Montpellier" : document.getElementById("path-montpellier").style.fill = "green";document.getElementById("montpellier").classList.remove("trouvernomville");document.getElementById("montpellier").offsetWidth;break;
			case "Strasbourg" : document.getElementById("path-strasbourg").style.fill = "green";document.getElementById("strasbourg").classList.remove("trouvernomville");document.getElementById("strasbourg").offsetWidth;break;
			case "Bordeaux" : document.getElementById("path-bordeaux").style.fill = "green";document.getElementById("bordeaux").classList.remove("trouvernomville");document.getElementById("bordeaux").offsetWidth;break;
		}

	switch(listevillestirage[tour])
		{
			case "Paris" : document.getElementById("reponse-Paris").style.display="";document.getElementById("paris").style.width="0";break;
			case "Lyon" : document.getElementById("reponse-Lyon").style.display="";document.getElementById("lyon").style.width="0";break;
			case "Marseille" : document.getElementById("reponse-Marseille").style.display="";document.getElementById("marseille").style.width="0";break;
			case "Lille" : document.getElementById("reponse-Lille").style.display="";document.getElementById("lille").style.width="0";break;
			case "Toulouse" : document.getElementById("reponse-Toulouse").style.display="";document.getElementById("toulouse").style.width="0";break;
			case "Nice" : document.getElementById("reponse-Nice").style.display="";document.getElementById("nice").style.width="0";break;
			case "Nantes" : document.getElementById("reponse-Nantes").style.display="";document.getElementById("nantes").style.width="0";break;
			case "Montpellier" : document.getElementById("reponse-Montpellier").style.display="";document.getElementById("montpellier").style.width="0";break;
			case "Strasbourg" : document.getElementById("reponse-Strasbourg").style.display="";document.getElementById("strasbourg").style.width="0";break;
			case "Bordeaux" : document.getElementById("reponse-Bordeaux").style.display="";document.getElementById("bordeaux").style.width="0";break;
		}
		
	if(essaiparis==1)
		{
		document.getElementById("paristexte").style.color="#4caf50";document.getElementById("paristexte").style.fontWeight="bold";
		}
	if(essaiparis>1)
		{
		document.getElementById("path-paris").style.fill = "red";
		document.getElementById("paris").classList.remove("trouvernomville");document.getElementById("paris").offsetWidth;
		}
	if(essailyon==1)
		{
		document.getElementById("lyontexte").style.color="#4caf50";document.getElementById("lyontexte").style.fontWeight="bold";
		}
	if(essailyon>1)
		{
		document.getElementById("path-lyon").style.fill = "red";
		document.getElementById("lyon").classList.remove("trouvernomville");document.getElementById("lyon").offsetWidth;
		}
	if(essaimarseille==1)
		{
		document.getElementById("marseilletexte").style.color="#4caf50";document.getElementById("marseilletexte").style.fontWeight="bold";
		}
	if(essaimarseille>1)
		{
		document.getElementById("path-marseille").style.fill = "red";
		document.getElementById("marseille").classList.remove("trouvernomville");document.getElementById("marseille").offsetWidth;
		}
	if(essailille==1)
		{
		document.getElementById("lilletexte").style.color="#4caf50";document.getElementById("lilletexte").style.fontWeight="bold";
		}
	if(essailille>1)
		{
		document.getElementById("path-lille").style.fill = "red";
		document.getElementById("lille").classList.remove("trouvernomville");document.getElementById("lille").offsetWidth;
		}
	if(essaitoulouse==1)
		{
		document.getElementById("toulousetexte").style.color="#4caf50";document.getElementById("toulousetexte").style.fontWeight="bold";
		}
	if(essaitoulouse>1)
		{
		document.getElementById("path-toulouse").style.fill = "red";
		document.getElementById("toulouse").classList.remove("trouvernomville");document.getElementById("toulouse").offsetWidth;
		}
	if(essainice==1)
		{
		document.getElementById("nicetexte").style.color="#4caf50";document.getElementById("nicetexte").style.fontWeight="bold";
		}
	if(essainice>1)
		{
		document.getElementById("path-nice").style.fill = "red";
		document.getElementById("nice").classList.remove("trouvernomville");document.getElementById("nice").offsetWidth;
		}
	if(essainantes==1)
		{
		document.getElementById("nantestexte").style.color="#4caf50";document.getElementById("nantestexte").style.fontWeight="bold";
		}
	if(essainantes>1)
		{
		document.getElementById("path-nantes").style.fill = "red";
		document.getElementById("nantes").classList.remove("trouvernomville");document.getElementById("nantes").offsetWidth;
		}
	if(essaimontpellier==1)
		{
		document.getElementById("montpelliertexte").style.color="#4caf50";document.getElementById("montpelliertexte").style.fontWeight="bold";
		}
	if(essaimontpellier>1)
		{
		document.getElementById("path-montpellier").style.fill = "red";
		document.getElementById("montpellier").classList.remove("trouvernomville");document.getElementById("montpellier").offsetWidth;
		}
	if(essaistrasbourg==1)
		{
		document.getElementById("strasbourgtexte").style.color="#4caf50";document.getElementById("strasbourgtexte").style.fontWeight="bold";
		}
	if(essaistrasbourg>1)
		{
		document.getElementById("path-strasbourg").style.fill = "red";
		document.getElementById("strasbourg").classList.remove("trouvernomville");document.getElementById("strasbourg").offsetWidth;
		}
	if(essaibordeaux==1)
		{
		document.getElementById("bordeauxtexte").style.color="#4caf50";document.getElementById("bordeauxtexte").style.fontWeight="bold";
		}
	if(essaibordeaux>1)
		{
		document.getElementById("path-bordeaux").style.fill = "red";
		document.getElementById("bordeaux").classList.remove("trouvernomville");document.getElementById("bordeaux").offsetWidth;
		}

	setTimeout(retourcadrequestionbis,1000);
}

function retourcadrequestionbis()
{
	document.getElementById("cadrequestion").value = '';
	document.getElementById("cadrequestion").classList.remove("reussite");
	document.getElementById("cadrequestion").offsetWidth;
	droitrepondre="on";
	tour++;
	
	if(nomrapide=="France-villes")
	{

			if(tour<11)
			{
				affichequestion();
			}
			else
				{
					document.getElementById("consigne").style.display="none";
					document.getElementById("cadrequestion").style.display="none";
					document.getElementById("verifier").style.display="none";
					document.getElementById("navigation").style.display="none";
					document.getElementById("consigne2").style.display="";
					document.getElementById("bouton-recommencer").style.display="";
					document.getElementById("bilan").style.display="";
					
			if(essaiparis==1)
				{
				document.getElementById("paristexte").style.color="#4caf50";document.getElementById("paristexte").style.fontWeight="bold";
				points++;
				}
			if(essailyon==1)
				{
				document.getElementById("lyontexte").style.color="#4caf50";document.getElementById("lyontexte").style.fontWeight="bold";
				points++;
				}
			if(essaimarseille==1)
				{
				document.getElementById("marseilletexte").style.color="#4caf50";document.getElementById("marseilletexte").style.fontWeight="bold";
				points++;
				}
			if(essailille==1)
				{
				document.getElementById("lilletexte").style.color="#4caf50";document.getElementById("lilletexte").style.fontWeight="bold";
				points++;
				}
			if(essaitoulouse==1)
				{
				document.getElementById("toulousetexte").style.color="#4caf50";document.getElementById("toulousetexte").style.fontWeight="bold";
				points++;
				}
			if(essainice==1)
				{
				document.getElementById("nicetexte").style.color="#4caf50";document.getElementById("nicetexte").style.fontWeight="bold";
				points++;
				}
			if(essainantes==1)
				{
				document.getElementById("nantestexte").style.color="#4caf50";document.getElementById("nantestexte").style.fontWeight="bold";
				points++;
				}
			if(essaimontpellier==1)
				{
				document.getElementById("montpelliertexte").style.color="#4caf50";document.getElementById("montpelliertexte").style.fontWeight="bold";
				points++;
				}
			if(essaistrasbourg==1)
				{
				document.getElementById("strasbourgtexte").style.color="#4caf50";document.getElementById("strasbourgtexte").style.fontWeight="bold";
				points++;
				}
			if(essaibordeaux==1)
				{
				document.getElementById("bordeauxtexte").style.color="#4caf50";document.getElementById("bordeauxtexte").style.fontWeight="bold";
				points++;
				}


		
				pourcentagereussite=Math.round(points/10*100);			
				document.getElementById("score").innerHTML=pourcentagereussite;		
				/*
				document.location.href="carte-France-regions.html?essaiauvergne="+essaiauvergne+"&essaibourgogne="+essaibourgogne+"&essaibretagne="+essaibretagne+"&essaicentre="+essaicentre+"&essaicorse="+essaicorse+
				"&essaigrandest="+essaigrandest+"&essaihauts="+essaihauts+"&essaiidf="+essaiidf+"&essainormandie="+essainormandie+"&essaiaquitaine="+essaiaquitaine+"&essaioccitanie="+essaioccitanie+"&essailoire="+essailoire+"&essaipaca="+essaipaca+	
				"&valeurzoom="+valeurzoom+"&valeurtop="+valeurtop+"&valeurgauche="+valeurgauche;
*/
				}
	}
}

function retourcadrequestion()
{
	document.getElementById("cadrequestion").classList.remove("erreur");
	document.getElementById("cadrequestion").offsetWidth;
	droitrepondre="on";
}

function zero()
{
	location.reload();
}