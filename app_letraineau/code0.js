gdjs.introCode = {};
gdjs.introCode.GDintroObjects1= [];
gdjs.introCode.GDintroObjects2= [];
gdjs.introCode.GDintroObjects3= [];
gdjs.introCode.GDaudioObjects1= [];
gdjs.introCode.GDaudioObjects2= [];
gdjs.introCode.GDaudioObjects3= [];
gdjs.introCode.GDpersonnageObjects1= [];
gdjs.introCode.GDpersonnageObjects2= [];
gdjs.introCode.GDpersonnageObjects3= [];
gdjs.introCode.GDbouton_9595commencerObjects1= [];
gdjs.introCode.GDbouton_9595commencerObjects2= [];
gdjs.introCode.GDbouton_9595commencerObjects3= [];


gdjs.introCode.eventsList0 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/traineau-passe.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/consigne.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/vrai.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/applaudissements.mp3");
}}

}


{


let isConditionTrue_0 = false;
{
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/audio_vide.mp3", 1, false, 100, 1);
}{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/audio_vide.mp3", 2, false, 100, 1);
}{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/audio_vide.mp3", 3, false, 100, 1);
}}

}


};gdjs.introCode.mapOfGDgdjs_9546introCode_9546GDbouton_95959595commencerObjects1Objects = Hashtable.newFrom({"bouton_commencer": gdjs.introCode.GDbouton_9595commencerObjects1});
gdjs.introCode.eventsList1 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("bouton_commencer"), gdjs.introCode.GDbouton_9595commencerObjects1);
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "chrono");
}{for(var i = 0, len = gdjs.introCode.GDbouton_9595commencerObjects1.length ;i < len;++i) {
    gdjs.introCode.GDbouton_9595commencerObjects1[i].hide();
}
}
{ //Subevents
gdjs.introCode.eventsList0(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "chrono") >= 5;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(9649500);
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("audio"), gdjs.introCode.GDaudioObjects1);
gdjs.copyArray(runtimeScene.getObjects("bouton_commencer"), gdjs.introCode.GDbouton_9595commencerObjects1);
{for(var i = 0, len = gdjs.introCode.GDbouton_9595commencerObjects1.length ;i < len;++i) {
    gdjs.introCode.GDbouton_9595commencerObjects1[i].hide(false);
}
}{for(var i = 0, len = gdjs.introCode.GDaudioObjects1.length ;i < len;++i) {
    gdjs.introCode.GDaudioObjects1[i].hide();
}
}{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/vrai.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_commencer"), gdjs.introCode.GDbouton_9595commencerObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.introCode.mapOfGDgdjs_9546introCode_9546GDbouton_95959595commencerObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.introCode.GDbouton_9595commencerObjects1.length;i<l;++i) {
    if ( gdjs.introCode.GDbouton_9595commencerObjects1[i].isVisible() ) {
        isConditionTrue_0 = true;
        gdjs.introCode.GDbouton_9595commencerObjects1[k] = gdjs.introCode.GDbouton_9595commencerObjects1[i];
        ++k;
    }
}
gdjs.introCode.GDbouton_9595commencerObjects1.length = k;
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "menu", false);
}}

}


};

gdjs.introCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.introCode.GDintroObjects1.length = 0;
gdjs.introCode.GDintroObjects2.length = 0;
gdjs.introCode.GDintroObjects3.length = 0;
gdjs.introCode.GDaudioObjects1.length = 0;
gdjs.introCode.GDaudioObjects2.length = 0;
gdjs.introCode.GDaudioObjects3.length = 0;
gdjs.introCode.GDpersonnageObjects1.length = 0;
gdjs.introCode.GDpersonnageObjects2.length = 0;
gdjs.introCode.GDpersonnageObjects3.length = 0;
gdjs.introCode.GDbouton_9595commencerObjects1.length = 0;
gdjs.introCode.GDbouton_9595commencerObjects2.length = 0;
gdjs.introCode.GDbouton_9595commencerObjects3.length = 0;

gdjs.introCode.eventsList1(runtimeScene);

return;

}

gdjs['introCode'] = gdjs.introCode;
