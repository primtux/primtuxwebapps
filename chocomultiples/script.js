// Octobre 2024 Arnaud Champollion //
// Chocomultiples //
// Licence GNU GPL //

// Ce préfixe sera utilisé pour le stockage des paramètres dans le cache

let prefixeAppli = 'chocomultiples';
const divMilieu = document.getElementById('milieu');
const divContenu = document.getElementById('contenu');

const divMultiplication = document.getElementById('multiplication');
const divRepresentation = document.getElementById('representation');
const divDessus = document.getElementById('dessus');
const divCote = document.getElementById('cote');
const divFlou = document.getElementById('flou');
const divEnsemble = document.getElementById('ensemble');
const divEnsembleCouleurs = document.getElementById('ensemble-couleurs');
const divColonnes = document.getElementById('colonnes');

const divPapier = document.getElementById('conteneur-papier');
const divSeparations = document.getElementById('separations');
const divPanneau = document.getElementById('panneau');
const darkbox = document.getElementById('darkbox');


const divXX = document.getElementById('xx');
const divXY= document.getElementById('xy');
const divYX = document.getElementById('yx');
const divYY = document.getElementById('yy');


const divApropos = document.getElementById('apropos');


const spanMultipleA = document.getElementById('multiple-a');
const spanMultipleB = document.getElementById('multiple-b');

const inputA = document.getElementById('input-multiple-a');
const inputB = document.getElementById('input-multiple-b');
const inputAmin = document.getElementById('input-multiple-a-min');
const inputAmax = document.getElementById('input-multiple-a-max');
const inputBmin = document.getElementById('input-multiple-b-min');
const inputBmax = document.getElementById('input-multiple-b-max');
const inputReversible = document.getElementById('input-reversible');
const inputFlou = document.getElementById('input-flou');
const inputAxeX = document.getElementById('input-axe-x');
const inputAxeY = document.getElementById('input-axe-y');
const inputDetailsCarreaux = document.getElementById('input-details');
const inputPapier = document.getElementById('input-papier');
const inputChangeFacteurs = document.getElementById('input-change-facteurs');

const checkboxMultipleA = document.getElementById('checkbox-multiple-a');
const checkboxMultipleB = document.getElementById('checkbox-multiple-b');
const checkboxResultat = document.getElementById('checkbox-resultat');

const checkboxSeparations= document.getElementById('checkbox-separations');
const checkboxAlternees= document.getElementById('checkbox-alternees');


const rangeTailleFlou = document.getElementById('range-taille-flou');
const rangeIntensiteFlou = document.getElementById('range-intensite-flou');

const spanResultat = document.getElementById('resultat');

const body = document.body;
const root = document.documentElement;

// Est-on dans Openboard ? ////
let openboard = Boolean(window.widget || window.sankore);
console.log('Openboard ? '+openboard);
if (openboard){
    document.body.classList.add('openboard');
}
///////////////////////////////

// Initialisation des variables globales

let tailleCarreaux = 'fixe';
let largeurCarreaux = 80;
let multipleA = 5;
let multipleB = 3;
let multipleAmin = 1;
let multipleAmax = 10;
let multipleBmin = 1;
let multipleBmax = 10;
let commutatif = true;
let voileFlou = false; 
let tailleFlou = 90;
let intensiteFlou = 10;
let detailsCarreaux = true;
let afficherMultipleA = true;
let afficherMultipleB = true;
let afficherResultat = true;
let couleursAxeX = false;
let couleursAxeY = false;
let estEnPleinEcranF11 = false;
let separations = false;
let alternees = false;
let emballagePapier = false;
let changeFacteurs = false;
let selection = 'aucune';

// Dimensions

let largeurFenetre = window.innerWidth;
let hauteurFenetre = window.innerHeight;

///////////////////// Lancement du programme ///////////////////
async function executeFunctions() {
    await checkReglages();
    await appliqueReglages();
    creeCarreaux('chocolat',multipleA,multipleB);    
}
executeFunctions();
//////////////////////////////////////////////////////////////


async function checkReglages() {
    console.log('--- Lecture URL et stockage local');
    let url = window.location.search;
    let urlParams = new URLSearchParams(url);
    let valeurARecuperer;


    let primtux = urlParams.get('primtuxmenu');
    if (primtux){
        body.classList.add('primtux');
    }

    valeurARecuperer = urlParams.get('taille-carreaux') || await litDepuisStockage('taille-carreaux');
    if (valeurARecuperer) {
        tailleCarreaux = valeurARecuperer;
    }

    valeurARecuperer = urlParams.get('flou') || await litDepuisStockage('flou');
    if (valeurARecuperer) {
        voileFlou = valeurARecuperer==='true';
    }

    valeurARecuperer = urlParams.get('taille-flou') || await litDepuisStockage('taille-flou');
    if (valeurARecuperer) {
        tailleFlou = parseInt(valeurARecuperer);
    }

    valeurARecuperer = urlParams.get('intensite-flou') || await litDepuisStockage('intensite-flou');
    if (valeurARecuperer) {
        intensiteFlou = parseInt(valeurARecuperer);
    }

    valeurARecuperer = urlParams.get('facteur-a') || await litDepuisStockage('facteur-a');
    if (valeurARecuperer && !isNaN(parseInt(valeurARecuperer))) {
        multipleA = parseInt(valeurARecuperer);
    }

    valeurARecuperer = urlParams.get('facteur-b') || await litDepuisStockage('facteur-b');
    if (valeurARecuperer && !isNaN(parseInt(valeurARecuperer))) {
        multipleB = parseInt(valeurARecuperer);
    }

    valeurARecuperer = urlParams.get('mutliple-a-min') || await litDepuisStockage('multiple-a-min');
    if (valeurARecuperer && !isNaN(parseInt(valeurARecuperer))) {
        multipleAmin = parseInt(valeurARecuperer);
    }

    valeurARecuperer = urlParams.get('mutliple-a-max') || await litDepuisStockage('multiple-a-max');
    if (valeurARecuperer && !isNaN(parseInt(valeurARecuperer))) {
        multipleAmax = parseInt(valeurARecuperer)
    }

    valeurARecuperer = urlParams.get('mutliple-b-min') || await litDepuisStockage('multiple-b-min');
    if (valeurARecuperer && !isNaN(parseInt(valeurARecuperer))) {
        multipleBmin = parseInt(valeurARecuperer);
    }

    valeurARecuperer = urlParams.get('mutliple-b-max') || await litDepuisStockage('multiple-b-max');
    if (valeurARecuperer && !isNaN(parseInt(valeurARecuperer))) {
        multipleBmax = parseInt(valeurARecuperer);
    }

    valeurARecuperer = urlParams.get('afficher-facteur-a') || await litDepuisStockage('afficher-facteur-a');
    if (valeurARecuperer) {
        afficherMultipleA = valeurARecuperer!='false';
    }

    valeurARecuperer = urlParams.get('afficher-facteur-b') || await litDepuisStockage('afficher-facteur-b');
    if (valeurARecuperer) {
        afficherMultipleB = valeurARecuperer!='false';
    }

    valeurARecuperer = urlParams.get('afficher-resultat') || await litDepuisStockage('afficher-resultat');
    if (valeurARecuperer) {
        afficherResultat = valeurARecuperer!='false';
    }

    valeurARecuperer = urlParams.get('commutatif') || await litDepuisStockage('commutatif');
    if (valeurARecuperer) {
        commutatif = valeurARecuperer!='false';
    }

    valeurARecuperer = urlParams.get('details') || await litDepuisStockage('details');
    if (valeurARecuperer) {
        detailsCarreaux = valeurARecuperer!='false';
    }

    valeurARecuperer = urlParams.get('couleurs-x') || await litDepuisStockage('couleurs-x');
    if (valeurARecuperer) {
        couleursAxeX = valeurARecuperer!='false';
    }

    valeurARecuperer = urlParams.get('couleurs-y') || await litDepuisStockage('couleurs-y');
    if (valeurARecuperer) {
        couleursAxeY = valeurARecuperer!='false';
    }

    valeurARecuperer = urlParams.get('separations') || await litDepuisStockage('separations');
    if (valeurARecuperer) {
        separations = valeurARecuperer!='false';
    }

    valeurARecuperer = urlParams.get('couleurs-alternees') || await litDepuisStockage('couleurs-alternees');
    if (valeurARecuperer) {
        alternees = valeurARecuperer!='false';
    }

    valeurARecuperer = urlParams.get('papier') || await litDepuisStockage('papier');
    if (valeurARecuperer) {
        emballagePapier = valeurARecuperer!='false';
    }

    valeurARecuperer = urlParams.get('changer-affichage-facteurs') || await litDepuisStockage('changer-affichage-facteurs');
    if (valeurARecuperer) {
        changeFacteurs = valeurARecuperer!='false';
    }

    valeurARecuperer = urlParams.get('selection') || await litDepuisStockage('selection');
    if (valeurARecuperer) {
        selection = valeurARecuperer;
    }
}

function appliqueReglages() {

    console.log('---- Application des réglages ----'); 
    
    inputA.value = multipleA;
    inputB.value = multipleB;
    inputAmin.value = multipleAmin;
    inputAmax.value = multipleAmax;
    inputBmin.value = multipleBmin;
    inputBmax.value = multipleBmax;
    inputReversible.checked = commutatif;    
    inputDetailsCarreaux.checked = detailsCarreaux;
    inputChangeFacteurs.checked = changeFacteurs;
    inputPapier.checked = emballagePapier;
    inputFlou.checked = voileFlou;
    rangeTailleFlou.value = tailleFlou;
    rangeIntensiteFlou.value = intensiteFlou;
    checkboxMultipleA.checked = afficherMultipleA;
    checkboxMultipleB.checked = afficherMultipleB;
    checkboxResultat.checked = afficherResultat;
    inputAxeX.checked = couleursAxeX;
    inputAxeY.checked = couleursAxeY;
    checkboxAlternees.checked = alternees;
    checkboxSeparations.checked = separations;    
    flou();
    details();
    papier();
    changeSeparations();
    couleurs();
    afficher();
    modeSelection(selection)       

}

// Fonction pour définir le bouton radio sélectionné au chargement de la page
function setTailleCarreaux() {
    const radioButtons = document.querySelectorAll('input[name="taille"]');
    radioButtons.forEach(button => {
        if (button.value === tailleCarreaux) {
            button.checked = true;  // Coche le bouton radio correspondant
        }
    });
}

// Appel à la fonction au chargement de la page pour mettre à jour l'état initial du bouton radio
window.onload = setTailleCarreaux;




function creeCarreaux(classe,a=false,b=false) {
      
    console.log('---création des carreaux')


    if (a && b) {

        multipleA = a;
        multipleB = b;

        console.log('---valeurs demandées : ',a,b)

    } else {

        console.log('---récupération des valeurs')

        // Récupération des valeurs
        let inputAValue = inputA.value.replace(/\D/g, ''); // On supprime tout sauf les chiffres
        let inputBValue = inputB.value.replace(/\D/g, ''); // On supprime tout sauf les chiffres
        multipleA = parseInt(inputAValue);
        multipleB = parseInt(inputBValue);

        console.log('Valeurs récupérées',multipleA,multipleB);


        // Limiter les valeurs à un maximum de 100
        if (multipleA > 100) multipleA = 100;
        if (multipleB > 100) multipleB = 100;

        stocke('facteur-a',multipleA);
        majUrl('facteur-a',multipleA);
        stocke('facteur-b',multipleB);
        majUrl('facteur-b',multipleB);

        // Mise à jour les inputs avec les valeurs corrigées
        inputA.value = multipleA;
        inputB.value = multipleB;

        if (inputA.value != ''){
            checkboxMultipleA.checked = afficherMultipleA = true;
            spanMultipleA.classList.remove('transparent');
        }
        if (inputB.value != ''){
            checkboxMultipleB.checked = afficherMultipleB = true;
            spanMultipleB.classList.remove('transparent');
        }
    }

    let resultat = multipleA * multipleB;

    if (resultat < 100000 && !isNaN(multipleA) && !isNaN(multipleB)) {

        spanMultipleA.innerHTML = multipleA;
        spanMultipleB.innerHTML = multipleB;

        spanResultat.innerHTML = resultat;

        console.log('Création des carreaux',multipleA,'X',multipleB);

        // Remise à zéro
        const rangees = divRepresentation.querySelectorAll('.rangee');
        rangees.forEach(rangee => {
            rangee.remove();
        })

        document.getElementById('separations').innerHTML='';
                
        // Affectation de la classe CSS au conteneur de la représentation
        divMultiplication.classList.add(classe);

        let coteCarreau;
        
        // Calcul des dimensions
        let largeurDisponible = divMultiplication.offsetWidth;
        let hauteurDisponible = divMultiplication.offsetHeight;
        let largeurCarreauPossible = Math.floor(largeurDisponible / multipleA);
        let hauteurCarreauPossible = Math.floor(hauteurDisponible / multipleB);
        let coteCarreauMax = Math.min(largeurCarreauPossible, hauteurCarreauPossible);


        if (tailleCarreaux === 'fixe' && largeurCarreaux < coteCarreauMax) {
            coteCarreau = largeurCarreaux;
        } else {
            coteCarreau = coteCarreauMax;
        }

        console.log('coteCarreau',coteCarreau);
        if (coteCarreau < 10) {
            console.log('Simplification des carreaux');
            body.classList.add('simple');
        } else {
            body.classList.remove('simple');
        }

        let largeurOmbre = `${Math.floor(coteCarreau / 10)}px`;
        let largeurOmbreNegative = `${-Math.floor(coteCarreau / 10)}px`;
        let borderRadiusChocolat = `${Math.floor(coteCarreau / 10)}px`;

        root.style.setProperty('--largeur-ombre', largeurOmbre);
        root.style.setProperty('--largeur-ombre-negative', largeurOmbreNegative);
        root.style.setProperty('--border-radius-chocolat', borderRadiusChocolat);

        let nombreDeDizainesX = Math.floor(multipleA/10);
        let nombreDeDizainesY = Math.floor(multipleB/10);

        let nombrePairDeDizainesX = nombreDeDizainesX % 2 === 0;
        let nombrePairDeDizainesY = nombreDeDizainesY % 2 ===0;

        if (nombrePairDeDizainesX) {body.classList.add('nombrePairDeDizainesX');}
        else {body.classList.remove('nombrePairDeDizainesX');} 
        
        if (nombrePairDeDizainesY) {body.classList.add('nombrePairDeDizainesY');}
        else {body.classList.remove('nombrePairDeDizainesY');} 
        

        document.documentElement.style.setProperty('--largeur-carreau', `${coteCarreau}px`);

        // Largeur première colonne XX YX   
        divXX.style.width = divYX.style.width = `${100*10*nombreDeDizainesX/multipleA}%`;
        // Hauteur première ligne XX XY
        document.getElementById('rangee1').style.height = `${100*10*nombreDeDizainesY/multipleB}%`;

       
        // Création des rangées
        for (let i = 0; i < multipleB; i++) {

            let rangee = document.createElement('div');
            rangee.classList.add('rangee');
            if (i===0){rangee.classList.add('premiere');}
            if (i===multipleB-1){rangee.classList.add('derniere');}

            rangee.addEventListener('click', function() {
                rangee.classList.toggle('visible');
                const rangees = divMultiplication.querySelectorAll('.rangee');
                rangees.forEach(rangee => {
                    rangee.classList.add('invisible')
                });
            });

            divRepresentation.appendChild(rangee);

            let dizaineY = false;
            if (multipleB - i - 1 >= multipleB % 10) {
                dizaineY = true;
            }
            
            // Séparations horizontales
            if (i!=0 && i%10===0) {
                let separation = document.createElement('div');
                separation.classList.add('separation','horizontale');
                separation.style.top = 100*(i / multipleB) + '%';
                document.getElementById('separations').appendChild(separation);
            }

            // Création des carreaux    
            for (let j = 0; j < multipleA; j++) {

                // Séparations verticales
                if (j!=0 && j%10===0) {
                    let separation = document.createElement('div');
                    separation.classList.add('separation','verticale');
                    separation.style.left = 100*(j / multipleA) + '%';
                    document.getElementById('separations').appendChild(separation);
                }
        
                let carreau = document.createElement('div');
                carreau.classList.add('carreau');
                carreau.position = j;

                let dizaineX = multipleA - j - 1 >= multipleA % 10;
                

                if (dizaineX) {
                    // Dizaines axe horizontal
                    if (dizaineY){
                        carreau.classList.add('dizaineX');
                        if ( Math.floor(j/10) % 2 === 0 ) {
                            // Impaires
                            carreau.classList.add('impairX');
                            // Paires
                        } else {
                            carreau.classList.add('pairX');
                        }
                    } else {
                        carreau.classList.add('dizaineX');
                        if ( Math.floor(j/10) % 2 == !nombrePairDeDizainesY ) {
                            // Impaires
                            carreau.classList.add('impairX');
                            // Paires
                        } else {
                            carreau.classList.add('pairX');
                        }
                    }


                } else {
                    // Unités axe horizontal
                    carreau.classList.add('uniteX');
                    console.log(Math.floor(i/10) % 2);
                    if ( Math.floor(i/10) % 2 == !nombrePairDeDizainesX) {
                        // Impaires verticales
                        carreau.classList.add('impairY');
                    } else {
                        // Paires verticales
                        carreau.classList.add('pairY');
                    }

                }

  
                if (dizaineY) {
                    // Dizaines axe vertical
                    carreau.classList.add('dizaineY');
                    if (dizaineX) {
                        if ( Math.floor(i/10) % 2 === 0) {
                            carreau.classList.add('impairY');
                        } else {
                            // Paires    
                            carreau.classList.add('pairY');
                        }
                    } else {
                        // Impaires
                        if ( Math.floor(i/10) % 2 == !nombrePairDeDizainesX) {
                            carreau.classList.add('impairY');
                        } else {
                            // Paires    
                            carreau.classList.add('pairY');
                        }
                    }

                } else {
                    // Unités axe vertical
                    carreau.classList.add('uniteY');
                    if ( Math.floor(j/10) % 2 == !nombrePairDeDizainesY ) {
                        // Impaires horizontales
                        carreau.classList.add('impairX');
                    } else {
                        // Paires horizontales
                        carreau.classList.add('pairX');
                    }
                }

 

                rangee.appendChild(carreau); 

            }
        }

        // Création des colonnes
        divColonnes.innerHTML='';
        const largeurColonne = 100 / multipleA;
        for (let i = 0; i < multipleA; i++) {
            let nouvelleColonne = document.createElement('div');
            nouvelleColonne.classList.add('colonne');
            nouvelleColonne.position = i;
            nouvelleColonne.style.width = largeurColonne + '%';

            nouvelleColonne.addEventListener('click', function() {

                const carreaux = divMultiplication.querySelectorAll('.carreau');
                carreaux.forEach(carreau => {
                    carreau.classList.add('invisible');
                    if (carreau.position === nouvelleColonne.position) {
                        carreau.classList.toggle('visible');
                    }
                });

            });

            divColonnes.appendChild(nouvelleColonne);
        }

    }
}

function redim() {
    console.log('redim');

    let coteCarreau;

    // Calcul des dimensions
    let largeurDisponible = divMultiplication.offsetWidth;
    let hauteurDisponible = divMultiplication.offsetHeight;
    let largeurCarreauPossible = Math.floor(largeurDisponible / multipleA);
    let hauteurCarreauPossible = Math.floor(hauteurDisponible / multipleB);
    let coteCarreauMax = Math.min(largeurCarreauPossible, hauteurCarreauPossible);

    if (tailleCarreaux === 'fixe' && largeurCarreaux < coteCarreauMax) {
        coteCarreau = largeurCarreaux;
    } else {
        coteCarreau = coteCarreauMax;
    }

    console.log('coteCarreau',coteCarreau);
    if (coteCarreau < 10) {
        console.log('Simplification des carreaux');
        body.classList.add('simple');
    } else {
        body.classList.remove('simple');
    }

    document.documentElement.style.setProperty('--largeur-carreau', `${coteCarreau}px`);

    let largeurOmbre = `${Math.floor(coteCarreau / 10)}px`;
    let largeurOmbreNegative = `${-Math.floor(coteCarreau / 10)}px`;
    let borderRadiusChocolat = `${Math.floor(coteCarreau / 10)}px`;

    root.style.setProperty('--largeur-ombre', largeurOmbre);
    root.style.setProperty('--largeur-ombre-negative', largeurOmbreNegative);
    root.style.setProperty('--border-radius-chocolat', borderRadiusChocolat);

    if (estEnPleinEcran() || estEnPleinEcranF11) {
        body.classList.add('pleinecran');
    } else {
        body.classList.remove('pleinecran');
    }
}

window.addEventListener('resize', redim);

window.addEventListener('load', () => {
    if (estEnPleinEcran() || estEnPleinEcranF11) {
        body.classList.add('pleinecran');
    } else {
        body.classList.remove('pleinecran');
    }
    redim(); // Ajuster les carreaux dès le chargement
});

function estEnPleinEcran() {
    return (window.innerWidth === screen.width && window.innerHeight === screen.height);
}

function passerEnPleinEcran() {
    if (!document.fullscreenElement && !estEnPleinEcranF11) {
        let elem = document.documentElement; // Cible l'élément racine (html)

        if (elem.requestFullscreen) {
            elem.requestFullscreen(); // Pour les navigateurs modernes
        } else if (elem.webkitRequestFullscreen) { // Safari
            elem.webkitRequestFullscreen();
        } else if (elem.msRequestFullscreen) { // Internet Explorer/Edge
            elem.msRequestFullscreen();
        }
    } else {
        if (document.exitFullscreen) {
            document.exitFullscreen(); // Pour les navigateurs modernes
        } else if (document.webkitExitFullscreen) { // Safari
            document.webkitExitFullscreen();
        } else if (document.msExitFullscreen) { // Internet Explorer/Edge
            document.msExitFullscreen();
        }
        estEnPleinEcranF11 = false;
    }
}

// Écouteurs d'événements pour détecter les changements de mode plein écran
window.addEventListener('fullscreenchange', () => {
    estEnPleinEcranF11 = false;
    redim();
});

window.addEventListener('webkitfullscreenchange', () => {
    estEnPleinEcranF11 = false;
    redim();
});

window.addEventListener('msfullscreenchange', () => {
    estEnPleinEcranF11 = false;
    redim();
});

// Détecter le mode plein écran activé par F11
window.addEventListener('keydown', (event) => {
    if (event.key === 'F11') {
        estEnPleinEcranF11 = !estEnPleinEcranF11;
        redim();
    }
});

function majTaille() {
    const tailleChoisie = document.querySelector('input[name="taille"]:checked').value;    
    tailleCarreaux = tailleChoisie;    
    creeCarreaux('chocolat');
    stocke('taille-carreaux',tailleCarreaux);
    majUrl('taille-carreaux',tailleCarreaux);
}


function lancerAuHasard() {

    console.log('---Lancement au hasard');
    console.log('De ',multipleAmin,' x ',multipleBmin);
    console.log('à ',multipleAmax,' x ',multipleBmax);

    let a = tirerNombreEntier(multipleAmin,multipleAmax);
    let b = tirerNombreEntier(multipleBmin,multipleBmax);

    if (commutatif && pileOuFace(0.5)) {
        const temp = a;
        a = b;
        b = temp;
    }
    
    if (changeFacteurs) {
        let nombre=tirerNombreEntier(0,2);
        const checkboxes = document.querySelectorAll('.affichage-valeurs');
        checkboxes.forEach((checkbox,index) => {
            if (index === nombre) {checkbox.checked = false;}
            else {checkbox.checked = true;}
        });
        afficher();

        if (nombre != 0) {inputA.value = a;}
        if (nombre != 1) {inputB.value = b;}
    } else {
        if (afficherMultipleA) {inputA.value = a;}
        if (afficherMultipleB) {inputB.value = b;}
    }

    creeCarreaux('chocolat',a,b);
}

function majHasard() {
    console.log("---Changement des paramètres du hasard")
    // Récupération des valeurs
    let inputAValueMin = inputAmin.value.replace(/\D/g, ''); // On supprime tout sauf les chiffres
    let inputAValueMax = inputAmax.value.replace(/\D/g, ''); // On supprime tout sauf les chiffres
    let inputBValueMin = inputBmin.value.replace(/\D/g, ''); // On supprime tout sauf les chiffres
    let inputBValueMax = inputBmax.value.replace(/\D/g, ''); // On supprime tout sauf les chiffres
    commutatif = inputReversible.checked;

    console.log('Valeurs récupérées',inputAValueMin,inputAValueMax,inputBValueMin,inputBValueMax);

    // Conversion en nombre entier
    multipleAmin = parseInt(inputAValueMin) || 0;
    multipleAmax = parseInt(inputAValueMax) || 0;
    multipleBmin = parseInt(inputBValueMin) || 0;
    multipleBmax = parseInt(inputBValueMax) || 0;

    // Limiter les valeurs à un maximum de 100
    if (multipleAmin > 100) {multipleAmin = 100;}
    if (multipleAmax > 100) {multipleAmax = 100;}
    if (multipleBmin > 100) {multipleBmin = 100;}
    if (multipleBmax > 100) {multipleBmax = 100;}

    // Mise à jour les inputs avec les valeurs corrigées
    inputAmin.value = multipleAmin;
    inputAmax.value = multipleAmax;
    inputBmin.value = multipleBmin;
    inputBmax.value = multipleBmax;

    console.log('Valeurs finales ',multipleAmin,multipleBmin,multipleAmax,multipleBmax);

    changeFacteurs = inputChangeFacteurs.checked;

    stocke('multiple-a-min', multipleAmin);
    stocke('multiple-a-max', multipleAmax);
    stocke('multiple-b-min', multipleBmin);
    stocke('multiple-b-max', multipleBmax);
    stocke('commutatif', commutatif);
    stocke('changer-affichage-facteurs', changeFacteurs);
    majUrl('multiple-a-min', multipleAmin);
    majUrl('multiple-a-max', multipleAmax);
    majUrl('multiple-b-min', multipleAmin);
    majUrl('multiple-b-max', multipleBmax);
    majUrl('commutatif', commutatif);
    majUrl('changer-affichage-facteurs', changeFacteurs);

}

function visibiliteFacteur(facteur) {
    const checkbox = document.getElementById(`checkbox-multiple-${facteur}`);
    checkbox.checked = !checkbox.checked;
    afficher();
}

function afficher() {

    // Vérifie si la case "inputFlou" est cochée
    afficherMultipleA = checkboxMultipleA.checked;
    afficherMultipleB = checkboxMultipleB.checked;
    afficherResultat = checkboxResultat.checked;

    if (afficherMultipleA) {
        spanMultipleA.classList.remove('transparent');
        inputA.value = multipleA;
    } else {
        spanMultipleA.classList.add('transparent');
        console.log('cacher inputA');
        inputA.value = '';
    }
    if (afficherMultipleB) {
        spanMultipleB.classList.remove('transparent');
        inputB.value = multipleB;
    } else {
        spanMultipleB.classList.add('transparent');
        inputB.value = '';
    }
    if (afficherResultat) {
        spanResultat.classList.remove('transparent');
    } else {
        spanResultat.classList.add('transparent');
    }
   

    stocke('afficher-facteur-a', afficherMultipleA);
    stocke('afficher-facteur-b', afficherMultipleB);
    stocke('afficher-resultat', afficherResultat);
    majUrl('afficher-facteur-a', afficherMultipleA);
    majUrl('afficher-facteur-b', afficherMultipleB);
    majUrl('afficher-resultat', afficherResultat);
}

function couleurs() {
    console.log('---Debut fonction couleurs');

    couleursAxeX = inputAxeX.checked;
    couleursAxeY = inputAxeY.checked;
    alternees = checkboxAlternees.checked;

    if (couleursAxeX) {
        body.classList.add('couleursX');
    } else {
        body.classList.remove('couleursX');
    }
    if (couleursAxeY) {
        body.classList.add('couleursY');
    } else {
        body.classList.remove('couleursY');
    }
    if (alternees) {
        body.classList.add('couleurs-alternees');
    } else {
        body.classList.remove('couleurs-alternees');
    }

    if (couleursAxeX || couleursAxeY){
        checkboxAlternees.disabled=false;
    } else {
        checkboxAlternees.disabled=true;
    }

    details();

    stocke('couleurs-x',couleursAxeX);
    stocke('couleurs-y',couleursAxeY);
    stocke('couleurs-alternees',alternees);
    majUrl('couleurs-x',couleursAxeX);
    majUrl('couleurs-y',couleursAxeY);
    majUrl('couleurs-alternees',alternees);
}

function details() {

    console.log('---Debut fonction details')

    // Vérifie si la case "inputFlou" est cochée
    voileFlou = inputFlou.checked;

    if (voileFlou) {
        divFlou.classList.remove('hide');
        rangeTailleFlou.disabled = false;
        rangeIntensiteFlou.disabled = false;
    } else {
        divFlou.classList.add('hide');
        rangeTailleFlou.disabled = true;
        rangeIntensiteFlou.disabled = true;
    }

    // Vérifie si la case "inputFlou" est cochée
    detailsCarreaux = inputDetailsCarreaux.checked;

    if (detailsCarreaux) {
        divEnsemble.classList.add('hide');
        divEnsembleCouleurs.classList.add('hide');
    } else {
        if (couleursAxeX || couleursAxeY) {
            divEnsemble.classList.add('hide'); 
            divEnsembleCouleurs.classList.remove('hide'); 
        } else {
            divEnsemble.classList.remove('hide'); 
            divEnsembleCouleurs.classList.add('hide'); 
        }
    }
    



    stocke('flou', voileFlou);
    majUrl('flou', voileFlou);
    stocke('details', detailsCarreaux);
    majUrl('details', detailsCarreaux);
}

function papier() {
        // Vérifie si la case "inpurPapier" est cochée
        emballagePapier = inputPapier.checked;

        if (emballagePapier) {
            divPapier.classList.remove('hide');
        } else {
            divPapier.classList.add('hide');
        }

        majUrl('papier', emballagePapier);
        stocke('papier', emballagePapier);
}


function changeSeparations() {
    separations = checkboxSeparations.checked;
    if (separations) {
        divSeparations.classList.remove('hide');
    } else {
        divSeparations.classList.add('hide');
    }
    majUrl('separations', separations);
    stocke('separations', separations);
}

function flou() {

    tailleFlou = rangeTailleFlou.value;
    intensiteFlou = rangeIntensiteFlou.value;

    // Applique la taille de flou (optionnel si tu veux une taille dynamique)
    divFlou.style.width = `${tailleFlou}%`;
    divFlou.style.height = `${tailleFlou}%`;

    if (tailleFlou === '100') {
        divFlou.style.borderRadius = '0px';
    } else {
        divFlou.style.borderRadius = null;
    }

    // Applique l'intensité du flou sur le fond avec `backdrop-filter`
    divFlou.style.backdropFilter = `blur(${intensiteFlou}px)`;

    // Appelle la fonction "debounced" pour le stockage et la mise à jour de l'URL
    stockeEtMajUrlDebounced();

}

let panneauActif = true;
let headerActif = true;

function replierPanneau () {
    panneauActif = !panneauActif;
    if (panneauActif) {
        body.classList.remove('sanspanneau');
    } else {
        body.classList.add('sanspanneau');
    }
    redim();
}
function replierHeader () {
    headerActif = !headerActif;
    if (headerActif) {
        body.classList.remove('sansheader');
    } else {
        body.classList.add('sansheader');
    }
    redim();
}



const stockeEtMajUrlDebounced = debounce(stockeEtMajUrl, 300);

// Fonction debounce
function debounce(func, delay) {
    let timeout;
    return function (...args) {
        clearTimeout(timeout);
        timeout = setTimeout(() => func(...args), delay);
    };
}

// Fonction pour regrouper stockage et mise à jour de l'URL
function stockeEtMajUrl() {
    stocke('taille-flou', tailleFlou);
    stocke('intensite-flou', intensiteFlou);
    majUrl('taille-flou', tailleFlou);
    majUrl('intensite-flou', intensiteFlou);
}

function modeSelection(choix) {
  
    selection = choix;

    const boutons = document.querySelectorAll('#boutons-gauche-bas>button');
    boutons.forEach(bouton => {
        bouton.classList.remove('actif');
    });
    document.getElementById('bouton-'+choix).classList.add('actif');

    const rangeesEtCarreaux = divMultiplication.querySelectorAll('.rangee, .carreau');
    rangeesEtCarreaux.forEach(rangeeOuCarreau => {
        rangeeOuCarreau.classList.remove('invisible','visible');
    });
    body.classList.remove('aucune','ligne','colonne');
    body.classList.add(choix);

    stocke('selection',selection);
    majUrl('selection',selection);

}

