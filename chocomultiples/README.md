## chocomultiples

*Ceci est la présentation de l'application.
Pour l'utiliser directement, rendez-vous sur https://educajou.forge.apps.education.fr/chocomultiples*

## Présentation

Chocomultiples est une application libre qui permet de travailler les mutliples et décompositions multiplicatives.

![](https://forge.apps.education.fr/educajou/chocomultiples/-/raw/main/images/screenshot_droite.png?ref_type=heads)



### Attendus de fin de cycle 2
- Utiliser diverses représentations des nombres (écritures en chiffres et en lettres, noms à
l’oral, graduations sur une demi-droite, constellations sur des dés, doigts de la main...)
- Associer un nombre entier à une position sur une demi-droite graduée, ainsi qu’à la
distance de ce point à l’origine.
- Repérer et placer un nombre décimal sur une demi-droite graduée adaptée. Comparer,
ranger des nombres décimaux.

### Attendus de fin de cycle 3
- Comparer, ranger, encadrer des grands nombres entiers, les repérer et les placer sur une
demi-droite graduée adaptée.
- Repérer et placer un nombre décimal sur une demi-droite graduée adaptée. Comparer,
ranger des nombres décimaux.

## Accès à l'application

### En ligne

Une instance fonctionnelle peut être utilisée sur https://educajou.forge.apps.education.fr/chocomultiples

### Hors ligne

- Télécharger l'application **[📦 chocomultiples-main.zip](https://forge.apps.education.fr/educajou/chocomultiples/-/archive/main/chocomultiples-main.zip)**
- Extraire le ZIP
- Ouvrir le fichier **index.html**


#### Primtux

Ajouter primtuxmenu=true pour adapter l'interface à Primtux.
https://educajou.forge.apps.education.fr/chocomultiples?primtuxmenu=true


## Signaler un bug ou proposer une amélioration

Pour participer au développement en rapportant des bugs ou en proposant une fonctionnalité, vous pouvez déposer un ticket sur cette page :
https://forge.apps.education.fr/educajou/chocomultiples/-/issues

### Comment créer un ticket ?

<iframe title="Créer un ticket sur La Forge des Communs Numériques Éducatifs - Vidéo Tuto" width="560" height="315" src="https://tube-sciences-technologies.apps.education.fr/videos/embed/058b9ab7-ab65-4d25-b24f-6a34feda013d" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups allow-forms"></iframe>

https://tube-sciences-technologies.apps.education.fr/w/1FHx5ntDrd9mwo5k6dAB2F

